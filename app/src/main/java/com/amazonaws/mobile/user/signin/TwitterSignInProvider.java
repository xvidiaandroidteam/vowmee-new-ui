package com.amazonaws.mobile.user.signin;
//
// Copyright 2015 Amazon.com, Inc. or its affiliates (Amazon). All Rights Reserved.
//
// Code generated by AWS Mobile Hub. Amazon gives unlimited permission to 
// copy, distribute and modify it.
//
// Source code generated from template: aws-my-sample-app-android v0.4
//

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

import com.amazonaws.mobile.user.IdentityManager;
import com.digits.sdk.android.Digits;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.tweetcomposer.Card;
import com.twitter.sdk.android.tweetcomposer.ComposerActivity;

import net.xvidia.vowmee.R;
import net.xvidia.vowmee.network.model.FacebookFriend;
import net.xvidia.vowmee.storage.DataStorage;

import java.io.File;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Sign-in provider for Twitter.
 */
public class TwitterSignInProvider implements SignInProvider {
    /** Log tag. */
    private static final String LOG_TAG = TwitterSignInProvider.class.getSimpleName();

    /** The Cognito login key for Facebook to be used in the Cognito login Map. */
    public static final String COGNITO_LOGIN_KEY_TWITTER = "api.twitter.com";

    TwitterLoginButton twitterLoginButton;
    /** User's name. */
    private String userName;

    /** User's image Url. */
    private String userImageUrl;
    /** User's email id. */
    private String userEmailId;

    private Context mContext;
    /**
     * Constuctor. Intitializes the SDK and debug logs the app KeyHash that must be set up with
     * the facebook backend to allow login from the app.
     *
     * @param context the context.
     */
    public TwitterSignInProvider(final Context context) {
//            Log.d(LOG_TAG, "Initializing Twitter SDK...");
            mContext = context;
            String TWITTER_KEY = context.getResources().getString(R.string.twitter_consumer_key);
            String TWITTER_SECRET = context.getResources().getString(R.string.twitter_secret_key);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        callbackManager = CallbackManager.Factory.create();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
//            Fabric.with(context, new Twitter(authConfig), new Crashlytics());
        Fabric.with(context, new Digits(), new Twitter(authConfig));

    }

    /**
     * @return the Facebook AccessToken when signed-in with a non-expired token.
     */
    private TwitterAuthToken getSignedInToken() {
        TwitterSession twitterSession = Twitter.getSessionManager().getActiveSession();
        if(twitterSession == null)
            return null;
        TwitterAuthToken authToken = twitterSession.getAuthToken();

        if (authToken != null) {
            return authToken;
        }

//        Log.d(LOG_TAG, "Twitter Access Token is null or expired.");
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isRequestCodeOurs(final int requestCode) {
        if(requestCode == TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE){
            return true;
        }
        return false;
    }

    /** {@inheritDoc} */
    @Override
    public void handleActivityResult(final int requestCode, final int resultCode, final Intent data) {
        twitterLoginButton.onActivityResult(requestCode, resultCode, data);
    }

    /** {@inheritDoc} */
    @Override
    public void initializeSignInButton(final Activity signInActivity, final View buttonView,
                                       final IdentityManager.SignInResultsHandler resultsHandler) {

    }

    @Override
    public void initializeSignInButton(Activity signInActivity, View buttonView,  TwitterLoginButton twitterButton, final IdentityManager.SignInResultsHandler resultsHandler) {

        twitterLoginButton = twitterButton;
//        twitterLoginButton.setAu
        buttonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                twitterLoginButton.performClick();

            }
        });
        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                userName = result.data.getUserName();
                resultsHandler.onSuccess(TwitterSignInProvider.this);
            }

            @Override
            public void failure(TwitterException exception) {
                resultsHandler.onError(TwitterSignInProvider.this, exception);
            }
        });
    }

    @Override
    public void initializeDigitButton(Activity signInActivity, IdentityManager.SignInResultsHandler resultsHandler) {

    }


    /** {@inheritDoc} */
    @Override
    public String getDisplayName() {
        return "Twitter";
    }

    /** {@inheritDoc} */
    @Override
    public String getCognitoLoginKey() {
        return COGNITO_LOGIN_KEY_TWITTER;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUserSignedIn() {
        return getSignedInToken() != null;
    }

    /** {@inheritDoc} */
    @Override
    public String getToken() {
       TwitterAuthToken authToken = getSignedInToken();
        if (authToken != null) {
            return authToken.token;
        }
        return null;
    }

    @Override
    public String refreshToken() {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public String getSecreteToken() {
        TwitterAuthToken authToken = getSignedInToken();
        if (authToken != null) {
            return authToken.secret;
        }
        return null;
    }
    /** {@inheritDoc} */
    @Override
    public void signOut() {
        clearUserInfo();
        try{
            Twitter.getSessionManager().clearActiveSession();
            Twitter.logOut();
        }catch(NullPointerException e){

        }catch(Exception ex){

        }
    }

    private void clearUserInfo() {
        userName = null;
        userImageUrl = null;
        userEmailId = null;
    }

    /** {@inheritDoc} */
    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public String getUserEmailId() {
        return userEmailId;
    }

    @Override
    public String getFacbookTokenExpiryDate() {
        return null;
    }

    @Override
    public String getFacebookUserId() {
        return null;
    }

    @Override
    public long getTwitterUserId() {
        TwitterSession twitterSession = Twitter.getSessionManager().getActiveSession();
        if (twitterSession != null) {
            return twitterSession.getUserId();
        }
        return 0;
    }

    @Override
    public boolean isTwitterTokenExpiry() {
        TwitterAuthToken authToken = getSignedInToken();
        if (authToken != null) {
            return authToken.isExpired();
        }
        return true;
    }


    /** {@inheritDoc} */
     @Override
    public String getUserImageUrl() {
        return userImageUrl;
    }

    /** {@inheritDoc} */
    public void reloadUserInfo() {
        clearUserInfo();
        if (!isUserSignedIn()) {
            return;
        }


        TwitterSession twitterSession = Twitter.getSessionManager().getActiveSession();
        TwitterAuthClient authClient = new TwitterAuthClient();
//        User user = authClient.showUser(userID);
        new MyTwitterApiClient(twitterSession).getUsersService().show(twitterSession.getUserId(), null, true,
                new Callback<User>() {
                    @Override
                    public void success(Result<User> result) {
//                        Log.d("twittercommunity", "user's profile url is "
//                                + result.data.profileImageUrlHttps);
                        userName =  result.data.screenName;
                        userImageUrl = result.data.profileImageUrlHttps;
                    }

                    @Override
                    public void failure(TwitterException exception) {
//                        Log.d("twittercommunity", "exception is " + exception);
                    }
                });
       /* authClient.requestEmail(twitterSession, new Callback<String>() {
            @Override
            public void success(Result<String> result) {

                userEmailId = result.data;
            }

            @Override
            public void failure(TwitterException exception) {
            }
        });
*/
    }

    @Override
    public List<FacebookFriend> getFriendList() {
        return null;
    }

    /** {@inheritDoc} */
    public void postTwitterAppInvite() {
        if (!isUserSignedIn()) {
            return;
        }
        TwitterSession twitterSession = Twitter.getSessionManager().getActiveSession();
        File myImageFile = new File(net.xvidia.vowmee.Utils.Utils.getInstance().getLocalImagePath(DataStorage.getInstance().getUsername()));
        Uri myImageUri = Uri.fromFile(myImageFile);
        Card card = new Card.AppCardBuilder(mContext)
                .imageUri(myImageUri)
                .build();
        final Intent intent = new ComposerActivity.Builder(mContext)
                .session(twitterSession)
                .card(card)
                .createIntent();
        mContext.startActivity(intent);
    }
}
