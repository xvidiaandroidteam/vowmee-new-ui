package net.xvidia.vowmee;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.location.Location;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.CheckNetworkConnection;
import net.xvidia.vowmee.Utils.GPSTracker;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.camera.CameraHelper;
import net.xvidia.vowmee.network.UploadffmpegService;
import net.xvidia.vowmee.network.model.ContactData;
import net.xvidia.vowmee.network.model.MomentShare;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 *  This activity uses the camera/camcorder as the A/V source for the {@link MediaRecorder} API.
 *  A {@link TextureView} is used as the camera preview which limits the code to API 14+. This
 *  can be easily replaced with a {@link android.view.SurfaceView} to run on older devices.
 */
public class RecordVideo extends AppCompatActivity implements TextureView.SurfaceTextureListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
private static final int REQUEST_CHECK_SETTINGS =123 ;

    private final int MAX_RECORD_DURATION = 20000;
    private Camera mCamera;
    private TextureView mPreview;
    private MediaRecorder mMediaRecorder;
    String mFileName;
    File videofile = null;
    private boolean flashOn = false;
    private static boolean isRecording = false;
    private boolean isPreviewRunning = false;
    private static int previewWidth,previewHeight;
    private static final String TAG = "RecordVideo";
    private ImageButton switchCameraButton;
    private ImageButton flashOnOffButton;
    private ImageButton uploadButton;
    private Button recordButton;
    private TextView broadcastButton;
    private ImageButton liveButton;
    private int cameraRotation = 90;
    private LinearLayout sendBroadcastInfo;
    private RelativeLayout relativeLayout;
    private EditText mCaptionEditText;
    private EditText mDescriptionEditText;
//    private CheckBox publicCheckBox;
//    private CheckBox privateCheckBox;

    private TextView privacySwitchText;
    private SwitchCompat privacySwitch;
    private boolean privateFlag;
    private SwitchCompat locationSwitch;
    private SwitchCompat chatSwitch;
    private LinearLayout mAudienceLayout;
    private LinearLayout mLocationLayout;
    private boolean liveCapture;
    private boolean called;
    String recordVideoPath;
    private ProgressBar barTimer;
    private CountDownTimer timer;
//    private PhoneContactsAdapter mAdapter;
//    private CheckBox followers, following, friends;
//    private RelativeLayout checkboxLayout;
    private String name, phoneNumber;
    private ContactData selectUser;
    HashMap<String, Profile> entryMap;
//    ArrayList<ContactData> selectUsers;
//    ArrayList<String> selectedContactNumbers;
    private static String uUid;
    private boolean groupMoment;
    private String groupUuid;
    private Double latitude;
    private Double longitude;
    private boolean chat;
    private GPSTracker gps;
    private GoogleApiClient googleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera);
        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            liveCapture = DataStorage.getInstance().getIsLiveFlag();//intent.getBooleanExtra(AppConsatants.LIVE_VIDEO, false);
            groupMoment = intent.getBooleanExtra(AppConsatants.UPLOAD_MOMENT_GROUP,false);
            groupUuid = intent.getStringExtra(AppConsatants.UUID);
        }
        latitude = 0d;
        longitude = 0d;
        called = false;
        gps = new GPSTracker(RecordVideo.this);
        mPreview = (TextureView) findViewById(R.id.surface_view);
        switchCameraButton = (ImageButton) findViewById(R.id.button_switch_camera);
        flashOnOffButton = (ImageButton) findViewById(R.id.button_flash);
        uploadButton = (ImageButton) findViewById(R.id.button_upload);
        recordButton = (Button) findViewById(R.id.button_record);
        broadcastButton = (TextView) findViewById(R.id.startBroadcast);
        barTimer = (ProgressBar) findViewById(R.id.progressBarToday);
        liveButton = (ImageButton) findViewById(R.id.button_live);
        sendBroadcastInfo = (LinearLayout) findViewById(R.id.sendBroadcastInfo);
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        mCaptionEditText = (EditText) findViewById(R.id.moment_caption);
        mDescriptionEditText = (EditText) findViewById(R.id.moment_description);
        mAudienceLayout = (LinearLayout) findViewById(R.id.audience_layout);
        mLocationLayout = (LinearLayout)findViewById(R.id.location_layout);
        chatSwitch = (SwitchCompat)findViewById(R.id.switch_chat);
        locationSwitch = (SwitchCompat)findViewById(R.id.switch_location);
        privacySwitch = (SwitchCompat)findViewById(R.id.switch_privacy);
        privacySwitchText = (TextView) findViewById(R.id.switch_privacy_text);
//        publicCheckBox = (CheckBox)findViewById(R.id.public_checkbox);
//        privateCheckBox = (CheckBox)findViewById(R.id.private_checkbox);
        int paddingLeftRight = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_10);
        int paddingTopBottom = Utils.getInstance().convertDimenToInt(R.dimen.button_text_padding);
        broadcastButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight,paddingTopBottom);

        mCaptionEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    mCaptionEditText.setNextFocusDownId(R.id.moment_description);
                    mDescriptionEditText.requestFocus();
                    return true;
                }
                return false;
            }
        });
        mDescriptionEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    mDescriptionEditText.clearFocus();
                    Utils.getInstance().hideKeyboard(RecordVideo.this, mDescriptionEditText);
                    return true;
                }
                return false;
            }
        });
        switchCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchCamera();
//                onCaptureClick(v);
            }
        });

        flashOnOffButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flashOn) {
                    flashOn = false;
                } else {
                    flashOn = true;
                }
                startFlash();
            }
        });
        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendBroadcastInfo.setVisibility(View.GONE);
//                selectVideo();
//                onCaptureClick(v);
            }
        });
        recordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(liveCapture){
//                    startBroadcast();
//                }else {
                    sendBroadcastInfo.setVisibility(View.GONE);
                    liveButton.setVisibility(View.GONE);
                    uploadButton.setVisibility(View.GONE);
                    onCaptureClick();
//                }
            }
        });
        broadcastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    startBroadcast();
            }
        });
        liveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liveCapture =true;
                sendBroadcastInfo.setVisibility(View.VISIBLE);
            }
        });
     /*   publicCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                publicCheckBox.setChecked(isChecked);
                privateCheckBox.setChecked(!isChecked);
                privateFlag = !isChecked;

            }
        });

        privateCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                privateFlag = isChecked;
                publicCheckBox.setChecked(!isChecked);
                privateCheckBox.setChecked(isChecked);
                if(privateFlag){
                    Intent mIntent = new Intent(RecordVideo.this, ShareMomentActivity.class);
//                    Bundle mBundle = new Bundle();
//                    mBundle.putString(AppConsatants.MOMENT_PATH, path);
//                    mBundle.putString(AppConsatants.UUID, groupUuid);
//                    mBundle.putBoolean(AppConsatants.UPLOAD_MOMENT_GROUP, groupMoment);
//                    mBundle.putBoolean(AppConsatants.RESHARE, false);
//                    mIntent.putExtras(mBundle);
                    startActivity(mIntent);
//                    finish();
                }
            }
        });*/
        locationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (CheckNetworkConnection
                        .isConnectionAvailable(getApplicationContext()) && isChecked) {
                    if (gps.canGetLocation()) {
                        Location l = gps.getLocation();

                        if (l != null) {
                            latitude = l.getLatitude();
                            longitude = l.getLongitude();
                            //Log.i("geo location", " latitude " + latitude + " longitude" + longitude);
                        }
                    } else {
                        location();
                    }
                }
            }
        });
        chatSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        chat = isChecked;
            }
        });

        MomentShare momemtShare = new MomentShare();
        UploadffmpegService.setMomentShare(momemtShare);
        SelectPrivateShareActivity.resetValues();
        privacySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                privateFlag = !isChecked;
                if(privateFlag){
                    privacySwitchText.setText(getString(R.string.prompt_private));
                    if(!groupMoment) {
                        Intent mIntent = new Intent(RecordVideo.this, SelectPrivateShareActivity.class);
                        startActivity(mIntent);
                    }
                }else{
                    privacySwitchText.setText(getString(R.string.prompt_public));
                }
            }
        });
        privacySwitch.setChecked(true);
        chatSwitch.setChecked(true);
        chat=true;
        privacySwitchText.setText(getString(R.string.prompt_public));
        privateFlag = false;
        if(groupUuid !=null && !groupUuid.isEmpty()){
            mLocationLayout.setVisibility(View.VISIBLE);
            mAudienceLayout.setVisibility(View.GONE);
            privateFlag= true;
            privacySwitch.setChecked(false);
            chatSwitch.setChecked(true);
            privacySwitchText.setText(getString(R.string.prompt_private));
            ArrayList<String> groupList = new ArrayList<>();
            groupList.add(groupUuid);
            SelectPrivateShareActivity.setGroupList(groupList);
        }else{
            mLocationLayout.setVisibility(View.VISIBLE);
            mAudienceLayout.setVisibility(View.VISIBLE);
        }
        mPreview.setSurfaceTextureListener(this);
//        if(CameraHelper.getDefaultFrontFacingCameraInstance() == null)
//            switchCameraButton.setVisibility(View.GONE);
        if (CameraHelper.getCameraId() == -1)
            CameraHelper.setDefaultCameraId();
        releaseCamera();
        mCamera = CameraHelper.getDefaultCamera(CameraHelper.getCameraId());
        if(mCamera == null){
            Utils.getInstance().displayToast(this,getString(R.string.error_failed_connect_camera),Utils.LONG_TOAST);
            finish();
            return;
        }
        if(!liveCapture){
            relativeLayout.setBackgroundColor(Color.TRANSPARENT);
            sendBroadcastInfo.setVisibility(View.GONE);
            flashOnOffButton.setVisibility(View.VISIBLE);
            uploadButton.setVisibility(View.GONE);
            liveButton.setVisibility(View.GONE);
            switchCameraButton.setVisibility(View.VISIBLE);
            broadcastButton.setVisibility(View.GONE);
            recordButton.setVisibility(View.VISIBLE);
            barTimer.setVisibility(View.VISIBLE);
            recordButton.setText(getString(R.string.action_record));
        }else{
            sendBroadcastInfo.setVisibility(View.VISIBLE);
            flashOnOffButton.setVisibility(View.GONE);
            uploadButton.setVisibility(View.GONE);
            liveButton.setVisibility(View.GONE);
            switchCameraButton.setVisibility(View.GONE);
            recordButton.setVisibility(View.GONE);
            barTimer.setVisibility(View.GONE);
            broadcastButton.setVisibility(View.VISIBLE);
            mCaptionEditText.requestFocus();
            Utils.getInstance().showSoftKeyboard(this,mCaptionEditText);
        }
        if(gps.canGetLocation()){
            locationSwitch.setChecked(true);
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        }else{
            locationSwitch.setChecked(false);
        }
       /* mAdapter = new PhoneContactsAdapter(
                RecordVideo.this,
                mCursor,
                0);
        subscribersListView.setAdapter(mAdapter);
        subscribersListView.setFastScrollEnabled(true);
        subscribersListView.setTextFilterEnabled(true);
        getLoaderManager().initLoader(0, null, this);
        initializeContactsList();
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                getLoaderManager().restartLoader(1, null,RecordVideo.this);
                mAdapter.getFilter().filter(s.toString());
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence constraint) {
                return FeedProvider.searchString(constraint.toString());
            }
        });*/
    }

    public void share(View v) {
          /*  switchCameraButton.setVisibility(View.GONE);
            flashOnOffButton.setVisibility(View.GONE);
            uploadButton.setVisibility(View.GONE);
            recordButton.setVisibility(View.GONE);
            liveButton.setVisibility(View.GONE);
            sendBroadcastInfo.setVisibility(View.GONE);
            checkboxLayout.setVisibility(View.VISIBLE);
            subscribersListView.setVisibility(View.VISIBLE);
            done.setVisibility(View.VISIBLE);
            inputSearch.setVisibility(View.VISIBLE);*/
    }

    private void location() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(RecordVideo.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            settingsrequest();
        }
    }

    public void settingsrequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
// All location settings are satisfied. The client can initialize location
// requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
// Location settings are not satisfied. But could be fixed by showing the user
// a dialog.
                        try {
// Show the dialog by calling startResolutionForResult(),
// and check the result in onActivityResult().
                            status.startResolutionForResult(RecordVideo.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
// Ignore the error.
                        }catch (Exception e){}
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
// Location settings are not satisfied. However, we have no way to fix the
// settings so we won't show the dialog.
                        break;
                }
            }
        });

    }
    private boolean goLive() {

        // Reset errors.
        mCaptionEditText.setError(null);
        mDescriptionEditText.setError(null);

        // Store values at the time of the login attempt.
        String mCaptionText = mCaptionEditText.getText().toString();
        String mDescriptionText = mDescriptionEditText.getText().toString();

        boolean goLiveFlag = true;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(mCaptionText)) {
//            mCaptionEditText.setError(getString(R.string.error_field_required));

            mCaptionEditText.setText("I am live on Vowmee");
//            mCaptionEditText.requestFocus();
//            goLiveFlag = false;
        }

        return goLiveFlag;
    }

    private void startBroadcast() {
        if (goLive()) {
//            if (friends.isChecked()) {
//                isFriendsChecked = true;
//            }
//            if (followers.isChecked()) {
//                isFollowersChecked = true;
//            }
//            if (following.isChecked()) {
//                isFollowingsChecked = true;
//            }

            if(latitude==null){
                latitude = 0d;
            }
            if(longitude==null){
                longitude = 0d;
            }
            releaseCamera();
            relativeLayout.setBackgroundColor(Color.TRANSPARENT);

            Intent mIntentActivity = new Intent(RecordVideo.this, PublishLiveActivity.class);
            Bundle mBundle = new Bundle();
            mBundle.putString(AppConsatants.LIVE_CAPTION, mCaptionEditText.getText().toString());
            mBundle.putString(AppConsatants.UUID, groupUuid);
            mBundle.putBoolean(AppConsatants.UPLOAD_MOMENT_GROUP, groupMoment);
            mBundle.putString(AppConsatants.LIVE_DESCRIPTION, mDescriptionEditText.getText().toString());
            mBundle.putBoolean(AppConsatants.PROFILE_PRIVATE, privateFlag);
            mBundle.putBoolean(AppConsatants.MOMENT_CHAT, chat);
            mBundle.putDouble(AppConsatants.MOMENT_LATITUDE, latitude);
            mBundle.putDouble(AppConsatants.MOMENT_LONGITUDE, longitude);
            mIntentActivity.putExtras(mBundle);
            startActivity(mIntentActivity);
            finish();
//            showDialog();
//            fetch
//           SessionConnectionData();
//            sendBroadcastInfo.setVisibility(View.GONE);
//            mPublisherViewContainer.setVisibility(View.VISIBLE);
//            messageLayout.setVisibility(View.VISIBLE);
        }
    }
    private void startFlash(){
        PackageManager pm= getPackageManager();
        final Camera.Parameters p = mCamera.getParameters();
        if(CameraHelper.isFlashSupported(pm)){

            if (flashOn) {
                //Log.i("info", "torch is turn on!");
                p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                mCamera.setParameters(p);
                mCamera.startPreview();
            } else {
                //Log.i("info", "torch is turn off!");
                p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                mCamera.setParameters(p);
                mCamera.startPreview();
            }
        }else{
            flashOnOffButton.setEnabled(false);
            final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("No Camera Flash");
            alertDialog.setMessage("The device's camera doesn't support flash.");
            alertDialog.setButton(RESULT_OK, "OK", new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, final int which) {
                    //Log.i("err", "The device's camera doesn't support flash.");
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }
    }

    private void switchCamera(){
        if(Camera.getNumberOfCameras() == 1){
            switchCameraButton.setVisibility(View.INVISIBLE);
        }
        else {
            if (isPreviewRunning) {
                mCamera.stopPreview();
            }
            //NB: if you don't release the current camera before switching, you app will crash
            mCamera.release();

            //swap the id of the camera to be used
            if (CameraHelper.getCameraId() == Camera.CameraInfo.CAMERA_FACING_BACK) {
                mCamera = CameraHelper.getDefaultFrontFacingCameraInstance();
                flashOnOffButton.setEnabled(false);
            } else {
                mCamera = CameraHelper.getDefaultBackFacingCameraInstance();
                PackageManager pm= getPackageManager();
                if(CameraHelper.isFlashSupported(pm)) {
                    flashOnOffButton.setEnabled(true);
                }else{
                    flashOnOffButton.setEnabled(false);
                }
            }
            try {
                //this step is critical or preview on new camera will no know where to render to
                setCameraDisplayOrientation(mCamera, previewWidth, previewHeight);
                mCamera.setPreviewTexture(mPreview.getSurfaceTexture());
                mCamera.startPreview();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }






    /**
     * The capture button controls all user interaction. When recording, the button click
     * stops recording, releases {@link MediaRecorder} and {@link Camera}. When not recording,
     * it prepares the {@link MediaRecorder} and starts recording.
     *
     */
    public void onCaptureClick() {
        if (isRecording) {
            // BEGIN_INCLUDE(stop_release_media_recorder)
            if(timer!=null)
                timer.cancel();
            recordButton.setEnabled(false);
            // stop recording and release camera
            mMediaRecorder.stop();  // stop the recording
            releaseMediaRecorder(); // release the MediaRecorder object
//            mCamera.lock();         // take camera access back from MediaRecorder

            // inform the user that recording has stopped
            // setCaptureButtonText("Capture");
            releaseCamera();
            Intent mIntent = new Intent(RecordVideo.this, UploadActivity.class);
            Bundle mBundle = new Bundle();
            mBundle.putString(AppConsatants.MOMENT_PATH, recordVideoPath);
            mBundle.putString(AppConsatants.UUID, groupUuid);
            mBundle.putBoolean(AppConsatants.UPLOAD_MOMENT_GROUP, groupMoment);
            mBundle.putBoolean(AppConsatants.RESHARE,false);
            mBundle.putBoolean(AppConsatants.RECORDED_VIDEO,true);
            mIntent.putExtras(mBundle);
            startActivity(mIntent);
            finish();
            isRecording = false;
            // END_INCLUDE(stop_release_media_recorder)

        } else {

            // BEGIN_INCLUDE(prepare_start_media_recorder)
//            recordButton.setBackgroundColor(getResources().getColor(R.color.tw__composer_red));

           timer =  new CountDownTimer(22000, 1000) {

                public void onTick(long millisUntilFinished) {
                    long seconds = (millisUntilFinished / 1000)-1;
                    barTimer.setProgress((int)seconds);
                    recordButton.setText(""+seconds);
                    recordButton.setTextSize(25);
                    recordButton.setBackgroundColor(Color.TRANSPARENT);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
//                    mTextField.setText("done!");
                }

            }.start();
            new MediaPrepareTask().execute(null, null, null);

            // END_INCLUDE(prepare_start_media_recorder)

        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        // if we are using MediaRecorder, release it first
        releaseMediaRecorder();
        // release the camera immediately on pause event
        releaseCamera();
        if(isRecording)
            finish();
    }

    private void releaseMediaRecorder() {
        if (mMediaRecorder != null) {
            // clear recorder configuration
            mMediaRecorder.reset();
            // release the recorder object
            mMediaRecorder.release();
            mMediaRecorder = null;
            // Lock camera for later use i.e taking it back from MediaRecorder.
            // MediaRecorder doesn't need it anymore and we will release it if the activity pauses.
            mCamera.lock();
        }
    }

    private void releaseCamera() {
        if (mCamera != null) {
            // release the camera for other applications
            mCamera.release();
            mCamera = null;
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private boolean prepareVideoRecorder() {

        // BEGIN_INCLUDE (configure_preview)
        if(mCamera == null)
            mCamera = CameraHelper.getDefaultCameraInstance();

        // We need to make sure that our preview and recording video size are supported by the
        // camera. Query camera to find all the sizes and choose the optimal size given the
        // dimensions of our preview surface.
        if(mCamera==null)
            return false;
        Camera.Parameters parameters = mCamera.getParameters();
        List<Camera.Size> mSupportedPreviewSizes = parameters.getSupportedPreviewSizes();
        Camera.Size optimalSize = CameraHelper.getOptimalPreviewSize(mSupportedPreviewSizes,AppConsatants.CAMERA_WIDTH,AppConsatants.CAMERA_HEIGHT);

        // Use the same size for recording profile.
        CamcorderProfile profile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
//        if(optimalSize!=null) {
//            profile.videoFrameWidth = optimalSize.width;
//            profile.videoFrameHeight = optimalSize.height;
//        }


        // likewise for the camera object itself.
//        parameters.setPreviewSize(profile.videoFrameWidth, profile.videoFrameHeight);
//        mCamera.setParameters(parameters);
//
//        try {
//            // Requires API level 11+, For backward compatibility use {@link setPreviewDisplay}
//            // with {@link SurfaceView}
//            mCamera.setPreviewTexture(mPreview.getSurfaceTexture());
//
//             /*  mCamera.setDisplayOrientation(90);*/
//
//        } catch (IOException e) {
//            //Log.i(TAG, "Surface texture is unavailable or unsuitable" + e.getMessage());
//            return false;
//        }
        // END_INCLUDE (configure_preview)


        // BEGIN_INCLUDE (configure_media_recorder)
        mMediaRecorder = new MediaRecorder();
//        mMediaRecorder.setProfile(profile);
        mMediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
            @Override
            public void onInfo(MediaRecorder mr, int what, int extra) {
                if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                    if (!called) {
                        try {
                            mMediaRecorder.stop();
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                            //Log.i(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
                            releaseMediaRecorder();
                        } catch (Exception e) {
                            //Log.i(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
                            releaseMediaRecorder();
                        }
                        Intent mIntent = new Intent(RecordVideo.this, UploadActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString(AppConsatants.MOMENT_PATH, recordVideoPath);
                        mBundle.putString(AppConsatants.UUID, groupUuid);
                        mBundle.putBoolean(AppConsatants.UPLOAD_MOMENT_GROUP, groupMoment);
                        mBundle.putBoolean(AppConsatants.RESHARE,false);
                        mBundle.putBoolean(AppConsatants.RECORDED_VIDEO,true);
                        mIntent.putExtras(mBundle);
                        startActivity(mIntent);
                        finish();
                        called = true;
                    }
                }
            }
        });
        // Step 1: Unlock and set camera to MediaRecorder
        mCamera.unlock();
//mMediaRecorder.setOrientationHint(270);
        mMediaRecorder.setCamera(mCamera);

        // Step 2: Set sources
        try {
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        String path = Environment.getExternalStorageDirectory() + AppConsatants.FILE_VOWMEE;
        File dir = new File(path);
        if(!dir.exists()){
            dir.mkdirs();
        }
        recordVideoPath = path +Utils.getInstance().generateUUIDStringDateTime()+AppConsatants.FILE_EXTENSION_MP4;
        // Step 4: Set output file
        mMediaRecorder.setOutputFile(recordVideoPath);
        mMediaRecorder.setProfile(profile);
        mMediaRecorder.setMaxDuration(MAX_RECORD_DURATION);
        mMediaRecorder.setOrientationHint(90);
        if (CameraHelper.getCameraId() == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            mMediaRecorder.setOrientationHint(270);
        }
//        int rotation = (cameraInfo.orientation - 180 + 360) % 360;
    //    mMediaRecorder.setVideoFrameRate(10);
        // END_INCLUDE (configure_media_recorder)
     //   mMediaRecorder.setPreviewDisplay(mPreview.getSurfaceTexture());
     //   mMediaRecorder.setCaptureRate(2);
        // Step 5: Prepare configured MediaRecorder

            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            //Log.i(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            //Log.i(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        previewWidth = width;
        previewHeight = height;
        final SurfaceTexture mSurface = surface;
//        if (!isPreviewRunning) {
//        Handler mHandler = new Handler();
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //Do something after 100ms

        if(mCamera == null){
            releaseCamera();
            mCamera = CameraHelper.getDefaultCamera(CameraHelper.getCameraId());
        }
                setCameraDisplayOrientation(mCamera, previewWidth, previewHeight);
                previewCamera(mSurface);
//            }
//        }, 2000);

//            mPreview.setRotation(90.0f);
//        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
//        if (isPreviewRunning) {
//            mCamera.stopPreview();
//        }
//        setCameraDisplayOrientation(mCamera, width, height);
//        previewCamera(surface);

    }


    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
//        mCamera = CameraHelper.getDefaultCameraInstance();
        if(mCamera !=null) {
            mCamera.stopPreview();
            isPreviewRunning = false;
  //          mCamera.release();
        }
            return true;
//        }
//        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    public void previewCamera(SurfaceTexture surface) {
        try {
            mCamera.setPreviewTexture(surface);
            mCamera.startPreview();
            mPreview.setAlpha(1.0f);
            isPreviewRunning = true;
        } catch (Exception e) {
            //Log.i("camera preview", "Cannot start preview", e);
        }
    }

    public void setCameraDisplayOrientation(Camera camera, int width, int height) {
        try {
            if (mCamera == null) {
                return;
            }
            Camera.Parameters parameters = camera.getParameters();

            Camera.CameraInfo camInfo =
                    new Camera.CameraInfo();
            Camera.getCameraInfo(CameraHelper.getCameraId(), camInfo);
            try {
                if (camInfo.canDisableShutterSound) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        mCamera.enableShutterSound(false);
                    }
                }
            } catch (NoSuchFieldError e) {

            } catch (Exception e) {

            }

            Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            int rotation = display.getRotation();
            int degrees = 0;
            switch (rotation) {
                case Surface.ROTATION_0:
                    degrees = 0;
                    break;
                case Surface.ROTATION_90:
                    degrees = 90;
                    break;
                case Surface.ROTATION_180:
                    degrees = 180;
                    break;
                case Surface.ROTATION_270:
                    degrees = 270;
                    break;
            }

            int result;
            if (camInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (camInfo.orientation + degrees) % 360;
                result = (360 - result) % 360;  // compensate the mirror
            } else {  // back-facing
                result = (camInfo.orientation - degrees + 360) % 360;
            }
            Camera.Size mCamerSize = CameraHelper.getOptimalPreviewSize(camera.getParameters().getSupportedPreviewSizes(), width, height);


//        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ) {
            if (mCamerSize != null) {
                parameters.setPreviewSize(mCamerSize.width, mCamerSize.height);
//        }else{
//            parameters.setPreviewSize(mCamerSize.height, mCamerSize.width);
//        }
           /* if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ) {
                mPreview.setLayoutParams(new FrameLayout.LayoutParams(
                        mCamerSize.width, mCamerSize.height, Gravity.CENTER));
            }else{

                mPreview.setLayoutParams(new FrameLayout.LayoutParams(
                        mCamerSize.height, mCamerSize.width, Gravity.CENTER));
            }*/
            }
            camera.setParameters(parameters);
            camera.setDisplayOrientation(result);
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                camera.setDisplayOrientation(90);
            }
        }catch (IllegalStateException e){

        }catch (Exception e){

        }
    }

    @Override
    protected void onResume() {
        if(liveCapture) {
            if (!SelectPrivateShareActivity.isAllFollowers()
                    && !SelectPrivateShareActivity.isAllFriends()
                    && SelectPrivateShareActivity.getGroupList().size() == 0
                    && SelectPrivateShareActivity.getMemberUuidList().size() == 0) {
                privacySwitch.setChecked(true);
                privacySwitchText.setText(getString(R.string.prompt_public));
                privateFlag = false;
            }
        }
        setGoogleAnalyticScreen("Record Video");
        super.onResume();
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
// Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        if(gps!=null &&gps.canGetLocation()){
                            latitude= gps.getLatitude();
                            longitude= gps.getLongitude();
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        if(locationSwitch!=null)
                            locationSwitch.setChecked(false);
//                        settingsrequest();//keep asking if imp or do whatever
                        break;
                }
                break;
        }
    }
    /**
     * Asynchronous task for preparing the {@link MediaRecorder} since it's a long blocking
     * operation.
     */
    class MediaPrepareTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            // initialize video camera
            try{
            if (prepareVideoRecorder()) {
                // Camera is available and unlocked, MediaRecorder is prepared,
                // now you can start recording
                mMediaRecorder.start();

                isRecording = true;
            } else {
                // prepare didn't work, release the camera
                if(timer!=null)
                timer.cancel();
                releaseMediaRecorder();
                return false;
            }
            } catch (IllegalStateException e) {
                e.printStackTrace();
                if(timer!=null)
                    timer.cancel();
Utils.getInstance().displayToast(RecordVideo.this,getString(R.string.error_failed_connect_camera),Utils.SHORT_TOAST);
                finish();
                return false;
            } catch (Exception e) {
                if(timer!=null)
                    timer.cancel();

                Utils.getInstance().displayToast(RecordVideo.this,getString(R.string.error_failed_connect_camera),Utils.SHORT_TOAST);
                finish();

                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            /*if (!result) {
                RecordVideo.this.finish();
            }*/
            // inform the user that recording has started
            // setCaptureButtonText("Stop");

        }
    }

    private void setGoogleAnalyticScreen(String screenName){
        MyApplication application = (MyApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.setClientId(DataStorage.getInstance().getUserUUID());
        mTracker.setAppVersion(""+Utils.getInstance().getAppVersion(MyApplication.getAppContext()));
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

}
