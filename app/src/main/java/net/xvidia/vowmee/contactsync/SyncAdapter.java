/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.xvidia.vowmee.contactsync;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.SyncResult;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.contactsync.provider.FeedContract;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.ContactData;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONArray;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.support.v4.content.PermissionChecker.checkSelfPermission;

/**
 * Define a sync adapter for the app.
 *
 * <p>This class is instantiated in {@link SyncService}, which also binds SyncAdapter to the system.
 * SyncAdapter should only be initialized in SyncService, never anywhere else.
 *
 * <p>The system calls onPerformSync() via an RPC call through the IBinder object supplied by
 * SyncService.
 */
class SyncAdapter extends AbstractThreadedSyncAdapter {
    public static final String TAG = "SyncAdapter";

    /**
     * URL to fetch content from during a sync.
     *
     * <p>This points to the Android Developers Blog. (Side note: We highly recommend reading the
     * Android Developer Blog to stay up to date on the latest Android platform developments!)
     */
//    private static final String FEED_URL = "http://android-developers.blogspot.com/atom.xml";

    /**
     * Network connection timeout, in milliseconds.
     */
    private static final int NET_CONNECT_TIMEOUT_MILLIS = 15000;  // 15 seconds

    /**
     * Network read timeout, in milliseconds.
     */
    private static final int NET_READ_TIMEOUT_MILLIS = 10000;  // 10 seconds

    /**
     * Content resolver, for performing database operations.
     */
    private final ContentResolver mContentResolver;

    /**
     * Project used when querying content provider. Returns all known fields.
     */
    private static final String[] PROJECTION = new String[] {
            FeedContract.Entry._ID,
            FeedContract.Entry.COLUMN_CONTACT_VOWMEE_USERNAME,
            FeedContract.Entry.COLUMN_CONTACT_DISPLAY_NAME,
            FeedContract.Entry.COLUMN_CONTACT_STATUS_MESSAGE,
            FeedContract.Entry.COLUMN_CONTACT_THUNMBNAIL,
            FeedContract.Entry.COLUMN_CONTACT_PROFILE_IMAGE,
            FeedContract.Entry.COLUMN_CONTACT_PHONENUMBER,
            FeedContract.Entry.COLUMN_CONTACT_IS_FOLLOWER,
            FeedContract.Entry.COLUMN_CONTACT_IS_FOLLOWING,
            FeedContract.Entry.COLUMN_CONTACT_IS_FRIEND,
            FeedContract.Entry.COLUMN_CONTACT_IS_FRIEND_REQUESTED_FROM_VIEWER,
            FeedContract.Entry.COLUMN_CONTACT_IS_FRIEND_REQUESTED_TO_VIEWER,
            FeedContract.Entry.COLUMN_CONTACT_IS_BLOCKED};

    // Constants representing column positions from PROJECTION.
    public static final int COLUMN_ID = 0;
//    public static final int COLUMN_CONTACT_ID = 1;
//    public static final int COLUMN_CONTACT_DISPLAY_NAME = 2;
//    public static final int COLUMN_CONTACT_PHONENUMBER = 3;
//    public static final int COLUMN_CONTACT_VOWMEE = 4;
//    public static final int COLUMN_CONTACT_IS_FOLLOWER = 5;
//    public static final int COLUMN_CONTACT_IS_FOLLOWING = 6;
//    public static final int COLUMN_CONTACT_IS_FRIEND = 7;
//    public static final int COLUMN_CONTACT_IS_FRIEND_REUESTED = 8;
//    public static final int COLUMN_CONTACT_IS_BLOCKED = 9;

    ArrayList<ContactData> selectUsers;
    private static int countListRequested= 0;
    private static boolean sendRequest = true;
    public static boolean discardSyncRequest = false;
    HashMap<String, Profile> entryMap;
    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContentResolver = context.getContentResolver();
    }

    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContentResolver = context.getContentResolver();
    }

    /**
     * Called by the Android system in response to a request to run the sync adapter. The work
     * required to read data from the network, parse it, and store it in the content provider is
     * done here. Extending AbstractThreadedSyncAdapter ensures that all methods within SyncAdapter
     * run on a background thread. For this reason, blocking I/O and other long-running tasks can be
     * run <em>in situ</em>, and you don't have to set up a separate thread for them.
     .
     *
     * <p>This is where we actually perform any work required to perform a sync.
     * {@link AbstractThreadedSyncAdapter} guarantees that this will be called on a non-UI thread,
     * so it is safe to peform blocking I/O here.
     *
     * <p>The syncResult argument allows you to pass information back to the method that triggered
     * the sync.
     */
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {
//        Log.i(TAG, "Beginning network synchronization");
        try {
//                Log.i(TAG, "Streaming data from network: " + location);
//                stream = downloadUrl(location);
//                updateLocalFeedData(stream, syncResult);
//                // Makes sure that the InputStream is closed after the app is
//                // finished using it.
//            } finally {
//                if (stream != null) {
//                    stream.close();
//                }
//            }
            if (checkSelfPermission(getContext(), android.Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                discardSyncRequest = true;
                sendRequest = true;
                entryMap = new HashMap<>();
                Cursor contact = getContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
                if (contact != null) {
//                    Log.e("count", "" + contact.getCount());
//                if (contact.getCount() == 0) {
//                    Toast.makeText(Splash.this, "No contacts in your contact list.", Toast.LENGTH_LONG).show();
//                }
                    String contactTemp = "";
                    selectUsers = new ArrayList<>();
                    while (contact.moveToNext()) {
//					Bitmap bit_thumb = null;
//					String id = contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                        String name = contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        String phoneNumber = contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
					/*try {
						if (image_thumb != null) {
							bit_thumb = MediaStore.Images.Media.getBitmap(resolver, Uri.parse(image_thumb));
						} else {
							Log.e("No Image Thumb", "--------------");
						}
					} catch (IOException e) {
						e.printStackTrace();
					}*/

                        phoneNumber = Utils.getInstance().get10DigitPhoneNumber(phoneNumber);
                        if (!contactTemp.equalsIgnoreCase(phoneNumber)) {
                            contactTemp = phoneNumber;
                            ContactData selectUser = new ContactData();
                            selectUser.setDisplayName(name);
                            selectUser.setPhone(phoneNumber);
                            Profile profile = new Profile();
                            profile.setDisplayName(name);
                            profile.setPhone(phoneNumber);
                            selectUsers.add(selectUser);
                            entryMap.put(profile.getPhone(), profile);
//                            Log.e("Cursor selectUsers", "----------------" + selectUser.toString());
                        }
                    }
                }
            } else {
                Log.e("Cursor close 1", "----------------");
            }
            if (selectUsers != null && selectUsers.size() > 0) {
//                Log.e("Size selectUsers", "----------------" + selectUsers.size());
                try {
                    List<ContactData> subList = selectUsers.subList(getStartCount(), getEndCount());
                    getContactFriendInfoRequest(subList, syncResult);
                }catch(IndexOutOfBoundsException e){
//                    Log.e("Index Exception", "----------------" + e.toString());
                    return;
                }catch (Exception e) {
//                    Log.e(TAG, "Error selectUsers: " + e.toString());

                    return;
                }
            }
        } catch (Exception e) {
//            Log.e(TAG, "Error reading from network: " + e.toString());
            syncResult.stats.numIoExceptions++;
            discardSyncRequest = false;
            return;
        }
//        Log.i(TAG, "Network synchronization complete");
    }

    public void updateLocalFeedData(final List<Profile> entries,final SyncResult syncResult){

        for (Profile e : entries) {
            entryMap.put(e.getPhone(), e);
        }
        if(sendRequest) {
            discardSyncRequest = true;
            int startPos = getStartCount();
            int endPos = getEndCount();
            if(startPos<0 ||(startPos>endPos)){
                sendRequest = false;
                try {
                    updateLocalFeedData(syncResult);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (OperationApplicationException e) {
                    e.printStackTrace();
                }catch (Exception e){

                }
                return;
            }
            if(endPos>0 && endPos>= selectUsers.size()){
                sendRequest = false;
                try {
                    updateLocalFeedData(syncResult);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (OperationApplicationException e) {
                    e.printStackTrace();
                }catch (Exception e){

                }
                return;
            }

            List<ContactData> subList = selectUsers.subList(startPos, endPos);
            sendUploadRequest(subList, syncResult);
        }else{
            try {
                updateLocalFeedData(syncResult);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (OperationApplicationException e) {
                e.printStackTrace();
            }catch (Exception e){

            }
        }
    }
    private void sendUploadRequest(final List<ContactData> subList,final SyncResult syncResult){
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                try {
                    getContactFriendInfoRequest(subList, syncResult);

//                }catch (Exception e){
//
//                }
//            }
//        },20000);
    }
    /**
     * Read XML from an input stream, storing it into the content provider.
     *
     * <p>This is where incoming data is persisted, committing the results of a sync. In order to
     * minimize (expensive) disk operations, we compare incoming data with what's already in our
     * database, and compute a merge. Only changes (insert/update/delete) will result in a database
     * write.
     *
     * <p>As an additional optimization, we use a batch operation to perform all database writes at
     * once.
     *
     * <p>Merge strategy:
     * 1. Get cursor to all items in feed<br/>
     * 2. For each item, check if it's in the incoming data.<br/>
     *    a. YES: Remove from "incoming" list. Check if data has mutated, if so, perform
     *            database UPDATE.<br/>
     *    b. NO: Schedule DELETE from database.<br/>
     * (At this point, incoming database only contains missing items.)<br/>
     * 3. For any items remaining in incoming list, ADD to database.
     */
    public void updateLocalFeedData(final SyncResult syncResult)
            throws IOException,OperationApplicationException{
        final ContentResolver contentResolver = getContext().getContentResolver();
        ArrayList<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();

        // Build hash table of incoming entries
//        for (Profile e : entries) {
//            entryMap.put(e.getPhone(), e);
//        }

        // Get list of all items
//        Log.i(TAG, "Fetching local entries for merge");
        Uri uri = FeedContract.Entry.CONTENT_URI; // Get all entries
        Cursor c = contentResolver.query(uri, PROJECTION, null, null, null);
        assert c != null;
//        Log.i(TAG, "Found " + c.getCount() + " local entries. Computing merge solution...");

        // Find stale data
        int id;
        String contactName;
        String contactNo;
        String vowmeeUsername;
        String contactFriend;
        String statusMessage;
        String thumbnail;
        String profileImage;
        String contactFriendRequestedFromViewer;
        String contactFriendRequestedToViewer;
        String contactFollowing;
        String contactFollower;
        String contactBlocked;
        while (c.moveToNext()) {
            syncResult.stats.numEntries++;
            id = c.getInt(COLUMN_ID);
            contactName = c.getString(c.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_DISPLAY_NAME));
            statusMessage = c.getString(c.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_STATUS_MESSAGE));
            thumbnail = c.getString(c.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_THUNMBNAIL));
            profileImage = c.getString(c.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_PROFILE_IMAGE));
            contactNo = c.getString(c.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_PHONENUMBER));
            vowmeeUsername = c.getString(c.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_VOWMEE_USERNAME));
            contactFollower = c.getString(c.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_IS_FOLLOWER));
            contactFollowing = c.getString(c.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_IS_FOLLOWING));
            contactFriend = c.getString(c.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_IS_FRIEND));
            contactFriendRequestedFromViewer = c.getString(c.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_IS_FRIEND_REQUESTED_FROM_VIEWER));
            contactFriendRequestedToViewer = c.getString(c.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_IS_FRIEND_REQUESTED_FROM_VIEWER));
            contactBlocked = c.getString(c.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_IS_BLOCKED));
            Profile match = entryMap.get(contactNo);
            if (match != null) {
                // Entry exists. Remove from entry map to prevent insert later.
                entryMap.remove(contactNo);
                // Check to see if the entry needs to be updated
                Uri existingUri = FeedContract.Entry.CONTENT_URI.buildUpon()
                        .appendPath(Integer.toString(id)).build();
                if ((match.getDisplayName() != null && !match.getDisplayName().equals(contactName)) ||
                        (match.getUsername() != null && !match.getUsername().equals(vowmeeUsername)) ||
//                        (match.getPhone() != null && !match.getPhone().equals(contactNo)) ||
                        (!String.valueOf(match.isFriendWithViewer()).equals(contactFriend))||
                        (!String.valueOf(match.isFollowingViewer()).equals(contactFollowing))||
                        (!String.valueOf(match.isFollowedByViewer()).equals(contactFollower))||
                        (!String.valueOf(match.isFriendRequestFromViewer()).equals(contactFriendRequestedFromViewer))||
                        (!String.valueOf(match.isFriendRequestToViewer()).equals(contactFriendRequestedToViewer))||
                        (!String.valueOf(match.getProfileImage()).equals(profileImage))||
                        (!String.valueOf(match.getThumbNail()).equals(thumbnail))||
                        (!String.valueOf(match.isHasBlockedViewer()).equals(contactBlocked))) {
                    // Update existing record
//                    Log.i(TAG, "Scheduling update: " + existingUri+"\n"+match.toString());
                    batch.add(ContentProviderOperation.newUpdate(existingUri)
                            .withValue(FeedContract.Entry.COLUMN_CONTACT_VOWMEE_USERNAME, match.getUsername())
                            .withValue(FeedContract.Entry.COLUMN_CONTACT_DISPLAY_NAME, match.getDisplayName())
                            .withValue(FeedContract.Entry.COLUMN_CONTACT_STATUS_MESSAGE, match.getStatusMsg())
                            .withValue(FeedContract.Entry.COLUMN_CONTACT_THUNMBNAIL, match.getThumbNail())
                            .withValue(FeedContract.Entry.COLUMN_CONTACT_PROFILE_IMAGE, match.getProfileImage())
                            .withValue(FeedContract.Entry.COLUMN_CONTACT_PHONENUMBER, match.getPhone())
                            .withValue(FeedContract.Entry.COLUMN_CONTACT_IS_FRIEND, String.valueOf(match.isFriendWithViewer()))
                            .withValue(FeedContract.Entry.COLUMN_CONTACT_IS_FOLLOWER, String.valueOf(match.isFollowingViewer()))
                            .withValue(FeedContract.Entry.COLUMN_CONTACT_IS_FOLLOWING, String.valueOf(match.isFollowedByViewer()))
                            .withValue(FeedContract.Entry.COLUMN_CONTACT_IS_FRIEND_REQUESTED_FROM_VIEWER, String.valueOf(match.isFriendRequestFromViewer()))
                            .withValue(FeedContract.Entry.COLUMN_CONTACT_IS_FRIEND_REQUESTED_TO_VIEWER, String.valueOf(match.isFriendRequestToViewer()))
                            .withValue(FeedContract.Entry.COLUMN_CONTACT_IS_BLOCKED, String.valueOf(match.isHasBlockedViewer()))
                            .build());
                    syncResult.stats.numUpdates++;
                } else {
//                    Log.i(TAG, "No action: " + existingUri);
                }
            } else {
                // Entry doesn't exist. Remove it from the database.
                Uri deleteUri = FeedContract.Entry.CONTENT_URI.buildUpon()
                        .appendPath(Integer.toString(id)).build();
//                Log.i(TAG, "Scheduling delete: " + deleteUri);
                batch.add(ContentProviderOperation.newDelete(deleteUri).build());
                syncResult.stats.numDeletes++;
            }
        }
        c.close();

        // Add new items
        for (Profile match : entryMap.values()) {
//            Log.i(TAG, "Scheduling insert: getPhone=" +match.toString());
            batch.add(ContentProviderOperation.newInsert(FeedContract.Entry.CONTENT_URI)
                    .withValue(FeedContract.Entry.COLUMN_CONTACT_VOWMEE_USERNAME, match.getUsername())
                    .withValue(FeedContract.Entry.COLUMN_CONTACT_DISPLAY_NAME, match.getDisplayName())
                    .withValue(FeedContract.Entry.COLUMN_CONTACT_DISPLAY_NAME, match.getStatusMsg())
                    .withValue(FeedContract.Entry.COLUMN_CONTACT_DISPLAY_NAME, match.getThumbNail())
                    .withValue(FeedContract.Entry.COLUMN_CONTACT_DISPLAY_NAME, match.getProfileImage())
                    .withValue(FeedContract.Entry.COLUMN_CONTACT_PHONENUMBER, match.getPhone())
                    .withValue(FeedContract.Entry.COLUMN_CONTACT_IS_FRIEND, String.valueOf(match.isFriendWithViewer()))
                    .withValue(FeedContract.Entry.COLUMN_CONTACT_IS_FOLLOWER, String.valueOf(match.isFollowingViewer()))
                    .withValue(FeedContract.Entry.COLUMN_CONTACT_IS_FOLLOWING, String.valueOf(match.isFollowedByViewer()))
                    .withValue(FeedContract.Entry.COLUMN_CONTACT_IS_FRIEND_REQUESTED_FROM_VIEWER, String.valueOf(match.isFriendRequestFromViewer()))
                    .withValue(FeedContract.Entry.COLUMN_CONTACT_IS_FRIEND_REQUESTED_TO_VIEWER, String.valueOf(match.isFriendRequestToViewer()))
                    .withValue(FeedContract.Entry.COLUMN_CONTACT_IS_BLOCKED, String.valueOf(match.isHasBlockedViewer()))
                    .build());
            syncResult.stats.numInserts++;
        }
//        Log.i(TAG, "Merge solution ready. Applying batch update");
                         // IMPORTANT: Do not sync to network
        // This sample doesn't support uploads, but if *your* code does, make sure you set
        // syncToNetwork=false in the line above to prevent duplicate syncs.
//       else{
            try {
                mContentResolver.applyBatch(FeedContract.CONTENT_AUTHORITY, batch);
            } catch (RemoteException e) {
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();

            }

            mContentResolver.notifyChange(
                    FeedContract.Entry.CONTENT_URI, // URI where data was modified
                    null,                           // No local observer
                    false);
        sendRequest =true;
//        }

    }


    private int getEndCount(){
        int returnCount = countListRequested;
        if(selectUsers != null && selectUsers.size()>0){
            returnCount = selectUsers.size();
            if((returnCount%300)>countListRequested){
                countListRequested = countListRequested+1;
                if(selectUsers.size() >= 300) {
                    returnCount = countListRequested * 300;
                }else{
                    returnCount = selectUsers.size();
                }
            }else{
                returnCount = returnCount-1;
                sendRequest = false;
            }
        }
        return returnCount;
    }
    private int getStartCount(){
        int returnCount = countListRequested*300;
        if(selectUsers != null && selectUsers.size()>0 && returnCount>selectUsers.size()){
           returnCount = selectUsers.size()-1;
        }
        return returnCount;
    }

    private void getContactFriendInfoRequest(List<ContactData> stringArrayList,final SyncResult syncResult) {
        try {

            String url = ServiceURLManager.getInstance().getConctactList(DataStorage.getInstance().getUsername());

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(stringArrayList);
//                Log.i(TAG, "Found " + stringArrayList.size() + " getConctactList...");
                Log.i("Contact list",jsonObject);
            } catch (IOException e) {
                e.printStackTrace();
            }
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    ObjectMapper mapper = new ObjectMapper();
                    List<Profile> obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Profile>>(){});
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {
                            try {

//                                Log.i(TAG, "Found Response " + obj.size() + " getConctactList...");
                                updateLocalFeedData(obj, syncResult);
                            } catch (Exception e) {
                                e.printStackTrace();
                                discardSyncRequest = false;
                            }

                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null && error.networkResponse != null) {
//                        Log.e("VolleyError ", "" + error.networkResponse.statusCode);
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND) {
//                            Toast.makeText(context, "Please try again",
//                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
//                        Toast.makeText(context, "Please try again",
//                                Toast.LENGTH_SHORT).show();
                    }

                    discardSyncRequest = false;
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
//            e.printStackTrace();
            discardSyncRequest = false;
        }
    }
}
