/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.xvidia.vowmee.contactsync;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.ContactsContract;

import com.android.volley.toolbox.JsonObjectRequest;

import net.xvidia.vowmee.network.model.ContactData;

import java.util.ArrayList;

/** Service to handle sync requests.
 *
 * <p>This service is invoked in response to Intents with action android.content.SyncAdapter, and
 * returns a Binder connection to SyncAdapter.
 *
 * <p>For performance, only one sync adapter will be initialized within this application's context.
 *
 * <p>Note: The SyncService itself is not notified when a new sync occurs. It's role is to
 * manage the lifecycle
 */
public class UploadContactService extends Service {
//    private static final String TAG = "UploadContactService";

    private JsonObjectRequest request = null;
    Cursor contact;
    ArrayList<ContactData> selectUsers;
    @Override
    public void onCreate() {
//        Log.i(TAG, "Service created");
        super.onCreate();
        contact = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        LoadContact loadContact = new LoadContact();
        loadContact.execute();
        return  START_STICKY;
    }

    @Override
    /**
     * Logging-only destructor.
     */
    public void onDestroy() {
        super.onDestroy();
//        Log.i(TAG, "Service destroyed");
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    // Load data on background
    class LoadContact extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Get Contact list from Phone
            if (contact != null) {
//                Log.e("count", "" + contact.getCount());
//                if (contact.getCount() == 0) {
//                    Toast.makeText(Splash.this, "No contacts in your contact list.", Toast.LENGTH_LONG).show();
//                }
                selectUsers = new ArrayList<>();
                while (contact.moveToNext()) {
//					Bitmap bit_thumb = null;
//					String id = contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                    String name = contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phoneNumber = contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
					/*try {
						if (image_thumb != null) {
							bit_thumb = MediaStore.Images.Media.getBitmap(resolver, Uri.parse(image_thumb));
						} else {
							Log.e("No Image Thumb", "--------------");
						}
					} catch (IOException e) {
						e.printStackTrace();
					}*/

                    ContactData selectUser = new ContactData();
                    selectUser.setDisplayName(name);
                    selectUser.setPhone(phoneNumber);
                    selectUsers.add(selectUser);
                }
            } else {
//                Log.e("Cursor close 1", "----------------");
            }
            //contact.close();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            Log.i("Contact ", "-------ContactData---------" + .selectUsers.size());
        }
    }
}
