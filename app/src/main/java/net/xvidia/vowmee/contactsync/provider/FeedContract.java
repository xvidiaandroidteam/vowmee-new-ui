/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.xvidia.vowmee.contactsync.provider;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract;

/**
 * Field and table name constants for
 * {@link net.xvidia.vowmee.contactsync.provider.FeedProvider}.
 */
public class FeedContract {
    private FeedContract() {
    }

    /**
     * Content provider authority.
     */
    public static final String CONTENT_AUTHORITY = "net.xvidia.vowmee";

    /**
     * Base URI. (content://com.example.android.basicsyncadapter)
     */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    /**
     * Path component for "entry"-type resources..
     */
    private static final String PATH_ENTRIES = "entries";

    /**
     * Columns supported by "entries" records.
     */
    public static class Entry implements BaseColumns {
        /**
         * MIME type for lists of entries.
         */
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.vowmee.entries";
        /**
         * MIME type for individual entries.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.vowmee.entry";

        /**
         * Fully qualified URI for "entry" resources.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ENTRIES).build();


        /**
         * Table name where records are stored for "entry" resources.
         */
        public static final String TABLE_NAME = "contact_vowmee";
        public static final String COLUMN_CONTACT_DISPLAY_NAME = "contactName";
        public static final String COLUMN_CONTACT_STATUS_MESSAGE = "contactStatusMEssage";
        public static final String COLUMN_CONTACT_THUNMBNAIL = "contactThumbnail";
        public static final String COLUMN_CONTACT_PROFILE_IMAGE = "contactProfileImage";
        public static final String COLUMN_CONTACT_PHONENUMBER = "contactNo";
        public static final String COLUMN_CONTACT_VOWMEE_USERNAME = "vowmeeUsername";
        public static final String COLUMN_CONTACT_IS_FRIEND = "contactFriend";
        public static final String COLUMN_CONTACT_IS_FRIEND_REQUESTED_FROM_VIEWER = "contactFriendRequestedFromViewer";
        public static final String COLUMN_CONTACT_IS_FRIEND_REQUESTED_TO_VIEWER = "contactFriendRequestedToViewer";
        public static final String COLUMN_CONTACT_IS_FOLLOWING = "contactFollowing";
        public static final String COLUMN_CONTACT_IS_FOLLOWER = "contactFollower";
        public static final String COLUMN_CONTACT_IS_BLOCKED = "contactBlocked";
    }
}