package net.xvidia.vowmee.contactsync;

import android.database.ContentObserver;
import android.os.Handler;

/**
 * Created by Ravi_office on 01-Feb-16.
 */
public class ContactObserver extends ContentObserver {
    /**
     * Creates a content observer.
     *
     * @param handler The handler to run {@link #onChange} on, or null if none.
     */
    public ContactObserver(Handler handler) {
        super(handler);
    }

    @Override
    public boolean deliverSelfNotifications() {
        return true;
    }


    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
//        int vowmeeContactCount = SyncUtils.getContactCount();
//        int contactDatabaseCount = SyncUtils.getContactDataBaseCount();
        SyncUtils.TriggerRefresh();
//        if(vowmeeContactCount==contactDatabaseCount)
    }
}
