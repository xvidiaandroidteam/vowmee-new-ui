package net.xvidia.vowmee;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import net.xvidia.vowmee.camera.CameraHelper;

import java.io.IOException;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
	private SurfaceHolder mHolder;
	private Camera mCamera;
	Context mContext;

	public CameraPreview(Context context, Camera camera) {
		super(context);
		mCamera = camera;
		mHolder = getHolder();
		mHolder.addCallback(this);
		mContext=context;
		// deprecated setting, but required on Android versions prior to 3.0
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	public void surfaceCreated(SurfaceHolder holder) {
		try {
			// create the surface and start camera preview
			if (mCamera == null) {
				mCamera.setPreviewDisplay(holder);
				mCamera.startPreview();
			}
		} catch (IOException e) {
//			Log.d(VIEW_LOG_TAG, "Error setting camera preview: " + e.getMessage());
		}
	}

	public void refreshCamera(Camera camera,int width, int height) {
		if (mHolder.getSurface() == null) {
			// preview surface does not exist
			return;
		}
		// stop preview before making changes
		try {
			mCamera.stopPreview();
		} catch (Exception e) {
			// ignore: tried to stop a non-existent preview
		}
		// set preview size and make any resize, rotate or
		// reformatting changes here
		// start preview with new settings
		setCamera(camera);
		try {
//			setCameraDisplayOrientation(mCamera,width,height);
			setCameraDisplayOrientation(mContext,0,mCamera);
			mCamera.setPreviewDisplay(mHolder);
			mCamera.startPreview();
		} catch (Exception e) {
//			Log.d(VIEW_LOG_TAG, "Error starting camera preview: " + e.getMessage());
		}
	}
	public static void setCameraDisplayOrientation(Context activity,
												   int cameraId, android.hardware.Camera camera) {
		android.hardware.Camera.CameraInfo info =
				new android.hardware.Camera.CameraInfo();
		android.hardware.Camera.getCameraInfo(cameraId, info);
		int rotation = ((Activity)activity).getWindowManager().getDefaultDisplay()
				.getRotation();
		int degrees = 0;
		switch (rotation) {
			case Surface.ROTATION_0: degrees = 0; break;
			case Surface.ROTATION_90: degrees = 90; break;
			case Surface.ROTATION_180: degrees = 180; break;
			case Surface.ROTATION_270: degrees = 270; break;
		}

		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360;  // compensate the mirror
		} else {  // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		camera.setDisplayOrientation(result);
	}
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		// If your preview can change or rotate, take care of those events here.
		// Make sure to stop the preview before resizing or reformatting it.
		refreshCamera(mCamera,w,h);
	}

	public void setCamera(Camera camera) {
		//method to set a camera instance
		mCamera = camera;
	}
	public void setCameraDisplayOrientation(Camera camera, int width, int height) {
		Camera.Parameters parameters = camera.getParameters();

		Camera.CameraInfo camInfo =
				new Camera.CameraInfo();
		Camera.getCameraInfo(CameraHelper.getCameraId(), camInfo);

		Display display = ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		int rotation = display.getRotation();
		int degrees = 0;
		switch (rotation) {
			case Surface.ROTATION_0:
				degrees = 0;
				break;
			case Surface.ROTATION_90:
				degrees = 90;
				break;
			case Surface.ROTATION_180:
				degrees = 180;
				break;
			case Surface.ROTATION_270:
				degrees = 270;
				break;
		}

		int result;
		if (camInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (camInfo.orientation + degrees) % 360;
			result = (360 - result) % 360;  // compensate the mirror
		} else {  // back-facing
			result = (camInfo.orientation - degrees + 360) % 360;
		}
		Camera.Size mCamerSize = CameraHelper.getOptimalPreviewSize(mCamera.getParameters().getSupportedPreviewSizes(), width, height);


//        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ) {
		if(mCamerSize !=null) {
			parameters.setPreviewSize(mCamerSize.width, mCamerSize.height);
//        }else{
//            parameters.setPreviewSize(mCamerSize.height, mCamerSize.width);
//        }
//			if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ) {
//				mPreview.setLayoutParams(new FrameLayout.LayoutParams(
//						mCamerSize.width, mCamerSize.height, Gravity.CENTER));
//			}else{
//
//				mPreview.setLayoutParams(new FrameLayout.LayoutParams(
//						mCamerSize.height, mCamerSize.width, Gravity.CENTER));
//			}
		}
		mCamera.setParameters(parameters);
		camera.setDisplayOrientation(result);
//        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ){
//            camera.setDisplayOrientation(90);
//        }
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		// mCamera.release();

	}
}