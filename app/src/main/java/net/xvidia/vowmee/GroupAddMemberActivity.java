package net.xvidia.vowmee;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.listadapter.GroupAddMemberListAdapter;
import net.xvidia.vowmee.listadapter.RecyclerViewScrollListener;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.GroupProfile;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class GroupAddMemberActivity extends AppCompatActivity {


    private Context context;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    private ArrayList<Profile> searchList;
    private ArrayList<Profile> groupMemberList;
    private static Button next;
    private static MenuItem menuNext;
    //    private static MenuItem menuSave;
//    private static MenuItem menuCancel;
    private Toolbar toolbar;
    private TextView mMessageTextView;
//    private SwipeRefreshLayout swipeRefreshLayout;
    private static boolean createNewGroup;
    private ProgressBar mProgressBar;
    private boolean disableRequest;

    private static ArrayList<Profile> memberList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_members_add);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mMessageTextView = (TextView) findViewById(R.id.message_no_results);
        mMessageTextView.setVisibility(View.GONE);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        toolbar.setPadding(0, 0, 0, 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView_list);
        mRecyclerView.setHasFixedSize(true);
        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            createNewGroup = intent.getBooleanExtra(AppConsatants.PROFILE_REGISTER, true);
        }
        if(!createNewGroup) {
            getGroupMemberList();
            getSupportActionBar().setTitle(getString(R.string.add_meember_to_group,ModelManager.getInstance().getGroupProfile().getName()));
        }else {
            if(GroupSettingActivity.getGroupProfile()!=null)
                getSupportActionBar().setTitle(getString(R.string.add_meember_to_group,GroupSettingActivity.getGroupProfile().getName()));
        }
        memberList = new ArrayList<>();
        searchList = new ArrayList<>();
//        swipeRefreshLayout.setOnRefreshListener(this);
        initialiseRecyclerView();
//        swipeRefreshLayout.setRefreshing(true);



        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        context = GroupAddMemberActivity.this;


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        showProgressBar();
//        swipeRefreshLayout.setRefreshing(true);
//        groupMemberList = GroupMemberActivity.getMemberList();
//        if(groupMemberList==null)
//            getGroupMemberList();
//        if(groupMemberList!=null && groupMemberList.size()==0)
//            getGroupMemberList();

        initialiseData();
    }

    private void initialiseData() {
        String userName = DataStorage.getInstance().getUsername();
        sendListRequest(IAPIConstants.API_KEY_GET_FRIEND_LIST, userName, userName, "0", "0");
    }

    /**
     * intialises the recycler view
     */
    private void initialiseRecyclerView() {
//        swipeRefreshLayout.setRefreshing(false);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        filterOutMembers();
        mAdapter = new GroupAddMemberListAdapter(GroupAddMemberActivity.this, searchList, false);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerViewScrollListener(this) {
            @Override
            public void onMoved(int distance) {
                toolbar.setTranslationY(-distance);
            }
        });
    }

    /**
     * Filters already member from the all add member list
     */
    private void filterOutMembers(){
        if(!createNewGroup) {
            if (groupMemberList != null
                    && searchList != null
                    && searchList.size() > 0
                    && groupMemberList.size() > 0)
                searchList.removeAll(groupMemberList);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_create_group_next, menu);
        if (menu != null) {
            menuNext = menu.findItem(R.id.action_create_group_next);
            MenuItemCompat.setActionView(menuNext, R.layout.group_next);
            View viewNext = (View) MenuItemCompat.getActionView(menuNext);
            next = (Button) viewNext.findViewById(R.id.group_next);
            next.setEnabled(false);
            next.setTextColor(getResources().getColor(R.color.white_disabled_menu));
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        setSelectedMemberList();
                }
            });
            if (memberList != null) {
                if (memberList.size() > 0) {
                    if (createNewGroup) {
                        if (next != null) {
                            next.setVisibility(View.VISIBLE);
                            menuNext.setVisible(true);
                            next.setEnabled(true);
                            next.setText(MyApplication.getAppContext().getResources().getString(R.string.action_next));
                            next.setTextColor(getResources().getColor(R.color.white));
                        }
                    } else {
                        menuNext.setVisible(true);
                        next.setVisibility(View.VISIBLE);
                        next.setEnabled(true);
                        next.setText(getResources().getString(R.string.action_done));
                        next.setTextColor(getResources().getColor(R.color.white));
                    }

                } else {
                    if (createNewGroup) {
                        if (next != null) {
                            next.setVisibility(View.VISIBLE);
                            menuNext.setVisible(true);
                            next.setEnabled(false);
                            next.setText(MyApplication.getAppContext().getResources().getString(R.string.action_next));
                            next.setTextColor(getResources().getColor(R.color.white_disabled_menu));
                        }
                    } else {
                        menuNext.setVisible(true);
                        next.setVisibility(View.VISIBLE);
                        next.setEnabled(false);
                        next.setText(getResources().getString(R.string.action_done));
                        next.setTextColor(getResources().getColor(R.color.white_disabled_menu));
                    }


                }
            } else {
                if (createNewGroup) {
                    if (next != null) {
                        next.setVisibility(View.VISIBLE);
                        menuNext.setVisible(true);
                        next.setEnabled(false);
                        next.setText(MyApplication.getAppContext().getResources().getString(R.string.action_next));
                        next.setTextColor(getResources().getColor(R.color.white_disabled_menu));
                    }
                } else {
                    menuNext.setVisible(true);
                    next.setVisibility(View.VISIBLE);
                    next.setEnabled(false);
                    next.setText(getResources().getString(R.string.action_done));
                    next.setTextColor(getResources().getColor(R.color.white_disabled_menu));
                }
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
          *//*  case R.id.action_create_group_next:
                setSelectedMemberList();
                break;*//*
            case R.id.action_save_group:
                setSelectedMemberList();
                break;
            case R.id.action_cancel_group:
                onBackPressed();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }*/

    private void getGroupMemberList() {
        try {
            String userUUID = DataStorage.getInstance().getUserUUID();
            final String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
            String url = ServiceURLManager.getInstance().getGroupMemberListUrl(groupUuid, userUUID, "0", "0");

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<Profile> obj = null;
                    try {
//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Profile>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {

                        if (obj.size() > 0) {
                            groupMemberList = (ArrayList<Profile>) obj;
                        }
                    } else {

//                        showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                            }
//                        });
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    mRecyclerView.setVisibility(View.GONE);

//                    showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
                }
            }) {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        //                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSelectedMemberList() {
        ArrayList<String> memberList = new ArrayList<String>();

        ArrayList<Profile> memberProfileList = new ArrayList<Profile>();
        try {

            Profile obj = null;
            if (GroupAddMemberListAdapter.selectedItems == null) {
                return;
            }
            for (int i = 0; i < GroupAddMemberListAdapter.selectedItems.size(); i++) {
                int pos = GroupAddMemberListAdapter.selectedItems.keyAt(i);
                obj = searchList.get(pos);
                if (obj != null) {
                    memberProfileList.add(obj);
                    memberList.add(obj.getUuid());
                }

            }
            if (createNewGroup) {
                GroupSettingActivity.setMemberList(memberProfileList);
                GroupProfile profile = GroupSettingActivity.getGroupProfile();
                if (profile != null) {
                    profile.setInitialMembers(memberList);
                    GroupSettingActivity.setGroupProfile(profile);
                    Intent mIntent = new Intent(GroupAddMemberActivity.this, GroupSettingActivity.class);
                    startActivity(mIntent);
                } else {
                    finish();
                }
            } else {
                if(!disableRequest)
                    sendAddMemberListRequest(memberList);
            }
        } catch (Exception ex) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void sendFollowersListRequest(int urlKey, final String usernameOwn, final String usernameOther, String page, String size) {
        try {

            String url = ServiceURLManager.getInstance().getListUrl(urlKey, usernameOwn, usernameOther, page, size);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    List<Profile> obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Profile>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {

                            mMessageTextView.setVisibility(View.GONE);
                            if (searchList != null && searchList.size() > 0) {
                                searchList.addAll((ArrayList) obj);
                            } else {
                                searchList = (ArrayList) obj;
                            }
                            HashSet<Profile> uniqueValues = new HashSet<>(searchList);
                            searchList = new ArrayList<>();
                            searchList.addAll(uniqueValues);
//                            initialiseRecyclerView();
//                            if (mRecyclerView != null)
//                                mRecyclerView.setVisibility(View.VISIBLE);
//
//                            if (mAdapter != null)
//                                mAdapter.notifyDataSetChanged();

                        } else {
//                            if (searchList != null && searchList.size() == 0) {
//                                mMessageTextView.setVisibility(View.VISIBLE);
//                                if (mRecyclerView != null)
//                                    mRecyclerView.setVisibility(View.GONE);
//                            }
                        }
//                    } else {
//                        if(!swipeRefreshLayout.isRefreshing()) {
//                            showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
////                                    finish();
//                                }
//                            });
//                        }
                    }

                    sendFollowingListRequest(IAPIConstants.API_KEY_GET_FOLLOWERS_LIST, usernameOwn, usernameOwn, "0", "0");
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    sendFollowingListRequest(IAPIConstants.API_KEY_GET_FOLLOWERS_LIST, usernameOwn, usernameOwn, "0", "0");
//                    if (error != null && error.networkResponse != null) {
//                        Log.e("VolleyError ", "" + error.networkResponse.statusCode);
//                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND) {
////                            Toast.makeText(context, "Please try again",
////                                    Toast.LENGTH_SHORT).show();
//                        }
//                    } else {
////                        Toast.makeText(context, "Please try again",
////                                Toast.LENGTH_SHORT).show();
//                    }
//                    hideProgressBar();
//                    showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//
////                            finish();
//                        }
//                    });
                }
            })
            {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + cacheHitButRefreshed;
                        final long ttl = now + cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFollowingListRequest(int urlKey, final String usernameOwn, final String usernameOther, String page, String size) {
        try {

            String url = ServiceURLManager.getInstance().getListUrl(urlKey, usernameOwn, usernameOther, page, size);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    List<Profile> obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Profile>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {

                            mMessageTextView.setVisibility(View.GONE);
                            if (searchList != null && searchList.size() > 0) {
                                searchList.addAll((ArrayList) obj);
                            } else {
                                searchList = (ArrayList) obj;
                            }
                            HashSet<Profile> uniqueValues = new HashSet<>(searchList);
                            searchList = new ArrayList<>();
                            searchList.addAll(uniqueValues);
                            initialiseRecyclerView();
                            if (mRecyclerView != null)
                                mRecyclerView.setVisibility(View.VISIBLE);

                            if (mAdapter != null)
                                mAdapter.notifyDataSetChanged();

                        } else {
                            if (searchList != null && searchList.size() == 0) {
                                mMessageTextView.setVisibility(View.VISIBLE);
                                if (mRecyclerView != null)
                                    mRecyclerView.setVisibility(View.GONE);
                            }else{
                                initialiseRecyclerView();
                                if (mRecyclerView != null)
                                    mRecyclerView.setVisibility(View.VISIBLE);

                                if (mAdapter != null)
                                    mAdapter.notifyDataSetChanged();
                            }
                        }
                    } else {
                        if (searchList != null) {
                            if (searchList.size() == 0) {

//                            if (!swipeRefreshLayout.isRefreshing()) {
                                showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                    finish();
                                    }
                                });
//                            }
                            } else {
                                initialiseRecyclerView();
                                if (mRecyclerView != null)
                                    mRecyclerView.setVisibility(View.VISIBLE);

                                if (mAdapter != null)
                                    mAdapter.notifyDataSetChanged();
                            }
                        }
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    if (searchList != null && searchList.size() == 0) {

                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

//                            finish();
                            }
                        });
                    }else{
                        initialiseRecyclerView();
                        if (mRecyclerView != null)
                            mRecyclerView.setVisibility(View.VISIBLE);

                        if (mAdapter != null)
                            mAdapter.notifyDataSetChanged();
                    }
                }
            })
            {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + cacheHitButRefreshed;
                        final long ttl = now + cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendListRequest(int urlKey, final String usernameOwn, final String usernameOther, String page, String size) {
        try {

            String url = ServiceURLManager.getInstance().getListUrl(urlKey, usernameOwn, usernameOther, page, size);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    List<Profile> obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Profile>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {

                            mMessageTextView.setVisibility(View.GONE);
//                            if (searchList != null && searchList.size() > 0) {
//                                searchList.addAll((ArrayList) obj);
//                            } else {
                                searchList = (ArrayList) obj;
//                            }
//                            HashSet<Profile> uniqueValues = new HashSet<>(searchList);
//                            searchList = new ArrayList<>();
//                            searchList.addAll(uniqueValues);
                            initialiseRecyclerView();
                            if (mRecyclerView != null)
                                mRecyclerView.setVisibility(View.VISIBLE);

                            if (mAdapter != null)
                                mAdapter.notifyDataSetChanged();

                        } else {
                            if (searchList != null && searchList.size() == 0) {
                                mMessageTextView.setVisibility(View.VISIBLE);
                                if (mRecyclerView != null)
                                    mRecyclerView.setVisibility(View.GONE);
                            }else{
                                initialiseRecyclerView();
                                if (mRecyclerView != null)
                                    mRecyclerView.setVisibility(View.VISIBLE);

                                if (mAdapter != null)
                                    mAdapter.notifyDataSetChanged();
                            }
                        }
                    } else {
                        if (searchList != null) {
                            if (searchList.size() == 0) {

//                            if (!swipeRefreshLayout.isRefreshing()) {
                                showError(getString(R.string.group_add_friends), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                    }
                                });
//                            }
                            } else {
                                initialiseRecyclerView();
                                if (mRecyclerView != null)
                                    mRecyclerView.setVisibility(View.VISIBLE);

                                if (mAdapter != null)
                                    mAdapter.notifyDataSetChanged();
                            }
                        }else{
                            showError(getString(R.string.group_add_friends), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                        }
                    }

//                    sendFollowersListRequest(IAPIConstants.API_KEY_GET_FOLLOW_LIST, usernameOwn, usernameOwn, "0", "0");
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

//                    sendFollowersListRequest(IAPIConstants.API_KEY_GET_FOLLOW_LIST, usernameOwn, usernameOwn, "0", "0");
//                     hideProgressBar();
                    if (searchList != null && searchList.size() == 0) {
                        showError(getString(R.string.group_add_friends), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                    }else{
                        showError(getString(R.string.group_add_friends), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                    }
                }
            })
            {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + cacheHitButRefreshed;
                        final long ttl = now + cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showMenuIcon(boolean show) {
        if (createNewGroup) {
            if (menuNext != null && next != null) {
                if (show) {
                    if (next != null) {
                        next.setVisibility(View.VISIBLE);
                        menuNext.setVisible(true);
                        next.setEnabled(true);
                        next.setText(MyApplication.getAppContext().getResources().getString(R.string.action_next));
                        next.setTextColor(MyApplication.getAppContext().getResources().getColor(R.color.white));
                    }
                } else {
                    if (next != null) {
                        next.setVisibility(View.VISIBLE);
                        menuNext.setVisible(true);
                        next.setEnabled(false);
                        next.setText(MyApplication.getAppContext().getResources().getString(R.string.action_next));
                        next.setTextColor(MyApplication.getAppContext().getResources().getColor(R.color.white_disabled_menu));
                    }
                }
            }
        } else {
            if (show) {
                if (next != null) {
                    next.setVisibility(View.VISIBLE);
                    menuNext.setVisible(true);
                    next.setEnabled(true);
                    next.setText(MyApplication.getAppContext().getResources().getString(R.string.action_done));
                    next.setTextColor(MyApplication.getAppContext().getResources().getColor(R.color.white));
                }
            } else {
                if (next != null) {
                    next.setVisibility(View.VISIBLE);
                    menuNext.setVisible(true);
                    next.setEnabled(false);
                    next.setText(MyApplication.getAppContext().getResources().getString(R.string.action_done));
                    next.setTextColor(MyApplication.getAppContext().getResources().getColor(R.color.white_disabled_menu));
                }
            }

        }

    }

    private void sendAddMemberListRequest(ArrayList<String> members) {
        try {
            disableRequest=true;
            final String userUUID = DataStorage.getInstance().getUserUUID();
            final String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
            String url = ServiceURLManager.getInstance().getGroupMemberAddUrl(groupUuid, userUUID);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(members);
            } catch (IOException e) {
            }catch (OutOfMemoryError e) {
            }catch (Exception e) {
            }
//            Log.i("Register request", jsonObject);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    disableRequest=false;
                    showProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    GroupProfile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), GroupProfile.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        ModelManager.getInstance().setGroupProfile(obj);

                        String userUUID = DataStorage.getInstance().getUserUUID();
                        final String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
                        String url = ServiceURLManager.getInstance().getGroupMemberListUrl(groupUuid, userUUID, "0", "0");
                        VolleySingleton.getInstance(GroupAddMemberActivity.this).refresh(url,true);
                        String username = obj.getCreatorUuid();
                        if (username != null && !username.isEmpty()) {
                            String url1 = ServiceURLManager.getInstance().getGroupMemberListUrl(groupUuid,userUUID,"0","0");
                            VolleySingleton.getInstance(GroupAddMemberActivity.this).refresh(url1,true);
                            CropGroupImageActivity.groupImage = "";
                            finish();
                            Utils.getInstance().displayToast(GroupAddMemberActivity.this,getString(R.string.error_request_member_add_successful),Utils.SHORT_TOAST);
//
                        } else {
                            Utils.getInstance().displayToast(GroupAddMemberActivity.this,getString(R.string.error_request_member_add_failed),Utils.SHORT_TOAST);
//
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    disableRequest=false;
                    hideProgressBar();
                    Utils.getInstance().displayToast(GroupAddMemberActivity.this,getString(R.string.error_request_member_add_failed),Utils.SHORT_TOAST);
//                    if (searchList != null && searchList.size() == 0) {
//
//                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//
////                            finish();
//                            }
//                        });
//                    }
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {disableRequest=false;
        }
    }

    private void openProfile() {
        Intent mIntent = new Intent(this, TimelineProfile.class);
        startActivity(mIntent);
        finish();
    }


    private void showError(final String message,final DialogInterface.OnClickListener okClicked) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(GroupAddMemberActivity.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(message)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .show();
                }
            });
        }catch (WindowManager.BadTokenException e){

        }catch (Exception e){

        }
    }
    private void showProgressBar() {
        try{
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressBar != null) {
                mProgressBar.setVisibility(View.VISIBLE);
            }
//                swipeRefreshLayout.setRefreshing(true);
            }
        });
        }catch (WindowManager.BadTokenException e){

        }catch (Exception e){

        }
    }

    private void hideProgressBar() {
        try{
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressBar != null) {
                    mProgressBar.setVisibility(View.GONE);
                }
//                swipeRefreshLayout.setRefreshing(false);
               /* if (progressDialog != null) {
                    progressDialog.dismiss();
                }*/
            }
        });
        }catch (WindowManager.BadTokenException e){

        }catch (Exception e){

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

//    @Override
//    public void onRefresh() {
//        initialiseData();
//    }
}
