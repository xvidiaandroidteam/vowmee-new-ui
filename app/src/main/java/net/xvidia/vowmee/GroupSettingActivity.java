package net.xvidia.vowmee;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.listadapter.GroupAddMemberListSettingAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.GroupProfile;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class GroupSettingActivity extends AppCompatActivity  {
    private CheckBox publicCheckBox;
    private CheckBox privateCheckBox;
    private CheckBox searchableCheckBox;
    private TextView publicMessage;
    private ProgressDialog progressDialog;
    private static GroupProfile groupProfile;
    private boolean privateFlag;
    private boolean searchableFlag;
    private RelativeLayout searchableLayout;
    private RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    private Toolbar toolbar;
    private static ArrayList<Profile> memberList;

    public static ArrayList<Profile> getMemberList() {
        return memberList;
    }

    public static void setMemberList(ArrayList<Profile> memberList) {
        GroupSettingActivity.memberList = memberList;
    }

    public static GroupProfile getGroupProfile() {
        return groupProfile;
    }

    public static void setGroupProfile(GroupProfile groupProfile) {
        GroupSettingActivity.groupProfile = groupProfile;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_setting);

        searchableCheckBox = (CheckBox)findViewById(R.id.searchable_checkBox);
        publicCheckBox = (CheckBox)findViewById(R.id.public_checkBox);
        privateCheckBox = (CheckBox)findViewById(R.id.private_checkBox);
        publicMessage = (TextView)findViewById(R.id.checkBox_text_message);
        searchableLayout = (RelativeLayout)findViewById(R.id.searchable_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mRecyclerView.setHasFixedSize(true);
        searchableCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                searchableFlag = isChecked;
                searchableCheckBox.setChecked(isChecked);
            }
        });
        publicCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                   publicCheckBox.setChecked(isChecked);
                        privateCheckBox.setChecked(!isChecked);
                privateFlag = !isChecked;
                if(isChecked){
                    publicMessage.setText(getString(R.string.prompt_group_public_message));
                    searchableLayout.setVisibility(View.GONE);
                }else{
                    publicMessage.setText(getString(R.string.prompt_group_private_message));
                    searchableLayout.setVisibility(View.VISIBLE);
                }

            }
        });

        privateCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    publicMessage.setText(getString(R.string.prompt_group_private_message));
                    searchableLayout.setVisibility(View.VISIBLE);
                } else {
                    publicMessage.setText(getString(R.string.prompt_group_public_message));
                    searchableLayout.setVisibility(View.GONE);
                }

                privateFlag = isChecked;
                publicCheckBox.setChecked(!isChecked);
                privateCheckBox.setChecked(isChecked);
            }
        });
        publicCheckBox.setChecked(true);
        publicMessage.setText(getString(R.string.prompt_group_public_message));
        searchableLayout.setVisibility(View.GONE);
        initialiseRecyclerView();
    }
    private void initialiseRecyclerView() {

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new GroupAddMemberListSettingAdapter(GroupSettingActivity.this, memberList);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(false);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        bindActivity();
    }

    private void bindActivity() {
        toolbar.setPadding(0, 0, 0, 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        getSupportActionBar().setTitle(getString(R.string.action_create_group));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_group_create, menu);
        if (menu != null) {
            MenuItem menuNext = menu.findItem(R.id.action_create_group_create);
            MenuItemCompat.setActionView(menuNext, R.layout.group_next);
            View viewNext = (View) MenuItemCompat.getActionView(menuNext);

//            View viewNext =menuNext.getActionView();
            Button notifCount = (Button) viewNext.findViewById(R.id.group_next);
            notifCount.setText(getString(R.string.action_create));
            notifCount.setVisibility(View.GONE);
            notifCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().uploadProfileImage(false, DataStorage.getInstance().getGroupImagePath(),CropGroupImageActivity.groupImage);

                    sendCreateGroupRequest();
                }
            });

            notifCount.setTextColor(getResources().getColor(R.color.white));
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       /* switch (item.getItemId()) {
            case R.id.action_create_group_create:
                sendCreateGroupRequest();
                break;

            default:
                break;
        }
*/
        return super.onOptionsItemSelected(item);
    }

    private void sendCreateGroupRequest() {
        try {
            GroupProfile groupProfile = getGroupProfile();
            String profile = AppConsatants.PROFILE_PUBLIC;
            if(privateFlag && searchableFlag){
                profile= AppConsatants.PROFILE_PRIVATE_SEARCHABLE;
            }else if(privateFlag){
                profile= AppConsatants.PROFILE_PRIVATE;
            }
            if(groupProfile == null) {
                return;
            }
            groupProfile.setProfile(profile);
            showProgressBar("Creating Group");
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_GROUP_CREATE);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(groupProfile);
            } catch (IOException e) {
                e.printStackTrace();
            }
//            Log.i("Register request", jsonObject);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    GroupProfile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), GroupProfile.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        String username = obj.getCreatorUuid();
                        if(username != null && !username.isEmpty()) {
//                            CropGroupImageActivity.groupImage = "";
                            String userUuid = DataStorage.getInstance().getUserUUID();
                            String urlList = ServiceURLManager.getInstance().getGroupListUrl(IAPIConstants.API_KEY_GROUP_LIST, userUuid);

                            VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(urlList, true);

                            Intent intent = new Intent(GroupSettingActivity.this, GroupListActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }else{
                            showError(getString(R.string.error_general));
                        }
                    }
                    hideProgressBar();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
//                    if (error != null&& error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND) {
////                        Log.e("VolleyError ", "" + error.networkResponse.statusCode);
//                        showError(error.getMessage());
//                    } else {
                        showError(getString(R.string.error_general));
//                    }
                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }
    private void showError(final String mesg) {
        try{
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                new AlertDialog.Builder(GroupSettingActivity.this,R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(mesg)
                        .setPositiveButton(android.R.string.ok, null)
                        .show();
            }
        });
        }catch(WindowManager.BadTokenException e){}
        catch(Exception e){}
    }

    private void showProgressBar(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = ProgressDialog.show(GroupSettingActivity.this, null, null, true, false);
                progressDialog.setContentView(R.layout.progressbar);
                TextView progressBarMessage = (TextView)progressDialog.findViewById(R.id.progressBar_message);
                progressBarMessage.setText(msg);
            }
        });

    }

    private void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

}
