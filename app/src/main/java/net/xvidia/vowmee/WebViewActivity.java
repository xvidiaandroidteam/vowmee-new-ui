package net.xvidia.vowmee;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

public class WebViewActivity extends AppCompatActivity {
    private WebView mWebView;
    private ProgressBar progressbar;
    public static String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view);
        mWebView = (WebView) findViewById(R.id.activity_main_webview);
        progressbar = (ProgressBar) findViewById(R.id.progressBar);
//        WebSettings webSettings = mWebView.getSettings();
//        webSettings.setJavaScriptEnabled(true);
        mWebView.loadUrl(url);
        WebChromeClient webClient = new WebChromeClient();
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
               progressbar.setVisibility(View.VISIBLE);

                if(progress == 100)
                    progressbar.setVisibility(View.GONE);;
            }
        });
//        mWebView.set
    }
}
