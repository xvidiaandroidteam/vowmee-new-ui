package net.xvidia.vowmee;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.mobile.AWSConfiguration;
import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.amazonaws.mobile.user.IdentityManager;
import com.amazonaws.mobile.user.IdentityProvider;
import com.amazonaws.mobile.user.signin.CognitoSyncClientManager;
import com.amazonaws.mobile.user.signin.SignInManager;
import com.amazonaws.mobile.user.signin.VowmeeSignInProvider;
import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.appevents.AppEventsLogger;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.contactsync.SyncUtils;
import net.xvidia.vowmee.helper.BlurBitmap;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.network.model.Register;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;

/**
 * A login screen that offers login via email/password.
 */
public class ProfileActivity extends AppCompatActivity {

    //    private final int REQUEST_CAMERA = 101;
//    private final int GALLERY_INTENT_CALLED = 102;
//    private final int GALLERY_KITKAT_INTENT_CALLED = 103;
//    private final int REQUIRED_SIZE = 150;
    private final int THUMBNAIL_REQUIRED_SIZE = 75;
    //    private EditText mNameEditText;
    private EditText mDisplayNameEditText;
    //    private EditText mStatusEditText;
    private EditText mPhoneNumberEditText;
    //    private EditText mCountryCodeEditText;
    private AutoCompleteTextView mEmailView;
    //    CollapsingToolbarLayout collapsingToolbarLayout;
    private CircularImageView profileImage;
    private ImageView profileImageEdit;
    private ImageView profileImageBg;
    private Button saveButton;
    //    private ImageButton editButton;
    private String facebookId;
    private String mPhoneNumber;
    private long twitterId;
    private boolean registerProfile;
    private ProgressDialog progressDialog;
    private Bitmap userProfileImage;
    private Bitmap thumbnailProfile;
    private String selectedImagePath;
    private static ArrayList<String> emails;
    private UserFileManager userFileManager;
    private SignInManager signInManager;
    private JsonObjectRequest request;

    private class SignInResultsHandler implements IdentityManager.SignInResultsHandler {
        @Override
        public void onSuccess(final IdentityProvider provider) {
            // Load user name and image.
//            showProgressBar();
            AWSMobileClient.defaultMobileClient()
                    .getIdentityManager().loadUserInfoAndImage(provider, new Runnable() {
                @Override
                public void run() {
                    String bucket = AWSConfiguration.AMAZON_S3_USER_FILES_BUCKET;
                    String prefix = "public/";
                    AWSMobileClient.defaultMobileClient()
                            .createUserFileManager(bucket,
                                    prefix,
                                    new UserFileManager.BuilderResultHandler() {

                                        @Override
                                        public void onComplete(final UserFileManager userFileManager) {
                                            Utils.getInstance().setUserFileManager(userFileManager);
                                            getProfileRequest();
//                                            if(provider.getDisplayName().equalsIgnoreCase("Facebook")) {
//                                                sendFacebookRegisterRequest(provider.getFacebookUserId(), provider.getToken(), provider.getFacbookTokenExpiryDate());
//                                            }else if(provider.getDisplayName().equalsIgnoreCase("Twitter")){
//                                                sendTwitterRegisterRequest(provider.getTwitterUserId(), provider.getToken(), provider.getSecreteToken());
//
//                                            }
                                        }
                                    });
                }
            });


        }

        @Override
        public void onCancel(final IdentityProvider provider) {
            hideProgressBar();
            Utils.getInstance().displayToast(MyApplication.getAppContext(),String.format("Sign-in with %s canceled.",
                    provider.getDisplayName()), Utils.LONG_TOAST);

//            Toast.makeText(ProfileActivity.this, String.format("Sign-in with %s canceled.",
//                    provider.getDisplayName()), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(final IdentityProvider provider, final Exception ex) {
            hideProgressBar();
//            final AlertDialog.Builder errorDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
//            errorDialogBuilder.setTitle("Sign-In Error");
//            errorDialogBuilder.setMessage(
//                    String.format("Sign-in with %s failed.\n%s", provider.getDisplayName(), ex.getMessage()));
//            errorDialogBuilder.setNeutralButton("Ok", null);
//            errorDialogBuilder.show();
            Utils.getInstance().displayToast(MyApplication.getAppContext(),String.format("Sign-in with %s failed.",
                    provider.getDisplayName()), Utils.LONG_TOAST);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setTitleTextColor(Color.WHITE);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        signInManager = new SignInManager(this);
        signInManager.setResultsHandler(this, new SignInResultsHandler());
        mDisplayNameEditText = (EditText) findViewById(R.id.register_profile_name);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.register_email);
        mPhoneNumberEditText = (EditText) findViewById(R.id.register_phone_number);
        profileImage = (CircularImageView) findViewById(R.id.home_profile_image);
        profileImageEdit = (ImageView) findViewById(R.id.home_profile_image_edit);
        profileImageBg = (ImageView) findViewById(R.id.home_profile_image_background);
        saveButton = (Button) findViewById(R.id.save_button);
        userFileManager = Utils.getInstance().getUserFileManager();
        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            facebookId = intent.getStringExtra(AppConsatants.PROFILE_FACEBOOK);
            twitterId = intent.getLongExtra(AppConsatants.PROFILE_TWITTER, 0);
            mPhoneNumber = intent.getStringExtra(AppConsatants.PROFILE_PHONE);
            registerProfile = intent.getBooleanExtra(AppConsatants.PROFILE_REGISTER, false);
        }
        profileImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(ProfileActivity.this, CropImageActivityNew.class);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, registerProfile);
                mIntent.putExtras(mBundle);
                startActivity(mIntent);
//                selectImage();
            }
        });
        profileImageEdit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(ProfileActivity.this, CropImageActivityNew.class);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, registerProfile);
                mIntent.putExtras(mBundle);
                startActivity(mIntent);
//                selectImage();
            }
        });
      mDisplayNameEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
          @Override
          public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
              if (actionId == EditorInfo.IME_ACTION_NEXT) {
                  if (TextUtils.isEmpty(mDisplayNameEditText.getText().toString())) {
                      Utils.getInstance().displayToast(ProfileActivity.this, getString(R.string.error_invalid_name), Utils.LONG_TOAST);

                      return false;
                  } else {
                      return true;
                  }
              }
              return false;
          }
      });

        mPhoneNumberEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    String phonenumber = mPhoneNumberEditText.getText().toString();
                    if (phonenumber != null && (!TextUtils.isEmpty(phonenumber)&& phonenumber.trim().length() < 10)) {
                        return true;
                    } else {
                        Utils.getInstance().displayToast(ProfileActivity.this,getString(R.string.error_invalid_phone),Utils.LONG_TOAST);
                        return false;
                    }
                }
                return false;
            }
        });
        mEmailView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String emailId = mEmailView.getText().toString();
                    if (emailId != null && (!TextUtils.isEmpty(emailId) && android.util.Patterns.EMAIL_ADDRESS.matcher(emailId).matches())) {
                        return true;
                    }else {
                        Utils.getInstance().displayToast(ProfileActivity.this, getString(R.string.error_invalid_email), Utils.LONG_TOAST);
                        return false;
                    }
                }
                return false;
            }
        });
        saveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                saveProfile();
            }
        });
        if (registerProfile) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//            getSupportActionBar().setTitle(R.string.title_register);
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.title_Profile);
        }
        updateUserImage(this);
        showProfileImage(true);
        getEmailId();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, emails);
        adapter.setNotifyOnChange(true);
        mEmailView.setAdapter(adapter);
    }

    private void showProfileImage(boolean show) {
        if(profileImage == null)
            return;
        if(profileImageEdit == null)
            return;
        if (show) {
            profileImage.setVisibility(View.VISIBLE);
            profileImageEdit.setVisibility(View.VISIBLE);
        } else {
            profileImage.setVisibility(View.GONE);
            profileImageEdit.setVisibility(View.GONE);
        }

    }
//    private void setPalette() {
//        Bitmap bitmap = ((BitmapDrawable) profileImage.getDrawable()).getBitmap();
//        if(bitmap != null)
//        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
//            @Override
//            public void onGenerated(Palette palette) {
//                int primaryDark = getResources().getColor(R.color.primary_dark);
//                int primary = getResources().getColor(R.color.primary);
//                collapsingToolbarLayout.setContentScrimColor(getResources().getColor(R.color.primary_light));
//                collapsingToolbarLayout.setStatusBarScrimColor(getResources().getColor(R.color.primary_light));
//            }
//        });

    //    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        SyncUtils.CreateSyncAccount(this);
    }

    private void downloadContentFromS3Bucket(String filePath) {
        String filePathImage = Utils.getInstance().getUserFileManager().getLocalContentPath() + File.separator + filePath;
        profileImage.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePathImage, true));
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 256);
//            userFileManager.setContentCacheSize(1024 * 1024 * 15);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePath = Utils.getInstance().getLocalContentPath(contentItem.getFilePath());
                profileImage.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePath, true));
                Bitmap originalThumbnail = Utils.getInstance().decodeScaledBitmapFromSdCard(filePath);
                Bitmap blurredBitmap = BlurBitmap.blur(ProfileActivity.this, originalThumbnail);
                profileImageBg.setImageBitmap(blurredBitmap);
            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
                ////Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
                ////Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_profile_save, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.action_save:
//                saveProfile();
//                break;
//
//            default:
//                break;
//        }
//
//        return true;
//    }

    private void getProfileRequest() {
        try {
//	showProgressBar(getString(R.string.progressbar_fetching_info));
            final String username = DataStorage.getInstance().getUsername();//"F_815817965196060";//
            String url = ServiceURLManager.getInstance().getProfileUrl(IAPIConstants.API_KEY_GET_PROFILE, username, username);

            request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    String getUsername = obj.getUsername();
                    Map<String, String> headers = request.getResponseHeaders();
                    hideProgressBar();
                    if (getUsername != null && !getUsername.isEmpty() && username.equals(getUsername)) {

                        ModelManager.getInstance().setOwnProfile(obj);
//						////Log.i("X-AUTH-TOKEN ", "" + headers.get("X-AUTH-TOKEN"));
                        if (Utils.getInstance().getIntegerValueOfString(obj.getPosts()) > 0) {
                            startActivity(new Intent(ProfileActivity.this, TimelineTabs.class));
                        } else {
                            startActivity(new Intent(ProfileActivity.this, TimelineTabs.class));
                        }
                        finish();
                    } else {
//						showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//							@Override
//							public void onClick(DialogInterface dialog, int which) {
//								finish();
//							}
//						});
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    showError(getString(R.string.error_general));
                }
            }) {
                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        final long cacheHitButRefreshed = 1 * 30 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                        final long cacheExpired = 10 * 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + cacheHitButRefreshed;
                        final long ttl = now + cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONObject(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONObject response) {
                    super.deliverResponse(response);
                    ////Log.e("VolleyError ", "" + response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    /**
     * Attempts to sign in or register the account specified by the //Login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual //Login attempt is made.
     */
    private void saveProfile() {

        // Reset errors.
//        mNameEditText.setError(null);
        mDisplayNameEditText.setError(null);
        mPhoneNumberEditText.setError(null);
//        mCountryCodeEditText.setError(null);
        // Store values at the time of the //Login attempt.
//        String name = mNameEditText.getText().toString();
        String displayName = mDisplayNameEditText.getText().toString();
        String phonenumber = mPhoneNumberEditText.getText().toString();
//        String countryCode = mCountryCodeEditText.getText().toString();
        String emailId = mEmailView.getText().toString();
//        String statusMessage = mStatusEditText.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(displayName)) {
//            mDisplayNameEditText.setError(getString(R.string.error_invalid_name));

            Utils.getInstance().displayToast(this,getString(R.string.error_invalid_name),Utils.LONG_TOAST);
            focusView = mDisplayNameEditText;
            cancel = true;
        }else if (phonenumber != null && (TextUtils.isEmpty(phonenumber) || phonenumber.trim().length() < 10)) {
            focusView = mPhoneNumberEditText;
            cancel = true;
            phonenumber = mPhoneNumber;
            Utils.getInstance().displayToast(this,getString(R.string.error_invalid_phone),Utils.LONG_TOAST);

        }
        if (emailId != null && (!TextUtils.isEmpty(emailId) && android.util.Patterns.EMAIL_ADDRESS.matcher(emailId).matches())) {
        }else{
            focusView = mEmailView;
            cancel = true;
            Utils.getInstance().displayToast(this,getString(R.string.error_invalid_email),Utils.LONG_TOAST);
//            mPhoneNumberEditText.setError(getString(R.string.error_invalid_email))
        }
        if (cancel) {
            focusView.requestFocus();
        } else {
            if (registerProfile) {
                CognitoSyncClientManager.init(this);
//                mPhoneNumber = phonenumber;
                phonenumber = Utils.getInstance().get10DigitPhoneNumber(phonenumber);
                Register register = new Register();
                register.setFullName(displayName);
                register.setEmail(emailId);
                register.setContactNo(phonenumber);
                register.setDisplayName(displayName);
                register.setStatusMsg("");
                if (userProfileImage != null) {
                    register.setProfileImage(DataStorage.getInstance().getUsername() + ".jpg");
                    register.setThumbNail(DataStorage.getInstance().getUsername() + "_thumbnail.jpg");
                }
                if (facebookId != null && !facebookId.isEmpty()) {
                    register.setFaceBookId(facebookId);
                    Utils.getInstance().setRegisterObj(register);
                    sendRegisterRequest(register);
                } else if (twitterId > 0) {
                    register.setTwitterId(twitterId);
                    Utils.getInstance().setRegisterObj(register);
                    sendRegisterRequest(register);
                } else if (!mPhoneNumber.isEmpty()) {
                    register.setUsername(mPhoneNumber);
                    register.setPassword(mPhoneNumber);
                    Utils.getInstance().setRegisterObj(register);
                    sendDigitRegisterRequest(register);
                }


//                Intent in = new Intent(ProfileActivity.this, VerifyMobile.class);
//
//                in.putExtra("app_id", "d88d5b78fbcf4c00a3780c6");
//                in.putExtra("access_token",
//                        "e2e78a7eff1b10198ba4706cae7efb3f12d17bfb");
//                in.putExtra("mobile", mPhoneNumber);
////                if (phonenumber.length() == 0) {
////                    mCountryCodeEditText.setError("Please enter mobile number");
////                } else {
//                    if (CheckNetworkConnection
//                            .isConnectionAvailable(getApplicationContext())) {
//                        startActivityForResult(in, VerifyMobile.REQUEST_CODE);
//                    } else {
//                        Toast.makeText(getApplicationContext(),
//                                "no internet connection", Toast.LENGTH_SHORT)
//                                .show();
//                    }
//                }
//                startActivity(new Intent(ProfileActivity.this, VerifyPhonenumberActivity.class));
//                Intent mIntent = new Intent(ProfileActivity.this, VerifyPhonenumberActivity.class);
//                Bundle mBundle = new Bundle();
//                mBundle.putString(AppConsatants.PROFILE_PHONE, facebookId);
//                mBundle.putLong(AppConsatants.PROFILE_TWITTER, twitterId);
//                mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, false);
//                mIntent.putExtras(mBundle);
//                startActivity(mIntent);
//                finish();
            } else {
                Profile register = new Profile();
                register.setFullName(displayName);
                register.setEmail(emailId);
                register.setPhone(phonenumber);
                register.setDisplayName(displayName);
                register.setStatusMsg("");
                register.setUsername(DataStorage.getInstance().getUsername());
                if (userProfileImage != null) {
                    register.setProfileImage(DataStorage.getInstance().getUsername() + ".jpg");
                    register.setThumbNail(DataStorage.getInstance().getUsername() + "_thumbnail.jpg");
                }

                sendUpdateProfileRequest(register);
            }
        }
    }


    private void sendDigitRegisterRequest(Register register) {
        try {
            if (register == null) {
                return;
            }
            showProgressBar(getString(R.string.registering_progress));
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_DIGIT_REGISTER);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(register);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //////Log.i("Register request", jsonObject);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        DataStorage.getInstance().setRegisteredFlag(true);
                        String username = obj.getUsername();
                        if (username != null && !username.isEmpty()) {
                            DataStorage.getInstance().setUsername(username);
                            DataStorage.getInstance().setUserUUID(obj.getUuid());
                            ModelManager.getInstance().setOwnProfile(obj);
                            DataStorage.getInstance().setAppSocialId(username);
                            DataStorage.getInstance().setLastLoginType(AppConsatants.PROFILE_PHONE);
                            DataStorage.getInstance().setCognitoDigitId(obj.getCognitoId());
                            DataStorage.getInstance().setCognitoDigitToken(obj.getCognitoToken());
                            String gcmToken = DataStorage.getInstance().getGcmRegId(ProfileActivity.this);
                            if (!gcmToken.isEmpty()) {
                                sendRegistrationToServer(MyApplication.getAppContext(), gcmToken, username);
                            }
                            signInManager.initializeDigitButton(VowmeeSignInProvider.class);
                        } else {
                            DataStorage.getInstance().setRegisteredFlag(false);
                            showError(getString(R.string.error_general));
                        }
                    }
                    hideProgressBar();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    DataStorage.getInstance().setRegisteredFlag(false);
                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND) {
                        ////Log.e("VolleyError ", "" + error.networkResponse.statusCode);
                        showError(error.getMessage());
                    } else {
                        showError(getString(R.string.error_general));
                    }
                    hideProgressBar();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put(AppConsatants.DIGIT_HEADER_PROVIDER, DataStorage.getInstance().getDigitHeaderProvider());
                    headers.put(AppConsatants.DIGIT_HEADER_AUTH, DataStorage.getInstance().getDigitHeaderAuth());
                    return headers;
                }
            };
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void sendRegisterRequest(Register register) {
        try {
            if (register == null) {
                return;
            }
            showProgressBar("Registering user");
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_SOCIAL_REGISTER);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(register);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ////Log.i("Register request", jsonObject);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        DataStorage.getInstance().setRegisteredFlag(true);
                        String username = obj.getUsername();
                        if (username != null && !username.isEmpty()) {
                            DataStorage.getInstance().setUsername(username);
                            DataStorage.getInstance().setUserUUID(obj.getUuid());
                            String gcmToken = DataStorage.getInstance().getGcmRegId(ProfileActivity.this);
                            if (!gcmToken.isEmpty()) {
                                sendRegistrationToServer(MyApplication.getAppContext(), gcmToken, username);
                            }
                            ModelManager.getInstance().setOwnProfile(obj);
                            Intent desire = new Intent();
                            desire.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            desire.setClass(ProfileActivity.this, TimelineTabs.class);
                            startActivity(desire);
                            finish();
                        } else {
                            DataStorage.getInstance().setRegisteredFlag(false);
                            showError(getString(R.string.error_general));
                        }
                    }
                    hideProgressBar();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    DataStorage.getInstance().setRegisteredFlag(false);
                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND) {
                        ////Log.e("VolleyError ", "" + error.networkResponse.statusCode);
                        showError(error.getMessage());
                    } else {
                        showError(getString(R.string.error_general));
                    }
                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void sendRegistrationToServer(final Context context, final String token, String username) {
        if (token == null || context == null)
            return;
        String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_UPDATE_GCM);
        Profile profile = new Profile();
        profile.setUsername(username);
        profile.setGcmId(token);
        ObjectMapper mapper = new ObjectMapper();
        String jsonObject = null;
        try {
            jsonObject = mapper.writeValueAsString(profile);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        Log.d("GCM UPDATE", "GCM ToServer: " + profile.toString());
        JsonObjectRequest ObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
//                Log.d("GCM UPDATE", "Response: " + response.toString());

                if (response.toString().isEmpty() || response.toString().equals(null)) {
//                    Toast.makeText(context, "Register error. Try again.", Toast.LENGTH_LONG);
                } else {

//                    Toast.makeText(context, "Successfully registered with server", Toast.LENGTH_LONG).show();

                    try {
                        DataStorage.getInstance().storeRegistrationIdOnServer(context, true);
                    } catch (Exception e) {
                        // JSON error
                        e.printStackTrace();
//                        Toast.makeText(context, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                //Log.e("GCM UPDATE", "Login Error: " + error != null ? error.getMessage() : "");
//                Toast.makeText(context,
//                        "Error! Check your internet connection.", Toast.LENGTH_LONG).show();

            }
        });

        // Adding request to request queue
        VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(ObjReq);
//        DataStorage.getInstance().storeRegistrationIdOnServer(this, true);
    }

    private String getMy10DigitPhoneNumber() {
        if (mPhoneNumber != null && !mPhoneNumber.isEmpty())
            return Utils.getInstance().get10DigitPhoneNumber(mPhoneNumber);
        String s = getMyPhoneNumber();
        return s != null && s.length() > 2 ? s : null;
    }

    private String getMyPhoneNumber() {
        TelephonyManager mTelephonyMgr;
        mTelephonyMgr = (TelephonyManager)
                getSystemService(Context.TELEPHONY_SERVICE);
        return mTelephonyMgr.getLine1Number();
    }

    private void updateUserImage(final AppCompatActivity activity) {
        Bitmap userImage = null;
        String name = null;
        String email = null;
        String displayName = null;
        String phoneNumber = null;
        String statusMessage;
        String username = null;
        if (registerProfile) {

            if (facebookId != null && !facebookId.isEmpty()) {
                username = "F_" + facebookId;
                mPhoneNumberEditText.setEnabled(true);
            } else if (twitterId > 0) {
                username = "T_" + twitterId;
                mPhoneNumberEditText.setEnabled(true);
            } else if (mPhoneNumber != null && !mPhoneNumber.isEmpty()) {
                username = mPhoneNumber;
                phoneNumber = username;
                mPhoneNumberEditText.setText(Utils.getInstance().get10DigitPhoneNumber(mPhoneNumber));
            }
            if (username == null)
                return;
            if (username.isEmpty())
                return;
            DataStorage.getInstance().setUsername(username);
            IdentityManager identityManager = null;
            IdentityProvider identityProvider = null;
            if (AWSMobileClient.defaultMobileClient() != null) {
                identityManager =
                        AWSMobileClient.defaultMobileClient().getIdentityManager();
                if (identityManager != null)
                    identityProvider = identityManager.getCurrentIdentityProvider();
            }
//            userProfileImage = Utils.getInstance().loadImageFromStorage(Utils.getInstance().getLocalImagePath());
            if (identityProvider == null) {
                // Not signed in
                if (Build.VERSION.SDK_INT < 22) {
                    profileImage.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(), R.drawable.user));
                } else {
                    profileImage.setImageDrawable(activity.getDrawable(R.drawable.user));
                }

//                return;
            }
            if (identityManager != null) {
                userImage = identityManager.getUserImage();
                name = identityManager.getUserName();
                email = identityManager.getUserEmailId();
            }
            displayName = mDisplayNameEditText.getText().toString();
            if (userProfileImage == null) {
                if (userImage != null) {
                    userProfileImage = userImage;
                    profileImage.setImageBitmap(userImage);
//                    Bitmap originalThumbnail = Utils.getInstance().decodeScaledBitmapFromSdCard(filePath);
                    Bitmap blurredBitmap = BlurBitmap.blur(ProfileActivity.this, userImage);
                    profileImageBg.setImageBitmap(blurredBitmap);
                    Utils.getInstance().storeImage(userImage, false, DataStorage.getInstance().getUsername());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            selectedImagePath = Utils.getInstance().getLocalImagePath(DataStorage.getInstance().getUsername());
                            thumbnailProfile = Utils.getInstance().getThumbnail(selectedImagePath, THUMBNAIL_REQUIRED_SIZE);
//                            uploadProfileImage(false);
                        }
                    }, 1000);
                } else {
                    if (identityManager != null && !identityManager.getUserImageUrl().isEmpty()) {
                        Picasso.with(this).load(identityManager.getUserImageUrl())
                                .error(R.drawable.user)
                                .placeholder(R.drawable.user)
                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                .into(profileImage);
                       new LoadUserImage().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,identityManager.getUserImageUrl());
                    }
                }
            } else {
                profileImage.setImageBitmap(userProfileImage);
                Bitmap blurredBitmap = BlurBitmap.blur(ProfileActivity.this, userImage);
                profileImageBg.setImageBitmap(blurredBitmap);
            }
            if (email == null) {
                mEmailView.setText(getEmailId());
            } else if (email.isEmpty()) {
                mEmailView.setText(getEmailId());
            }
            if (displayName == null) {
                mDisplayNameEditText.setText(name);
            } else if (displayName.isEmpty()) {
                mDisplayNameEditText.setText(name);
            }
           /* mCountryCodeEditText.setText(VerifyMobile.getCountryCode(getApplicationContext()));
            mPhoneNumberEditText.setText(getMy10DigitPhoneNumber());*/
            if (ContextCompat.checkSelfPermission(ProfileActivity.this, android.Manifest.permission.READ_CONTACTS)
                    == PackageManager.PERMISSION_GRANTED) {
                try {
                    mPhoneNumberEditText.setText(getMy10DigitPhoneNumber());
                } catch (IllegalStateException e) {
                    Log.d("PHONE_NUMBER", "Error setting phone number edittext: " + e.getMessage());
                }catch(Exception e){}
            } else {
                mPhoneNumberEditText.setText(Utils.getInstance().get10DigitPhoneNumber(mPhoneNumber));
            }

//            mNameEditText.setText(name);
//            mStatusEditText.setText(getString(R.string.home_status_message));
        } else {
            Profile obj = ModelManager.getInstance().getOwnProfile();
            String profImagePath = obj.getProfileImage();
            profImagePath = (profImagePath == null ? (obj.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? obj.getUsername() + AppConsatants.FILE_EXTENSION_JPG : obj.getThumbNail()));

            downloadContentFromS3Bucket(profImagePath);
            name = obj.getFullName();
            email = obj.getEmail();
            displayName = obj.getDisplayName();
            phoneNumber = obj.getPhone();
            statusMessage = obj.getStatusMsg();
            if (userProfileImage != null) {
                profileImage.setImageBitmap(userProfileImage);
            }
            if (email == null) {
//                mEmailView.setText(getEmailId());
            } else if (email.isEmpty()) {
//                mEmailView.setText(getEmailId());
            } else {
                mEmailView.setText(email);
            }
            if (displayName == null) {
                mDisplayNameEditText.setText(name);
            } else if (displayName.isEmpty()) {
                mDisplayNameEditText.setText(name);
            } else {
                mDisplayNameEditText.setText(displayName);
            }
            if (phoneNumber != null && !phoneNumber.isEmpty()) {
                if (phoneNumber.indexOf("+91") > 0) {
                    phoneNumber = phoneNumber.replace("+91", "");
                }
            }
            mPhoneNumberEditText.setText(phoneNumber);
            saveButton.setText(getString(R.string.action_save));
//            mNameEditText.setText(name);
//            mStatusEditText.setText(statusMessage);
//            mCountryCodeEditText.setText(VerifyMobile.getCountryCode(getApplicationContext()));
        }

//        setPalette();
    }

    private class LoadUserImage extends AsyncTask<String, Void, Void> {


        @Override
        protected Void doInBackground(String... params) {
            try {
                // Do your long operations here and return the result
                String url = params[0];
                loadUserImage(url);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


    }

    private void loadUserImage(final String userImageUrl) {
        if (userImageUrl == null)
            return;
        if (userImageUrl.isEmpty())
            return;

        try {
            Bitmap userImageB = null;
            final InputStream is = new URL(userImageUrl).openStream();
            userImageB = BitmapFactory.decodeStream(is);
            is.close();
            if (userImageB != null) {
                userProfileImage = userImageB;
                profileImage.setImageBitmap(userImageB);
                Bitmap blurredBitmap = BlurBitmap.blur(ProfileActivity.this, userProfileImage);
                profileImageBg.setImageBitmap(blurredBitmap);
                Utils.getInstance().storeImage(userImageB, false, DataStorage.getInstance().getUsername());
                selectedImagePath = Utils.getInstance().getLocalImagePath(DataStorage.getInstance().getUsername());
                thumbnailProfile = Utils.getInstance().getThumbnail(selectedImagePath, THUMBNAIL_REQUIRED_SIZE);
//                uploadProfileImage(false);

            }

        } catch (IOException e) {
        }
    }

    private String getEmailId() {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        String possibleEmail = "";
        emails = new ArrayList<>();
        Account[] accounts = AccountManager.get(this).getAccounts();
        for (Account account : accounts) {
            emails.add(account.name);
            if (emailPattern.matcher(account.name).matches()) {
                possibleEmail = account.name;
            }
        }

        return possibleEmail;
    }

    private void updateProfileImage() {
        if (profileImage != null && !DataStorage.getInstance().getProfileImagePath().isEmpty()) {
            profileImage.setImageBitmap(Utils.getInstance().loadImageFromStorage(DataStorage.getInstance().getProfileImagePath(), false));
            Bitmap originalThumbnail = Utils.getInstance().decodeScaledBitmapFromSdCard(DataStorage.getInstance().getProfileImagePath());
            Bitmap blurredBitmap = BlurBitmap.blur(this, originalThumbnail);
            profileImageBg.setImageBitmap(blurredBitmap);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateProfileImage();
        final AWSMobileClient awsMobileClient = AWSMobileClient.defaultMobileClient();

        if (awsMobileClient == null) {
            // At this point, the app is resuming after being shut down and the onCreate
            // method has already fired an intent to launch the splash activity. The splash
            // activity will refresh the user's credentials and re-initialize all required
            // components. We bail out here, because without that initialization, the steps
            // that follow here would fail. Note that if you don't have any features enabled,
            // then there may not be any steps here to skip.
            return;
        }

        // pause/resume Mobile Analytics collection
        awsMobileClient.handleOnResume();
        AppEventsLogger.activateApp(this);
    }


    @Override
    protected void onPause() {
        super.onPause();

        // Obtain a reference to the mobile client.
        final AWSMobileClient awsMobileClient = AWSMobileClient.defaultMobileClient();

        if (awsMobileClient != null) {
            // pause/resume Mobile Analytics collection
            awsMobileClient.handleOnPause();
        }
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
        hideProgressBar();
    }

    private void sendUpdateProfileRequest(Profile register) {
        try {
            showProgressBar("Updating profile information");
            final String originalUsername = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_UPDATE_PROFILE);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(register);
            } catch (IOException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    hideProgressBar();
                    if (obj != null) {
                        DataStorage.getInstance().setRegisteredFlag(true);
                        String username = obj.getUsername();
                        if (username != null && !username.isEmpty() && username.equals(originalUsername)) {
                            //Log.i("Profile", obj.toString());
                            ModelManager.getInstance().setOwnProfile(obj);
                            DataStorage.getInstance().setRegisteredFlag(true);
                            DataStorage.getInstance().setUsername(username);
                            DataStorage.getInstance().setUserUUID(obj.getUuid());
                            Intent desire = new Intent();
                            desire.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            desire.setClass(ProfileActivity.this, TimelineTabs.class);
                            startActivity(desire);
                            finish();
                        } else {
                            showError(getString(R.string.error_general));
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND) {
                        //Log.e("VolleyError ", "" + error.networkResponse.statusCode);
                        showError(error.getMessage());
                    } else {
                        showError(getString(R.string.error_general));
                    }
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }


    private void uploadProfileImage(boolean showDialog) {
        UserFileManager userFileManager = Utils.getInstance().getUserFileManager();

        if (userFileManager != null) {

            String path = selectedImagePath;

            if (path == null)
                return;
            if (path.isEmpty())
                return;
            final File file = new File(path);
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle(R.string.app_name);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setCancelable(false);
            dialog.setIndeterminate(true);
            if (showDialog)
                dialog.show();
            String fileName = DataStorage.getInstance().getUsername() + AppConsatants.FILE_EXTENSION_JPG;
//                    downloadContentFromS3ucket("25.mp4");
//                    dialog.dismiss();
            userFileManager.uploadContent(file, fileName, new ContentProgressListener() {
                @Override
                public void onSuccess(final ContentItem contentItem) {
                    //Log.i("FIle uploaded", contentItem.getFilePath());
                    profileImage.setImageBitmap(userProfileImage);
                    Utils.getInstance().storeImage(userProfileImage, false, DataStorage.getInstance().getUsername());
                    Utils.getInstance().storeImage(thumbnailProfile, true, DataStorage.getInstance().getUsername());
                    uploadThumbnailProfileImage(false);
                    dialog.dismiss();
                }

                @Override
                public void onProgressUpdate(final String fileName, final boolean isWaiting,
                                             final long bytesCurrent, final long bytesTotal) {
                    dialog.setProgress((int) bytesCurrent);
                }

                @Override
                public void onError(final String fileName, final Exception ex) {
                    dialog.dismiss();
                    showError("update of image failed");
                    //Log.i("FIle uploaded error", ex.getMessage());
                }
            });
        }
    }

    private void uploadThumbnailProfileImage(boolean showDialog) {
        UserFileManager userFileManager = Utils.getInstance().getUserFileManager();

        if (userFileManager != null) {

            String path = Utils.getInstance().getLocalThumbnailImagePath(DataStorage.getInstance().getUsername());
            if (path != null && path.isEmpty())
                return;
            final File file = new File(path);
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle(R.string.app_name);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setCancelable(false);
            dialog.setIndeterminate(true);
            if (showDialog)
                dialog.show();
            String fileName = DataStorage.getInstance().getUsername() + "_thumbnail.jpg";
//                    downloadContentFromS3ucket("25.mp4");
//                    dialog.dismiss();
            userFileManager.uploadContent(file, fileName, new ContentProgressListener() {
                @Override
                public void onSuccess(final ContentItem contentItem) {
                    //Log.i("FIle uploaded", contentItem.getFilePath());
                    Utils.getInstance().storeImage(thumbnailProfile, true, DataStorage.getInstance().getUsername());
                    dialog.dismiss();
                }

                @Override
                public void onProgressUpdate(final String fileName, final boolean isWaiting,
                                             final long bytesCurrent, final long bytesTotal) {
                    dialog.setProgress((int) bytesCurrent);
                }

                @Override
                public void onError(final String fileName, final Exception ex) {
                    dialog.dismiss();
                    showError("update of image failed");
                    //Log.i("FIle uploaded error", ex.getMessage());
                }
            });
        }
    }

    private void showError(final String mesg) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new AlertDialog.Builder(ProfileActivity.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                }
            });
        }catch(WindowManager.BadTokenException e){

        }catch (Exception e){}
    }

    private void showProgressBar(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = ProgressDialog.show(ProfileActivity.this, null, null, true, false);
                progressDialog.setContentView(R.layout.progressbar);
                TextView progressBarMessage = (TextView) progressDialog.findViewById(R.id.progressBar_message);
                progressBarMessage.setText(msg);
            }
        });

    }

    private void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        if (registerProfile) {
            DataStorage.getInstance().setRegisteredFlag(false);
            Intent intent = new Intent(ProfileActivity.this, RegisterActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            finish();
        }

        super.onBackPressed();
    }
}
