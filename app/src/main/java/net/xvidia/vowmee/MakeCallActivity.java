package net.xvidia.vowmee;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opentok.android.AudioDeviceManager;
import com.opentok.android.BaseAudioDevice;
import com.opentok.android.BaseVideoRenderer;
import com.opentok.android.OpentokError;
import com.opentok.android.Publisher;
import com.opentok.android.PublisherKit;
import com.opentok.android.Session;
import com.opentok.android.Session.SessionListener;
import com.opentok.android.Stream;
import com.opentok.android.Subscriber;
import com.opentok.android.SubscriberKit;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.broadcastreceiver.NetworkStateReceiver;
import net.xvidia.vowmee.fragments.AudioLevelView;
import net.xvidia.vowmee.fragments.MeterView;
import net.xvidia.vowmee.fragments.SubscriberQualityFragment;
import net.xvidia.vowmee.helper.BlurBitmap;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.network.model.Opentok;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MakeCallActivity extends Activity implements NetworkStateReceiver.NetworkStateReceiverListener,
        PublisherKit.PublisherListener,Session.ReconnectionListener, SessionListener,
        SubscriberKit.SubscriberListener,SubscriberKit.StreamListener, SubscriberKit.VideoListener {

    private static final int REQUEST_CODE_ASK_PERMISSIONS = 1111;
    private static final String LOGTAG = "demo-voice-only";
    private Session mSession;
    private Publisher mPublisher;
    private ArrayList<Subscriber> mSubscribers = new ArrayList<Subscriber>();
    private HashMap<Stream, Subscriber> mSubscriberStream = new HashMap<Stream, Subscriber>();
    private Handler mHandler = new Handler();
    //    private LinearLayout callLayout;
    private RelativeLayout hangupLayout;
    private ProgressDialog pDialog;
    private TextView timerValue, callerName, callStatus;
    private Activity activity;
    //            callStatus;
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;

    private String mApiKey;
    private String mToken;
    Moment momentAdded;
    private boolean firstFlag;

    private String groupUuid;
    private String mSessionId;
    private Subscriber mSubscriber;
    private ArrayList<Stream> mStreams;
    private AudioLevelView mAudioLevelView;
    private boolean mSubscriberAudioOnly = false;
    private SubscriberQualityFragment.CongestionLevel congestion = SubscriberQualityFragment.CongestionLevel.Low;
    private FrameLayout mSubscriberViewContainer;
    private boolean notificationIntent;
    private RelativeLayout mSubscriberAudioOnlyView;

    private ImageView imageView;
    private CircularImageView circularImageView;
    private String thumbnailPath, userDisplayName;
    private SubscriberQualityFragment mSubscriberQualityFragment;
    private FragmentTransaction mFragmentTransaction;
    private  MeterView mv;
    private PhoneStateListener callStateListener;
    private TelephonyManager telephonyManager;
    private long startTime2 = 0L;
    private Handler customHandler2 = new Handler();
    long timeInMilliseconds2 = 0L;
    long timeSwapBuff2 = 0L;
    long updatedTime2 = 0L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        Log.i(LOGTAG, "ONCREATE");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_make_call);
        timerValue = (TextView) findViewById(R.id.timerText);
        timerValue.setVisibility(View.INVISIBLE);
        callStatus = (TextView) findViewById(R.id.callProgress);
        callStatus.setText(getString(R.string.connecting));
        hangupLayout = (RelativeLayout) findViewById(R.id.hangupLayout);
        imageView = (ImageView) findViewById(R.id.home_profile_image_background);
        circularImageView = (CircularImageView) findViewById(R.id.home_profile_image);
        callerName = (TextView) findViewById(R.id.callerName);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Calling...wait");
        pDialog.setCancelable(true);
        activity = this;
        showDialog();
        firstFlag = true;
        Intent intent = getIntent();
        if (null != intent) { //Null Checking
//            momentCaption = "CALL";
//            momentDescription = intent.getStringExtra(AppConsatants.LIVE_DESCRIPTION);
            groupUuid = intent.getStringExtra(AppConsatants.UUID);
            thumbnailPath = intent.getStringExtra(AppConsatants.USER_THUMBNAIL);
            userDisplayName = intent.getStringExtra(AppConsatants.PERSON_FRIEND);
//        privateFlag = true;
        }
        if (savedInstanceState == null) {
            mFragmentTransaction = getFragmentManager().beginTransaction();
            initSubscriberQualityFragment();
            mFragmentTransaction.commitAllowingStateLoss();
        }
        mSubscriberAudioOnlyView = (RelativeLayout) findViewById(R.id.audioOnlyView);
        mSubscriberViewContainer = (FrameLayout) findViewById(R.id.subscriber_container);
        mStreams = new ArrayList<Stream>();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSubscriber != null) {
                    if (mSubscriberAudioOnly) {
                        mSubscriber.getView().setVisibility(View.GONE);
                        setAudioOnlyView(true);
                        congestion = SubscriberQualityFragment.CongestionLevel.High;
                    }
                }
            }
        }, 0);

        mv = (MeterView) findViewById(R.id.volumeMic);
        mv.setIcons(BitmapFactory.decodeResource(getResources(),
                R.drawable.unmute_pub), BitmapFactory.decodeResource(
                getResources(), R.drawable.mute_pub));
        mv.setVisibility(View.GONE);
        downloadContentFromS3Bucket(thumbnailPath);
        callerName.setText(getString(R.string.calling_friend, userDisplayName));
        fetchPublishSessionConnectionData();

        startTime2 = SystemClock.uptimeMillis();
        customHandler2.postDelayed(updateTimerThread2, 0);
        DataStorage.getInstance().setCallProgress(true);
    }
    private Runnable updateTimerThread2 = new Runnable() {

        public void run() {

            timeInMilliseconds2 = SystemClock.uptimeMillis() - startTime2;
            updatedTime2 = timeSwapBuff2 + timeInMilliseconds2;

            if (updatedTime2 >= AppConsatants.timeoutMakeCallEnd) {
                if (mSession != null) {
                    mSession.disconnect();
                    finish();
                }else{
                    finish();
                }
                Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_failed_connect),Utils.LONG_TOAST);

            }
            customHandler2.postDelayed(this, 0);

        }
    };
    private void downloadContentFromS3Bucket(String filePath) {
        if(filePath==null)
            return;
        if(filePath.isEmpty())
            return;
        String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
        File file = new File(filePathImage);
        if (file.exists()) {
            Bitmap originalThumbnail = Utils.getInstance().decodeScaledBitmapFromSdCard(filePathImage);
            Bitmap blurredBitmap = BlurBitmap.blur(this, originalThumbnail );
            imageView.setImageBitmap(blurredBitmap);
            circularImageView.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePathImage, false));
//            imageView.setImageAlpha(80);
        }
    }
    private void setAudioOnlyView(boolean audioOnlyEnabled) {
        mSubscriberAudioOnly = audioOnlyEnabled;

        if (audioOnlyEnabled) {
            mSubscriber.getView().setVisibility(View.GONE);
            mSubscriberAudioOnlyView.setVisibility(View.VISIBLE);
//            mSubscriberAudioOnlyView.setOnClickListener(onViewClick);
//
//            // Audio only text for subscriber
//            TextView subStatusText = (TextView) findViewById(R.id.subscriberName);
//            subStatusText.setText(R.string.audioOnly);
//            AlphaAnimation aa = new AlphaAnimation(1.0f, 0.0f);
//            aa.setDuration(ANIMATION_DURATION);
//            subStatusText.startAnimation(aa);


            mSubscriber
                    .setAudioLevelListener(new SubscriberKit.AudioLevelListener() {
                        @Override
                        public void onAudioLevelUpdated(
                                SubscriberKit subscriber, float audioLevel) {
                            mAudioLevelView.setMeterValue(audioLevel);
                        }
                    });
        } else {
            mSubscriber.getView().setVisibility(View.VISIBLE);
            mSubscriberAudioOnlyView.setVisibility(View.GONE);

            mSubscriber.setAudioLevelListener(null);
            attachSubscriberView(mSubscriber);
        }
    }

    private void attachSubscriberView(Subscriber subscriber) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        mSubscriberViewContainer.removeView(mSubscriber.getView());
        mSubscriberViewContainer.addView(subscriber.getView(), layoutParams);
        subscriber.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                BaseVideoRenderer.STYLE_VIDEO_FIT);
    }

    public void fetchPublishSessionConnectionData() {
        try {
           if(groupUuid==null) {
               finish();
               return;
           }
            if(groupUuid.isEmpty()){
                finish();
            return;
        }
            ArrayList<String> memberUuidList = new ArrayList<>();
            memberUuidList.add(groupUuid);
            String url = ServiceURLManager.getInstance().getMakeCallUrl(DataStorage.getInstance().getUserUUID());
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(memberUuidList);
            } catch (IOException e) {
                e.printStackTrace();
            }
//            Log.i("Add moment", jsonObject);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideDialog();
                    ObjectMapper mapper = new ObjectMapper();
                    Opentok obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Opentok.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        mSessionId = obj.getSessionId();
                        mToken = obj.getToken();
                        mApiKey = obj.getApiKey();
                        if (mSessionId != null && !mSessionId.equalsIgnoreCase("null")) {
                            initializeSession();
                            initializePublisher();
                            firstFlag = false;
                        }
                    } else {
                        closeSession();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    closeSession();
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
            closeSession();
        }
    }

    private void closeSession() {
        hideDialog();
        if (mSession != null)
            mSession.disconnect();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                showMessage(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            }
        });
    }

    public void closePublishSessionConnection() {

        if (mSessionId == null) {
            finish();
            return;
        }
        if (mSessionId.isEmpty()) {
            finish();
            return;
        }
        String subUrl = ServiceURLManager.getInstance().getEndCallUrl(mSessionId, DataStorage.getInstance().getUserUUID());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, subUrl, "{}", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
//                    String url = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, DataStorage.getInstance().getUsername(), DataStorage.getInstance().getUsername(), AppConsatants.TRUE, "0", "0");
//                    VolleySingleton.getInstance(getAppContext()).refresh(url, false);
                    finish();

                    DataStorage.getInstance().setCallProgress(false);
                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                finish();
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void showMessage(final String mesg,final DialogInterface.OnClickListener okClicked) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(MakeCallActivity.this,R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(mesg)
                        .setPositiveButton(android.R.string.ok, okClicked)
                        .setCancelable(false)
                        .show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    public void onBackPressed() {
        restartAudioMode();
        if(telephonyManager!=null && callStateListener!=null)
            telephonyManager.listen(callStateListener, PhoneStateListener.LISTEN_NONE);

        if (mSession != null) {
//            if(mPublisher!=null)
//                mSession.unpublish(mPublisher);
//            if(mSubscriber!=null)
//                mSession.unsubscribe(mSubscriber);
            closePublishSessionConnection();
            hideDialog();
            mSession.disconnect();
        } else {
            finish();

            DataStorage.getInstance().setCallProgress(false);
        }
        //       restartAudioMode();

        super.onBackPressed();
    }
    public void initSubscriberQualityFragment() {
        mSubscriberQualityFragment = new SubscriberQualityFragment();
        getFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_sub_quality_container,
                        mSubscriberQualityFragment).commit();
    }

    public void restartAudioMode() {
        AudioManager Audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        Audio.setMode(AudioManager.MODE_NORMAL);
        this.setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
    }

    private void initializeSession() {
        mSession = new Session(this, mApiKey, mSessionId);
        mSession.setSessionListener(this);
        mSession.connect(mToken);
        AudioManager Audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        Audio.setMode(AudioManager.MODE_IN_CALL);
        Audio.setSpeakerphoneOn(false);
//        changeVoiceMode(true);
    }

    private void initializePublisher() {
        mPublisher = new Publisher(getApplicationContext(), "Xvidia's Demo");
        mPublisher.getRenderer().setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                BaseVideoRenderer.STYLE_VIDEO_FILL);
        mPublisher.setPublisherListener(this);
        mPublisher.setPublishVideo(false);
        mPublisher.setPublishAudio(true);
        hideDialog();

    }

    public void onEndCall(View v) {
        if (mSession != null)
            mSession.disconnect();
        finish();
    }

    @Override
    public void onConnected(Session session) {
        if (mPublisher==null)
            return;

        mSession.publish(mPublisher);
        hideDialog();
        AudioDeviceManager.getAudioDevice().setOutputMode(BaseAudioDevice.OutputMode.Handset);
//        changeVoiceMode(true);
//        Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_LONG);
//


        mPublisher.setAudioLevelListener(new PublisherKit.AudioLevelListener() {
            @Override
            public void onAudioLevelUpdated(PublisherKit publisher,
                                            float audioLevel) {
                mv.setMeterValue(audioLevel);
            }
        });

        mv.setOnClickListener(new MeterView.OnClickListener() {
            @Override
            public void onClick(MeterView view) {
                mPublisher.setPublishAudio(!view.isMuted());
            }
        });
    }

    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;
            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            secs = secs % 60;
            int milliseconds = (int) (updatedTime % 1000);
            timerValue.setText("" + String.format("%02d", mins) + ":"
                    + String.format("%02d", secs));
            /*timerValue.setText("" + mins + ":"
                    + String.format("%02d", secs) + ":"
                    + String.format("%03d", milliseconds));*/

            customHandler.postDelayed(this, 0);

        }
    };

    @Override
    public void onDisconnected(Session session) {
//        closePublishSessionConnection();
        if(mPublisher!=null)
            mPublisher.destroy();
        onBackPressed();
    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {
        if(customHandler2!=null && updateTimerThread2 != null)
            customHandler2.removeCallbacks(updateTimerThread2);
        mStreams.add(stream);
        if (mSubscriber == null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    changeVoiceMode(false);
                }
            },500);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    changeVoiceMode(true);
                }
            },1000);
            subscribeToStream(stream);
            hideDialog();

            mv.setVisibility(View.VISIBLE);
            final MeterView meterView = (MeterView) findViewById(R.id.volume);
            meterView.setMuted(true);
            meterView.setIcons(BitmapFactory.decodeResource(getResources(),
                    R.drawable.unmute_sub), BitmapFactory.decodeResource(
                    getResources(), R.drawable.mute_sub));

            meterView.setOnClickListener(new MeterView.OnClickListener() {
                @Override
                public void onClick(MeterView view) {
//                    mSubscriber.setSubscribeToAudio(!view.isMuted());
                    changeVoiceMode(view.isMuted());
                }
            });
            mSubscriber
                    .setAudioLevelListener(new SubscriberKit.AudioLevelListener() {
                        @Override
                        public void onAudioLevelUpdated(
                                SubscriberKit subscriber, float audioLevel) {
                            meterView.setMeterValue(audioLevel);
                        }
                    });
//            meterView.setMuted(true);
            callerName.setText(userDisplayName);
            callStatus.setText(getString(R.string.call_in_progress));
            timerValue.setVisibility(View.VISIBLE);
            startTime = SystemClock.uptimeMillis();
            customHandler.postDelayed(updateTimerThread, 0);

//            changeVoiceMode(true);
        }

        loadFragments();
    }
    private void initialiseIncomingCallListener() {
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        // register PhoneStateListener
        callStateListener = new PhoneStateListener() {
            public void onCallStateChanged(int state, String incomingNumber) {

                // If phone ringing
                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    onBackPressed();

                }
                // If incoming call received
                if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    onBackPressed();
                }


//                if(state== TelephonyManager.CALL_STATE_IDLE)
//                {
//                    Toast.makeText(getApplicationContext(),"phone is neither ringing nor in a call", Toast.LENGTH_LONG).show();
//                }
            }
        };
        if(telephonyManager!=null)
        telephonyManager.listen(callStateListener, PhoneStateListener.LISTEN_CALL_STATE);


    }
    @Override
    public void onResume() {
        super.onResume();
//        checkPermission();

    }

    private void changeVoiceMode(final boolean isMuted){
        new Handler().postAtTime(new Runnable() {
            @Override
            public void run() {
                if(isMuted){
                    boolean fl = AudioDeviceManager.getAudioDevice().setOutputMode(BaseAudioDevice.OutputMode.Handset);
//                    Toast.makeText(MakeCallActivity.this,"handset "+fl,Toast.LENGTH_LONG);
                }else{
                    AudioDeviceManager.getAudioDevice().setOutputMode(BaseAudioDevice.OutputMode.SpeakerPhone);
                }
            }
        },500);

    }
    public void loadFragments() {
        // show subscriber status
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSubscriber != null) {
//                    mSubscriberFragment.showSubscriberWidget(true);
//                    mSubscriberFragment.initSubscriberUI();

                    if (congestion != SubscriberQualityFragment.CongestionLevel.Low) {
                        mSubscriberQualityFragment.setCongestion(congestion);
                        mSubscriberQualityFragment.showSubscriberWidget(true);
                    }
                }
            }
        }, 0);

    }

    private void subscribeToStream(Stream stream) {
        mSubscriber = new Subscriber(this, stream);
        mSubscriber.setSubscriberListener(this);
        mSubscriber.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                BaseVideoRenderer.STYLE_VIDEO_FIT);
        mSession.subscribe(mSubscriber);
        if (mSubscriber.getSubscribeToVideo()) {
            showDialog();
        }
        mSubscriber.setSubscribeToVideo(false);
        mSubscriber.setSubscribeToAudio(true);

//        AudioDeviceManager.getAudioDevice().setOutputMode(BaseAudioDevice.OutputMode.Handset);
    }



    @Override
    public void onStreamDropped(Session session, Stream stream) {
        mStreams.remove(stream);
        if (mSubscriber != null
                && mSubscriber.getStream().getStreamId()
                .equals(stream.getStreamId())) {
            mSubscriberViewContainer.removeView(mSubscriber.getView());
            mSubscriber = null;
            findViewById(R.id.avatar).setVisibility(View.GONE);
            mSubscriberAudioOnly = false;
            if (!mStreams.isEmpty()) {
//                subscribeToStream(mStreams.get(0));
            }
            Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_message_call_ended),Utils.LONG_TOAST);

//            Toast.makeText(this, "Call has ended", Toast.LENGTH_LONG).show();
            onDisconnected(mSession);
        }
    }

    @Override
    public void onError(Session session, OpentokError error) {
//        Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
    }

    private void showDialog() {
//        if (!pDialog.isShowing())
//            pDialog.show();
    }

    private void hideDialog() {
//        if (pDialog.isShowing())
//            pDialog.dismiss();
    }

    @Override
    public void onNetworkAvailable() {
        if (!firstFlag) {
            initializeSession();
            initializePublisher();
        }
    }

    @Override
    public void onNetworkUnavailable() {
        showDialog();
    }

    // Publisher Listener Methods

    @Override
    public void onStreamCreated(PublisherKit publisherKit, Stream stream) {

    }

    @Override
    public void onStreamDestroyed(PublisherKit publisherKit, Stream stream) {

    }

    @Override
    public void onError(PublisherKit publisherKit, OpentokError opentokError) {

    }

    // Subscriber Listener Methods

    @Override
    public void onConnected(SubscriberKit subscriberKit) {

    }

    @Override
    public void onReconnected(SubscriberKit subscriberKit) {

        callStatus.setText(getString(R.string.call_in_progress));
    }

    @Override
    public void onDisconnected(SubscriberKit subscriberKit) {

    }

    @Override
    public void onError(SubscriberKit subscriberKit, OpentokError opentokError) {

    }

    @Override
    public void onVideoDataReceived(SubscriberKit subscriberKit) {
        hideDialog();
        attachSubscriberView(mSubscriber);
    }

    @Override
    public void onVideoDisabled(SubscriberKit subscriber, String reason) {
        if (mSubscriber == subscriber) {
            setAudioOnlyView(true);
        }

        if (reason.equals("quality")) {
            mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.High);
            congestion = SubscriberQualityFragment.CongestionLevel.High;
            setSubQualityMargins();
            mSubscriberQualityFragment.showSubscriberWidget(true);
        }
    }

    public void setSubQualityMargins() {
        RelativeLayout subQualityContainer = mSubscriberQualityFragment.getSubQualityContainer();
        if (subQualityContainer != null) {
            RelativeLayout.LayoutParams subQualityLayoutParams = (RelativeLayout.LayoutParams) subQualityContainer.getLayoutParams();
            RelativeLayout.LayoutParams audioMeterLayoutParams = (RelativeLayout.LayoutParams) mAudioLevelView.getLayoutParams();

            int bottomMargin = 0;

            // control pub fragment
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {

                if (bottomMargin == 0) {
                    bottomMargin = Utils.getInstance().convertDpToPixel(10, this);
                }
                subQualityLayoutParams.rightMargin = Utils.getInstance().convertDpToPixel(10, this);
            }

            subQualityLayoutParams.bottomMargin = bottomMargin;

            mSubscriberQualityFragment.getSubQualityContainer().setLayoutParams(
                    subQualityLayoutParams);
        }

    }

    @Override
    public void onVideoEnabled(SubscriberKit subscriber, String reason) {
        if (mSubscriber == subscriber) {
            setAudioOnlyView(false);
        }
        if (reason.equals("quality")) {
            mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.Low);
            congestion = SubscriberQualityFragment.CongestionLevel.Low;
            mSubscriberQualityFragment.showSubscriberWidget(false);
        }
    }

    @Override
    public void onVideoDisableWarning(SubscriberKit subscriberKit) {
        mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.Mid);
        congestion = SubscriberQualityFragment.CongestionLevel.Mid;
        setSubQualityMargins();
        mSubscriberQualityFragment.showSubscriberWidget(true);
    }

    @Override
    public void onVideoDisableWarningLifted(SubscriberKit subscriberKit) {
        mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.Low);
        congestion = SubscriberQualityFragment.CongestionLevel.Low;
        mSubscriberQualityFragment.showSubscriberWidget(false);
    }


    //Incoming call state listener permission

    private void checkPermission() {

        activity = this;
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, android.Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("Handle Incoming Call");

// final String[] PERMISSIONS_CONTACT = {Manifest.permission.READ_CONTACTS,Manifest.permission.READ_SMS};

        Map<String, Integer> perms = new HashMap<String, Integer>();
// Initial
        perms.put(android.Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);

        if (permissionsList.size() == 0) {
        } else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
// Need Rationale
// String message = "You need to grant access to Contacts and SMS permissions";
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                            REQUEST_CODE_ASK_PERMISSIONS);
                                }
                            });
                    return;
                }
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;

            }
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<String, Integer>();
// Initial
            perms.put(android.Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);

// Fill with results
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
// Check for ACCESS_FINE_LOCATION
            if (perms.get(android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                initialiseIncomingCallListener();
            } else {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_message_permission_denied),Utils.LONG_TOAST);
//                Toast.makeText(getApplicationContext(), "Permissions has not been granted", Toast.LENGTH_LONG).show();

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
// Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
                return false;
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(MakeCallActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onReconnecting(Session session) {
        callStatus.setText(getString(R.string.connecting));
    }

    @Override
    public void onReconnected(Session session) {

        callStatus.setText(getString(R.string.action_request_sent_friend));
    }

    @Override
    public void onPause() {
        super.onPause();
        try{
            if(customHandler!=null && updateTimerThread != null)
                customHandler.removeCallbacks(updateTimerThread);
            if(customHandler2!=null && updateTimerThread2 != null)
                customHandler2.removeCallbacks(updateTimerThread2);

            DataStorage.getInstance().setCallProgress(false);
        }catch (Exception e){}
    }
}
