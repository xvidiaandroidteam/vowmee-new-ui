package net.xvidia.vowmee.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.TimelineTabs;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.helper.DividerItemDecoration;
import net.xvidia.vowmee.helper.MyCustomLayoutManager;
import net.xvidia.vowmee.listadapter.ILoadMoreItems;
import net.xvidia.vowmee.listadapter.TimelineRecyclerViewAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.storage.DataStorage;
import net.xvidia.vowmee.videoplayer.IVideoDownloadListener;
import net.xvidia.vowmee.videoplayer.VideosDownloader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ravi_office on 09-Jan-16.
 */
public class TimelineVows extends Fragment implements IVideoDownloadListener, SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView mRecyclerView;
    //    private ProgressBar progressBar;
    private TextView mMessageTextView;
    private TimelineRecyclerViewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Moment> momentList;
    VideosDownloader videosDownloader;
    private ProgressDialog progressDialog;
    private Context mContext;
    private ProgressBar mProgressBar;
    private boolean refresh;
    private boolean canscroll;
    private boolean isLoading;
    private int currentPage;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount,firstVisibleItem;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Handler videoLoadHandler;
    private Runnable videoLoadRunnable;
    public TimelineVows() {
    }

    @Override
    public void onResume() {
        super.onResume();
    if(!TimelineRecyclerViewAdapter.refreshimeline)
        sendMomentList(true);

        if(DataStorage.getInstance().getDestroyLiveFlag());
            sendDestroyAllLive();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timeline_vow, container, false);

        final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.fragment_timeline_vow);
        mMessageTextView = (TextView) frameLayout.findViewById(R.id.message_no_moments);
        mProgressBar = (ProgressBar)frameLayout.findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView = (RecyclerView) frameLayout.findViewById(R.id.fragment_timeline_recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
//        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.appcolor);
        swipeRefreshLayout.setColorSchemeResources(R.color.init_gradient, R.color.com_facebook_blue, R.color.red_color);
//        swipeRefreshLayout.setDistanceToTriggerSync(20);
        videoLoadHandler = new Handler();
        initialiseRecyclerView();

        isLoading = false;
        mContext = getContext();
        momentList = new ArrayList<Moment>();
        mMessageTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        currentPage = 0;
        showProgressBar();
        mMessageTextView.setVisibility(View.GONE);
        refresh =false;
        return view;
    }

    private void initialiseRecyclerView(){
        TimelineRecyclerViewAdapter.refreshimeline = false;
        swipeRefreshLayout.setRefreshing(false);
        mRecyclerView.setHasFixedSize(true);
        canscroll = true;
        if(momentList !=null && momentList.size()<1)
            canscroll = false;
        mLayoutManager = new MyCustomLayoutManager(mContext,canscroll);
        mRecyclerView.setLayoutManager(mLayoutManager);
        if(!refresh) {
            mRecyclerView.addItemDecoration(new DividerItemDecoration(20,false));
            refresh =true;
        }
        mAdapter = new TimelineRecyclerViewAdapter(true,mContext,getActivity(), momentList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnLoadMoreListener(new ILoadMoreItems() {
            @Override
            public void loadMoreItems(boolean load) {
                    isLoading = true;
                    TimelineRecyclerViewAdapter.disableLoad= false;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadMore();
                        }
                    },450);
            }
        });
        videosDownloader = new VideosDownloader(mContext);
        videosDownloader.setOnVideoDownloadListener(this);

//        if(Utils.getInstance().hasConnection(context))
//        {
//        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return disableTouch;
//            }
//        });
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                    LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                    int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
                    int findFirstCompletelyVisibleItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition();
//                    Log.i("CompletelyVisibleItem", String.valueOf(findFirstCompletelyVisibleItemPosition));
//                    Log.i("firstVisiblePosition", String.valueOf(firstVisiblePosition));
                    totalItemCount = layoutManager.getItemCount();
                    lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                    firstVisibleItem = firstVisiblePosition;
                    Moment video;
                    if (momentList != null && momentList.size() > 0) {
                        mAdapter.videoPlayerController.pauseVideo();
                        if (findFirstCompletelyVisibleItemPosition >= 0) {
                            TimelineRecyclerViewAdapter.disableLoad= false;
                            video = momentList.get(findFirstCompletelyVisibleItemPosition);
                            mAdapter.videoPlayerController.setcurrentPositionOfItemToPlay(findFirstCompletelyVisibleItemPosition);
//                            mAdapter.videoPlayerController.handlePlayBack(video,false);
                            disableTouchHandler(video);
//                            mRecyclerView.smoothScrollToPosition(findFirstCompletelyVisibleItemPosition);
                        } else if(firstVisiblePosition>-1) {
                            TimelineRecyclerViewAdapter.disableLoad= false;
                            video = momentList.get(firstVisiblePosition);
                            mAdapter.videoPlayerController.setcurrentPositionOfItemToPlay(firstVisiblePosition);
//                            mAdapter.videoPlayerController.handlePlayBack(video,false);
//                            mRecyclerView.smoothScrollToPosition(firstVisiblePosition);
                            disableTouchHandler(video);
                        }else if(lastVisibleItem>-1){
                            TimelineRecyclerViewAdapter.disableLoad= false;
                            video = momentList.get(lastVisibleItem);
                            mAdapter.videoPlayerController.setcurrentPositionOfItemToPlay(lastVisibleItem);
//                            mAdapter.videoPlayerController.handlePlayBack(video,false);
                            disableTouchHandler(video);
//                            mRecyclerView.smoothScrollToPosition(lastVisibleItem);
//                        }else{
//                            mAdapter.videoPlayerController.pauseVideo();
                        }
                    }
                    if (!isLoading &&totalItemCount>9&& totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        momentList.add(null);
                        isLoading =true;
                        mAdapter.notifyItemInserted(momentList.size() - 1);
                    }
                } if (newState == RecyclerView.SCROLL_STATE_SETTLING) {
                    TimelineRecyclerViewAdapter.disableLoad= false;
                    mAdapter.videoPlayerController.pauseVideo();

                }else{
                    TimelineRecyclerViewAdapter.disableLoad= true;
                    mAdapter.videoPlayerController.pauseVideo();
                }

            }
        });
    }

    private void disableTouchHandler(final Moment video){
        if(videoLoadRunnable != null && videoLoadHandler !=null){
            videoLoadHandler.removeCallbacks(videoLoadRunnable);
        }
        videoLoadRunnable = new Runnable() {
            @Override
            public void run() {
                mAdapter.videoPlayerController.handlePlayBack(video,false);
            }
        };
        videoLoadHandler.postDelayed(videoLoadRunnable,1500);
    }
    @Override
    public void onVideoDownloaded(Moment video,boolean image) {
        mAdapter.videoPlayerController.handlePlayBack(video,image);
        try {

            mAdapter.notifyDataSetChanged();
        }catch (IllegalStateException e){

        }catch(Exception e){

        }
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    private void sendMomentList(final boolean firstTime) {
        try {
            currentPage =  1;
            final String username = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getMomentFeedsListUrl(IAPIConstants.API_KEY_GET_MOMENT_FEEDS_TIMELINE_LIST,username,username, AppConsatants.FALSE,"1", "10");
            VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(url,false);
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<Moment> obj = null;
                    try {
//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Moment>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    hideProgressBar();
                    if (obj != null) {

                        if (obj.size() > 0) {
                            currentPage = 2;
                            mMessageTextView.setVisibility(View.GONE);
                            momentList = obj;
                            initialiseRecyclerView();
                            mAdapter.notifyDataSetChanged();
                            videosDownloader.startVideosDownloading(momentList,firstTime);
                            mRecyclerView.setVisibility(View.VISIBLE);
                        }else{
                            mMessageTextView.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);

                        }
                    }else{
                        mMessageTextView.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    if(momentList != null && momentList.size() > 0){
                        mMessageTextView.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                    }else{
                        mMessageTextView.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    }

//                    showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
                }
            })
            {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        //                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadMore() {
        try {
            final int previousPage = currentPage;
            final String username = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getMomentFeedsListUrl(IAPIConstants.API_KEY_GET_MOMENT_FEEDS_TIMELINE_LIST,username,username, AppConsatants.FALSE,""+previousPage, "10");
//            Log.i("Loading completed","currentPage ="+currentPage +"\nUrl "+url);
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<Moment> obj1 = null;
                    try {

//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj1 = mapper.readValue(response.toString(), new TypeReference<List<Moment>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    final List<Moment> obj = obj1;
                    isLoading = false;
                    Moment lastMoment = momentList.get(momentList.size()-1);
                    if(lastMoment==null) {
                        momentList.remove(momentList.size() - 1);
                        mAdapter.notifyItemRemoved(momentList.size());
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressBar();
                            if (obj != null) {
                                if (obj.size() > 0) {
                                    if (momentList == null)
                                        momentList = obj;
                                    if (currentPage == previousPage) {
                                        currentPage = previousPage + 1;
                                        mMessageTextView.setVisibility(View.GONE);
                                        momentList.addAll(obj);
                                        videosDownloader.startVideosDownloading(obj, true);
                                        mRecyclerView.setVisibility(View.VISIBLE);
                                        int curSize = mAdapter.getItemCount();
                                        mAdapter.setMomentList(momentList);
                                        mAdapter.notifyItemRangeInserted(curSize, momentList.size() - 1);
                                    }
                                }
                            }
                        }
                    });
//                    Log.i("Loading completed","currentPage ="+currentPage);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    isLoading = false;
                    currentPage = currentPage-1;
                    if(currentPage<previousPage)
                        currentPage=previousPage;
                    if(momentList!=null) {
                        if(momentList.size()>0) {
                            Moment lastMoment = momentList.get(momentList.size() - 1);
                            if (lastMoment == null) {
                                momentList.remove(momentList.size() - 1);
                                mAdapter.notifyItemRemoved(momentList.size());
                            }
                        }
                        if (momentList != null && momentList.size() > 0) {
                            mMessageTextView.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.VISIBLE);
                        } else {
                            mMessageTextView.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                        }
                    }
                }
            }) {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        //                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendDestroyAllLive() {
        try {
//	showProgressBar(getString(R.string.progressbar_fetching_info));
            final String username = DataStorage.getInstance().getUserUUID();
            String url = ServiceURLManager.getInstance().getDestroyAllLive(username);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                     DataStorage.getInstance().setDestroyLiveFlag(false);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }
    private void showProgressBar() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                swipeRefreshLayout.setRefreshing(true);
            }
        });
       /* if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }*/
//        progressDialog = ProgressDialog.show(mContext, null, null, true, false);
//        progressDialog.setContentView(R.layout.progressbar);
//        TextView progressBarMessage = (TextView) progressDialog.findViewById(R.id.progressBar_message);
//        progressBarMessage.setText(msg);
    }
    private void hideProgressBar() {
        ((TimelineTabs)mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                if (mProgressBar != null) {
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });

//        if (progressDialog != null) {
//            progressDialog.dismiss();
//        }
    }

    @Override
    public void onRefresh() {
        TimelineRecyclerViewAdapter.refreshimeline = false;
        sendMomentList(false);
    }
}
