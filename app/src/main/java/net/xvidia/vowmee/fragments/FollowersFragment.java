package net.xvidia.vowmee.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.ConnectionTabs;
import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.SearchActivity;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.helper.DividerItemDecoration;
import net.xvidia.vowmee.helper.MyCustomLayoutManager;
import net.xvidia.vowmee.listadapter.FollowerListAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ravi_office on 09-Jan-16.
 */
public class FollowersFragment extends Fragment implements  SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView mRecyclerView;
    private TextView mMessageTextView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Profile> searchList;
    private Context mContext;
    private ProgressBar mProgressBar;
    private boolean refresh;

    private SwipeRefreshLayout swipeRefreshLayout;
    public FollowersFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();

        sendListRequest();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timeline_vow, container, false);

        final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.fragment_timeline_vow);
        mMessageTextView = (TextView) frameLayout.findViewById(R.id.message_no_moments);
        mProgressBar = (ProgressBar)frameLayout.findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView = (RecyclerView) frameLayout.findViewById(R.id.fragment_timeline_recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
//        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.appcolor);
        swipeRefreshLayout.setColorSchemeResources(R.color.init_gradient, R.color.com_facebook_blue, R.color.red_color);
//        swipeRefreshLayout.setDistanceToTriggerSync(20);
        initialiseRecyclerView();

        mContext = getContext();
        searchList = new ArrayList<Profile>();
        mMessageTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntentSearch = new Intent(getActivity(), SearchActivity.class);
                startActivity(mIntentSearch);
            }
        });

        showProgressBar();

        mMessageTextView.setText(getString(R.string.connections_followers));
        mMessageTextView.setVisibility(View.GONE);
        refresh =false;
        return view;
    }

    private void initialiseRecyclerView(){
        swipeRefreshLayout.setRefreshing(false);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new MyCustomLayoutManager(mContext,true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        if(!refresh) {
            mRecyclerView.addItemDecoration(new DividerItemDecoration(0,false));
            refresh =true;
        }
        mAdapter = new FollowerListAdapter(mContext, searchList);
        mRecyclerView.setAdapter(mAdapter);
//        mRecyclerView.addOnScrollListener(new RecyclerViewScrollListener(this) {
//            @Override
//            public void onMoved(int distance) {
//                toolbar.setTranslationY(-distance);
//            }
//        });
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    private void sendListRequest() {
        try {
            String username= DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getListUrl(IAPIConstants.API_KEY_GET_FOLLOWERS_LIST, username, username, "0", "0");

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    List<Profile> obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Profile>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {

                            mMessageTextView.setVisibility(View.GONE);
                            if (searchList != null)
                                searchList.clear();
                            searchList = obj;
                            initialiseRecyclerView();
                            if (mRecyclerView != null)
                                mRecyclerView.setVisibility(View.VISIBLE);

                            if (mAdapter != null)
                                mAdapter.notifyDataSetChanged();

                        } else {
                            mMessageTextView.setVisibility(View.VISIBLE);
                            if (mRecyclerView != null)
                                mRecyclerView.setVisibility(View.GONE);
                        }
                    } else {
                        mMessageTextView.setVisibility(View.VISIBLE);
                        if (mRecyclerView != null)
                            mRecyclerView.setVisibility(View.GONE);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    mMessageTextView.setVisibility(View.VISIBLE);
                    if (mRecyclerView != null)
                        mRecyclerView.setVisibility(View.GONE);

                }
            }){
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        //                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgressBar() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                swipeRefreshLayout.setRefreshing(true);
            }
        });
       /* if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }*/
//        progressDialog = ProgressDialog.show(mContext, null, null, true, false);
//        progressDialog.setContentView(R.layout.progressbar);
//        TextView progressBarMessage = (TextView) progressDialog.findViewById(R.id.progressBar_message);
//        progressBarMessage.setText(msg);
    }
    private void hideProgressBar() {
        ((ConnectionTabs)mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                if (mProgressBar != null) {
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });

//        if (progressDialog != null) {
//            progressDialog.dismiss();
//        }
    }

    @Override
    public void onRefresh() {
        sendListRequest();
    }
}
