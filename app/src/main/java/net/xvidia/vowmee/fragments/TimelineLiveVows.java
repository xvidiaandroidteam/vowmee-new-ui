package net.xvidia.vowmee.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.TimelineTabs;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.helper.MyCustomLayoutManager;
import net.xvidia.vowmee.listadapter.ILoadMoreItems;
import net.xvidia.vowmee.listadapter.TimelineLiveRecyclerViewAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ravi_office on 09-Jan-16.
 */
public class TimelineLiveVows extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView mRecyclerView;
    private TimelineLiveRecyclerViewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Moment> momentList;
    private Context mContext;
    private TextView mMessageTextView;
    private ProgressBar mProgressBar;
    private boolean canscroll;
    private boolean isLoading;
    private int currentPage;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private SwipeRefreshLayout swipeRefreshLayout;

    public TimelineLiveVows() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timeline_live_vow, container, false);

        final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.fragment_timeline_live_vow);
        mMessageTextView = (TextView) frameLayout.findViewById(R.id.message_no_moments);
        mProgressBar = (ProgressBar) frameLayout.findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView = (RecyclerView) frameLayout.findViewById(R.id.fragment_timeline_live_recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.init_gradient, R.color.com_facebook_blue, R.color.red_color);
        initialiseRecyclerView();
//        swipeRefreshLayout.setRefreshing(true);

        momentList = new ArrayList<Moment>();
        mContext = getContext();
        isLoading = false;
//        showProgressBar();
        currentPage = 0;
//        sendMomentList();
        mMessageTextView.setVisibility(View.GONE);
        return view;
    }


    private void initialiseRecyclerView() {
        swipeRefreshLayout.setRefreshing(false);
        mRecyclerView.setHasFixedSize(true);
        canscroll = true;
        if (momentList != null && momentList.size() < 1)
            canscroll = false;
        mLayoutManager = new MyCustomLayoutManager(mContext,canscroll) ;
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new TimelineLiveRecyclerViewAdapter(true, mContext, momentList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnLoadMoreListener(new ILoadMoreItems() {
            @Override
            public void loadMoreItems(boolean load) {
                isLoading = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadMore();
                    }
                }, 450);
            }
        });
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                    totalItemCount = layoutManager.getItemCount();
                    lastVisibleItem = layoutManager.findLastVisibleItemPosition();

                    if (!isLoading && totalItemCount > 9 && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        isLoading=true;
                        momentList.add(null);
                        mAdapter.notifyItemInserted(momentList.size() - 1);

                    }
                }
            }
        });

//        if(Utils.getInstance().hasConnection(context))
//        {

//        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//            }
//
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//
//                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
//
//                    LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
//                    int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
//                    int findFirstCompletelyVisibleItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition();
//                    Log.i("CompletelyVisibleItem", String.valueOf(findFirstCompletelyVisibleItemPosition));
//                    Log.i("firstVisiblePosition", String.valueOf(firstVisiblePosition));
//
//                    Moment video;
//                    if (momentList != null && momentList.size() > 0) {
//                        if (findFirstCompletelyVisibleItemPosition >= 0) {
//                            video = momentList.get(findFirstCompletelyVisibleItemPosition);
//                            mAdapter.videoPlayerController.setcurrentPositionOfItemToPlay(findFirstCompletelyVisibleItemPosition);
//                            mAdapter.videoPlayerController.handlePlayBack(video, false);
//                        } else {
//                            video = momentList.get(firstVisiblePosition);
//                            mAdapter.videoPlayerController.setcurrentPositionOfItemToPlay(firstVisiblePosition);
//                            mAdapter.videoPlayerController.handlePlayBack(video, false);
//                        }
//                    }
//                } else {
//                    mAdapter.videoPlayerController.pauseVideo();
//                }
//            }
//        });

    }


    private void sendMomentList() {
        try {
            currentPage =  1;
            final String username = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getMomentFeedsListUrl(IAPIConstants.API_KEY_GET_MOMENT_FEEDS_TIMELINE_LIST, username, username, AppConsatants.TRUE, "1", "30");

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<Moment> obj = null;
                    try {
//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Moment>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    hideProgressBar();
                    if (obj != null) {

                        if (obj.size() > 0) {
                            currentPage = 2;
//                            Log.i("Moment uuid", "" + obj.get(0).toString());
//                            String fileName = obj.get(0).getLink();
//                            downloadContentFromS3Bucket(fileName);
                            mMessageTextView.setVisibility(View.GONE);
                            momentList = obj;
                            initialiseRecyclerView();
                            mAdapter.notifyDataSetChanged();
                            mRecyclerView.setVisibility(View.VISIBLE);
                        } else {
                            mMessageTextView.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);

                        }
                    } else {
                        mMessageTextView.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
//                        showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                            }
//                        });
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    if (momentList != null && momentList.size() > 0) {
                        mMessageTextView.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                    } else {
                        mMessageTextView.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    }
//                    showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
                }
            }) {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
//                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadMore() {
        try {
            final int previousPage = currentPage;
            final String username = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getMomentFeedsListUrl(IAPIConstants.API_KEY_GET_MOMENT_FEEDS_TIMELINE_LIST, username, username, AppConsatants.TRUE, "" + currentPage, "30");

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<Moment> obj = null;
                    try {
//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Moment>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    isLoading = false;
                    hideProgressBar();
                    Moment lastMoment = momentList.get(momentList.size() - 1);
                    if (lastMoment == null) {
                        momentList.remove(momentList.size() - 1);
                        mAdapter.notifyItemRemoved(momentList.size());
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {
                            if(momentList==null)
                                momentList = obj;
                            if(currentPage==previousPage) {
                                currentPage = previousPage + 1;
                                mMessageTextView.setVisibility(View.GONE);
                                momentList.addAll(obj);

//                            HashSet<Moment> set = new HashSet<>();
//                            set.addAll(momentList);
//                            momentList.clear();
//                            momentList.addAll(set);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                int curSize = mAdapter.getItemCount();
                                mAdapter.setMomentList(momentList);
                                mAdapter.notifyItemRangeInserted(curSize, momentList.size() - 1);
                            }
                        } else {
//                            currentPage = currentPage-1;
                        }
                    } else {
//                        currentPage = currentPage-1;
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    isLoading = false;
                    currentPage = currentPage - 1;
                    if (currentPage < previousPage)
                        currentPage = previousPage;

                    if(momentList!=null) {
                        if (momentList.size() > 0) {
                            Moment lastMoment = momentList.get(momentList.size() - 1);
                            if (lastMoment == null) {
                                momentList.remove(momentList.size() - 1);
                                mAdapter.notifyItemRemoved(momentList.size());
                            }
                        }
                        if (momentList != null && momentList.size() > 0) {
                            mMessageTextView.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.VISIBLE);
                        } else {
                            mMessageTextView.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                        }
                    }
//                    showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
                }
            }) {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        //                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

 /*   private void showError(String mesg, DialogInterface.OnClickListener okClicked) {
        new AlertDialog.Builder(mContext).setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(mesg)
                .setPositiveButton(android.R.string.ok, okClicked)
                .show();
    }
*/

    private void hideProgressBar() {
        try {
            ((TimelineTabs) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(false);
                    if (mProgressBar != null) {
                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            });
        }catch (WindowManager.BadTokenException e){

        }catch (Exception e){

        }

    }

    @Override
    public void onResume() {
        super.onResume();

        sendMomentList();

    }

    @Override
    public void onRefresh() {
        sendMomentList();
    }
}
