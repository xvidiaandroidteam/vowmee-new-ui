package net.xvidia.vowmee.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.helper.DividerItemDecoration;
import net.xvidia.vowmee.helper.MyCustomLayoutManager;
import net.xvidia.vowmee.listadapter.ILoadMoreItems;
import net.xvidia.vowmee.listadapter.UserActivityAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.ActivityLog;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ravi_office on 09-Jan-16.
 */
public class TimelineActivityOwn extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView mRecyclerView;
    private UserActivityAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private List<ActivityLog> activityLogs;
    private Context mContext;
    private TextView mMessageTextView;
    private ProgressBar mProgressBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean refreshed;
    private boolean isLoading;
    private int currentPage;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;

    public TimelineActivityOwn() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timeline_activity, container, false);

        final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.fragment_timeline_activity);
        mMessageTextView = (TextView) frameLayout.findViewById(R.id.message_no_moments);
        mProgressBar = (ProgressBar)frameLayout.findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView = (RecyclerView) frameLayout.findViewById(R.id.fragment_timeline_activity_recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.init_gradient, R.color.colorPrimaryDark);
        initialiseRecyclerView();
        swipeRefreshLayout.setRefreshing(true);
        mContext = getContext();
        activityLogs = new ArrayList<ActivityLog>();

        isLoading = false;
        refreshed = false;
        currentPage = 0;
        showProgressBar();
        getActivityList();
        mMessageTextView.setVisibility(View.GONE);
        return view;
    }

    private void initialiseRecyclerView(){
        swipeRefreshLayout.setRefreshing(false);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new MyCustomLayoutManager(mContext,true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        if(!refreshed) {
            refreshed = true;
            mRecyclerView.addItemDecoration(new DividerItemDecoration(1,false));
        }
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new UserActivityAdapter(mContext, activityLogs,true);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnLoadMoreListener(new ILoadMoreItems() {
            @Override
            public void loadMoreItems(boolean load) {
                isLoading = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadMore();
                    }
                }, 450);
            }
        });
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();

                if (!isLoading &&totalItemCount>9&& totalItemCount <= (lastVisibleItem + visibleThreshold)) {
//                    if (mOnLoadMoreListener != null) {
//                        mOnLoadMoreListener.onLoadMore();
//                    }
                    activityLogs.add(null);
                    mAdapter.notifyItemInserted(activityLogs.size() - 1);
                    isLoading = true;
                    loadMore();
                }
            }
        });
    }


    private void getActivityList() {
        try {

            currentPage =  1;
            final String username = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getActivityList(IAPIConstants.API_KEY_GET_OWN_ACTIVITY_LIST,username,"1", "30");

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<ActivityLog> obj = null;
                    try {
//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj = mapper.readValue(response.toString(), new TypeReference<List<ActivityLog>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    hideProgressBar();
                    if (obj != null) {

                        if (obj.size() > 0) {
                            currentPage = 2;
                            ModelManager.getInstance().setOwnActivity(obj);
                            mMessageTextView.setVisibility(View.GONE);
                            activityLogs = obj;
                            initialiseRecyclerView();
                            mAdapter.notifyDataSetChanged();
                            mRecyclerView.setVisibility(View.VISIBLE);

                        }else{
                            mMessageTextView.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);

                        }
                    }else{
                        mMessageTextView.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
//                        showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                            }
//                        });
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    if(activityLogs != null && activityLogs.size() > 0){
                        mMessageTextView.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                    }else{
                        mMessageTextView.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    }
//                    showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
                }
            })
            {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        //                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void loadMore() {
        try {
            final int previousPage = currentPage;
            final String username = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getActivityList(IAPIConstants.API_KEY_GET_OWN_ACTIVITY_LIST,username,""+currentPage, "30");

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<ActivityLog> obj = null;
                    try {
//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj = mapper.readValue(response.toString(), new TypeReference<List<ActivityLog>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    isLoading = false;
                    hideProgressBar();
                    ActivityLog lastMoment = activityLogs.get(activityLogs.size()-1);
                    if(lastMoment==null) {
                        activityLogs.remove(activityLogs.size() - 1);
                        mAdapter.notifyItemRemoved(activityLogs.size());
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {

                            if(currentPage==previousPage) {
                                currentPage = previousPage + 1;
                                ModelManager.getInstance().setOthersActivity(obj);
                                mMessageTextView.setVisibility(View.GONE);
                                activityLogs.addAll(obj);
//                            HashSet<ActivityLog> set = new HashSet<>();
//                            set.addAll(activityLogs);
//                            activityLogs.clear();
//                            activityLogs.addAll(set);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                int curSize = mAdapter.getItemCount();
                                mAdapter.setActivityLogs(activityLogs);
                                mAdapter.notifyItemRangeInserted(curSize, activityLogs.size() - 1);
                            }
//                            mAdapter.notifyDataSetChanged();
                        }else{
//                            currentPage = currentPage-1;
                        }
                    }else{
//                        currentPage = currentPage-1;
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    currentPage = currentPage - 1;
                    if (currentPage < previousPage)
                        currentPage = previousPage;
                    if (activityLogs != null) {
                        if(activityLogs.size()>0) {
                            ActivityLog lastMoment = activityLogs.get(activityLogs.size() - 1);
                            if (lastMoment == null) {
                                activityLogs.remove(activityLogs.size() - 1);
                                mAdapter.notifyItemRemoved(activityLogs.size());
                            }
                        }
                        isLoading = false;
                        if (activityLogs != null && activityLogs.size() > 0) {
                            mMessageTextView.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.VISIBLE);
                        } else {
                            mMessageTextView.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                        }
                    }
//                    showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
                }
            })
            {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        //                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void showError(final int resId, String mesg, DialogInterface.OnClickListener okClicked) {
        new AlertDialog.Builder(mContext).setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(mesg)
                .setPositiveButton(android.R.string.ok, okClicked)
                .show();
    }


    private void showProgressBar() {
        swipeRefreshLayout.setRefreshing(true);
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
//        progressDialog = ProgressDialog.show(mContext, null, null, true, false);
//        progressDialog.setContentView(R.layout.progressbar);
//        TextView progressBarMessage = (TextView) progressDialog.findViewById(R.id.progressBar_message);
//        progressBarMessage.setText(msg);
    }
    private void hideProgressBar() {
        swipeRefreshLayout.setRefreshing(false);
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
//        if (progressDialog != null) {
//            progressDialog.dismiss();
//        }
    }

    @Override
    public void onResume() {
        super.onResume();

//        getActivityList();

    }

    @Override
    public void onRefresh() {
        getActivityList();
    }
}
