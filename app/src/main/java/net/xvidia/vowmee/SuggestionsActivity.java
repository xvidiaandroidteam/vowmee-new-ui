package net.xvidia.vowmee;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.amazonaws.mobile.AWSMobileClient;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.WrappingLinearLayoutManager;
import net.xvidia.vowmee.listadapter.RecyclerViewScrollListener;
import net.xvidia.vowmee.listadapter.SearchListAdapter;
import net.xvidia.vowmee.listadapter.SearchListGroupAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.FacebookFriend;
import net.xvidia.vowmee.network.model.GroupProfile;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.network.model.Search;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class SuggestionsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private Context context;
    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerViewGroup;
    private RecyclerView.LayoutManager mLayoutManager;
    private SearchListAdapter mAdapter;
    private SearchListGroupAdapter mAdapterGroup;
    private List<GroupProfile> groupSearchList;
    private List<Profile> searchList;
    private Toolbar toolbar;
    private TextView mMessageTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestion);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mMessageTextView = (TextView) findViewById(R.id.message_no_results);
        mMessageTextView.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView_list);
//        pendingRecyclerView = (RecyclerView) findViewById(R.id.pendingRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        context = SuggestionsActivity.this;
        toolbar.setPadding(0, 0, 0, 0);

        mMessageTextView.setVisibility(View.GONE);
        searchList = new ArrayList<>();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initialiseData();
    }

    private void initialiseData() {
           getSupportActionBar().setTitle(getString(R.string.nav_suggestions));
        setFinalList();

    }



    private void initialiseRecyclerViewGroup(){
//        addList();
        RecyclerView.LayoutManager mLayoutManager = new WrappingLinearLayoutManager(this);
        mRecyclerViewGroup = (RecyclerView) findViewById(R.id.recycleView_list_group);
        mRecyclerViewGroup.setLayoutManager(mLayoutManager);
        mAdapterGroup = new SearchListGroupAdapter(SuggestionsActivity.this, groupSearchList);

        mRecyclerViewGroup.setAdapter(mAdapterGroup);
        mRecyclerViewGroup.setNestedScrollingEnabled(false);
        mRecyclerViewGroup.setHasFixedSize(false);
        mRecyclerViewGroup.addOnScrollListener(new RecyclerViewScrollListener(this) {
            @Override
            public void onMoved(int distance) {
                toolbar.setTranslationY(-distance);
            }
        });
    }
    private void initialiseRecyclerView() {

        RecyclerView.LayoutManager mLayoutManager = new WrappingLinearLayoutManager(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView_list);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new SearchListAdapter(SuggestionsActivity.this, searchList);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.addOnScrollListener(new RecyclerViewScrollListener(this) {
            @Override
            public void onMoved(int distance) {
                toolbar.setTranslationY(-distance);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setGoogleAnalyticScreen("Suggestion");
    }
    private void setFinalList(){
        if (AWSMobileClient.defaultMobileClient() != null &&
                AWSMobileClient.defaultMobileClient().getIdentityManager() != null &&
                AWSMobileClient.defaultMobileClient().getIdentityManager().getFacebookFriendList() != null &&
                AWSMobileClient.defaultMobileClient().getIdentityManager().getFacebookFriendList().size() > 0) {
            getFacebookFriendList();
        }else{
            sendSuggestionListRequest();
        }
    }
    private void getFacebookFriendList(){
        List<FacebookFriend> facebookFriendList = AWSMobileClient.defaultMobileClient().getIdentityManager().getFacebookFriendList();
        ArrayList<String> list = null;
        if(facebookFriendList!=null && facebookFriendList.size()>0){
            if(ModelManager.getInstance().getFacebookFriendList() == null) {
                list = new ArrayList<>();
                for (FacebookFriend obj : facebookFriendList) {
                    list.add(obj.getId());
                }
                getFacebookFriendInfoRequest(list);
            }else{
                if(searchList!=null)
                    searchList.clear();
                searchList= getFacebookSuggestion();
                initialiseRecyclerView();
                if (mAdapter != null)
                    mAdapter.notifyDataSetChanged();

                sendSuggestionListRequest();
            }
        }
    }

    private ArrayList<Profile> getFacebookSuggestion(){
        ArrayList<Profile> suggsList= new ArrayList<>();
        for(Profile obj:ModelManager.getInstance().getFacebookFriendList()){
            if(obj != null){
                if(!obj.isBlockedByViewer()
                        &&!obj.isFollowedByViewer()
                        && !obj.isFollowingViewer()
                        && !obj.isFriendWithViewer()
                        && !obj.isFriendRequestFromViewer()){
                    suggsList.add(obj);
                }
            }
        }
        return suggsList;
    }
    private void getFacebookFriendInfoRequest(ArrayList<String> stringArrayList) {
        try {

            String url = ServiceURLManager.getInstance().getFacebookAddList(IAPIConstants.API_KEY_GET_PERSON_BY_FACEBOOK_ID, DataStorage.getInstance().getUsername());

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(stringArrayList);
            } catch (IOException e) {
                e.printStackTrace();
            }
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    ObjectMapper mapper = new ObjectMapper();
                    List<Profile> obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Profile>>(){});
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {
                            if (searchList != null)
                                searchList.clear();
                            searchList = obj;
                            ModelManager.getInstance().setFacebookFriendList(searchList);
                            searchList= getFacebookSuggestion();
                            initialiseRecyclerView();
                            if (mAdapter != null)
                                mAdapter.notifyDataSetChanged();

                        }
                    }

                    sendSuggestionListRequest();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    sendSuggestionListRequest();
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    private void sendSuggestionListRequest() {
        try {


            String url = ServiceURLManager.getInstance().getSuggestListUrl(DataStorage.getInstance().getUserUUID(),"0","0");

            JsonObjectRequest request =new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    ObjectMapper mapper = new ObjectMapper();
                    Search obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Search.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
                        if (obj.getPersonVos()!=null) {
                        if (obj.getPersonVos().size() > 0) {
                            mMessageTextView.setVisibility(View.GONE);
                            if (searchList != null) {
                                searchList.addAll(obj.getPersonVos());
                            }else{
                                searchList = obj.getPersonVos();
                            }
                            HashSet<Profile> duplicate = new HashSet<>();
                            duplicate.addAll(searchList);
                            if(searchList!=null)
                                searchList.clear();
                            searchList.addAll(duplicate);
                            initialiseRecyclerView();

                            if (mRecyclerView != null)
                                mRecyclerView.setVisibility(View.VISIBLE);

                            if (mAdapter != null)
                                mAdapter.notifyDataSetChanged();

                            }
                            if (obj.getGroupVos()!=null) {
                                mMessageTextView.setVisibility(View.GONE);
                                if(groupSearchList != null)
                                    groupSearchList.clear();
                                groupSearchList = obj.getGroupVos();
//                            Log.i("Moment uuid", "" + obj.get(0).toString());
                                initialiseRecyclerViewGroup();
                                if(mRecyclerViewGroup != null)
                                    mRecyclerViewGroup.setVisibility(View.VISIBLE);
                                mAdapterGroup.notifyDataSetChanged();
                            }

                        }
                    }

                }

            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if(searchList!=null && groupSearchList !=null ) {
                        if (searchList.size() == 0 && groupSearchList.size() == 0) {
                            mMessageTextView.setVisibility(View.VISIBLE);
                            if (mRecyclerView != null)
                                mRecyclerView.setVisibility(View.GONE);
                            if (mRecyclerViewGroup != null)
                                mRecyclerViewGroup.setVisibility(View.GONE);
                        }
                    }
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
    private void sendRequest(String groupUuid,String requesterUuid,boolean accept){
        try {
            if(groupUuid.isEmpty()||requesterUuid.isEmpty())
                return;
            final String userUuid = DataStorage.getInstance().getUsername();
//            String groupUuid = profile.getGroup().getUuid();
            String url =ServiceURLManager.getInstance().getGroupJoinDenyUrl(groupUuid,requesterUuid,userUuid);
            if(accept) {
                url = ServiceURLManager.getInstance().getGroupJoinAcceptUrl(groupUuid, requesterUuid, userUuid);
            }
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.LONG_TOAST);

                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public void onRefresh() {
        initialiseData();
    }

    private void setGoogleAnalyticScreen(String screenName){
        MyApplication application = (MyApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.setClientId(DataStorage.getInstance().getUserUUID());
        mTracker.setAppVersion(""+Utils.getInstance().getAppVersion(MyApplication.getAppContext()));
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}
