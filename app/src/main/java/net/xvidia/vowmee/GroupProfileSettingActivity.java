package net.xvidia.vowmee;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.GroupProfile;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.IOException;

public class GroupProfileSettingActivity extends AppCompatActivity  {
    private CheckBox publicCheckBox;
    private CheckBox privateCheckBox;
    private CheckBox searchableCheckBox;
    private TextView publicMessage;
    private static GroupProfile groupProfile;
    private boolean privateFlag;
    private boolean searchableFlag;
    private RelativeLayout searchableLayout;
    private Toolbar toolbar;
    MenuItem menuSave;

    private TextView mExitView;
    private TextView mFollowGroup;
    private TextView mReportGroup;
    private TextView mGroupNameEditText;
    private String quesryString="";
    private CircularImageView groupImage;
    private ProgressBar mProgressBar;
    private Button doneButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_profile);

        searchableCheckBox = (CheckBox)findViewById(R.id.searchable_checkBox);
        publicCheckBox = (CheckBox)findViewById(R.id.public_checkBox);
        privateCheckBox = (CheckBox)findViewById(R.id.private_checkBox);
//        searchableCheckBoxTextView = (TextView)findViewById(R.id.searchable_checkBox_text);
//        publicCheckBoxTextView = (TextView)findViewById(R.id.public_checkBox_text);
//        privateCheckBoxTextView = (TextView)findViewById(R.id.private_checkBox_text);
        publicMessage = (TextView)findViewById(R.id.checkBox_text_message);
        searchableLayout = (RelativeLayout)findViewById(R.id.searchable_layout);
//        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mGroupNameEditText = (TextView)findViewById(R.id.new_groupname);
        groupImage = (CircularImageView)findViewById(R.id.group_image);
        mExitView = (TextView)findViewById(R.id.exit_view);
        mFollowGroup = (TextView)findViewById(R.id.follow_view);
        mReportGroup = (TextView)findViewById(R.id.report_view);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar);

        hideProgressBar();
//        groupImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent mIntent = new Intent(GroupProfileSettingActivity.this, CropGroupImageActivity.class);
//                Bundle mBundle = new Bundle();
//                mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, false);
//                mIntent.putExtras(mBundle);
//                startActivity(mIntent);
//            }
//        });
        groupProfile = ModelManager.getInstance().getGroupProfile();
        mExitView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (DataStorage.getInstance().getUserUUID().equalsIgnoreCase(ModelManager.getInstance().getGroupProfile().getOwnerUuid())) {
//                    showAlertDialog(getString(R.string.alert_message_owner_exit), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
////                            showMemberListDialog();
//                        }
//                    });
//                }else{
                if (ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_OWNER)) {
                    showAlertDialog(getString(R.string.alert_message_delete_group), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sendDeleteGroup();
                        }
                    });
                }else{
                    showAlertDialog(getString(R.string.alert_message_exit), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sendGroupExitRequest();
                        }
                    });
                }
//                }
            }
        });

        mFollowGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ModelManager.getInstance().getGroupProfile()== null)
                    return;
                if(ModelManager.getInstance().getGroupProfile().getRoleWithViewer()==null)
                    return;
                if (ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_FOLLOWER)){

                    sendFollowRequest(ModelManager.getInstance().getGroupProfile(), false);
                }else if (!ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MEMBER)
                        && !ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_OWNER)
                        && !ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MODERATOR)) {
                    sendFollowRequest(ModelManager.getInstance().getGroupProfile(), true);
                }
            }
        });
        mReportGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog(getString(R.string.alert_message_report_group), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendReportRequest();
                    }
                });
            }
        });

        CropGroupImageActivity.groupImage = groupProfile.getProfileImage();
        updateImage();
//        mRecyclerView.setHasFixedSize(true);
        searchableCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                searchableFlag = isChecked;
                searchableCheckBox.setChecked(isChecked);
                showDoneButton(true);
            }
        });
        publicCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                   publicCheckBox.setChecked(isChecked);
                        privateCheckBox.setChecked(!isChecked);
                privateFlag = !isChecked;
                if(isChecked){
                    publicMessage.setText(getString(R.string.prompt_group_public_message));
                    searchableLayout.setVisibility(View.GONE);
                }else{
                    publicMessage.setText(getString(R.string.prompt_group_private_message));
                    searchableLayout.setVisibility(View.VISIBLE);
                }


                showDoneButton(true);
            }
        });

        privateCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    publicMessage.setText(getString(R.string.prompt_group_private_message));
                    searchableLayout.setVisibility(View.VISIBLE);
                } else {
                    publicMessage.setText(getString(R.string.prompt_group_public_message));
                    searchableLayout.setVisibility(View.GONE);
                }

                privateFlag = isChecked;
                publicCheckBox.setChecked(!isChecked);
                privateCheckBox.setChecked(isChecked);
                showDoneButton(true);
            }
        });

        if(groupProfile == null) {
            return;
        }


//        initialiseRecyclerView();
    }

    private void updateImage() {

        if (groupImage != null) {
            Bitmap image = null;
            if (CropGroupImageActivity.groupImage != null && !CropGroupImageActivity.groupImage.isEmpty())
                image = Utils.getInstance().loadImageFromStorage(Utils.getInstance().getLocalContentPath(CropGroupImageActivity.groupImage), false);
            if (image != null) {
                groupImage.setImageBitmap(image);
            } else {
                groupImage.setImageResource(R.drawable.user);
            }
            download(groupImage, CropGroupImageActivity.groupImage);
        }
        mFollowGroup.setVisibility(View.GONE);
        if(ModelManager.getInstance().getGroupProfile()== null)
            return;
        if(ModelManager.getInstance().getGroupProfile().getRoleWithViewer()==null)
            return;
        if (ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MEMBER)
                || ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MODERATOR)) {
            mFollowGroup.setVisibility(View.GONE);
            mExitView.setText(getString(R.string.action_group_exit));
            mExitView.setVisibility(View.VISIBLE);
        }else if (ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_OWNER)){
            mFollowGroup.setVisibility(View.GONE);
            mExitView.setText(getString(R.string.action_group_delete));
            mExitView.setVisibility(View.VISIBLE);
        }else{
            if (!ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MEMBER)
                    && !ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_OWNER)
                    && !ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_FOLLOWER)
                    && !ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MODERATOR)) {
                mFollowGroup.setText(getResources().getString(R.string.follow_group));
            } else {
                mFollowGroup.setText(getResources().getString(R.string.action_unfollow));
            }
            mFollowGroup.setVisibility(View.VISIBLE);
            mExitView.setVisibility(View.GONE);
        }
    }

    private void showDoneButton(boolean show){
        if(show){
            if(menuSave !=null && doneButton != null) {
                doneButton.setEnabled(true);
                doneButton.setText(getResources().getString(R.string.action_done));
                doneButton.setTextColor(getResources().getColor(R.color.white));
            }
        }else{
            if(menuSave !=null && doneButton != null) {
                doneButton.setText(getResources().getString(R.string.action_done));
                doneButton.setEnabled(false);
                doneButton.setTextColor(getResources().getColor(R.color.white_disabled_menu));

            }
        }
    }
//    private void initialiseRecyclerView() {
//
//        RecyclerView.LayoutManager mLayoutManager = new WrappingLinearLayoutManager(this);
//        mRecyclerView.setLayoutManager(mLayoutManager);
//        mAdapter = new GroupMemberListAdapter(GroupProfileSettingActivity.this, memberList);
//
//        mRecyclerView.setAdapter(mAdapter);
//        mRecyclerView.setNestedScrollingEnabled(false);
//        mRecyclerView.setHasFixedSize(false);
////        mRecyclerView.addOnScrollListener(new RecyclerViewScrollListener(this) {
////            @Override
////            public void onMoved(int distance) {
////                toolbar.setTranslationY(-distance);
////            }
////        });
//    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        bindActivity();
    }

    private void bindActivity() {
        toolbar.setPadding(0, 0, 0, 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        quesryString = groupProfile.getName();
        mGroupNameEditText.setText(quesryString);

        getSupportActionBar().setTitle(quesryString);

        if(setOptionsVisibility()){
            if(groupProfile.getProfile().equalsIgnoreCase(AppConsatants.PROFILE_PRIVATE)){
                publicCheckBox.setChecked(false);
                privateCheckBox.setChecked(true);
                publicMessage.setText(getString(R.string.prompt_group_private_message));
                searchableLayout.setVisibility(View.VISIBLE);
            }else if(groupProfile.getProfile().equalsIgnoreCase(AppConsatants.PROFILE_PRIVATE_SEARCHABLE)){
                publicCheckBox.setChecked(false);
                privateCheckBox.setChecked(true);
                searchableCheckBox.setChecked(true);
                publicMessage.setText(getString(R.string.prompt_group_private_message));
                searchableLayout.setVisibility(View.VISIBLE);
            }else{
                publicCheckBox.setChecked(true);
                privateCheckBox.setChecked(false);
                publicMessage.setText(getString(R.string.prompt_group_public_message));
                searchableLayout.setVisibility(View.GONE);
            }
        }else{
            if(groupProfile.getProfile().equalsIgnoreCase(AppConsatants.PROFILE_PRIVATE)){
                publicCheckBox.setVisibility(View.GONE);
                privateCheckBox.setChecked(true);
                privateCheckBox.setVisibility(View.VISIBLE);
                privateCheckBox.setEnabled(false);
                publicMessage.setText(getString(R.string.prompt_group_private_message));
                searchableLayout.setVisibility(View.GONE);
//                publicCheckBoxTextView.setVisibility(View.GONE);
//                privateCheckBoxTextView.setVisibility(View.VISIBLE);

            }else if(groupProfile.getProfile().equalsIgnoreCase(AppConsatants.PROFILE_PRIVATE_SEARCHABLE)){
                publicCheckBox.setVisibility(View.GONE);
//                privateCheckBox.setVisibility(View.GONE);
                privateCheckBox.setChecked(true);
                privateCheckBox.setVisibility(View.VISIBLE);
                privateCheckBox.setEnabled(false);
//                publicCheckBoxTextView.setVisibility(View.GONE);
//                privateCheckBoxTextView.setVisibility(View.VISIBLE);
                publicMessage.setText(getString(R.string.prompt_group_private_message));
                searchableLayout.setVisibility(View.VISIBLE);
                searchableCheckBox.setChecked(true);
                searchableCheckBox.setVisibility(View.VISIBLE);
                searchableCheckBox.setEnabled(false);
//                searchableCheckBoxTextView.setVisibility(View.VISIBLE);
            }else{
//                publicCheckBox.setVisibility(View.GONE);
                privateCheckBox.setVisibility(View.GONE);
                searchableLayout.setVisibility(View.GONE);
                publicCheckBox.setChecked(true);
                publicCheckBox.setVisibility(View.VISIBLE);
                publicCheckBox.setEnabled(false);
//                publicCheckBoxTextView.setVisibility(View.VISIBLE);
//                privateCheckBoxTextView.setVisibility(View.GONE);
                publicMessage.setText(getString(R.string.prompt_group_public_message));
                searchableLayout.setVisibility(View.GONE);
            }
        }
    }

    private boolean setOptionsVisibility() {
        boolean visible = false;

        if (ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MODERATOR)
                ||ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_OWNER)) {
            visible = true;
        }

        return visible;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        getGroupMemberList();
        updateImage();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_group, menu);
        if (menu != null) {
           MenuItem menuAddMember = menu.findItem(R.id.action_add_member_group);
            menuSave = menu.findItem(R.id.action_next);
            MenuItemCompat.setActionView(menuSave, R.layout.group_next);
            View viewNext = (View) MenuItemCompat.getActionView(menuSave);
            doneButton = (Button) viewNext.findViewById(R.id.group_next);
            doneButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    sendSaveGroupInfoRequest();
                }
            });
            doneButton.setEnabled(false);
            doneButton.setTextColor(getResources().getColor(R.color.white_disabled_menu));

            menuSave.setVisible(true);
            doneButton.setVisibility(View.VISIBLE);
            showDoneButton(false);
//            if (setOptionsVisibility()) {
//                if (menuAddMember != null)
//                    menuAddMember.setVisible(true);
//            } else {
//                if (menuAddMember != null)
                    menuAddMember.setVisible(false);
//            }
        }
        return super.onCreateOptionsMenu(menu);
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cancel_group:
                onBackPressed();
                break;
            case R.id.action_save_group:
                sendSaveGroupInfoRequest();
                break;
           *//* case R.id.action_add_member_group:
                Intent mIntent = new Intent(GroupProfileSettingActivity.this, GroupAddMemberActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, false);
                mIntent.putExtras(mBundle);
                startActivity(mIntent);
                break;*//*
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }*/


    private void sendSaveGroupInfoRequest() {
        try {
            String profile = AppConsatants.PROFILE_PUBLIC;
            if(privateFlag && searchableFlag){
                profile= AppConsatants.PROFILE_PRIVATE_SEARCHABLE;
            }else if(privateFlag){
                profile= AppConsatants.PROFILE_PRIVATE;
            }
            if(groupProfile == null) {
                return;
            }
            groupProfile.setProfile(profile);
            groupProfile.setName(quesryString);

            showProgressbar();
            String userUuid = DataStorage.getInstance().getUserUUID();
            String url = ServiceURLManager.getInstance().getGroupEditUrl(userUuid);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(groupProfile);
            } catch (IOException e) {
                e.printStackTrace();
            }
//            Log.i("Register request", jsonObject);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    GroupProfile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), GroupProfile.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        String userUuid = DataStorage.getInstance().getUserUUID();
                        String urlList = ServiceURLManager.getInstance().getGroupListUrl(IAPIConstants.API_KEY_GROUP_LIST, userUuid);

                        VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(urlList, false);

                        ModelManager.getInstance().setGroupProfile(obj);
                        String username = obj.getCreatorUuid();
                        if(username != null && !username.isEmpty()) {
                            CropGroupImageActivity.groupImage = "";
                            finish();
                        }else{
                            showError(getString(R.string.error_general));
                        }
                    }
                    hideProgressBar();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
//                    if (error != null&& error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND) {
////                        Log.e("VolleyError ", "" + error.networkResponse.statusCode);
//                        showError(error.getMessage());
//                    } else {
                        showError(getString(R.string.error_general));
//                    }
                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void sendOwnerChangeRequest(Profile obj) {
        try {
            showProgressbar();
            String userUuid = DataStorage.getInstance().getUserUUID();
            String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
            String url = ServiceURLManager.getInstance().getGroupOwnerChangeUrl(groupUuid, userUuid,obj.getUuid());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    sendGroupExitRequest();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    showError(getString(R.string.error_general));

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }
    private void sendReportRequest() {
        try {
            showProgressbar();
            String userUuid = DataStorage.getInstance().getUserUUID();
            String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
            String url = ServiceURLManager.getInstance().getGroupReportUrl(groupUuid, userUuid);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    GroupProfile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), GroupProfile.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        ModelManager.getInstance().setGroupProfile(obj);
                    }
                    hideProgressBar();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    showError(getString(R.string.error_general));

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }
    private void sendGroupExitRequest() {
        try {
            showProgressbar();
            String userUuid = DataStorage.getInstance().getUserUUID();
            String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
            String url = ServiceURLManager.getInstance().getGroupMemberExitUrl(groupUuid, userUuid);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    String userUuid = DataStorage.getInstance().getUserUUID();
                    String urlList = ServiceURLManager.getInstance().getGroupListUrl(IAPIConstants.API_KEY_GROUP_LIST, userUuid);

                    VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(urlList, false);
                    Intent intent = new Intent(GroupProfileSettingActivity.this, GroupListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    showError(getString(R.string.error_general));

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }
    private void showError(final String mesg) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new AlertDialog.Builder(GroupProfileSettingActivity.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                }
            });
        }catch(WindowManager.BadTokenException e){}
        catch(Exception e){}
    }
    private void showAlertDialog(final String mesg, final DialogInterface.OnClickListener okListner) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new AlertDialog.Builder(GroupProfileSettingActivity.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, okListner)
                            .setNegativeButton(android.R.string.cancel, null)
                            .show();
                }
            });
        }catch(WindowManager.BadTokenException e){}
        catch(Exception e){}
    }

    private void sendDeleteGroup() {
        try {

            final String userUUID = DataStorage.getInstance().getUserUUID();
            String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
            String url = ServiceURLManager.getInstance().getGroupDeleteUrl(groupUuid,userUUID);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    String userUuid = DataStorage.getInstance().getUserUUID();
                    String urlList = ServiceURLManager.getInstance().getGroupListUrl(IAPIConstants.API_KEY_GROUP_LIST, userUuid);

                    VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(urlList, false);
                    Intent intent = new Intent(GroupProfileSettingActivity.this, GroupListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_request_failed),Utils.SHORT_TOAST);

                }
            });

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//    private void showProgressBar(final String msg) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                progressDialog = ProgressDialog.show(GroupProfileSettingActivity.this, null, null, true, false);
//                progressDialog.setContentView(R.layout.progressbar);
//                TextView progressBarMessage = (TextView)progressDialog.findViewById(R.id.progressBar_message);
//                progressBarMessage.setText(msg);
//            }
//        });
//
//    }


    private void  download(final CircularImageView holder, String filePath){
        UserFileManager userFileManager = Utils.getInstance().getUserFileManager();
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePath = Utils.getInstance().getLocalContentPath(contentItem.getFilePath());
                holder.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePath, false));

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }

    private void hideProgressBar(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressBar!=null)
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    private void showProgressbar(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressBar!=null)
                    mProgressBar.setVisibility(View.VISIBLE);
            }
        });
    }
    private void sendFollowRequest(final GroupProfile profile,final boolean follow){
        try {
            if(profile == null)
                return;
            showProgressbar();
            final String username = DataStorage.getInstance().getUserUUID();
            final String grouUuid = profile.getUuid();
            String url = ServiceURLManager.getInstance().getGroupFollowRequestUrl(grouUuid,username);
            if(!follow)
                url = ServiceURLManager.getInstance().getGroupUNFollowRequestUrl(grouUuid,username);
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    GroupProfile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), GroupProfile.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        ModelManager.getInstance().setGroupProfile(obj);
                        if (!follow) {
                            mFollowGroup.setText(getResources().getString(R.string.follow_group));
                            mFollowGroup.setVisibility(View.VISIBLE);
                        } else {
                            mFollowGroup.setText(getResources().getString(R.string.action_unfollow));
                            mFollowGroup.setVisibility(View.VISIBLE);
                        }
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_request_failed),Utils.SHORT_TOAST);
//                    Toast.makeText(GroupProfileSettingActivity.this, MyApplication.getAppContext().getString(R.string.error_request_failed),
//                            Toast.LENGTH_SHORT).show();
                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

//    private void showMemberListDialog(){
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                AlertDialog.Builder alertDialog = new AlertDialog.Builder(GroupProfileSettingActivity.this);
//                LayoutInflater inflater = getLayoutInflater();
//                View convertView = inflater.inflate(R.layout.listview_member, null);
//                alertDialog.setView(convertView);
//                alertDialog.setTitle(getString(R.string.prompt_members));
//                ListView lv = (ListView) convertView.findViewById(R.id.list_view);
//                ListMemberAdapter adapter = new ListMemberAdapter(GroupProfileSettingActivity.this,memberList);
//                lv.setAdapter(adapter);
//                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                        sendOwnerChangeRequest(memberList.get(position));
//                    }
//                });
//                alertDialog.show();
//            }
//        });
//
//    }
}
