package net.xvidia.vowmee;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.listadapter.GroupAddMemberListAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.GroupProfile;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;
import net.xvidia.vowmee.helper.WrappingLinearLayoutManager;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class SelectPrivateShareActivity extends AppCompatActivity  {

    private RelativeLayout allFriendsCheckBoxLayout;
    private RelativeLayout allFollowersCheckBoxLayout;
    private CheckBox allFriendsCheckBox;
    private CheckBox allFollowersCheckBox;
    private static LinearLayout selectedMemberLayout;
    private RelativeLayout groupArrow;
    private static TextView selectedGroups;
    private static TextView selectedMembers;

    private static boolean allFriends;
    private static boolean allFollowers;
    private GroupProfile groupProfile;
    private RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    private Toolbar toolbar;
    private static  ArrayList<Profile> memberList;
    private static ArrayList<String> memberUuidList;
    private static ArrayList<String> groupList;
    private TextView mMessageTextView;
    private ProgressBar mProgressBar;

    public static ArrayList<String> getMemberUuidList() {
        return memberUuidList;
    }

    public static ArrayList<String> getGroupList() {
        return groupList;
    }

    public static void setGroupList(ArrayList<String> grouplst) {
        groupList = grouplst;
    }
//    public static void setMemberUuidList(ArrayList<String> memberUuidList) {
//        ShareMomentActivity.memberUuidList = memberUuidList;
//    }

    public static boolean isAllFriends() {
        return allFriends;
    }

    public static boolean isAllFollowers() {
        return allFollowers;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_members);

        allFriendsCheckBoxLayout = (RelativeLayout)findViewById(R.id.friends_checkbox_layout);
        allFollowersCheckBoxLayout = (RelativeLayout)findViewById(R.id.followrs_checkbox_layout);
        allFriendsCheckBox = (CheckBox)findViewById(R.id.friends_checkbox);
        allFollowersCheckBox = (CheckBox)findViewById(R.id.followrs_checkbox);
        selectedMemberLayout = (LinearLayout)findViewById(R.id.selected_member_layout);
        groupArrow = (RelativeLayout)findViewById(R.id.arrow_next);
        selectedGroups = (TextView) findViewById(R.id.selected_group);
        selectedMembers = (TextView) findViewById(R.id.selected_members);
        groupArrow = (RelativeLayout)findViewById(R.id.arrow_next);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar);
        mRecyclerView = (RecyclerView)findViewById(R.id.recycleView_list) ;
        mMessageTextView = (TextView) findViewById(R.id.message_no_results);
        mMessageTextView.setVisibility(View.GONE);
        groupProfile = ModelManager.getInstance().getGroupProfile();
        groupArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mIntent = new Intent(SelectPrivateShareActivity.this, GroupListActivity.class);
                GroupListActivity.groupSelection = true;
                startActivity(mIntent);
            }
        });
        allFriendsCheckBox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                allFriends = !allFriends;
            }
        });

        allFollowersCheckBox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                allFollowers = !allFollowers;
            }
        });
        allFriendsCheckBoxLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                allFriends = !allFriends;
                allFriendsCheckBox.setChecked(allFriends);
            }
        });

        allFollowersCheckBoxLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                allFollowers = !allFollowers;
                allFollowersCheckBox.setChecked(allFollowers);
            }
        });
//        initialiseRecyclerView();
    }

    public static void resetValues(){
        SelectPrivateShareActivity.groupList = new ArrayList<>();
        SelectPrivateShareActivity.memberUuidList = new ArrayList<>();
        allFollowers = false;
        allFollowers = false;
    }

    private void initialiseRecyclerView() {

        RecyclerView.LayoutManager mLayoutManager = new WrappingLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new GroupAddMemberListAdapter(SelectPrivateShareActivity.this, memberList,true);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(false);
    }

    public static void setSelectedMemberList() {
       memberUuidList = new ArrayList<>();

        try {

            Profile obj= null;
            if(GroupAddMemberListAdapter.selectedItems == null){
                return;
            }
            for (int i = 0; i < GroupAddMemberListAdapter.selectedItems.size(); i++)
            {
                int pos =GroupAddMemberListAdapter.selectedItems.keyAt(i);
                obj = memberList.get(pos);
                if(obj != null){
                    memberUuidList.add(obj.getUuid());
                }

            }
            setSelectedView();
        } catch (Exception ex) {
        }
    }
    private void initialiseData() {
        String userName = DataStorage.getInstance().getUsername();
        sendListRequest(IAPIConstants.API_KEY_GET_FRIEND_LIST, userName, userName, "0", "0");
        selectedMemberLayout.setVisibility(View.GONE);
    }

    private static void setSelectedView(){
        try {
            boolean show = false;
            if (memberUuidList != null && memberUuidList.size() > 0) {
                selectedMemberLayout.setVisibility(View.VISIBLE);
                selectedMembers.setText(MyApplication.getAppContext().getString(R.string.member_count, memberUuidList.size()));
                show = true;
            }else{
                selectedMembers.setText(MyApplication.getAppContext().getString(R.string.member_count, 0));

            }

            if (groupList != null && groupList.size() > 0) {
                selectedMemberLayout.setVisibility(View.VISIBLE);
                selectedGroups.setText(MyApplication.getAppContext().getString(R.string.group_count, getGroupList().size()));
                show = true;
            }else{

                selectedGroups.setText(MyApplication.getAppContext().getString(R.string.group_count, 0));
            }
            if (!show)
                selectedMemberLayout.setVisibility(View.GONE);
        }catch (Exception e){

        }

    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        bindActivity();
    }

    private void bindActivity() {
        toolbar.setPadding(0, 0, 0, 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        initialiseData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    @Override
    protected void onResume() {
        setSelectedView();
        setGoogleAnalyticScreen("Private Share");
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_group, menu);
        MenuItem menuAdd = menu.findItem(R.id.action_create_group);
        menuAdd.setVisible(false);
        MenuItem menuNext = menu.findItem(R.id.action_next);
        MenuItemCompat.setActionView(menuNext, R.layout.group_next);
        View viewNext =  MenuItemCompat.getActionView(menuNext);
       Button notifCount = (Button) viewNext.findViewById(R.id.group_next);
        notifCount.setVisibility(View.GONE);
        notifCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectedMemberList();
                finish();
            }
        });

        notifCount.setTextColor(getResources().getColor(R.color.white));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_next:
                setSelectedMemberList();
                finish();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
   private void showError(final String mesg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                new AlertDialog.Builder(SelectPrivateShareActivity.this,R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(mesg)
                        .setPositiveButton(android.R.string.ok, null)
                        .show();
            }
        });
    }


    private void hideProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressBar != null) {
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });
    }
      private void sendFollowingListRequest(int urlKey,final String usernameOwn,final String usernameOther, String page, String size) {
        try {

            String url = ServiceURLManager.getInstance().getListUrl(urlKey, usernameOwn, usernameOther, page, size);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    List<Profile> obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Profile>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {

                            mMessageTextView.setVisibility(View.GONE);
                            if (memberList != null && memberList.size()>0) {
                                memberList.addAll((ArrayList)obj);
                            }else {
                                memberList = (ArrayList) obj;
                            }
                            HashSet<Profile> uniqueValues = new HashSet<>(memberList);
                            memberList = new ArrayList<>();
                            memberList.addAll(uniqueValues);
                            initialiseRecyclerView();
                            if (mRecyclerView != null)
                                mRecyclerView.setVisibility(View.VISIBLE);

                            if (mAdapter != null)
                                mAdapter.notifyDataSetChanged();

                        } else {
                            if (memberList != null && memberList.size()==0) {
                                mMessageTextView.setVisibility(View.VISIBLE);
                                if (mRecyclerView != null)
                                    mRecyclerView.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        if (memberList != null && memberList.size()==0) {
                                showError(getString(R.string.error_general));
                            }
                        }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    if (memberList != null && memberList.size()==0) {

                        showError(getString(R.string.error_general));
                    }
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void sendListRequest(int urlKey,final String usernameOwn,final String usernameOther, String page, String size) {
        try {

            String url = ServiceURLManager.getInstance().getListUrl(urlKey, usernameOwn, usernameOther, page, size);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    List<Profile> obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Profile>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {

                                mMessageTextView.setVisibility(View.GONE);
//                                if (memberList != null && memberList.size()>0) {
//                                    memberList.addAll((ArrayList)obj);
//                                }else {
                                    memberList = (ArrayList) obj;
//                                }
//                                HashSet<Profile> uniqueValues = new HashSet<>(memberList);
//                                memberList = new ArrayList<>();
//                                memberList.addAll(uniqueValues);
                                initialiseRecyclerView();
                                if (mRecyclerView != null)
                                    mRecyclerView.setVisibility(View.VISIBLE);

                                if (mAdapter != null)
                                    mAdapter.notifyDataSetChanged();

                            } else {
                                if (memberList != null && memberList.size()==0) {
                                    mMessageTextView.setVisibility(View.VISIBLE);
                                    if (mRecyclerView != null)
                                        mRecyclerView.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            if (memberList != null && memberList.size()==0) {
//                                showError(getString(R.string.error_general));
                                Utils.getInstance().displayToast(SelectPrivateShareActivity.this,getString(R.string.share_friends),Utils.SHORT_TOAST);
                            }
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        hideProgressBar();
                        if (memberList != null && memberList.size()==0) {

//                            showError(getString(R.string.error_general));
                            Utils.getInstance().displayToast(SelectPrivateShareActivity.this,getString(R.string.share_friends),Utils.SHORT_TOAST);

                        }
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setGoogleAnalyticScreen(String screenName){
        MyApplication application = (MyApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.setClientId(DataStorage.getInstance().getUserUUID());
        mTracker.setAppVersion(""+Utils.getInstance().getAppVersion(MyApplication.getAppContext()));
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

}
