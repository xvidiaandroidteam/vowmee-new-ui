package net.xvidia.vowmee;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.gcm.NotifBuilderSingleton;
import net.xvidia.vowmee.listadapter.GroupsGridAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.GroupProfile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class GroupListActivity extends AppCompatActivity {

    private ArrayList<GroupProfile> groupList;
    private static final float MILLISECONDS_PER_INCH = 50f;
//    private GridView gridView;
    private GroupsGridAdapter mAdapter;
    private View mProgressView;
    private TextView mMessageTextView;
    private RecyclerView mRecyclerView;
    private static MenuItem menuNext;
    private static Button next;
    private static MenuItem menuCreate;
    public static boolean groupSelection =false;
    private boolean notificationIntent;
    private int notificationId, idInboxStyle;

    private GridLayoutManager mLayoutManager;
//    private SwipeRefreshLayout swipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_layout);
        mProgressView = findViewById(R.id.loading_progress);
        mMessageTextView = (TextView) findViewById(R.id.message_no_moments);
        mRecyclerView = (RecyclerView) findViewById(R.id.group_recycler_view);
        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            notificationIntent = intent.getBooleanExtra(AppConsatants.NOTIFICATION_NEXT_INTENT, false);
            notificationId = intent.getIntExtra(AppConsatants.NOTIFICATION_ID_INTENT, 0);
            idInboxStyle = intent.getIntExtra(AppConsatants.IDINBOXSTYLE, 0);
        }
        if(notificationIntent) {
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancel(notificationId);
            NotifBuilderSingleton.getInstance().resetInboxStyle(idInboxStyle);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        bindActivity();
    }

    private void bindActivity() {

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.action_group));
        mToolbar.setPadding(0, 0, 0, 0);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        groupList = new ArrayList<>();
        mMessageTextView.setVisibility(View.GONE);
        showProgress(true);
        getGroupList(true);
//        initialiseRecyclerView();
    }

    private void initialiseRecyclerView(){

//        swipeRefreshLayout.setRefreshing(false);

        mAdapter = new GroupsGridAdapter(GroupListActivity.this, groupList,groupSelection);
        mLayoutManager = new GridLayoutManager(GroupListActivity.this, 2){
            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView,
                                               RecyclerView.State state, final int position) {

                LinearSmoothScroller smoothScroller =
                        new LinearSmoothScroller(GroupListActivity.this) {

                            //This controls the direction in which smoothScroll looks
                            //for your view
                            @Override
                            public PointF computeScrollVectorForPosition
                            (int targetPosition) {
                                return computeScrollVectorForPosition(targetPosition);
                            }

                            //This returns the milliseconds it takes to
                            //scroll one pixel.
                            @Override
                            protected float calculateSpeedPerPixel
                            (DisplayMetrics displayMetrics) {
                                return MILLISECONDS_PER_INCH/displayMetrics.densityDpi;
                            }
                        };

                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }
        };
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
         if(notificationIntent) {
            Intent intent = new Intent(GroupListActivity.this, TimelineTabs.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
//        if(!groupSelection) {
//            Intent intent = new Intent(GroupListActivity.this, TimelineTabs.class);
////            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//        }
            groupSelection =false;
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        getGroupList(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_group, menu);
        menuCreate = menu.findItem(R.id.action_create_group);
        menuNext = menu.findItem(R.id.action_next);

        MenuItemCompat.setActionView(menuNext, R.layout.group_next);
        View viewNext =  MenuItemCompat.getActionView(menuNext);
        next = (Button) viewNext.findViewById(R.id.group_next);
        next.setTextColor(getResources().getColor(R.color.white));
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectedMemberList();
            }
        });
        showMenuIcon();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create_group:
                Intent mIntent = new Intent(GroupListActivity.this, CreateNewGroupActivity.class);
                startActivity(mIntent);
                break;
            case R.id.action_next:
                setSelectedMemberList();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
    private void getGroupList(final boolean refresh) {
        try {


            final String username = DataStorage.getInstance().getUserUUID();
            String url = ServiceURLManager.getInstance().getGroupListUrl(IAPIConstants.API_KEY_GROUP_LIST, username);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<GroupProfile> obj = null;
                    try {
//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj = mapper.readValue(response.toString(), new TypeReference<List<GroupProfile>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    showProgress(false);
                    if (obj != null) {

                        if (obj.size() > 0) {
                            groupList = (ArrayList<GroupProfile>)obj;
                            mMessageTextView.setVisibility(View.GONE);
                            if(refresh) {
                                initialiseRecyclerView();
                                mAdapter.notifyDataSetChanged();
                            }
                            mRecyclerView.setVisibility(View.VISIBLE);
                        }else{
                            mMessageTextView.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);

                        }
                    }else{
                        mMessageTextView.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
//                        showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                            }
//                        });
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    showProgress(false);
                    if(groupList != null && groupList.size() > 0){
                        mMessageTextView.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                    }else{
                        mMessageTextView.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    }
//                    showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
                }
            })
            {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        //                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void setSelectedMemberList() {
        ArrayList<String> memberList = new ArrayList<String>();
        try {

            GroupProfile obj= null;
            if(GroupsGridAdapter.selectedItems == null){
                return;
            }
            for (int i = 0; i < GroupsGridAdapter.selectedItems.size(); i++)
            {
                int pos =GroupsGridAdapter.selectedItems.keyAt(i);
                obj = groupList.get(pos);
                if(obj != null){
                    memberList.add(obj.getUuid());
                }

            }
            if(groupSelection) {
                SelectPrivateShareActivity.setGroupList(memberList);
                 finish();
            }
        } catch (Exception ex) {
        }
    }

    private void showError(final String message,final DialogInterface.OnClickListener okClicked) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(GroupListActivity.this,R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, okClicked)
                        .show();
            }
        });
    }

    private void showProgress(final boolean show) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                Animation rotation = AnimationUtils.loadAnimation(GroupListActivity.this, R.anim.rotate);
//                rotation.setRepeatCount(Animation.INFINITE);
                if (show) {
//                    mProgressView.startAnimation(rotation);
                    mProgressView.setVisibility(View.VISIBLE);
                } else {
//                    mProgressView.clearAnimation();
                    mProgressView.setVisibility(View.GONE);
                }
            }
        });
    }



    private void showMenuIcon() {
        if (groupSelection) {
            if (menuNext != null && menuCreate != null) {
                menuNext.setVisible(true);
                menuCreate.setVisible(false);
                next.setVisibility(View.VISIBLE);
            }
        } else {
            if (menuNext != null && menuCreate != null) {
                menuCreate.setVisible(true);
                menuNext.setVisible(false);
                next.setVisibility(View.GONE);
            }

        }

    }

//    @Override
//    public void onRefresh() {
//        getGroupList();
////        swipeRefreshLayout.setRefreshing(false);
//    }
}
