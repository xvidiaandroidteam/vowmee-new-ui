package net.xvidia.vowmee;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.gcm.NotifBuilderSingleton;
import net.xvidia.vowmee.helper.CustomFontEditTextView;
import net.xvidia.vowmee.listadapter.GroupPendingRequestListAdapter;
import net.xvidia.vowmee.listadapter.PendingRequestListAdapter;
import net.xvidia.vowmee.listadapter.RecyclerViewScrollListener;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.FriendRequest;
import net.xvidia.vowmee.network.model.GroupMembership;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

public class PendingRequestActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private Context context;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    private List<Profile> pendingList;
    private List<GroupMembership> groupPendingList;
    private ProgressBar progressBar;
    private boolean groupPending;
    private boolean notificationIntent;
    private String groupUuid;
    private int notificationId, idInboxStyle;
    private boolean acceptActivity;
    private boolean declineActivity;
    private String friendUsername;
    private Toolbar toolbar;
    private TextView mMessageTextView;
    private CustomFontEditTextView mSearchQueryTextView;

    private String quesryString;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mMessageTextView = (TextView) findViewById(R.id.message_no_results);
        mMessageTextView.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView_list);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        mRecyclerView.setHasFixedSize(true);

        initialiseRecyclerView();
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.init_gradient, R.color.com_facebook_blue, R.color.red_color);
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        context = PendingRequestActivity.this;
        toolbar.setPadding(0, 0, 0, 0);
        mMessageTextView = (TextView) findViewById(R.id.message_no_results);
        mSearchQueryTextView = (CustomFontEditTextView) findViewById(R.id.search_query);
        mSearchQueryTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                quesryString = s.toString().toLowerCase();

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (quesryString != null) {
                    if (quesryString.length() > 2) {

                        if (!groupPending) {
                            final List<Profile> filteredList = new ArrayList<>();

                            for (int i = 0; i < pendingList.size(); i++) {

                                final String text = pendingList.get(i).toString().toLowerCase();
                                if (text.contains(quesryString)) {

                                    filteredList.add(pendingList.get(i));
                                }
                            }
                            mAdapter = new PendingRequestListAdapter(PendingRequestActivity.this, filteredList, true);

                        } else {
                            final List<GroupMembership> filteredList = new ArrayList<>();

                            for (int i = 0; i < groupPendingList.size(); i++) {

                                final String text = groupPendingList.get(i).toString().toLowerCase();
                                if (text.contains(quesryString)) {

                                    filteredList.add(groupPendingList.get(i));
                                }
                            }
                            mAdapter = new GroupPendingRequestListAdapter(PendingRequestActivity.this, filteredList);
                        }
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();  // data set changed


                    } else {
                        if (!groupPending) {
                            mAdapter = new PendingRequestListAdapter(PendingRequestActivity.this, pendingList, true);

                        } else {
                            mAdapter = new GroupPendingRequestListAdapter(PendingRequestActivity.this, groupPendingList);
                        }
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                    }
//                    sendSearchRequest(quesryString);
                }
            }
        });
        mSearchQueryTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Utils.getInstance().hideKeyboard(PendingRequestActivity.this, mSearchQueryTextView);

                    return true;
                }
                return false;
            }
        });
        mMessageTextView.setVisibility(View.GONE);

        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            groupUuid = intent.getStringExtra(AppConsatants.UUID);
            groupPending = intent.getBooleanExtra(AppConsatants.GROUP_ROLE_MODERATOR, false);
            notificationIntent = intent.getBooleanExtra(AppConsatants.NOTIFICATION_NEXT_INTENT, false);
            friendUsername = intent.getStringExtra(AppConsatants.NOTIFICATION_FRIEND_USERNAME);
            acceptActivity = intent.getBooleanExtra(AppConsatants.NOTIFICATION_ACCEPT_INTENT, false);
            declineActivity = intent.getBooleanExtra(AppConsatants.NOTIFICATION_DECLINE_INTENT, false);
            notificationId = intent.getIntExtra(AppConsatants.NOTIFICATION_ID_INTENT, 0);
            idInboxStyle = intent.getIntExtra(AppConsatants.IDINBOXSTYLE, 0);
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        showProgressBar();
        initialiseData();
        if(notificationIntent) {
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancel(notificationId);
            NotifBuilderSingleton.getInstance().resetInboxStyle(idInboxStyle);
        }
    }

    private void initialiseData() {
        getSupportActionBar().setTitle(getString(R.string.title_friends));
        if (notificationIntent && acceptActivity) {
            if (groupPending) {

            } else {
                if (progressBar != null)
                    progressBar.setVisibility(View.VISIBLE);
                sendFriendsAcceptRequest(friendUsername);
            }
        } else if (notificationIntent && declineActivity) {
            if (groupPending) {

            } else {
                if (progressBar != null)
                    progressBar.setVisibility(View.VISIBLE);
                sendFriendsDenyRequest(friendUsername);
            }
        } else if (!groupPending) {
            sendPendingListRequest(IAPIConstants.API_KEY_GET_PENDING_FRIENDS_LIST, DataStorage.getInstance().getUsername(), DataStorage.getInstance().getUsername(), "0", "0", false);
        } else {
            getSupportActionBar().setTitle(getString(R.string.add_member));
            sendGroupPendingListRequest();
        }
    }

    private void initialiseRecyclerView() {
        swipeRefreshLayout.setRefreshing(false);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        if (!groupPending) {
            mAdapter = new PendingRequestListAdapter(PendingRequestActivity.this, pendingList, false);
        } else {
            mAdapter = new GroupPendingRequestListAdapter(PendingRequestActivity.this, groupPendingList);
        }
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerViewScrollListener(this) {
            @Override
            public void onMoved(int distance) {
                toolbar.setTranslationY(-distance);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    private void sendGroupPendingListRequest() {
        try {
            String userUUID = DataStorage.getInstance().getUserUUID();
            String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();

            String url = ServiceURLManager.getInstance().getGroupJoinPendingRequestUrl(groupUuid, userUUID);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    List<GroupMembership> obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<List<GroupMembership>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {
//                            if (followPending) {
//                                ModelManager.getInstance().setPendingFollowList(obj);
//                            } else {
//                                ModelManager.getInstance().setPendingFriendList(obj);
//                            }
                            mMessageTextView.setVisibility(View.GONE);
                            if (groupPendingList != null)
                                groupPendingList.clear();
                            groupPendingList = obj;
                            initialiseRecyclerView();

                            if (mRecyclerView != null)
                                mRecyclerView.setVisibility(View.VISIBLE);

                            if (mAdapter != null)
                                mAdapter.notifyDataSetChanged();
//                            if(notificationIntent && acceptActivity){
//                                sendFriendsAcceptRequest(friendUsername);
//                            }else if(notificationIntent && declineActivity){
//
//                                sendFriendsDenyRequest(friendUsername);
//                            }else{

//                            }
                        } else {
                            mMessageTextView.setVisibility(View.VISIBLE);
                            if (mRecyclerView != null)
                                mRecyclerView.setVisibility(View.VISIBLE);

                        }

                    } else {
                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                    }
                }

            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null && error.networkResponse != null) {
                        //Log.e("VolleyError ", "" + error.networkResponse.statusCode);
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND) {
//                            Toast.makeText(context, "Please try again",
//                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
//                        Toast.makeText(context, "Please try again",
//                                Toast.LENGTH_SHORT).show();
                    }
                    hideProgressBar();
                    showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendPendingListRequest(int urlKey, String usernameOwn, String usernameOther, String page, String size, final boolean followPending) {
        try {

            String url = ServiceURLManager.getInstance().getListUrl(urlKey, usernameOwn, usernameOther, page, size);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<Profile> obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Profile>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {
                            if (followPending) {
                                ModelManager.getInstance().setPendingFollowList(obj);
                            } else {
                                ModelManager.getInstance().setPendingFriendList(obj);
                            }
                            mMessageTextView.setVisibility(View.GONE);
                            if (pendingList != null)
                                pendingList.clear();
                            pendingList = obj;
                            initialiseRecyclerView();

                            if (mRecyclerView != null)
                                mRecyclerView.setVisibility(View.VISIBLE);

                            if (mAdapter != null)
                                mAdapter.notifyDataSetChanged();

                            hideProgressBar();
                        } else {
                            onBackPressed();
                        }
                    } else {
                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                onBackPressed();
                            }
                        });
                    }
                }

            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            onBackPressed();
                        }
                    });
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFriendsDenyRequest(final String friendUsername) {
        try {
            if (friendUsername == null)
                return;
            if (friendUsername.isEmpty())
                return;
//            showProgressBar("Processing your request");
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_DENY_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(username);
            obj.setRequestingPersonUsername(friendUsername);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Log.i("Friend req object ", "" +obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
//                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        ModelManager.getInstance().setOwnProfile(obj);

                    }
                    sendPendingListRequest(IAPIConstants.API_KEY_GET_PENDING_FRIENDS_LIST, DataStorage.getInstance().getUsername(), DataStorage.getInstance().getUsername(), "0", "0", false);

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    sendPendingListRequest(IAPIConstants.API_KEY_GET_PENDING_FRIENDS_LIST, DataStorage.getInstance().getUsername(), DataStorage.getInstance().getUsername(), "0", "0", false);

                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFriendsAcceptRequest(final String friendUsername) {
        try {
            if (friendUsername == null)
                return;
            if (friendUsername.isEmpty())
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_ACCEPT_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(friendUsername)) {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_request_failed), Utils.LONG_TOAST);

//                Toast.makeText(context, MyApplication.getAppContext().getString(R.string.error_request_failed),
//                        Toast.LENGTH_SHORT).show();
                return;
            }

//            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(username);
            obj.setRequestingPersonUsername(friendUsername);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
//            //Log.i("Friend req object ", "" +obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
//                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        ModelManager.getInstance().setOwnProfile(obj);

                    }
                    sendPendingListRequest(IAPIConstants.API_KEY_GET_PENDING_FRIENDS_LIST, DataStorage.getInstance().getUsername(), DataStorage.getInstance().getUsername(), "0", "0", false);

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    sendPendingListRequest(IAPIConstants.API_KEY_GET_PENDING_FRIENDS_LIST, DataStorage.getInstance().getUsername(), DataStorage.getInstance().getUsername(), "0", "0", false);

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showError(final String mesg, final DialogInterface.OnClickListener okClicked) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(PendingRequestActivity.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .show();
                }
            });
        } catch (WindowManager.BadTokenException e) {
        } catch (Exception e) {
        }
    }

    private void showProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });

    }

    private void hideProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (notificationIntent) {
            Intent intent = new Intent(context, TimelineTabs.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            finish();
        }
//        super.onBackPressed();
    }

    private void sendRequest(String groupUuid, String requesterUuid, boolean accept) {
        try {
            if (groupUuid.isEmpty() || requesterUuid.isEmpty())
                return;
            showProgressBar();
            final String userUuid = DataStorage.getInstance().getUsername();
//            String groupUuid = profile.getGroup().getUuid();
            String url = ServiceURLManager.getInstance().getGroupJoinDenyUrl(groupUuid, requesterUuid, userUuid);
            if (accept) {
                url = ServiceURLManager.getInstance().getGroupJoinAcceptUrl(groupUuid, requesterUuid, userUuid);
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_request_failed), Utils.LONG_TOAST);

//                    Toast.makeText(context, "Request failed",
//                            Toast.LENGTH_SHORT).show();
                    hideProgressBar();
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
        initialiseData();
    }
}
