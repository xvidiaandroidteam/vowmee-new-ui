package net.xvidia.vowmee;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.CheckNetworkConnection;
import net.xvidia.vowmee.Utils.GPSTracker;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.UploadffmpegService;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.network.model.MomentShare;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

public class ShareIntentActivity extends AppCompatActivity  implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final int REQUEST_CHECK_SETTINGS =123 ;
    private EditText mCaptionEditText;
    private EditText mDescriptionAutoCompleteTextView;
    private LinearLayout mAudienceLayout;
    private LinearLayout mLocationLayout;
//    private CheckBox publicCheckBox;
//    private CheckBox privateCheckBox;

    private TextView privacySwitchText;
    private boolean privateFlag;
    private SwitchCompat locationSwitch;
    private SwitchCompat privacySwitch;
    private ImageView mPreviewVideoPlayer;
    private ImageView mPreviewVideoPlayerBg;
    private String path;
    private Moment moment;
    TextView upload;
    private Double latitude;
    private Double longitude;
    private GoogleApiClient googleApiClient;
    private GPSTracker gps;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        getSupportActionBar().setTitle(R.string.title_share);
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if (type.startsWith("video/")) {
                handleSendImage(intent); // Handle single video being sent
            } else {
                finish();
            }
        }
        if (DataStorage.getInstance().getUsername().isEmpty()) {
            Intent mIntent = new Intent(ShareIntentActivity.this, RegisterActivity.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(mIntent);
            finish();
        }

        latitude = 0d;
        longitude = 0d;
        MomentShare momemtShare = new MomentShare();
        UploadffmpegService.setMomentShare(momemtShare);

        SelectPrivateShareActivity.resetValues();
        moment = new Moment();
        initialiseViews();


    }
    void handleSendImage(Intent intent) {
        Uri selectedImageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (selectedImageUri != null) {
            path = Utils.getInstance().getPath(this, selectedImageUri, false);
            if(!path.isEmpty()) {

            }else{
                showAlert();
            }

        }
    }


    private void attemptUpload() {
        // Reset errors.
        mCaptionEditText.setError(null);
        String caption = mCaptionEditText.getText().toString();
        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(caption)) {
            mCaptionEditText.setError(getString(R.string.error_field_required));
            focusView = mCaptionEditText;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        }else{
            uploadMoment();
        }
    }
    private void initialiseViews(){
        mCaptionEditText = (EditText)findViewById(R.id.moment_caption);
        mDescriptionAutoCompleteTextView = (EditText)findViewById(R.id.moment_description);
        mPreviewVideoPlayer = (ImageView) findViewById(R.id.preview_videoplayer);
        mPreviewVideoPlayerBg = (ImageView) findViewById(R.id.preview_videoplayerBg);
        locationSwitch = (SwitchCompat)findViewById(R.id.switch_location);
        privacySwitch = (SwitchCompat)findViewById(R.id.switch_privacy);
        privacySwitchText = (TextView) findViewById(R.id.switch_privacy_text);
        mAudienceLayout = (LinearLayout) findViewById(R.id.audience_layout);
        mLocationLayout = (LinearLayout)findViewById(R.id.location_layout);
//        publicCheckBox = (CheckBox)findViewById(R.id.public_checkbox);
//        privateCheckBox = (CheckBox)findViewById(R.id.private_checkbox);
        upload = (TextView) findViewById(R.id.button_save);
        upload.setVisibility(View.INVISIBLE);
        gps = new GPSTracker(ShareIntentActivity.this);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.getInstance().hideKeyboard(ShareIntentActivity.this, mCaptionEditText);
                Utils.getInstance().hideKeyboard(ShareIntentActivity.this, mDescriptionAutoCompleteTextView);
                attemptUpload();
//                uploadMomentNew();
//                uploadMoment();
            }
        });
//        compressedFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + AppConsatants.FILE_VOWMEE;
////        vkLogPath = workFolder + "vk.log";
//        compressedFilePath = compressedFilePath + "compressed/";
        Bitmap image = Utils.getInstance().getVideoThumbnail(path);
        mPreviewVideoPlayer.setImageBitmap(image);
        mPreviewVideoPlayerBg.setImageBitmap(image);
        mPreviewVideoPlayerBg.setImageAlpha(80);

        mPreviewVideoPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Uri fileUri = Uri.parse(path);
//                Intent intent = new Intent(Intent.ACTION_VIEW, fileUri);
//                intent.setDataAndType(fileUri, "video/*");
//                startActivity(intent);

                Intent mIntent = new Intent(ShareIntentActivity.this, TimelineMomentDetail.class);
                Bundle mBundle = new Bundle();
                mBundle.putString(AppConsatants.UUID, path);
                mBundle.putBoolean(AppConsatants.UPLOAD_FILE_PATH, true);
                mIntent.putExtras(mBundle);
                startActivity(mIntent);
            }
        });

       /* publicCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                publicCheckBox.setChecked(isChecked);
                privateCheckBox.setChecked(!isChecked);
                privateFlag = !isChecked;

            }
        });*/

        /*privateCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                privateFlag = isChecked;
                publicCheckBox.setChecked(!isChecked);
                privateCheckBox.setChecked(isChecked);
                if(privateFlag){
                    Intent mIntent = new Intent(UploadActivity.this, ShareMomentActivity.class);
//                    Bundle mBundle = new Bundle();
//                    mBundle.putString(AppConsatants.MOMENT_PATH, path);
//                    mBundle.putString(AppConsatants.UUID, groupUuid);
//                    mBundle.putBoolean(AppConsatants.UPLOAD_MOMENT_GROUP, groupMoment);
//                    mBundle.putBoolean(AppConsatants.RESHARE, false);
//                    mIntent.putExtras(mBundle);
                    startActivity(mIntent);
//                    finish();
                }
            }
        });*/
        locationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (CheckNetworkConnection
                        .isConnectionAvailable(getApplicationContext()) && isChecked) {
                    if (gps.canGetLocation()) {
                        Location l = gps.getLocation();

                        if (l != null) {
                            latitude = l.getLatitude();
                            longitude = l.getLongitude();
//                            Log.i("geo location", " latitude " + latitude + " longitude" + longitude);
                        }
                    } else {
                        location();
                    }
                }
            }
        });

        if(gps.canGetLocation()){
            locationSwitch.setChecked(true);
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        }else{
            locationSwitch.setChecked(false);
        }
        privacySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                privateFlag = !isChecked;
                if(privateFlag){
                    privacySwitchText.setText(getString(R.string.prompt_private));
                    Intent mIntent = new Intent(ShareIntentActivity.this, SelectPrivateShareActivity.class);
                    startActivity(mIntent);
                }else{
                    privacySwitchText.setText(getString(R.string.prompt_public));
                }
            }
        });
        privacySwitch.setChecked(true);
        privacySwitchText.setText(getString(R.string.prompt_public));
        privateFlag = false;
        mCaptionEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        mDescriptionAutoCompleteTextView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        upload.setVisibility(View.VISIBLE);

            mLocationLayout.setVisibility(View.VISIBLE);
            mAudienceLayout.setVisibility(View.VISIBLE);

        getProfileRequest();
    }

    private void uploadMoment() {
        try {
            moment.setUuid("");
            moment.setCaption(mCaptionEditText.getText().toString());
            moment.setDescription(mDescriptionAutoCompleteTextView.getText().toString());
            moment.setIsLive(false);
            moment.setLatitude(latitude);
            moment.setLongitude(longitude);
            moment.setOwner(DataStorage.getInstance().getUsername());
            if(privateFlag)
                moment.setProfile(AppConsatants.PROFILE_PRIVATE);
            else
                moment.setProfile(AppConsatants.PROFILE_PUBLIC);

            UploadffmpegService.getMomentShare().setFollowers(SelectPrivateShareActivity.isAllFollowers());
            UploadffmpegService.getMomentShare().setFriends(SelectPrivateShareActivity.isAllFriends());
            UploadffmpegService.getMomentShare().setGroupUuids(SelectPrivateShareActivity.getGroupList());
            UploadffmpegService.getMomentShare().setUserUuids(SelectPrivateShareActivity.getMemberUuidList());
            if(gps != null)
                gps.stopUsingGPS();

            UploadffmpegService.getMomentShare().setNewMoment(moment);
            Intent mIntent = new Intent(this, UploadffmpegService.class);
            Bundle mBundle = new Bundle();
            mBundle.putString(AppConsatants.UPLOAD_FILE_CAPTION, mCaptionEditText.getText().toString());
            mBundle.putString(AppConsatants.UPLOAD_FILE_DESCRIPTION, mDescriptionAutoCompleteTextView.getText().toString());
            mBundle.putString(AppConsatants.UPLOAD_FILE_PATH,path);
            mBundle.putBoolean(AppConsatants.UPLOAD_MOMENT_GROUP,false);
            mBundle.putBoolean(AppConsatants.RESHARE, false);
            mIntent.putExtras(mBundle);
            startService(mIntent);
            onBackPressed();

        } catch (Exception e) {

        }
    }


    private void getProfileRequest(){
        try {
            final String username = DataStorage.getInstance().getUsername();
            String url =ServiceURLManager.getInstance().getProfileUrl(IAPIConstants.API_KEY_GET_PROFILE, username, username);

            JsonObjectRequest  request= new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    String getUsername = obj.getUsername();
                    if(getUsername != null && !getUsername.isEmpty() && username.equals(getUsername)) {
                        ModelManager.getInstance().setOwnProfile(obj);
                    }else{

                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){

        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent mIntent = new Intent(ShareIntentActivity.this, TimelineTabs.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mIntent);
        finish();
    }

    @Override
    protected void onResume() {
        if(!SelectPrivateShareActivity.isAllFollowers()
                &&!SelectPrivateShareActivity.isAllFriends()
                && SelectPrivateShareActivity.getGroupList().size()==0
                && SelectPrivateShareActivity.getMemberUuidList().size()==0){
            privacySwitch.setChecked(true);
            privacySwitchText.setText(getString(R.string.prompt_public));
            privateFlag = false;
        }
        super.onResume();
    }

    private void location() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(ShareIntentActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            settingsrequest();
        }
    }

    public void settingsrequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
// All location settings are satisfied. The client can initialize location
// requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
// Location settings are not satisfied. But could be fixed by showing the user
// a dialog.
                        try {
// Show the dialog by calling startResolutionForResult(),
// and check the result in onActivityResult().
                            status.startResolutionForResult(ShareIntentActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
// Ignore the error.
                        }catch (Exception E){

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
// Location settings are not satisfied. However, we have no way to fix the
// settings so we won't show the dialog.
                        break;
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
// Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        if(gps!=null &&gps.canGetLocation()){
                            latitude= gps.getLatitude();
                            longitude= gps.getLongitude();
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        if(locationSwitch!=null)
                            locationSwitch.setChecked(false);
//                        settingsrequest();//keep asking if imp or do whatever
                        break;
                }
                break;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private void checkVideoValidSize(){
        if(path !=null && !path.isEmpty()) {
            File file = new File(path);
            if (file.exists()) {
//                MediaMetadataRetriever fetchVideoInfo = new MediaMetadataRetriever();
//                fetchVideoInfo.setDataSource(path);
//                String bitrate = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE);
//                String duration = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//                int defaultBitrate = 400;
//                int intBitrate = 0, intDuration = 0;
                long compressedVideoSize;
//                if (bitrate != null) {
//                    intBitrate = Integer.parseInt(bitrate);
//                    intBitrate = intBitrate / 8192; //converting bits/sec to kb/sec
//                    if (intBitrate >= defaultBitrate) {
//                        intBitrate = defaultBitrate;
//                    }
//                }
//                if (duration != null) {
//                    intDuration = Integer.parseInt(duration);
//                    intDuration = intDuration / 1000; // converting ms to sec
//                }
                compressedVideoSize = file.length();
                if(compressedVideoSize>0) {
                    compressedVideoSize = compressedVideoSize/1024; // kb
                    compressedVideoSize = compressedVideoSize/1024;//mb
//                    compressedVideoSize = intBitrate * 8 * intDuration;
                    if (compressedVideoSize > 20) {
                        showAlert();
                    }
                }
            }
        }
    }

    private void showAlert() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(ShareIntentActivity.this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Large file")
                            .setMessage(getString(R.string.error_large_file_upload))
                            .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
//                                    finish();
                                }

                            })
                            .show();
                }
            });

        }catch(WindowManager.BadTokenException e){

        }catch (Exception e){}
    }
}
