package net.xvidia.vowmee;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.network.model.GroupProfile;
import net.xvidia.vowmee.storage.DataStorage;
import net.xvidia.vowmee.helper.CircularImageView;

public class CreateNewGroupActivity extends AppCompatActivity  {
    Button notifCount;
    private EditText mGroupNameEditText;
    private String quesryString="";
    private CircularImageView groupImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_group_name);
        mGroupNameEditText = (EditText)findViewById(R.id.new_groupname);
        groupImage = (CircularImageView)findViewById(R.id.group_image);
       groupImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(CreateNewGroupActivity.this, CropGroupImageActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, true);
                mIntent.putExtras(mBundle);
                mIntent.putExtras(mBundle);
                startActivity(mIntent);
            }
        });
        mGroupNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                quesryString = s.toString();


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!quesryString.isEmpty()) {
                    if (quesryString.length() > 2) {
                        if (notifCount != null) {
                            notifCount.setVisibility(View.VISIBLE);
                            notifCount.setEnabled(true);
                            notifCount.setTextColor(getResources().getColor(R.color.white));
                        }
                    } else {
                        if (notifCount != null) {
                            notifCount.setVisibility(View.VISIBLE);
                            notifCount.setEnabled(false);
                            notifCount.setTextColor(getResources().getColor(R.color.white_translucent));
                        }
                    }
                } else {
                    if (notifCount != null) {
                        notifCount.setVisibility(View.VISIBLE);
                        notifCount.setEnabled(false);
                        notifCount.setTextColor(getResources().getColor(R.color.white_translucent));
                    }
                }
            }
        });
        mGroupNameEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Utils.getInstance().hideKeyboard(CreateNewGroupActivity.this, mGroupNameEditText);
                    return true;
                }
                return false;
            }
        });
//        updateImage();

        final String uuidString = Utils.getInstance().generateUUIDString();
        CropGroupImageActivity.groupImage= uuidString+AppConsatants.FILE_EXTENSION_JPG;
        DataStorage.getInstance().setGroupImagePath("");
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        bindActivity();
    }
    private void updateImage(){
    if(groupImage == null)
        return;
    Bitmap image = null;
    if(CropGroupImageActivity.groupImage !=null && !CropGroupImageActivity.groupImage.isEmpty())
    image = Utils.getInstance().loadImageFromStorage(Utils.getInstance().getLocalImagePath(CropGroupImageActivity.groupImage), false);
    if(image != null) {
        groupImage.setImageBitmap(image);
    }else {
        groupImage.setImageResource(R.drawable.group_default);
    }
}
    private void bindActivity() {

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.action_create_group));
        mToolbar.setPadding(0, Utils.getInstance().getStatusBarHeight(this), 0, 0);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateImage();
        setGoogleAnalyticScreen("Create Group");
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_group_next, menu);
        if (menu != null) {
            MenuItem menuNext = menu.findItem(R.id.action_create_group_next);
//            menuNext = menu.findItem(R.id.action_create_group_next);
            MenuItemCompat.setActionView(menuNext, R.layout.group_next);
            View viewNext =  MenuItemCompat.getActionView(menuNext);

//            View viewNext =menuNext.getActionView();
            notifCount = (Button) viewNext.findViewById(R.id.group_next);
            notifCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!quesryString.isEmpty()) {
                        if (quesryString.length() > 2) {
                            GroupProfile profile = new GroupProfile();
                            profile.setName(quesryString);
                            profile.setOwnerUuid(DataStorage.getInstance().getUserUUID());
                            profile.setCreatorUuid(DataStorage.getInstance().getUserUUID());
                            profile.setProfileImage(CropGroupImageActivity.groupImage);
                            String thumbnail = CropGroupImageActivity.groupImage;
                            thumbnail = thumbnail.replace(AppConsatants.FILE_EXTENSION_JPG, AppConsatants.THUMBNAIL_EXTENSION_JPG);
                            profile.setThumbNail(thumbnail);
                            GroupSettingActivity.setGroupProfile(profile);
                            Intent mIntent = new Intent(CreateNewGroupActivity.this, GroupAddMemberActivity.class);
                            Bundle mBundle = new Bundle();
                            mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, true);
                            mIntent.putExtras(mBundle);
                            startActivity(mIntent);
                        }
                    }
                }
            });

            notifCount.setEnabled(false);
            notifCount.setTextColor(getResources().getColor(R.color.white_disabled_menu));
            if (!quesryString.isEmpty()) {
                if (quesryString.length() > 2) {
                    if (notifCount != null) {
                        notifCount.setVisibility(View.VISIBLE);
                        notifCount.setEnabled(true);
                        notifCount.setTextColor(getResources().getColor(R.color.white));
                    }

                } else {
                    if(notifCount != null) {
                        notifCount.setEnabled(false);
                        notifCount.setVisibility(View.VISIBLE);
                        notifCount.setTextColor(getResources().getColor(R.color.white_disabled_menu));
                    }
                }
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
      /*  switch (item.getItemId()) {
            case R.id.action_create_group_next:
                GroupProfile profile = new GroupProfile();
                profile.setName(quesryString);
                profile.setOwnerUuid(DataStorage.getInstance().getUserUUID());
                profile.setCreatorUuid(DataStorage.getInstance().getUserUUID());
                profile.setProfileImage(CropGroupImageActivity.groupImage);
                String thumbnail =CropGroupImageActivity.groupImage;
                if(thumbnail.contains(AppConsatants.FILE_EXTENSION_JPG)){
                    thumbnail = thumbnail.replace(AppConsatants.FILE_EXTENSION_JPG,AppConsatants.THUMBNAIL_EXTENSION_JPG);
                }
                profile.setThumbNail(thumbnail);
                GroupSettingActivity.setGroupProfile(profile);
                Intent mIntent = new Intent(CreateNewGroupActivity.this, GroupAddMemberActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, true);
                mIntent.putExtras(mBundle);
                startActivity(mIntent);
                break;

            default:
                break;
        }*/

        return super.onOptionsItemSelected(item);
    }

    private void setGoogleAnalyticScreen(String screenName){
        MyApplication application = (MyApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


}
