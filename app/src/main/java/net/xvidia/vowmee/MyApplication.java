package net.xvidia.vowmee;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.amazonaws.mobile.user.signin.CognitoSyncClientManager;
import com.crashlytics.android.Crashlytics;
import com.digits.sdk.android.Digits;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;

public class MyApplication extends MultiDexApplication {



    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
//    private static final String TWITTER_KEY = "YpbvFU7nX5dTEgem7OEcHndex";
//    private static final String TWITTER_SECRET = "rSEq0l8cu4dFOqESVnYOsDwHqeRRdSQS3T8NEJTlg7duwDrNUf";


    private static Context context;

    private Tracker mTracker;

    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

        CognitoSyncClientManager.init(this);
        String consumerKey = context.getResources().getString(R.string.twitter_consumer_key);
            String consumerSecret = context.getResources().getString(R.string.twitter_secret_key);

        TwitterAuthConfig authConfig = new TwitterAuthConfig(consumerKey, consumerSecret);
        Fabric.with(this, new Digits(), new Twitter(authConfig), new Crashlytics());

    }

    @Override
    public void attachBaseContext(Context base) {
        MultiDex.install(base);
        super.attachBaseContext(base);
    }
    public static Context getAppContext() {
        return context;
    }


    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }
}
