package net.xvidia.vowmee;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.mobile.AWSConfiguration;
import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.mobile.content.UserFileManager;
import com.amazonaws.mobile.user.IdentityManager;
import com.amazonaws.mobile.user.IdentityProvider;
import com.amazonaws.mobile.user.signin.SignInManager;
import com.facebook.FacebookSdk;

import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.fragments.FollowersFragment;
import net.xvidia.vowmee.fragments.FollowingFragment;
import net.xvidia.vowmee.fragments.FriendsFragment;
import net.xvidia.vowmee.gcm.INotificationListener;
import net.xvidia.vowmee.network.model.ActivityLog;

import java.util.ArrayList;
import java.util.List;

public class ConnectionTabs extends AppCompatActivity implements INotificationListener {

    private final int GALLERY_INTENT_CALLED = 1234;
    private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 123;
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 125;
    private Context context;


    private Activity activity;
    private boolean isLive;
    private SignInManager signInManager;
    private Toolbar toolbarTop;
    private FloatingActionButton videoMenu;
    private RelativeLayout layout;
    private LinearLayout tintLayout;
    private TextView notifCountMenu;

    /**
     * SignInResultsHandler handles the results from sign-in for a previously signed in user.
     */
    private class SignInResultsHandler implements IdentityManager.SignInResultsHandler {
        /**
         * Receives the successful sign-in result for an alraedy signed in user and starts the main
         * activity.
         * @param provider the identity provider used for sign-in.
         */
        @Override
        public void onSuccess(final IdentityProvider provider) {


            AWSMobileClient.defaultMobileClient()
                    .getIdentityManager()
                    .loadUserInfoAndImage(provider, new Runnable() {
                        @Override
                        public void run() {
                            String bucket = AWSConfiguration.AMAZON_S3_USER_FILES_BUCKET;
                            String prefix = "public/";
                            AWSMobileClient.defaultMobileClient()
                                    .createUserFileManager(bucket,
                                            prefix,
                                            new UserFileManager.BuilderResultHandler() {

                                                @Override
                                                public void onComplete(final UserFileManager userFileManager) {
                                                    Utils.getInstance().setUserFileManager(userFileManager);
//													if(provider.getDisplayName().equalsIgnoreCase("Facebook")) {
//														sendFacebookRegisterRequest(provider.getFacebookUserId(), provider.getToken(), provider.getFacbookTokenExpiryDate());
//													}else if(provider.getDisplayName().equalsIgnoreCase("Twitter")){
//														sendTwitterRegisterRequest(provider.getTwitterUserId(), provider.getToken(), provider.getSecreteToken());
//
//													}
                                                }
                                            });
                        }
                    });


        }

        /**
         * For the case where the user previously was signed in, and an attempt is made to sign the
         * user back in again, there is not an option for the user to cancel, so this is overriden
         * as a stub.
         * @param provider the identity provider with which the user attempted sign-in.
         */
        @Override
        public void onCancel(final IdentityProvider provider) {
//			if(DataStorage.getInstance().getAppSocialId().isEmpty() && !DataStorage.getInstance().getRegisteredFlag()){
//				goSignIn();
//			}else{
//				if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_FACEBOOK)){
//					SocialAuthFacebook obj = DataCacheManager.getInstance().getFacebookData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && !obj.getUserId().isEmpty()){
//						sendFacebookRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getExpiryDate());
//					}else{
//						goSignIn();
//					}
//				}else if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_TWITTER)){
//					SocialAuthTwitter obj = DataCacheManager.getInstance().getTwitterData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && obj.getUserId()>0){
//						sendTwitterRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getAccessSecret());
//					}else{
//						goSignIn();
//					}
//				}else{
//					goSignIn();
//				}
//			}
//			hideProgressBar();
        }

        /**
         * Receives the sign-in result that an error occurred signing in with the previously signed
         * in provider and re-directs the user to the sign-in activity to sign in again.
         * @param provider the identity provider with which the user attempted sign-in.
         * @param ex the exception that occurred.
         */
        @Override
        public void onError(final IdentityProvider provider, Exception ex) {
//            Log.e("credentials refresh",
//                    String.format("Cognito credentials refresh with %s provider failed. Error: %s",
//                            provider.getDisplayName(), ex.getMessage()), ex);
//
//			Toast.makeText(Splash.this, String.format("Sign-in with %s failed.",
//					provider.getDisplayName()), Toast.LENGTH_LONG).show();
//			if(DataStorage.getInstance().getAppSocialId().isEmpty() && !DataStorage.getInstance().getRegisteredFlag()){
//				goSignIn();
//			}else{
//				if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_FACEBOOK)){
//					SocialAuthFacebook obj = DataCacheManager.getInstance().getFacebookData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && !obj.getUserId().isEmpty()){
//						sendFacebookRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getExpiryDate());
//					}else{
//						goSignIn();
//					}
//				}else if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_TWITTER)){
//					SocialAuthTwitter obj = DataCacheManager.getInstance().getTwitterData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && obj.getUserId()>0){
//						sendTwitterRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getAccessSecret());
//					}else{
//						goSignIn();
//					}
//				}else{
//					goSignIn();
//				}
//			}

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_timeline_tabs);

        FacebookSdk.sdkInitialize(this);
        activity = this;
        context = ConnectionTabs.this;
        videoMenu = (FloatingActionButton) findViewById(R.id.floatingActionButton);

        layout = (RelativeLayout)findViewById(R.id.bottomLayout);
        tintLayout = (LinearLayout) findViewById(R.id.tint_layout);

        videoMenu.setVisibility(View.GONE);
        layout.setVisibility(View.GONE);
        tintLayout.setVisibility(View.GONE);
        initToolbars();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

  private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(ConnectionTabs.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    private void initToolbars() {
        toolbarTop = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbarTop);
        getSupportActionBar().setTitle(getString(R.string.nav_contacts));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarTop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
       toolbarTop.setPadding(0, 0, 0, 0);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.timeline_view_pager);
        setupViewPager(viewPager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.timeline_tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();
    }

    private void showError(final String message,final DialogInterface.OnClickListener okClicked) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(ConnectionTabs.this).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, okClicked)
                        .show();
            }
        });
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new FriendsFragment(), getString(R.string.title_friends));
        adapter.addFrag(new FollowingFragment(), getString(R.string.title_following));
        adapter.addFrag(new FollowersFragment(), getString(R.string.title_followers));
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onNotificationReceived(ActivityLog activityLog) {

    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
            finish();
            super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}