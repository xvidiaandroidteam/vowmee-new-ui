package net.xvidia.vowmee;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.listadapter.GroupMemberListAdapter;
import net.xvidia.vowmee.listadapter.RecyclerViewScrollListener;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;
import net.xvidia.vowmee.helper.CustomFontEditTextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class GroupMemberActivity extends AppCompatActivity {


    private Context context;
    private RecyclerView mRecyclerView;
    private GroupMemberListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Profile> searchList;
    private String quesryString;
    private Toolbar toolbar;
    private TextView mMessageTextView;
    private CustomFontEditTextView mSearchQueryTextView;
    MenuItem menuAddMember;

//    public static ArrayList<Profile> getMemberList() {
//        return searchList;
//    }

//    public void setMemberList(ArrayList<Profile> searchList) {
//        this.searchList = searchList;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        quesryString="";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(ModelManager.getInstance().getGroupProfile().getName().toString());
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setPadding(0, 0, 0, 0);
        mMessageTextView = (TextView) findViewById(R.id.message_no_results);
        mSearchQueryTextView = (CustomFontEditTextView) findViewById(R.id.search_query);
        mSearchQueryTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                quesryString = s.toString();


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (quesryString!=null && !quesryString.isEmpty()) {
                    if (quesryString.length() > 0) {

                        final ArrayList<Profile> filteredList = new ArrayList<>();

                        for (int i = 0; i < searchList.size(); i++) {

                            final String text = searchList.get(i).toString().toLowerCase();
                            if (text.contains(quesryString)) {

                                filteredList.add(searchList.get(i));
                            }
                        }
                        mAdapter = new GroupMemberListAdapter(GroupMemberActivity.this, filteredList);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    } else {
                        mAdapter = new GroupMemberListAdapter(GroupMemberActivity.this, searchList);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                    }
//                    sendSearchRequest(quesryString);
                }
            }
        });
        mSearchQueryTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Utils.getInstance().hideKeyboard(GroupMemberActivity.this, mSearchQueryTextView);
                    return true;
                }
                return false;
            }
        });
        mMessageTextView.setVisibility(View.GONE);
        context = GroupMemberActivity.this;
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getGroupMemberList();
    }

    private void initialiseRecyclerView() {
//        addList();
        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView_list);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new GroupMemberListAdapter(GroupMemberActivity.this, searchList);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerViewScrollListener(this) {
            @Override
            public void onMoved(int distance) {
                toolbar.setTranslationY(-distance);
            }
        });
    }

    private void getGroupMemberList() {
        try {
            String userUUID = DataStorage.getInstance().getUserUUID();
            String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
            String url = ServiceURLManager.getInstance().getGroupMemberListUrl(groupUuid, userUUID, "0", "0");

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<Profile> obj = null;
                    try {
//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Profile>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {

                        if (obj.size() > 0) {
                            searchList = (ArrayList<Profile>) obj;
                            initialiseRecyclerView();
                            mAdapter.notifyDataSetChanged();
                                mRecyclerView.setVisibility(View.VISIBLE);
                        } else {
                            if(mRecyclerView!=null)
                                mRecyclerView.setVisibility(View.GONE);
                            mMessageTextView.setVisibility(View.VISIBLE);
                            mMessageTextView.setText(getResources().getString(R.string.message_no_moment));

                        }
                    } else {
                        if(mRecyclerView!=null)
                            mRecyclerView.setVisibility(View.GONE);
                        mMessageTextView.setVisibility(View.VISIBLE);
                        mMessageTextView.setText(getResources().getString(R.string.message_no_moment));

//                        showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                            }
//                        });
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    if(mRecyclerView!=null)
                        mRecyclerView.setVisibility(View.GONE);
                    mMessageTextView.setVisibility(View.VISIBLE);
                    mMessageTextView.setText(getResources().getString(R.string.message_no_moment));


//                    showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
                }
            }) {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        //                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_group, menu);
        if (menu != null) {
            menuAddMember = menu.findItem(R.id.action_add_member_group);
            MenuItem cancel = menu.findItem(R.id.action_next);
//            MenuItem save = menu.findItem(R.id.action_save_group);
            if (cancel != null)
                cancel.setVisible(false);

            if (setOptionsVisibility()) {
                if (menuAddMember != null)
                    menuAddMember.setVisible(true);
            } else {
                if (menuAddMember != null)
                    menuAddMember.setVisible(false);
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_member_group:
                Intent mIntent = new Intent(GroupMemberActivity.this, GroupAddMemberActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, false);
                mIntent.putExtras(mBundle);
                startActivity(mIntent);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean setOptionsVisibility() {
        boolean visible = false;
        if (ModelManager.getInstance().getGroupProfile() == null)
            return false;
        if (ModelManager.getInstance().getGroupProfile().getRoleWithViewer() == null)
            return false;
        if (ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MODERATOR)) {
            visible = true;
        } else if (ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_OWNER)) {
            visible = true;
        }

        return visible;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setGoogleAnalyticScreen("Group Members");
    }

    private void setGoogleAnalyticScreen(String screenName){
        MyApplication application = (MyApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


}
