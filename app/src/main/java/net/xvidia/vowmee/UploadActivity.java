package net.xvidia.vowmee;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.CheckNetworkConnection;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.GPSTracker;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.network.UploadffmpegService;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.network.model.MomentShare;
import net.xvidia.vowmee.storage.DataStorage;

import java.io.File;
import java.util.ArrayList;

public class UploadActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final int REQUEST_CHECK_SETTINGS =123 ;
    //    private static final String TAG = UploadActivity.class.getSimpleName();
    private EditText mCaptionEditText;
    private EditText mDescriptionAutoCompleteTextView;
    private LinearLayout mAudienceLayout;
    private LinearLayout mLocationLayout;
    //    private CheckBox publicCheckBox;
//    private CheckBox privateCheckBox;
//private String compressedFilePath = null;
    private GoogleApiClient googleApiClient;
    private TextView privacySwitchText;
    private boolean privateFlag;
    private SwitchCompat locationSwitch;
    private SwitchCompat privacySwitch;
    private ImageView mPreviewVideoPlayer;
    private ImageView mPreviewVideoPlayerBg;
    private String path;
    private boolean groupMoment;
    private String groupUuid;
    private boolean reshareFlag;
    private Moment moment;
    private boolean recordedVideo;
    private TextView upload;
    private Double latitude;
    private Double longitude;
    private GPSTracker gps;
//    FFmpeg ffmpeg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            path = intent.getStringExtra(AppConsatants.MOMENT_PATH);
            groupMoment = intent.getBooleanExtra(AppConsatants.UPLOAD_MOMENT_GROUP, false);
            groupUuid = intent.getStringExtra(AppConsatants.UUID);
            reshareFlag = intent.getBooleanExtra(AppConsatants.RESHARE, false);
            recordedVideo = intent.getBooleanExtra(AppConsatants.RECORDED_VIDEO, false);
        }
        if (reshareFlag) {
            if (UploadffmpegService.getMomentShare() == null) {
                finish();
            }
        } else {
            MomentShare momemtShare = new MomentShare();
            UploadffmpegService.setMomentShare(momemtShare);
        }
        latitude = 0d;
        longitude = 0d;
        SelectPrivateShareActivity.resetValues();
        moment = new Moment();
        initialiseViews();


    }


    private void attemptUpload() {

        // Reset errors.
        mCaptionEditText.setError(null);
        String caption = mCaptionEditText.getText().toString();
        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(caption)) {
            mCaptionEditText.setError(getString(R.string.error_field_required));
            focusView = mCaptionEditText;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            uploadMoment();
        }
    }

    private void initialiseViews() {
        mCaptionEditText = (EditText) findViewById(R.id.moment_caption);
        mDescriptionAutoCompleteTextView = (EditText) findViewById(R.id.moment_description);
        mPreviewVideoPlayer = (ImageView) findViewById(R.id.preview_videoplayer);
        mPreviewVideoPlayerBg = (ImageView) findViewById(R.id.preview_videoplayerBg);
        locationSwitch = (SwitchCompat) findViewById(R.id.switch_location);
        privacySwitch = (SwitchCompat) findViewById(R.id.switch_privacy);
        privacySwitchText = (TextView) findViewById(R.id.switch_privacy_text);
        mAudienceLayout = (LinearLayout) findViewById(R.id.audience_layout);
        mLocationLayout = (LinearLayout) findViewById(R.id.location_layout);
//        publicCheckBox = (CheckBox)findViewById(R.id.public_checkbox);
//        privateCheckBox = (CheckBox)findViewById(R.id.private_checkbox);
        upload = (TextView) findViewById(R.id.button_save);
        upload.setVisibility(View.INVISIBLE);
        gps = new GPSTracker(UploadActivity.this);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.getInstance().hideKeyboard(UploadActivity.this, mCaptionEditText);
                Utils.getInstance().hideKeyboard(UploadActivity.this, mDescriptionAutoCompleteTextView);
                attemptUpload();
            }
        });
        int paddingLeftRight = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_10);
        int paddingTopBottom = Utils.getInstance().convertDimenToInt(R.dimen.button_text_padding);
        upload.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight, paddingTopBottom);
        Bitmap image = null;
        if(reshareFlag){
            Moment obj = UploadffmpegService.getMomentShare().getOriginalMoment();
            if(obj!=null) {
                Utils.getInstance().setMomentObj(obj);
                image = FileCache.getInstance().getBitmapFromMemCache(obj.getThumbNail());
                mCaptionEditText.setText(obj.getCaption());
                mDescriptionAutoCompleteTextView.setText(obj.getDescription());
            }
        }else {
           image = Utils.getInstance().getVideoThumbnail(path);
        }
        mPreviewVideoPlayer.setImageBitmap(image);
        mPreviewVideoPlayerBg.setImageBitmap(image);
        mPreviewVideoPlayerBg.setImageAlpha(80);

        mPreviewVideoPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Uri fileUri = Uri.parse(path);
//                Intent intent = new Intent(Intent.ACTION_VIEW, fileUri);
//                intent.setDataAndType(fileUri, "video/*");
//                startActivity(intent);
                Intent mIntent = new Intent(UploadActivity.this, TimelineMomentDetail.class);
                Bundle mBundle = new Bundle();
                mBundle.putString(AppConsatants.UUID, path);
                mBundle.putBoolean(AppConsatants.UPLOAD_FILE_PATH, true);
                mIntent.putExtras(mBundle);
                startActivity(mIntent);
            }
        });

       /* publicCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                publicCheckBox.setChecked(isChecked);
                privateCheckBox.setChecked(!isChecked);
                privateFlag = !isChecked;

            }
        });*/

        /*privateCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                privateFlag = isChecked;
                publicCheckBox.setChecked(!isChecked);
                privateCheckBox.setChecked(isChecked);
                if(privateFlag){
                    Intent mIntent = new Intent(UploadActivity.this, ShareMomentActivity.class);
//                    Bundle mBundle = new Bundle();
//                    mBundle.putString(AppConsatants.MOMENT_PATH, path);
//                    mBundle.putString(AppConsatants.UUID, groupUuid);
//                    mBundle.putBoolean(AppConsatants.UPLOAD_MOMENT_GROUP, groupMoment);
//                    mBundle.putBoolean(AppConsatants.RESHARE, false);
//                    mIntent.putExtras(mBundle);
                    startActivity(mIntent);
//                    finish();
                }
            }
        });*/
        locationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (CheckNetworkConnection
                        .isConnectionAvailable(getApplicationContext()) && isChecked) {
                    if (gps.canGetLocation()) {
                        Location l = gps.getLocation();

                        if (l != null) {
                            latitude = l.getLatitude();
                            longitude = l.getLongitude();
//                            Log.i("geo location", " latitude " + latitude + " longitude" + longitude);
                        }
                    } else {
                       location();
                    }
                }
            }
        });
        privacySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                privateFlag = !isChecked;
                if (privateFlag) {
                    privacySwitchText.setText(getString(R.string.prompt_private));
                    Intent mIntent = new Intent(UploadActivity.this, SelectPrivateShareActivity.class);
                    startActivity(mIntent);
                } else {
                    privacySwitchText.setText(getString(R.string.prompt_public));
                }
            }
        });
        if(gps.canGetLocation()){
            locationSwitch.setChecked(true);
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        }else{
            locationSwitch.setChecked(false);
        }
        privacySwitch.setChecked(true);
        privacySwitchText.setText(getString(R.string.prompt_public));
        privateFlag = false;
        mCaptionEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        mDescriptionAutoCompleteTextView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        upload.setVisibility(View.VISIBLE);
        if (groupUuid != null && !groupUuid.isEmpty()) {
            mLocationLayout.setVisibility(View.GONE);
            mAudienceLayout.setVisibility(View.GONE);
            privateFlag = true;
            ArrayList<String> groupList = new ArrayList<>();
            groupList.add(groupUuid);
            SelectPrivateShareActivity.setGroupList(groupList);

        } else {
            mLocationLayout.setVisibility(View.VISIBLE);
            mAudienceLayout.setVisibility(View.VISIBLE);
        }
        if(!reshareFlag)
            checkVideoValidSize();
    }

    private void uploadMoment() {
        try {

            moment.setUuid("");
            moment.setCaption(mCaptionEditText.getText().toString());
            moment.setDescription(mDescriptionAutoCompleteTextView.getText().toString());
            moment.setIsLive(false);
            moment.setOwner(DataStorage.getInstance().getUsername());
            moment.setLatitude(latitude);
            moment.setLongitude(longitude);

            if (privateFlag)
                moment.setProfile(AppConsatants.PROFILE_PRIVATE);
            else
                moment.setProfile(AppConsatants.PROFILE_PUBLIC);

            UploadffmpegService.getMomentShare().setFollowers(SelectPrivateShareActivity.isAllFollowers());
            UploadffmpegService.getMomentShare().setFriends(SelectPrivateShareActivity.isAllFriends());
            UploadffmpegService.getMomentShare().setGroupUuids(SelectPrivateShareActivity.getGroupList());
            UploadffmpegService.getMomentShare().setUserUuids(SelectPrivateShareActivity.getMemberUuidList());
            if (gps != null)
                gps.stopUsingGPS();
            if (groupMoment) {
                if (groupUuid != null && !groupUuid.isEmpty()) {

                    moment.setGroupUuid(groupUuid);
                    UploadffmpegService.getMomentShare().setNewMoment(moment);
                    Intent mIntent = new Intent(this, UploadffmpegService.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString(AppConsatants.UPLOAD_FILE_CAPTION, mCaptionEditText.getText().toString());
                    mBundle.putString(AppConsatants.UPLOAD_FILE_DESCRIPTION, mDescriptionAutoCompleteTextView.getText().toString());
                    mBundle.putString(AppConsatants.UPLOAD_FILE_PATH, path);
                    mBundle.putBoolean(AppConsatants.UPLOAD_MOMENT_GROUP, groupMoment);
                    mBundle.putBoolean(AppConsatants.RESHARE, reshareFlag);
                    mBundle.putBoolean(AppConsatants.RECORDED_VIDEO, recordedVideo);
                    mBundle.putString(AppConsatants.UUID, groupUuid);
                    mIntent.putExtras(mBundle);
                    startService(mIntent);
                    finish();
                } else {
                    showError(getString(R.string.error_upload_failed), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            onBackPressed();
                        }
                    });

                }
            } else {
                UploadffmpegService.getMomentShare().setNewMoment(moment);
                Intent mIntent = new Intent(this, UploadffmpegService.class);
                Bundle mBundle = new Bundle();
                mBundle.putString(AppConsatants.UPLOAD_FILE_CAPTION, mCaptionEditText.getText().toString());
                mBundle.putString(AppConsatants.UPLOAD_FILE_DESCRIPTION, mDescriptionAutoCompleteTextView.getText().toString());
                mBundle.putString(AppConsatants.UPLOAD_FILE_PATH, path);
                mBundle.putBoolean(AppConsatants.UPLOAD_MOMENT_GROUP, groupMoment);
                mBundle.putBoolean(AppConsatants.RESHARE, reshareFlag);
                mBundle.putBoolean(AppConsatants.RECORDED_VIDEO, recordedVideo);
                mBundle.putString(AppConsatants.UUID, groupUuid);
                mIntent.putExtras(mBundle);
                startService(mIntent);
                finish();
            }


        } catch (Exception e) {

        }
    }


    private void showError(final String mesg, final DialogInterface.OnClickListener okClicked) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new AlertDialog.Builder(UploadActivity.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .setCancelable(false)
                            .show();
                }
            });
        }catch (WindowManager.BadTokenException e){}
        catch (Exception e){}
    }

    @Override
    public void onBackPressed() {
        if (gps != null)
            gps.stopUsingGPS();
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        if (!SelectPrivateShareActivity.isAllFollowers()
                && !SelectPrivateShareActivity.isAllFriends()
                && SelectPrivateShareActivity.getGroupList().size() == 0
                && SelectPrivateShareActivity.getMemberUuidList().size() == 0) {
            privacySwitch.setChecked(true);
            privacySwitchText.setText(getString(R.string.prompt_public));
            privateFlag = false;
        }
        super.onResume();
    }

    private void location() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(UploadActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            settingsrequest();
        }
    }

    public void settingsrequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
// All location settings are satisfied. The client can initialize location
// requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
// Location settings are not satisfied. But could be fixed by showing the user
// a dialog.
                        try {
// Show the dialog by calling startResolutionForResult(),
// and check the result in onActivityResult().
                            status.startResolutionForResult(UploadActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
// Ignore the error.
                        }catch (Exception E){}
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
// Location settings are not satisfied. However, we have no way to fix the
// settings so we won't show the dialog.
                        break;
                }
            }
        });

    }


    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
// Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        if(gps!=null &&gps.canGetLocation()){
                          latitude= gps.getLatitude();
                            longitude= gps.getLongitude();
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        if(locationSwitch!=null)
                            locationSwitch.setChecked(false);
//                        settingsrequest();//keep asking if imp or do whatever
                        break;
                }
                break;
       }
    }



    private void showAlert() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(UploadActivity.this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Large file")
                            .setMessage(getString(R.string.error_large_file_upload))
                            .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }

                            })
                            .show();
                }
            });

        }catch(WindowManager.BadTokenException e){

        }catch (Exception e){}
    }

    private void checkVideoValidSize(){
        if(path !=null && !path.isEmpty()) {
            File file = new File(path);
            if (file.exists()) {
//                MediaMetadataRetriever fetchVideoInfo = new MediaMetadataRetriever();
//                fetchVideoInfo.setDataSource(path);
//                String bitrate = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE);
//                String duration = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//                int defaultBitrate = 400;
//                int intBitrate = 0, intDuration = 0;
                long compressedVideoSize;
//                if (bitrate != null) {
//                    intBitrate = Integer.parseInt(bitrate);
//                    intBitrate = intBitrate / 8192; //converting bits/sec to kb/sec
//                    if (intBitrate >= defaultBitrate) {
//                        intBitrate = defaultBitrate;
//                    }
//                }
//                if (duration != null) {
//                    intDuration = Integer.parseInt(duration);
//                    intDuration = intDuration / 1000; // converting ms to sec
//                }
                compressedVideoSize = file.length();
                if(compressedVideoSize>0) {
                    compressedVideoSize = compressedVideoSize/1024; // kb
                    compressedVideoSize = compressedVideoSize/1024;//mb
//                    compressedVideoSize = intBitrate * 8 * intDuration;
                    if (compressedVideoSize > 20) {
                        showAlert();
                    }
                }
            }
        }
    }
}
