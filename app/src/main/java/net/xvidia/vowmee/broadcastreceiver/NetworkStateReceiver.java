package net.xvidia.vowmee.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.network.UploadffmpegService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ravi_office on 17-Dec-15.
 */
public class NetworkStateReceiver extends BroadcastReceiver {
    private ConnectivityManager mManager;
    private List<NetworkStateReceiverListener> mListeners;
    private boolean mConnected;
    public NetworkStateReceiver() {

    }
    public NetworkStateReceiver(Context context) {
        mListeners = new ArrayList<>();
        mManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        checkStateChanged();
    }

    public void onReceive(Context context, Intent intent) {
        if (intent == null || intent.getExtras() == null)
            return;
        if(mListeners==null)
            mListeners = new ArrayList<>();
        mManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (checkStateChanged()){
            Intent mIntent = new Intent(context, UploadffmpegService.class);
            Bundle mBundle = new Bundle();
            mBundle.putBoolean(AppConsatants.NETWORK_RESTART, true);
            mIntent.putExtras(mBundle);
            context.startService(mIntent);
            notifyStateToAll();
        }
    }

    private boolean checkStateChanged() {
        boolean prev = mConnected;
        NetworkInfo activeNetwork = mManager.getActiveNetworkInfo();
        mConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return prev != mConnected;
    }

    private void notifyStateToAll() {
        for (NetworkStateReceiverListener listener : mListeners) {
            notifyState(listener);
        }
    }

    private void notifyState(NetworkStateReceiverListener listener) {
        if (listener != null) {
            if (mConnected){
                listener.onNetworkAvailable();
            }
            else listener.onNetworkUnavailable();
        }
    }

    public void addListener(NetworkStateReceiverListener l) {
        mListeners.add(l);
        notifyState(l);
    }

    public void removeListener(NetworkStateReceiverListener l) {
        mListeners.remove(l);
    }

    public interface NetworkStateReceiverListener {
        void onNetworkAvailable();

        void onNetworkUnavailable();
    }



}