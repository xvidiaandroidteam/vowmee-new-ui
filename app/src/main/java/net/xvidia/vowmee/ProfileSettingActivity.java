package net.xvidia.vowmee;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.helper.DividerItemDecoration;
import net.xvidia.vowmee.helper.WrappingLinearLayoutManager;
import net.xvidia.vowmee.listadapter.BlockedUserListAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProfileSettingActivity extends AppCompatActivity  {
    private static final int TONE_PICKER = 123 ;
    private static final int NOTIFICATION_PICKER = 456 ;
    private TextView privacyTextView;
    private TextView ringtoneTextView;
    private TextView notificationToneTextView;
    private static TextView blockedCountTextView;
    private static Profile profile;
    private boolean privateFlag;
    private SwitchCompat privacySwitch;
    private SwitchCompat notificationSwitch;
    private LinearLayout notificationLayout;
    private RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    private Toolbar toolbar;
    private MenuItem menuSave;
    private Button doneButton;
    private Uri currentTone;
    private Uri currentNotificationTone;
    private TextView mVersionApp;
    private TextView mDeactivateView;
    private TextView mTermsAndCondition;
    private TextView mPrivacyPolicy;
    private TextView mNameEditText;
    private CircularImageView profileImage;
    private static ArrayList<Profile> memberList;
    private ProgressBar mProgressBar;
    private boolean refresh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_setting);

        privacyTextView = (TextView)findViewById(R.id.switch_privacy_text);
        ringtoneTextView = (TextView)findViewById(R.id.call_ringtone_view);
        notificationToneTextView = (TextView)findViewById(R.id.notification_ringtone_view);
        privacySwitch = (SwitchCompat) findViewById(R.id.switch_privacy);
        notificationSwitch = (SwitchCompat) findViewById(R.id.switch_notification);
        notificationLayout = (LinearLayout)findViewById(R.id.notification_ringtone_layout);
        blockedCountTextView = (TextView)findViewById(R.id.blocked_count);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mNameEditText = (TextView)findViewById(R.id.profile_name);
        profileImage = (CircularImageView)findViewById(R.id.profile_image);
        mDeactivateView = (TextView)findViewById(R.id.deactivate_view);
        mVersionApp = (TextView)findViewById(R.id.app_version);
        mTermsAndCondition = (TextView)findViewById(R.id.terms_condition);
        mPrivacyPolicy = (TextView)findViewById(R.id.privacy_policy);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar);
        profile = ModelManager.getInstance().getOwnProfile();
        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView_list);

        mVersionApp.setText(getString(R.string.message_app_version,getAppVersion()));
//        final Uri currentTone= RingtoneManager.getActualDefaultRingtoneUri(ProfileSettingActivity.this, RingtoneManager.TYPE_ALARM);
        String ringtoneUriString = DataStorage.getInstance().getCallPath();
        if(ringtoneUriString.isEmpty()) {
            currentTone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        }else {
            currentTone = Uri.parse(ringtoneUriString);
        }
        String notificationUriString = DataStorage.getInstance().getCallPath();
        if(notificationUriString.isEmpty()) {
            currentNotificationTone= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        }else {
            currentNotificationTone = Uri.parse(notificationUriString);
        }
        Ringtone ringTone = RingtoneManager.getRingtone(getApplicationContext(), currentTone);
        ringtoneTextView.setText(ringTone.getTitle(this));

        Ringtone notificationTone = RingtoneManager.getRingtone(getApplicationContext(), currentNotificationTone);
        notificationToneTextView.setText(notificationTone.getTitle(this));
        ringtoneTextView.setOnClickListener(new View.OnClickListener()  {

            public void onClick(View arg0) {
                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_RINGTONE);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Call RingTone");
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, currentTone);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
                startActivityForResult(intent, TONE_PICKER);
            }

        });

        notificationToneTextView.setOnClickListener(new View.OnClickListener()  {

            public void onClick(View arg0) {
                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Notification Tone");
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, currentNotificationTone);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
                startActivityForResult(intent, NOTIFICATION_PICKER);
            }

        });
        mPrivacyPolicy.setOnClickListener(new View.OnClickListener()  {

            public void onClick(View arg0) {
                WebViewActivity.url = getString(R.string.api_privacy_policy);
                Intent intent = new Intent(ProfileSettingActivity.this,WebViewActivity.class);
                startActivity(intent);
            }

        });
        mTermsAndCondition.setOnClickListener(new View.OnClickListener()  {

            public void onClick(View arg0) {
                WebViewActivity.url = getString(R.string.api_terms_condition);
                Intent intent = new Intent(ProfileSettingActivity.this,WebViewActivity.class);
                startActivity(intent);
            }

        });
        mDeactivateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 showAlertDialog(getString(R.string.alert_message_deactivate), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            showProgressbar();
                            sendDeactivateAccountRequest();
                        }
                    });
            }
        });

        blockedCountTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profile!=null&&profile.getBlockedUserCount() > 0) {
                    getBlockedPersonList();
                }
            }
        });
        privacySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                privateFlag = !isChecked;
                showDoneButton(true);
                if(privateFlag){
                    privacyTextView.setText(getString(R.string.prompt_private));
                }else{
                    privacyTextView.setText(getString(R.string.prompt_public));
                }
            }
        });
        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    notificationLayout.setVisibility(isChecked?View.VISIBLE:View.GONE);
                DataStorage.getInstance().setNotificationEnabled(isChecked);
            }
        });
        updateImage();
        if(profile == null) {
            finish();
            return;
        }
        if(profile.getProfile().equalsIgnoreCase(AppConsatants.PROFILE_PUBLIC))
            privateFlag = false;
        else
            privateFlag = true;

        if(privateFlag){
            privacyTextView.setText(getString(R.string.prompt_private));
        }else{
            privacyTextView.setText(getString(R.string.prompt_public));
        }

        privacySwitch.setChecked(!privateFlag);
        notificationSwitch.setChecked(DataStorage.getInstance().getNotificationEnabled());
        if(DataStorage.getInstance().getNotificationEnabled()) {
            notificationLayout.setVisibility(View.VISIBLE);
        }else{
            notificationLayout.setVisibility(View.GONE);
        }
        blockedCountTextView.setText(getString(R.string.member_count,profile.getBlockedUserCount()));
        mRecyclerView.setVisibility(View.GONE);
//        initialiseRecyclerView();
    }

    private void updateImage(){

        if(profileImage == null || profile==null)
            return;
           String profImagePath = profile.getProfileImage();
            profImagePath = (profImagePath == null ? (profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG : profile.getThumbNail()));

            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(profImagePath);
            if (image != null) {
                profileImage.setImageBitmap(image);
            } else {
                String filePathImage = Utils.getInstance().getLocalContentPath(profImagePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(profImagePath, image1);
                    profileImage.setImageBitmap(image1);
                } else {
                    profileImage.setImageResource(R.drawable.user);
                }
            }
        mNameEditText.setText(profile.getDisplayName());
        download(profileImage,CropGroupImageActivity.groupImage);
    }

    private void initialiseRecyclerView() {

        mRecyclerView.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager mLayoutManager = new WrappingLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new BlockedUserListAdapter(ProfileSettingActivity.this, memberList);
        if(!refresh) {
            mRecyclerView.addItemDecoration(new DividerItemDecoration(0,false));
            refresh =true;
        }
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(false);
//        mRecyclerView.addOnScrollListener(new RecyclerViewScrollListener(this) {
//            @Override
//            public void onMoved(int distance) {
//                toolbar.setTranslationY(-distance);
//            }
//        });
    }



    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        bindActivity();
    }

    private void bindActivity() {
        toolbar.setPadding(0, 0, 0, 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        getSupportActionBar().setTitle(getString(R.string.settings));

    }
 @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        getGroupMemberList();
        getProfileRequest();
        updateImage();
        setGoogleAnalyticScreen("Setting");
    }
    public static void setBlockCount(int count){
        if(blockedCountTextView!=null)
        blockedCountTextView.setText(MyApplication.getAppContext().getResources().getString(R.string.member_count,count));
    }
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_group, menu);
        if (menu != null) {
            menuSave = menu.findItem(R.id.action_next);
            MenuItem menuExtra = menu.findItem(R.id.action_add_member_group);
            menuExtra.setVisible(false);
            MenuItemCompat.setActionView(menuSave, R.layout.group_next);
            View viewNext = (View) MenuItemCompat.getActionView(menuSave);
            doneButton = (Button) viewNext.findViewById(R.id.group_next);
            doneButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    sendSaveProfileInfoRequest();
                }
            });
            doneButton.setEnabled(false);
            doneButton.setTextColor(getResources().getColor(R.color.white_disabled_menu));

            menuSave.setVisible(true);
            doneButton.setVisibility(View.VISIBLE);
            showDoneButton(false);
        }
        return super.onCreateOptionsMenu(menu);
    }
    private void showDoneButton(boolean show){
        if(show){
            if(menuSave !=null && doneButton != null) {
                doneButton.setEnabled(true);
                doneButton.setText(getResources().getString(R.string.action_done));
                doneButton.setTextColor(getResources().getColor(R.color.white));
            }
        }else{
            if(menuSave !=null && doneButton != null) {
                doneButton.setText(getResources().getString(R.string.action_done));
                doneButton.setEnabled(false);
                doneButton.setTextColor(getResources().getColor(R.color.white_disabled_menu));

            }
        }
    }



    private void sendSaveProfileInfoRequest() {
        try {
            String profileType = AppConsatants.PROFILE_PUBLIC;
            if(privateFlag){
                profileType = AppConsatants.PROFILE_PRIVATE;
            }
            if(profile == null) {
                return;
            }
            profile.setProfile(profileType);
            showProgressbar();
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_UPDATE_PROFILE);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(profile);
            } catch (IOException e) {
                e.printStackTrace();
            }
//            Log.i("Register request", jsonObject);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        String username = obj.getUsername();
                        if(username != null && !username.isEmpty()) {

                            showDoneButton(false);
                            ModelManager.getInstance().setOwnProfile(obj);
//                            finish();
                        }else{
                            showError(getString(R.string.error_general));
                        }
                    }
                    hideProgressBar();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    showError(getString(R.string.error_general));
                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void showError(final String mesg) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new AlertDialog.Builder(ProfileSettingActivity.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                }
            });
        }catch(WindowManager.BadTokenException e){

        }catch (Exception e){}
    }

    /**
     * Returns application version
     * @return
     */
    private String getAppVersion(){
        String version = "1.0";
        try {
            PackageManager manager = MyApplication.getAppContext().getPackageManager();
            PackageInfo info;
            info = manager.getPackageInfo( MyApplication.getAppContext().getPackageName(), 0);
            version = info.versionName;

        } catch (PackageManager.NameNotFoundException e) {
        }catch(Exception e){

        }
        return version;
    }

    private void  download(final CircularImageView holder, String filePath){
        UserFileManager userFileManager = Utils.getInstance().getUserFileManager();
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePath = Utils.getInstance().getLocalContentPath(contentItem.getFilePath());
                holder.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePath, false));

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }

    private void hideProgressBar(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressBar!=null)
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    private void showProgressbar(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressBar!=null)
                    mProgressBar.setVisibility(View.VISIBLE);
            }
        });
    }


    private void showAlertDialog(final String mesg, final DialogInterface.OnClickListener okListner) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(ProfileSettingActivity.this,R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, okListner)
                            .setNegativeButton(android.R.string.cancel, null)
                            .show();
                }
            });
        }catch(WindowManager.BadTokenException e){}
        catch(Exception e){}
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK) {
//            Log.i("Ringtone",data.getData().toString());
//            ringtoneTextView.setText(data.getData().toString());
//        }

        if(resultCode == RESULT_OK && data!=null){
            if(requestCode == TONE_PICKER) {
                Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                Ringtone ringTone = RingtoneManager.getRingtone(getApplicationContext(), uri);
                ringtoneTextView.setText(ringTone.getTitle(this));
                if (uri != null)
                    DataStorage.getInstance().setCallPath(uri.toString());
            }else if(requestCode == NOTIFICATION_PICKER){
                Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                Ringtone ringTone = RingtoneManager.getRingtone(getApplicationContext(), uri);
                notificationToneTextView.setText(ringTone.getTitle(this));
                if (uri != null)
                    DataStorage.getInstance().setNotificationPath(uri.toString());
            }
//            ringTone.play();
        }
    }

    private void getBlockedPersonList() {
        try {showProgressbar();
            final String userUuid = DataStorage.getInstance().getUserUUID();
            String url = ServiceURLManager.getInstance().getBlockedListUrl(userUuid,"0", "0");

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    List<Profile> obj = null;
                    try {
//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Profile>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    memberList = new ArrayList<>();
                    if (obj != null) {
                        if (obj.size() > 0) {
                            memberList = (ArrayList<Profile>) obj;
                            initialiseRecyclerView();
                        }else{
                            showError(getString(R.string.error_general));
                        }
                    }else{
                        showError(getString(R.string.error_general));
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    showError(getString(R.string.error_general));
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendDeactivateAccountRequest(){
        try {
            final String username = DataStorage.getInstance().getUsername();
            String url =ServiceURLManager.getInstance().getDeactivateAccountUrl(username);
            JsonObjectRequest  request= new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    AWSMobileClient.defaultMobileClient().getIdentityManager().signOut();
                    VolleySingleton.getInstance(MyApplication.getAppContext()).clearCache();
                    DataStorage.getInstance().setRegisteredFlag(false);
                    Intent mIntentLogin = new Intent(ProfileSettingActivity.this, RegisterActivity.class);
                    mIntentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntentLogin);
                    finish();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    Utils.getInstance().displayToast(ProfileSettingActivity.this,getString(R.string.error_request_failed),Utils.SHORT_TOAST);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){

        }
    }

    private void getProfileRequest(){
        try {
            final String username = DataStorage.getInstance().getUsername();
            String url =ServiceURLManager.getInstance().getProfileUrl(IAPIConstants.API_KEY_GET_PROFILE, username, username);
            VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(url, true);
            JsonObjectRequest  request= new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    String getUsername = obj.getUsername();
                    if(getUsername != null && !getUsername.isEmpty() && username.equals(getUsername)) {
                        ModelManager.getInstance().setOwnProfile(obj);
                        setBlockCount(obj.getBlockedUserCount());
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){

        }
    }

    private void setGoogleAnalyticScreen(String screenName){
        MyApplication application = (MyApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.setClientId(DataStorage.getInstance().getUserUUID());
        mTracker.setAppVersion(""+Utils.getInstance().getAppVersion(MyApplication.getAppContext()));
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}
