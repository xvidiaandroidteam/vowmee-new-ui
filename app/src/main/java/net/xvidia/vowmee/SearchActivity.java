package net.xvidia.vowmee;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CustomFontEditTextView;
import net.xvidia.vowmee.helper.WrappingLinearLayoutManager;
import net.xvidia.vowmee.listadapter.RecyclerViewScrollListener;
import net.xvidia.vowmee.listadapter.SearchListAdapter;
import net.xvidia.vowmee.listadapter.SearchListGroupAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.GroupProfile;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.network.model.Search;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class SearchActivity extends AppCompatActivity {


    private Context context;
    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerViewGroup;
    private SearchListAdapter mAdapter;
    private SearchListGroupAdapter mAdapterGroup;
    private List<Profile> searchList;
    private List<GroupProfile> groupSearchList;
    private String quesryString;
    private Toolbar toolbar;
    private TextView mMessageTextView;
    private CustomFontEditTextView mSearchQueryTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setPadding(0, 0, 0, 0);
        mMessageTextView = (TextView) findViewById(R.id.message_no_results);
        mSearchQueryTextView = (CustomFontEditTextView) findViewById(R.id.search_query);
        mSearchQueryTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                quesryString = s.toString();


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (quesryString!=null && !quesryString.isEmpty()) {
                    if (quesryString.length() > 2) {
                        sendSearchRequest(quesryString);

                    }
//                    sendSearchRequest(quesryString);
                }
            }
        });
        mSearchQueryTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Utils.getInstance().hideKeyboard(SearchActivity.this, mSearchQueryTextView);
                    sendSearchRequest(quesryString);
                    return true;
                }
                return false;
            }
        });
        mMessageTextView.setVisibility(View.GONE);
        context = SearchActivity.this;
        searchList = new ArrayList<>();
        groupSearchList= new ArrayList<>();
        initialiseRecyclerViewGroup();
        initialiseRecyclerView();
        sendPendingListRequest();
    }

    private void initialiseRecyclerViewGroup(){
//        addList();
        RecyclerView.LayoutManager mLayoutManager = new WrappingLinearLayoutManager(this);
        mRecyclerViewGroup = (RecyclerView) findViewById(R.id.recycleView_list_group);
        mRecyclerViewGroup.setLayoutManager(mLayoutManager);
        mAdapterGroup = new SearchListGroupAdapter(SearchActivity.this, groupSearchList);

        mRecyclerViewGroup.setAdapter(mAdapterGroup);
        mRecyclerViewGroup.setNestedScrollingEnabled(false);
        mRecyclerViewGroup.setHasFixedSize(false);
        mRecyclerViewGroup.addOnScrollListener(new RecyclerViewScrollListener(this) {
            @Override
            public void onMoved(int distance) {
                toolbar.setTranslationY(-distance);
            }
        });
    }
    private void initialiseRecyclerView() {

        RecyclerView.LayoutManager mLayoutManager = new WrappingLinearLayoutManager(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView_list);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new SearchListAdapter(SearchActivity.this, searchList);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.addOnScrollListener(new RecyclerViewScrollListener(this) {
            @Override
            public void onMoved(int distance) {
                toolbar.setTranslationY(-distance);
            }
        });
    }
/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        // Retrieve the SearchView and plug it into SearchManager
//        MenuItem searchMenuItem = menu.findItem( R.id.action_search ); // get my MenuItem with placeholder submenu
//        searchMenuItem.expandActionView();
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setIconifiedByDefault(false);
        searchView.requestFocus();
//        SearchViewCompat.setIconified(searchView,true);
        Utils.getInstance().showSoftKeyboard(SearchActivity.this, searchView);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!quesryString.isEmpty()) {

                    Utils.getInstance().hideKeyboard(SearchActivity.this, searchView);
                    sendSearchRequest(quesryString);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                quesryString = newText;
                if (newText.length() > 2) {
                    sendSearchRequest(quesryString);
                }
                return false;
            }
        });
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return true;
    }*/

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
*/
      /*Api Request & Response
      * Moment added to s3 bucket*/

    private void sendSearchRequest(String searchString){
        try {
            String url =ServiceURLManager.getInstance().getSearchListUrl(IAPIConstants.API_KEY_SEARCH_PERSON, DataStorage.getInstance().getUserUUID(), searchString, "0", "0");

            JsonObjectRequest request =new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    ObjectMapper mapper = new ObjectMapper();
                   Search obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Search.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
                        if (obj.getPersonVos()!=null) {

                            mMessageTextView.setVisibility(View.GONE);
                            if(searchList != null)
                                searchList.clear();
                            searchList = obj.getPersonVos();
                            initialiseRecyclerView();
                            if(mRecyclerView != null)
                                mRecyclerView.setVisibility(View.VISIBLE);
                            mAdapter.notifyDataSetChanged();
                        }
                        if (obj.getGroupVos()!=null) {
                            mMessageTextView.setVisibility(View.GONE);
                            if(groupSearchList != null)
                                groupSearchList.clear();
                            groupSearchList = obj.getGroupVos();
//                            Log.i("Moment uuid", "" + obj.get(0).toString());
                            initialiseRecyclerViewGroup();
                            if(mRecyclerViewGroup != null)
                                mRecyclerViewGroup.setVisibility(View.VISIBLE);
                            mAdapterGroup.notifyDataSetChanged();
                        }
                        if(searchList.size()==0 && groupSearchList.size()==0){
                            mMessageTextView.setVisibility(View.VISIBLE);
                            if(mRecyclerView != null)
                                mRecyclerView.setVisibility(View.GONE);
                            if(mRecyclerViewGroup != null)
                                mRecyclerViewGroup.setVisibility(View.GONE);
                        }

//                        Utils.getInstance().hideKeyboard(SearchActivity.this, mSearchQueryTextView);
                    }else{

                        mMessageTextView.setVisibility(View.VISIBLE);
                        if(mRecyclerView != null)
                            mRecyclerView.setVisibility(View.GONE);
                        if(mRecyclerViewGroup != null)
                            mRecyclerViewGroup.setVisibility(View.GONE);
                        Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_try_again), Utils.LONG_TOAST);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_try_again), Utils.LONG_TOAST);
                    mMessageTextView.setVisibility(View.VISIBLE);
                        if(mRecyclerView != null)
                            mRecyclerView.setVisibility(View.GONE);
                    if(mRecyclerViewGroup != null)
                        mRecyclerViewGroup.setVisibility(View.GONE);
//                    }
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void sendPendingListRequest() {
        try {


            String url = ServiceURLManager.getInstance().getSuggestListUrl(DataStorage.getInstance().getUserUUID(),"0","0");

            JsonObjectRequest request =new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    ObjectMapper mapper = new ObjectMapper();
                    Search obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Search.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
                        if (obj.getPersonVos()!=null) {
                            if (obj.getPersonVos().size() > 0) {
                                mMessageTextView.setVisibility(View.GONE);
                                if (searchList != null) {
                                    searchList.addAll(obj.getPersonVos());
                                }
                                HashSet<Profile> duplicate = new HashSet<>();
                                duplicate.addAll(searchList);
                                if(searchList!=null)
                                    searchList.clear();
                                searchList.addAll(duplicate);
                                initialiseRecyclerView();

                                if (mRecyclerView != null)
                                    mRecyclerView.setVisibility(View.VISIBLE);

                                if (mAdapter != null)
                                    mAdapter.notifyDataSetChanged();

                            }
                            if (obj.getGroupVos()!=null) {
                                mMessageTextView.setVisibility(View.GONE);
                                if(groupSearchList != null)
                                    groupSearchList.clear();
                                groupSearchList = obj.getGroupVos();
//                            Log.i("Moment uuid", "" + obj.get(0).toString());
                                initialiseRecyclerViewGroup();
                                if(mRecyclerViewGroup != null)
                                    mRecyclerViewGroup.setVisibility(View.VISIBLE);
                                mAdapterGroup.notifyDataSetChanged();
                            }

                        }
                    }

                }

            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if(searchList.size()==0 && groupSearchList.size()==0){
                        mMessageTextView.setVisibility(View.VISIBLE);
                        if(mRecyclerView != null)
                            mRecyclerView.setVisibility(View.GONE);
                        if(mRecyclerViewGroup != null)
                            mRecyclerViewGroup.setVisibility(View.GONE);
                    }
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        setGoogleAnalyticScreen("Search");
        super.onResume();
    }

    private void setGoogleAnalyticScreen(String screenName){
        MyApplication application = (MyApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.setClientId(DataStorage.getInstance().getUserUUID());
        mTracker.setAppVersion(""+Utils.getInstance().getAppVersion(MyApplication.getAppContext()));
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


}
