package net.xvidia.vowmee.videoplayer;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.SurfaceTexture;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.network.model.Moment;

import java.io.File;
import java.io.IOException;


public class VideoPlayer extends TextureView implements TextureView.SurfaceTextureListener {

    private static String TAG = "VideoPlayer";
    private MediaMetadataRetriever fetchVideoInfo;

    /**This flag determines that if current VideoPlayer object is first item of the list if it is first item of list*/
    boolean isFirstListItem;

    boolean isLoaded;
    boolean isMuted;
    boolean isMpPrepared;

    IVideoPreparedListener iVideoPreparedListener;

    Moment video;
    String url;
    MediaPlayer mp;
    Surface surface;
    SurfaceTexture s;
    Context mContext;
    private boolean fullscreen =false;
    private String height, width;
    private int intWidth,intHeight;
    private int newHeight, newWidth;

    public boolean isFullscreen() {
        return fullscreen;
    }

    public void setFullscreen(boolean fullscreen) {
        this.fullscreen = fullscreen;
    }

    public VideoPlayer(Context context) {
        super(context);
        mContext = context;
    }
    public VideoPlayer(Context context,String url) {
        super(context);
        mContext = context;
        this.url = url;
    }
    public VideoPlayer(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    public void setIsLoaded(boolean loaded)
    {
        this.isLoaded = loaded;
    }
    public Moment getVideo()
    {        return video;
    }
    public void loadVideo(String localPath,Moment video) {

        this.url = localPath;
        this.video = video;
        isLoaded = true;

        if (this.isAvailable()) {
            prepareVideo(getSurfaceTexture());
        }

        setSurfaceTextureListener(this);
    }

    @Override
    public void onSurfaceTextureAvailable(final SurfaceTexture surface, int width, int height) {
        isMpPrepared = false;
        prepareVideo(surface);
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {

        if(mp!=null)
        {
            mp.stop();
            mp.reset();
            mp.release();
            mp = null;
        }

        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }

    public void prepareVideo(SurfaceTexture t)
    {

        this.surface = new Surface(t);
        mp = new MediaPlayer();
        mp.setSurface(this.surface);

        try {
            mp.setDataSource(url);
            mp.prepare();

            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    isMpPrepared = true;
                    mp.setLooping(true);
                    iVideoPreparedListener.onVideoPrepared(video);

                }


            });
        } catch (IllegalArgumentException e1) {
            e1.printStackTrace();
        } catch (SecurityException e1) {
            e1.printStackTrace();
        } catch (IllegalStateException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        try {

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        try {

        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

    }
    public int getHeight(int deviceWidth, int width, int height){
        int newHeight = height;
        try {
            if(width>0)
            newHeight = (deviceWidth*height)/width;
        }catch (NumberFormatException e){
            e.printStackTrace();

        } catch(Exception e) {
            e.printStackTrace();
        }
        return newHeight;
    }

    public int getWidth(int deviceHeight, int width, int height){
        int newWidth = width;
        try {
            if(height>0)
            newWidth = (deviceHeight*width)/height;
        }catch (NumberFormatException e){

        } catch(Exception e) {
            e.printStackTrace();
        }
        return newWidth;
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Configuration configuration = MyApplication.getAppContext().getResources().getConfiguration();
        int screenHeightDp = configuration.screenWidthDp;
        int deviceWidth = Utils.getInstance().convertDpToPixel(screenHeightDp, mContext);
        newWidth = deviceWidth;
        newHeight = newWidth;
        try {
            if (url != null && getVisibility(url)) {
                fetchVideoInfo = new MediaMetadataRetriever();
                fetchVideoInfo.setDataSource(url);
                height = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
                width = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
                String orientation = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
//                Log.e("Orientation", orientation);
                if(orientation.equalsIgnoreCase("90")){
                    intHeight = Utils.getInstance().getIntegerValueOfString(width);
                    intWidth = Utils.getInstance().getIntegerValueOfString(height);
                }else {
                    intWidth = Utils.getInstance().getIntegerValueOfString(width);
                    intHeight = Utils.getInstance().getIntegerValueOfString(height);
                }
                if (intWidth < deviceWidth && !fullscreen) {
                    float height = intHeight;
                    float width = intWidth;
                    height = (deviceWidth / width) * height;
                    intHeight = (int) height;
                    intWidth = deviceWidth;
                }

//                Log.i("screen width", "width size: " + deviceWidth + "====" + intWidth + 'x' + intHeight + "\n");
                newWidth = getDefaultSize(intWidth, widthMeasureSpec);
                newHeight = getDefaultSize(intHeight, heightMeasureSpec);


                if (intWidth > 0 && intHeight > 0) {
                    if (intWidth * newHeight > newWidth * intHeight) {
                        // Log.i("@@@", "image too tall, correcting");
                        newHeight = newWidth * intHeight / intWidth;
                    } else if (intWidth * newHeight < newWidth * newHeight) {
                        // Log.i("@@@", "image too wide, correcting");
                        newWidth = newHeight * intWidth / intHeight;
                    } else {
                        newHeight = intHeight;
                        newWidth = intWidth;
                        // Log.i("@@@", "aspect ratio is correct: " +
                        // width+"/"+height+"="+
                        // mVideoWidth+"/"+mVideoHeight);
                    }
                }
            }
        } catch (IllegalStateException e) {
        } catch (Exception e) {
        }
        if (newWidth == 0 && newHeight == 0) {
            newWidth = 640;
            newHeight = 480;
        } else if (newHeight == 0) {
            newHeight = 480;
        } else if (newWidth == 0) {
            newWidth = (newHeight * 16) / 9;
        }
        setMeasuredDimension(newWidth, newHeight);
    }
    private boolean getVisibility(String filePath) {
        boolean visible = false;
        if (filePath != null && !filePath.isEmpty()) {
//            String filePath = Utils.getInstance().getLocalContentPath(fileName);
            File vidFile = new File(filePath);
            if (vidFile != null && vidFile.exists()) {
                visible = true;
            }
        }
        return visible;
    }


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
    }

    public boolean startPlay() {
        try {
            if (mp != null)
                if (!mp.isPlaying()) {
                    mp.start();
                    return true;
                }
        } catch (IllegalStateException e) {

        } catch (Exception e) {

        }
        return false;
    }

    public void pausePlay() {
        try {
            if (mp != null)
                if (mp.isPlaying())
                    mp.pause();
        } catch (IllegalStateException e) {

        } catch (Exception e) {

        }
    }

    public void stopPlay() {
        try {
            if (mp != null)
                mp.stop();
        } catch (IllegalStateException e) {

        } catch (Exception e){

        }
    }
    public void reset() {
        try {
            if (mp != null)
                mp.reset();
        } catch (IllegalStateException e) {

        } catch (Exception e){

        }
    }

    public void changePlayState()
    {
        if(mp!=null) {
            if (mp.isPlaying()) {
                if (!isMuted) {
                    isMuted = true;
                    mp.setVolume(1f, 1f);
                } else {
                    isMuted = false;
                    mp.setVolume(0f, 0f);
                }
            }else{
                mp.start();
            }
        }


    }

    public void setOnVideoPreparedListener(IVideoPreparedListener iVideoPreparedListener) {
        this.iVideoPreparedListener = iVideoPreparedListener;
    }


}