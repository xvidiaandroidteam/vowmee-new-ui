package net.xvidia.vowmee.videoplayer;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.amazonaws.mobile.util.ThreadUtils;

import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.network.model.Moment;

import java.io.File;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;

public class VideoPlayerController {

    private static String TAG = "VideoPlayerController";

    Context context;
    int currentPositionOfItemToPlay = 0;
    Moment currentPlayingVideo;
//    private int screenWidthPixels,screenHeightPixelsDefault;
    private Map<String, VideoPlayer> videos = Collections.synchronizedMap(new WeakHashMap<String, VideoPlayer>());
    private Map<String, ProgressBar> videosSpinner = Collections.synchronizedMap(new WeakHashMap<String, ProgressBar>());
    private Map<String, ImageView> videoThumbnail = Collections.synchronizedMap(new WeakHashMap<String, ImageView>());
//    private Map<String, LinearLayout> videoThumbnailLayout = Collections.synchronizedMap(new WeakHashMap<String, LinearLayout>());
//    private Map<String, Boolean> videoAvailable = Collections.synchronizedMap(new WeakHashMap<String, Boolean>());
    public VideoPlayerController(Context context) {
        this.context = context;
//        Configuration configuration = MyApplication.getAppContext().getResources().getConfiguration();
//        int screenWidthDp = configuration.screenWidthDp;
//        screenWidthPixels= Utils.getInstance().convertDpToPixel(screenWidthDp, context);
//        screenHeightPixelsDefault = (screenWidthPixels / 3 + screenWidthPixels / 15);

    }

    public void loadVideo(Moment video, VideoPlayer videoPlayer, ProgressBar progressBar) {

        //Add video to map
        videos.put(video.getIndexPosition(), videoPlayer);
        videosSpinner.put(video.getIndexPosition(), progressBar);

        handlePlayBack(video,false);
    }
    public void loadVideoWithThumbnail(Moment video, VideoPlayer videoPlayer, ProgressBar progressBar,ImageView thumbnail,LinearLayout layoutThumbnail) {

        //Add video to map
        videos.put(video.getIndexPosition(), videoPlayer);
        videosSpinner.put(video.getIndexPosition(), progressBar);
        videoThumbnail.put(video.getIndexPosition(), thumbnail);
//        videoThumbnailLayout.put(video.getIndexPosition(), layoutThumbnail);
//        videoAvailable.put(video.getIndexPosition(), false);

//        handlePlayBack(video,true);
    }
    public void loadVideoFullScreen(Moment video, VideoPlayer videoPlayer, ProgressBar progressBar) {

        //Add video to map
        video.setIndexPosition("0");
        videos.put(video.getIndexPosition(), videoPlayer);
        videosSpinner.put(video.getIndexPosition(), progressBar);
//        layoutThumbnail.put(video.getIndexPosition(), thumbnailLayout);

        handlePlayBack(video,true);
    }
    public void loadVideoReset(Moment video, VideoPlayer videoPlayer) {

        //Add video to map
        videos.put(video.getIndexPosition(), videoPlayer);
    }
//This method would check two things
//First if video is downloaded or its local path exist
//Second if the videoplayer of this video is currently showing in the list or visible

    public void handlePlayBack(final Moment video, final boolean image) {
        if (image) {
            if (isVideoDownloaded(video)) {
                // then check if it is currently at a visible or playable position in the listview
                if (isVideoVisible(video)) {
                    ImageView imageView = videoThumbnail.get(video.getIndexPosition());
                    VideoPlayer videoPlayer1 = videos.get(video.getIndexPosition());
                    if (imageView != null)
                        imageView.setVisibility(View.GONE);
                    if (videoPlayer1 != null)
                        videoPlayer1.setVisibility(View.VISIBLE);
                    playVideo(video);
                }
            } else {
//                        if (isVideoVisible(video)) {
                Bitmap originalThumbnail = FileCache.getInstance().getBitmapFromMemCache(video.getThumbNail());
                if(originalThumbnail==null) {
                    originalThumbnail = Utils.getInstance().decodeScaledBitmapFromSdCard(Utils.getInstance().getLocalContentPath(video.getThumbNail()));
                    try{
                        originalThumbnail = Utils.getInstance().setImageDimension(originalThumbnail);
                    }catch (OutOfMemoryError e){

                    }catch (Exception e){

                    }
                    if(originalThumbnail!=null)
                        FileCache.getInstance().addBitmapToMemoryCache(video.getThumbNail(),originalThumbnail);
                }
//                Bitmap blurredBitmap = originalThumbnail;
//                if(blurredBitmap != null)
//                    BlurBitmap.blur(context, originalThumbnail);

//                            String filePath = Utils.getInstance().getLocalContentPath(video.getThumbNail());
                ImageView imageView = videoThumbnail.get(video.getIndexPosition());
//                            LinearLayout imageViewLayout = videoThumbnailLayout.get(video.getIndexPosition());
                VideoPlayer videoPlayer1 = videos.get(video.getIndexPosition());
                if (videoPlayer1 != null)
                    videoPlayer1.setVisibility(View.GONE);
//                            if (imageViewLayout != null){
//                                imageViewLayout.setBackground(new BitmapDrawable(MyApplication.getAppContext().getResources(), blurredBitmap));
////                                imageViewLayout.setBackground(Utils.getInstance().loadImageFromSdCard(Utils.getInstance().getLocalContentPath(video.getThumbNail())));
////////                                imageView.setVisibility(View.VISIBLE);
//                            }
                if (imageView != null&&originalThumbnail!=null){
                    imageView.setImageBitmap(originalThumbnail);
//                    imageView.setMinimumHeight(originalThumbnail.getHeight());
                    imageView.setVisibility(View.VISIBLE);
                }
                showProgressSpinner(video);
//                        }else{
//                            showProgressSpinner(video);
//                        }
            }
        } else {
            //Check if video is available
            if (isVideoDownloaded(video)) {
                if (isVideoVisible(video)) {
//                    ImageView imageView = videoThumbnail.get(video.getIndexPosition());
//                            LinearLayout imageViewLayout = videoThumbnailLayout.get(video.getIndexPosition());
                    VideoPlayer videoPlayer1 = videos.get(video.getIndexPosition());
//                            if (imageViewLayout != null) {
//                                imageViewLayout.setBackground(new BitmapDrawable(MyApplication.getAppContext().getResources(), blurredBitmap));
////                                imageView.setVisibility(View.VISIBLE);
//                            }
//                    if (imageView != null)
//                        imageView.setVisibility(View.GONE);
                    if (videoPlayer1 != null)
                        videoPlayer1.setVisibility(View.VISIBLE);
                    playVideo(video);
                }
            }
        }
    }

    public void pauseVideo(){
       if(currentPlayingVideo!=null)
        {
            VideoPlayer videoPlayer1 = videos.get(currentPlayingVideo.getIndexPosition());
            if(videoPlayer1 !=null)
                videoPlayer1.pausePlay();
//            else
//                Log.i("pauseVideo", "failed");
        }
    }

    private void playVideo(final Moment video)
    {
        //Before playing it check if this video is already playing

        if(currentPlayingVideo != video)
        {
            //Start playing new url
            if(videos.containsKey(video.getIndexPosition()))
            {
                final VideoPlayer videoPlayer2 = videos.get(video.getIndexPosition());
                String path = Utils.getInstance().getLocalContentPath(video.getLink());
                File vidFile = new File(path);
//                Bitmap vidThumbnail = Utils.getInstance().getVideoThumbnail(path);
                if(vidFile == null)
                    return;
                String localPath = vidFile.getAbsolutePath();
                if(!videoPlayer2.isLoaded)
                {
//                    if(vidThumbnail!=null)
//                        videoPlayer2.setVideoSize(vidThumbnail.getWidth(),vidThumbnail.getHeight());
                    videoPlayer2.loadVideo(localPath, video);
                    videoPlayer2.setOnVideoPreparedListener(new IVideoPreparedListener() {
                        @Override
                        public void onVideoPrepared(Moment mVideo) {

                            //Pause current playing video if any
                            if(video.getIndexPosition() == mVideo.getIndexPosition())
                            {
                                if(currentPlayingVideo!=null)
                                {
                                    VideoPlayer videoPlayer1 = videos.get(currentPlayingVideo.getIndexPosition());

                                    if(videoPlayer1 !=null) {
                                        videoPlayer1.pausePlay();
                                    }
                                }
                                videoPlayer2.mp.setVolume(0f,0f);
                                videoPlayer2.mp.start();
                                videoPlayer2.setVisibility(View.VISIBLE);
                                currentPlayingVideo = mVideo;
                            }


                        }
                    });
                }else{
                    //Pause current playing video if any
                    if(currentPlayingVideo!=null)
                    {
                        VideoPlayer videoPlayer1 = videos.get(currentPlayingVideo.getIndexPosition());
                        if(videoPlayer1 !=null)
                            videoPlayer1.pausePlay();
                    }

                    videoPlayer2.setVisibility(View.VISIBLE);
                    boolean isStarted = videoPlayer2.startPlay();

//                    Log.i(TAG, "Started playing Video: " + video.getCaption());
//                    Log.i(TAG, "Started playing Video Index: " + video.getIndexPosition() +"isStarted " +isStarted);
                    currentPlayingVideo = video;
                }
            }
        }
        else
        {
            VideoPlayer videoPlayer1 = videos.get(currentPlayingVideo.getIndexPosition());
            if(videoPlayer1 !=null) {
                videoPlayer1.setVisibility(View.VISIBLE);
                videoPlayer1.startPlay();
            }
            //Log.i(TAG, "Already playing Video: " + video.getUrl());
        }

    }

    private boolean isVideoVisible(Moment video) {
        int positionOfVideo = Utils.getInstance().getIntegerValueOfString(video.getIndexPosition());

        if(currentPositionOfItemToPlay == positionOfVideo)
            return true;
//        Log.i("Moment ", "" + video.toString() + "positionOfVideo " + positionOfVideo + " currentPositionOfItemToPlay " + currentPositionOfItemToPlay);
        return false;
    }

    private boolean isVideoDownloaded(Moment video) {

        if(video == null)
            return false;
//        boolean isVideoAvailable = DataStorage.getInstance().readPreferences(context, video.getLink(), false);
        String filePath = Utils.getInstance().getLocalContentPath(video.getLink());
        File file = new File(filePath);
        boolean isVideoAvailable = file.exists();
        if(isVideoAvailable)
        {
            hideProgressSpinner(video);
            return true;
        }else{
            showProgressSpinner(video);
        }
//        Log.i("isVideoDownloaded ", "isVideoAvailable " + isVideoAvailable + "Moment" + video.toString());

        return false;
    }

    private void showProgressSpinner(final Moment video) {
        ThreadUtils.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ProgressBar progressBar = videosSpinner.get(video.getIndexPosition());
                if(progressBar!=null)
                    progressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    private void hideProgressSpinner(Moment video) {

        ProgressBar progressBar = videosSpinner.get(video.getIndexPosition());
        ImageView layout = videoThumbnail.get(video.getIndexPosition());
        VideoPlayer videoPlayer = videos.get(video.getIndexPosition());
        if(layout != null)
            layout.setVisibility(View.GONE);
        if(videoPlayer!=null)
         videoPlayer.setVisibility(View.VISIBLE);
        if(progressBar!=null && progressBar.isShown())
        {
            if(progressBar != null)
            progressBar.setVisibility(View.GONE);
        }
    }

    public void setcurrentPositionOfItemToPlay(int mCurrentPositionOfItemToPlay) {
        currentPositionOfItemToPlay = mCurrentPositionOfItemToPlay;
    }
    public int getCurrentPositionOfItemToPlay() {
        return currentPositionOfItemToPlay;
    }
}