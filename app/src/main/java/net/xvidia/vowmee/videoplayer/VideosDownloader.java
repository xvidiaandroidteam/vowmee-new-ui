package net.xvidia.vowmee.videoplayer;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;

import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.network.model.Moment;

import java.io.File;
import java.util.List;

public class VideosDownloader {

    private static String TAG = "VideosDownloader";

    Context context;
    IVideoDownloadListener iVideoDownloadListener;
    private int screenWidthPixels;
    //    int size = 0;
    int startPos = 0;
    List<Moment> momentList;
    private UserFileManager userFileManager;
    boolean firstTime = false;

    public VideosDownloader(Context context) {
        this.context = context;
//        fileCache = new FileCache(context);
        userFileManager = Utils.getInstance().getUserFileManager();
        if (userFileManager == null) {
            Utils.getInstance().initialiseAmazon();
            userFileManager = Utils.getInstance().getUserFileManager();

        }

    }


/////////////////////////////////////////////////////////////////
// Start downloading all videos from given urls

    public void startVideosDownloading(final List<Moment> arrayList, boolean first) {

        this.momentList = arrayList;
        startPos = 0;
        firstTime = first;
        initDownloadImages();


//        }
    }

    private void initDownloadImages() {
        if (momentList == null)
            return;

        if (startPos < momentList.size()) {
            final Moment video = momentList.get(startPos);
            if (video != null) {

                if (video.getThumbNail() != null) {
                    String fileImage = Utils.getInstance().getLocalContentPath(video.getThumbNail());
                    File fileThumbnail = new File(fileImage);
                    if (fileThumbnail.exists()) {
//                    DataStorage.getInstance().savePreferences(context, video.getLink(), true);
//                    video.setIndexPosition(""+startPos);
                        startPos = startPos + 1;
                        initDownloadImages();
                        if (firstTime)
                            iVideoDownloadListener.onVideoDownloaded(video, true);
                    } else {
                        if (userFileManager == null)
                            return;
                        startThumbnailDownloading(video);
                    }
                } else {
                    startPos = startPos + 1;
                    initDownloadImages();
                }
            } else {
                startPos = startPos + 1;
                initDownloadImages();
            }
        } else {
            startPos = 0;
            initDownloader();
        }
    }

    private void initDownloader() {
        if (momentList == null)
            return;

        if (startPos < momentList.size()) {
            final Moment video = momentList.get(startPos);
            if(video!=null) {
                if (video.getLink() != null) {
                    String filePath = Utils.getInstance().getLocalContentPath(video.getLink());
                    File file = new File(filePath);
                    if (file.exists()) {
                        if (firstTime)
                            iVideoDownloadListener.onVideoDownloaded(video, false);
                        startPos = startPos + 1;
                        initDownloader();
                    } else {
//                    String fileImage = Utils.getInstance().getLocalContentPath(video.getThumbNail());
//                    File fileThumbnail = new File(fileImage);
//                    if (fileThumbnail.exists()) {
////                    DataStorage.getInstance().savePreferences(context, video.getLink(), true);
////                    video.setIndexPosition(""+startPos);
//                        iVideoDownloadListener.onVideoDownloaded(video, true);
//                    }else{
//                        startThumbnailDownloading(video);
//                    }
                        if (userFileManager == null)
                            return;
                        startVideosDownloading(video);
                    }
                } else {
                    startPos = startPos + 1;
                    initDownloader();
                }
            } else {
                startPos = startPos + 1;
                initDownloader();
            }
        }
    }

    public void startVideosDownloading(final Moment video) {
        if (video == null)
            return;
        userFileManager = Utils.getInstance().getUserFileManager();
        if (userFileManager == null) {
            Utils.getInstance().initialiseAmazon();
            userFileManager = Utils.getInstance().getUserFileManager();

        }
        if (userFileManager == null)
            return;

        long fileSize = (1024 * 1024 * 5);
        userFileManager.getContent(video.getLink(), fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                Log.i("FIle download", "success " + contentItem.getFilePath());
//                    Log.i("FIle download", "getLocalContentPath " + userFileManager.getLocalContentPath());
//                    DataStorage.getInstance().savePreferences(context, video.getLink(), true);
                iVideoDownloadListener.onVideoDownloaded(video, false);
                startPos = startPos + 1;
                initDownloader();
            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
                Log.i("FIle download error" + filePath, ex.getMessage());
                startPos = startPos + 1;
                initDownloader();
            }
        });

    }

    public void startThumbnailDownloading(final Moment video) {
        if (video == null)
            return;
        userFileManager = Utils.getInstance().getUserFileManager();
        if (userFileManager == null) {
            Utils.getInstance().initialiseAmazon();
            userFileManager = Utils.getInstance().getUserFileManager();

        }
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 1024);
        userFileManager.getContent(video.getThumbNail(), fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
//                Log.i("FIle download", "success " + contentItem.getFilePath());
//                Log.i("FIle download", "getLocalContentPath " + userFileManager.getLocalContentPath());
                String fileName = contentItem.getFilePath();
                Bitmap originalThumbnail =  Utils.getInstance().decodeScaledBitmapFromSdCard(Utils.getInstance().getLocalContentPath(fileName));
                    try{
                        originalThumbnail =Utils.getInstance().setImageDimension(originalThumbnail);// Bitmap.createScaledBitmap(originalThumbnail,screenWidthPixels,screenHeightPixelsDefault,false);
                    }catch (OutOfMemoryError e){

                    }catch (Exception e){

                    }
                    if(originalThumbnail!=null)
                        FileCache.getInstance().addBitmapToMemoryCache(fileName,originalThumbnail);
                startPos = startPos + 1;
                initDownloadImages();
                iVideoDownloadListener.onVideoDownloaded(video, true);
            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
                Log.i("FIle download error " + filePath, ex.getMessage());
                startPos = startPos + 1;
                initDownloadImages();
            }
        });

    }


/////////////////////////////////////////////////////////////////

   /* private String downloadVideo(String urlStr) {
        URL url = null;
        File file = null;
        try {
            file = fileCache.getFile(urlStr);
            url = new URL(urlStr);
            long startTime = System.currentTimeMillis();
            URLConnection ucon = null;
            ucon = url.openConnection();
            InputStream is = ucon.getInputStream();
            BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
            FileOutputStream outStream = new FileOutputStream(file);
            byte[] buff = new byte[5 * 1024];

            //Read bytes (and store them) until there is nothing more to read(-1)
            int len;
            while ((len = inStream.read(buff)) != -1) {
                outStream.write(buff, 0, len);
            }

            //clean up
            outStream.flush();
            outStream.close();
            inStream.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file.getAbsolutePath();
    }
*/

    public void setOnVideoDownloadListener(IVideoDownloadListener iVideoDownloadListener) {
        this.iVideoDownloadListener = iVideoDownloadListener;
    }

    public class downloadFilesInBackground extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            initDownloader();
            return null;
        }
    }

}
