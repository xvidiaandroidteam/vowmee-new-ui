package net.xvidia.vowmee.videoplayer;

import net.xvidia.vowmee.network.model.Moment;

/**
 * A login screen that offers login via email/password.
 */
public interface IVideoDownloadListener {
    public void onVideoDownloaded(Moment moment,boolean image);
}