package net.xvidia.vowmee.videoplayer;

import net.xvidia.vowmee.network.model.Moment;

public interface IVideoPreparedListener {

    public void onVideoPrepared(Moment video);
}