package net.xvidia.vowmee;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.mobile.AWSConfiguration;
import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.mobile.content.UserFileManager;
import com.amazonaws.mobile.user.IdentityManager;
import com.amazonaws.mobile.user.IdentityProvider;
import com.amazonaws.mobile.user.signin.FacebookSignInProvider;
import com.amazonaws.mobile.user.signin.SignInManager;
import com.amazonaws.mobile.user.signin.TwitterSignInProvider;
import com.amazonaws.mobile.user.signin.VowmeeSignInProvider;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.Digits;
import com.digits.sdk.android.DigitsAuthConfig;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsOAuthSigning;
import com.digits.sdk.android.DigitsSession;
import com.facebook.appevents.AppEventsLogger;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.contactsync.SyncUtils;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.Login;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.network.model.Register;
import net.xvidia.vowmee.network.model.ResponseSocialAuth;
import net.xvidia.vowmee.network.model.SocialAuthFacebook;
import net.xvidia.vowmee.network.model.SocialAuthTwitter;
import net.xvidia.vowmee.storage.DataStorage;
import net.xvidia.vowmee.storage.sqlite.manager.DataCacheManager;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A login screen that offers login via email/password.
 */
public class RegisterActivity extends AppCompatActivity{

    private SignInManager signInManager;
    private TwitterLoginButton twitterLoginButton;
//    private ImageButton mPasswordVisibilityView;
    private RelativeLayout customTwitterLogin;
    private RelativeLayout facebookLoginButton;
    private TextView digitLoginButton;
    private JsonObjectRequest request = null;
    private static ArrayList<String> emails;
    private String mPhoneNumber;
//    private ProgressDialog progressDialog;
//    private MediaPlayer mp = null;
    private Activity activity;
    private ProgressBar loadingView;
    public static Map<String, String> authHeaders;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    /**
     * SignInResultsHandler handles the final result from sign in. Making it static is a best
     * practice since it may outlive the SplashActivity's life span.
     */
    private class SignInResultsHandler implements IdentityManager.SignInResultsHandler {
        @Override
        public void onSuccess(final IdentityProvider provider) {
            // Load user name and image.
            if(provider == null)
                return;
            showProgressBar();
            AWSMobileClient.defaultMobileClient()
                    .getIdentityManager().loadUserInfoAndImage(provider, new Runnable() {
                @Override
                public void run() {
                    String bucket = AWSConfiguration.AMAZON_S3_USER_FILES_BUCKET;
                    String prefix = "public/";
                    AWSMobileClient.defaultMobileClient()
                            .createUserFileManager(bucket,
                                    prefix,
                                    new UserFileManager.BuilderResultHandler() {

                                        @Override
                                        public void onComplete(final UserFileManager userFileManager) {
                                            Utils.getInstance().setUserFileManager(userFileManager);
                                            if(provider.getDisplayName().equalsIgnoreCase("Facebook")) {
                                                sendFacebookRegisterRequest(provider.getFacebookUserId(), provider.getToken(), provider.getFacbookTokenExpiryDate());
                                            }else if(provider.getDisplayName().equalsIgnoreCase("Twitter")){
                                                sendTwitterRegisterRequest(provider.getTwitterUserId(), provider.getToken(), provider.getSecreteToken());

                                            }else if(provider.getDisplayName().equalsIgnoreCase("Digit")){
                                                getProfileRequest();
                                            }
                                        }
                                    });
                }
            });


        }
        @Override
        public void onCancel(final IdentityProvider provider) {
            hideProgressBar();
            Utils.getInstance().displayToast(MyApplication.getAppContext(),String.format("Sign-in with %s cancelled.",
                    provider.getDisplayName()), Utils.LONG_TOAST);

//            Toast.makeText(RegisterActivity.this, String.format("Sign-in with %s canceled.",
//                    provider.getDisplayName()), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(final IdentityProvider provider, final Exception ex) {
            hideProgressBar();
            if(!provider.getDisplayName().equalsIgnoreCase("Digit")) {
//                final AlertDialog.Builder errorDialogBuilder = new AlertDialog.Builder(RegisterActivity.this);
//                errorDialogBuilder.setTitle("Sign-In Error");
//                errorDialogBuilder.setMessage(
//                        String.format("Sign-in with %s failed.\n%s", provider.getDisplayName(), ex.getMessage()));
//                errorDialogBuilder.setNeutralButton("Ok", null);
//                errorDialogBuilder.show();
                Utils.getInstance().displayToast(MyApplication.getAppContext(),String.format("Sign-in with %s cancelled.",
                        provider.getDisplayName()), Utils.LONG_TOAST);

            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
//        initialiseSocialNetworks();
        setContentView(R.layout.new_activity_login_phone);
        activity = this;
        loadingView = (ProgressBar) findViewById(R.id.progressBar);
        loadingView.setVisibility(View.GONE);
        DataStorage.getInstance().setRegisteredFlag(false);
        initialiseLoginView();
//        DigitsAuthButton digitsAuthButton = (DigitsAuthButton) findViewById(R.id.auth_button);
//        if(Digits.getSessionManager() != null)
//            Digits.getSessionManager().clearActiveSession();
        resetLoginDetails();
    }

    private void resetLoginDetails(){
    DataStorage.getInstance().setDigitHeaderAuth("");
    DataStorage.getInstance().setDigitHeaderProvider("");
    DataStorage.getInstance().setCognitoDigitToken("");
    DataStorage.getInstance().setCognitoDigitId("");
    DataStorage.getInstance().setAppSocialId("");
    DataStorage.getInstance().setLastLoginType("");
    DataStorage.getInstance().setUsername("");
    DataStorage.getInstance().setUserUUID("");
}

    private void initialiseLoginView() {
        customTwitterLogin = (RelativeLayout) findViewById(R.id.customer_twitter_login_button);
        twitterLoginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
        facebookLoginButton =(RelativeLayout) findViewById(R.id.facebook_login_button);
        digitLoginButton =(TextView) findViewById(R.id.digit_login_button);
        signInManager = new SignInManager(this);
        signInManager.setResultsHandler(this, new SignInResultsHandler());
        // Initialize sign-in buttons.
        signInManager.initializeTwitterSignInButton(TwitterSignInProvider.class, customTwitterLogin, twitterLoginButton);
        signInManager.initializeSignInButton(FacebookSignInProvider.class, facebookLoginButton);
        signOut();
        int paddingLeftRight = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_15);
//        int paddingTopBottom = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_15);
//        customTwitterLogin.setPadding(paddingLeftRight, paddingLeftRight, paddingLeftRight,paddingLeftRight);
//        facebookLoginButton.setPadding(paddingLeftRight, paddingLeftRight, paddingLeftRight, paddingLeftRight);
        digitLoginButton.setPadding(paddingLeftRight, paddingLeftRight, paddingLeftRight, paddingLeftRight);
        twitterLoginButton.setVisibility(View.INVISIBLE);
        final AuthCallback digitCallback = new AuthCallback() {
            @Override
            public void success(DigitsSession session, String phone) {
//                Toast.makeText(getApplicationContext(), "Authentication successful for "
//                        + phone, Toast.LENGTH_LONG).show();
//                phoneNumber = phone;
                showProgressBar();
                TwitterAuthConfig authConfig = TwitterCore.getInstance().getAuthConfig();
                TwitterAuthToken authToken = (TwitterAuthToken) session.getAuthToken();
                String token, secret;
                if (authToken != null) {
//                    token = authToken.token;
//                    secret = authToken.secret;
                    DigitsOAuthSigning oauthSigning = new DigitsOAuthSigning(authConfig, authToken);
                    authHeaders = oauthSigning.getOAuthEchoHeadersForVerifyCredentials();
                    if(phone ==null){
                        phone = DataStorage.getInstance().getUsername();
                    }
                    DataStorage.getInstance().setDigitHeaderProvider(authHeaders.get(AppConsatants.DIGIT_HEADER_PROVIDER));
                    DataStorage.getInstance().setDigitHeaderAuth(authHeaders.get(AppConsatants.DIGIT_HEADER_AUTH));
                    sendDigits(authHeaders, phone);
                }else{
                    Utils.getInstance().displayToast(MyApplication.getAppContext(),"Authentication successful for but token null. Please retry"
                            + phone,Utils.SHORT_TOAST);
//                    Toast.makeText(getApplicationContext(), "Authentication successful for but token null. Please retry"
//                            + phone, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failure(DigitsException e) {
                e.printStackTrace();
//                Log.d("GCM UPDATE", "Response: " + e.getMessage());
                Utils.getInstance().displayToast(MyApplication.getAppContext(),getString(R.string.error_message_authentication_failed), Utils.LONG_TOAST);

//                Toast.makeText(getApplicationContext(), "Authentication failed. Please retry"+e.getMessage(), Toast.LENGTH_LONG).show();
            }

        };
//        digitsAuthButton.setCallback(digitCallback);
//        digitsAuthButton.setAuthTheme(R.style.AppTheme)
        digitLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DigitsAuthConfig.Builder digitsAuthConfigBuilder = new DigitsAuthConfig.Builder()
                        .withAuthCallBack(digitCallback)
                        .withPhoneNumber("")
                        .withThemeResId(R.style.AppTheme);

                Digits.authenticate(digitsAuthConfigBuilder.build());
            }
        });
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(RegisterActivity.this)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, okListener)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS:
            {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CALL_PHONE, PackageManager.PERMISSION_GRANTED);


                // Fill with results
                for (int i = 0; i < permissions.length; i++){
                    perms.put(permissions[i],grantResults[i]);
                }
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                } else {
                    // Permission Denied
//                    Toast.makeText(RegisterActivity.this, "Some Permission is Denied", Toast.LENGTH_LONG)
//                            .show();
                    Utils.getInstance().displayToast(MyApplication.getAppContext(),getString(R.string.error_message_permission_denied), Utils.LONG_TOAST);

                }
                // Check for ACCESS_FINE_LOCATION

            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

//    private String getEmailId(){
//        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
//        String possibleEmail="";
//        emails = new ArrayList<>();
//        Account[] accounts = AccountManager.get(this).getAccounts();
//        for (Account account : accounts) {
//            emails.add(account.name);
//            if (emailPattern.matcher(account.name).matches()) {
//                possibleEmail = account.name;
//            }
//        }
//        return possibleEmail;
//    }
//
//    private boolean isEmailValid(String email) {
//        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
//            if (emailPattern.matcher(email).matches()) {
//                return true;
//            }
//        return false;
//    }
//
//    private boolean isPasswordValid(String password) {
//        //TODO: Replace this with your own logic
//        return password.length() > 4;
//    }

    private void signOut(){
        AWSMobileClient awsMobileClient = AWSMobileClient.defaultMobileClient();

        if (awsMobileClient != null && awsMobileClient.getIdentityManager() !=null) {
            awsMobileClient.getIdentityManager().signOut();
        }
        if(Digits.getSessionManager() != null)
            Digits.getSessionManager().clearActiveSession();
        DataStorage.getInstance().setRegisteredFlag(false);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (signInManager != null) {
            showProgressBar();
            signInManager.handleActivityResult(requestCode, resultCode, data);
        }
    }

    private void sendRegisterRequest(Register register) {
        try {
            if(register == null) {
                return;
            }
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_SOCIAL_REGISTER);

            String fb =register.getFaceBookId();
            long tw =register.getTwitterId();
            if(fb != null && !fb.isEmpty()){
                DataStorage.getInstance().setLastLoginType(AppConsatants.PROFILE_FACEBOOK);
                DataStorage.getInstance().setAppSocialId(fb);
            }else if(tw>0){
                DataStorage.getInstance().setLastLoginType(AppConsatants.PROFILE_TWITTER);
                DataStorage.getInstance().setAppSocialId(""+tw);
            }
                    ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(register);
            } catch (IOException e) {
                e.printStackTrace();
            }
//            Log.i("Register request",jsonObject);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        DataStorage.getInstance().setRegisteredFlag(true);
                        String username = obj.getUsername();
                        if(username != null && !username.isEmpty()) {
                            DataStorage.getInstance().setRegisteredFlag(true);
                            DataStorage.getInstance().setUsername(username);
                            DataStorage.getInstance().setUserUUID(obj.getUuid());
                            ModelManager.getInstance().setOwnProfile(obj);
                            startActivity(new Intent(RegisterActivity.this, TimelineProfile.class));
                            finish();
                        }else{
                            showError(getString(R.string.error_general));
                        }
                    }
                    hideProgressBar();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    signOut();
                    if (error != null && error.networkResponse != null)
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND) {
//                            Log.e("VolleyError ", "" + error.networkResponse.statusCode);
                            showError(error.getMessage());
                        } else if (error.networkResponse.statusCode == HttpURLConnection.HTTP_CONFLICT) {
//                            Log.e("VolleyError ", "" + error.networkResponse.statusCode);
                            showError(getString(R.string.error_user_exists));

                }else {
                        showError(getString(R.string.error_general));
                    }
                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }
    private String getMy10DigitPhoneNumber(){
        String s = getMyPhoneNumber();
        return s != null && s.length() > 2 ? s : null;
    }
    private String getMyPhoneNumber(){
        TelephonyManager mTelephonyMgr;
        mTelephonyMgr = (TelephonyManager)
                getSystemService(Context.TELEPHONY_SERVICE);
        String numb =mTelephonyMgr.getLine1Number();
        return numb;
    }

    @Override
    protected void onResume() {
        super.onResume();

        final AWSMobileClient awsMobileClient = AWSMobileClient.defaultMobileClient();

        if (awsMobileClient == null) {
            // At this point, the app is resuming after being shut down and the onCreate
            // method has already fired an intent to launch the splash activity. The splash
            // activity will refresh the user's credentials and re-initialize all required
            // components. We bail out here, because without that initialization, the steps
            // that follow here would fail. Note that if you don't have any features enabled,
            // then there may not be any steps here to skip.
            return;
        }

        // pause/resume Mobile Analytics collection
        awsMobileClient.handleOnResume();
        AppEventsLogger.activateApp(this);
        setGoogleAnalyticScreen("Register/Login");
    }



    @Override
    protected void onPause() {
        super.onPause();

        // Obtain a reference to the mobile client.
        final AWSMobileClient awsMobileClient = AWSMobileClient.defaultMobileClient();

        if (awsMobileClient != null) {
            // pause/resume Mobile Analytics collection
            awsMobileClient.handleOnPause();
        }
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
        hideProgressBar();
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */

//    private void sendRegisterRequest(String name, String emailId, String contactNo, String facebookId,long twitterId){
//        try {
//
//            String url =ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_SOCIAL_REGISTER);
//            Login register = new Login();
//            register.setName(name);
//            register.setEmail(emailId);
//            register.setContactNo(contactNo);
//            register.setFaceBookId(facebookId);
//            register.setTwitterId(twitterId);
//            register.setUsername(contactNo);
//
//            ObjectMapper mapper = new ObjectMapper();
//            String jsonObject = null;
//            try {
//                jsonObject = mapper.writeValueAsString(register);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
//
//                @Override
//                public void onResponse(JSONObject response) {
//                    ObjectMapper mapper = new ObjectMapper();
//                    ResponseSocialAuth obj = null;
//                    try {
//                        obj = mapper.readValue(response.toString(), ResponseSocialAuth.class);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    String username = obj.getUsername();
//                    Intent desire = new Intent();
//                    desire.setClass(RegisterActivity.this,ProfileActivity.class);
//                    startActivity(desire);
////                startActivityForResult(desire, 500);
////                overridePendingTransition(android.support.design.R.anim.abc_slide_in_bottom,android.support.design.R.anim.abc_slide_in_top);
//                    finish();
//                }
//            }, new Response.ErrorListener() {
//
//                @Override
//                public void onErrorResponse(VolleyError error) {
//
//                    Log.e("VolleyError ", "" + error.networkResponse.statusCode);
//                    if(error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND){
//                        startActivity(new Intent(RegisterActivity.this, RegisterActivity.class));
//                        finish();
//                    }
//                }
//            });
//            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
////			}
//        }catch(Exception e){
//
//        }
//    }

      /*Api Request & Response*/

    private void sendLoginRequest(final String userId,String password){
        try {

            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_AUTH_VOWMEE);
            Login login = new Login(userId,userId);
            login.setUsername(userId);
            login.setPassword(password);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(login);
            } catch (IOException e) {
                e.printStackTrace();
            }
            request= new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    hideProgressBar();
                    String getUsername = obj.getUsername();
//                    Map<String, String> headers = request.getResponseHeaders();
                    if(!getUsername.isEmpty()) {
                        DataStorage.getInstance().setUsername(obj.getUsername());
                        DataStorage.getInstance().setUserUUID(obj.getUuid());
                        String gcmToken = DataStorage.getInstance().getGcmRegId(RegisterActivity.this);
                        if (!gcmToken.isEmpty()) {
                            sendGcmRegistrationToServer(MyApplication.getAppContext(), gcmToken, getUsername);
                        }
                        getProfileRequest();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    if(error!= null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND){
//                        Log.e("VolleyError ", "" + error.networkResponse.statusCode);
                        Intent mIntent = new Intent(RegisterActivity.this, ProfileActivity.class);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                        Bundle mBundle = new Bundle();
                        mBundle.putString(AppConsatants.PROFILE_FACEBOOK, userId);
                        mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, true);
                        mIntent.putExtras(mBundle);
                        startActivity(mIntent);
                        finish();
                    }else {

                        showError(getString(R.string.error_general));
                    }
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        }catch(Exception e){

        }
    }
    private void sendDigits(final Map<String,String> headers,final String phone){
        try {

            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_DIGIT_LOGIN);
            Login login = new Login(phone,phone);
            login.setUsername(phone);
            login.setPassword(phone);
            login.setPhoneNumber(phone);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(login);
            } catch (IOException e) {
                e.printStackTrace();
            }
            request= new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    hideProgressBar();
                    String getUsername = obj.getUsername();
//                    Map<String, String> headers = request.getResponseHeaders();
                    if(getUsername != null&&!getUsername.isEmpty()) {
                        DataStorage.getInstance().setRegisteredFlag(true);
                        DataStorage.getInstance().setUsername(obj.getUsername());
                        DataStorage.getInstance().setUserUUID(obj.getUuid());
                        DataStorage.getInstance().setLastLoginType(AppConsatants.PROFILE_PHONE);
                        signInManager.refreshCredentialsWithProvider(RegisterActivity.this,
                                new VowmeeSignInProvider(), new SignInResultsHandler());
                        String gcmToken = DataStorage.getInstance().getGcmRegId(RegisterActivity.this);
                        if (!gcmToken.isEmpty()) {
                            sendGcmRegistrationToServer(MyApplication.getAppContext(), gcmToken, getUsername);
                        }
                        getProfileRequest();
                    }else{
                        showError(getString(R.string.error_general));
                        signOut();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    if(error!= null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND){
//                        Log.e("VolleyError ", "" + error.networkResponse.statusCode);
                        Intent mIntent = new Intent(RegisterActivity.this, ProfileActivity.class);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                        Bundle mBundle = new Bundle();
                        mBundle.putString(AppConsatants.PROFILE_FACEBOOK, "");
                        mBundle.putLong(AppConsatants.PROFILE_TWITTER, 0);
                        mBundle.putString(AppConsatants.PROFILE_PHONE, phone);
                        mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, true);
                        mIntent.putExtras(mBundle);
                        startActivity(mIntent);
                        finish();
                    }else {
//                        Log.e("VolleyError ", "" + error.getMessage());
                        signOut();
                        showError(getString(R.string.error_general));
                    }
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();

                    String provider = headers.get("X-Auth-Service-Provider");
                    String Authorization =headers.get("X-Verify-Credentials-Authorization");
                    params = headers;
                    return params;
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        }catch(Exception e){

        }
    }

    private void sendFacebookRegisterRequest(final String userId,String accessToken,String expiryDate ){
        try {

            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_AUTH_FACEBOOK);
            SocialAuthFacebook socialAuthFacebook = new SocialAuthFacebook();
            socialAuthFacebook.setAccessToken(accessToken);
            socialAuthFacebook.setUserId(userId);
            socialAuthFacebook.setExpiryDate(expiryDate);
            DataCacheManager.getInstance().clearSocialAuthFacebook();
            DataCacheManager.getInstance().saveSigninFacebookData(socialAuthFacebook);
            DataStorage.getInstance().setLastLoginType(AppConsatants.PROFILE_FACEBOOK);
            DataStorage.getInstance().setAppSocialId(userId);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(socialAuthFacebook);
            } catch (IOException e) {
                e.printStackTrace();
            }
            request= new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseSocialAuth obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), ResponseSocialAuth.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    hideProgressBar();
                    String getUsername = obj.getUsername();
//                    Map<String, String> headers = request.getResponseHeaders();
                    if(getUsername != null&&!getUsername.isEmpty()) {
                        DataStorage.getInstance().setRegisteredFlag(true);
                            DataStorage.getInstance().setUsername(obj.getUsername());
                            String gcmToken = DataStorage.getInstance().getGcmRegId(RegisterActivity.this);
                            if (!gcmToken.isEmpty()) {
                                sendGcmRegistrationToServer(MyApplication.getAppContext(), gcmToken, getUsername);
                            }
                            getProfileRequest();
                    }else{
                        showError(getString(R.string.error_general));
                        signOut();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    if(error!= null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND){
//                        Log.e("VolleyError ", "" + error.networkResponse.statusCode);
                        Intent mIntent = new Intent(RegisterActivity.this, ProfileActivity.class);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                        Bundle mBundle = new Bundle();
                        mBundle.putString(AppConsatants.PROFILE_FACEBOOK, userId);
                        mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, true);
                        mIntent.putExtras(mBundle);
                        startActivity(mIntent);
                        finish();
                    }else {
//                        Log.e("VolleyError ", "" + error.getMessage());
                            showError(getString(R.string.error_general));
                            signOut();
                        }
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        }catch(Exception e){

        }
    }


    private void sendTwitterRegisterRequest(final long userId,String accessToken,String accessSecret ){
        try {

            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_AUTH_TWITTER);
            SocialAuthTwitter socialAuthTwitter = new SocialAuthTwitter();
            socialAuthTwitter.setAccessToken(accessToken);
            socialAuthTwitter.setUserId(userId);
            socialAuthTwitter.setAccessSecret(accessSecret);
            DataCacheManager.getInstance().clearSocialAuthTwitter();
            DataCacheManager.getInstance().saveSigninTwitterData(socialAuthTwitter);
            DataStorage.getInstance().setLastLoginType(AppConsatants.PROFILE_TWITTER);
            DataStorage.getInstance().setAppSocialId(""+userId);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(socialAuthTwitter);
            } catch (IOException e) {
                e.printStackTrace();
            }
            request= new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseSocialAuth obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), ResponseSocialAuth.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    hideProgressBar();
                    String getUsername = obj.getUsername();
//                    Map<String, String> headers = request.getResponseHeaders();
                    if(getUsername != null&&!getUsername.isEmpty()) {
                        DataStorage.getInstance().setRegisteredFlag(true);
                        DataStorage.getInstance().setUsername(obj.getUsername());
                        String gcmToken = DataStorage.getInstance().getGcmRegId(RegisterActivity.this);
                        if(!gcmToken.isEmpty()) {
                            sendGcmRegistrationToServer(MyApplication.getAppContext(),gcmToken,getUsername);
                        }
                        getProfileRequest();
                    }else{
                        showError(getString(R.string.error_general));
                        signOut();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    if(error!= null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND){
//                        Log.e("VolleyError ", "" + error.networkResponse.statusCode);
                        Intent mIntent = new Intent(RegisterActivity.this, ProfileActivity.class);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                        Bundle mBundle = new Bundle();
                        mBundle.putLong(AppConsatants.PROFILE_TWITTER, userId);
                        mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, true);
                        mIntent.putExtras(mBundle);
                        startActivity(mIntent);
                        finish();
                        mIntent.putExtras(mBundle);
                        startActivity(mIntent);
                    }else{
                        showError(getString(R.string.error_general));
                        signOut();
                    }
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        }catch(Exception e){

        }
    }

    private void sendGcmRegistrationToServer(final Context context, final String token, String username) {
        if(token== null|| context == null)
            return;
        String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_UPDATE_GCM);
        Profile profile = new Profile();
        profile.setUsername(username);
        profile.setGcmId(token);
        ObjectMapper mapper = new ObjectMapper();
        String jsonObject = null;
        try {
            jsonObject = mapper.writeValueAsString(profile);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        Log.d("GCM UPDATE", "GCM ToServer: " + profile.toString());
        JsonObjectRequest ObjReq = new JsonObjectRequest(Request.Method.POST,url,jsonObject,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
//                Log.d("GCM UPDATE", "Response: " + response.toString());

                if(response.toString().isEmpty() || response.toString().equals(null)){
//                    Toast.makeText(context, "Register error. Try again.", Toast.LENGTH_LONG);
                }else {

//                    Toast.makeText(context, "Successfully registered with server", Toast.LENGTH_LONG).show();

                    try {
                        DataStorage.getInstance().storeRegistrationIdOnServer(context,true);
                    } catch (Exception e) {
                        // JSON error
                        e.printStackTrace();
//                        Toast.makeText(context, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(context,
//                        "Error! Check your internet connection.", Toast.LENGTH_LONG).show();

            }
        });

        // Adding request to request queue
        VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(ObjReq);
//        DataStorage.getInstance().storeRegistrationIdOnServer(this, true);
    }

    private void getProfileRequest(){
        try {

            final String username = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getProfileUrl(IAPIConstants.API_KEY_GET_PROFILE,username,username);

            request= new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    String getUsername = obj.getUsername();
                    Map<String, String> headers = request.getResponseHeaders();
                    hideProgressBar();
                    if(getUsername != null && !getUsername.isEmpty() && username.equals(getUsername)) {
                        ModelManager.getInstance().setOwnProfile(obj);
                        ModelManager.getInstance().setOwnProfile(obj);
                        DataStorage.getInstance().setUserUUID(obj.getUuid());
//						Log.i("X-AUTH-TOKEN ", "" + headers.get("X-AUTH-TOKEN"));
                        if (!DataStorage.getInstance().getContactSyncCompleted()) {
                            SyncUtils.CreateSyncAccount(MyApplication.getAppContext());
                        }
                        if (Utils.getInstance().getIntegerValueOfString(obj.getPosts()) > 0) {
                            startActivity(new Intent(RegisterActivity.this, TimelineTabs.class));
                        } else {
                            startActivity(new Intent(RegisterActivity.this, TimelineTabs.class));
                        }
                        finish();
                    }else{

                        showError(getString(R.string.error_general));
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    showError(getString(R.string.error_general));
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        }catch(Exception e){

        }
    }

    private void showError(final String mesg) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(RegisterActivity.this,R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                }
            });

        }catch(WindowManager.BadTokenException e){

        }catch(Exception e){

        }
    }

    private void showProgressBar() {
    if(loadingView != null){
//        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotate);
//        rotation.setRepeatCount(Animation.INFINITE);
//        loadingView.startAnimation(rotation);
        loadingView.setVisibility(View.VISIBLE);
    }
       /* progressDialog = new ProgressDialog(RegisterActivity.this);
//        progressDialog.setContentView(R.layout.progressbar);
        progressDialog.setMessage(getString(R.string.progressbar_fetching_info));
        progressDialog.show();*/
    }

    private void hideProgressBar() {

               /* if (progressDialog != null) {
                    progressDialog.hide();
                    progressDialog.dismiss();
                    progressDialog = null;
                }*/
        if(loadingView != null){
//            Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotate);
//            rotation.setRepeatCount(Animation.INFINITE);
//            loadingView.startAnimation(rotation);
//            loadingView.clearAnimation();
            loadingView.setVisibility(View.GONE);
        }

    }

    private void setGoogleAnalyticScreen(String screenName){
        MyApplication application = (MyApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.setAppVersion(""+Utils.getInstance().getAppVersion(MyApplication.getAppContext()));
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

}
