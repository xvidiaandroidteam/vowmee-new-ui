package net.xvidia.vowmee;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.mobile.AWSConfiguration;
import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.mobile.content.UserFileManager;
import com.amazonaws.mobile.user.IdentityManager;
import com.amazonaws.mobile.user.IdentityProvider;
import com.amazonaws.mobile.user.signin.SignInManager;
import com.amazonaws.mobile.user.signin.SignInProvider;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.CheckNetworkConnection;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.fragments.TimelineLiveVows;
import net.xvidia.vowmee.fragments.TimelineVows;
import net.xvidia.vowmee.gcm.GCMRegistrationIntentService;
import net.xvidia.vowmee.gcm.INotificationListener;
import net.xvidia.vowmee.gcm.MyInstanceIDListenerService;
import net.xvidia.vowmee.gcm.NotifBuilderSingleton;
import net.xvidia.vowmee.helper.CustomFontTextView;
import net.xvidia.vowmee.helper.MyCustomLayoutManager;
import net.xvidia.vowmee.listadapter.BottomListAdapter;
import net.xvidia.vowmee.listadapter.NavDrawerListAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.ActivityLog;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static net.xvidia.vowmee.MyApplication.getAppContext;

public class TimelineTabs extends AppCompatActivity implements INotificationListener {

    public static final int GALLERY_INTENT_CALLED = 1234;
    public static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 123;
    public static final int REQUEST_CODE_ASK_PERMISSIONS = 125;
    private Context context;
    private Activity activity;
    //    private TwitterLoginButton twitterLoginButton;
    private CustomFontTextView homeButton;
    private CustomFontTextView liveButton;
    //    public static boolean isLive;
    private FloatingActionButton videoMenu;
    private SignInManager signInManager;
    private int backButtonCount = 0;
    private Handler mExitHandler = new Handler();
    private final Runnable mExitRunnable = new Runnable() {

        @Override
        public void run() {
            backButtonCount = 0;
        }
    };
    String[] bottomListItemName = {
            MyApplication.getAppContext().getResources().getString(R.string.menu_go_live),
//            MyApplication.getAppContext().getResources().getString(R.string.menu_create_event),
            MyApplication.getAppContext().getResources().getString(R.string.menu_record_video),
            MyApplication.getAppContext().getResources().getString(R.string.menu_upload_video)
    };
    int[] bottomListImages = new int[]{
            R.drawable.go_live,
            R.drawable.record_new,
            R.drawable.upload_new
    };
    int[] ICON = new int[]{
            R.drawable.nav_home,
           /* R.drawable.nav_activities,*/
            R.drawable.group,
            R.drawable.nav_go_live,
            R.drawable.nav_record_icon,
            R.drawable.nav_upload_icon,
            R.drawable.contact,
            R.drawable.nav_suggestion_icon,
            R.drawable.setting,
            R.drawable.nav_logout_icon
    };

    private static int flag = 0;
    private Animation slideUp, slideDown;
    private RelativeLayout layout;
    private TextView menuPendingCountMenu;
    public NavDrawerListAdapter mAdapter;
    String TITLES[] = {
            MyApplication.getAppContext().getResources().getString(R.string.nav_home),
         /*   MyApplication.getAppContext().getResources().getString(R.string.nav_activities),*/
            MyApplication.getAppContext().getResources().getString(R.string.nav_group),
            MyApplication.getAppContext().getResources().getString(R.string.nav_go_live),
            MyApplication.getAppContext().getResources().getString(R.string.nav_record_video),
            MyApplication.getAppContext().getResources().getString(R.string.nav_upload_video),
            MyApplication.getAppContext().getResources().getString(R.string.nav_contacts),
            MyApplication.getAppContext().getResources().getString(R.string.nav_suggestions),
            MyApplication.getAppContext().getResources().getString(R.string.settings),
            MyApplication.getAppContext().getResources().getString(R.string.nav_logout)
    };
//    String COUNTER[] = {"","", "", "", "", "", "", "", ""};

    int PROFILE = R.drawable.user;
    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    public static DrawerLayout Drawer;
    private Toolbar toolbar;                              // Declaring the Toolbar Object
    private ActionBarDrawerToggle mDrawerToggle;
    //    private String mActivityTitle;
    private ImageButton cancel;
    private LinearLayout tintLayout;
    private boolean notificationIntent;
    private int notificationId, idInboxStyle;
    private TimelineVows timelineVows;
    private TimelineLiveVows timelineLiveVows;
    private RelativeLayout menuPending;
    private boolean homeVowsView;
    private ImageView groupMenu, searchMenu;
    private FragmentTransaction fragmentTransaction;

    /**
     * SignInResultsHandler handles the results from sign-in for a previously signed in user.
     */
    private class SignInResultsHandler implements IdentityManager.SignInResultsHandler {
        /**
         * Receives the successful sign-in result for an alraedy signed in user and starts the main
         * activity.
         *
         * @param provider the identity provider used for sign-in.
         */
        @Override
        public void onSuccess(final IdentityProvider provider) {
            try {

                AWSMobileClient.defaultMobileClient()
                        .getIdentityManager()
                        .loadUserInfoAndImage(provider, new Runnable() {
                            @Override
                            public void run() {
                                String bucket = AWSConfiguration.AMAZON_S3_USER_FILES_BUCKET;
                                String prefix = "public/";
                                AWSMobileClient.defaultMobileClient()
                                        .createUserFileManager(bucket,
                                                prefix,
                                                new UserFileManager.BuilderResultHandler() {

                                                    @Override
                                                    public void onComplete(final UserFileManager userFileManager) {
                                                        Utils.getInstance().setUserFileManager(userFileManager);
                                                        Utils.getInstance().uploadProfileImage(true, DataStorage.getInstance().getProfileImagePath(), (DataStorage.getInstance().getUsername() + AppConsatants.FILE_EXTENSION_JPG));

//													if(provider.getDisplayName().equalsIgnoreCase("Facebook")) {
//														sendFacebookRegisterRequest(provider.getFacebookUserId(), provider.getToken(), provider.getFacbookTokenExpiryDate());
//													}else if(provider.getDisplayName().equalsIgnoreCase("Twitter")){
//														sendTwitterRegisterRequest(provider.getTwitterUserId(), provider.getToken(), provider.getSecreteToken());
//
//													}
                                                    }
                                                });
                            }
                        });

            } catch (Exception e) {

            }
        }

        /**
         * For the case where the user previously was signed in, and an attempt is made to sign the
         * user back in again, there is not an option for the user to cancel, so this is overriden
         * as a stub.
         *
         * @param provider the identity provider with which the user attempted sign-in.
         */
        @Override
        public void onCancel(final IdentityProvider provider) {
//			if(DataStorage.getInstance().getAppSocialId().isEmpty() && !DataStorage.getInstance().getRegisteredFlag()){
//				goSignIn();
//			}else{
//				if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_FACEBOOK)){
//					SocialAuthFacebook obj = DataCacheManager.getInstance().getFacebookData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && !obj.getUserId().isEmpty()){
//						sendFacebookRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getExpiryDate());
//					}else{
//						goSignIn();
//					}
//				}else if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_TWITTER)){
//					SocialAuthTwitter obj = DataCacheManager.getInstance().getTwitterData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && obj.getUserId()>0){
//						sendTwitterRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getAccessSecret());
//					}else{
//						goSignIn();
//					}
//				}else{
//					goSignIn();
//				}
//			}
//			hideProgressBar();
        }

        /**
         * Receives the sign-in result that an error occurred signing in with the previously signed
         * in provider and re-directs the user to the sign-in activity to sign in again.
         *
         * @param provider the identity provider with which the user attempted sign-in.
         * @param ex       the exception that occurred.
         */
        @Override
        public void onError(final IdentityProvider provider, Exception ex) {
//            Log.e("credentials refresh",
//                    String.format("Cognito credentials refresh with %s provider failed. Error: %s",
//                            provider.getDisplayName(), ex.getMessage()), ex);
//
//			Toast.makeText(Splash.this, String.format("Sign-in with %s failed.",
//					provider.getDisplayName()), Toast.LENGTH_LONG).show();
//			if(DataStorage.getInstance().getAppSocialId().isEmpty() && !DataStorage.getInstance().getRegisteredFlag()){
//				goSignIn();
//			}else{
//				if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_FACEBOOK)){
//					SocialAuthFacebook obj = DataCacheManager.getInstance().getFacebookData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && !obj.getUserId().isEmpty()){
//						sendFacebookRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getExpiryDate());
//					}else{
//						goSignIn();
//					}
//				}else if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_TWITTER)){
//					SocialAuthTwitter obj = DataCacheManager.getInstance().getTwitterData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && obj.getUserId()>0){
//						sendTwitterRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getAccessSecret());
//					}else{
//						goSignIn();
//					}
//				}else{
//					goSignIn();
//				}
//			}

        }
    }

    public void slideToBottom(View view) {
        tintLayout.setClickable(false);
        TranslateAnimation animate = new TranslateAnimation(0, 0, 0, view.getHeight());
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);

    }

    public void slideToTop(View view) {
        tintLayout.setBackgroundColor(getResources().getColor(R.color.bottom_list_tint));
        tintLayout.setClickable(true);
        TranslateAnimation animate = new TranslateAnimation(0, 0, view.getHeight(), 0);
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);

    }

    public void CloseBottomListView() {

        if (layout.getVisibility() == View.VISIBLE) {
            slideToBottom(tintLayout);
//        tintLayout.setVisibility(View.INVISIBLE);
            layout.startAnimation(slideDown);
            layout.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline_tabs);
        FacebookSdk.sdkInitialize(this);
        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            notificationIntent = intent.getBooleanExtra(AppConsatants.NOTIFICATION_NEXT_INTENT, false);
            notificationId = intent.getIntExtra(AppConsatants.NOTIFICATION_ID_INTENT, 0);
            idInboxStyle = intent.getIntExtra(AppConsatants.IDINBOXSTYLE, 0);
        }

        activity = this;
        context = TimelineTabs.this;
        videoMenu = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        cancel = (ImageButton) findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseBottomListView();
            }
        });
        initialiseGcm();
        homeVowsView = true;
        initToolbars();
        logUser();
        // Getting a reference to listview of main.xml layout file
        ListView listView = (ListView) findViewById(R.id.bottomlistview);
        listView.setAdapter(new BottomListAdapter(this, bottomListItemName, bottomListImages));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        if (Utils.getInstance().checkCameraHardware(context)) {
//                            isLive = true;
                            DataStorage.getInstance().setIsLiveFlag(true);
                            callCameraApp(true);
                        } else {
                            showMessage("Your phone does not support camera", null);
                        }
                        break;

                    case 1:
                        if (Utils.getInstance().checkCameraHardware(context)) {
//                            isLive = false;
                            DataStorage.getInstance().setIsLiveFlag(false);
                            callCameraApp(false);
                        } else {
                            showMessage("Your phone does not support camera", null);
                        }
                        break;

                    case 2:
                        selectVideo();
                        break;

                }
            }
        });

        layout = (RelativeLayout) findViewById(R.id.bottomLayout);
        layout.setVisibility(View.GONE);
        slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        tintLayout = (LinearLayout) findViewById(R.id.tint_layout);
        tintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseBottomListView();
            }
        });
        tintLayout.setClickable(false);
        videoMenu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                slideToTop(tintLayout);
//                tintLayout.setVisibility(View.VISIBLE);
                layout.startAnimation(slideUp);
                layout.setVisibility(View.VISIBLE);

            }
        });

        getSupportActionBar().setTitle("");
        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View

        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size

        mAdapter = new NavDrawerListAdapter(TITLES, ICON, PROFILE, context);
        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        mLayoutManager = new MyCustomLayoutManager(this, true);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);        // Drawer object Assigned to the view

        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar,
                R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // getSupportActionBar().setTitle("Navigation!");
                invalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
                if (layout.getVisibility() == View.VISIBLE)
                    CloseBottomListView();
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
            }
        };

        // Drawer Toggle Object Madem
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.hemburgar_icon, getTheme());
        mDrawerToggle.setHomeAsUpIndicator(drawable);
        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Drawer.isDrawerVisible(GravityCompat.START)) {
                    Drawer.closeDrawer(GravityCompat.START);
                } else {
                    Drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        if (notificationIntent) {
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancel(notificationId);
            NotifBuilderSingleton.getInstance().resetInboxStyle(idInboxStyle);
        }
    }

    private void setButtonView() {
        if (homeVowsView) {
            liveButton.setBackgroundResource(R.drawable.button_tab_live_selected);
            liveButton.setTextColor(getResources().getColor(R.color.appcolor));
            homeButton.setBackgroundResource(R.drawable.button_tab_home_unselect);
            homeButton.setTextColor(getResources().getColor(R.color.white));
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, timelineLiveVows);
            fragmentTransaction.commit();

            setGoogleAnalyticScreen("Timeline Live");
        } else {
            homeButton.setBackgroundResource(R.drawable.button_tab_home_selected);
            homeButton.setTextColor(getResources().getColor(R.color.appcolor));
            liveButton.setBackgroundResource(R.drawable.button_tab_live_unselect);
            liveButton.setTextColor(getResources().getColor(R.color.white));
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, timelineVows);
            fragmentTransaction.commit();

            setGoogleAnalyticScreen("Timeline Home");
        }
        homeVowsView = !homeVowsView;
    }

    @Override
    protected void onPause() {
        super.onPause();
        CloseBottomListView();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (CheckNetworkConnection.isConnectionAvailable(TimelineTabs.this)) {
            initialiseAmazonApi();
        }

        mDrawerToggle.syncState();
        DataStorage.getInstance().setCallProgress(false);
    }

    private void initialiseAmazonApi() {
//        if (AWSMobileClient.defaultMobileClient() == null) {
        signInManager = new SignInManager(this);
        final SignInProvider provider = signInManager.getPreviouslySignedInProvider();
        // if the user was already previously in to a provider.
        if (provider != null) {
            // asyncronously handle refreshing credentials and call our handler.
            if (DataStorage.getInstance().getRegisteredFlag()) {
                signInManager.refreshCredentialsWithProvider(TimelineTabs.this,
                        provider, new SignInResultsHandler());
            }
        }
//        } else {
//            String bucket = AWSConfiguration.AMAZON_S3_USER_FILES_BUCKET;
//            String prefix = "public/";
//            AWSMobileClient.defaultMobileClient()
//                    .createUserFileManager(bucket,
//                            prefix,
//                            new UserFileManager.BuilderResultHandler() {
//
//                                @Override
//                                public void onComplete(final UserFileManager userFileManager) {
//                                    Utils.getInstance().setUserFileManager(userFileManager);
////													if(provider.getDisplayName().equalsIgnoreCase("Facebook")) {
////														sendFacebookRegisterRequest(provider.getFacebookUserId(), provider.getToken(), provider.getFacbookTokenExpiryDate());
////													}else if(provider.getDisplayName().equalsIgnoreCase("Twitter")){
////														sendTwitterRegisterRequest(provider.getTwitterUserId(), provider.getToken(), provider.getSecreteToken());
////
////													}
//                                }
//                            });
//        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(TimelineTabs.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<String, Integer>();
            // Initial
            perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);

            // Fill with results
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
            // Check for ACCESS_FINE_LOCATION
            if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                    && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && perms.get(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                callCameraApp(DataStorage.getInstance().getIsLiveFlag());
            } else {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_message_camera_permission_denied), Utils.LONG_TOAST);

            }
        } else if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectVideo();
            } else {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_message_upload_permission_denied), Utils.LONG_TOAST);

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void callCameraApp(boolean live) {

        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!Utils.getInstance().addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!Utils.getInstance().addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write External Storage");
        if (!Utils.getInstance().addPermission(permissionsList, Manifest.permission.RECORD_AUDIO))
            permissionsNeeded.add("Record Audio");

        Map<String, Integer> perms = new HashMap<String, Integer>();
        // Initial
        perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);

        if (permissionsList.size() == 0) {
            Intent mIntent = new Intent(TimelineTabs.this, RecordVideo.class);
            Bundle mBundle = new Bundle();
            mBundle.putBoolean(AppConsatants.LIVE_VIDEO, live);
            mIntent.putExtras(mBundle);
            startActivity(mIntent);
        } else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    // Need Rationale
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                }
                            });
                    return;
                }
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;

            }
         /*   if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
                showMessageOKCancel("You need to allow access to Camera",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });

                return;
            } else {
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;
            }*/
        }

    }

    private void initToolbars() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toolbar.setPadding(0, 0, 0, 0);
        homeButton = (CustomFontTextView) findViewById(R.id.home);
        liveButton = (CustomFontTextView) findViewById(R.id.liveVows);
        searchMenu = (ImageView) findViewById(R.id.search_button);
        groupMenu = (ImageView) findViewById(R.id.group_menu);
//        notificationMenu = (ImageView) findViewById(R.id.notification_menu);
        menuPending = (RelativeLayout) findViewById(R.id.pending_request_layout);
        timelineLiveVows = new TimelineLiveVows();
        timelineVows = new TimelineVows();
        menuPending.setVisibility(View.GONE);
        int paddingLeftRight = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_10);
        int paddingTopBottom = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_5);
        homeButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight, paddingTopBottom);
        liveButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight, paddingTopBottom);
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, timelineVows);
        fragmentTransaction.commit();
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!homeVowsView)
                    setButtonView();
            }
        });
        liveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (homeVowsView)
                    setButtonView();
            }
        });
        searchMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntentSearch = new Intent(TimelineTabs.this, SearchActivity.class);
                startActivity(mIntentSearch);
            }
        });
        groupMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntentVideo = new Intent(TimelineTabs.this, GroupListActivity.class);
                GroupListActivity.groupSelection = false;
                startActivity(mIntentVideo);
            }
        });
//        notificationMenu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent mIntentActivity = new Intent(TimelineTabs.this, TimelineActivityTabs.class);
//                startActivity(mIntentActivity);
//            }
//        });
        menuPending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntentActivity = new Intent(TimelineTabs.this, PendingRequestActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean(AppConsatants.GROUP_ROLE_MODERATOR, false);
                mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, false);
                mIntentActivity.putExtras(mBundle);
                startActivity(mIntentActivity);
            }
        });

        menuPendingCountMenu = (TextView) findViewById(R.id.pending_count);
        int count = 0;
        if (ModelManager.getInstance().getOwnProfile() != null)
            count = Utils.getInstance().getIntegerValueOfString(ModelManager.getInstance().getOwnProfile().getPendingFriendRequests());
        menuPendingCountMenu.setText("" + count);
        menuPendingCountMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntentActivity = new Intent(TimelineTabs.this, PendingRequestActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean(AppConsatants.GROUP_ROLE_MODERATOR, false);
                mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, false);
                mIntentActivity.putExtras(mBundle);
                startActivity(mIntentActivity);
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (layout.getVisibility() == View.VISIBLE) {
            CloseBottomListView();
            return;
        } else if (Drawer.isDrawerOpen(Gravity.LEFT)) {
            Drawer.closeDrawer(Gravity.LEFT);
        } else if (backButtonCount >= 1) {
            mExitHandler.removeCallbacks(mExitRunnable);
            mExitHandler = null;
            finish();
            super.onBackPressed();
        } else {
            backButtonCount++;
            String exitMesg = getResources().getString(R.string.alert_message_back_press_exit);
            Utils.getInstance().displayToast(this, exitMesg, Utils.SHORT_TOAST);
            mExitHandler.postDelayed(mExitRunnable, 2500);
        }
    }

    private void showMessage(final String mesg, final DialogInterface.OnClickListener okClicked) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new AlertDialog.Builder(TimelineTabs.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .setCancelable(false)
                            .show();
                }
            });
        } catch (WindowManager.BadTokenException e) {
        } catch (Exception e) {
        }
    }


    public void selectVideo() {
//        sendMomentList();
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            Intent intent = new Intent();
            intent.setType("video/*");
            intent.setAction(Intent.ACTION_PICK);
//            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//            intent.setType("video/*");
            startActivityForResult(intent, GALLERY_INTENT_CALLED);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showMessageOKCancel("You need to allow access to Camera",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);

                            }
                        });

                return;
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);

                return;
            }
        }

    }

    @Override
    protected void onResume() {
        if (menuPending != null) {
            int count = 0;
            if (ModelManager.getInstance().getOwnProfile() != null) {
                count = Utils.getInstance().getIntegerValueOfString(ModelManager.getInstance().getOwnProfile().getPendingFriendRequests());
                if (count > 0) {
                    menuPendingCountMenu.setText("" + count);
                    menuPending.setVisibility(View.VISIBLE);
                } else {
                    menuPending.setVisibility(View.GONE);
                }
            }
        }
        getProfileRequest();
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
        if (PublishLiveActivity.closePublish) {
            closePublishSessionConnection();
        }
        if (SubscriberActivity.closeSubscriber) {
            closeSubscriberSessionConnection();
        }
        if (ReceiveVideoCallActivity.closeReceiveVideoCall) {
            closeVideoCallSessionConnection();
        }
        if (MakeVideoCallActivity.closeMakeVideoCall) {
            closeMakeVideoSessionConnection();
        }
        setGoogleAnalyticScreen("Timeline Home");
        super.onResume();
    }

    public void closeVideoCallSessionConnection() {

        if (ReceiveVideoCallActivity.mSessionPublisherId == null || ReceiveVideoCallActivity.mSessionSubscriberId == null) {
            return;
        }
        if (ReceiveVideoCallActivity.mSessionPublisherId.isEmpty() || ReceiveVideoCallActivity.mSessionSubscriberId.isEmpty()) {
            return;
        }
        String subUrl = ServiceURLManager.getInstance().getCloseLiveUrl(ReceiveVideoCallActivity.mSessionPublisherId, ReceiveVideoCallActivity.mSessionPublisherId, DataStorage.getInstance().getUserUUID());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, subUrl, "{}", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    ReceiveVideoCallActivity.mSessionPublisherId = "";
                    ReceiveVideoCallActivity.mSessionSubscriberId = "";
                    ReceiveVideoCallActivity.closeReceiveVideoCall = false;

                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ReceiveVideoCallActivity.mSessionPublisherId = "";
                ReceiveVideoCallActivity.mSessionSubscriberId = "";
                ReceiveVideoCallActivity.closeReceiveVideoCall = false;
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    public void closeMakeVideoSessionConnection() {

        if (MakeVideoCallActivity.mSessionId == null) {
            return;
        }
        if (MakeVideoCallActivity.mSessionId.isEmpty()) {
            return;
        }
        String subUrl = ServiceURLManager.getInstance().getDestroyLive(MakeVideoCallActivity.mSessionId, DataStorage.getInstance().getUserUUID());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, subUrl, "{}", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    MakeVideoCallActivity.mSessionId = "";
                    MakeVideoCallActivity.closeMakeVideoCall = false;
                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MakeVideoCallActivity.mSessionId = "";
                MakeVideoCallActivity.closeMakeVideoCall = false;
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    public void closePublishSessionConnection() {

        if (PublishLiveActivity.mSessionId == null) {
            return;
        }
        if (PublishLiveActivity.mSessionId.isEmpty()) {
            return;
        }
        String subUrl = ServiceURLManager.getInstance().getDestroyLive(PublishLiveActivity.mSessionId, DataStorage.getInstance().getUserUUID());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, subUrl, "{}", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    PublishLiveActivity.mSessionId = "";
                    PublishLiveActivity.closePublish = false;
                    String url = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, DataStorage.getInstance().getUsername(), DataStorage.getInstance().getUsername(), AppConsatants.TRUE, "0", "0");
                    VolleySingleton.getInstance(getAppContext()).refresh(url, false);
                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                PublishLiveActivity.mSessionId = "";
                PublishLiveActivity.closePublish = false;
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    public void closeSubscriberSessionConnection() {

        if (SubscriberActivity.mSessionPublisherId == null) {
            return;
        }
        if (SubscriberActivity.mSessionPublisherId.isEmpty()) {
            return;
        }
        String subUrl = ServiceURLManager.getInstance().getCloseLiveUrl(SubscriberActivity.mSessionPublisherId, SubscriberActivity.mSessionPublisherId, DataStorage.getInstance().getUserUUID());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, subUrl, "{}", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    String url = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, DataStorage.getInstance().getUsername(), DataStorage.getInstance().getUsername(), AppConsatants.TRUE, "0", "0");
                    VolleySingleton.getInstance(getAppContext()).refresh(url, false);
                    SubscriberActivity.mSessionPublisherId = "";
                    SubscriberActivity.closeSubscriber = false;
                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                SubscriberActivity.mSessionPublisherId = "";
                SubscriberActivity.closeSubscriber = false;
                String url = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, DataStorage.getInstance().getUsername(), DataStorage.getInstance().getUsername(), AppConsatants.TRUE, "0", "0");
                VolleySingleton.getInstance(getAppContext()).refresh(url, false);

            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void getProfileRequest() {
        try {
//	showProgressBar(getString(R.string.progressbar_fetching_info));
            final String username = DataStorage.getInstance().getUsername();//"F_815817965196060";//
            String url = ServiceURLManager.getInstance().getProfileUrl(IAPIConstants.API_KEY_GET_PROFILE, username, username);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    } catch (OutOfMemoryError e) {
                    } catch (Exception e) {
                    }
                    if (obj != null) {
                        String getUsername = obj.getUsername();
                        if (getUsername != null && !getUsername.isEmpty() && username.equals(getUsername)) {
                            ModelManager.getInstance().setOwnProfile(obj);
                            DataStorage.getInstance().setUserUUID(obj.getUuid());
                            mRecyclerView.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                            int count = Utils.getInstance().getIntegerValueOfString(ModelManager.getInstance().getOwnProfile().getPendingFriendRequests());
                            if (count > 0) {
                                menuPendingCountMenu.setText("" + count);
                                menuPending.setVisibility(View.VISIBLE);
                            } else {
                                menuPending.setVisibility(View.GONE);
                            }
                        } else {
//						showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//							@Override
//							public void onClick(DialogInterface dialog, int which) {
//								finish();
//							}
//						});
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                }
            }) {
                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        final long cacheHitButRefreshed = AppConsatants.cacheHitButRefreshed; // in 3 minutes cache will be hit, but also refreshed on background
                        final long cacheExpired = AppConsatants.cacheExpired; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + cacheHitButRefreshed;
                        final long ttl = now + cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONObject(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONObject response) {
                    super.deliverResponse(response);
//                    Log.e("VolleyError ", "" + response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        try {
            Crashlytics.setUserIdentifier(DataStorage.getInstance().getUsername());
            Crashlytics.setUserEmail(DataStorage.getInstance().getUsername() + "@vowmee.com");
            Crashlytics.setUserName(ModelManager.getInstance().getOwnProfile().getDisplayName());
        } catch (Exception e) {

        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedImageUri = null;
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_INTENT_CALLED) {
                selectedImageUri = data.getData();
                String path = Utils.getInstance().getPath(this, selectedImageUri, false);
//					Bitmap bp = ThumbnailUtils.createVideoThumbnail(selectedImagePath, MediaStore.Images.Thumbnails.MINI_KIND);
//                Log.i("Mediapath", path);
                if (!path.isEmpty()) {
                    Intent mIntent = new Intent(TimelineTabs.this, UploadActivity.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString(AppConsatants.MOMENT_PATH, path);
                    mBundle.putBoolean(AppConsatants.RESHARE, false);
                    mIntent.putExtras(mBundle);
                    startActivity(mIntent);
                }

            }
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new TimelineVows(), getString(R.string.title_fragment_vows));
        adapter.addFrag(new TimelineLiveVows(), getString(R.string.title_live));
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onNotificationReceived(ActivityLog activityLog) {

    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void initialiseGcm() {
        if (DataStorage.getInstance().getGcmRegId(this).isEmpty()) {
            Intent intent = new Intent(this, GCMRegistrationIntentService.class);
            startService(intent);
        }
        Intent i = new Intent(getApplicationContext(), MyInstanceIDListenerService.class);
        startService(i);

    }

    private void setGoogleAnalyticScreen(String screenName) {
        MyApplication application = (MyApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.setClientId(DataStorage.getInstance().getUserUUID());
        mTracker.setAppVersion("" + Utils.getInstance().getAppVersion(MyApplication.getAppContext()));
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

}