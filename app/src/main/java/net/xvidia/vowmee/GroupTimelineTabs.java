package net.xvidia.vowmee;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.mobile.AWSConfiguration;
import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.mobile.content.UserFileManager;
import com.amazonaws.mobile.user.IdentityManager;
import com.amazonaws.mobile.user.IdentityProvider;
import com.amazonaws.mobile.user.signin.SignInManager;
import com.amazonaws.mobile.user.signin.SignInProvider;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.FacebookSdk;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.CheckNetworkConnection;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.fragments.GroupLiveVows;
import net.xvidia.vowmee.fragments.GroupTimelineVows;
import net.xvidia.vowmee.gcm.INotificationListener;
import net.xvidia.vowmee.listadapter.BottomListAdapter;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.ActivityLog;
import net.xvidia.vowmee.network.model.GroupProfile;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupTimelineTabs extends AppCompatActivity implements INotificationListener {

    private final int GALLERY_INTENT_CALLED = 1234;
    private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 123;
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 125;
    private Context context;


    private Activity activity;
    private boolean isLive;
    private SignInManager signInManager;
    private Toolbar toolbarTop;
    private TextView toolbarTitle;
    private TextView toolbarSubTitle;
    private FloatingActionButton videoMenu;
    private RelativeLayout layout;
    private Animation slideUp, slideDown;
    private ImageButton cancel;
    private MenuItem menuPending;
    private TextView notifCountMenu;
    private LinearLayout tintLayout;
    String[] bottomListItemName = {
            MyApplication.getAppContext().getResources().getString(R.string.menu_go_live),
//            MyApplication.getAppContext().getResources().getString(R.string.menu_create_event),
            MyApplication.getAppContext().getResources().getString(R.string.menu_record_video),
            MyApplication.getAppContext().getResources().getString(R.string.menu_upload_video)
    };
    int[] bottomListImages = new int[]{
            R.drawable.go_live,
//            R.drawable.event,
            R.drawable.record_new,
            R.drawable.upload_new
    };

    /**
     * SignInResultsHandler handles the results from sign-in for a previously signed in user.
     */
    private class SignInResultsHandler implements IdentityManager.SignInResultsHandler {
        /**
         * Receives the successful sign-in result for an alraedy signed in user and starts the main
         * activity.
         * @param provider the identity provider used for sign-in.
         */
        @Override
        public void onSuccess(final IdentityProvider provider) {


            AWSMobileClient.defaultMobileClient()
                    .getIdentityManager()
                    .loadUserInfoAndImage(provider, new Runnable() {
                        @Override
                        public void run() {
                            String bucket = AWSConfiguration.AMAZON_S3_USER_FILES_BUCKET;
                            String prefix = "public/";
                            AWSMobileClient.defaultMobileClient()
                                    .createUserFileManager(bucket,
                                            prefix,
                                            new UserFileManager.BuilderResultHandler() {

                                                @Override
                                                public void onComplete(final UserFileManager userFileManager) {
                                                    Utils.getInstance().setUserFileManager(userFileManager);
//													if(provider.getDisplayName().equalsIgnoreCase("Facebook")) {
//														sendFacebookRegisterRequest(provider.getFacebookUserId(), provider.getToken(), provider.getFacbookTokenExpiryDate());
//													}else if(provider.getDisplayName().equalsIgnoreCase("Twitter")){
//														sendTwitterRegisterRequest(provider.getTwitterUserId(), provider.getToken(), provider.getSecreteToken());
//
//													}
                                                }
                                            });
                        }
                    });


        }

        /**
         * For the case where the user previously was signed in, and an attempt is made to sign the
         * user back in again, there is not an option for the user to cancel, so this is overriden
         * as a stub.
         * @param provider the identity provider with which the user attempted sign-in.
         */
        @Override
        public void onCancel(final IdentityProvider provider) {
//			if(DataStorage.getInstance().getAppSocialId().isEmpty() && !DataStorage.getInstance().getRegisteredFlag()){
//				goSignIn();
//			}else{
//				if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_FACEBOOK)){
//					SocialAuthFacebook obj = DataCacheManager.getInstance().getFacebookData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && !obj.getUserId().isEmpty()){
//						sendFacebookRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getExpiryDate());
//					}else{
//						goSignIn();
//					}
//				}else if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_TWITTER)){
//					SocialAuthTwitter obj = DataCacheManager.getInstance().getTwitterData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && obj.getUserId()>0){
//						sendTwitterRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getAccessSecret());
//					}else{
//						goSignIn();
//					}
//				}else{
//					goSignIn();
//				}
//			}
//			hideProgressBar();
        }

        /**
         * Receives the sign-in result that an error occurred signing in with the previously signed
         * in provider and re-directs the user to the sign-in activity to sign in again.
         * @param provider the identity provider with which the user attempted sign-in.
         * @param ex the exception that occurred.
         */
        @Override
        public void onError(final IdentityProvider provider, Exception ex) {
//            Log.e("credentials refresh",
//                    String.format("Cognito credentials refresh with %s provider failed. Error: %s",
//                            provider.getDisplayName(), ex.getMessage()), ex);
//
//			Toast.makeText(Splash.this, String.format("Sign-in with %s failed.",
//					provider.getDisplayName()), Toast.LENGTH_LONG).show();
//			if(DataStorage.getInstance().getAppSocialId().isEmpty() && !DataStorage.getInstance().getRegisteredFlag()){
//				goSignIn();
//			}else{
//				if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_FACEBOOK)){
//					SocialAuthFacebook obj = DataCacheManager.getInstance().getFacebookData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && !obj.getUserId().isEmpty()){
//						sendFacebookRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getExpiryDate());
//					}else{
//						goSignIn();
//					}
//				}else if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_TWITTER)){
//					SocialAuthTwitter obj = DataCacheManager.getInstance().getTwitterData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && obj.getUserId()>0){
//						sendTwitterRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getAccessSecret());
//					}else{
//						goSignIn();
//					}
//				}else{
//					goSignIn();
//				}
//			}

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_timeline_tabs);

        FacebookSdk.sdkInitialize(this);
        activity = this;
        context = GroupTimelineTabs.this;
        videoMenu = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        ListView listView = (ListView) findViewById(R.id.bottomlistview);
        listView.setAdapter(new BottomListAdapter(this, bottomListItemName, bottomListImages));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {

                    case 0:
                        if (Utils.getInstance().checkCameraHardware(context)) {
                            isLive = true;
                            callCameraApp(isLive);
                        } else {
                            showMessage("Your phone does not support camera", null);
                        }
                        break;

                    case 1:
                        if (Utils.getInstance().checkCameraHardware(context)) {
                            isLive = false;
                            callCameraApp(isLive);
                        } else {
                            showMessage("Your phone does not support camera", null);
                        }
                        break;

                    case 2:
                        selectVideo();
                        break;

                }
            }
        });
        layout = (RelativeLayout)findViewById(R.id.bottomLayout);
        tintLayout = (LinearLayout) findViewById(R.id.tint_layout);
        cancel = (ImageButton) findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseBottomListView();
            }
        });
        slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        videoMenu.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View arg0) {

                slideToTop(tintLayout);
                layout.startAnimation(slideUp);
                layout.setVisibility(View.VISIBLE);

            }
        });


        initToolbars();
    }

    public void slideToBottom(View view) {
        tintLayout.setClickable(false);
        TranslateAnimation animate = new TranslateAnimation(0, 0, 0, view.getHeight());
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);

    }

    public void slideToTop(View view) {
        tintLayout.setBackgroundColor(getResources().getColor(R.color.bottom_list_tint));
        tintLayout.setClickable(true);
        TranslateAnimation animate = new TranslateAnimation(0, 0, view.getHeight(), 0);
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);

    }
    public void CloseBottomListView() {

        if (layout.getVisibility() == View.VISIBLE) {
            slideToBottom(tintLayout);
//        tintLayout.setVisibility(View.INVISIBLE);
            layout.startAnimation(slideDown);
            layout.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (CheckNetworkConnection.isConnectionAvailable(GroupTimelineTabs.this)) {
            initialiseAmazonApi();
        }

    }
    private void initialiseAmazonApi(){
        if(AWSMobileClient.defaultMobileClient() == null) {
            signInManager = new SignInManager(this);
            final SignInProvider provider = signInManager.getPreviouslySignedInProvider();
            // if the user was already previously in to a provider.
            if (provider != null) {
                // asyncronously handle refreshing credentials and call our handler.
                if (DataStorage.getInstance().getRegisteredFlag()) {
                    signInManager.refreshCredentialsWithProvider(GroupTimelineTabs.this,
                            provider, new SignInResultsHandler());
                }
            }
        }else{
            String bucket = AWSConfiguration.AMAZON_S3_USER_FILES_BUCKET;
            String prefix = "public/";
            AWSMobileClient.defaultMobileClient()
                    .createUserFileManager(bucket,
                            prefix,
                            new UserFileManager.BuilderResultHandler() {

                                @Override
                                public void onComplete(final UserFileManager userFileManager) {
                                    Utils.getInstance().setUserFileManager(userFileManager);
//													if(provider.getDisplayName().equalsIgnoreCase("Facebook")) {
//														sendFacebookRegisterRequest(provider.getFacebookUserId(), provider.getToken(), provider.getFacbookTokenExpiryDate());
//													}else if(provider.getDisplayName().equalsIgnoreCase("Twitter")){
//														sendTwitterRegisterRequest(provider.getTwitterUserId(), provider.getToken(), provider.getSecreteToken());
//
//													}
                                }
                            });
        }
    }

    /*private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
                return false;
        }
        return true;
    }
*/
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(GroupTimelineTabs.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS){
            Map<String, Integer> perms = new HashMap<String, Integer>();
            // Initial
            perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);

            // Fill with results
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
            // Check for ACCESS_FINE_LOCATION
            if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                    && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && perms.get(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                callCameraApp(isLive);
            }else{
                Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_message_camera_permission_denied),Utils.SHORT_TOAST);
//
//                Toast.makeText(getApplicationContext(), "Camera permission has not been granted, cannot go live", Toast.LENGTH_LONG).show();

            }
        }else if(requestCode == REQUEST_CODE_ASK_PERMISSIONS){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                selectVideo();
            }else{
                Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_message_upload_permission_denied),Utils.SHORT_TOAST);
//
//                Toast.makeText(getApplicationContext(), "Permission has not been granted, cannot upload files", Toast.LENGTH_LONG).show();

            }
        }else{
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void callCameraApp(boolean live){

        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!Utils.getInstance().addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!Utils.getInstance().addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write External Storage");
        if (!Utils.getInstance().addPermission(permissionsList, Manifest.permission.RECORD_AUDIO))
            permissionsNeeded.add("Record Audio");

        Map<String, Integer> perms = new HashMap<String, Integer>();
        // Initial
        perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);

        if (permissionsList.size()==0) {
            Intent mIntent = new Intent(GroupTimelineTabs.this, RecordVideo.class);
            Bundle mBundle = new Bundle();
            mBundle.putBoolean(AppConsatants.LIVE_VIDEO, live);
            mBundle.putString(AppConsatants.UUID, ModelManager.getInstance().getGroupProfile().getUuid());
            mBundle.putBoolean(AppConsatants.UPLOAD_MOMENT_GROUP, true);
            mIntent.putExtras(mBundle);
            startActivity(mIntent);
        }else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    // Need Rationale
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                }
                            });
                    return;
                }
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;

            }
         /*   if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
                showMessageOKCancel("You need to allow access to Camera",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });

                return;
            } else {
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;
            }*/
        }

    }

    private void initToolbars() {
        toolbarTop = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle= (TextView) findViewById(R.id.toolbarTitle);
        toolbarSubTitle= (TextView) findViewById(R.id.toolbarSubTitle);
        setSupportActionBar(toolbarTop);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarTop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });

        GroupProfile obj = ModelManager.getInstance().getGroupProfile();
        if (obj != null) {
            toolbarTitle.setText("  "+obj.getName());
            if(obj.getLastActiveDate()>0)
                toolbarSubTitle.setText("  "+getString(R.string.group_last_active,Utils.getInstance().getDateDiffString(obj.getLastActiveDate())));
        } else {
            toolbarTitle.setText("");
            toolbarSubTitle.setText("");
        }
      /*  toolbarTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
        toolbarTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntentVideo = new Intent(GroupTimelineTabs.this, GroupProfileActivity.class);
                startActivity(mIntentVideo);
            }
        });
        toolbarSubTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntentVideo = new Intent(GroupTimelineTabs.this, GroupProfileActivity.class);
                startActivity(mIntentVideo);
            }
        });
        getSupportActionBar().setLogo(getDrawableFromPath(Utils.getInstance().getLocalContentPath(obj.getThumbNail())));
        toolbarTop.setPadding(0, 0, 0, 0);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.timeline_view_pager);
        setupViewPager(viewPager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.timeline_tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }
    public Drawable getDrawableFromPath(String filePath) {
        BitmapDrawable returnBitmap=null;
        try {
            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            Bitmap resized = Bitmap.createScaledBitmap(bitmap, 75, 75, true);
            returnBitmap = new BitmapDrawable(getResources(), Utils.getInstance().createCircleBitmap(resized));
        }catch(Exception e){

        }
        if(returnBitmap==null){
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.group_default);
            Bitmap resized = Bitmap.createScaledBitmap(bitmap, 75, 75, true);
            returnBitmap = new BitmapDrawable(getResources(), Utils.getInstance().createCircleBitmap(resized));
        }
        return returnBitmap;
    }
    //New
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_group, menu);

        if (menu != null) {
            MenuItem delete = menu.findItem(R.id.action_group_delete);


            menuPending = menu.findItem(R.id.action_pending);
            MenuItemCompat.setActionView(menuPending, R.layout.menu_request_notification);
            View viewNotif =  MenuItemCompat.getActionView(menuPending);
            RelativeLayout notifCountRelativeLayout= (RelativeLayout) viewNotif.findViewById(R.id.pending_request_layout);

            notifCountMenu= (TextView) viewNotif.findViewById(R.id.pending_count);
            int count = 0;
            if(ModelManager.getInstance().getGroupProfile()!=null)
             count = ModelManager.getInstance().getGroupProfile().getNoOfPendingRequests();
            notifCountMenu.setText(""+count);
            notifCountRelativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent mIntentActivity = new Intent(GroupTimelineTabs.this, PendingRequestActivity.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putBoolean(AppConsatants.GROUP_ROLE_MODERATOR, true);
                    mBundle.putString(AppConsatants.UUID, ModelManager.getInstance().getGroupProfile().getUuid());
                    mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, false);
                    mIntentActivity.putExtras(mBundle);
                    startActivity(mIntentActivity);
                }
            });
//            notifCountMenu.setTextColor(getResources().getColor(R.color.appcolor));

            if(!ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_OWNER)){
                delete.setVisible(false);
            }
            if(ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_OWNER)
                    ||ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MODERATOR)){
                menuPending.setVisible(true);
            }else{
                menuPending.setVisible(false);
            }
            if(count==0){
                menuPending.setVisible(false);
            }
        }
        return true;
    }

    //New
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_group_profile:
                Intent mIntentVideo = new Intent(GroupTimelineTabs.this, GroupProfileActivity.class);
                startActivity(mIntentVideo);
                break;
            case R.id.action_group_delete:
                showExitGroupMessageOwner(getString(R.string.alert_message_delete_group), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendDeleteGroup();
                    }
                });
                break;
            case R.id.action_pending:
                Intent mIntentActivity = new Intent(GroupTimelineTabs.this, PendingRequestActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean(AppConsatants.GROUP_ROLE_MODERATOR, true);
                mBundle.putString(AppConsatants.UUID, ModelManager.getInstance().getGroupProfile().getUuid());
                mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, false);
                mIntentActivity.putExtras(mBundle);
                startActivity(mIntentActivity);
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void onResume() {
        try {
            if (toolbarTop != null) {
                GroupProfile obj = ModelManager.getInstance().getGroupProfile();
                if (obj != null) {
                    toolbarTitle.setText("  " + obj.getName());
                    if (obj.getLastActiveDate() > 0)
                        toolbarSubTitle.setText("  " + getString(R.string.group_last_active, Utils.getInstance().getDateDiffString(obj.getLastActiveDate())));
                } else {
                    toolbarTitle.setText("");
                    toolbarSubTitle.setText("");
                }

                if (menuPending != null) {
                    int count = 0;
                    if (ModelManager.getInstance().getGroupProfile() != null)
                        count = ModelManager.getInstance().getGroupProfile().getNoOfPendingRequests();
                    if (count > 0) {
                        notifCountMenu.setText("" + count);
                        menuPending.setVisible(true);
                    } else {
                        menuPending.setVisible(false);
                    }
                }
            }
        }catch(Exception e){

        }
        super.onResume();
    }

    private void showMessage(final String message,final DialogInterface.OnClickListener okClicked) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(GroupTimelineTabs.this,R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, okClicked)
                        .show();
            }
        });
    }
    private void showExitGroupMessageOwner(final String mesg, final DialogInterface.OnClickListener okListner) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                new AlertDialog.Builder(GroupTimelineTabs.this,R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(mesg)
                        .setPositiveButton(android.R.string.ok, okListner)
                        .setNegativeButton(android.R.string.cancel,null)
                        .show();
            }
        });
    }
    private void sendDeleteGroup() {
        try {

            final String userUUID = DataStorage.getInstance().getUserUUID();
            String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
            String url = ServiceURLManager.getInstance().getGroupDeleteUrl(groupUuid,userUUID);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    showMessage(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                }
            });

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void selectVideo() {
//        sendMomentList();
        if (ContextCompat.checkSelfPermission(activity,Manifest.permission.READ_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("video/*");
            startActivityForResult(intent, GALLERY_INTENT_CALLED);
        }else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showMessageOKCancel("You need to allow access to storage",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);

                            }
                        });

                return;
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);

                return;
            }
        }

    }


    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedImageUri = null;
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_INTENT_CALLED) {
                selectedImageUri = data.getData();
                String path = Utils.getInstance().getPath(this, selectedImageUri,false);
//					Bitmap bp = ThumbnailUtils.createVideoThumbnail(selectedImagePath, MediaStore.Images.Thumbnails.MINI_KIND);
//                Log.i("Mediapath", path);
                if(!path.isEmpty()) {
                    Intent mIntent = new Intent(GroupTimelineTabs.this, UploadActivity.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString(AppConsatants.MOMENT_PATH, path);
                    mBundle.putString(AppConsatants.UUID, ModelManager.getInstance().getGroupProfile().getUuid());
                    mBundle.putBoolean(AppConsatants.UPLOAD_MOMENT_GROUP, true);
                    mBundle.putBoolean(AppConsatants.RESHARE, false);
                    mIntent.putExtras(mBundle);
                    startActivity(mIntent);
                }

            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new GroupTimelineVows(), getString(R.string.title_fragment_vows));
        adapter.addFrag(new GroupLiveVows(), getString(R.string.title_live));
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onNotificationReceived(ActivityLog activityLog) {

    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        if(layout.getVisibility() == View.VISIBLE){
            CloseBottomListView();
        }else {
            layout.setVisibility(View.INVISIBLE);
            finish();
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        CloseBottomListView();
    }
}