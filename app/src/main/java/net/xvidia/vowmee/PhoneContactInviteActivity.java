package net.xvidia.vowmee;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.FilterQueryProvider;
import android.widget.RelativeLayout;

import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.tweetcomposer.Card;
import com.twitter.sdk.android.tweetcomposer.ComposerActivity;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.contactsync.provider.FeedProvider;
import net.xvidia.vowmee.fragments.PhoneContactListFragment;
import net.xvidia.vowmee.helper.CustomFontEditTextView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class PhoneContactInviteActivity extends AppCompatActivity  {

    private static final int REQUEST_CODE_ASK_PERMISSIONS = 1111;
    private TwitterLoginButton twitterLoginButton;
    public static Activity activity;
    public boolean permissionGranted;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_phone_contacts);
//        SyncUtils.TriggerRefresh();
        activity =this;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        bindActivity();
    }

    private void bindActivity() {

        CustomFontEditTextView inputSearch = (CustomFontEditTextView) findViewById(R.id.search_query);
        RelativeLayout facebook = (RelativeLayout) findViewById(R.id.action_invite_facebook_friends);
        RelativeLayout twitter = (RelativeLayout) findViewById(R.id.action_invite_twitter_friends);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.comment_toolbar);

        twitterLoginButton = (TwitterLoginButton) findViewById(R.id.twitter_button);

        TwitterAuthConfig authConfig = new TwitterAuthConfig(getString(R.string.twitter_consumer_key), getString(R.string.twitter_secret_key));
        Fabric.with(this, new TwitterCore(authConfig), new TweetComposer());
        twitterLoginButton = (TwitterLoginButton) findViewById(R.id.twitter_button);
        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                postTwitterAppInvite();
            }

            @Override
            public void failure(TwitterException exception) {
            }
        });
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                PhoneContactListFragment.mAdapter.getFilter().filter(s.toString());
                PhoneContactListFragment.mAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFacebookInvites();
            }
        });
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postTwitterAppInvite();
            }
        });
        PhoneContactListFragment.mAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence constraint) {
                return FeedProvider.searchString(constraint.toString());
            }
        });
        checkSmsPermission();
    }


    private void sendFacebookInvites() {
        String appLinkUrl, previewImageUrl;

//		appLinkUrl = "https://play.google.com/store/apps/details?id=com.bezuur.ui";
        appLinkUrl = "https://fb.me/1687087211610697";
        previewImageUrl = getString(R.string.api_facebook_pinvite_preview_url);

        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLinkUrl)
                    .setPreviewImageUrl(previewImageUrl)
                    .build();
            AppInviteDialog.show(this, content);
        }
    }

    public void postTwitterAppInvite() {
        TwitterSession twitterSession = Twitter.getSessionManager().getActiveSession();
        if (twitterSession == null) {
            twitterLoginButton.performClick();
            return;
        }
        String filename ="+918800900109";// DataStorage.getInstance().getUsername();
        File myImageFile = new File(Utils.getInstance().getLocalImagePath(filename));
        Uri myImageUri = Uri.fromFile(myImageFile);

        final Card card = new Card.AppCardBuilder(PhoneContactInviteActivity.this)
                .imageUri(myImageUri)
                .googlePlayId("net.xvidia.vowmee")
                .build();
        final Intent intent = new ComposerActivity.Builder(PhoneContactInviteActivity.this)
                .session(twitterSession)
                .card(card)
                .createIntent();
        startActivity(intent);
    }


  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user_home, menu);
        return true;
    }

    private void selectVideo() {
//        sendMomentList();
        Intent intent = new Intent();
        intent.setType("video*//*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.home_profile_select_video)), GALLERY_INTENT_CALLED);

    }

    //New
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_timeline:
                Intent mIntent = new Intent(CommentActivity.this, TimelineTabs.class);
                startActivity(mIntent);
                finish();
                break;
            case R.id.action_setting:
                AWSMobileClient.defaultMobileClient().getIdentityManager().signOut();
                Intent mreg = new Intent(CommentActivity.this, RegisterActivity.class);
                startActivity(mreg);
                finish();
                break;
            case R.id.action_video:
//                AWSMobileClient.defaultMobileClient().getIdentityManager().signOut();
                Intent mIntentVideo = new Intent(CommentActivity.this, RecordVideo.class);
                startActivity(mIntentVideo);
                break;
            case R.id.action_search:
//                AWSMobileClient.defaultMobileClient().getIdentityManager().signOut();
                Intent mIntentSearch = new Intent(CommentActivity.this, SearchActivity.class);
                startActivity(mIntentSearch);
                break;
            default:
                break;
        }

        return true;
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE){
            twitterLoginButton.onActivityResult(requestCode, resultCode, data);
        }
    }


    private void checkSmsPermission() {

        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!Utils.getInstance().addPermission(permissionsList, android.Manifest.permission.READ_SMS))
            permissionsNeeded.add("Send SMS");

// final String[] PERMISSIONS_CONTACT = {Manifest.permission.READ_CONTACTS,Manifest.permission.READ_SMS};

        Map<String, Integer> perms = new HashMap<String, Integer>();
// Initial
        perms.put(android.Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);

        if (permissionsList.size() == 0) {
        } else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
// Need Rationale
// String message = "You need to grant access to Contacts and SMS permissions";
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                            REQUEST_CODE_ASK_PERMISSIONS);
                                }
                            });
                    return;
                }
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;

            }
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

         if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<String, Integer>();
// Initial
            perms.put(android.Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);

// Fill with results
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
// Check for ACCESS_FINE_LOCATION
            if (perms.get(android.Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
                permissionGranted = true;
            } else {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_message_permission_denied), Utils.LONG_TOAST);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

   /* private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
// Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
                return false;
        }
        return true;
    }*/

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(PhoneContactInviteActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
            finish();
    }
}
