package net.xvidia.vowmee;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.DividerItemDecoration;
import net.xvidia.vowmee.listadapter.BottomListAdapter;
import net.xvidia.vowmee.listadapter.ILoadMoreItems;
import net.xvidia.vowmee.listadapter.TimelineRecyclerViewAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;
import net.xvidia.vowmee.videoplayer.IVideoDownloadListener;
import net.xvidia.vowmee.videoplayer.VideosDownloader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static net.xvidia.vowmee.MyApplication.getAppContext;

public class TimelineFriendProfile extends AppCompatActivity implements IVideoDownloadListener, SwipeRefreshLayout.OnRefreshListener {
    private final int GALLERY_INTENT_CALLED = 1234;
    private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 123;
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 125;
    private static final int REQUEST_CODE_ASK_CALL_PERMISSIONS = 126;
    //    private boolean isLive;
    private Activity activity;
    private UserFileManager userFileManager;

    private Context context;
    private RecyclerView mRecyclerView;
    private TimelineRecyclerViewAdapter mAdapter;
    private List<Moment> momentList;
    VideosDownloader videosDownloader;
    private static String friendUsername;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FloatingActionButton videoMenu;
    private RelativeLayout layout;
    private Animation slideUp, slideDown;
    private ImageButton cancel;
    private LinearLayout tintLayout;
    private MenuItem callMenuItem;
    private MenuItem blockMenuItem;
    private MenuItem unBlockMenuItem;
    private TextView mMessageTextView;
    private boolean canscroll;
    private boolean refrshed;
    String[] bottomListItemName = {
            MyApplication.getAppContext().getResources().getString(R.string.menu_go_live),
//            MyApplication.getAppContext().getResources().getString(R.string.menu_create_event),
            MyApplication.getAppContext().getResources().getString(R.string.menu_record_video),
            MyApplication.getAppContext().getResources().getString(R.string.menu_upload_video)
    };
    //    public static boolean closelist;
    int[] bottomListImages = new int[]{
            R.drawable.go_live,
//            R.drawable.event,
            R.drawable.record_new,
            R.drawable.upload_new
    };

    private boolean isLoading;
    private int currentPage;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount, firstVisibleItem;

    public static String getFriendUsername() {
        return friendUsername;
    }

    public static void setFriendUsername(String friendUsername) {
        TimelineRecyclerViewAdapter.refreshimeline = false;
        TimelineFriendProfile.friendUsername = friendUsername;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mMessageTextView = (TextView) findViewById(R.id.message_no_moments);
        swipeRefreshLayout.setOnRefreshListener(this);
//        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.appcolor);
        swipeRefreshLayout.setColorSchemeResources(R.color.init_gradient, R.color.com_facebook_blue, R.color.red_color);
//        swipeRefreshLayout.setDistanceToTriggerSync(20);
        mRecyclerView = (RecyclerView) findViewById(R.id.timeline_recycler_view);
        videoMenu = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        ListView listView = (ListView) findViewById(R.id.bottomlistview);
        listView.setAdapter(new BottomListAdapter(this, bottomListItemName, bottomListImages));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CloseBottomListView();
                switch (position) {
                    case 0:
                        if (Utils.getInstance().checkCameraHardware(context)) {
//                            isLive = true;
                            DataStorage.getInstance().setIsLiveFlag(true);
                            callCameraApp(true);
                        } else {
                            showMessage("Your phone does not support camera", null);
                        }
                        break;

                    case 1:
                        if (Utils.getInstance().checkCameraHardware(context)) {
//                            isLive = false;
                            DataStorage.getInstance().setIsLiveFlag(false);
                            callCameraApp(false);
                        } else {
                            showMessage("Your phone does not support camera", null);
                        }
                        break;

                    case 2:
                        selectVideo();
                        break;

                }
            }
        });
        layout = (RelativeLayout) findViewById(R.id.bottomLayout);
        tintLayout = (LinearLayout) findViewById(R.id.tint_layout);
        cancel = (ImageButton) findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseBottomListView();
            }
        });
        slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        tintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseBottomListView();
            }
        });
        tintLayout.setClickable(false);
        videoMenu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                slideToTop(tintLayout);
                layout.startAnimation(slideUp);
                layout.setVisibility(View.VISIBLE);

            }
        });
        userFileManager = Utils.getInstance().getUserFileManager();
        activity = this;
        context = TimelineFriendProfile.this;
//        Intent intent = getIntent();
//        if (null != intent) { //Null Checking
//            friendUsername = intent.getStringExtra(AppConsatants.PERSON_FRIEND);
//        }
        mMessageTextView.setVisibility(View.GONE);
        momentList = new ArrayList<>();
        refrshed = false;
        initialiseRecyclerView();
        mAdapter.notifyDataSetChanged();
    }


//    private boolean addPermission(List<String> permissionsList, String permission) {
//        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
//            permissionsList.add(permission);
//            // Check for Rationale Option
//            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
//                return false;
//        }
//        return true;
//    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(TimelineFriendProfile.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<String, Integer>();
            // Initial
            perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
            perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
            perms.put(android.Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);

            // Fill with results
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
            // Check for ACCESS_FINE_LOCATION
            if (perms.get(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                    && perms.get(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && perms.get(android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                callCameraApp(DataStorage.getInstance().getIsLiveFlag());
            } else {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_message_camera_permission_denied), Utils.LONG_TOAST);
            }
        } else if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectVideo();
            } else {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_message_upload_permission_denied), Utils.LONG_TOAST);
            }
        }/*else if(requestCode == REQUEST_CODE_ASK_CALL_PERMISSIONS){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                callFriend();
            }else{
                Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_message_upload_permission_denied), Utils.LONG_TOAST);
            }
        }*/ else if (requestCode == REQUEST_CODE_ASK_CALL_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<String, Integer>();
// Initial
            perms.put(android.Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);
            perms.put(android.Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);

// Fill with results
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
// Check for ACCESS_FINE_LOCATION
            if (perms.get(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                    && perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                callFriend();
            } else {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_message_upload_permission_denied), Utils.LONG_TOAST);
            }
/* if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
callFriend();
}else{
Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_message_upload_permission_denied), Utils.LONG_TOAST);
}*/
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void callCameraApp(boolean live) {

        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!Utils.getInstance().addPermission(permissionsList, android.Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!Utils.getInstance().addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write External Storage");
        if (!Utils.getInstance().addPermission(permissionsList, android.Manifest.permission.RECORD_AUDIO))
            permissionsNeeded.add("Record Audio");

        Map<String, Integer> perms = new HashMap<String, Integer>();
        // Initial
        perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
        perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
        perms.put(android.Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);

        if (permissionsList.size() == 0) {
            Intent mIntent = new Intent(TimelineFriendProfile.this, RecordVideo.class);
            Bundle mBundle = new Bundle();
            mBundle.putBoolean(AppConsatants.LIVE_VIDEO, live);
            mIntent.putExtras(mBundle);
            startActivity(mIntent);
        } else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    // Need Rationale
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                }
                            });
                    return;
                }
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;

            }
         /*   if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
                showMessageOKCancel("You need to allow access to Camera",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });

                return;
            } else {
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;
            }*/
        }
    }

    private void callFriendPermission() {

        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!Utils.getInstance().addPermission(permissionsList, android.Manifest.permission.RECORD_AUDIO))
            permissionsNeeded.add("Record Audio");
        if (!Utils.getInstance().addPermission(permissionsList, android.Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("Handle Calls");

        Map<String, Integer> perms = new HashMap<String, Integer>();
// Initial
        perms.put(android.Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);
        perms.put(android.Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);


        if (permissionsList.size() == 0) {
            callFriend();
        } else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
// Need Rationale
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                            REQUEST_CODE_ASK_CALL_PERMISSIONS);
                                }
                            });
                    return;
                }
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_CALL_PERMISSIONS);
                return;

            }

        }
    }

    public void slideToBottom(View view) {
        tintLayout.setClickable(false);
        TranslateAnimation animate = new TranslateAnimation(0, 0, 0, view.getHeight());
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);

    }

    public void slideToTop(View view) {
        tintLayout.setBackgroundColor(getResources().getColor(R.color.bottom_list_tint));
        tintLayout.setClickable(true);
        TranslateAnimation animate = new TranslateAnimation(0, 0, view.getHeight(), 0);
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);

    }

    public void CloseBottomListView() {

        if (layout.getVisibility() == View.VISIBLE) {
            slideToBottom(tintLayout);
//        tintLayout.setVisibility(View.INVISIBLE);
            layout.startAnimation(slideDown);
            layout.setVisibility(View.INVISIBLE);
        }
    }


    private void showMessage(final String mesg, final DialogInterface.OnClickListener okClicked) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new AlertDialog.Builder(TimelineFriendProfile.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .setCancelable(false)
                            .show();
                }
            });
        } catch (WindowManager.BadTokenException e) {
        } catch (Exception e) {
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        bindActivity();
    }

    private void bindActivity() {

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
//        getSupportActionBar().setTitle(getString(R.string.title_Profile));
        mToolbar.setPadding(0, 0, 0, 0);
        momentList = new ArrayList<Moment>();

        videosDownloader = new VideosDownloader(context);
        videosDownloader.setOnVideoDownloadListener(this);

        showProgressBar();
//        getProfileRequest(false);
//        getVideoUrls();
    }

    private void getProfileRequest(final boolean firstTime) {
        try {
            final String username = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getProfileUrl(IAPIConstants.API_KEY_GET_PROFILE, username, friendUsername);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }


                    if(obj !=null) {
                        String getUsername = obj.getUsername();
                        if (getUsername != null && !getUsername.isEmpty() && !username.equals(getUsername)) {
                            ModelManager.getInstance().setOtherProfile(obj);
                            if (obj.isFriendWithViewer()) {
                                if (callMenuItem != null) {
                                    callMenuItem.setVisible(true);
                                }
                            } else {
                                if (callMenuItem != null) {
                                    callMenuItem.setVisible(false);
                                }
                            }
                            if (obj.isBlockedByViewer()) {
                                if (blockMenuItem != null && unBlockMenuItem != null) {
                                    blockMenuItem.setVisible(false);
                                    unBlockMenuItem.setVisible(true);
                                }
                            } else {
                                if (blockMenuItem != null && unBlockMenuItem != null) {
                                    blockMenuItem.setVisible(true);
                                    unBlockMenuItem.setVisible(false);
                                }
                            }
                            if(obj.getDisplayName()!=null)
                            getSupportActionBar().setTitle(obj.getDisplayName());
                            sendMomentList(obj.getUsername(), firstTime);
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {

        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void initialiseRecyclerView() {
        TimelineRecyclerViewAdapter.refreshimeline = false;
        if (momentList != null && momentList.size() > 0) {
            Moment obj = momentList.get(0);
            if (obj != null && obj.getOwner() != null) {
                Moment objDummy = new Moment();
                momentList.add(0, objDummy);
            }
        } else {
            momentList = new ArrayList<>();
            Moment objDummy = new Moment();
            momentList.add(0, objDummy);
        }

        swipeRefreshLayout.setRefreshing(false);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new TimelineRecyclerViewAdapter(true, false, TimelineFriendProfile.this, activity, momentList); // true: with header
        final LinearLayoutManager layoutManager;
        canscroll = true;
        if (momentList != null && momentList.size() < 2)
            canscroll = false;
        layoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return canscroll;
            }
        };
        mRecyclerView.setLayoutManager(layoutManager);
        if (!refrshed) {
            refrshed = true;
            mRecyclerView.addItemDecoration(new DividerItemDecoration(20, true));
        }
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnLoadMoreListener(new ILoadMoreItems() {
            @Override
            public void loadMoreItems(boolean load) {
                isLoading = true;
                TimelineRecyclerViewAdapter.disableLoad= false;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadMore();
                    }
                },450);
            }
        });
//        if(Utils.getInstance().hasConnection(context))
//        {
//            getVideoUrls();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
                int findFirstCompletelyVisibleItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition();
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                firstVisibleItem = firstVisiblePosition;
                Moment video;
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (momentList != null && momentList.size() > 1) {
                        if (findFirstCompletelyVisibleItemPosition > 0) {
                            video = momentList.get(findFirstCompletelyVisibleItemPosition);
                            mAdapter.videoPlayerController.setcurrentPositionOfItemToPlay(findFirstCompletelyVisibleItemPosition);
                            mAdapter.videoPlayerController.handlePlayBack(video, false);
                        } else if (firstVisiblePosition > 0) {
                            video = momentList.get(firstVisiblePosition);
                            mAdapter.videoPlayerController.setcurrentPositionOfItemToPlay(firstVisiblePosition);
                            mAdapter.videoPlayerController.handlePlayBack(video, false);
                        } else if (firstVisiblePosition > -1) {
                            video = momentList.get(1);
                            mAdapter.videoPlayerController.setcurrentPositionOfItemToPlay(1);
                            mAdapter.videoPlayerController.handlePlayBack(video, false);
                        }
                    }
                } else {
                    mAdapter.videoPlayerController.pauseVideo();
                    if (!isLoading && totalItemCount > 9 && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        momentList.add(null);
                        mAdapter.notifyItemInserted(momentList.size() - 1);
                        isLoading = true;
//                        Log.i("Loading More","currentPage ="+currentPage);
                    }
                }
            }
        });

    }

    private void loadMore() {
        try {
            final int previousPage = currentPage;
//            String friendUsername = ModelManager.getInstance().getOtherProfile().getUsername();
            final String username = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, username, friendUsername, AppConsatants.FALSE, "" + currentPage, "10");
//            Log.i("Loading completed","currentPage ="+currentPage +"\nUrl "+url);
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<Moment> obj = null;
                    try {
//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Moment>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    isLoading = false;
                    hideProgressBar();
                    Moment lastMoment = momentList.get(momentList.size() - 1);
                    if (lastMoment == null) {
                        momentList.remove(momentList.size() - 1);
                        mAdapter.notifyItemRemoved(momentList.size());
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {
                            if (momentList == null)
                                momentList = obj;
                            if (currentPage == previousPage) {
                                currentPage = previousPage + 1;
                                mMessageTextView.setVisibility(View.GONE);
                                momentList.addAll(obj);
                                videosDownloader.startVideosDownloading(obj, true);

//                            HashSet<Moment> set = new HashSet<>();
//                            set.addAll(momentList);
//                            momentList.clear();
//                            momentList.addAll(set);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                int curSize = mAdapter.getItemCount();
                                mAdapter.setMomentList(momentList);
                                mAdapter.notifyItemRangeInserted(curSize, momentList.size() - 1);
                            }
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    isLoading = false;
                    currentPage = currentPage - 1;
                    if (currentPage < previousPage)
                        currentPage = previousPage;
                    if (momentList != null) {
                        if (momentList.size() > 0) {
                            Moment lastMoment = momentList.get(momentList.size() - 1);
                            if (lastMoment == null) {
                                momentList.remove(momentList.size() - 1);
                                mAdapter.notifyItemRemoved(momentList.size());
                            }
                        }
                        if (momentList != null && momentList.size() > 0) {
                            mMessageTextView.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.VISIBLE);
                        } else {
                            mMessageTextView.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.GONE);
                        }
                    }
                }
            }) {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        //                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        if (userFileManager != null) {
            userFileManager.destroy();
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user_profile, menu);
        if (menu != null) {
            blockMenuItem = menu.findItem(R.id.action_block);
            unBlockMenuItem = menu.findItem(R.id.action_unBlock);
            callMenuItem = menu.findItem(R.id.action_call);
            if (blockMenuItem != null && unBlockMenuItem != null) {
                blockMenuItem.setVisible(false);
                unBlockMenuItem.setVisible(false);
                callMenuItem.setVisible(false);
            }
        }
        return true;
    }


    //New
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_block:
                if (ModelManager.getInstance().getOtherProfile() != null) {
                    showOkCancel(context.getResources().getString(R.string.alert_message_block_user, ModelManager.getInstance().getOtherProfile().getDisplayName()), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sendBlockUserRequest(true);
                        }
                    });
                }
                break;
            case R.id.action_unBlock:
                sendBlockUserRequest(false);
                break;
            case R.id.action_call:
                callFriendPermission();
                break;
            default:
                break;
        }

        return true;
    }


    private void callFriend() {
        Profile obj = ModelManager.getInstance().getOtherProfile();
        if (obj == null)
            return;
        String profImagePath = obj.getProfileImage();
        profImagePath = (profImagePath == null ? obj.getThumbNail() : (profImagePath.isEmpty() ? obj.getProfileImage() : obj.getUsername() + AppConsatants.FILE_EXTENSION_JPG));

        Intent mIntent = new Intent(context, MakeVideoCallActivity.class);
        Bundle mBundle = new Bundle();
        mBundle.putString(AppConsatants.UUID, obj.getUuid());
        mBundle.putString(AppConsatants.USER_THUMBNAIL, profImagePath);
        mBundle.putString(AppConsatants.PERSON_FRIEND, obj.getDisplayName());
        mIntent.putExtras(mBundle);
        mIntent.replaceExtras(mBundle);
        context.startActivity(mIntent);
    }

    @Override
    protected void onResume() {
        friendUsername = getFriendUsername();
//        if(mRecyclerView!=null)
//            mRecyclerView.setVisibility(View.GONE);
        if (!TimelineRecyclerViewAdapter.refreshimeline)
            getProfileRequest(true);
        if(PublishLiveActivity.closePublish){
            closePublishSessionConnection();
        }
        setGoogleAnalyticScreen("Friends Profile");
        super.onResume();
    }

    public void closePublishSessionConnection() {

        if (PublishLiveActivity.mSessionId == null) {
            return;
        }
        if (PublishLiveActivity.mSessionId.isEmpty()) {
            return;
        }
        String subUrl = ServiceURLManager.getInstance().getDestroyLive(PublishLiveActivity.mSessionId, DataStorage.getInstance().getUserUUID());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, subUrl, "{}", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    PublishLiveActivity.mSessionId="";
                    PublishLiveActivity.closePublish = false;
                    String url = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, DataStorage.getInstance().getUsername(), DataStorage.getInstance().getUsername(), AppConsatants.TRUE, "0", "0");
                    VolleySingleton.getInstance(getAppContext()).refresh(url, false);
                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }
    @Override
    public void onVideoDownloaded(Moment video, boolean image) {
        if (mAdapter != null)
            mAdapter.videoPlayerController.handlePlayBack(video, image);
    }

    private void selectVideo() {
//        sendMomentList();
        if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent();
            intent.setType("video/*");
            intent.setAction(Intent.ACTION_PICK);
//            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//            intent.setType("video/*");
            startActivityForResult(intent, GALLERY_INTENT_CALLED);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showMessageOKCancel("You need to allow access to Media Storage",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);

                            }
                        });

                return;
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);

                return;
            }
        }

    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedImageUri = null;
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_INTENT_CALLED) {
                selectedImageUri = data.getData();
                String path = Utils.getInstance().getPath(this, selectedImageUri, false);
                if(!path.isEmpty()) {
                    Intent mIntent = new Intent(TimelineFriendProfile.this, UploadActivity.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString(AppConsatants.MOMENT_PATH, path);
                    mBundle.putBoolean(AppConsatants.RESHARE, false);
                    mIntent.putExtras(mBundle);
                    startActivity(mIntent);
                }else{
                    showAlert();
                }

            }
        }
    }
    private void showAlert() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(TimelineFriendProfile.this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Large file")
                            .setMessage(getString(R.string.error_large_file_upload))
                            .show();
                }
            });

        }catch(WindowManager.BadTokenException e){

        }catch (Exception e){}
    }

    private void sendMomentList(String friendUsername, final boolean firstTime) {
        try {

            currentPage = 1;
            final String username = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, username, friendUsername, AppConsatants.FALSE, "1", "10");

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<Moment> obj = null;
                    try {
//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Moment>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    hideProgressBar();
                    momentList = new ArrayList<>();
                    if (obj != null) {

                        momentList = obj;
                        if (obj.size() > 0) {
                            currentPage = 2;
                            mMessageTextView.setVisibility(View.GONE);
                            momentList = obj;
                            videosDownloader.startVideosDownloading(momentList, firstTime);
                            if(mRecyclerView!=null)
                                mRecyclerView.setVisibility(View.VISIBLE);
                            initialiseRecyclerView();
                            mAdapter.notifyDataSetChanged();
                            if (momentList.size() == 1)
                                mMessageTextView.setVisibility(View.GONE);
                            else
                                mMessageTextView.setVisibility(View.GONE);
                        } else {
                            initialiseRecyclerView();
                            mAdapter.notifyDataSetChanged();
                            if(mRecyclerView!=null)
                                mRecyclerView.setVisibility(View.VISIBLE);
                            if (momentList.size() == 1)
                                mMessageTextView.setVisibility(View.GONE);
                            else
                                mMessageTextView.setVisibility(View.GONE);
//                            showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    finish();
//                                }
//                            });
                        }
                    } else {
                        initialiseRecyclerView();
                        mAdapter.notifyDataSetChanged();
                        if(mRecyclerView!=null)
                            mRecyclerView.setVisibility(View.VISIBLE);
                        if (momentList.size() == 1)
                            mMessageTextView.setVisibility(View.GONE);
                        else
                            mMessageTextView.setVisibility(View.GONE);
//                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                finish();
//                            }
//                        });
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    initialiseRecyclerView();
                    mAdapter.notifyDataSetChanged();
                    if (momentList.size() == 1)
                        mMessageTextView.setVisibility(View.GONE);
                    else
                        mMessageTextView.setVisibility(View.GONE);
                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendBlockUserRequest(final boolean block) {
        try {
            showProgressBar();
            String userUuid = DataStorage.getInstance().getUserUUID();
            String blockinguuid = ModelManager.getInstance().getOtherProfile().getUuid();
            String url = ServiceURLManager.getInstance().getBlockUserUrl(userUuid, blockinguuid);
            if (!block)
                url = ServiceURLManager.getInstance().getUnBlockUserUrl(userUuid, blockinguuid);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        if (obj.isBlockedByViewer()) {
                            if (blockMenuItem != null && unBlockMenuItem != null) {
                                blockMenuItem.setVisible(false);
                                unBlockMenuItem.setVisible(true);
                            }
                        } else {
                            if (blockMenuItem != null && unBlockMenuItem != null) {
                                blockMenuItem.setVisible(true);
                                unBlockMenuItem.setVisible(false);
                            }
                        }
                        ModelManager.getInstance().setOtherProfile(obj);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    if (error != null && error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_CONFLICT) {
//                            Log.e("VolleyError ", "" + error.networkResponse.statusCode);
                            showError(getString(R.string.error_user_already_block, ModelManager.getInstance().getOtherProfile().getDisplayName()), null);

                        } else {
                            Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.LONG_TOAST);
                        }
                    }

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }


    private void showError(final String message, final DialogInterface.OnClickListener okClicked) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(TimelineFriendProfile.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(message)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .show();
                }
            });
        } catch (WindowManager.BadTokenException e) {
        } catch (Exception r) {
        }
    }

    private void showOkCancel(final String message, final DialogInterface.OnClickListener okClicked) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(TimelineFriendProfile.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(message)
                            .setNegativeButton(android.R.string.cancel, null)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .show();
                }
            });
        } catch (WindowManager.BadTokenException e) {

        } catch (Exception e) {
        }
    }

    private void showProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(true);
            }
        });

    }

    private void hideProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

//                if (progressDialog != null) {
//                    progressDialog.dismiss();
//                }
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (layout.getVisibility() == View.VISIBLE) {
            CloseBottomListView();
        } else {
            layout.setVisibility(View.INVISIBLE);
            finish();
            super.onBackPressed();
        }
    }

    @Override
    public void onRefresh() {

        TimelineRecyclerViewAdapter.refreshimeline = false;
        getProfileRequest(false);
    }
    private void setGoogleAnalyticScreen(String screenName){
        MyApplication application = (MyApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.setClientId(DataStorage.getInstance().getUserUUID());
        mTracker.setAppVersion(""+Utils.getInstance().getAppVersion(MyApplication.getAppContext()));
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

}
