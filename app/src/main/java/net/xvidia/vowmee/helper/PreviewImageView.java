package net.xvidia.vowmee.helper;

import android.content.Context;
import android.content.res.Configuration;
import android.media.MediaMetadataRetriever;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.network.model.Moment;

import java.io.File;

/**
 * Created by Ravi_office on 06-Jun-16.
 */
public class PreviewImageView extends ImageView {
    Moment video;
    String url;
    Context mContext;
    private String height, width;
    private int intWidth,intHeight;
    private int newHeight, newWidth;
    public PreviewImageView(Context context) {
        super(context);
    }
    public PreviewImageView(Context context,String url) {
        super(context);
        mContext = context;
        this.url = url;
    }
    public PreviewImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PreviewImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        Configuration configuration = MyApplication.getAppContext().getResources().getConfiguration();
        int screenHeightDp = configuration.screenWidthDp;
        int deviceWidth = Utils.getInstance().convertDpToPixel(screenHeightDp, mContext);
        newWidth = deviceWidth;
        newHeight = newWidth;
        try {
            if (url != null && getVisibility(url)) {

                MediaMetadataRetriever fetchVideoInfo = new MediaMetadataRetriever();
                fetchVideoInfo.setDataSource(url);
                height = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
                width = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
                intWidth = Utils.getInstance().getIntegerValueOfString(width);
                intHeight = Utils.getInstance().getIntegerValueOfString(height);
                if (intWidth < deviceWidth) {
                    float height = intHeight;
                    float width = intWidth;
                    height = (deviceWidth / width) * height;
                    intHeight = (int) height;
                    intWidth = deviceWidth;
                }
//            Log.i("screen width", "width size: " + deviceWidth+"===="+ intWidth + 'x' + intHeight+"\n");
                newWidth = getDefaultSize(intWidth, widthMeasureSpec);
                newHeight = getDefaultSize(intHeight, heightMeasureSpec);
                if (intWidth > 0 && intHeight > 0) {
                    if (intWidth * newHeight > newWidth * intHeight) {
                        // Log.i("@@@", "image too tall, correcting");
                        newHeight = newWidth * intHeight / intWidth;
                    } else if (intWidth * newHeight < newWidth * newHeight) {
                        // Log.i("@@@", "image too wide, correcting");
                        newWidth = newHeight * intWidth / intHeight;
                    } else {
                        newHeight = intHeight;
                        newWidth = intWidth;
                        // Log.i("@@@", "aspect ratio is correct: " +
                        // width+"/"+height+"="+
                        // mVideoWidth+"/"+mVideoHeight);
                    }
                }
            }
        } catch(IllegalStateException e) {
        }catch (Exception e) { Log.i("Videosize", "setting size: " +e.getMessage());
        }
        setMeasuredDimension(newWidth, newHeight);
    }
    private boolean getVisibility(String filePath) {
        boolean visible = false;
        try {
            if (filePath != null && !filePath.isEmpty()) {
//            String filePath = Utils.getInstance().getLocalContentPath(fileName);
                File vidFile = new File(filePath);
                if (vidFile != null && vidFile.exists()) {
                    visible = true;
                }
            }
        }catch (Exception e){

        }
        return visible;
    }
}
