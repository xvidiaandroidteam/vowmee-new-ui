package net.xvidia.vowmee.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Ravi_office on 16-Feb-16.
 */
public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    private static final int[] ATTRS = new int[]{android.R.attr.listDivider};

    private Drawable mDivider;

    /**
     * Default divider will be used
     */
    public DividerItemDecoration(Context context) {
        final TypedArray styledAttributes = context.obtainStyledAttributes(ATTRS);
        mDivider = styledAttributes.getDrawable(0);
        styledAttributes.recycle();
    }

    private int space;
    private boolean noSpacing;

    public DividerItemDecoration(int space, boolean noSpacing) {
        this.space = space;
        this.noSpacing = noSpacing;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
//        outRect.left = space;
//        outRect.right = space;
//        outRect.bottom = space;

        // Add top margin only for the first item to avoid double space between items
//        if(parent.getChildAdapterPosition (view) == 0)
//            outRect.top = space;
        if (!noSpacing) {
            if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1) {
                if (space > 0) {
                    outRect.bottom = space;
                }
            }
        } else {
            if (parent.getChildAdapterPosition(view) != 0 && parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1) {
                if (space > 0) {
                    outRect.bottom = space;
                }
            }
//            if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1) {
//                if (space > 0) {
//                    outRect.bottom = space;
//                }
//            }
        }
    }
    /**
     * Custom divider will be used
     */
    /*public DividerItemDecoration(Context context, int resId) {
        mDivider = ContextCompat.getDrawable(context, resId);
    }*/
/*
    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if(mDivider == null)
            return;
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top;
            if (parent.getChildAdapterPosition(child) != parent.getAdapter().getItemCount() - 1) {
                bottom = top + mDivider.getIntrinsicHeight();
            }
//            int bottom = top + mDivider.getIntrinsicHeight();

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }*/
}
