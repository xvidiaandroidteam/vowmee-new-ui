package net.xvidia.vowmee.helper;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import net.xvidia.vowmee.Utils.CustomFontUtils;


/**
 * Created by Ravi_office on 18-Jan-16.
 */
public class CustomFontEditTextView extends EditText {

    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";

    public CustomFontEditTextView(Context context) {
        super(context);

        CustomFontUtils.applyCustomFont(this, context, null);
    }

    public CustomFontEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        CustomFontUtils.applyCustomFont(this, context, attrs);
    }

    public CustomFontEditTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        CustomFontUtils.applyCustomFont(this, context, attrs);
    }
}
