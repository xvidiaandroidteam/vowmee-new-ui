package net.xvidia.vowmee.helper;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;

/**
 * Created by Ravi_office on 22-Mar-16.
 */
public class ScrollableSwipeRefreshLayout extends SwipeRefreshLayout {

    public ScrollableSwipeRefreshLayout(Context context) {
        super(context);
    }

    public ScrollableSwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

  /*  @Override
    public boolean canChildScrollUp() {
        // Condition to check whether scrollview reached at the top while scrolling or not
        return findViewById(R.id).canScrollVertically(-1);
    }*/
}
