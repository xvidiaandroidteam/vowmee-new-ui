package net.xvidia.vowmee;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.listadapter.CommentsAdapter;
import net.xvidia.vowmee.listadapter.EmoticonsGridAdapter;
import net.xvidia.vowmee.listadapter.EmoticonsPagerAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.Comment;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

public class CommentActivity extends AppCompatActivity implements EmoticonsGridAdapter.KeyClickListener  {
    private final int GALLERY_INTENT_CALLED = 1234;
    private UserFileManager userFileManager;

    private Context context;
    private ListView mRecyclerView;
    private CommentsAdapter mAdapter;
    private List<Comment> commentList;
    private ProgressDialog progressDialog;
    private String uuid;
    private EditText mCommentEditText;
    private ImageButton mCommentSendButton;
    private boolean notificationIntent;
    private static final int NO_OF_EMOTICONS = 54;
    private LinearLayout emoticonsCover;
    private PopupWindow popupWindow;
    private View popUpView;
    private int keyboardHeight;

    private RelativeLayout parentLayout;

    private boolean isKeyBoardVisible;

    private Bitmap[] emoticons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        userFileManager = Utils.getInstance().getUserFileManager();

        context = CommentActivity.this;
        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            uuid = intent.getStringExtra(AppConsatants.UUID);
            notificationIntent = intent.getBooleanExtra(AppConsatants.NOTIFICATION_NEXT_INTENT, false);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        bindActivity();
    }

    private void bindActivity() {

        Toolbar mToolbar = (Toolbar) findViewById(R.id.comment_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if(notificationIntent) {
//            getSupportActionBar().setTitle(TimelineNotificationActivityHandler.getNotificationActivityLog() != null?TimelineNotificationActivityHandler.getNotificationActivityLog().getMomentCaption():getString(R.string.app_name));
        }else{
            getSupportActionBar().setTitle(Utils.getInstance().getMomentObj().getCaption());
        }
        mCommentEditText = (EditText) findViewById(R.id.message_edit_text);
        mCommentSendButton = (ImageButton) findViewById(R.id.send_button);
        mRecyclerView = (ListView) findViewById(R.id.message_history_list_view);
        parentLayout = (RelativeLayout) findViewById(R.id.list_parent);
        emoticonsCover = (LinearLayout) findViewById(R.id.footer_for_emoticons);
        popUpView = getLayoutInflater().inflate(R.layout.emoticons_popup, null);

        // Defining default height of keyboard which is equal to 230 dip
        final float popUpheight = getResources().getDimension(
                R.dimen.keyboard_height);
        changeKeyboardHeight((int) popUpheight);

        // Showing and Dismissing pop up on clicking emoticons button
        ImageView emoticonsButton = (ImageView) findViewById(R.id.emoticons_button);
        emoticonsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (!popupWindow.isShowing()) {

                    popupWindow.setHeight((int) (keyboardHeight));

                    if (isKeyBoardVisible) {
                        emoticonsCover.setVisibility(LinearLayout.GONE);
                    } else {
                        emoticonsCover.setVisibility(LinearLayout.VISIBLE);
                    }
                    popupWindow.showAtLocation(parentLayout, Gravity.BOTTOM, 0, 0);

                } else {
                    popupWindow.dismiss();
                }

            }
        });
        readEmoticons();
        enablePopUpView();
        checkKeyboardHeight(parentLayout);
//        enableFooterView();
        commentList = new ArrayList<Comment>();

        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (popupWindow.isShowing())
                    popupWindow.dismiss();
                return false;
            }
        });
        mCommentEditText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (popupWindow.isShowing()) {

                    popupWindow.dismiss();

                }

            }
        });
        mCommentSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String comment = mCommentEditText.getText().toString();
                String comment =  Html.toHtml(mCommentEditText.getText()).trim();
                Comment commentItem = null;
                Moment moment = Utils.getInstance().getMomentObj();
                Profile profile = ModelManager.getInstance().getOwnProfile();
                if(!comment.isEmpty()){
                    commentItem = new Comment();
                    commentItem.setUuid(Utils.getInstance().generateUUIDString());
                    commentItem.setMomentUuid(uuid);
                    commentItem.setMomentOwner(moment.getOwner());
                    commentItem.setComment(comment);
                    commentItem.setDate((new Date()).getTime());
                    commentItem.setCommenter(profile.getUsername());
                    commentItem.setCommenterDisplayName(profile.getDisplayName());
                    commentItem.setCommenterThumbNail(profile.getThumbNail());
                    if(commentList == null)
                        commentList = new ArrayList<Comment>();
                    commentList.add(commentItem);
                    if(mAdapter == null){
                        initialiseRecyclerView();
                    }
                    mAdapter.notifyDataSetChanged();
                    sendCommentAdd(commentItem);
                }

            }
        });

        showProgressBar(getString(R.string.progressbar_fetching_info));
        getCommentList();
//        getVideoUrls();
    }

    /**
     * change height of emoticons keyboard according to height of actual
     * keyboard
     *
     * @param height
     *            minimum height by which we can make sure actual keyboard is
     *            open or not
     */
    private void changeKeyboardHeight(int height) {

        if (height > 100) {
            keyboardHeight = height;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, keyboardHeight);
            emoticonsCover.setLayoutParams(params);
        }

    }
    private void readEmoticons () {

        emoticons = new Bitmap[NO_OF_EMOTICONS];
        for (short i = 0; i < NO_OF_EMOTICONS; i++) {
            emoticons[i] = getImage((i+1) + ".png");
        }

    }
    /**
     * For loading smileys from assets
     */
    private Bitmap getImage(String path) {
        AssetManager mngr = getAssets();
        InputStream in = null;
        try {
            in = mngr.open("emoticons/" + path);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bitmap temp = BitmapFactory.decodeStream(in, null, null);
        return temp;
    }
    /**
     * Defining all components of emoticons keyboard
     */
    private void enablePopUpView() {

        ViewPager pager = (ViewPager) popUpView.findViewById(R.id.emoticons_pager);
        pager.setOffscreenPageLimit(3);

        ArrayList<String> paths = new ArrayList<String>();

        for (short i = 1; i <= NO_OF_EMOTICONS; i++) {
            paths.add(i + ".png");
        }

        EmoticonsPagerAdapter adapter = new EmoticonsPagerAdapter(CommentActivity.this, paths, this);
        pager.setAdapter(adapter);

        // Creating a pop window for emoticons keyboard
        popupWindow = new PopupWindow(popUpView, ViewGroup.LayoutParams.MATCH_PARENT,
                (int) keyboardHeight, false);

      /*  TextView backSpace = (TextView) popUpView.findViewById(R.id.back);
        backSpace.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                mMessageEditText.dispatchKeyEvent(event);
            }
        });
*/
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                emoticonsCover.setVisibility(LinearLayout.GONE);
            }
        });
    }
    /**
     * Checking keyboard height and keyboard visibility
     */
    int previousHeightDiffrence = 0;
    private void checkKeyboardHeight(final View parentLayout) {

        parentLayout.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {

                        Rect r = new Rect();
                        parentLayout.getWindowVisibleDisplayFrame(r);

                        int screenHeight = parentLayout.getRootView()
                                .getHeight();
                        int heightDifference = screenHeight - (r.bottom);

                        if (previousHeightDiffrence - heightDifference > 50) {
                            popupWindow.dismiss();
                        }

                        previousHeightDiffrence = heightDifference;
                        if (heightDifference > 100) {

                            isKeyBoardVisible = true;
                            changeKeyboardHeight(heightDifference);

                        } else {

                            isKeyBoardVisible = false;

                        }

                    }
                });

    }


    @TargetApi(Build.VERSION_CODES.M)
    private void initialiseRecyclerView(){

        mAdapter = new CommentsAdapter(CommentActivity.this, commentList);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setItemsCanFocus(false);
        mRecyclerView.setChoiceMode(ListView.CHOICE_MODE_NONE);
        mAdapter.notifyDataSetChanged();

    }


    @Override
    public void onDestroy() {
        if (userFileManager != null) {
            userFileManager.destroy();
        }
        super.onDestroy();
    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user_home, menu);
        return true;
    }

    private void selectVideo() {
//        sendMomentList();
        Intent intent = new Intent();
        intent.setType("video*//*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.home_profile_select_video)), GALLERY_INTENT_CALLED);

    }

    //New
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_timeline:
                Intent mIntent = new Intent(CommentActivity.this, TimelineTabs.class);
                startActivity(mIntent);
                finish();
                break;
            case R.id.action_setting:
                AWSMobileClient.defaultMobileClient().getIdentityManager().signOut();
                Intent mreg = new Intent(CommentActivity.this, RegisterActivity.class);
                startActivity(mreg);
                finish();
                break;
            case R.id.action_video:
//                AWSMobileClient.defaultMobileClient().getIdentityManager().signOut();
                Intent mIntentVideo = new Intent(CommentActivity.this, RecordVideo.class);
                startActivity(mIntentVideo);
                break;
            case R.id.action_search:
//                AWSMobileClient.defaultMobileClient().getIdentityManager().signOut();
                Intent mIntentSearch = new Intent(CommentActivity.this, SearchActivity.class);
                startActivity(mIntentSearch);
                break;
            default:
                break;
        }

        return true;
    }*/


    private void getProfileRequest(){
        try {
            final String username = DataStorage.getInstance().getUsername();
            String url =ServiceURLManager.getInstance().getProfileUrl(IAPIConstants.API_KEY_GET_PROFILE, username, username);

            JsonObjectRequest  request= new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    String getUsername = obj.getUsername();
                    if(getUsername != null && !getUsername.isEmpty() && username.equals(getUsername)) {
                        ModelManager.getInstance().setOwnProfile(obj);
//                        initialiseValues();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(ModelManager.getInstance().getOwnProfile() != null && mAdapter!= null){
            mAdapter.notifyDataSetChanged();
        }
    }

    private void getCommentList() {
        try {
            String url = ServiceURLManager.getInstance().getCommentList(IAPIConstants.API_KEY_GET_COMMENT_LIST, uuid, "0", "0");

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<Comment> obj = null;
                    try {
//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Comment>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    hideProgressBar();
                    if (obj != null) {

                        if (obj.size() > 0) {
                            commentList = obj;
                            initialiseRecyclerView();
                            mAdapter.notifyDataSetChanged();
                        }
                    }else{
                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendCommentAdd(Comment comment){
        try {

            if(comment== null)
                return;
            final String username =DataStorage.getInstance().getUsername();
            String url =ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_ADD_COMMENT);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(comment);
            } catch (IOException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Comment obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Comment.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if(obj!=null) {
                        String url = ServiceURLManager.getInstance().getMomentFeedsListUrl(IAPIConstants.API_KEY_GET_MOMENT_FEEDS_TIMELINE_LIST,username,username, AppConsatants.FALSE,"1", "10");
                        VolleySingleton.getInstance(CommentActivity.this).refresh(url,false);
                        mAdapter.notifyDataSetChanged();
                        mCommentEditText.setText("");
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    showError(getString(R.string.error_upload_failed), null);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void showError(final String message,final DialogInterface.OnClickListener okClicked) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(CommentActivity.this).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, okClicked)
                        .show();
            }
        });
    }


    private void showProgressBar(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = ProgressDialog.show(context, null, null, true, false);
                progressDialog.setContentView(R.layout.progressbar);
                TextView progressBarMessage = (TextView) progressDialog.findViewById(R.id.progressBar_message);
                progressBarMessage.setText(msg);
            }
        });

    }

    private void hideProgressBar() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                }
            });
        }catch(Exception e){

        }
    }

    @Override
    public void onBackPressed() {
        if (isKeyBoardVisible) {
            Utils.getInstance().hideKeyboard(this, mCommentEditText);
        } else if (popupWindow.isShowing()) {

            popupWindow.dismiss();

        }else if(notificationIntent) {
            Intent intent = new Intent(context, TimelineTabs.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }else{
            finish();
        }
        super.onBackPressed();
    }

    @Override
    public void keyClickedIndex(final String index) {
        Html.ImageGetter imageGetter = new Html.ImageGetter() {
            public Drawable getDrawable(String source) {
                StringTokenizer st = new StringTokenizer(index, ".");
                Drawable d = new BitmapDrawable(getResources(),emoticons[Integer.parseInt(st.nextToken()) - 1]);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                return d;
            }
        };

        Spanned cs = Html.fromHtml("<img src ='"+ index +"'/>", imageGetter, null);

        int cursorPosition = mCommentEditText.getSelectionStart();
        mCommentEditText.getText().insert(cursorPosition, cs);
    }
}
