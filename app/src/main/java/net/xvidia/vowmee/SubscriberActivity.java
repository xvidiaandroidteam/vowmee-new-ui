package net.xvidia.vowmee;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.opentok.android.AudioDeviceManager;
import com.opentok.android.BaseAudioDevice;
import com.opentok.android.BaseVideoRenderer;
import com.opentok.android.Connection;
import com.opentok.android.OpentokError;
import com.opentok.android.Session;
import com.opentok.android.Stream;
import com.opentok.android.Subscriber;
import com.opentok.android.SubscriberKit;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.broadcastreceiver.NetworkStateReceiver;
import net.xvidia.vowmee.fragments.AudioLevelView;
import net.xvidia.vowmee.fragments.MeterView;
import net.xvidia.vowmee.fragments.SubscriberQualityFragment;
import net.xvidia.vowmee.gcm.NotifBuilderSingleton;
import net.xvidia.vowmee.helper.CustomFontTextView;
import net.xvidia.vowmee.helper.OnSwipeTouchListener;
import net.xvidia.vowmee.listadapter.ChatAdapterNew;
import net.xvidia.vowmee.listadapter.EmoticonsGridAdapter;
import net.xvidia.vowmee.listadapter.ViewersListAdapter;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.ChatMessage;
import net.xvidia.vowmee.network.model.LiveMomentViewerDetails;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Opentok;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;


public class SubscriberActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener,SubscriberKit.VideoListener,
        EmoticonsGridAdapter.KeyClickListener, Session.SessionListener,
        SubscriberKit.SubscriberListener, Session.SignalListener, View.OnClickListener {

    private static final String LOG_TAG = SubscriberActivity.class.getSimpleName();
    public static final String SIGNAL_TYPE_CHAT = "chat";
    public static final String SIGNAL_TYPE_IMAGE = "image";
    public static final String SIGNAL_TYPE_CLOSE = "close";

    private String mApiKey;
    private String mStatus;
    public static boolean closeSubscriber;
    public static String mSessionPublisherId;
    public static  String mSessionSubscriberId;
    private String mToken;
    private Session mSession;
    private Subscriber mSubscriber;
    private boolean mLiveChatOn;
    private FrameLayout mSubscriberViewContainer;
//    private ImageView imageView;
    private ProgressDialog pDialog;
    private ImageButton mSendButton;
    private EditText mMessageEditText;
    private Animation pulse;
    private RelativeLayout heartLayout;
    private RelativeLayout cancelSession;
    private boolean notificationIntent;
    private int notificationId, idInboxStyle;
    private RelativeLayout mSubscriberAudioOnlyView;

    private MeterView meterView;
    private RecyclerView mMessageHistoryListView;
    private ChatAdapterNew mMessageHistory;
    private List<ChatMessage> commentList;

//    private static final int NO_OF_EMOTICONS = 54;
//    private LinearLayout emoticonsCover;
//    private PopupWindow popupWindow;
//    private View popUpView;
//    private int keyboardHeight;
    private SubscriberQualityFragment mSubscriberQualityFragment;
    private FragmentTransaction mFragmentTransaction;
    private SubscriberQualityFragment.CongestionLevel congestion = SubscriberQualityFragment.CongestionLevel.Low;

    private AudioLevelView mAudioLevelView;
    private boolean mSubscriberAudioOnly = false;
    private Handler mHandler = new Handler();
    private RelativeLayout parentLayout;

    private boolean isKeyBoardVisible;

    private Bitmap[] emoticons;
    private ArrayList<Stream> mStreams;
    private TextView viewerCount;

    private Handler mHandlerRefreshCount;
    private Runnable mRunnableCounter;
    private boolean firstFlag = true;
    private String sessionClosed = null;
    private Boolean isClosedByPublisher = false;
    private TextView reconnect;
    private Boolean isFirstTimeConnecting = true;
    private NetworkStateReceiver networkStateReceiver;

    private RecyclerView mRecyclerView;
    private ViewersListAdapter mAdapter;
    private List<Profile> viewersList;
    private LinearLayout tintLayout, commentLayout;
    private Animation slideUp, slideDown;
    private RelativeLayout layout;
    public static int count, totalCount;
    private int deviceHeight;
    private int[] images = new int[]{R.drawable.kiss,R.drawable.heart1,R.drawable.heart2,R.drawable.love,R.drawable.heart3};
    int maximum = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.subscriber);
        if (savedInstanceState == null) {
            mFragmentTransaction = getFragmentManager().beginTransaction();
            initSubscriberQualityFragment();
            mFragmentTransaction.commitAllowingStateLoss();
        }
        closeSubscriber=false;
        mSubscriberAudioOnlyView = (RelativeLayout) findViewById(R.id.audioOnlyView);
        parentLayout = (RelativeLayout) findViewById(R.id.list_parent);
        commentLayout = (LinearLayout) findViewById(R.id.buttonLayout);
//        emoticonsCover = (LinearLayout) findViewById(R.id.footer_for_emoticons);
//        popUpView = getLayoutInflater().inflate(R.layout.emoticons_popup, null);
        viewerCount = (CustomFontTextView) findViewById(R.id.viewerCount);
        mAudioLevelView = (AudioLevelView) findViewById(R.id.subscribermeter);
        mMessageHistoryListView = (RecyclerView) findViewById(R.id.message_history_list_view);
        mAudioLevelView.setIcons(BitmapFactory.decodeResource(getResources(),R.drawable.headset));

        viewerCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initialiseRecyclerViewViewers();
                slideToTop(tintLayout);
//                tintLayout.setVisibility(View.VISIBLE);
                layout.startAnimation(slideUp);
                layout.setVisibility(View.VISIBLE);
            }
        });

        ///Viewers list
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        deviceHeight = displayMetrics.heightPixels;

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        params.height = deviceHeight/2;
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        layout = (RelativeLayout) findViewById(R.id.bottomLayout);
        layout.setVisibility(View.GONE);
        layout.setLayoutParams(params);
        slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        tintLayout = (LinearLayout) findViewById(R.id.tint_layout);
        tintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseBottomListView();
            }
        });
        tintLayout.setClickable(false);

        meterView = (MeterView) findViewById(R.id.volume);
        meterView.setMuted(false);
        meterView.setIcons(BitmapFactory.decodeResource(getResources(),
                R.drawable.unmute_sub), BitmapFactory.decodeResource(
                getResources(), R.drawable.mute_sub));
        meterView.setVisibility(View.GONE);
        mMessageHistoryListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                if (popupWindow.isShowing())
//                    popupWindow.dismiss();
                return false;
            }
        });
        // Defining default height of keyboard which is equal to 230 dip
//        final float popUpheight = getResources().getDimension(
//                R.dimen.keyboard_height);
//        changeKeyboardHeight((int) popUpheight);
//
//        // Showing and Dismissing pop up on clicking emoticons button
//        ImageView emoticonsButton = (ImageView) findViewById(R.id.emoticons_button);
//        emoticonsButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                if (!popupWindow.isShowing()) {
//
//                    popupWindow.setHeight((int) (keyboardHeight));
//
//                    if (isKeyBoardVisible) {
//                        emoticonsCover.setVisibility(LinearLayout.GONE);
//                    } else {
//                        emoticonsCover.setVisibility(LinearLayout.VISIBLE);
//                    }
//                    popupWindow.showAtLocation(parentLayout, Gravity.BOTTOM, 0, 0);
//
//                } else {
//                    popupWindow.dismiss();
//                }
//
//            }
//        });
//        readEmoticons();
//        enablePopUpView();
//        checkKeyboardHeight(parentLayout);
        enableFooterView();

        reconnect = (TextView) findViewById(R.id.reconnect);
        reconnect.setVisibility(View.GONE);
        cancelSession = (RelativeLayout) findViewById(R.id.cancelLiveSession);
//        cancelSession.setImageResource(R.drawable.cross_white);
        cancelSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeSubscriber = true;
                callSessionCancel();
            }
        });

        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            mSessionPublisherId = intent.getStringExtra(AppConsatants.LIVE_PUBLISHER_ID);
            notificationIntent = intent.getBooleanExtra(AppConsatants.NOTIFICATION_NEXT_INTENT, false);
            notificationId = intent.getIntExtra(AppConsatants.NOTIFICATION_ID_INTENT, 0);
            idInboxStyle = intent.getIntExtra(AppConsatants.IDINBOXSTYLE, 0);
        }

        if (mSessionPublisherId == null) {
            showMessage(getString(R.string.error_live_session_closed), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onBackPressed();
                }
            });
        } else if (mSessionPublisherId.isEmpty()) {
            showMessage(getString(R.string.error_live_session_closed), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onBackPressed();
//                    SubscriberActivity.this.finish();
                }
            });
        }
        heartLayout = (RelativeLayout) findViewById(R.id.heartLayout);
        pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);
        mSubscriberViewContainer = (FrameLayout) findViewById(R.id.subscriber_container);
//        mSubscriberViewContainer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sendImages();
//
//            }
//        });


        mSubscriberViewContainer.setOnTouchListener(new OnSwipeTouchListener(SubscriberActivity.this) {
            public void onSwipeTop() {
                initialiseRecyclerViewViewers();
                slideToTop(tintLayout);
                layout.startAnimation(slideUp);
                layout.setVisibility(View.VISIBLE);
            }
            public void onSwipeRight() {
//                enableMessageViews();
                slideToRight(commentLayout);
            }
            public void onSwipeLeft() {
//                disableMessageViews();
                slideToLeft(commentLayout);
            }
            public void onSwipeBottom() {
                CloseBottomListView();
            }
            public void onClick(){
                sendImages();
            }

        });
        // Attach data source to message history

        commentList = new ArrayList<ChatMessage>();
        // Attach handlers to UI


        pDialog = new ProgressDialog(this);
//        pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                onBackPressed();
//            }
//        });
        pDialog.setMessage("Connecting...Please wait.");
        pDialog.setCancelable(true);
        showDialog();
        mStreams = new ArrayList<Stream>();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSubscriber != null) {
                    if (mSubscriberAudioOnly) {
                        mSubscriber.getView().setVisibility(View.GONE);
                        setAudioOnlyView(true);
                        congestion = SubscriberQualityFragment.CongestionLevel.High;
                    }
                }
            }
        }, 0);
        viewersList = new ArrayList<>();
        if(notificationIntent) {
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancel(notificationId);
            NotifBuilderSingleton.getInstance().resetInboxStyle(idInboxStyle);
        }
    }

    private void initializeRecyclerView(){
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setStackFromEnd(true);
        mMessageHistoryListView.setLayoutManager(mLayoutManager);
        mMessageHistory = new ChatAdapterNew(this, commentList);
        mMessageHistoryListView.setAdapter(mMessageHistory);
        mMessageHistory.notifyDataSetChanged();
    }


//    private void changeKeyboardHeight(final int height) {
//        try {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    if (height > 100) {
//                        keyboardHeight = height;
//                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//                                ViewGroup.LayoutParams.MATCH_PARENT, keyboardHeight);
//                        emoticonsCover.setLayoutParams(params);
//                    }
//                }
//            });
//        } catch (WindowManager.BadTokenException e) {
//
//        } catch (Exception e) {
//
//        }
//    }


    private void initialiseRecyclerViewViewers() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
//        mLayoutManager.setStackFromEnd(f);
        mRecyclerView = (RecyclerView) findViewById(R.id.bottomlistview);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ViewersListAdapter(SubscriberActivity.this, viewersList,count,totalCount);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(false);
    }

    public void slideToBottom(View view) {
        tintLayout.setClickable(false);
        TranslateAnimation animate = new TranslateAnimation(0, 0, 0, view.getHeight());
        animate.setDuration(1000);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);

    }
    public void slideToLeft(View view) {
        view.setClickable(false);
        TranslateAnimation animate = new TranslateAnimation( view.getWidth(),0, 0, 0);
        animate.setDuration(600);
        animate.setFillAfter(true);
        view.startAnimation(animate);
//        view.setVisibility(View.GONE);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ABOVE,R.id.buttonLayout);
        mMessageHistoryListView.setLayoutParams(params);

    }
    public void slideToRight(View view) {
        view.setClickable(true);
        TranslateAnimation animate = new TranslateAnimation(0, view.getWidth(), 0, 0);
        animate.setDuration(600);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        mMessageHistoryListView.setLayoutParams(params);
//        view.setVisibility(View.GONE);

    }
    public void slideToTop(View view) {
        tintLayout.setClickable(true);
        TranslateAnimation animate = new TranslateAnimation(0, 0, view.getHeight(), 0);
        animate.setDuration(1000);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);

    }

    public void CloseBottomListView() {

        if (layout.getVisibility() == View.VISIBLE) {
            slideToBottom(tintLayout);
//        tintLayout.setVisibility(View.INVISIBLE);
            layout.startAnimation(slideDown);
            layout.setVisibility(View.INVISIBLE);
        }

    }

    private void setRefreshCount() {
        if (mHandlerRefreshCount == null)
            mHandlerRefreshCount = new Handler();
        if (mRunnableCounter == null)
            mRunnableCounter = new Runnable() {
                @Override
                public void run() {
                    getLiveCount();
                    setRefreshCount();
                }
            };

        mHandlerRefreshCount.postDelayed(mRunnableCounter, 15000);
    }

//    private void readEmoticons() {
//
//        emoticons = new Bitmap[NO_OF_EMOTICONS];
//        for (short i = 0; i < NO_OF_EMOTICONS; i++) {
//            emoticons[i] = getImage((i + 1) + ".png");
//        }
//
//    }

    /**
     * For loading smileys from assets
     */
    private Bitmap getImage(String path) {
        AssetManager mngr = getAssets();
        InputStream in = null;
        try {
            in = mngr.open("emoticons/" + path);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bitmap temp = BitmapFactory.decodeStream(in, null, null);
        return temp;
    }

    /**
     * Defining all components of emoticons keyboard
     */
//    private void enablePopUpView() {
//
//        ViewPager pager = (ViewPager) popUpView.findViewById(R.id.emoticons_pager);
//        pager.setOffscreenPageLimit(3);
//
//        ArrayList<String> paths = new ArrayList<String>();
//
//        for (short i = 1; i <= NO_OF_EMOTICONS; i++) {
//            paths.add(i + ".png");
//        }
//
//        EmoticonsPagerAdapter adapter = new EmoticonsPagerAdapter(SubscriberActivity.this, paths, this);
//        pager.setAdapter(adapter);
//
//        // Creating a pop window for emoticons keyboard
//        popupWindow = new PopupWindow(popUpView, ViewGroup.LayoutParams.MATCH_PARENT,
//                (int) keyboardHeight, false);
//
//      /*  TextView backSpace = (TextView) popUpView.findViewById(R.id.back);
//        backSpace.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
//                mMessageEditText.dispatchKeyEvent(event);
//            }
//        });
//*/
//        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
//
//            @Override
//            public void onDismiss() {
//                emoticonsCover.setVisibility(LinearLayout.GONE);
//            }
//        });
//    }

    /**
     * Checking keyboard height and keyboard visibility
     */
    int previousHeightDiffrence = 0;

/*
    private void checkKeyboardHeight(final View parentLayout) {

        parentLayout.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {

                        Rect r = new Rect();
                        parentLayout.getWindowVisibleDisplayFrame(r);

                        int screenHeight = parentLayout.getRootView()
                                .getHeight();
                        int heightDifference = screenHeight - (r.bottom);

                        if (previousHeightDiffrence - heightDifference > 50) {
                            popupWindow.dismiss();
                        }

                        previousHeightDiffrence = heightDifference;
                        if (heightDifference > 100) {

                            isKeyBoardVisible = true;
                            changeKeyboardHeight(heightDifference);

                        } else {

                            isKeyBoardVisible = false;

                        }

                    }
                });

    }
*/

    private void enableFooterView() {
        mMessageEditText = (EditText) findViewById(R.id.message_edit_text);

        mSendButton = (ImageButton) findViewById(R.id.send_button);
        mSendButton.setOnClickListener(this);
        mSendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

               /* if (content.getText().toString().length() > 0) {

                    Spanned sp = content.getText();
                    chats.add(sp);
                    content.setText("");
                    mAdapter.notifyDataSetChanged();

                }*/
                sendMessage();

            }
        });
    }

    private void sendMessage() {
        if (mSession == null)
            return;
        String inputText = Html.toHtml(mMessageEditText.getText()).trim();
//        Log.i("message", inputText);
        if (!inputText.matches("")) {
//            disableMessageViews();
            Profile obj = ModelManager.getInstance().getOwnProfile();
            if(obj!=null) {
                ChatMessage message = new ChatMessage();
                message.setMessageText(inputText);
                message.setOwner(obj.getUsername());
                message.setOwnerDisplayName(obj.getDisplayName());
                message.setOwnerThumbNail(obj.getThumbNail());
                ObjectMapper mapper = new ObjectMapper();
                String jsonObject = null;
                try {
                    jsonObject = mapper.writeValueAsString(message);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mSession.sendSignal(SIGNAL_TYPE_CHAT, jsonObject);
                mMessageEditText.setText("");
                Utils.getInstance().hideKeyboard(this,mMessageEditText);
//                enableMessageViews();
            }
        }

    }


    private void callSessionCancel() {
        if (mSession != null)
            mSession.disconnect();
        finish();
    }

    private void listViewOnClick(View v) {
        sendImages();
    }


//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (popupWindow.isShowing()) {
//            popupWindow.dismiss();
//            return false;
//        } else {
//            return super.onKeyDown(keyCode, event);
//        }
//    }

    @Override
    public void onBackPressed() {

        closeSubscriber = true;
        restartAudioMode();
        if (isKeyBoardVisible) {
            Utils.getInstance().hideKeyboard(this, mMessageEditText);
//        } else if (popupWindow.isShowing()) {
//            popupWindow.dismiss();

        } else {
            if (mSession != null) {
                hideDialog();
                mSession.disconnect();
                finish();
            } else {
                finish();
            }
        }
    }


    private void setViewerCount(){
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewerCount != null) {
                        viewerCount.setVisibility(View.VISIBLE);
                        viewerCount.setText(getString(R.string.live_count_message, count));
                    }
                }
            });
        }catch (WindowManager.BadTokenException e){

        }catch (Exception e){

        }

    }

    private void getLiveCount() {
        try {
            String url = ServiceURLManager.getInstance().getLiveCountUrl(mSessionPublisherId);

            //Log.i("Add moment", jsonObject);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideDialog();
                    ObjectMapper mapper = new ObjectMapper();
                    LiveMomentViewerDetails obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), LiveMomentViewerDetails.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        count = (int) obj.getActiveViewerCount();
                        totalCount = (int) obj.getTotalViewerCount();
                        viewersList = obj.getLastFewActiveViewers();
                        setViewerCount();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setGoogleAnalyticScreen("Subscribe Live");
        networkStateReceiver = new NetworkStateReceiver(this);
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }
    @Override
    protected void onPause() {
        closeSubscriber = true;
        if (mHandlerRefreshCount != null)
            mHandlerRefreshCount.removeCallbacks(mRunnableCounter);
        if (mSession != null) {
            hideDialog();
            mSession.disconnect();
        }
        if (notificationIntent) {
            Intent intent = new Intent(SubscriberActivity.this, TimelineTabs.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            finish();
        }
        if(networkStateReceiver!=null){
            networkStateReceiver.removeListener(this);
            this.unregisterReceiver(networkStateReceiver);
        }
        super.onPause();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        fetchSubscriberSessionConnectionData();
    }

    private void initializeSession() {
        mSession = new Session(this, mApiKey, mSessionSubscriberId);
        mSession.setSessionListener(this);
        mSession.setSignalListener(this);
        mSession.connect(mToken);
        ///TO set speaker as audi
        AudioDeviceManager.getAudioDevice().setOutputMode(
                BaseAudioDevice.OutputMode.SpeakerPhone);
    }

    private void logOpenTokError(OpentokError opentokError) {
//        Log.e(LOG_TAG, "Opentok Error: " + opentokError.getMessage());
//        Log.e(LOG_TAG, "Error Code: " + opentokError.getErrorCode().name());
    }


    private void sendImages() {
//        ChatMessage message = new ChatMessage("heart");
        if (mSession != null) {
            Profile obj = ModelManager.getInstance().getOwnProfile();
            if(obj!=null) {
                ChatMessage message = new ChatMessage();
                message.setMessageText("heart");
                message.setOwner(obj.getUsername());
                message.setOwnerDisplayName(obj.getDisplayName());
                message.setOwnerThumbNail(obj.getThumbNail());
                mSession.sendSignal(SIGNAL_TYPE_IMAGE, message.toString());
            }
        }
    }

    private void disableMessageViews() {
        mMessageEditText.setVisibility(View.GONE);
        mSendButton.setVisibility(View.GONE);
    }

    private void enableMessageViews() {
        mMessageEditText.setVisibility(View.VISIBLE);
        mSendButton.setVisibility(View.VISIBLE);
    }

    private void showMessage(String messageData, boolean remote) {
        ObjectMapper mapper = new ObjectMapper();
        ChatMessage message = null;
        try {
            message = mapper.readValue(messageData.toString(), ChatMessage.class);
        } catch (IOException e) {
        }catch (OutOfMemoryError e) {
        }catch (Exception e) {
        }
        if (commentList == null) {
            commentList = new ArrayList<>();
        }
//        if (commentList != null && commentList.size() > 4) {
//            commentList.remove(0);
//        }
        if(mMessageHistory == null){
            commentList.add(message);
            initializeRecyclerView();

        }else {
//            mMessageHistory.add(message);
            int pos =  ChatAdapterNew.chatList.size();
            ChatAdapterNew.chatList.add(message);
            mMessageHistory.notifyItemInserted(pos);
//            mMessageHistory.notifyDataSetChanged();
        }
//        }
    }

    public void restartAudioMode() {
        AudioManager Audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        Audio.setMode(AudioManager.MODE_NORMAL);
        this.setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
    }

    public void fetchSubscriberSessionConnectionData() {

        String subUrl = ServiceURLManager.getInstance().getLiveSubscriberTokenUrl(mSessionPublisherId, DataStorage.getInstance().getUserUUID());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, subUrl, "{}", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    Opentok obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Opentok.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        mSessionSubscriberId = obj.getSessionId();
                        mToken = obj.getToken();
                        mApiKey = obj.getApiKey();
                        mStatus = obj.getStatus();
                        mLiveChatOn = obj.isLiveChatOn();
                        if (mToken != null && !mToken.equalsIgnoreCase("null") && mStatus.equalsIgnoreCase(AppConsatants.STATUS_IS_LIVE)
                                && mSessionSubscriberId != null && !mSessionSubscriberId.equalsIgnoreCase("null")) {
                            initializeSession();
                            firstFlag = false;

                        } else {
                            showMessage(getString(R.string.error_live_session_closed), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    SubscriberActivity.this.finish();
                                }
                            });
                        }
                    }

//                    Log.i(LOG_TAG, mApiKey);
//                    Log.i(LOG_TAG, mSessionSubscriberId);
//                    Log.i(LOG_TAG, mToken);

                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                showMessage(getString(R.string.error_live_session_closed), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onBackPressed();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //add params <key,value>
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                // add headers <key,value>
                String credentials = "user" + ":" + "govind";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(),
                        Base64.NO_WRAP);
                headers.put("Authorization", auth);
                return headers;
            }

        };
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

        /* Session Listener methods */

    @Override
    public void onConnected(Session session) {
        if(reconnect.getVisibility() == View.VISIBLE)
            reconnect.setVisibility(View.GONE);
    }

    @Override
    public void onDisconnected(Session session) {
//        Log.i(LOG_TAG, "Session Disconnected");
        disableMessageViews();

//        showMessage("Session Disconnected", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                onBackPressed();
//            }
//        });
    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {

//        Log.i(LOG_TAG, "Stream Received");

        if (reconnect.getVisibility() == View.VISIBLE)
            reconnect.setVisibility(View.GONE);
        mStreams.add(stream);
        if (mSubscriber == null) {
            subscribeToStream(stream);
            meterView.setOnClickListener(new MeterView.OnClickListener() {
                @Override
                public void onClick(MeterView view) {
                    mSubscriber.setSubscribeToAudio(!view.isMuted());
                }
            });
            mSubscriber
                    .setAudioLevelListener(new SubscriberKit.AudioLevelListener() {
                        @Override
                        public void onAudioLevelUpdated(
                                SubscriberKit subscriber, float audioLevel) {
                            meterView.setMeterValue(audioLevel);
                        }
                    });
        }

        loadFragments();
        setRefreshCount();
    }

    private void subscribeToStream(Stream stream) {
        mSubscriber = new Subscriber(this, stream);
        mSubscriber.setSubscriberListener(this);
        mSubscriber.setVideoListener(this);
        mSubscriber.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                BaseVideoRenderer.STYLE_VIDEO_FILL);
        mSession.subscribe(mSubscriber);
        if (mSubscriber.getSubscribeToVideo()) {
            if(isFirstTimeConnecting) {
                showDialog();
                isFirstTimeConnecting = false;
            } else {
                reconnect.setVisibility(View.VISIBLE);
            }
        }
        mSubscriber.setSubscribeToAudio(true);
        mSubscriber.setSubscribeToVideo(true);
    }

    private void unsubscriberFromStream(Stream stream) {
        mStreams.remove(stream);
        if (reconnect.getVisibility() == View.GONE)
            reconnect.setVisibility(View.VISIBLE);
        if (isClosedByPublisher) {
            showMessage(sessionClosed, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onBackPressed();
                }
            });
        }
        if (mSubscriber != null
                && mSubscriber.getStream().getStreamId()
                .equals(stream.getStreamId())) {
            mSubscriberViewContainer.removeView(mSubscriber.getView());
            mSubscriber = null;
            findViewById(R.id.avatar).setVisibility(View.GONE);
            mSubscriberAudioOnly = false;
            if (!mStreams.isEmpty()) {
                subscribeToStream(mStreams.get(0));
            }
        }
    }

    @Override
    public void onStreamDropped(Session session, Stream stream) {
        unsubscriberFromStream(stream);

    }

    @Override
    public void onError(Session session, OpentokError opentokError) {
    }

    /* Subscriber Listener methods */

    @Override
    public void onConnected(SubscriberKit subscriberKit) {
        mSubscriber.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);
        enableMessageViews();

    }

    @Override
    public void onDisconnected(SubscriberKit subscriberKit) {
//        Utils.getInstance().displayToast(SubscriberActivity.this,getString(R.string.error_live_session_ended),Utils.SHORT_TOAST);
//        showMessage(getString(R.string.error_live_session_ended), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                onBackPressed();
//            }
//        });
    }

    @Override
    public void onError(SubscriberKit subscriberKit, OpentokError opentokError) {
    }

    @Override
    public void onClick(View v) {
        if (v.equals(mSendButton)) {
            sendMessage();
        }
    }

    @Override
    public void onSignalReceived(Session session, String type, String data, Connection connection) {
        if(!mLiveChatOn)
            return;
        boolean remote = !connection.equals(mSession.getConnection());
        switch (type) {
            case SIGNAL_TYPE_CHAT:
                showMessage(data, remote);
                break;
            case SIGNAL_TYPE_IMAGE:
                generateHearts();
                break;
            case SIGNAL_TYPE_CLOSE:
                sessionClosed = data;
                isClosedByPublisher = true;
// onDisconnected(session);
                break;
        }

    }



    private void showDialog() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (!pDialog.isShowing())
                        pDialog.show();
                }
            });
        } catch (IllegalArgumentException e) {

        } catch (Exception e) {

        }
    }

    private void hideDialog() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (pDialog.isShowing())
                        pDialog.dismiss();
                }
            });
        } catch (IllegalArgumentException e) {

        } catch (Exception e) {

        }
    }


    private void showMessage(final String mesg, final DialogInterface.OnClickListener okClicked) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new AlertDialog.Builder(SubscriberActivity.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .setCancelable(false)
                            .show();
                }
            });
        } catch (WindowManager.BadTokenException e) {
        } catch (Exception e) {
        }
    }

    @Override
    public void keyClickedIndex(final String index) {

        Html.ImageGetter imageGetter = new Html.ImageGetter() {
            public Drawable getDrawable(String source) {
                StringTokenizer st = new StringTokenizer(index, ".");
                Drawable d = new BitmapDrawable(getResources(), emoticons[Integer.parseInt(st.nextToken()) - 1]);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                return d;
            }
        };

        Spanned cs = Html.fromHtml("<img src ='" + index + "'/>", imageGetter, null);

        int cursorPosition = mMessageEditText.getSelectionStart();
        mMessageEditText.getText().insert(cursorPosition, cs);

    }

    public void initSubscriberQualityFragment() {
        mSubscriberQualityFragment = new SubscriberQualityFragment();
        getFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_sub_quality_container,
                        mSubscriberQualityFragment).commit();
    }


    public void loadFragments() {
        // show subscriber status
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSubscriber != null) {
//                    mSubscriberFragment.showSubscriberWidget(true);
//                    mSubscriberFragment.initSubscriberUI();

                    if (congestion != SubscriberQualityFragment.CongestionLevel.Low &&mSubscriberQualityFragment!=null) {
                        mSubscriberQualityFragment.setCongestion(congestion);
                        mSubscriberQualityFragment.showSubscriberWidget(true);
                    }
                }
            }
        }, 0);

    }

    private void setAudioOnlyView(boolean audioOnlyEnabled) {
        mSubscriberAudioOnly = audioOnlyEnabled;

        if (audioOnlyEnabled) {
            mSubscriber.getView().setVisibility(View.GONE);
            mSubscriberAudioOnlyView.setVisibility(View.VISIBLE);
//            mSubscriberAudioOnlyView.setOnClickListener(onViewClick);
//
//            // Audio only text for subscriber
//            TextView subStatusText = (TextView) findViewById(R.id.subscriberName);
//            subStatusText.setText(R.string.audioOnly);
//            AlphaAnimation aa = new AlphaAnimation(1.0f, 0.0f);
//            aa.setDuration(ANIMATION_DURATION);
//            subStatusText.startAnimation(aa);


            mSubscriber
                    .setAudioLevelListener(new SubscriberKit.AudioLevelListener() {
                        @Override
                        public void onAudioLevelUpdated(
                                SubscriberKit subscriber, float audioLevel) {
                            mAudioLevelView.setMeterValue(audioLevel);
                        }
                    });
        } else {
            mSubscriber.getView().setVisibility(View.VISIBLE);
            mSubscriberAudioOnlyView.setVisibility(View.GONE);

            mSubscriber.setAudioLevelListener(null);
            attachSubscriberView(mSubscriber);
        }
    }

    private void attachSubscriberView(Subscriber subscriber) {
        if (subscriber != null) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
            mSubscriberViewContainer.removeView(mSubscriber.getView());
            mSubscriberViewContainer.addView(subscriber.getView(), layoutParams);
            subscriber.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                    BaseVideoRenderer.STYLE_VIDEO_FILL);
        }
    }

    @Override
    public void onVideoDataReceived(SubscriberKit subscriberKit) {

        hideDialog();
        if(reconnect.getVisibility() == View.VISIBLE)
            reconnect.setVisibility(View.GONE);
        if (mSubscriber != null)
            attachSubscriberView(mSubscriber);
    }

    @Override
    public void onVideoDisabled(SubscriberKit subscriber, String reason) {
        if (mSubscriber == subscriber) {
            setAudioOnlyView(true);
        }

        if (reason.equals("quality") && mSubscriberQualityFragment!=null) {
            mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.High);
            congestion = SubscriberQualityFragment.CongestionLevel.High;
            setSubQualityMargins();
            mSubscriberQualityFragment.showSubscriberWidget(true);
        }
    }

    @Override
    public void onVideoEnabled(SubscriberKit subscriber, String reason) {
        if (mSubscriber == subscriber) {
            setAudioOnlyView(false);
        }
        if (reason.equals("quality")&& mSubscriberQualityFragment!=null) {
            mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.Low);
            congestion = SubscriberQualityFragment.CongestionLevel.Low;
            mSubscriberQualityFragment.showSubscriberWidget(false);
        }
    }

    @Override
    public void onVideoDisableWarning(SubscriberKit subscriberKit) {
        if(mSubscriberQualityFragment!=null) {
            mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.Mid);
            congestion = SubscriberQualityFragment.CongestionLevel.Mid;
            setSubQualityMargins();
            mSubscriberQualityFragment.showSubscriberWidget(true);
        }
    }

    @Override
    public void onVideoDisableWarningLifted(SubscriberKit subscriberKit) {
        if(mSubscriberQualityFragment!=null) {
            mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.Low);
            congestion = SubscriberQualityFragment.CongestionLevel.Low;
            mSubscriberQualityFragment.showSubscriberWidget(false);
        }
    }

    public void setSubQualityMargins() {
        RelativeLayout subQualityContainer = mSubscriberQualityFragment.getSubQualityContainer();
        if (subQualityContainer != null) {
            RelativeLayout.LayoutParams subQualityLayoutParams = (RelativeLayout.LayoutParams) subQualityContainer.getLayoutParams();
            RelativeLayout.LayoutParams audioMeterLayoutParams = (RelativeLayout.LayoutParams) mAudioLevelView.getLayoutParams();

            int bottomMargin = 0;

            // control pub fragment
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {

                if (bottomMargin == 0) {
                    bottomMargin = Utils.getInstance().convertDpToPixel(10, this);
                }
                subQualityLayoutParams.rightMargin = Utils.getInstance().convertDpToPixel(10, this);
            }

            subQualityLayoutParams.bottomMargin = bottomMargin;

            mSubscriberQualityFragment.getSubQualityContainer().setLayoutParams(
                    subQualityLayoutParams);
        }

    }

    @Override
    public void onNetworkAvailable() {
        if (!firstFlag) {

            if(mSubscriber!=null) {
                if (mSubscriberViewContainer != null)
                    mSubscriberViewContainer.removeView(mSubscriber.getView());
                if (mSession != null)
                    mSession.disconnect();
                if (mSubscriber != null)
                    mSubscriber.destroy();
                mSession = null;
                mSubscriber = null;
            }
            initializeSession();
        }
    }

    @Override
    public void onNetworkUnavailable() {
// showDialog();
        reconnect.setVisibility(View.VISIBLE);
    }

    private void setGoogleAnalyticScreen(String screenName){
        MyApplication application = (MyApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.setClientId(DataStorage.getInstance().getUserUUID());
        mTracker.setAppVersion(""+Utils.getInstance().getAppVersion(MyApplication.getAppContext()));
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }



    private void generateHearts(){
        try {
//    imageView.setGravity(Gravity.BOTTOM);
            final ImageView imageHearts = new ImageView(this);
            Animation pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);
            Random ran = new Random();
            int x = ran.nextInt(maximum);

            imageHearts.setImageResource(images[x]);
//            imageHearts.setImageResource(R.drawable.heart_selected);
            imageHearts.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT|RelativeLayout.ALIGN_PARENT_BOTTOM);
//            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.setMargins(0, 0, 10, 0);
            imageHearts.setLayoutParams(params);
            imageHearts.startAnimation(pulse);
            pulse.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
//            imageView.removeView(imageHearts);
                    imageHearts.clearAnimation();
                    imageHearts.setVisibility(View.GONE);
//                messageLayout.removeView(imageHearts);imageHeart

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            heartLayout.addView(imageHearts);
        }catch (WindowManager.BadTokenException e){

        }catch (Exception e){

        }
    }

}
