package net.xvidia.vowmee.gcm;

import net.xvidia.vowmee.network.model.ActivityLog;

/**
 * A notification listener.
 */
public interface INotificationListener {
    public void onNotificationReceived(ActivityLog activityLog);
}