package net.xvidia.vowmee.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v7.app.NotificationCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.gcm.GcmListenerService;

import net.xvidia.vowmee.GroupListActivity;
import net.xvidia.vowmee.PendingRequestActivity;
import net.xvidia.vowmee.PersonActivity;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.ReceiveVideoCallActivity;
import net.xvidia.vowmee.SubscriberActivity;
import net.xvidia.vowmee.TimelineTabs;
import net.xvidia.vowmee.Utils.ActivityTypes;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.network.model.ActivityLog;
import net.xvidia.vowmee.storage.DataStorage;

import java.io.IOException;

/**
 * Created by Ravi_office on 14-Jan-16.
 */
public class GcmNotificationReceiveService extends GcmListenerService {
    private NotificationManager mNotificationManager;
    Builder mBuilder;
//    int NOTIFICATION_ID = 111;
    private int idLike_comment_post = 111112;
    private int idFollowPerson = 111113;
    private int idGroupMember_add_delete = 111114;
    private int idGroup_edit_delete = 111115;
    private int idShare = 111116;
    private int idAcceptedRequest = 111117;
    private int idMissedCall = 111118;
    private int idInboxStyle = 001;

    public GcmNotificationReceiveService() {
        super();
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);
        sendNotification(data.getString("message"));
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @Override
    public void onMessageSent(String msgId) {
        super.onMessageSent(msgId);
    }

    @Override
    public void onSendError(String msgId, String error) {
        super.onSendError(msgId, error);
    }

    private void sendNotification(String msg) {
//        Log.d("NOtification", "Preparing to send notification...: " + msg);

        ObjectMapper mapper = new ObjectMapper();
        ActivityLog obj = null;

        try {
            obj = mapper.readValue(msg, ActivityLog.class);

        } catch (IOException e) {
        }catch (OutOfMemoryError e) {
        }catch (Exception e) {
        }
        String activityMessage = ActivityTypes.getActivityMessageString(obj);

        int iUniqueId = (int) (System.currentTimeMillis() & 0xfffffff);
//        TimelineNotificationActivityHandler.setNotificationActivityLog(obj);

        if (activityMessage.isEmpty())
            return;
        if (obj != null && obj.getActivityType() != null) {

            if (obj.getActivityType().equalsIgnoreCase(ActivityTypes.CALL_INITIATED)) {
                boolean showMissedCall= false;
                if(!Utils.getInstance().getCancelNotification(obj.getDate())) {
                    if (!DataStorage.getInstance().getCallProgress()) {
                        openReceiveCallActivity(obj);
                    }else{
                        showMissedCall=true;
                    }
                }else{
                    showMissedCall=true;
                }
                if(showMissedCall){
                    int color = getResources().getColor(R.color.appcolor);
                    generateNotification(obj,activityMessage,color,iUniqueId);
                }
            } else if (DataStorage.getInstance().getNotificationEnabled()) {
//                activityMessage = activityMessage.replace("@", "");
//                activityMessage = activityMessage.replace("[", "");
                int color = getResources().getColor(R.color.appcolor);
                if (activityMessage.contains(" has send you a friend request")) {
                    int NOTIFICATION_ID = DataStorage.getInstance().getUniqueNotificationId();
                    if (NOTIFICATION_ID > 2000) {
                        NOTIFICATION_ID = 1000;
                    }
                    Intent mAcceptIntent = new Intent(this, PendingRequestActivity.class);
                    mAcceptIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle mAcceptBundle = new Bundle();
                    mAcceptBundle.putString(AppConsatants.NOTIFICATION_FRIEND_USERNAME, obj.getInitiaterId());
                    mAcceptBundle.putInt(AppConsatants.NOTIFICATION_ID_INTENT, NOTIFICATION_ID);
                    mAcceptBundle.putBoolean(AppConsatants.NOTIFICATION_ACCEPT_INTENT, true);
                    mAcceptBundle.putBoolean(AppConsatants.NOTIFICATION_DECLINE_INTENT, false);
                    mAcceptBundle.putBoolean(AppConsatants.GROUP_ROLE_MODERATOR, false);
                    mAcceptBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, true);
                    mAcceptIntent.putExtras(mAcceptBundle);

                    PendingIntent contentAcceptIntent = PendingIntent.getActivity(this, (iUniqueId + 100), mAcceptIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
                    Intent mIntentDecline = new Intent(this, PendingRequestActivity.class);
                    mIntentDecline.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle mBundleDecline = new Bundle();
                    mBundleDecline.putString(AppConsatants.NOTIFICATION_FRIEND_USERNAME, obj.getInitiaterId());
                    mBundleDecline.putBoolean(AppConsatants.NOTIFICATION_ACCEPT_INTENT, false);
                    mBundleDecline.putBoolean(AppConsatants.NOTIFICATION_DECLINE_INTENT, true);
                    mBundleDecline.putInt(AppConsatants.NOTIFICATION_ID_INTENT, NOTIFICATION_ID);
                    mBundleDecline.putBoolean(AppConsatants.GROUP_ROLE_MODERATOR, false);
                    mBundleDecline.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, true);
                    mIntentDecline.putExtras(mBundleDecline);
                    PendingIntent contentDeclineIntent = PendingIntent.getActivity(this, (iUniqueId + 1000), mIntentDecline, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

                    mBuilder = new Builder(
                            this)
                            .setContentTitle(getString(R.string.app_name))
                            .setWhen(System.currentTimeMillis())
                            .addAction(R.drawable.accept, getString(R.string.action_accept_friend), contentAcceptIntent)
                            .setAutoCancel(true)
                            .addAction(R.drawable.cancel, getString(R.string.action_decline_friend), contentDeclineIntent)
                            .setPriority(Notification.PRIORITY_MAX);
//                            .setStyle(new NotificationCompat.BigTextStyle().bigText(activityMessage))
//                            .setContentText(activityMessage);

                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        mBuilder.setSmallIcon(R.drawable.notification_logo);
                        mBuilder.setColor(color);
                    }else{
                        mBuilder.setSmallIcon(R.drawable.main_logo);
                    }

                    Intent mIntent = new Intent(this, TimelineTabs.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle mBundle = new Bundle();
                    mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, true);
                    mBundle.putInt(AppConsatants.NOTIFICATION_ID_INTENT, NOTIFICATION_ID);
                    mBundle.putInt(AppConsatants.IDINBOXSTYLE, idInboxStyle);
                    mIntent.putExtras(mBundle);
                    PendingIntent contentIntent = PendingIntent.getActivity(this, iUniqueId, mIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

//                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        mBuilder.setColor(color);
//                    }

                    String ringtoneUriString = DataStorage.getInstance().getNotificationPath();
                    Uri currentTone;
                    if (ringtoneUriString.isEmpty()) {
                        currentTone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    } else {
                        currentTone = Uri.parse(ringtoneUriString);
                    }
                    mBuilder.setSound(currentTone);
                    mBuilder.setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL);
                    mBuilder.setContentIntent(contentIntent);

                    SpannableStringBuilder sb = setStyle(activityMessage);
                    NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
                    inboxStyle.addLine(sb);
                    mBuilder.setStyle(inboxStyle);
                    mBuilder.setContentText(sb);

                    mNotificationManager = (NotificationManager) this
                            .getSystemService(Context.NOTIFICATION_SERVICE);

                    mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
                    NOTIFICATION_ID = NOTIFICATION_ID + 1;
                    DataStorage.getInstance().setUniqueNotificationId(NOTIFICATION_ID);
                } else {
                    generateNotification(obj,activityMessage,color,iUniqueId);
                }
            }
        }
    }

    private void generateNotification(ActivityLog obj,String activityMessage, int color,int iUniqueId){
        if(obj == null)
            return;
            int newNotificationID =0;
            Intent mIntent = null;
            Bundle mBundle = new Bundle();
            if (obj.getActivityType().equalsIgnoreCase(ActivityTypes.IS_FOLLOWING)) {
                newNotificationID = idFollowPerson;
                idInboxStyle = 002;
                mIntent = new Intent(this, PersonActivity.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mBundle.putBoolean(AppConsatants.PERSON_FRIEND, false);
                mBundle.putBoolean(AppConsatants.PERSON_FOLLOWER, false);
                mBundle.putBoolean(AppConsatants.PERSON_FOLLOWING, true);
                mBundle.putBoolean(AppConsatants.OTHER_PERSON_PROFILE, false);
                mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, true);
                mBundle.putInt(AppConsatants.NOTIFICATION_ID_INTENT, newNotificationID);
                mBundle.putInt(AppConsatants.IDINBOXSTYLE, idInboxStyle);
            } else if (obj.getActivityType().equalsIgnoreCase(ActivityTypes.LIKE_NOTIFICATION) ||
                    obj.getActivityType().equalsIgnoreCase(ActivityTypes.COMMENT_NOTIFICATION) ||
                    obj.getActivityType().equalsIgnoreCase(ActivityTypes.POST_NOTIFICATION)) {
                newNotificationID = idLike_comment_post;
                idInboxStyle = 003;
                mIntent = new Intent(this, TimelineTabs.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
                mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, true);
                mBundle.putInt(AppConsatants.NOTIFICATION_ID_INTENT, newNotificationID);
                mBundle.putInt(AppConsatants.IDINBOXSTYLE, idInboxStyle);
            } else if (obj.getActivityType().equalsIgnoreCase(ActivityTypes.DELETE_GROUP) ||
                    obj.getActivityType().equalsIgnoreCase(ActivityTypes.EDIT_GROUP)) {
                newNotificationID = idGroup_edit_delete;
                idInboxStyle = 004;
                mIntent = new Intent(this, GroupListActivity.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, true);
                mBundle.putInt(AppConsatants.NOTIFICATION_ID_INTENT, newNotificationID);
                mBundle.putInt(AppConsatants.IDINBOXSTYLE, idInboxStyle);
                mIntent.putExtras(mBundle);
            } else if (obj.getActivityType().equalsIgnoreCase(ActivityTypes.ADD_GROUP_MEMBER) ||
                    obj.getActivityType().equalsIgnoreCase(ActivityTypes.DELETE_GROUP_MEMBER)) {
                newNotificationID = idGroupMember_add_delete;
                idInboxStyle = 005;
                mIntent = new Intent(this, GroupListActivity.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, true);
                mBundle.putInt(AppConsatants.NOTIFICATION_ID_INTENT, newNotificationID);
                mBundle.putInt(AppConsatants.IDINBOXSTYLE, idInboxStyle);
            } else if (obj.getActivityType().equalsIgnoreCase(ActivityTypes.SHARE_NOTIFICATION)) {
                newNotificationID = idShare;
                idInboxStyle = 006;
                mIntent = new Intent(this, TimelineTabs.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, true);
                mBundle.putInt(AppConsatants.NOTIFICATION_ID_INTENT, newNotificationID);
                mBundle.putInt(AppConsatants.IDINBOXSTYLE, idInboxStyle);
                mIntent.putExtras(mBundle);
            } else if (obj.getActivityType().equalsIgnoreCase(ActivityTypes.ACCEPT_FRIEND_REQUEST)) {
                newNotificationID = idAcceptedRequest;
                idInboxStyle = 007;
                mIntent = new Intent(this, PersonActivity.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mBundle.putBoolean(AppConsatants.PERSON_FRIEND, true);
                mBundle.putBoolean(AppConsatants.PERSON_FOLLOWER, false);
                mBundle.putBoolean(AppConsatants.PERSON_FOLLOWING, false);
                mBundle.putBoolean(AppConsatants.OTHER_PERSON_PROFILE, false);
                mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, true);
                mBundle.putInt(AppConsatants.NOTIFICATION_ID_INTENT, newNotificationID);
                mBundle.putInt(AppConsatants.IDINBOXSTYLE, idInboxStyle);
            }else if (obj.getActivityType().equalsIgnoreCase(ActivityTypes.LIVE_POST_NOTIFICATION)) {
                int NOTIFICATION_ID = DataStorage.getInstance().getUniqueNotificationId();
                if (NOTIFICATION_ID > 1000) {
                    NOTIFICATION_ID = 0;
                }
                newNotificationID = NOTIFICATION_ID;
                NOTIFICATION_ID = NOTIFICATION_ID + 1;
                DataStorage.getInstance().setUniqueNotificationId(NOTIFICATION_ID);
                mIntent = new Intent(this, SubscriberActivity.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mBundle.putString(AppConsatants.LIVE_PUBLISHER_ID, (obj== null?"":obj.getMomentSessionId()));
                mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, true);
                mBundle.putInt(AppConsatants.NOTIFICATION_ID_INTENT, newNotificationID);
                mBundle.putInt(AppConsatants.IDINBOXSTYLE, idInboxStyle);
                mIntent.putExtras(mBundle);
            }else if (obj.getActivityType().equalsIgnoreCase(ActivityTypes.CALL_INITIATED)) {
                newNotificationID = idMissedCall;
                activityMessage = "Missed call from "+obj.getInitiaterName() ;
                idInboxStyle = 8;
                mIntent = new Intent(this, PersonActivity.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mBundle.putBoolean(AppConsatants.PERSON_FRIEND, true);
                mBundle.putBoolean(AppConsatants.PERSON_FOLLOWER, false);
                mBundle.putBoolean(AppConsatants.PERSON_FOLLOWING, false);
                mBundle.putBoolean(AppConsatants.OTHER_PERSON_PROFILE, false);
                mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, true);
                mBundle.putInt(AppConsatants.NOTIFICATION_ID_INTENT, newNotificationID);
                mBundle.putInt(AppConsatants.IDINBOXSTYLE, idInboxStyle);
            }

            if(mIntent==null){
                mIntent = new Intent(this, TimelineTabs.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);

                mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, true);
                mBundle.putInt(AppConsatants.NOTIFICATION_ID_INTENT, newNotificationID);
                mBundle.putInt(AppConsatants.IDINBOXSTYLE, idInboxStyle);
            }
            mIntent.putExtras(mBundle);
            PendingIntent contentIntent = PendingIntent.getActivity(this, iUniqueId, mIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);


            mBuilder = NotifBuilderSingleton.getInstance().getmBuilder();

            if (mBuilder != null) {
                if(obj !=null && obj.getActivityType().equalsIgnoreCase(ActivityTypes.LIVE_POST_NOTIFICATION)){
//                            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(activityMessage));
//                            mBuilder.setContentText(activityMessage);
                    mBuilder.setNumber(0);
                    SpannableStringBuilder sb = setStyle(activityMessage);
                    NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
                    inboxStyle.addLine(sb);
                    mBuilder.setStyle(inboxStyle);
                    mBuilder.setContentText(sb);
                }else {
                    NotifBuilderSingleton.getInstance().setMessage(activityMessage, idInboxStyle);
                }

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mBuilder.setSmallIcon(R.drawable.notification_logo);
                    mBuilder.setColor(color);
                }else{
                    mBuilder.setSmallIcon(R.drawable.main_logo);
                }

                String ringtoneUriString = DataStorage.getInstance().getNotificationPath();
                Uri currentTone;
                if (ringtoneUriString.isEmpty()) {
                    currentTone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                } else {
                    currentTone = Uri.parse(ringtoneUriString);
                }
                mBuilder.setSound(currentTone);
                mBuilder.setWhen(System.currentTimeMillis());
                mBuilder.setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS);
                mBuilder.setContentIntent(contentIntent);

                mNotificationManager = (NotificationManager) this
                        .getSystemService(Context.NOTIFICATION_SERVICE);

                mNotificationManager.notify(newNotificationID, mBuilder.build());
            }

    }
    private void openReceiveCallActivity(ActivityLog activityLog) {
        if (activityLog == null)
            return;
        String profImagePath = "";
        profImagePath = activityLog.getInitiatorThumbNail();
        profImagePath = (profImagePath == null ? activityLog.getInitiaterId()+ AppConsatants.FILE_EXTENSION_JPG :(profImagePath.isEmpty()? activityLog.getInitiaterId()+ AppConsatants.FILE_EXTENSION_JPG:profImagePath));

        Intent mIntent = new Intent(this, ReceiveVideoCallActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle mBundle = new Bundle();
        mBundle.putString(AppConsatants.LIVE_PUBLISHER_ID, activityLog.getCallSessionId());
        mBundle.putString(AppConsatants.USER_THUMBNAIL, profImagePath);
        mBundle.putString(AppConsatants.NOTIFICATION_FRIEND_USERNAME, activityLog.getInitiaterId());
        mBundle.putString(AppConsatants.PERSON_FRIEND, activityLog.getInitiaterName());
        mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, true);
        mIntent.putExtras(mBundle);
        startActivity(mIntent);
    }

    private SpannableStringBuilder setStyle(String activityMessage){
        String string = activityMessage;
        String[] parts = string.split("@", 3);
        String part1 = parts[0]; // 004
        part1 = part1.replace("@","");
        part1 = part1.replace("[","");
        String part2 = parts[1]; // 034556-42
        part2 = part2.replace("@","");
        part2 = part2.replace("[","");
        String part3 = parts[2];
        part3 = part3.replace("@","");
        part3 = part3.replace("[","");

        SpannableStringBuilder sb = new SpannableStringBuilder(part1 + part2 + part3);

        StyleSpan b = new StyleSpan(android.graphics.Typeface.BOLD);

        sb.setSpan(b, 0, + part2.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return sb;
    }
}
