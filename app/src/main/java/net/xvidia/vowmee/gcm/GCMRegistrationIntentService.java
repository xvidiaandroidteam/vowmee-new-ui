package net.xvidia.vowmee.gcm;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.IOException;

public class GCMRegistrationIntentService extends IntentService {

	private static final String TAG = "RegIntentService";
	private static final String[] TOPICS = {"global"};

	public GCMRegistrationIntentService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		try {
			// [START register_for_gcm]
			// Initially this call goes out to the network to retrieve the token, subsequent calls
			// are local.
			// R.string.gcm_defaultSenderId (the Sender ID) is typically derived from google-services.json.
			// See https://developers.google.com/cloud-messaging/android/start for details on this file.
			// [START get_token]
			InstanceID instanceID = InstanceID.getInstance(this);
			String token = instanceID.getToken(getString(R.string.gcm_app_Id),GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
			// [END get_token]
			Log.i(TAG, "GCM Registration Token: " + token);

			// TODO: Implement this method to send any registration to your app's servers.
			if(!DataStorage.getInstance().getUsername().isEmpty())
			sendRegistrationToServer(this,token);

			// Subscribe to topic channels
//			subscribeTopics(token);

			// You should store a boolean that indicates whether the generated token has been
			// sent to your server. If the boolean is false, send the token to your server,
			// otherwise your server should have already received the token.
			DataStorage.getInstance().storeRegistrationId(this,token);
			// [END register_for_gcm]
		} catch (Exception e) {
			Log.d(TAG, "Failed to complete token refresh", e);
			// If an exception happens while fetching the new token or updating our registration data
			// on a third-party server, this ensures that we'll attempt the update at a later time.

			DataStorage.getInstance().storeRegistrationIdOnServer(this, false);
		}
		// Notify UI that registration has completed, so the progress indicator can be hidden.
//		Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
//		LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
	}

	/**
	 * Persist registration to third-party servers.
	 *
	 * Modify this method to associate the user's GCM registration token with any server-side account
	 * maintained by your application.
	 *
	 * @param token The new token.
	 */
	private void sendRegistrationToServer(final Context context,final String token) {
		if(token== null|| context == null)
			return;
		String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_UPDATE_GCM);
		Profile profile = new Profile();
		profile.setUsername(DataStorage.getInstance().getUsername());
		profile.setGcmId(token);
		ObjectMapper mapper = new ObjectMapper();
		String jsonObject = null;
		try {
			jsonObject = mapper.writeValueAsString(profile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Log.d(TAG, "GCM ToServer: " + profile.toString());
		JsonObjectRequest ObjReq = new JsonObjectRequest(Request.Method.POST,url,jsonObject,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {

				Log.d(TAG, "Response: " + response.toString());

				if(response.toString().isEmpty() || response.toString().equals(null)){
//					Toast.makeText(context, "Register error. Try again.", Toast.LENGTH_LONG);
				}else {

//					Toast.makeText(context, "Successfully registered with server", Toast.LENGTH_LONG).show();

					try {
					DataStorage.getInstance().storeRegistrationIdOnServer(context,true);
					} catch (Exception e) {
						// JSON error
//						e.printStackTrace();
//						Toast.makeText(context, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
					}
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
//				Log.e(TAG, "Login Error: " + error !=null? error.getMessage():"");
//				Toast.makeText(context,
//						"Error! Check your internet connection.", Toast.LENGTH_LONG).show();

			}
		});

		// Adding request to request queue
		VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(ObjReq);
//		DataStorage.getInstance().storeRegistrationIdOnServer(this, true);
	}

	/**
	 * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
	 *
	 * @param token GCM token
	 * @throws IOException if unable to reach the GCM PubSub service
	 */
	// [START subscribe_topics]
	private void subscribeTopics(String token) throws IOException {
		GcmPubSub pubSub = GcmPubSub.getInstance(this);
		for (String topic : TOPICS) {
			pubSub.subscribe(token, "/topics/" + topic, null);
		}
	}
	// [END subscribe_topics]

}