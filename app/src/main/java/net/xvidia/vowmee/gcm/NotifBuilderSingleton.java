package net.xvidia.vowmee.gcm;

import android.app.Notification;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;

/**
 * Created by vasu on 23/6/16.
 */
public class NotifBuilderSingleton {

    private Builder mBuilder;
    public static NotificationCompat.InboxStyle inboxStyle002,inboxStyle003,inboxStyle004,inboxStyle005,inboxStyle006,inboxStyle007,inboxStyle008 ;
    private static NotifBuilderSingleton mInstance;
    public static int count002 = 0,count003 = 0,count004 = 0,count005 = 0,count006 =0,count007 = 0,count008 = 0;

    private NotifBuilderSingleton() {

    }

    public static synchronized NotifBuilderSingleton getInstance() {
        if (mInstance == null) {
            mInstance = new NotifBuilderSingleton();
            inboxStyle002 = new NotificationCompat.InboxStyle();
            inboxStyle003 = new NotificationCompat.InboxStyle();
            inboxStyle004 = new NotificationCompat.InboxStyle();
            inboxStyle005 = new NotificationCompat.InboxStyle();
            inboxStyle006 = new NotificationCompat.InboxStyle();
            inboxStyle007 = new NotificationCompat.InboxStyle();
            inboxStyle008 = new NotificationCompat.InboxStyle();

        }
        return mInstance;
    }

    public void createExpandableNotification(Context context) {

        // Building the notification
        mBuilder = new NotificationCompat.Builder(context)
                .setContentTitle(context.getString(R.string.app_name))
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setPriority(Notification.PRIORITY_DEFAULT);

    }
    public Builder getmBuilder(){
        if(mBuilder == null){
            createExpandableNotification(MyApplication.getAppContext());
        }
        return mBuilder;
    }
    public void setMessage(String activityMessage, int styleId){

        String string = activityMessage;
        String[] parts = string.split("@", 3);
        String part1 = "",part2 = "",part3="";
        if(parts!=null && parts.length>0) {
            part1 = parts[0]; // 004
            part1 = part1.replace("@", "");
            part1 = part1.replace("[", "");
            if(parts.length>1) {
                part2 = parts[1]; // 034556-42
                part2 = part2.replace("@", "");
                part2 = part2.replace("[", "");
                if(parts.length>2) {
                    part3 = parts[2];
                    part3 = part3.replace("@", "");
                    part3 = part3.replace("[", "");
                }
            }
        }

        SpannableStringBuilder sb = new SpannableStringBuilder(part1 + part2 + part3);

        StyleSpan b = new StyleSpan(android.graphics.Typeface.BOLD);

        sb.setSpan(b, 0, + part2.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);


        switch (styleId){
            case 002:
                inboxStyle002.addLine(sb);
                mBuilder.setStyle(inboxStyle002);
                mBuilder.setNumber(++count002);
//                inboxStyle002.setSummaryText("People following you");
                if(count002 == 1)
                    mBuilder.setContentText(sb);
                else
                mBuilder.setContentText("People following you");
                break;
            case 003:
                inboxStyle003.addLine(sb);
                mBuilder.setStyle(inboxStyle003);
                mBuilder.setNumber(++count003);
//                inboxStyle003.setSummaryText("People liked you");
                if(count003 == 1)
                    mBuilder.setContentText(sb);
                else
                mBuilder.setContentText("People liked your moments");
                break;
            case 004:
                inboxStyle004.addLine(sb);
                mBuilder.setStyle(inboxStyle004);
                mBuilder.setNumber(++count004);
                if(count004 == 1)
                    mBuilder.setContentText(sb);
                else
                    mBuilder.setContentText("Groups");
                break;
            case 005:
                inboxStyle005.addLine(sb);
                mBuilder.setStyle(inboxStyle005);
                mBuilder.setNumber(++count005);
                if(count005 == 1)
                    mBuilder.setContentText(sb);
                else
                    mBuilder.setContentText("Group Member");
                break;
            case 006:
                inboxStyle006.addLine(sb);
                mBuilder.setStyle(inboxStyle006);
                mBuilder.setNumber(++count006);
                if(count006 == 1)
                    mBuilder.setContentText(sb);
                else
                    mBuilder.setContentText("Share");
                break;
            case 007:
                inboxStyle007.addLine(sb);
                mBuilder.setStyle(inboxStyle007);
                mBuilder.setNumber(++count007);
                if(count007 == 1)
                    mBuilder.setContentText(sb);
                else
                    mBuilder.setContentText("Friend Request Accepted");
                break;
            case 8:
                inboxStyle008.addLine(sb);
                mBuilder.setStyle(inboxStyle008);
                mBuilder.setNumber(++count008);
                if(count008 == 1)
                    mBuilder.setContentText(sb);
                else
                    mBuilder.setContentText("Missed Calls");
                break;
        }
    }
    public void resetInboxStyle(int idInboxStyle){
        switch (idInboxStyle){
            case 002:
                NotifBuilderSingleton.inboxStyle002 = new NotificationCompat.InboxStyle();
                NotifBuilderSingleton.count002 = 0;
                break;
            case 003:
                NotifBuilderSingleton.inboxStyle003 = new NotificationCompat.InboxStyle();
                NotifBuilderSingleton.count003 = 0;
                break;
            case 004:
                NotifBuilderSingleton.inboxStyle004 = new NotificationCompat.InboxStyle();
                NotifBuilderSingleton.count004 = 0;
                break;
            case 005:
                NotifBuilderSingleton.inboxStyle005 = new NotificationCompat.InboxStyle();
                NotifBuilderSingleton.count005 = 0;
                break;
            case 006:
                NotifBuilderSingleton.inboxStyle006 = new NotificationCompat.InboxStyle();
                NotifBuilderSingleton.count006 = 0;
                break;
            case 007:
                NotifBuilderSingleton.inboxStyle007 = new NotificationCompat.InboxStyle();
                NotifBuilderSingleton.count007 = 0;
                break;
            case 8:
                NotifBuilderSingleton.inboxStyle008 = new NotificationCompat.InboxStyle();
                NotifBuilderSingleton.count008 = 0;
                break;
        }

    }
}
