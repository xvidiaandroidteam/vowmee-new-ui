package net.xvidia.vowmee;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.FacebookSdk;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.fragments.TimelineActivityOthers;
import net.xvidia.vowmee.fragments.TimelineActivityOwn;
import net.xvidia.vowmee.listadapter.BottomListAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static net.xvidia.vowmee.MyApplication.getAppContext;

public class TimelineActivityTabs extends AppCompatActivity {
    @Override
    protected void onPause() {
        super.onPause();
        CloseBottomListView();
    }

    private final int GALLERY_INTENT_CALLED = 1234;
    private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 123;
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 125;
//    private boolean isLive;
    private Activity activity;
    private Context context;
    String[] bottomListItemName = {
            MyApplication.getAppContext().getResources().getString(R.string.menu_go_live),
//            MyApplication.getAppContext().getResources().getString(R.string.menu_create_event),
            MyApplication.getAppContext().getResources().getString(R.string.menu_record_video),
            MyApplication.getAppContext().getResources().getString(R.string.menu_upload_video)
    };
    int[] bottomListImages = new int[]{
            R.drawable.go_live,
//            R.drawable.event,
            R.drawable.record_new,
            R.drawable.upload_new
    };
    private FloatingActionButton videoMenu;
    private RelativeLayout layout;
    private Animation slideUp, slideDown;
    private ImageButton cancel;
    private LinearLayout tintLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_timeline_tabs);
        FacebookSdk.sdkInitialize(this);
        activity = this;
        context = TimelineActivityTabs.this;
        TwitterAuthConfig authConfig = new TwitterAuthConfig(getString(R.string.twitter_consumer_key), getString(R.string.twitter_secret_key));
        Fabric.with(this, new TwitterCore(authConfig), new TweetComposer());

        videoMenu = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        ListView listView = (ListView) findViewById(R.id.bottomlistview);
        listView.setAdapter(new BottomListAdapter(this, bottomListItemName, bottomListImages));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {

                    case 0:
                        if (Utils.getInstance().checkCameraHardware(context)) {
//                            isLive = true;
                            DataStorage.getInstance().setIsLiveFlag(true);
                            callCameraApp(true);
                        } else {
                            showMessage("Your phone does not support camera", null);
                        }
                        break;

                    case 1:
                        if (Utils.getInstance().checkCameraHardware(context)) {
//                            isLive = false;
                            DataStorage.getInstance().setIsLiveFlag(false);
                            callCameraApp(false);
                        } else {
                            showMessage("Your phone does not support camera", null);
                        }
                        break;

                    case 2:
                        selectVideo();
                        break;

                }
            }
        });
        layout = (RelativeLayout) findViewById(R.id.bottomLayout);
        tintLayout = (LinearLayout) findViewById(R.id.tint_layout);
        cancel = (ImageButton) findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseBottomListView();
            }
        });
        slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        tintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseBottomListView();
            }
        });
        tintLayout.setClickable(false);
        videoMenu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                slideToTop(tintLayout);
                layout.startAnimation(slideUp);
                layout.setVisibility(View.VISIBLE);

            }
        });

        initToolbars();
    }

    public void slideToBottom(View view) {
        tintLayout.setClickable(false);
        TranslateAnimation animate = new TranslateAnimation(0, 0, 0, view.getHeight());
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);

    }

    public void slideToTop(View view) {
        tintLayout.setBackgroundColor(getResources().getColor(R.color.bottom_list_tint));
        tintLayout.setClickable(true);
        TranslateAnimation animate = new TranslateAnimation(0, 0, view.getHeight(), 0);
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);

    }

    public void CloseBottomListView() {
        if (layout.getVisibility() == View.VISIBLE) {
            slideToBottom(tintLayout);
            layout.startAnimation(slideDown);
            layout.setVisibility(View.INVISIBLE);
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(TimelineActivityTabs.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<String, Integer>();
            // Initial
            perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
            perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
            perms.put(android.Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);

            // Fill with results
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
            // Check for ACCESS_FINE_LOCATION
            if (perms.get(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                    && perms.get(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && perms.get(android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                callCameraApp(DataStorage.getInstance().getIsLiveFlag());
            } else {
                Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_message_camera_permission_denied), Utils.LONG_TOAST);
            }
        } else if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectVideo();
            } else {
                Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_message_upload_permission_denied), Utils.LONG_TOAST);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void callCameraApp(boolean live) {

        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!Utils.getInstance().addPermission(permissionsList, android.Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!Utils.getInstance().addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write External Storage");
        if (!Utils.getInstance().addPermission(permissionsList, android.Manifest.permission.RECORD_AUDIO))
            permissionsNeeded.add("Record Audio");

        Map<String, Integer> perms = new HashMap<String, Integer>();
        // Initial
        perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
        perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
        perms.put(android.Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);

        if (permissionsList.size() == 0) {
            Intent mIntent = new Intent(TimelineActivityTabs.this, RecordVideo.class);
            Bundle mBundle = new Bundle();
            mBundle.putBoolean(AppConsatants.LIVE_VIDEO, live);
            mIntent.putExtras(mBundle);
            startActivity(mIntent);
        } else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    // Need Rationale
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                }
                            });
                    return;
                }
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;

            }
         /*   if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
                showMessageOKCancel("You need to allow access to Camera",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });

                return;
            } else {
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;
            }*/
        }
    }


    private void initToolbars() {
        Toolbar toolbarTop = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbarTop);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.nav_activities);
        toolbarTop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        toolbarTop.setPadding(0, 0, 0, 0);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.timeline_view_pager);
        setupViewPager(viewPager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.timeline_tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

      /*  FloatingActionButton mFloatingActionButton = (FloatingActionButton) findViewById(R.id.goLive);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent mIntent = new Intent(TimelineActivityTabs.this, PublishLiveActivity.class);
                    startActivity(mIntent);
            }
        });*/
//        mFloatingActionButton.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if (event.getAction() == MotionEvent.ACTION_UP) {
//                    Intent mIntent = new Intent(TimelineActivityTabs.this, PublishLiveActivity.class);
//                    startActivity(mIntent);
//                    return true;
//                }
//                return true; // consume the event
//            }
//        });
    }


    private void showMessage(final String mesg, final DialogInterface.OnClickListener okClicked) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new AlertDialog.Builder(TimelineActivityTabs.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .setCancelable(false)
                            .show();
                }
            });
        }catch (WindowManager.BadTokenException e){}
        catch (Exception e){}
    }


    private void selectVideo() {
//        sendMomentList();
        if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent();
            intent.setType("video/*");
            intent.setAction(Intent.ACTION_PICK);
//            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//            intent.setType("video/*");
            startActivityForResult(intent, GALLERY_INTENT_CALLED);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showMessageOKCancel("You need to allow access to Media Storage",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);

                            }
                        });

                return;
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);

                return;
            }
        }

    }


    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedImageUri = null;
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_INTENT_CALLED) {
                selectedImageUri = data.getData();
                String path = Utils.getInstance().getPath(this, selectedImageUri, false);
//					Bitmap bp = ThumbnailUtils.createVideoThumbnail(selectedImagePath, MediaStore.Images.Thumbnails.MINI_KIND);
//                Log.i("Mediapath", path);
                if(!path.isEmpty()) {
                    Intent mIntent = new Intent(TimelineActivityTabs.this, UploadActivity.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString(AppConsatants.MOMENT_PATH, path);
                    mBundle.putBoolean(AppConsatants.RESHARE, false);
                    mIntent.putExtras(mBundle);
                    startActivity(mIntent);
                }

            }
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new TimelineActivityOwn(), getString(R.string.title_fragment_you));
        adapter.addFrag(new TimelineActivityOthers(), getString(R.string.title_friends));
        viewPager.setAdapter(adapter);
    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        if (layout.getVisibility() == View.VISIBLE) {
            CloseBottomListView();
        } else {
            layout.setVisibility(View.INVISIBLE);
            finish();
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        if(PublishLiveActivity.closePublish){
            closePublishSessionConnection();
        }
        super.onResume();
    }

    public void closePublishSessionConnection() {

        if (PublishLiveActivity.mSessionId == null) {
            return;
        }
        if (PublishLiveActivity.mSessionId.isEmpty()) {
            return;
        }
        String subUrl = ServiceURLManager.getInstance().getDestroyLive(PublishLiveActivity.mSessionId, DataStorage.getInstance().getUserUUID());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, subUrl, "{}", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    PublishLiveActivity.mSessionId="";
                    PublishLiveActivity.closePublish = false;
                    String url = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, DataStorage.getInstance().getUsername(), DataStorage.getInstance().getUsername(), AppConsatants.TRUE, "0", "0");
                    VolleySingleton.getInstance(getAppContext()).refresh(url, false);
                    finish();
                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }
}