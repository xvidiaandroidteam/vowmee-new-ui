package net.xvidia.vowmee;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opentok.android.AudioDeviceManager;
import com.opentok.android.BaseAudioDevice;
import com.opentok.android.BaseVideoRenderer;
import com.opentok.android.Connection;
import com.opentok.android.OpentokError;
import com.opentok.android.Publisher;
import com.opentok.android.PublisherKit;
import com.opentok.android.Session;
import com.opentok.android.Session.SessionListener;
import com.opentok.android.Stream;
import com.opentok.android.Subscriber;
import com.opentok.android.SubscriberKit;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.broadcastreceiver.NetworkStateReceiver;
import net.xvidia.vowmee.fragments.MeterView;
import net.xvidia.vowmee.fragments.SubscriberQualityFragment;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.Opentok;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReceiveVideoCallActivity extends Activity implements NetworkStateReceiver.NetworkStateReceiverListener, SessionListener, SubscriberKit.SubscriberListener,
        SubscriberKit.VideoListener, PublisherKit.PublisherListener, Session.SignalListener {

    public static final String SIGNAL_TYPE_CLOSE = "close";
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 1111;
    private static final String LOG_TAG = SubscriberActivity.class.getSimpleName();
    public Session mSession, receivedSession;
    private Publisher mPublisher;
    //    private ArrayList<Subscriber> mSubscribers = new ArrayList<Subscriber>();
//    private HashMap<Stream, Subscriber> mSubscriberStream = new HashMap<Stream, Subscriber>();
    private Handler mHandler = new Handler();

    private LinearLayout incomingCallLayout;
    private RelativeLayout hangupLayout;
    private ProgressDialog pDialog;
    private TextView timerValue, callConnecting, callInProgress, callerName;
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;

    private long startTime2 = 0L;
    private Handler customHandler2 = new Handler();
    long timeInMilliseconds2 = 0L;
    long timeSwapBuff2 = 0L;
    long updatedTime2 = 0L;
    private Uri currentTone;
    private Ringtone ringTone;
    private Activity activity;
//    MediaPlayer mediaPlayer;
    private String sessionClosed = null;
    private Boolean isClosedByPublisher = false;
    private String mApiKey;
    private String mStatus;
    public static String mSessionPublisherId;
    public static String mSessionSubscriberId;
    private String mToken;
    private Subscriber mSubscriber;
    private FrameLayout mSubViewContainer, mPubViewContainer;
    private boolean notificationIntent;
    private String notification_friend_username;
    private RelativeLayout /*mSubscriberAudioOnlyView,*/ defaultUserBackgroundLayout;

    private SubscriberQualityFragment mSubscriberQualityFragment;
    private FragmentTransaction mFragmentTransaction;
    private SubscriberQualityFragment.CongestionLevel congestion = SubscriberQualityFragment.CongestionLevel.Low;
    private boolean mSubscriberAudioOnly = false;
    private ImageView imageView;
    private String thumbnailPath, userDisplayName;

    private CircularImageView circularImageView;

    private ArrayList<Stream> mStreams;
    private PhoneStateListener callStateListener;
    private TelephonyManager telephonyManager;
    MeterView meterView1;
    private boolean firstFlag;
    private ImageView switchCamera;
    private ImageView videoCameraOnOffImageView;
    private boolean isPublisherViewSmall;
    private NetworkStateReceiver networkStateReceiver;
    private boolean pauseTimer = false;
    private boolean initializeTimer = true;
    public static boolean closeReceiveVideoCall;
    private Button acceptCall;
    private Button declineCall;
    private Button endCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_video_call);
        if (savedInstanceState == null) {
            mFragmentTransaction = getFragmentManager().beginTransaction();
            initSubscriberQualityFragment();
            mFragmentTransaction.commitAllowingStateLoss();
        }
        activity = this;

        DataStorage.getInstance().setCallProgress(true);
        Intent intent = getIntent();
        if (null != intent) { //Null Checking
//            receivedSession = intent.getParcelableExtra(AppConsatants.RECEIVER_SESSION);
            mSessionPublisherId = intent.getStringExtra(AppConsatants.LIVE_PUBLISHER_ID);
            notificationIntent = intent.getBooleanExtra(AppConsatants.NOTIFICATION_NEXT_INTENT, false);
            thumbnailPath = intent.getStringExtra(AppConsatants.USER_THUMBNAIL);
            userDisplayName = intent.getStringExtra(AppConsatants.PERSON_FRIEND);
            notification_friend_username = intent.getStringExtra(AppConsatants.NOTIFICATION_FRIEND_USERNAME);
//            mApiKey = intent.getStringExtra(AppConsatants.API_KEY);
//            mStatus = intent.getStringExtra(AppConsatants.STATUS);
        }

        if (mSessionPublisherId == null) {
            showMessage(getString(R.string.error_call_ended), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onBackPressed();
                }
            });
        } else if (mSessionPublisherId.isEmpty()) {
            showMessage(getString(R.string.error_call_ended), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onBackPressed();
//                    SubscriberActivity.this.finish();
                }
            });
        }
        firstFlag  = true;
        initializeTimer = true;
        closeReceiveVideoCall = false;
        switchCamera = (ImageView) findViewById(R.id.button_switch_camera);
        acceptCall = (Button) findViewById(R.id.acceptCall);
        declineCall = (Button) findViewById(R.id.declineBeforeAccept);
        endCall = (Button) findViewById(R.id.endcall);
        acceptCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptCall();
            }
        });
        declineCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                declineBeforeAccept();
            }
        });
        endCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEndCall();
            }
        });
        switchCamera.setVisibility(View.GONE);
        switchCamera.setOnClickListener(new View.OnClickListener(

        ) {
            @Override
            public void onClick(View v) {
                flipCamera();
            }
        });
        videoCameraOnOffImageView = (ImageView) findViewById(R.id.video_camera_imageview);
        videoCameraOnOffImageView.setVisibility(View.GONE);
        videoCameraOnOffImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mPublisher != null) {
                    if (mPublisher.getPublishVideo() == true) {
                        mPublisher.setPublishVideo(false);
                        videoCameraOnOffImageView.setImageDrawable(getResources().getDrawable(R.drawable.video));
                    } else {
                        mPublisher.setPublishVideo(true);
                        videoCameraOnOffImageView.setImageDrawable(getResources().getDrawable(R.drawable.video_off));
                    }
                }
            }
        });
        defaultUserBackgroundLayout = (RelativeLayout) findViewById(R.id.defaultUserbackgroundLayout);
        mSubViewContainer = (FrameLayout) findViewById(R.id.publisher_container);
        mPubViewContainer = (FrameLayout) findViewById(R.id.subscriber_container);
        isPublisherViewSmall = true;
        mPubViewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mPublisher != null  && mSubscriber !=null) {
                    if (isPublisherViewSmall) {
                        mPubViewContainer.removeView(mPublisher.getView());
                        mSubViewContainer.removeView(mSubscriber.getView());
                        mPubViewContainer.addView(mSubscriber.getView(),0);
                        mSubViewContainer.addView(mPublisher.getView());
                        isPublisherViewSmall = false;
                    } else {
                        mPubViewContainer.removeView(mSubscriber.getView());
                        mSubViewContainer.removeView(mPublisher.getView());
                        mPubViewContainer.addView(mPublisher.getView(),0);
                        mSubViewContainer.addView(mSubscriber.getView());
                        isPublisherViewSmall = true;
                    }
                }
            }
        });
        callInProgress = (TextView) findViewById(R.id.callProgress);
        callInProgress.setVisibility(View.INVISIBLE);
        imageView = (ImageView) findViewById(R.id.home_profile_image_background);
        circularImageView = (CircularImageView) findViewById(R.id.home_profile_image);
        callerName = (TextView) findViewById(R.id.callerName);
        callConnecting = (TextView) findViewById(R.id.callConnecting);
        callConnecting.setVisibility(View.INVISIBLE);
        timerValue = (TextView) findViewById(R.id.timerText);
        timerValue.setVisibility(View.INVISIBLE);

        incomingCallLayout = (LinearLayout) findViewById(R.id.incomingCallLayout);
        hangupLayout = (RelativeLayout) findViewById(R.id.hangupLayout);
        incomingCallLayout.setVisibility(View.VISIBLE);
        hangupLayout.setVisibility(View.INVISIBLE);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Connecting...please wait..");
        pDialog.setCancelable(false);
//        mediaPlayer = MediaPlayer.create(this, R.raw.tring_tring_tring);
        String ringtoneUriString = DataStorage.getInstance().getCallPath();
        if(ringtoneUriString.isEmpty()) {
            currentTone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        }else {
            currentTone = Uri.parse(ringtoneUriString);
        }

        ringTone = RingtoneManager.getRingtone(getApplicationContext(), currentTone);
        startRing();
        downloadContentFromS3Bucket(thumbnailPath);
        callerName.setText(getString(R.string.is_calling, userDisplayName));

        mStreams = new ArrayList<Stream>();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSubscriber != null) {
                    if (mSubscriberAudioOnly) {
                        mSubscriber.getView().setVisibility(View.GONE);
//                        setAudioOnlyView(true);
                        congestion = SubscriberQualityFragment.CongestionLevel.High;
                    }
                }
            }
        }, 0);


    }
    @Override
    public void onSaveInstanceState( Bundle outState ) {

    }
    private Runnable updateTimerThread2 = new Runnable() {

        public void run() {

            timeInMilliseconds2 = SystemClock.uptimeMillis() - startTime2;
            updatedTime2 = timeSwapBuff2 + timeInMilliseconds2;

            if (updatedTime2 >= AppConsatants.timeoutMakeCallEnd) {
                onBackPressed();
                Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_failed_connect),Utils.LONG_TOAST);

            }
            customHandler2.postDelayed(this, 0);

        }
    };
    public void flipCamera() {
        try {
            if (isPublisherViewSmall) {
                mPubViewContainer.removeView(mPublisher.getView());
                mPublisher.cycleCamera();
                mPubViewContainer.addView(mPublisher.getView(), 0);
                mPubViewContainer.requestFocus();
                switchCamera.requestFocus();
            } else {
                mSubViewContainer.removeView(mPublisher.getView());
                mPublisher.cycleCamera();
                mSubViewContainer.addView(mPublisher.getView(), 0);
            }

//        mPubViewContainer.removeView(mPublisher.getView());
//        mPublisher.cycleCamera();
//        mPubViewContainer.addView(mPublisher.getView());
//        mPubViewContainer.requestFocus();
        } catch (OutOfMemoryError e) {

        } catch (Exception e) {

        }
    }
    private void changeVoiceMode(boolean isMuted){
        if(isMuted){
            AudioDeviceManager.getAudioDevice().setOutputMode(
                    BaseAudioDevice.OutputMode.Handset);
        }else{
            AudioDeviceManager.getAudioDevice().setOutputMode(
                    BaseAudioDevice.OutputMode.SpeakerPhone);
        }

    }

    private void downloadContentFromS3Bucket(String filePath){
        if(filePath!=null && !filePath.isEmpty()) {

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
                circularImageView.setImageBitmap(image);
                imageView.setImageAlpha(80);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null) {
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                        imageView.setImageBitmap(image1);
                        circularImageView.setImageBitmap(image1);
                        imageView.setImageAlpha(80);
                    }
                } else {
                    thumbnailPath = notification_friend_username+AppConsatants.FILE_EXTENSION_JPG;
                    circularImageView.setImageResource(R.drawable.user);
                     image = FileCache.getInstance().getBitmapFromMemCache(thumbnailPath);
                    if(image!=null ) {
                        imageView.setImageBitmap(image);
                        circularImageView.setImageBitmap(image);
                        imageView.setImageAlpha(80);
                    }else {
                        filePathImage = Utils.getInstance().getLocalContentPath(thumbnailPath);
                        file = new File(filePathImage);
                        if (file.exists()) {
                            Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                            if (image1 != null) {
                                FileCache.getInstance().addBitmapToMemoryCache(thumbnailPath, image1);
                                imageView.setImageBitmap(image1);
                                circularImageView.setImageBitmap(image1);
                                imageView.setImageAlpha(80);
                            }
                        }
                    }
                }
            }
        }else {
            thumbnailPath = notification_friend_username+AppConsatants.FILE_EXTENSION_JPG;
            circularImageView.setImageResource(R.drawable.user);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(thumbnailPath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
                circularImageView.setImageBitmap(image);
                imageView.setImageAlpha(80);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(thumbnailPath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null) {
                        FileCache.getInstance().addBitmapToMemoryCache(thumbnailPath, image1);
                        imageView.setImageBitmap(image1);
                        circularImageView.setImageBitmap(image1);
                        imageView.setImageAlpha(80);
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        restartAudioMode();
        stopRing();

        closeReceiveVideoCall = true;
        DataStorage.getInstance().setCallProgress(false);
        if(telephonyManager!=null && callStateListener!=null)
            telephonyManager.listen(callStateListener, PhoneStateListener.LISTEN_NONE);

        if (mSession != null) {
            hideDialog();
            mSession.disconnect();
            if (mSession != null)
                mSession.disconnect();
            if (mPublisher != null)
                mPublisher.destroy();
            if (mSubscriber != null)
                mSubscriber.destroy();
        }

        finish();
        super.onBackPressed();
    }

    public void initSubscriberQualityFragment() {
        mSubscriberQualityFragment = new SubscriberQualityFragment();
        getFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_sub_quality_container,
                        mSubscriberQualityFragment).commit();
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        fetchSubscriberSessionConnectionData();
        startTime2 = SystemClock.uptimeMillis();
        customHandler2.postDelayed(updateTimerThread2, 0);
    }

    public void fetchSubscriberSessionConnectionData() {

        String subUrl = ServiceURLManager.getInstance().getAcceptCallUrl(mSessionPublisherId, DataStorage.getInstance().getUserUUID());

//        String subUrl = ServiceURLManager.getInstance().getLiveSubscriberTokenUrl(mSessionPublisherId, DataStorage.getInstance().getUserUUID());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, subUrl, "{}", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    Opentok obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Opentok.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        mSessionSubscriberId = obj.getSessionId();
                        mToken = obj.getToken();
                        mApiKey = obj.getApiKey();
                        mStatus = obj.getStatus();

                    }else{
                        onBackPressed();
                    }

//                    Log.i(LOG_TAG, mApiKey);
//                    Log.i(LOG_TAG, mSessionSubscriberId);
//                    Log.i(LOG_TAG, mToken);

                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                showMessage(getString(R.string.error_failed_connect), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onBackPressed();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //add params <key,value>
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                // add headers <key,value>
                String credentials = "user" + ":" + "govind";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(),
                        Base64.NO_WRAP);
                headers.put("Authorization", auth);
                return headers;
            }

        };
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void showMessage(final String mesg, final DialogInterface.OnClickListener okClicked) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                new AlertDialog.Builder(ReceiveVideoCallActivity.this).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(mesg)
                        .setPositiveButton(android.R.string.ok, okClicked)
                        .setCancelable(false)
                        .show();
            }
        });
    }

    public void onEndCall() {
        mSession.sendSignal(SIGNAL_TYPE_CLOSE, getString(R.string.error_live_session_ended));
        onBackPressed();
    }


    public void acceptCall() {
//        restartAudioMode();
        stopRing();
        if (mToken != null && !mToken.equalsIgnoreCase("null") && mStatus.equalsIgnoreCase(AppConsatants.STATUS_IS_LIVE)
                && mSessionSubscriberId != null && !mSessionSubscriberId.equalsIgnoreCase("null")) {
            initializeSession();
            initializePublisher();
            firstFlag  = false;
            if (mSession != null) {

                callConnecting.setVisibility(View.VISIBLE);
                incomingCallLayout.setVisibility(View.INVISIBLE);
                hangupLayout.setVisibility(View.VISIBLE);
                callerName.setText(userDisplayName);
                stopRing();
            }

        } else {
//            showMessage(getString(R.string.error_live_session_closed), new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    ReceiveVideoCallActivity.this.finish();
//                }
//            });
            Utils.getInstance().displayToast(ReceiveVideoCallActivity.this,getString(R.string.error_live_session_closed),Utils.SHORT_TOAST);
            finish();
        }

        //showDialog();

    }

    private void stopRing() {
        try {
//            if (mediaPlayer != null) {
//                if (mediaPlayer.isPlaying()) {
//                    mediaPlayer.stop();
//                    mediaPlayer.release();
//                }
//
//            }
            if (ringTone != null) {
                if (ringTone.isPlaying()) {
                    ringTone.stop();
                }
            }
        } catch (Exception e) {
            Log.e("MediaPlayer", e.getMessage());
        }

    }

    private void initialiseIncomingCallListener() {
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        // register PhoneStateListener
        callStateListener = new PhoneStateListener() {
            public void onCallStateChanged(int state, String incomingNumber) {

                // If phone ringing
                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    onBackPressed();

                }
                // If incoming call received
                if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    onBackPressed();
                }


//                if(state== TelephonyManager.CALL_STATE_IDLE)
//                {
//                    Toast.makeText(getApplicationContext(),"phone is neither ringing nor in a call", Toast.LENGTH_LONG).show();
//                }
            }
        };
        telephonyManager.listen(callStateListener, PhoneStateListener.LISTEN_CALL_STATE);


    }

    private void startRing() {
        try {
            if(ringTone!=null)
                ringTone.play();

        } catch (Exception e) {
            Log.e("MediaPlayer", e.getMessage());
        }

    }

    public void declineBeforeAccept() {
        onBackPressed();
    }


    public void onResume() {
        super.onResume();
        checkPermission();
        networkStateReceiver = new NetworkStateReceiver(this);
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

    }
    @Override
    protected void onPause() {
        try{
            if(customHandler!=null && updateTimerThread != null)
                customHandler.removeCallbacks(updateTimerThread);
            if(customHandler2!=null && updateTimerThread2 != null)
                customHandler2.removeCallbacks(updateTimerThread2);

            DataStorage.getInstance().setCallProgress(false);
        }catch (Exception e){}
        if(networkStateReceiver!=null){
            networkStateReceiver.removeListener(this);
            this.unregisterReceiver(networkStateReceiver);
        }
//        callSessionCancel();

        stopRing();
        closeReceiveVideoCall = true;
        DataStorage.getInstance().setCallProgress(false);
        super.onPause();
    }


    public void restartAudioMode() {
        AudioManager Audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        Audio.setMode(AudioManager.MODE_NORMAL);
        this.setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
    }

    private void initializeSession() {
        mSession = new Session(this, mApiKey, mSessionSubscriberId);
        mSession.setSessionListener(this);
        mSession.setSignalListener(this);
        mSession.connect(mToken);
        changeVoiceMode(true);
    }

    @Override
    public void onConnected(Session session) {

        if(customHandler2!=null && updateTimerThread2 != null)
            customHandler2.removeCallbacks(updateTimerThread2);
        meterView1 = (MeterView) findViewById(R.id.volumeMic);
        if (mPublisher != null) {
            mSession.publish(mPublisher);
            mPublisher.setAudioLevelListener(new PublisherKit.AudioLevelListener() {
                @Override
                public void onAudioLevelUpdated(PublisherKit publisher,
                                                float audioLevel) {
                    meterView1.setMeterValue(audioLevel);
                }
            });
            hideDialog();
        }

        meterView1.setVisibility(View.INVISIBLE);
        meterView1.setIcons(BitmapFactory.decodeResource(getResources(),
                R.drawable.unmute_pub), BitmapFactory.decodeResource(
                getResources(), R.drawable.mute_pub));

        meterView1.setOnClickListener(new MeterView.OnClickListener() {
            @Override
            public void onClick(MeterView view) {
                mPublisher.setPublishAudio(!view.isMuted());
            }
        });
//        callInProgress.setText(getString(R.string.call_in_progress));
    }

    private Runnable updateTimerThread = new Runnable() {

        public void run() {
            if(!pauseTimer) {

                timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
                updatedTime = timeSwapBuff + timeInMilliseconds;
                int secs = (int) (updatedTime / 1000);
                int mins = secs / 60;
                secs = secs % 60;
//            int milliseconds = (int) (updatedTime % 1000);
                timerValue.setText("" + String.format("%02d", mins) + ":"
                        + String.format("%02d", secs));
            /*timerValue.setText("" + mins + ":"
                    + String.format("%02d", secs) + ":"
                    + String.format("%03d", milliseconds));*/
            }else{
                startTime = SystemClock.uptimeMillis() - updatedTime;
//                startTime = SystemClock.uptimeMillis();
            }
            customHandler.postDelayed(this, 0);

        }
    };

    private void subscribeToStream(Stream stream) {
        mSubscriber = new Subscriber(this, stream);
        mSubscriber.setSubscriberListener(this);
        mSubscriber.setVideoListener(this);
        mSubscriber.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                BaseVideoRenderer.STYLE_VIDEO_FILL);
        mSession.subscribe(mSubscriber);
        if (mSubscriber.getSubscribeToVideo()) {
            showDialog();
        }
        mSubscriber.setSubscribeToVideo(true);
        mSubscriber.setSubscribeToAudio(true);

        defaultUserBackgroundLayout.setVisibility(View.INVISIBLE);
    }

    public void loadFragments() {
        // show subscriber status
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSubscriber != null) {
//                    mSubscriberFragment.showSubscriberWidget(true);
//                    mSubscriberFragment.initSubscriberUI();

                    if (congestion != SubscriberQualityFragment.CongestionLevel.Low) {
                        mSubscriberQualityFragment.setCongestion(congestion);
                        mSubscriberQualityFragment.showSubscriberWidget(true);
                    }
                }
            }
        }, 0);

    }

    /*private void setAudioOnlyView(boolean audioOnlyEnabled) {
        mSubscriberAudioOnly = audioOnlyEnabled;

        if (audioOnlyEnabled) {
            mSubscriber.getView().setVisibility(View.GONE);
            mSubscriberAudioOnlyView.setVisibility(View.VISIBLE);
//            mSubscriberAudioOnlyView.setOnClickListener(onViewClick);
//
//            // Audio only text for subscriber
//            TextView subStatusText = (TextView) findViewById(R.id.subscriberName);
//            subStatusText.setText(R.string.audioOnly);
//            AlphaAnimation aa = new AlphaAnimation(1.0f, 0.0f);
//            aa.setDuration(ANIMATION_DURATION);
//            subStatusText.startAnimation(aa);


            mSubscriber
                    .setAudioLevelListener(new SubscriberKit.AudioLevelListener() {
                        @Override
                        public void onAudioLevelUpdated(
                                SubscriberKit subscriber, float audioLevel) {
                            mAudioLevelView.setMeterValue(audioLevel);
                        }
                    });
        } else {
            mSubscriber.getView().setVisibility(View.VISIBLE);
            mSubscriberAudioOnlyView.setVisibility(View.GONE);

            mSubscriber.setAudioLevelListener(null);
            attachSubscriberView(mSubscriber);
        }
    }*/

    private void attachSubscriberView(Subscriber subscriber) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        mSubViewContainer.removeView(mSubscriber.getView());
        mSubViewContainer.addView(subscriber.getView(), layoutParams);
        subscriber.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                BaseVideoRenderer.STYLE_VIDEO_FIT);
    }

    @Override
    public void onDisconnected(Session session) {
//        mPublisher.destroy();
//        if (mPublisher != null) {
//            mPublisher.destroy();
//        }
//        if (mSession != null)
//            mSession.disconnect();
//        finish();
    }

    private void initializePublisher() {
        mPublisher = new Publisher(getApplicationContext(), "Receiver",
                Publisher.CameraCaptureResolution.MEDIUM,
                Publisher.CameraCaptureFrameRate.FPS_15);
        mPublisher.getRenderer().setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                BaseVideoRenderer.STYLE_VIDEO_FILL);
        mPublisher.setPublisherListener(this);
        mPublisher.setPublishVideo(true);
        mPublisher.setPublishAudio(true);
        mPubViewContainer.addView(mPublisher.getView(), 0);
        mPubViewContainer.setVisibility(View.VISIBLE);
        videoCameraOnOffImageView.setVisibility(View.VISIBLE);
        switchCamera.setVisibility(View.VISIBLE);
        hideDialog();

    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {

        Log.i(LOG_TAG, "Stream Received");
        mStreams.add(stream);
        if (mSubscriber == null) {
            subscribeToStream(stream);
            hideDialog();
            if(meterView1 != null) {
                meterView1.setVisibility(View.VISIBLE);
            }
            final MeterView meterView = (MeterView) findViewById(R.id.volume);
//            meterView.setMuted(false);
            meterView.setIcons(BitmapFactory.decodeResource(getResources(),
                    R.drawable.unmute_sub), BitmapFactory.decodeResource(
                    getResources(), R.drawable.mute_sub));
//            AudioDeviceManager.getAudioDevice().setOutputMode(
//                    BaseAudioDevice.OutputMode.SpeakerPhone);

            meterView.setOnClickListener(new MeterView.OnClickListener() {
                @Override
                public void onClick(MeterView view) {
//                    mSubscriber.setSubscribeToAudio(!view.isMuted());
                    changeVoiceMode(view.isMuted());

                }
            });
            mSubscriber
                    .setAudioLevelListener(new SubscriberKit.AudioLevelListener() {
                        @Override
                        public void onAudioLevelUpdated(
                                SubscriberKit subscriber, float audioLevel) {
                            meterView.setMeterValue(audioLevel);
                        }
                    });
            callConnecting.setVisibility(View.INVISIBLE);
            callInProgress.setVisibility(View.VISIBLE);
            callInProgress.setText(getString(R.string.call_in_progress));
            pauseTimer = false;
            timerValue.setVisibility(View.VISIBLE);
            if(initializeTimer) {
                startTime = SystemClock.uptimeMillis();
                initializeTimer = false;
            }
            customHandler.postDelayed(updateTimerThread, 0);
        }


        loadFragments();
//        Subscriber subscriber = new Subscriber(this, stream);
//
//        // Subscribe audio only
//        subscriber.setSubscribeToVideo(false);
//
//        mSession.subscribe(subscriber);
//        mSubscribers.add(subscriber);
//        mSubscriberStream.put(stream, subscriber);

    }

    @Override
    public void onStreamDropped(Session session, Stream stream) {
//        Subscriber subscriber = mSubscriberStream.get(stream);
//        if (subscriber != null) {
//            mSession.unsubscribe(subscriber);
//            mSubscribers.remove(subscriber);
//            mSubscriberStream.remove(stream);
//        }

        mStreams.remove(stream);
        if(callInProgress.getText().toString() == getString(R.string.call_in_progress)) {
            callInProgress.setText("Reconnecting...");
            pauseTimer = true;
        }
        if (isClosedByPublisher) {
            Utils.getInstance().displayToast(ReceiveVideoCallActivity.this,sessionClosed,Utils.SHORT_TOAST);
            onBackPressed();
        }
        if (mSubscriber != null
                && mSubscriber.getStream().getStreamId()
                .equals(stream.getStreamId())) {
            mSubViewContainer.removeView(mSubscriber.getView());
            defaultUserBackgroundLayout.setVisibility(View.VISIBLE);
            mSubscriber = null;
            mSubscriberAudioOnly = false;
            if (!mStreams.isEmpty()) {
//                subscribeToStream(mStreams.get(0));
            }
//            Toast.makeText(this, "Call has ended", Toast.LENGTH_LONG).show();
//            onDisconnected(mSession);


        }
//        showMessage(getString(R.string.error_call_ended), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                onBackPressed();
//            }
//        });
    }

    @Override
    public void onError(Session session, OpentokError error) {
//        Toast.makeText(this, error.getMessage() + "Session Error", Toast.LENGTH_LONG).show();
//        hideDialog();
    }

    private void showDialog() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

//                    if (!pDialog.isShowing())
//                        pDialog.show();
                }
            });
        } catch (Exception e) {
        }
    }

    private void hideDialog() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

//                    if (pDialog.isShowing())
//                        pDialog.dismiss();
                }
            });
        } catch (Exception e) {
        }
    }

    //Subscriber Listener Methods
    @Override
    public void onConnected(SubscriberKit subscriberKit) {

    }

    @Override
    public void onDisconnected(SubscriberKit subscriberKit) {

    }

    @Override
    public void onError(SubscriberKit subscriberKit, OpentokError opentokError) {
//        Toast.makeText(this, opentokError.getMessage() + "Subscription Error", Toast.LENGTH_LONG).show();
    }

    //Publisher Listener Methods
    @Override
    public void onStreamCreated(PublisherKit publisherKit, Stream stream) {
        Log.i("Stream created", "Publisher");
    }

    @Override
    public void onStreamDestroyed(PublisherKit publisherKit, Stream stream) {

    }

    @Override
    public void onError(PublisherKit publisherKit, OpentokError opentokError) {
//        Toast.makeText(this, opentokError.getMessage() + "Publisher Error", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onVideoDataReceived(SubscriberKit subscriberKit) {
        hideDialog();
        attachSubscriberView(mSubscriber);
    }

    @Override
    public void onVideoDisabled(SubscriberKit subscriber, String reason) {

        if (mSubscriber == subscriber) {
//            setAudioOnlyView(false);
            defaultUserBackgroundLayout.setVisibility(View.VISIBLE);
            if (reason.equals("quality")) {
                mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.High);
                congestion = SubscriberQualityFragment.CongestionLevel.High;
                setSubQualityMargins();
                mSubscriberQualityFragment.showSubscriberWidget(true);
            }
        }
    }

    public void setSubQualityMargins() {
        RelativeLayout subQualityContainer = mSubscriberQualityFragment.getSubQualityContainer();
        if (subQualityContainer != null) {
            RelativeLayout.LayoutParams subQualityLayoutParams = (RelativeLayout.LayoutParams) subQualityContainer.getLayoutParams();
//            RelativeLayout.LayoutParams audioMeterLayoutParams = (RelativeLayout.LayoutParams) mAudioLevelView.getLayoutParams();

            int bottomMargin = 0;

            // control pub fragment
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {

                if (bottomMargin == 0) {
                    bottomMargin = Utils.getInstance().convertDpToPixel(10, this);
                }
                subQualityLayoutParams.rightMargin = Utils.getInstance().convertDpToPixel(10, this);
            }

            subQualityLayoutParams.bottomMargin = bottomMargin;

            mSubscriberQualityFragment.getSubQualityContainer().setLayoutParams(
                    subQualityLayoutParams);
        }

    }

    @Override
    public void onVideoEnabled(SubscriberKit subscriber, String reason) {
        if (mSubscriber == subscriber) {
//            setAudioOnlyView(false);
            defaultUserBackgroundLayout.setVisibility(View.INVISIBLE);
        }
        if (reason.equals("quality")) {
            mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.Low);
            congestion = SubscriberQualityFragment.CongestionLevel.Low;
            mSubscriberQualityFragment.showSubscriberWidget(false);
        }
    }

    @Override
    public void onVideoDisableWarning(SubscriberKit subscriberKit) {
        mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.Mid);
        congestion = SubscriberQualityFragment.CongestionLevel.Mid;
        setSubQualityMargins();
        mSubscriberQualityFragment.showSubscriberWidget(true);
    }

    @Override
    public void onVideoDisableWarningLifted(SubscriberKit subscriberKit) {
        mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.Low);
        congestion = SubscriberQualityFragment.CongestionLevel.Low;
        mSubscriberQualityFragment.showSubscriberWidget(false);
    }

    //Incoming call state listener permission

    private void checkPermission() {

        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("Handle Incoming Call");

// final String[] PERMISSIONS_CONTACT = {Manifest.permission.READ_CONTACTS,Manifest.permission.READ_SMS};

        Map<String, Integer> perms = new HashMap<String, Integer>();
// Initial
        perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);

        if (permissionsList.size() == 0) {
        } else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
// Need Rationale
// String message = "You need to grant access to Contacts and SMS permissions";
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                            REQUEST_CODE_ASK_PERMISSIONS);
                                }
                            });
                    return;
                }
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;

            }
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<String, Integer>();
// Initial
            perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);

// Fill with results
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
// Check for ACCESS_FINE_LOCATION
            if (perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                initialiseIncomingCallListener();
            } else {
                Toast.makeText(getApplicationContext(), "Permissions has not been granted", Toast.LENGTH_LONG).show();

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
// Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
                return false;
        }
        return true;
    }

    private void showMessageOKCancel(final String message, final DialogInterface.OnClickListener okListener) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new android.support.v7.app.AlertDialog.Builder(ReceiveVideoCallActivity.this)
                            .setMessage(message)
                            .setPositiveButton("OK", okListener)
                            .setNegativeButton("Cancel", null)
                            .create()
                            .show();
                }
            });
        }catch (WindowManager.BadTokenException e){}
        catch (Exception e){}

    }

    @Override
    public void onNetworkAvailable() {
        if (!firstFlag) {

            if(mSubscriber!=null) {
                if (mPubViewContainer != null)
                    mPubViewContainer.removeView(mSubscriber.getView());
                if (mSubViewContainer != null)
                    mSubViewContainer.removeView(mPublisher.getView());
                if (mSession != null)
                    mSession.disconnect();
                if (mPublisher != null)
                    mPublisher.destroy();
                if (mSubscriber != null)
                    mSubscriber.destroy();
                mPublisher = null;
                mSession = null;
                mSubscriber = null;
            }
            initializeSession();
            initializePublisher();
        }
    }

    @Override
    public void onNetworkUnavailable() {
// showDialog();
        callInProgress.setText("Reconnecting...");
        pauseTimer = true;
        initializeTimer = false;
    }

    @Override
    public void onSignalReceived(Session session, String type, String data, Connection connection) {
        switch (type){
            case SIGNAL_TYPE_CLOSE:
                sessionClosed = data;
                isClosedByPublisher = true;
// onDisconnected(session);
                break;
        }
    }

}
