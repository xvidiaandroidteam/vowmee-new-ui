package net.xvidia.vowmee.storage.sqlite.manager;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.network.model.SocialAuthFacebook;
import net.xvidia.vowmee.network.model.SocialAuthTwitter;
import net.xvidia.vowmee.network.model.UploadData;
import net.xvidia.vowmee.storage.sqlite.CacheDataSource;

import java.util.ArrayList;

public class DataCacheManager {

	private static DataCacheManager Instance = null;// new DataCacheManager();
	CacheDataSource dataSource = null;// CachDataSource.getInstance();

	public static DataCacheManager getInstance() {
		if (Instance == null)
			Instance = new DataCacheManager();
		return Instance;
	}

	private DataCacheManager() {
		dataSource = CacheDataSource.getInstance();
	}

	public void open() {
		dataSource.open(MyApplication.getAppContext());
	}

	public void close() {
		dataSource.close();
	}



	 /*
	  Social Auth Facebook DATA
	 */

	/**
	 * Save SocialAuthFacebook data
	 * 
	 * @param obj
	 * @return boolean
	 */

	public boolean saveSigninFacebookData(SocialAuthFacebook obj) {
		boolean retVal = false;
		try {
			if (obj != null) {
				dataSource.open(MyApplication.getAppContext());
				dataSource.saveSigninFacebookData(obj);
				dataSource.close();
				retVal = true;
			} else {
				return false;
			}
		} catch (Exception e) {
			retVal = false;
		}
		return retVal;
	}

	/**
	 * updates setting Data
	 * 
	 * @param obj SettingData
	 * @return boolean
	 */
	public boolean updateSigninFacebookData(SocialAuthFacebook obj) {
		boolean retVal = false;
		try {
			if (obj != null) {
				dataSource.open(MyApplication.getAppContext());
				dataSource.updateSigninFacebookData(obj);
				dataSource.close();
				retVal = true;
			} else {
				return false;
			}
		} catch (Exception e) {
			retVal = false;
		}
		return retVal;
	}

	/**
	 * Gets SocialAuthFacebook data
	 * 
	 * @return SocialAuthFacebook
	 */
	public final SocialAuthFacebook getFacebookData(String userId) {
		dataSource.open(MyApplication.getAppContext());
		SocialAuthFacebook progData = dataSource.getFacebookData(userId);
		dataSource.close();
		if (progData == null) {
			progData = new SocialAuthFacebook();
		}
		return progData;
	}

	/**
	 * Clears database for a setting
	 * 
	 */
	public final void clearSocialAuthFacebook() {
		dataSource.open(MyApplication.getAppContext());
		dataSource.removeSigninFacebookData();
		dataSource.close();
	}

/*
	  Social Auth Twitter DATA
	 */

	/**
	 * Save SocialAuthFacebook data
	 *
	 * @param obj
	 * @return boolean
	 */

	public boolean saveSigninTwitterData(SocialAuthTwitter obj) {
		boolean retVal = false;
		try {
			if (obj != null) {
				dataSource.open(MyApplication.getAppContext());
				dataSource.saveSigninTwitterData(obj);
				dataSource.close();
				retVal = true;
			} else {
				return false;
			}
		} catch (Exception e) {
			retVal = false;
		}
		return retVal;
	}

	/**
	 * updates setting Data
	 *
	 * @param obj SettingData
	 * @return boolean
	 */
	public boolean updateSigninTwitterData(SocialAuthFacebook obj) {
		boolean retVal = false;
		try {
			if (obj != null) {
				dataSource.open(MyApplication.getAppContext());
				dataSource.updateSigninFacebookData(obj);
				dataSource.close();
				retVal = true;
			} else {
				return false;
			}
		} catch (Exception e) {
			retVal = false;
		}
		return retVal;
	}

	/**
	 * Gets SocialAuthFacebook data
	 *
	 * @return SocialAuthFacebook
	 */
	public final SocialAuthTwitter getTwitterData(String userID) {
		dataSource.open(MyApplication.getAppContext());
		SocialAuthTwitter progData = dataSource.getTwitterData(userID);
		dataSource.close();
		if (progData == null) {
			progData = new SocialAuthTwitter();
		}
		return progData;
	}

	/**
	 * Clears database for a setting
	 *
	 */
	public final void clearSocialAuthTwitter() {
		dataSource.open(MyApplication.getAppContext());
		dataSource.removeSigninTwitterData();
		dataSource.close();
	}


	/*
	  Upload DATA
	*/

	/**
	 * Save upload data
	 *
	 * @param obj
	 * @return boolean
	 */

	public boolean saveUploadData(UploadData obj) {
		boolean retVal = false;
		try {
			if (obj != null) {
				dataSource.open(MyApplication.getAppContext());
				dataSource.saveUploadData(obj);
				dataSource.close();
				retVal = true;
			} else {
				return false;
			}
		} catch (Exception e) {
			retVal = false;
		}
		return retVal;
	}

	/**
	 * updates Upload Data
	 *
	 * @param obj UploadData
	 * @return boolean
	 */
	public boolean updateUploadData(UploadData obj) {
		boolean retVal = false;
		try {
			if (obj != null) {
				dataSource.open(MyApplication.getAppContext());
				dataSource.updateUploadData(obj);
				dataSource.close();
				retVal = true;
			} else {
				return false;
			}
		} catch (Exception e) {
			retVal = false;
		}
		return retVal;
	}

	/**
	 * Gets UploadData data
	 *
	 * @return UploadData
	 */
	public final UploadData getUploadData(String userID) {
		dataSource.open(MyApplication.getAppContext());
		UploadData progData = dataSource.getUploadData(userID);
		dataSource.close();
		if (progData == null) {
			progData = new UploadData();
		}
		return progData;
	}
	/**
	 * Getsall UploadData data
	 *
	 * @return UploadData
	 */
	public final ArrayList<UploadData> getAllUploadData() {
		dataSource.open(MyApplication.getAppContext());
		ArrayList<UploadData> progData = dataSource.getAllUploadData();
		dataSource.close();
		if (progData == null) {
			progData = new ArrayList();
		}
		return progData;
	}
	/**
	 * Clears table for a upload
	 *
	 */
	public final void removeUploadData() {
		dataSource.open(MyApplication.getAppContext());
		dataSource.removeUploadData();
		dataSource.close();
	}
	/**
	 * Clears data by moment uuid for a upload
	 *
	 */
	public final void removeUploadData(String uuid) {
		dataSource.open(MyApplication.getAppContext());
		dataSource.removeUploadDataByName(uuid);
		dataSource.close();
	}
}
