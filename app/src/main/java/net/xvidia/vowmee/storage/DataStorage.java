package net.xvidia.vowmee.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.Utils.Utils;


public class DataStorage {
	static String MY_PREFERENCES = "SETTINGPREFERENCE";
	static SharedPreferences sharedpreferences;
	private static final String STR_REGISTERED = "REGISTERED";
	private static final String STR_USERNAME = "USERNAME";
	private static final String STR_DESTROY_LIVE = "DESTROY_LIVE";
	private static final String STR_USERNAME_UUID = "USERNAMEUUID";
	private static final String GCM_REG_ID = "GCM_REG_ID";
	private static final String GCM_REG_ID_TO_SERVER = "GCM_REG_ID_TO_SERVER";
	private static final String APP_VERSION = "APP_VERSION";
	private static final String APP_NOTIFICATION_ID = "APP_NOTIFICATION_ID";
	private static final String APP_SOCIAL_ID = "APP_SOCIAL_ID";
	private static final String APP_LAST_LOGIN_TYPE = "APP_LAST_LOGIN_TYPE";
	private static final String COGNITO_DIGIT_TOKEN = "COGNITO_DIGIT_TOKEN";
	private static final String COGNITO_DIGIT_ID = "COGNITO_DIGIT_ID";
	private static final String DIGIT_HEADER_PROVIDER = "DIGIT_HEADER_PROVIDER";
	private static final String DIGIT_HEADER_AUTH = "DIGIT_HEADER_AUTH";
	private static final String PROFILE_IMAGE_PATH = "PROFILE_IMAGE_PATH";
	private static final String GROUP_IMAGE_PATH = "GROUP_IMAGE_PATH";
	private static final String NOTIFICATION_PATH = "NOTIFICATION_PATH";
	private static final String NOTIFICATION_ENABLED= "NOTIFICATION_ENABLED";
	private static final String CALL_PATH = "CALL_PATH";
	private static final String CALL_PROGRESS = "CALL_PROGRESS";
	private static final String IS_LIVE = "IS_LIVE";
	private static final String PREF_SETUP_COMPLETE = "setup_complete";
	private static DataStorage instance = null;


	public static DataStorage getInstance() {
		if (instance == null) {
			instance = new DataStorage();
		}
		return instance;
	}

	private SharedPreferences getSharedPreference() {
		sharedpreferences = MyApplication.getAppContext().getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		return sharedpreferences;
	}

	public boolean getRegisteredFlag() {
		try {
			return getSharedPreference().getBoolean(STR_REGISTERED, false);
		} catch (Exception e) {
		}
		return false;
	}

	public void setRegisteredFlag(boolean flag) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putBoolean(STR_REGISTERED, flag);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

	public String getUsername() {
		try {
			return getSharedPreference().getString(STR_USERNAME, "");
		} catch (Exception e) {
		}
		return "";
	}

	public void setUsername(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(STR_USERNAME, value);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

	public String getUserUUID() {
		try {
			return getSharedPreference().getString(STR_USERNAME_UUID, "");
		} catch (Exception e) {
		}
		return "";
	}

	public void setUserUUID(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(STR_USERNAME_UUID, value);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

	public int getUniqueNotificationId() {
		try {
			return getSharedPreference().getInt(APP_NOTIFICATION_ID, 1000);
		} catch (Exception e) {
		}
		return 0;
	}

	public void setUniqueNotificationId(int value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putInt(APP_NOTIFICATION_ID, value);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

//	public void savePreferences(Context ctx, String key, boolean prefVal) {
//		try {
//			SharedPreferences.Editor editor = getSharedPreference().edit();
//			editor.putBoolean(key, prefVal);
//			editor.commit();
//		} catch (Exception e) {
//		}
//	}
//
//	public boolean readPreferences(Context ctx, String key, boolean prefVal) {
//		boolean retVal = false;
//		try {
//			retVal = getSharedPreference().getBoolean(key, prefVal);
//		} catch (Exception e) {
//		}
//		return retVal;
//	}

	public void storeRegistrationId(Context context, String regId) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(GCM_REG_ID, regId);
			editor.putInt(APP_VERSION, Utils.getInstance().getAppVersion(context));
			editor.commit();
		} catch (Exception e) {
		}
	}


	public String getGcmRegId(Context context) {
		String registrationId = "";
		try {
			registrationId = getSharedPreference().getString(GCM_REG_ID, "");
			int registeredVersion = getSharedPreference().getInt(APP_VERSION, Integer.MIN_VALUE);
			int currentVersion = Utils.getInstance().getAppVersion(context);
			if (registeredVersion != currentVersion) {
				registrationId = "";
			}

		} catch (Exception e) {
		}
		return registrationId;
	}

	public void storeRegistrationIdOnServer(Context context, boolean regId) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putBoolean(GCM_REG_ID_TO_SERVER, regId);
			editor.commit();
		} catch (Exception e) {
		}
	}


	public boolean getGcmRegIdSentToServer(Context context) {
		boolean registrationId = false;
		try {
			registrationId = getSharedPreference().getBoolean(GCM_REG_ID_TO_SERVER, false);

		} catch (Exception e) {
		}
		return registrationId;
	}

	public String getLastLoginType() {
		try {
			String loginType = getSharedPreference().getString(APP_LAST_LOGIN_TYPE, "");
			return loginType ;
		} catch (Exception e) {
		}
		return "";
	}

	public void setLastLoginType(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(APP_LAST_LOGIN_TYPE, value);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}
	public String getAppSocialId() {
		try {
			String socialId = getSharedPreference().getString(APP_SOCIAL_ID, "");
			return socialId ;
		} catch (Exception e) {
		}
		return "";
	}

	public void setAppSocialId(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(APP_SOCIAL_ID, value);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}
	public String getCognitoDigitToken() {
		try {
			String socialId = getSharedPreference().getString(COGNITO_DIGIT_TOKEN, "");
			return socialId ;
		} catch (Exception e) {
		}
		return "";
	}

	public void setCognitoDigitToken(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(COGNITO_DIGIT_TOKEN, value);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

	public String getCognitoDigitId() {
		try {
			String socialId = getSharedPreference().getString(COGNITO_DIGIT_ID, "");
			return socialId ;
		} catch (Exception e) {
		}
		return "";
	}
	public void setCognitoDigitId(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(COGNITO_DIGIT_ID, value);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}
	public void setDigitHeaderProvider(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(DIGIT_HEADER_PROVIDER, value);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}
	public String getDigitHeaderProvider() {
		try {
			String socialId = getSharedPreference().getString(DIGIT_HEADER_PROVIDER, "");
			return socialId ;
		} catch (Exception e) {
		}
		return "";
	}
	public void setDigitHeaderAuth(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(DIGIT_HEADER_AUTH, value);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}
	public String getDigitHeaderAuth() {
		try {
			String socialId = getSharedPreference().getString(DIGIT_HEADER_AUTH, "");
			return socialId ;
		} catch (Exception e) {
		}
		return "";
	}
	public void setGroupImagePath(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(GROUP_IMAGE_PATH, value);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}
	public String getGroupImagePath() {
		try {
			String socialId = getSharedPreference().getString(GROUP_IMAGE_PATH, "");
			return socialId ;
		} catch (Exception e) {
		}
		return "";
	}


	public void setProfileImagePath(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(PROFILE_IMAGE_PATH, value);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}
	public String getProfileImagePath() {
		try {
			String socialId = getSharedPreference().getString(PROFILE_IMAGE_PATH, "");
			return socialId ;
		} catch (Exception e) {
		}
		return "";
	}

	public void setCallPath(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(CALL_PATH, value);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}
	public String getCallPath() {
		try {
			String socialId = getSharedPreference().getString(CALL_PATH, "");
			return socialId ;
		} catch (Exception e) {
		}
		return "";
	}

	public boolean getDestroyLiveFlag() {
		try {
			return getSharedPreference().getBoolean(STR_DESTROY_LIVE, false);
		} catch (Exception e) {
		}
		return false;
	}
	public void setDestroyLiveFlag(boolean flag) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putBoolean(STR_DESTROY_LIVE, flag);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

	public boolean getIsLiveFlag() {
		try {
			return getSharedPreference().getBoolean(IS_LIVE, false);
		} catch (Exception e) {
		}
		return false;
	}
	public void setIsLiveFlag(boolean flag) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putBoolean(IS_LIVE, flag);
			editor.commit();
		} catch (Exception e) {
// retVal = STR_UNKNOWN;
		}
	}
	public void setNotificationPath(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(NOTIFICATION_PATH, value);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}
	public String getNotificationPath() {
		try {
			String socialId = getSharedPreference().getString(NOTIFICATION_PATH, "");
			return socialId ;
		} catch (Exception e) {
		}
		return "";
	}

	public boolean getNotificationEnabled() {
		try {
			return getSharedPreference().getBoolean(NOTIFICATION_ENABLED, true);
		} catch (Exception e) {
		}
		return false;
	}

	public void setNotificationEnabled(boolean flag) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putBoolean(NOTIFICATION_ENABLED, flag);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

	public boolean getCallProgress() {
		try {
			return getSharedPreference().getBoolean(CALL_PROGRESS, false);
		} catch (Exception e) {
		}
		return false;
	}

	public void setCallProgress(boolean flag) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putBoolean(CALL_PROGRESS, flag);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

	public boolean getContactSyncCompleted() {
		try {
			return getSharedPreference().getBoolean(PREF_SETUP_COMPLETE, false);
		} catch (Exception e) {
		}
		return false;
	}

	public void setContactSyncCompleted(boolean flag) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putBoolean(PREF_SETUP_COMPLETE, flag);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

}