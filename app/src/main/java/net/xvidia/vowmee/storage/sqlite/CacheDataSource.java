package net.xvidia.vowmee.storage.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.network.model.SocialAuthFacebook;
import net.xvidia.vowmee.network.model.SocialAuthTwitter;
import net.xvidia.vowmee.network.model.UploadData;

import java.util.ArrayList;
import java.util.Date;

public class CacheDataSource {
    private SQLiteDatabase database;
    private final MySqliteHelper dbHelper;
    private static CacheDataSource INSTANCE = null;// new CachDataSource();

    public static CacheDataSource getInstance() {
        if (INSTANCE == null)
            INSTANCE = new CacheDataSource();
        return INSTANCE;
    }

    private CacheDataSource() {
        dbHelper = MySqliteHelper.getInstance();
    }

    public void open(Context c) throws SQLException {
        try {
            if (database == null) {
                database = dbHelper.getWritableDatabase();
            } else if (!database.isOpen()) {
                database = dbHelper.getWritableDatabase();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public  void close() {
        // Log.d("CachedDataSource", "Closed");
        dbHelper.close();
        database.close();
    }


	/*
     * Facebook DATA
	 */

    public synchronized void saveSigninFacebookData(SocialAuthFacebook settingObj) {
        try {
            if (settingObj == null)
                return;
            ContentValues values = new ContentValues();
            values.put(MySqliteHelper.COLUMN_SOCIAL_ID,
                    settingObj.getUserId());
            values.put(MySqliteHelper.COLUMN_SOCIAL_ACCESS_TOKEN,
                    settingObj.getAccessToken());
            values.put(MySqliteHelper.COLUMN_SOCIAL_EXPIRY,
                    settingObj.getExpiryDate());
            if (getFacebookData(settingObj.getUserId()) != null) {
                database.delete(MySqliteHelper.TABLE_NAME_SOCIAL_FACEBOOK,
                        MySqliteHelper.COLUMN_SOCIAL_ID + "="
                                + settingObj.getUserId(), null);
            }
            database.insert(MySqliteHelper.TABLE_NAME_SOCIAL_FACEBOOK, null, values);
        } catch (Exception e) {
        }
    }

    public synchronized void removeSigninFacebookData() {
        int log = database.delete(MySqliteHelper.TABLE_NAME_SOCIAL_FACEBOOK, null, null);
        Log.i("deleted", "" + log);
    }

    public synchronized void updateSigninFacebookData(SocialAuthFacebook settingObj) {
        try {
            if (settingObj == null)
                return;
            ContentValues values = new ContentValues();
            values.put(MySqliteHelper.COLUMN_SOCIAL_ID,
                    settingObj.getUserId());
            values.put(MySqliteHelper.COLUMN_SOCIAL_ACCESS_TOKEN,
                    settingObj.getAccessToken());
            values.put(MySqliteHelper.COLUMN_SOCIAL_EXPIRY,
                    settingObj.getExpiryDate());
            database.update(MySqliteHelper.TABLE_NAME_SOCIAL_FACEBOOK, values,
                    MySqliteHelper.COLUMN_SOCIAL_ID + "=?",
                    new String[]{settingObj.getUserId()});
        } catch (Exception e) {
        }
    }

    public synchronized SocialAuthFacebook getFacebookData(String userID) {
        SocialAuthFacebook data = null;

        Cursor cursor = null;
        try {
            cursor = database.query(MySqliteHelper.TABLE_NAME_SOCIAL_FACEBOOK, null,
                    MySqliteHelper.COLUMN_SOCIAL_ID + "=?",
                    new String[]{userID}, null, null, null);

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                data = new SocialAuthFacebook();
                data.setUserId(cursor.getString(cursor
                        .getColumnIndex(MySqliteHelper.COLUMN_SOCIAL_ID)));
                data.setAccessToken(cursor.getString(cursor
                        .getColumnIndex(MySqliteHelper.COLUMN_SOCIAL_ACCESS_TOKEN)));
                data.setExpiryDate(cursor.getString(cursor
                        .getColumnIndex(MySqliteHelper.COLUMN_SOCIAL_EXPIRY)));
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            if (cursor != null)
                cursor.close();
        }
        return data;
    }

	/*
	 * TWITTER DATA
	 */

    public synchronized void saveSigninTwitterData(SocialAuthTwitter settingObj) {
        try {
            if (settingObj == null)
                return;
            ContentValues values = new ContentValues();
            values.put(MySqliteHelper.COLUMN_SOCIAL_ID,
                    settingObj.getUserId());
            values.put(MySqliteHelper.COLUMN_SOCIAL_ACCESS_TOKEN,
                    settingObj.getAccessToken());
            values.put(MySqliteHelper.COLUMN_SOCIAL_SECRET_TOKEN,
                    settingObj.getAccessSecret());
            if (getTwitterData("" + settingObj.getUserId()) != null) {
                database.delete(MySqliteHelper.TABLE_NAME_SOCIAL_TWITTER,
                        MySqliteHelper.COLUMN_SOCIAL_ID + "="
                                + settingObj.getUserId(), null);
            }
            database.insert(MySqliteHelper.TABLE_NAME_SOCIAL_TWITTER, null, values);
        } catch (Exception e) {
        }
    }

    public synchronized void removeSigninTwitterData() {
        database.delete(MySqliteHelper.TABLE_NAME_SOCIAL_TWITTER, null, null);
    }

    public synchronized void updateSigninTwitterData(SocialAuthTwitter settingObj) {
        try {
            if (settingObj == null)
                return;
            ContentValues values = new ContentValues();
            values.put(MySqliteHelper.COLUMN_SOCIAL_ID,
                    settingObj.getUserId());
            values.put(MySqliteHelper.COLUMN_SOCIAL_ACCESS_TOKEN,
                    settingObj.getAccessToken());
            values.put(MySqliteHelper.COLUMN_SOCIAL_SECRET_TOKEN,
                    settingObj.getAccessSecret());
            database.update(MySqliteHelper.TABLE_NAME_SOCIAL_TWITTER, values,
                    MySqliteHelper.COLUMN_SOCIAL_ID + "=?",
                    new String[]{"" + settingObj.getUserId()});
        } catch (Exception e) {
        }
    }

    public synchronized SocialAuthTwitter getTwitterData(String userID) {
        SocialAuthTwitter data = null;

        Cursor cursor = null;
        try {
            cursor = database.query(MySqliteHelper.TABLE_NAME_SOCIAL_TWITTER, null,
                    MySqliteHelper.COLUMN_SOCIAL_ID + "=?",
                    new String[]{userID}, null, null, null);

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                data = new SocialAuthTwitter();
                data.setUserId(Utils.getInstance().getLongValueOfString(cursor.getString(cursor
                        .getColumnIndex(MySqliteHelper.COLUMN_SOCIAL_ID))));
                data.setAccessToken(cursor.getString(cursor
                        .getColumnIndex(MySqliteHelper.COLUMN_SOCIAL_ACCESS_TOKEN)));
                data.setAccessSecret(cursor.getString(cursor
                        .getColumnIndex(MySqliteHelper.COLUMN_SOCIAL_SECRET_TOKEN)));
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            if (cursor != null)
                cursor.close();
        }
        return data;
    }


		/*
	 * Upload DATA
	 */

    public synchronized void saveUploadData(UploadData data) {
        try {
            if (data == null)
                return;
            Date today = new Date();

            int reshare = (data.getmReshare() == true) ? 1 : 0;
            int recorded = (data.getRecordedMoment() == true) ? 1 : 0;
            ContentValues values = new ContentValues();
            values.put(MySqliteHelper.COLUMN_MOMENT_UUID,
                    data.getMomentUuid());
            values.put(MySqliteHelper.COLUMN_SOCIAL_EXPIRY,
                    today.getTime());
            values.put(MySqliteHelper.COLUMN_ADD_MOMENT_RESHARE,
                    reshare);
            values.put(MySqliteHelper.COLUMN_ADD_MOMENT_RECORDED,
                    recorded);
            values.put(MySqliteHelper.COLUMN_ADD_MOMENT_PATH,
                    data.getMomentPath());
            values.put(MySqliteHelper.COLUMN_ADD_MOMENT_COMPRESSED_PATH,
                    data.getMomentCompressedPath());
            values.put(MySqliteHelper.COLUMN_ADD_MOMENT_JSON,
                    data.getMomentShareText());
            if (getUploadData(data.getMomentUuid()) != null) {
                database.delete(MySqliteHelper.TABLE_NAME_UPLOADS,
                        MySqliteHelper.COLUMN_MOMENT_UUID + "="
                                + data.getMomentUuid(), null);
            }
            database.insert(MySqliteHelper.TABLE_NAME_UPLOADS, null, values);
        } catch (Exception e) {
        }
    }

    public synchronized void removeUploadData() {
        int log = database.delete(MySqliteHelper.TABLE_NAME_UPLOADS, null, null);
        Log.i("deleted", "" + log);
    }
    public synchronized void removeUploadDataByName(String progName) {
        try {
            database.delete(MySqliteHelper.TABLE_NAME_UPLOADS,
                    MySqliteHelper.COLUMN_MOMENT_UUID + "=?",
                    new String[] { progName });
        } catch (Exception e) {

        }

    }
    public synchronized void updateUploadData(UploadData data) {
        try {
            if (data == null)
                return;
            Date today = new Date();
            int reshare = (data.getmReshare() == true) ? 1 : 0;
            int recorded = (data.getRecordedMoment() == true) ? 1 : 0;
            ContentValues values = new ContentValues();
            values.put(MySqliteHelper.COLUMN_MOMENT_UUID,
                    data.getMomentUuid());
            values.put(MySqliteHelper.COLUMN_SOCIAL_EXPIRY,
                    today.getTime());
            values.put(MySqliteHelper.COLUMN_ADD_MOMENT_RESHARE,
                    reshare);
            values.put(MySqliteHelper.COLUMN_ADD_MOMENT_RECORDED,
                    recorded);
            values.put(MySqliteHelper.COLUMN_ADD_MOMENT_PATH,
                    data.getMomentPath());
            values.put(MySqliteHelper.COLUMN_ADD_MOMENT_COMPRESSED_PATH,
                    data.getMomentCompressedPath());
            values.put(MySqliteHelper.COLUMN_ADD_MOMENT_JSON,
                    data.getMomentShareText());
            if (getUploadData(data.getMomentUuid()) != null) {
                database.delete(MySqliteHelper.TABLE_NAME_UPLOADS,
                        MySqliteHelper.COLUMN_MOMENT_UUID + "="
                                + data.getMomentUuid(), null);
            }
            database.update(MySqliteHelper.TABLE_NAME_UPLOADS, values,
                    MySqliteHelper.COLUMN_MOMENT_UUID + "=?",
                    new String[]{data.getMomentUuid()});
        } catch (Exception e) {
        }
    }

    public synchronized UploadData getUploadData(String userID) {
        UploadData data = null;

        Cursor cursor = null;
        try {
            cursor = database.query(MySqliteHelper.TABLE_NAME_UPLOADS, null,
                    MySqliteHelper.COLUMN_MOMENT_UUID + "=?",
                    new String[]{userID}, null, null, null);

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                data = new UploadData();
                data.setMomentUuid(cursor.getString(cursor.getColumnIndex(MySqliteHelper.COLUMN_MOMENT_UUID)));
                data.setMomentShareText(cursor.getString(cursor.getColumnIndex(MySqliteHelper.COLUMN_ADD_MOMENT_JSON)));
                data.setMomentPath(cursor.getString(cursor.getColumnIndex(MySqliteHelper.COLUMN_ADD_MOMENT_PATH)));
                data.setMomentCompressedPath(cursor.getString(cursor.getColumnIndex(MySqliteHelper.COLUMN_ADD_MOMENT_COMPRESSED_PATH)));
                data.setmReshare(cursor.getInt(cursor.getColumnIndex(MySqliteHelper.COLUMN_ADD_MOMENT_RESHARE)) == 1);
                data.setRecordedMoment(cursor.getInt(cursor.getColumnIndex(MySqliteHelper.COLUMN_ADD_MOMENT_RECORDED)) == 1);
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            if (cursor != null)
                cursor.close();
        }
        return data;
    }

    public ArrayList<UploadData> getAllUploadData() {

        ArrayList<UploadData> uploadDataList = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = database.query(MySqliteHelper.TABLE_NAME_UPLOADS, null, null, null, null, null,MySqliteHelper.COLUMN_SOCIAL_EXPIRY+" ASC");

            if (cursor != null && cursor.getCount() > 0) {
                UploadData data = null;
                cursor.moveToPosition(0);
                for (int i = cursor.getPosition(); i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    data = new UploadData();
                    data.setMomentUuid(cursor.getString(cursor.getColumnIndex(MySqliteHelper.COLUMN_MOMENT_UUID)));
                    data.setMomentShareText(cursor.getString(cursor.getColumnIndex(MySqliteHelper.COLUMN_ADD_MOMENT_JSON)));
                    data.setMomentPath(cursor.getString(cursor.getColumnIndex(MySqliteHelper.COLUMN_ADD_MOMENT_PATH)));
                    data.setMomentCompressedPath(cursor.getString(cursor.getColumnIndex(MySqliteHelper.COLUMN_ADD_MOMENT_COMPRESSED_PATH)));
                    data.setmReshare(cursor.getInt(cursor.getColumnIndex(MySqliteHelper.COLUMN_ADD_MOMENT_RESHARE)) == 1);
                    data.setRecordedMoment(cursor.getInt(cursor.getColumnIndex(MySqliteHelper.COLUMN_ADD_MOMENT_RECORDED)) == 1);
                    uploadDataList.add(data);
                }
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            if (cursor != null)
                cursor.close();
        }
        return uploadDataList;
    }
}
