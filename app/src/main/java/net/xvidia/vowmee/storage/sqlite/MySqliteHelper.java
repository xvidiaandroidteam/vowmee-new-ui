package net.xvidia.vowmee.storage.sqlite;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import net.xvidia.vowmee.MyApplication;

public class MySqliteHelper extends SQLiteOpenHelper {
	/*TABLES*/
	public static final String TABLE_NAME_SOCIAL_FACEBOOK = "SOCIAL_FACEBOOK";
	public static final String TABLE_NAME_SOCIAL_TWITTER = "SOCIAL_TWITTER";
	public static final String TABLE_NAME_SOCIAL_VOWMEE = "SOCIAL_VOWMEE";
	public static final String TABLE_NAME_CONTACTS = "VOWMEE_CONTACTS";
	public static final String TABLE_NAME_UPLOADS = "VOWMEE_UPLOADS";

	/*COLUMNS*/
	public static final String COLUMN_SOCIAL_ID = "SocialId";
	public static final String COLUMN_SOCIAL_ACCESS_TOKEN = "AccessToken";
	public static final String COLUMN_SOCIAL_SECRET_TOKEN = "SecretToken";
	public static final String COLUMN_SOCIAL_EXPIRY = "ExpiryDate";
	public static final String COLUMN_VOWMEE_USERNAME = "Username";
	public static final String COLUMN_VOWMEE_PASSWORD = "Password";
	public static final String COLUMN_CONTACT_ID = "ContactId";
	public static final String COLUMN_CONTACT_DISPLAY_NAME = "ContactName";
	public static final String COLUMN_CONTACT_PHONENUMBER = "ContactNo";
	public static final String COLUMN_CONTACT_VOWMEE = "VowmeeInstalled";
	public static final String COLUMN_MOMENT_UUID = "MomentUuid";
	public static final String COLUMN_ADD_MOMENT_JSON= "MomentJson";
	public static final String COLUMN_ADD_MOMENT_RESHARE= "MomentReshare";
	public static final String COLUMN_ADD_MOMENT_PATH= "MomentPath";
	public static final String COLUMN_ADD_MOMENT_COMPRESSED_PATH= "MomentCompressedPath";
	public static final String COLUMN_ADD_MOMENT_RECORDED= "MomentRecorded";

	private static final String DATABASE_NAME = "vowmee.db";
	private static final int DATABASE_VERSION =2;

	private static final String TYPE_TEXT = " TEXT";
	private static final String TYPE_INTEGER = " INTEGER";
	private static final String COMMA_SEP = ",";
	/* Caching table create sql statement */
	final String TABLE_CREATE_SOCIAL_FACEBOOK = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_NAME_SOCIAL_FACEBOOK + "(" + COLUMN_SOCIAL_ID
			+ TYPE_TEXT+" PRIMARY KEY," + COLUMN_SOCIAL_ACCESS_TOKEN +  TYPE_TEXT + COMMA_SEP
			+ COLUMN_SOCIAL_EXPIRY + TYPE_TEXT+")";
	final String TABLE_CREATE_SOCIAL_TWITTER = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_NAME_SOCIAL_TWITTER + "(" + COLUMN_SOCIAL_ID
			+ TYPE_TEXT+" PRIMARY KEY," + COLUMN_SOCIAL_ACCESS_TOKEN + TYPE_TEXT + COMMA_SEP
			+ COLUMN_SOCIAL_SECRET_TOKEN + TYPE_TEXT+")";
	final String TABLE_CREATE_SOCIAL_VOWMEE = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_NAME_SOCIAL_VOWMEE + "(" + COLUMN_VOWMEE_USERNAME
			+ TYPE_TEXT+" PRIMARY KEY," + COLUMN_VOWMEE_PASSWORD + TYPE_TEXT+")";

	final String TABLE_CREATE_CONTACTS = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_NAME_CONTACTS + "(" + COLUMN_CONTACT_ID
			+ TYPE_TEXT+" PRIMARY KEY,"+ COLUMN_CONTACT_DISPLAY_NAME + TYPE_TEXT + COMMA_SEP
			+ COLUMN_CONTACT_PHONENUMBER + TYPE_TEXT + COMMA_SEP
			+ COLUMN_CONTACT_VOWMEE + TYPE_TEXT+")";
	final String TABLE_CREATE_UPLOADS= "CREATE TABLE IF NOT EXISTS "
			+ TABLE_NAME_UPLOADS + "(" + COLUMN_MOMENT_UUID
			+ TYPE_TEXT+" PRIMARY KEY,"
			+ COLUMN_SOCIAL_EXPIRY+ TYPE_INTEGER + COMMA_SEP
			+ COLUMN_ADD_MOMENT_RESHARE + TYPE_INTEGER + COMMA_SEP
			+ COLUMN_ADD_MOMENT_RECORDED + TYPE_INTEGER + COMMA_SEP
			+ COLUMN_ADD_MOMENT_PATH + TYPE_TEXT + COMMA_SEP
			+ COLUMN_ADD_MOMENT_COMPRESSED_PATH + TYPE_TEXT + COMMA_SEP
			+ COLUMN_ADD_MOMENT_JSON + TYPE_TEXT+")";

	private static MySqliteHelper INSTANCE = null;// new
													// MySqliteHelper(MyApplication.getAppContext());

	public static MySqliteHelper getInstance() {
		if (INSTANCE == null)
			INSTANCE = new MySqliteHelper(MyApplication.getAppContext());
		return INSTANCE;
	}

	private MySqliteHelper(Context cntxt) {
		super(cntxt, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		try {
			database.execSQL(TABLE_CREATE_SOCIAL_FACEBOOK);
			database.execSQL(TABLE_CREATE_SOCIAL_TWITTER);
			database.execSQL(TABLE_CREATE_SOCIAL_VOWMEE);
			database.execSQL(TABLE_CREATE_CONTACTS);
			database.execSQL(TABLE_CREATE_UPLOADS);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
//		Log.w(MySqliteHelper.class.getName(),
//				"Upgrading database from version " + oldVersion + " to "
//						+ newVersion + ", which will destroy all old data");
		try{
			switch(oldVersion) {
				case 1:
					database.execSQL(TABLE_CREATE_UPLOADS);
			/*	case 2:
					database.execSQL(TABLE_CREATE_INTERVAL_TIMER_PROGRAM);
					database.execSQL(TABLE_CREATE_INTERVAL_TIMER_ROUND);
				case 3:
					database.execSQL(TABLE_CREATE_BLE_DEVICE);
				case 4:
					database.execSQL(TABLE_CREATE_HEART_RATE_DATA);
				case 5:
					database.execSQL("ALTER TABLE " + TABLE_CREATE_INTERVAL_TIMER_ROUND + " ADD COLUMN " + COLUMN_PROGRAM_MEDIA + " TEXT");
					database.execSQL("ALTER TABLE " + TABLE_CREATE_INTERVAL_TIMER_ROUND + " ADD COLUMN " + COLUMN_PROGRAM_INTERVAL_ROUND_NAME + " TEXT");
*/
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
//		onCreate(db);
	}
}
