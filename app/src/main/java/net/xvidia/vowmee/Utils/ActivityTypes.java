package net.xvidia.vowmee.Utils;

import net.xvidia.vowmee.network.model.ActivityLog;
import net.xvidia.vowmee.storage.DataStorage;

/**
 * Created by Ravi_office on 10-Jan-16.
 */
public class ActivityTypes {

    public final static String SEND_FRIEND_REQUEST = "SEND_FRIEND_REQUEST";
    public final static String ACCEPT_FRIEND_REQUEST = "ACCEPT_FRIEND_REQUEST";
    public final static String ADD_GROUP_MEMBER = "ADD_GROUP_MEMBER";
    public final static String DELETE_GROUP_MEMBER = "DELETE_GROUP_MEMBER";
    public final static String CREATE_GROUP = "CREATE_GROUP";
    public final static String DELETE_GROUP = "DELETE_GROUP";
    public final static String EDIT_GROUP = "EDIT_GROUP";
    public final static String SHARE_NOTIFICATION = "SHARE_NOTIFICATION";
    public final static String IS_FRIEND_WITH = "IS_FRIEND_WITH";
    public final static String IS_FOLLOWING = "IS_FOLLOWING";
    public final static String LIKE_NOTIFICATION = "LIKE_NOTIFICATION";
    public final static String COMMENT_NOTIFICATION = "COMMENT_NOTIFICATION";
    public final static String POST_NOTIFICATION = "POST_NOTIFICATION";
    public final static String LIVE_POST_NOTIFICATION = "LIVE_POST_NOTIFICATION";
    public final static String LIVE_INVALID = "INVALID_SESSION_ID";
    public final static String LIVE_CLOSED = "CLOSED";
    public final static String LIVE_CREATED = "CREATED";
    public final static String LIVE_SUBSCRIBED = "SUBSCRIBED";

    public final static String POST_MOMENT_IN_GROUP = "POST_MOMENT_IN_GROUP";
    public final static String DELETE_MOMENT_FROM_GROUP = "DELETE_MOMENT_FROM_GROUP";
    public final static String FOLLOW_GROUP = "FOLLOW_GROUP";
    public final static String UNFOLLOW_GROUP = "UNFOLLOW_GROUP";
    /*CAll*/

    public final static String CALL_INITIATED = "CALL_INITIATED";
    public final static String CALL_ACCEPTED = "CALL_ACCEPTED";
    public final static String CALL_REJECTED = "CALL_REJECTED";
    public final static String CALL_ENDED = "CALL_ENDED";

    public static String getActivityMessageString(ActivityLog activityLog) {
        String activityMessage = "";
        String username = DataStorage.getInstance().getUsername();
        if (activityLog == null)
            return "";
        switch (activityLog.getActivityType()) {
            case SEND_FRIEND_REQUEST:

                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage = "";//"You have send a friend request to @" + activityLog.getReceiverName() + "@";
                } else {
                    if (username.equalsIgnoreCase(activityLog.getReceiverId())) {
                        activityMessage = "@" + activityLog.getInitiaterName() + "@ has send you a friend request";
                    } else {
                        activityMessage = "@" + activityLog.getInitiaterName() + "@ has send a friend request to @" + activityLog.getReceiverName() + "@";
                    }
                }
                break;
            case ACCEPT_FRIEND_REQUEST:
                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage = "";//"You have accepted friend request of @" + activityLog.getReceiverName() + "@";
                } else {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ has accepted  the friend request of @" + activityLog.getReceiverName() + "@";
                }
                break;
            case IS_FRIEND_WITH:
                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage= "";// "You are now friends with @" + activityLog.getReceiverName() + "@";
                } else if (username.equalsIgnoreCase(activityLog.getReceiverId())) {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ is now friends with you.";
                } else {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ is now friends with @" + activityLog.getReceiverName() + "@";
                }
                break;
            case IS_FOLLOWING:
                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage = "";// "You are now following @" + activityLog.getReceiverName() + "@";
                } else if (username.equalsIgnoreCase(activityLog.getReceiverId())) {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ is now following you.";
                } else {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ is now following @" + activityLog.getReceiverName() + "@";
                }
                break;
            case LIKE_NOTIFICATION:
                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    if (activityLog.getInitiaterId().equalsIgnoreCase(activityLog.getReceiverId()) || (activityLog.getReceiverId().isEmpty())) {
                        activityMessage= "";// "You have liked your own post " + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption() + "[");
                    } else {
                        activityMessage = "You have liked @" + activityLog.getReceiverName() + "@'s post " + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption() + "[");
                    }
                    activityMessage= "";//
                } else if (username.equalsIgnoreCase(activityLog.getReceiverId())) {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ has liked your post " + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption() + "[");
                } else {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ has liked @" + activityLog.getReceiverName() + "@'s post " + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption() + "[");
                }
                break;
            case COMMENT_NOTIFICATION:
                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    if (activityLog.getInitiaterId().equalsIgnoreCase(activityLog.getReceiverId()) || (activityLog.getReceiverId().isEmpty())) {
                        activityMessage = "You have commented on your own post " + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption() + "[");
                    } else {
                        activityMessage = "You have commented on @" + activityLog.getReceiverName() + "@'s post " + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption() + "[");
                    }
                    activityMessage= "";//
                } else if (username.equalsIgnoreCase(activityLog.getReceiverId())) {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ has commented on your post " + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption() + "[");
                } else {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ has commented on @" + activityLog.getReceiverName() + "@'s post " + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption() + "[");
                }
                break;
            case POST_NOTIFICATION:
                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage= "";//"You have posted a new moment " + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption() + "[");
                } else {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ has posted a new moment " + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption() + "[");
                }
                break;
            case LIVE_POST_NOTIFICATION:
                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage= "";// "You have posted a new live moment " + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption() + "[");
                } else {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ is broadcasting live now \n" + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption() + "[");
                }
                break;


            case SHARE_NOTIFICATION:
                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage= "";//"You have shared a new moment " + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption() + "[");
                } else {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ has shared a new moment \n" + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption() + "[");
                }
                break;
            case CREATE_GROUP:
                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage= "";// "You have created a new group " + ((activityLog.getGroupName() == null) ? "" : (activityLog.getGroupName().trim().isEmpty()) ? "" : "[" + activityLog.getGroupName() + "[");
                } else {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ has created a new group \n" + ((activityLog.getGroupName() == null) ? "" : (activityLog.getGroupName().trim().isEmpty()) ? "" : "[" + activityLog.getGroupName() + "[");
                }
                break;
            case DELETE_GROUP:
                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage= "";//"You have deleted group " + ((activityLog.getGroupName() == null) ? "" : (activityLog.getGroupName().trim().isEmpty()) ? "" : "[" + activityLog.getGroupName() + "[");
                } else {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ has deleted group \n" + ((activityLog.getGroupName() == null) ? "" : (activityLog.getGroupName().trim().isEmpty()) ? "" : "[" + activityLog.getGroupName() + "[");
                }

                break;
            case EDIT_GROUP:
                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage= "";//"You have edited group " + ((activityLog.getGroupName() == null) ? "" : (activityLog.getGroupName().trim().isEmpty()) ? "" : "[" + activityLog.getGroupName() + "[");
                } else {
                    activityMessage = "@" + activityLog.getInitiaterName() + " has edited group \n" + ((activityLog.getGroupName() == null) ? "" : (activityLog.getGroupName().trim().isEmpty()) ? "" : "[" + activityLog.getGroupName() + "[");
                }

                break;
            case FOLLOW_GROUP:
                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage= "";//"You are now following group " + ((activityLog.getGroupName() == null) ? "" : (activityLog.getGroupName().trim().isEmpty()) ? "" : "[" + activityLog.getGroupName() + "[");
                } else {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ is now following group \n" + ((activityLog.getGroupName() == null) ? "" : (activityLog.getGroupName().trim().isEmpty()) ? "" : "[" + activityLog.getGroupName() + "[");
                }
            case UNFOLLOW_GROUP:
                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage= "";// "You have now stopped following group " + ((activityLog.getGroupName() == null) ? "" : (activityLog.getGroupName().trim().isEmpty()) ? "" : "[" + activityLog.getGroupName() + "[");
                } else {
                    activityMessage= "";// "@" + activityLog.getInitiaterName() + "@ has now stopped following group \n" + ((activityLog.getGroupName() == null) ? "" : (activityLog.getGroupName().trim().isEmpty()) ? "" : "[" + activityLog.getGroupName() + "[");
                }

                break;
            case POST_MOMENT_IN_GROUP:
                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage= "";//"You have posted a new moment " + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption()) + "M$ in group " + ((activityLog.getGroupName() == null) ? "" : (activityLog.getGroupName().trim().isEmpty()) ? "" : "[" + activityLog.getGroupName() + "[");
                } else {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ has posted a new moment a new moment " + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption()) + "M$ in group " + ((activityLog.getGroupName() == null) ? "" : (activityLog.getGroupName().trim().isEmpty()) ? "" : "[" + activityLog.getGroupName() + "[");
                }
                break;
            case CALL_INITIATED:
                if (username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage= "";// "You have posted a new moment " + ((activityLog.getMomentCaption() == null) ? "" : (activityLog.getMomentCaption().trim().isEmpty()) ? "" : "[" + activityLog.getMomentCaption()) + "M$ in group " + ((activityLog.getGroupName() == null) ? "" : (activityLog.getGroupName().trim().isEmpty()) ? "" : "[" + activityLog.getGroupName() + "[");
                } else {
                    activityMessage = "@" + activityLog.getInitiaterName() + "@ is calling you ";
                }
                break;
            case ADD_GROUP_MEMBER:
                if (!username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage = "@" + activityLog.getInitiaterName() + " has added " + "@" + activityLog.getMemberName() + " to group \n" + ((activityLog.getGroupName() == null) ? "" : (activityLog.getGroupName().trim().isEmpty()) ? "" : "[" + activityLog.getGroupName() + "[");
                }
                break;
            case DELETE_GROUP_MEMBER:
                if (!username.equalsIgnoreCase(activityLog.getInitiaterId())) {
                    activityMessage = "@" + activityLog.getInitiaterName() + " has deleted " + "@" + activityLog.getMemberName() + " from the group \n" + ((activityLog.getGroupName() == null) ? "" : (activityLog.getGroupName().trim().isEmpty()) ? "" : "[" + activityLog.getGroupName() + "[");
                }
                break;
        }
        return activityMessage;
    }

}
