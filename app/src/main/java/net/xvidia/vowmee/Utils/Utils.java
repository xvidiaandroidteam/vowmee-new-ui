package net.xvidia.vowmee.Utils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobile.AWSConfiguration;
import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.amazonaws.mobile.user.signin.SignInManager;
import com.amazonaws.mobile.user.signin.SignInProvider;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.network.model.Register;
import net.xvidia.vowmee.storage.DataStorage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by Ravi_office on 05-Nov-15.
 */
public class Utils {

    private static  Utils instance = null;
    private UserFileManager userFileManager;
    private Register registerObj;
    private Moment momentObj;
    private static String basePath;
    public static int SHORT_TOAST = 0;
    public static int LONG_TOAST = 1;
    private static int screenWidthPixels;

    public Moment getMomentObj() {
        return momentObj;
    }

    public void setMomentObj(Moment momentObj) {
        this.momentObj = momentObj;
    }

    public static  Utils getInstance() {
        if (instance == null) {
            instance = new Utils();
            basePath = MyApplication.getAppContext().getFilesDir().getAbsolutePath();

            Configuration configuration = MyApplication.getAppContext().getResources().getConfiguration();
            int screenWidthDp = configuration.screenWidthDp;
            screenWidthPixels= Utils.getInstance().convertDpToPixel(screenWidthDp, MyApplication.getAppContext());
        }
        return instance;
    }

    public UserFileManager getUserFileManager() {
        return userFileManager;
    }

    public void setUserFileManager(UserFileManager userFileManager) {
        this.userFileManager = userFileManager;
    }

    public void initialiseAmazon(){
        SignInManager signInManager = new SignInManager(MyApplication.getAppContext());
        SignInProvider provider = signInManager.getPreviouslySignedInProvider();
        if(provider != null) {
            String bucket = AWSConfiguration.AMAZON_S3_USER_FILES_BUCKET;
            String prefix = "public/";
            AWSMobileClient.defaultMobileClient()
                    .createUserFileManager(bucket,
                            prefix,
                            new UserFileManager.BuilderResultHandler() {

                                @Override
                                public void onComplete(final UserFileManager userFileManager) {
                                    setUserFileManager(userFileManager);

                                }
                            });
        }
    }

    public String getLocalContentPath(String file){
        String bucket = AWSConfiguration.AMAZON_S3_USER_FILES_BUCKET;
        String localDirPrefix ="/public";
        String baseContentPath = basePath + "/s3_" + bucket + localDirPrefix+"/content/"+file;
//        final File prefixPathFile = new File(baseContentPath);
//        if (prefixPathFile.exists()) {
//            baseContentPath = baseContentPath+"/content";
//        }
        return  baseContentPath;
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param res A value in dimen resource (
     * @return int equivalent
     */
    public int convertDimenToInt(int res){
        Resources resources = MyApplication.getAppContext().getResources();
        int px = resources.getDimensionPixelSize(res);
        return px;
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public int convertDpToPixel(int dp, Context context){
        Resources resources = MyApplication.getAppContext().getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = (int)(dp * (metrics.densityDpi / 160f));
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }



    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @author paulburke
     */
    @SuppressLint("NewApi")
    public String getPath(final Context context, final Uri uri,boolean imageFile) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        String Path = "";
        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    Path = Environment.getExternalStorageDirectory() + "/" + split[1];
                    if(!checkVideoValidSize(Path)) {
                        Path = copyDirectoryOneLocationToAnotherLocation(Path);
                    }else{
                        Path="";
                    }
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                Path = getDataColumn(context, contentUri, null, null);
                if(!checkVideoValidSize(Path)) {
                    Path = copyDirectoryOneLocationToAnotherLocation(Path);
                }else{
                    Path="";
                }
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)&& imageFile) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                }else if ("video".equals(type)&& !imageFile) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
//                } else if ("audio".equals(type)) {
//                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                Path = getDataColumn(context, contentUri, selection, selectionArgs);
                if(!checkVideoValidSize(Path)) {
                    Path = copyDirectoryOneLocationToAnotherLocation(Path);
                }else{
                    Path="";
                }
            }else if ("content".equalsIgnoreCase(uri.getScheme())) {
                // Return the remote address
                String pathContent = "";
                pathContent = isGooglePhotosUri(context, uri);
                if (pathContent.isEmpty()) {
                    pathContent = getDataColumn(context, uri, null, null);
                    if(!checkVideoValidSize(pathContent)) {
                        pathContent = copyDirectoryOneLocationToAnotherLocation(pathContent);
                    }else{
                        pathContent="";
                    }
                }
                Path = pathContent;
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            String pathContent = "";
            pathContent = isGooglePhotosUri(context, uri);
            if (pathContent.isEmpty()) {
                pathContent = getDataColumn(context, uri, null, null);
                if(!checkVideoValidSize(pathContent)) {
                    pathContent = copyDirectoryOneLocationToAnotherLocation(pathContent);
                }else{
                    pathContent="";
                }
//                pathContent = copyDirectoryOneLocationToAnotherLocation(pathContent);
            }
            Path = pathContent;
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            Path = uri.getPath();
            if(!checkVideoValidSize(Path)) {
                Path = copyDirectoryOneLocationToAnotherLocation(Path);
            }else{
                Path="";
            }
        }
        if(Path.isEmpty())
            showAlert(context);
        return Path;
    }
    private boolean checkVideoValidSize(String path){
        boolean flag = false;
        if(path !=null && !path.isEmpty()) {
            File file = new File(path);
            if (file.exists()) {
                long uploadVideoSize;
                uploadVideoSize = file.length();
                if(uploadVideoSize>0) {
                    uploadVideoSize = uploadVideoSize/1024; // kb
                    uploadVideoSize = uploadVideoSize/1024;//mb
//                    compressedVideoSize = intBitrate * 8 * intDuration;
                    if (uploadVideoSize > 20) {
                        flag = true;
                    }
                }
            }
        }
        return flag;
    }
    private void showAlert(final Context mContext) {
        try {
            ((AppCompatActivity)mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(mContext)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Large file")
                            .setMessage(MyApplication.getAppContext().getString(R.string.error_large_file_upload))
                            .setNegativeButton(android.R.string.ok,null)
                            .show();
                }
            });

        }catch(WindowManager.BadTokenException e){

        }catch (Exception e){}
    }
    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {
        try {
            Cursor cursor = null;
            final String column = "_data";
            final String[] projection = {
                    column
            };

            try {
                cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                        null);
                if (cursor != null && cursor.moveToFirst()) {
                    final int index = cursor.getColumnIndexOrThrow(column);
                    return cursor.getString(index);
                }
            } finally {
                if (cursor != null)
                    cursor.close();
            }
        } catch (IllegalArgumentException e) {

        } catch (Exception e) {

        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google media.
     */
    public String isGooglePhotosUri(Context mContext,Uri uri) {
        String path="";

        String authority = uri.getAuthority();
        if("com.google.android.apps.docs.storage".equals(authority)){
            try {
                path = savefile(mContext,uri);
            } catch (IOException e) {
                e.printStackTrace();
            }catch(Exception e){

            }
        }
        return path ;
    }

    public String savefile(Context mContext, Uri sourceuri )
            throws IOException {
        // String sourceFilename = sourceuri.getPath();

        int originalSize = 0;
        String destination="";
        InputStream input = null;
        try {
            input = mContext.getContentResolver().openInputStream(sourceuri);

//            Log.Logger().finest("input in profileview Activity" + input);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return destination;
        }
        String path = Environment.getExternalStorageDirectory() + AppConsatants.FILE_VOWMEE;
        File dir = new File(path);
        if(!dir.exists()){
            dir.mkdirs();
        }
        destination = path +Utils.getInstance().generateUUIDStringDateTime()+ AppConsatants.FILE_EXTENSION_MP4;
        try {
            originalSize = input.available();

            BufferedInputStream bis = null;
            BufferedOutputStream bos = null;
            try {
                bis = new BufferedInputStream(input);
                bos = new BufferedOutputStream(new FileOutputStream(
                        destination, false));
                byte[] buf = new byte[originalSize];
                bis.read(buf);
                do {
                    bos.write(buf);
                } while (bis.read(buf) != -1);

            } catch (IOException e) {
                e.printStackTrace();
                return "";

            }

        } catch (NullPointerException e1) {
            e1.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }

        /*
         * String[] cmd = new String[] { "logcat", "-f", GridViewDemo_LOGPATH,
         * "-v", "time", "ActivityManager:W", "myapp:D" };
         *
         * Runtime.getRuntime().exec(cmd);
         */

        return destination;
    }

    public boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    public String getTimeStampDDMMYYYY(long time) {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "dd-MM-yyyy hh:mm", Locale.ENGLISH);
        Date now = new Date(time);
//        now.getSeconds();
        String timeString ="" + formatter.format(now);
//        timeString = timeString+"-"+now.getSeconds();
        return timeString;
//        return timeString;

    }



    public String getLocalThumbnailImagePath(String filename){
//        ContextWrapper cw = new ContextWrapper(MyApplication.getAppContext());
//        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
//        File mypath=new File(directory,"profile_thumbnail.jpg");
        if(filename ==null ||filename.isEmpty())
            return "";
        String path = Environment.getExternalStorageDirectory() + AppConsatants.FILE_EXTENSION_IMAGE;

        File dir = new File(path);
        if(!dir.exists()){
            dir.mkdirs();
        }
        if(filename.contains(AppConsatants.FILE_EXTENSION_JPG)&&!filename.contains(AppConsatants.THUMBNAIL_EXTENSION_JPG)){
            filename = filename.replace(AppConsatants.FILE_EXTENSION_JPG,AppConsatants.THUMBNAIL_EXTENSION_JPG);
        }
        if(!filename.contains(AppConsatants.THUMBNAIL_EXTENSION_JPG))
            path = path +filename+AppConsatants.THUMBNAIL_EXTENSION_JPG;
        else
            path = path +filename;
        return path;
    }

    public String getLocalMomentThumbnailImagePath(){
        ContextWrapper cw = new ContextWrapper(MyApplication.getAppContext());
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        File mypath=new File(directory,"moment_thumbnail.jpg");
        return mypath.getAbsolutePath();
    }

    public void storeImage(Bitmap image,boolean thumbnail,String filename) {
        if(image == null || (filename!=null &&filename.isEmpty()))
            return;
        File pictureFile = getOutputMediaFile(thumbnail,filename);
        if (pictureFile == null) {
//            Log.d("Save FIle",
//                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
//            image.compress(Bitmap.CompressFormat.PNG, 100, fos);
            image.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            fos.close();

//            compressImage(pictureFile.getAbsolutePath());
        } catch (FileNotFoundException e) {
            Log.d("Save FIle", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("Save FIle", "Error accessing file: " + e.getMessage());
        }catch (Exception e) {
            Log.d("Save FIle", "Error accessing file: " + e.getMessage());
        }

//        compressImage(pictureFile.getPath());
    }

    public void storeImageIntoCache(Bitmap image,String FileName) {
        if(image == null)
            return;
        File pictureFile = new File(getLocalContentPath(FileName));
        if (pictureFile == null) {
            Log.d("Save FIle",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("Save FIle", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("Save FIle", "Error accessing file: " + e.getMessage());
        }catch (Exception e) {
            Log.d("Save FIle", "Error accessing file: " + e.getMessage());
        }
    }

    public void storeImageCache(Bitmap image,String thumbnail) {
        if(image == null)
            return;
        File pictureFile =  new File(thumbnail);
        if (pictureFile == null) {
//            Log.d("Save FIle",
//                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("Save FIle", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("Save FIle", "Error accessing file: " + e.getMessage());
        }
    }

    /** Create a File for saving an image or video */
    private  File getOutputMediaFile(boolean thumbnail,String filename){
        File mediaFile;
        if(thumbnail){
            mediaFile = new File(getLocalThumbnailImagePath(filename));
        }else {
            mediaFile = new File(getLocalImagePath(filename));
        }
        return mediaFile;
    }

    public Bitmap loadImageFromStorage(String path,boolean thumbnail)
    {
        Bitmap bitmap = null;
        try {
//            File f=new File(path);
//            bitmap = BitmapFactory.decodeStream(new FileInputStream(f));
            bitmap = decodeScaledBitmapFromSdCard(path);

        }catch(OutOfMemoryError e){
        }catch(Exception e){

        }
        return bitmap;
    }



    public Bitmap decodeScaledBitmapFromSdCard(String filePath) {
        Bitmap returnBitmap = null;
//        Configuration configuration = MyApplication.getAppContext().getResources().getConfiguration();
//        int screenHeightDp = configuration.screenHeightDp; //The current width of the available screen space, in dp units, corresponding to screen width resource qualifier.
////        int smallestScreenWidthDp = configuration.smallestScreenWidthDp;
//        int screenHeightPixels = Utils.getInstance().convertDpToPixel(screenHeightDp, MyApplication.getAppContext());
//        int REQUIRED_SIZE = screenHeightPixels/3;
        try{
//            if(thumbnail){
//                REQUIRED_SIZE = 75;
//            }
            // First decode with inJustDecodeBounds=true to check dimensions
          /*  final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inDither = true;
            BitmapFactory.decodeFile(filePath, options);
//		Runtime.getRuntime().freeMemory();
            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, REQUIRED_SIZE, REQUIRED_SIZE);

            // Decode bitmap with inSampleSize set
//	    options.inJustDecodeBounds = false;
            returnBitmap =BitmapFactory.decodeFile(filePath, options);*/
            returnBitmap =BitmapFactory.decodeFile(filePath);
        }catch(OutOfMemoryError e){

        }catch(Exception e){

        }
        return returnBitmap;
    }



    public Register getRegisterObj(){
        return registerObj;
    }

    public void setRegisterObj(Register obj){
        registerObj = obj;
    }

    public Bitmap getThumbnail(String path, int size){
        Bitmap thumbnail = null;
        try{
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);
            int REQUIRED_SIZE = size;
            int scale = 1;
            while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                    && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            options.inSampleSize = scale;
            options.inJustDecodeBounds = false;
            thumbnail = BitmapFactory.decodeFile(path, options);
        }catch(Exception e){

        }
        return thumbnail;
    }

    public Bitmap getVideoThumbnail(String path){
        Bitmap thumbnail = null;
        try{
            thumbnail = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.MINI_KIND);
        }catch(Exception e){

        }
        return thumbnail;
    }

    public void underlineTextView(String text, TextView textView){
        SpannableString str = new SpannableString(text);
        str.setSpan(new UnderlineSpan(), 0, str.length(), Spanned.SPAN_PARAGRAPH);
        textView.setText(str);
    }

    public int getIntegerValueOfString(String value){
        int valueInteger = 0;
        try {
            value = (value == null) ? "0" : (value.isEmpty() ? "0" : value);
            valueInteger = Integer.valueOf(value);
        }catch(NumberFormatException e){

        }catch(Exception e){

        }
        return valueInteger;
    }

    public long getLongValueOfString(String value){
        long valueInteger = 0;
        try {
            value = (value == null) ? "0" : (value.isEmpty() ? "0" : value);
            valueInteger = Long.valueOf(value);
        }catch(NumberFormatException e){

        }catch(Exception e){

        }
        return valueInteger;
    }

    public String getDateDiffString(long dateOne)
    {
        String dayString="";
        long timeOne = dateOne;
        long timeTwo = new Date().getTime();
        long oneDay = 1000 * 60 * 60 * 24;
        long oneHour = 1000 * 60 * 60;
        long oneMinute = 1000 * 60;
        long delta = (timeTwo - timeOne);
        long totalDays = delta/oneDay;
        long year = totalDays / 365;
        long rest = totalDays % 365;
        long month = rest / 30;
        rest = rest % 30;
        long weeks = rest / 7;
        long days = rest % 7;
        long hours = delta / oneHour;
        long minutes = delta % oneHour;
        minutes = minutes/oneMinute;
        if (year > 0 && year < 2) {
            dayString = year+ " year ";
        }else if(year >1){
            dayString = year+ " years ";
        }
        if(dayString.isEmpty()) {
            if (month > 0 && month < 2) {
                dayString = dayString + month + " month ";
            } else if (month > 1) {
                dayString = dayString + month + " months ";
            }
        }
        if(dayString.isEmpty()) {
            if (weeks > 0 && weeks < 2) {
                dayString = dayString + weeks + " week ";
            } else if (weeks > 1) {
                dayString = dayString + weeks + " weeks ";
            }
        }
        if(dayString.isEmpty()) {
            if (days > 0 && days < 2) {
                dayString = dayString + days + " day ";
            } else if (days > 1) {
                dayString = dayString + days + " days ";
            }
        }
        if(dayString.isEmpty()) {
            if (hours > 0 && hours < 2) {
                dayString = hours + " hour ";
            } else if (hours > 1) {
                dayString = hours + " hours ";
            }

        }
        if(dayString.isEmpty()){
            if (minutes > 0 && minutes < 2) {
                dayString = dayString + minutes + " minutes ";
            } else if (minutes > 1) {
                dayString = dayString + minutes + " minutes ";
            }
        }
        if(dayString.isEmpty()) {
            dayString = "few seconds ago";
        }else{
            dayString = dayString +"ago";
        }
        return dayString;
    }

    public boolean getCancelNotification(Long dateOne) {
        boolean cancel = false;
        if(dateOne!=null) {
            long timeOne = dateOne;
            long timeTwo = new Date().getTime();
            long oneDay = 1000 * 60 * 60 * 24;
            long oneHour = 1000 * 60 * 60;
            long oneMinute = 1000 * 60;
            long delta = (timeTwo - timeOne);
            long totalDays = delta / oneDay;
            long year = totalDays / 365;
            long rest = totalDays % 365;
            long month = rest / 30;
            rest = rest % 30;
            long weeks = rest / 7;
            long days = rest % 7;
            long hours = delta / oneHour;
            long minutes = delta % oneHour;
            minutes = minutes / oneMinute;
            if (year > 0) {
                cancel = true;
            }
            if (month > 0) {
                cancel = true;
            }
            if (weeks > 0) {

                cancel = true;
            }
            if (days > 0) {

                cancel = true;
            }
            if (hours > 0) {
                cancel = true;
            }
            if (minutes > 4) {
                cancel = true;
            }
        }
        return cancel;
    }

    public void hideKeyboard(Context mContext,View inputField){
        InputMethodManager inputMethodManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(inputField.getWindowToken(), 0);
    }

    public void showSoftKeyboard(Context mContext,View view) {
        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public Bitmap createCircleBitmap(Bitmap bitmapimg){
        int origWidth= bitmapimg.getWidth();
        int origHeight =bitmapimg.getHeight();
        Bitmap output = Bitmap.createBitmap(origWidth,
                origHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        int cx =origWidth/2;//((origWidth / 2)-(origWidth-width)/2);
        final Rect rect = new Rect(0, 0, origWidth,
                origWidth);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(cx,cx, cx, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmapimg, rect, rect, paint);
        return output;
    }

    public String generateUUIDString(){
        final UUID uuid = UUID.randomUUID();
        String uuidString ;
        if(uuid != null){
            uuidString = uuid.toString();
        }else{
            Date date = new Date();
            uuidString = DataStorage.getInstance().getUsername()+""+date.getTime();
        }
        return uuidString;
    }

    public int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("RegisterActivity",
                    "I never expected this! Going down, going down!" + e);
            throw new RuntimeException(e);
        }
    }

    public int getStatusBarHeight(Context context) {
        int result = 0;
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if(currentapiVersion>=21) {
            int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                result = context.getResources().getDimensionPixelSize(resourceId);
            }
        }
        return result;
    }
    public String generateUUIDStringDateTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date now = new Date();
        String uuidString = formatter.format(now);
        return uuidString;
    }

    public String replceLast(String yourString, String frist,String second)
    {
        StringBuilder b = new StringBuilder(yourString);
        b.replace(yourString.lastIndexOf(frist), yourString.lastIndexOf(frist)+frist.length(),second );
        return b.toString();
    }

    public  String copyDirectoryOneLocationToAnotherLocation(String location) {
        String path = "";
        try {

            path = Environment.getExternalStorageDirectory() + AppConsatants.FILE_VOWMEE;
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            path = path + Utils.getInstance().generateUUIDStringDateTime() +  AppConsatants.FILE_EXTENSION_MP4;
            File targetLocation = new File(path);
            File sourceLocation = new File(location);
//	    	Log.e("File Coping",sourceLocation+" targetLocation "+targetLocation);
            InputStream in = new FileInputStream(sourceLocation);

            OutputStream out = new FileOutputStream(targetLocation);

            // Copy the bits from instream to outstream
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        } catch (IOException e) {

        } catch (Exception e) {

        }
        return path;
    }

    public Drawable loadImageFromSdCard(String path) {

        Drawable drawable = null;
        String filePathImage = Utils.getInstance().getLocalContentPath(path);
        File file = new File(filePathImage);

        // load image
        try {
            if(file.exists()) {
                drawable = Drawable.createFromPath(filePathImage);
            }
        }
        catch(Exception ex) {
            return null;
        }

        return drawable;
    }

    public String get10DigitPhoneNumber(String phoneNumber){
        String phonenumber = phoneNumber;
        if (phonenumber != null && !phonenumber.isEmpty()) {
            phonenumber = phonenumber.replaceAll(" ", "");
            phonenumber = phonenumber.replaceAll("-", "");
            if (phonenumber.length() >= 10) {
                int pos = phonenumber.length();
                phonenumber = phonenumber.substring(pos - 10, pos);
            }
        }else{
            phonenumber = "";
        }
        return phonenumber;
    }


    public  boolean checkIfFileExistAndNotEmpty(String fullFileName) {
        // the case of input images with %d: /sdcard/videokit/pic%03d.jpg

        ///////////////////////////////////
        File f = new File(fullFileName);
        long lengthInBytes = f.length();
//        Log.d("ffmpeg", fullFileName + " length in bytes: " + lengthInBytes);
        if (lengthInBytes > 100)
            return true;
        else {
            return false;
        }

    }

    public void uploadProfileImage(final boolean profile,final String filePath, final String fileName){

        UserFileManager userFileManager=Utils.getInstance().getUserFileManager();

        if(userFileManager != null){
            if(filePath == null)
                return;
            if(filePath.isEmpty())
                return ;
            final File file = new File(filePath);

            userFileManager.uploadContent(file, fileName, new ContentProgressListener() {
                @Override
                public void onSuccess(final ContentItem contentItem) {
//                    Log.i("FIle uploaded", contentItem.getFilePath());
                    File file = new File(filePath);
                    if (file.exists()) {
                        Bitmap image1 = Utils.getInstance().loadImageFromStorage(fileName, false);
                        if (image1 != null)
                            FileCache.getInstance().addBitmapToMemoryCache(fileName, image1);
                    }
                    Bitmap thumbnailProfile = Utils.getInstance().getThumbnail(filePath, 75);

                    Utils.getInstance().storeImage(thumbnailProfile, true,DataStorage.getInstance().getUsername());
                    uploadThumbnailProfileImage(profile,fileName);
                }

                @Override
                public void onProgressUpdate(final String fileName, final boolean isWaiting,
                                             final long bytesCurrent, final long bytesTotal) {

                }

                @Override
                public void onError(final String fileName, final Exception ex) {

                }
            });
        }
    }

    private void uploadThumbnailProfileImage(final boolean profile,String fileName){
        UserFileManager userFileManager=Utils.getInstance().getUserFileManager();

        if(userFileManager != null){

            final String path = Utils.getInstance().getLocalThumbnailImagePath(fileName);
            if(path != null && path.isEmpty())
                return;
            final File file = new File(path);
            final String fileName1= file.getName();
            userFileManager.uploadContent(file, fileName1, new ContentProgressListener() {
                @Override
                public void onSuccess(final ContentItem contentItem) {
                    File file = new File(path);
                    if (file.exists()) {
                        Bitmap image1 = Utils.getInstance().loadImageFromStorage(fileName1, false);
                        if (image1 != null)
                            FileCache.getInstance().addBitmapToMemoryCache(fileName1, image1);
                    }
                    if(profile) {
                        DataStorage.getInstance().setProfileImagePath("");
                    }else{
                        DataStorage.getInstance().setGroupImagePath("");

                    }
                }

                @Override
                public void onProgressUpdate(final String fileName, final boolean isWaiting,
                                             final long bytesCurrent, final long bytesTotal) {
//                    dialog.setProgress((int) bytesCurrent);
                }

                @Override
                public void onError(final String fileName, final Exception ex) {
                }
            });
        }
    }
    public  void displayToast(Context caller, String toastMsg, int toastType){

        try {// try-catch to avoid stupid app crashes
            LayoutInflater inflater = LayoutInflater.from(caller);

            View mainLayout = inflater.inflate(R.layout.toast_layout, null);
            View rootLayout = mainLayout.findViewById(R.id.toast_layout_root);

            TextView text = (TextView) mainLayout.findViewById(R.id.text);
            text.setText(toastMsg);

            Toast toast = new Toast(caller);
            //toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setMargin(0,0);
            toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.BOTTOM, 0, 0);
            if (toastType==SHORT_TOAST)//(isShort)
                toast.setDuration(Toast.LENGTH_SHORT);
            else
                toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(rootLayout);
            toast.show();
        }
        catch(Exception ex) {
        }
    }

    public int getTargetSdkVersion() {
        int targetSdkVersion = 22;
        try {
            final PackageInfo info = MyApplication.getAppContext().getPackageManager().getPackageInfo(
                    MyApplication.getAppContext().getPackageName(), 0);
            targetSdkVersion = info.applicationInfo.targetSdkVersion;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return targetSdkVersion;
    }

    public boolean addPermission(List<String> permissionList, String permission) {
// For Android < Android M, self permissions are always granted.
        boolean result = true;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (getTargetSdkVersion() >= Build.VERSION_CODES.M) {
// targetSdkVersion >= Android M, we can
// use Context#checkSelfPermission
                result = MyApplication.getAppContext().checkSelfPermission(permission)
                        == PackageManager.PERMISSION_GRANTED;
            } else {
// targetSdkVersion < Android M, we have to use PermissionChecker
                result = PermissionChecker.checkSelfPermission(MyApplication.getAppContext(), permission) == PermissionChecker.PERMISSION_GRANTED;
            }
        }
        if (!result) {
            permissionList.add(permission);
        }

        return result;
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public String compressImage(String filePath) {

//        String filePath = getRealPathFromURI(filePath);
        if(filePath.isEmpty())
            return "";
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath,options);

        int actualHeight = bmp.getHeight();
        int actualWidth = bmp.getWidth();
        float maxHeight = 180.0f;
        float maxWidth = 240.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inTempStorage = new byte[16*1024];

        try{
            bmp = BitmapFactory.decodeFile(filePath,options);
        }
        catch(OutOfMemoryError exception){
            exception.printStackTrace();

        }
        try{
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        }
        catch(OutOfMemoryError exception){
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float)options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth()/2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
//            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
//                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
//                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
//                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream out = null;
//        String filename = getFilename();
        File file = new File(filePath);
        if(file.exists()){
            file.delete();
        }
        try {
            out = new FileOutputStream(filePath);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";

    }

    public String getLocalImagePath(String filename) {
        if(filename ==null ||filename.isEmpty())
            return "";
        String path = Environment.getExternalStorageDirectory() + AppConsatants.FILE_EXTENSION_IMAGE;

        File dir = new File(path);
        if(!dir.exists()){
            dir.mkdirs();
        }
        if(!filename.contains(AppConsatants.FILE_EXTENSION_JPG))
            path = path +filename+AppConsatants.FILE_EXTENSION_JPG;
        else
            path = path +filename;
        return path;
    }

    public Bitmap setImageDimension(Bitmap bitmap){
        int newWidth,newHeight;
        Bitmap originalThumbnail=null;
        int deviceWidth = screenWidthPixels;
        newWidth = deviceWidth;
        newHeight = newWidth;
        try {
            if(bitmap!=null) {
                int intWidth = bitmap.getWidth();
                int intHeight = bitmap.getHeight();
                if (intWidth < deviceWidth) {
                    float height = intHeight;
                    float width = intWidth;
                    height = (deviceWidth / width) * height;
                    newHeight = (int) height;
                    newWidth = deviceWidth;
                }
                originalThumbnail = Bitmap.createScaledBitmap(bitmap,newWidth,newHeight,false);

            }
        } catch(IllegalStateException e) {
        }catch (OutOfMemoryError e) {
        }catch (Exception e) {
        }
        return originalThumbnail;
    }
}
