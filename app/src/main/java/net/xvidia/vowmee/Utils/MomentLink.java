package net.xvidia.vowmee.Utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import net.xvidia.vowmee.TimelineMomentDetail;
import net.xvidia.vowmee.network.model.ActivityLog;
import net.xvidia.vowmee.network.model.ModelManager;

import java.util.List;

/**
 * Created by David Nuon on 3/7/14.
 */
public class MomentLink extends ClickableSpan{
    Context context;
    TextPaint textPaint;
    boolean ownActivityFlag =false;
    public MomentLink(Context ctx,boolean ownActivity) {
        super();
        context = ctx;
        ownActivityFlag = ownActivity;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        textPaint = ds;
//        ds.setColor(ds.linkColor);
        ds.setTypeface(Typeface.DEFAULT_BOLD);
        ds.setARGB(255, 96, 99, 102);
    }

    @Override
    public void onClick(View widget) {
        TextView tv = (TextView) widget;
        Spanned s = (Spanned) tv.getText();
        int start = s.getSpanStart(this);
        int end = s.getSpanEnd(this);
        String theWord = s.subSequence(start + 1, end).toString();
//        Toast.makeText(context, String.format("Tags for tags: %s", theWord), Toast.LENGTH_LONG ).show();
        String uuid = getUuidFromName(theWord);
        if(!uuid.isEmpty()){
            Intent mIntent = new Intent(context, TimelineMomentDetail.class);
            Bundle mBundle = new Bundle();
            mBundle.putString(AppConsatants.UUID, uuid);
            mBundle.putBoolean(AppConsatants.UPLOAD_FILE_PATH, false);
            mIntent.putExtras(mBundle);
            context.startActivity(mIntent);
        }

    }

    private String getUuidFromName(String username){
        String uuidsString = "";
        List<ActivityLog> ownActivity = ModelManager.getInstance().getOwnActivity();
        List<ActivityLog> othersActivity = ModelManager.getInstance().getOthersActivity();
        if(!ownActivityFlag) {
            ownActivity = othersActivity;
//            if (ownActivity == null && othersActivity != null) {
//                ownActivity.addAll(othersActivity);
//            } else if (ownActivity == null) {
//                ownActivity = othersActivity;
//            }
        }

        if(ownActivity != null) {
            if (ownActivity.size() > 0) {
                for(ActivityLog obj : ownActivity){
                    if(obj != null && obj.getMomentCaption()!=null) {
                        if (obj.getMomentCaption().contains(username)) {
                            uuidsString = obj.getMomentId();
                            break;
                        }
                    }
                }
            }
        }

        return uuidsString;
    }
}
