package net.xvidia.vowmee.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.util.LruCache;

import net.xvidia.vowmee.MyApplication;

import java.io.File;

/**
 * Created by Ravi_office on 05-Nov-15.
 */
public class FileCache {
    private File file ;
    private static Context context;

    private static  FileCache instance = null;
    private static LruCache<String, Bitmap> mLruCache;
    public File getFile(String url) {
        String localPath = "";
        if(Utils.getInstance().getUserFileManager() != null)
         localPath = Utils.getInstance().getUserFileManager().getLocalContentPath() + File.separator + url;
        file = new File(localPath);
        return file ;
    }

    private FileCache() {
    }

    public static  FileCache getInstance() {
        context = MyApplication.getAppContext();
        if (instance == null) {
            instance = new FileCache();
            //1024 is used because LruCache constructor takes int in kilobytes
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
            final int cacheSize = maxMemory / 4;
            Log.d("timeline", "max memory " + maxMemory + " cache size " + cacheSize);

            // LruCache takes key-value pair in constructor
            // key is the string to refer bitmap
            // value is the stored bitmap
            mLruCache = new LruCache<String, Bitmap>(cacheSize) {
                @Override
                protected int sizeOf(String key, Bitmap bitmap) {
                    // The cache size will be measured in kilobytes
                    return bitmap.getByteCount() / 1024;
                }
            };
        }
        return instance;
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        try {
            if (getBitmapFromMemCache(key) == null) {
                mLruCache.put(key, bitmap);
            }
        }catch (OutOfMemoryError e){

        }catch (Exception e){

        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mLruCache.get(key);
    }
}
