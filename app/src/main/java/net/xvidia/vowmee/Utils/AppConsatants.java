package net.xvidia.vowmee.Utils;

/**
 * Created by Ravi_office on 17-Dec-15.
 */
public class AppConsatants {

    public final static String PROFILE_PUBLIC = "PUBLIC";
    public final static String PROFILE_PRIVATE = "PRIVATE";
    public final static String PROFILE_PRIVATE_SEARCHABLE = "PRIVATE_SEARCHABLE";
    public final static String GROUP_ROLE_OWNER = "OWNER";
    public final static String GROUP_ROLE_MEMBER = "MEMBER";
    public final static String GROUP_ROLE_CREATOR = "CREATOR";
    public final static String GROUP_ROLE_FOLLOWER = "FOLLOWER";
    public final static String GROUP_ROLE_MODERATOR = "MODERATOR";
    public final static String MOMENT_PATH = "MomentPath";
    public final static String MOMENT_LATITUDE = "MomentLatitude";
    public final static String MOMENT_LONGITUDE = "MomentLongitude";
    public final static String MOMENT_CHAT = "Moment_chat";
    public final static String PROFILE_FACEBOOK = "Facebook";
    public final static String PROFILE_TWITTER = "Twitter";
    public final static String PROFILE_REGISTER = "Register";
    public final static String PROFILE_PHONE = "PhoneNumber";
    public final static String DIGIT_HEADER_PROVIDER = "X-Auth-Service-Provider";
    public final static String DIGIT_HEADER_AUTH = "X-Verify-Credentials-Authorization";
    public final static String PERSON_FRIEND = "Friend";
    public final static String PERSON_FOLLOWER = "Follower";
    public final static String PERSON_FOLLOWING = "Following";
    public final static String PERSON_FACEBOOK = "Facebook";
    public final static String USER_THUMBNAIL = "UserThumbnail";
    public final static String PERSON_PENDING = "Pending";
    public final static String NOTIFICATION_NEXT_INTENT = "NextIntent";
    public final static String NOTIFICATION_FRIEND_USERNAME = "FriendUsername";
    public final static String NOTIFICATION_ID_INTENT = "NotificationId";
    public final static String NOTIFICATION_ACCEPT_INTENT = "AcceptIntent";
    public final static String NOTIFICATION_DECLINE_INTENT = "DeclineIntent";
    public final static String OTHER_PERSON_PROFILE = "Other";
    public final static String LIVE_PUBLISHER_ID = "LivePublisherId";
    public final static String LIVE_CAPTION= "LiveCaption";
    public final static String LIVE_DESCRIPTION = "LiveDescription";
    public final static String LIVE_VIDEO = "LiveVideo";
    public final static String RECORDED_VIDEO = "RecordedVideo";
    public final static String UPLOAD_FILE_DESCRIPTION = "UploadFileDescription";
    public final static String UPLOAD_FILE_CAPTION = "UploadFileCaption";
    public final static String UPLOAD_FILE_PATH = "UploadFilePath";
    public final static String UPLOAD_MOMENT_GROUP = "UploadGroupMoment";
    public final static String NETWORK_RESTART = "NetworkRestart";
    public final static String IDINBOXSTYLE = "IDINBOXSTYLE";
    public final static String UUID = "Uuid";
    public final static String RESHARE = "Reshare";
    public final static String STATUS_ACTIVE = "ACTIVE";
    public final static String STATUS_INACTIVE = "INACTIVE";
    public final static String STATUS_IS_LIVE= "SUBSCRIBED";
    public final static String FALSE = "false";
    public final static String TRUE = "true";
    public final static String THUMBNAIL_EXTENSION_JPG = "_thumbnail.jpg";
    public final static String FILE_EXTENSION_JPG = ".jpg";
    public final static String FILE_EXTENSION_MP4 = ".mp4";
    public final static String FILE_EXTENSION_IMAGE = "/vowmee/image/";
    public final static String FILE_VOWMEE = "/vowmee/";
    public final static long cacheHitButRefreshed = 05 * 1000; // in 30 seconds cache will be hit, but also refreshed on background
    public final static long cacheExpired = 10* 24 * 60 * 60 * 1000; // expiry time 10 days
    public final static long timeoutMakeCallEnd = 60 * 1000; // time out for make call
    public final static long timeoutReceiveCallEnd = 40 * 1000; // time out for receive call
    public final static int CAMERA_WIDTH = 480; // time out for receive call
    public final static int CAMERA_HEIGHT = 640; // time out for receive call

/*TEST CONSTANTS*/

//    public final static String TEST_USERNAME = "USERPerson101@gmail.com";
}
