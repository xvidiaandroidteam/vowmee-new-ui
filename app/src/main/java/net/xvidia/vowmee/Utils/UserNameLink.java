package net.xvidia.vowmee.Utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import net.xvidia.vowmee.TimelineFriendProfile;
import net.xvidia.vowmee.network.model.ActivityLog;
import net.xvidia.vowmee.network.model.ModelManager;

import java.util.List;

/**
 * Created by David Nuon on 3/7/14.
 */
public class UserNameLink extends ClickableSpan {
    Context context;
    boolean ownActivityFlag =false;
    public UserNameLink(Context ctx,boolean ownActivity) {
        super();
        context = ctx;
        ownActivityFlag = ownActivity;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setARGB(255, 96, 99, 102);
        ds.setTypeface(Typeface.DEFAULT_BOLD);

    }

    @Override
    public void onClick(View widget) {
        TextView tv = (TextView) widget;
        Spanned s = (Spanned) tv.getText();
        int start = s.getSpanStart(this);
        int end = s.getSpanEnd(this);
        String theWord = s.subSequence(start, end).toString();
//        Toast.makeText(context, String.format("Here's a cool person: %s", theWord), Toast.LENGTH_SHORT).show();
        String userId = getUserIdFromName(theWord);
        if(!userId.isEmpty()){
            TimelineFriendProfile.setFriendUsername(userId);
            Intent mIntent = new Intent(context, TimelineFriendProfile.class);
//            Bundle mBundle = new Bundle();
//            mBundle.putString(AppConsatants.PERSON_FRIEND, userId);
//            mIntent.putExtras(mBundle);
            context.startActivity(mIntent);
        }

    }

    private String getUserIdFromName(String username){
        String userId = "";
         List<ActivityLog> ownActivity = ModelManager.getInstance().getOwnActivity();
        List<ActivityLog> othersActivity = ModelManager.getInstance().getOthersActivity();
        if(!ownActivityFlag) {
            ownActivity = othersActivity;
//            if (ownActivity == null && othersActivity != null) {
//                ownActivity.addAll(othersActivity);
//            } else if (ownActivity == null) {
//                ownActivity = othersActivity;
//            }
        }

        if(ownActivity != null) {
            if (ownActivity.size() > 0) {
                for(ActivityLog obj : ownActivity){
                    if(obj != null) {
                        if (obj.getInitiaterName() != null && obj.getInitiaterName().contains(username)) {
                            userId = obj.getInitiaterId();
                            break;
                        } else if (obj.getReceiverName()!= null && obj.getReceiverName().contains(username)) {
                            userId = obj.getReceiverId();
                            break;
                        }
                    }
                }
            }
        }

        return userId;
    }
}