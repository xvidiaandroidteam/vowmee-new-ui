
package net.xvidia.vowmee;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.theartofdev.edmodo.cropper.CropImageView;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.CheckNetworkConnection;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.storage.DataStorage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CropImageActivityNew extends AppCompatActivity implements CropImageView.OnSetImageUriCompleteListener, CropImageView.OnGetCroppedImageCompleteListener {

    // Private Constants ///////////////////////////////////////////////////////////////////////////

//    private final int REQUIRED_SIZE = 150;
    private final int THUMBNAIL_REQUIRED_SIZE = 70;



    private CropImageView mCropImageView;
    private ImageView mCroppedImageView;
    private String selectedImagePath;
    private boolean registerProfile;
    private boolean croping;
    private Bitmap thumbnailProfile;
    Bitmap croppedImage;
    Button buttonCrop;
    Button buttonLoad;
    Button buttonSave;
    Button buttonCancel;
    MenuItem menuCrop;
    MenuItem menuLoad;
    MenuItem menuSave;
    MenuItem menuCancel;
    // Activity Methods ////////////////////////////////////////////////////////////////////////////

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_layout);

        mCropImageView = (CropImageView) findViewById(R.id.CropImageView);
        mCroppedImageView = (ImageView) findViewById(R.id.CroppedImageView);

        croping= false;
        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            registerProfile = intent.getBooleanExtra(AppConsatants.PROFILE_REGISTER, false);
        }
        String name = getString(R.string.select_profile_image);
        if(!registerProfile && ModelManager.getInstance().getOwnProfile()!=null) {
            name = ModelManager.getInstance().getOwnProfile().getDisplayName();
            selectedImagePath = Utils.getInstance().getLocalContentPath(ModelManager.getInstance().getOwnProfile().getProfileImage());
        }
        setCropImageViewOptions();
        Bitmap image = Utils.getInstance().loadImageFromStorage(selectedImagePath, false);
        if(image != null) {
            mCropImageView.setImageBitmap(image);
        }else {
             mCropImageView.setImageResource(R.drawable.user);
        }


        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(name);
        mToolbar.setPadding(0, 0, 0, 0);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    public void setCropImageViewOptions() {

        mCroppedImageView.setVisibility(View.GONE);
        mCropImageView.setVisibility(View.VISIBLE);
        mCropImageView.setScaleType(CropImageView.ScaleType.CENTER_INSIDE);
        mCropImageView.setCropShape(CropImageView.CropShape.RECTANGLE);
        mCropImageView.setGuidelines(CropImageView.Guidelines.OFF);
        mCropImageView.setAspectRatio(1, 1);
        mCropImageView.setFixedAspectRatio(true);
        mCropImageView.setShowCropOverlay(true);
        mCropImageView.setShowProgressBar(true);
        mCropImageView.setAutoZoomEnabled(true);
        mCropImageView.setMaxZoom(2);
        mCropImageView.setCropRect(new Rect(100, 300, 500, 1200));
    }

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedImageUri = null;
        if (resultCode == RESULT_OK) {

                selectedImageUri = getPickImageResultUri(data);
                try {
                    selectedImagePath =Utils.getInstance().getPath(this, selectedImageUri, true);
//                    Uri uriImage = Uri.parse(selectedImagePath);
                    mCropImageView.setImageUriAsync(selectedImageUri);
//                    mCropImageView.setImageBitmap(Utils.getInstance().loadImageFromStorage(selectedImagePath,false));
                } catch (Exception e) {
                    e.printStackTrace();
                }
//            }

        }
    }

        public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = Uri.fromFile(new File(Utils.getInstance().getLocalImagePath(DataStorage.getInstance().getUsername())));//getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
//        Uri outputFileUri = Uri.fromFile(new File(Utils.getInstance().getLocalImagePath()));
//        File getImage = getExternalCacheDir();
//        if (getImage != null) {
//            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
            outputFileUri = Uri.fromFile(new File(Utils.getInstance().getLocalImagePath(DataStorage.getInstance().getUsername())));
//        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }
    private void uploadProfileImage(){

        UserFileManager userFileManager=Utils.getInstance().getUserFileManager();

        if(userFileManager != null){

            String path = selectedImagePath;

            if(path == null)
                return;
            if(path.isEmpty())
                return ;
            final File file = new File(path);
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    mProgressBar.setVisibility(View.VISIBLE);
//                }
//            });
            final String fileName= DataStorage.getInstance().getUsername()+AppConsatants.FILE_EXTENSION_JPG;
//                    downloadContentFromS3ucket("25.mp4");
//
            userFileManager.uploadContent(file, fileName, new ContentProgressListener() {
                @Override
                public void onSuccess(final ContentItem contentItem) {
//                    String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                    File file = new File(selectedImagePath);
                    if (file.exists()) {
                        Bitmap image1 = Utils.getInstance().loadImageFromStorage(fileName, false);
                        if (image1 != null)
                            FileCache.getInstance().addBitmapToMemoryCache(fileName, image1);
                    }
                    thumbnailProfile = Utils.getInstance().getThumbnail(selectedImagePath, THUMBNAIL_REQUIRED_SIZE);

                    Utils.getInstance().storeImage(thumbnailProfile, true,DataStorage.getInstance().getUsername());
                    uploadThumbnailProfileImage(false);
                }

                @Override
                public void onProgressUpdate(final String fileName, final boolean isWaiting,
                                             final long bytesCurrent, final long bytesTotal) {

                }

                @Override
                public void onError(final String fileName, final Exception ex) {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            enableButtons(true);
//                            mProgressBar.setVisibility(View.GONE);
//                        }
//                    });
                    Utils.getInstance().displayToast(CropImageActivityNew.this,"update of image failed",Utils.SHORT_TOAST);
//                    Log.i("FIle uploaded error", ex.getMessage());
                    croping= false;
                }
            });
        }
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        setSaveVisible(false);
    }

    private void uploadThumbnailProfileImage(boolean showDialog){
        UserFileManager userFileManager=Utils.getInstance().getUserFileManager();

        if(userFileManager != null){

            final String path = Utils.getInstance().getLocalThumbnailImagePath(DataStorage.getInstance().getUsername());
            if(path != null && path.isEmpty())
                return;
            final File file = new File(path);
//            mProgressBar.setVisibility(View.VISIBLE);
//            final ProgressDialog dialog = new ProgressDialog(this);
//            dialog.setTitle(R.string.app_name);
//            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            dialog.setCancelable(false);
//            dialog.setIndeterminate(true);
//            if(showDialog)
//                dialog.show();
            final String fileName= DataStorage.getInstance().getUsername()+AppConsatants.THUMBNAIL_EXTENSION_JPG;
//                    downloadContentFromS3ucket("25.mp4");
//                    dialog.dismiss();
            userFileManager.uploadContent(file, fileName, new ContentProgressListener() {
                @Override
                public void onSuccess(final ContentItem contentItem) {
                    File file = new File(path);
                    if (file.exists()) {
                        Bitmap image1 = Utils.getInstance().loadImageFromStorage(fileName, false);
                        if (image1 != null)
                            FileCache.getInstance().addBitmapToMemoryCache(fileName, image1);
                    }
                    Utils.getInstance().storeImage(thumbnailProfile, true,DataStorage.getInstance().getUsername());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            mProgressBar.setVisibility(View.GONE);
                            onBackPressed();
                        }
                    });
                    croping= false;
                }

                @Override
                public void onProgressUpdate(final String fileName, final boolean isWaiting,
                                             final long bytesCurrent, final long bytesTotal) {
//                    dialog.setProgress((int) bytesCurrent);
                }

                @Override
                public void onError(final String fileName, final Exception ex) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

//                            enableButtons(true);
//                            mProgressBar.setVisibility(View.GONE);
                            onBackPressed();
                        }
                    });
//                    Utils.getInstance().displayToast(CropImageActivity.this,"update of image failed",Utils.SHORT_TOAST);
//                    Log.i("FIle uploaded error", ex.getMessage());croping= false;
                }
            });
        }
    }
    private void showError(final int resId, final String mesg) {
        try{
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                new AlertDialog.Builder(CropImageActivityNew.this).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(mesg)
                        .setPositiveButton(android.R.string.ok, null)
                        .show();
            }
        });
    }catch(WindowManager.BadTokenException e){}
    catch(Exception e){}
    }

    @Override
    public void onGetCroppedImageComplete(CropImageView view, Bitmap bitmap, Exception error) {
        if (error == null) {
            croppedImage = bitmap;
            setSaveVisible(true);
            mCroppedImageView.setVisibility(View.VISIBLE);
            mCropImageView.setVisibility(View.GONE);
            mCroppedImageView.setImageBitmap(croppedImage);
//            ImageView croppedImageView = (ImageView) findViewById(R.id.croppedImageView);
//            croppedImageView.setImageBitmap(croppedImage);

        } else {
            croping= false;
            Utils.getInstance().displayToast(MyApplication.getAppContext(), "Image crop failed: " + error.getMessage(),Utils.LONG_TOAST);
        }
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if (error != null) {
            Utils.getInstance().displayToast(MyApplication.getAppContext(), "Image load failed: " + error.getMessage(), Utils.LONG_TOAST);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mCropImageView.setOnSetImageUriCompleteListener(this);
        mCropImageView.setOnGetCroppedImageCompleteListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mCropImageView.setOnSetImageUriCompleteListener(null);
        mCropImageView.setOnGetCroppedImageCompleteListener(null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

//    private void enableButtons(boolean enable){
//        loadButton.setEnabled(enable);
//        rotateButton.setEnabled(enable);
//        saveButton.setEnabled(enable);
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_crop, menu);
        if (menu != null) {
            menuCrop = menu.findItem(R.id.main_action_crop);
            MenuItemCompat.setActionView(menuCrop, R.layout.group_next);
            View viewCrop =  MenuItemCompat.getActionView(menuCrop);

            menuLoad = menu.findItem(R.id.main_action_load);
            MenuItemCompat.setActionView(menuLoad, R.layout.group_next);
            View viewLoad=  MenuItemCompat.getActionView(menuLoad);

            menuSave = menu.findItem(R.id.main_action_save);
            MenuItemCompat.setActionView(menuSave, R.layout.group_next);
            View viewSave=  MenuItemCompat.getActionView(menuSave);

            menuCancel = menu.findItem(R.id.main_action_cancel);
            MenuItemCompat.setActionView(menuCancel, R.layout.group_next);
            View viewCancel=  MenuItemCompat.getActionView(menuCancel);
//            View viewNext =menuNext.getActionView();
            buttonCrop = (Button) viewCrop.findViewById(R.id.group_next);
            buttonCrop.setText(getString(R.string.action_crop));
            buttonCrop.setTextColor(getResources().getColor(R.color.white));
            buttonCrop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCropImageView.getCroppedImageAsync();
                }
            });

            buttonLoad = (Button) viewLoad.findViewById(R.id.group_next);
            buttonLoad.setText(getString(R.string.action_load));
            buttonLoad.setTextColor(getResources().getColor(R.color.white));
            buttonLoad.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }
            });
            buttonSave = (Button) viewSave.findViewById(R.id.group_next);
            buttonSave.setText(getString(R.string.action_save));
            buttonSave.setTextColor(getResources().getColor(R.color.white));
            buttonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (CheckNetworkConnection.isConnectionAvailable(CropImageActivityNew.this)) {
                        Utils.getInstance().storeImage(croppedImage, false, DataStorage.getInstance().getUsername());
                        selectedImagePath = Utils.getInstance().getLocalImagePath(DataStorage.getInstance().getUsername());
                        DataStorage.getInstance().setProfileImagePath(selectedImagePath);
                        if (registerProfile) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    onBackPressed();
                                }
                            });
                            croping = false;
                            return;
                        } else {
//                        uploadProfileImage();
                            Utils.getInstance().uploadProfileImage(true, DataStorage.getInstance().getProfileImagePath(), DataStorage.getInstance().getUsername() + AppConsatants.FILE_EXTENSION_JPG);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    onBackPressed();
                                }
                            });
                        }
                    }else{
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_no_internet), Utils.LONG_TOAST);

                    }
                }
            });

            buttonCancel = (Button) viewCancel.findViewById(R.id.group_next);
            buttonCancel.setText(getString(R.string.action_cancel));
            buttonCancel.setTextColor(getResources().getColor(R.color.white));
            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mCroppedImageView.setVisibility(View.GONE);
                    mCropImageView.setVisibility(View.VISIBLE);
                    Bitmap image = Utils.getInstance().loadImageFromStorage(selectedImagePath, false);
                    if(image != null) {
                        mCropImageView.setImageBitmap(image);
                    }else {
                        mCropImageView.setImageResource(R.drawable.user);
                    }
                    setSaveVisible(false);
                }
            });

            setSaveVisible(false);

        }
        return true;
    }

    private void setSaveVisible(boolean save){

        if (menuCrop != null)
            menuCrop.setVisible(!save);
        if (menuLoad != null)
            menuLoad.setVisible(!save);
        if (menuSave != null)
            menuSave.setVisible(save);
        if (menuCancel != null)
            menuCancel.setVisible(save);
        if(save) {
            if (buttonCrop != null)
                buttonCrop.setVisibility(View.GONE);
            if (buttonLoad != null)
                buttonLoad.setVisibility(View.GONE);
            if (buttonSave != null)
                buttonSave.setVisibility(View.VISIBLE);
            if (buttonCancel != null)
                buttonCancel.setVisibility(View.VISIBLE);

        }else{
            if (buttonCrop != null)
                buttonCrop.setVisibility(View.VISIBLE);
            if (buttonLoad != null)
                buttonLoad.setVisibility(View.VISIBLE);
            if (buttonSave != null)
                buttonSave.setVisibility(View.GONE);
            if (buttonCancel != null)
                buttonCancel.setVisibility(View.GONE);
        }
    }

}
