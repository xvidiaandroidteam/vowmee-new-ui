package net.xvidia.vowmee;

/**
 * Created by vasu on 22/12/15.
 */

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.opentok.android.BaseVideoRenderer;
import com.opentok.android.Connection;
import com.opentok.android.OpentokError;
import com.opentok.android.Publisher;
import com.opentok.android.PublisherKit;
import com.opentok.android.Session;
import com.opentok.android.Stream;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.broadcastreceiver.NetworkStateReceiver;
import net.xvidia.vowmee.fragments.MeterView;
import net.xvidia.vowmee.helper.CustomFontTextView;
import net.xvidia.vowmee.helper.OnSwipeTouchListener;
import net.xvidia.vowmee.listadapter.ChatAdapterNew;
import net.xvidia.vowmee.listadapter.ViewersListAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.UploadffmpegService;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.ChatMessage;
import net.xvidia.vowmee.network.model.LiveMomentViewerDetails;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.network.model.MomentShare;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class PublishLiveActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener, Session.SessionListener,
        PublisherKit.PublisherListener, Session.ArchiveListener, Session.SignalListener {
    public static final String SIGNAL_TYPE_CHAT = "chat";
    public static final String SIGNAL_TYPE_IMAGE = "image";
    public static final String SIGNAL_TYPE_CLOSE = "close";


    //    private Button toggleVoice;
    private String mApiKey;
    public static boolean closePublish;
    public static String mSessionId;
    private String mToken;
    private Session mSession;
    private Publisher mPublisher;
    private RecyclerView mMessageHistoryListView;
    private ChatAdapterNew mMessageHistory;
    private FrameLayout mPublisherViewContainer;
    private ProgressDialog pDialog;
    private RelativeLayout messageLayout;
//    private ImageView heartImageView;
    private RelativeLayout heartLayout;
    private RelativeLayout switchCamera;
    private TextView viewerCount;
//    private Animation pulse;
    private String momentCaption;
    private String momentDescription;
    private RelativeLayout cancelSession;
    private boolean chat;
    private Double latitude;
    private Double longitude;
    private TextView reconnect;

    private List<ChatMessage> commentList;
    private String groupUuid;
    MeterView mv;
    private boolean privateFlag;
    private boolean firstFlag;
    Moment momentAdded;
    private Handler mHandlerRefreshCount;
    private Runnable mRunnableCounter;
    private NetworkStateReceiver networkStateReceiver;

    private RecyclerView mRecyclerView;
    private ViewersListAdapter mAdapter;
    private List<Profile> viewersList;
    private LinearLayout tintLayout;
    private Animation slideUp, slideDown;
    private RelativeLayout layout;
    private int deviceHeight;
    public static int count, totalCount;
    private int[] images = new int[]{R.drawable.kiss,R.drawable.heart1,R.drawable.heart2,R.drawable.love,R.drawable.heart3};
    int maximum = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publisher_live);
        firstFlag = true;
        closePublish = false;
        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            momentCaption = intent.getStringExtra(AppConsatants.LIVE_CAPTION);
            momentDescription = intent.getStringExtra(AppConsatants.LIVE_DESCRIPTION);
//            groupMoment = intent.getBooleanExtra(AppConsatants.UPLOAD_MOMENT_GROUP,false);
            groupUuid = intent.getStringExtra(AppConsatants.UUID);
            privateFlag = intent.getBooleanExtra(AppConsatants.PROFILE_PRIVATE, false);
            chat = intent.getBooleanExtra(AppConsatants.MOMENT_CHAT, false);
            latitude = intent.getDoubleExtra(AppConsatants.MOMENT_LATITUDE, 0);
            longitude = intent.getDoubleExtra(AppConsatants.MOMENT_LONGITUDE, 0);


        }
        reconnect = (TextView) findViewById(R.id.reconnect);
        reconnect.setVisibility(View.GONE);
        cancelSession = (RelativeLayout) findViewById(R.id.cancelLiveSession);
        switchCamera = (RelativeLayout) findViewById(R.id.button_switch_camera);
        viewerCount = (CustomFontTextView) findViewById(R.id.viewerCount);
        viewerCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initialiseRecyclerViewViewers();
                slideToTop(tintLayout);
//                tintLayout.setVisibility(View.VISIBLE);
                layout.startAnimation(slideUp);
                layout.setVisibility(View.VISIBLE);
            }
        });

        ///Viewers list
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        deviceHeight = displayMetrics.heightPixels;

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        params.height = deviceHeight/2;
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        layout = (RelativeLayout) findViewById(R.id.bottomLayout);
        layout.setVisibility(View.GONE);
        layout.setLayoutParams(params);
        slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        tintLayout = (LinearLayout) findViewById(R.id.tint_layout);
        tintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseBottomListView();
            }
        });
        tintLayout.setClickable(false);

        cancelSession.setVisibility(View.VISIBLE);
        cancelSession.setOnClickListener(new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                callSessionCancel();
            }
        });
        switchCamera.setOnClickListener(new View.OnClickListener(

        ) {
            @Override
            public void onClick(View v) {
                flipCamera();
            }
        });
        mPublisherViewContainer = (FrameLayout) findViewById(R.id.publisher_container);
        heartLayout = (RelativeLayout) findViewById(R.id.heartLayout);
        mMessageHistoryListView = (RecyclerView) findViewById(R.id.message_history_list_view);
//        startBroadcast = (TextView) findViewById(R.id.startBroadcast);
        messageLayout = (RelativeLayout) findViewById(R.id.messageLayout);
        // Attach data source to message history
        commentList = new ArrayList<ChatMessage>();

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Connecting...Please wait.");
//        pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                onBackPressed();
//            }
//        });
        pDialog.setCancelable(true);
        mv = (MeterView) findViewById(R.id.publishermeter);
        mv.setIcons(BitmapFactory.decodeResource(getResources(),
                R.drawable.unmute_pub), BitmapFactory.decodeResource(
                getResources(), R.drawable.mute_pub));

        messageLayout.setVisibility(View.GONE);
//        mPublisherViewContainer.setVisibility(View.GONE);

        showDialog();
        fetchPublishSessionConnectionData();
        viewersList = new ArrayList<>();
        mPublisherViewContainer.setVisibility(View.VISIBLE);
        MomentShare momemtShare = new MomentShare();
        UploadffmpegService.setMomentShare(momemtShare);
        SelectPrivateShareActivity.resetValues();
        if (groupUuid != null && !groupUuid.isEmpty()) {
            ArrayList<String> groupList = new ArrayList<>();
            groupList.add(groupUuid);
            SelectPrivateShareActivity.setGroupList(groupList);
        }
        mPublisherViewContainer.setOnTouchListener(new OnSwipeTouchListener(PublishLiveActivity.this) {
            public void onSwipeTop() {
                initialiseRecyclerViewViewers();
                slideToTop(tintLayout);
                layout.startAnimation(slideUp);
                layout.setVisibility(View.VISIBLE);
            }
            public void onSwipeRight() {

            }
            public void onSwipeLeft() {
            }
            public void onSwipeBottom() {
                CloseBottomListView();
            }

        });
    }

    private void initializeRecyclerView(){
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setStackFromEnd(true);
        mMessageHistoryListView.setLayoutManager(mLayoutManager);
        mMessageHistory = new ChatAdapterNew(this, commentList);
        mMessageHistoryListView.setAdapter(mMessageHistory);
        mMessageHistory.notifyDataSetChanged();
    }

    private void initialiseRecyclerViewViewers() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
//        mLayoutManager.setStackFromEnd(f);
        mRecyclerView = (RecyclerView) findViewById(R.id.bottomlistview);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ViewersListAdapter(PublishLiveActivity.this, viewersList,count,totalCount);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(false);
    }

    public void slideToBottom(View view) {
        tintLayout.setClickable(false);
        TranslateAnimation animate = new TranslateAnimation(0, 0, 0, view.getHeight());
        animate.setDuration(1000);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);

    }

    public void slideToTop(View view) {
        tintLayout.setClickable(true);
        TranslateAnimation animate = new TranslateAnimation(0, 0, view.getHeight(), 0);
        animate.setDuration(1000);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);

    }

    public void CloseBottomListView() {

        if (layout.getVisibility() == View.VISIBLE) {
            slideToBottom(tintLayout);
//        tintLayout.setVisibility(View.INVISIBLE);
            layout.startAnimation(slideDown);
            layout.setVisibility(View.INVISIBLE);
        }

    }

    private void setRefreshCount() {
        if (mHandlerRefreshCount == null)
            mHandlerRefreshCount = new Handler();
        if (mRunnableCounter == null)
            mRunnableCounter = new Runnable() {
                @Override
                public void run() {
                    getLiveCount();
                    setRefreshCount();
                }
            };

        mHandlerRefreshCount.postDelayed(mRunnableCounter, 15000);
    }

    private void setViewerCount(){
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewerCount != null) {
                        viewerCount.setVisibility(View.VISIBLE);
                        viewerCount.setText(getString(R.string.live_count_message, count));
                    }
                }
            });
        }catch (WindowManager.BadTokenException e){

        }catch (Exception e){

        }

    }


    private void callSessionCancel() {
        closePublish = true;
        restartAudioMode();
        if (mSession != null) {
            mSession.sendSignal(SIGNAL_TYPE_CLOSE, getString(R.string.error_live_session_ended));
            mSession.disconnect();
        }
        finish();
    }

    public void flipCamera() {

        mPublisherViewContainer.removeView(mPublisher.getView());
        mPublisher.cycleCamera();
        mPublisherViewContainer.addView(mPublisher.getView());

    }

    private void initializeSession() {
        mSession = new Session(this, mApiKey, mSessionId);
        mSession.setSessionListener(this);
        mSession.setArchiveListener(this);
        mSession.setSignalListener(this);
        mSession.connect(mToken);
    }

    private void initializePublisher() {
        mPublisher = new Publisher(getApplicationContext(), "Xvidia's Demo",
                Publisher.CameraCaptureResolution.MEDIUM,
                Publisher.CameraCaptureFrameRate.FPS_15);
        mPublisher.setPublisherListener(this);
        mPublisher.getRenderer().setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                BaseVideoRenderer.STYLE_VIDEO_FILL);
        mPublisher.setPublishAudio(true);
        mPublisherViewContainer.addView(mPublisher.getView(), 0);
        mPublisherViewContainer.setVisibility(View.VISIBLE);

    }

    private void logOpenTokError(OpentokError opentokError) {
        //Log.e(LOG_TAG, "Error Domain: " + opentokError.getErrorDomain().name());
        //Log.e(LOG_TAG, "Error Code: " + opentokError.getErrorCode().name());
    }

    public void restartAudioMode() {
        AudioManager Audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        Audio.setMode(AudioManager.MODE_NORMAL);
        this.setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
    }

    @Override
    public void onBackPressed() {
        callSessionCancel();
//        if (mSession != null) {
////            closePublishSessionConnection();
//            mSession.disconnect();
//        }
    }

    public void fetchPublishSessionConnectionData() {


        Moment moment = new Moment();
        moment.setCaption(momentCaption);
        moment.setDescription(momentDescription);
        moment.setIsLive(true);
        moment.setLiveChatOn(chat);
        moment.setLongitude(longitude);
        moment.setLatitude(latitude);
//                        moment.setApiKey(mApiKey);
//                        moment.setSessionId(mSessionId);
//                        moment.setToken(mToken);
        if (privateFlag)
            moment.setProfile(AppConsatants.PROFILE_PRIVATE);
        else
            moment.setProfile(AppConsatants.PROFILE_PUBLIC);
        moment.setOwner(DataStorage.getInstance().getUsername());

        UploadffmpegService.getMomentShare().setFollowers(SelectPrivateShareActivity.isAllFollowers());
        UploadffmpegService.getMomentShare().setFriends(SelectPrivateShareActivity.isAllFriends());
        UploadffmpegService.getMomentShare().setGroupUuids(SelectPrivateShareActivity.getGroupList());
        UploadffmpegService.getMomentShare().setUserUuids(SelectPrivateShareActivity.getMemberUuidList());
        UploadffmpegService.getMomentShare().setNewMoment(moment);
//                        //Log.i("Moment uploaded", moment.toString());
        sendMomentAddSuccess(UploadffmpegService.getMomentShare());


    }



    private void getLiveCount() {
        try {
            String url = ServiceURLManager.getInstance().getLiveCountUrl(mSessionId);

            //Log.i("Add moment", jsonObject);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    LiveMomentViewerDetails obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), LiveMomentViewerDetails.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        count = (int) obj.getActiveViewerCount();
                        totalCount = (int) obj.getTotalViewerCount();
                        viewersList = obj.getLastFewActiveViewers();
                        setViewerCount();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
        }
    }


    private void sendMomentAddSuccess(final MomentShare moment) {
        try {
            if (moment == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_MOMENT_ADD);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(moment);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Log.i("Add moment", jsonObject);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Moment obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Moment.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        DataStorage.getInstance().setDestroyLiveFlag(true);
                        momentAdded = obj;
                        mSessionId = momentAdded.getSessionId();
                        mToken = momentAdded.getToken();
                        mApiKey = momentAdded.getApiKey();
                        if (mSessionId != null && !mSessionId.equalsIgnoreCase("null")) {
                            initializeSession();
                            initializePublisher();
                            firstFlag = false;
                        }
                    } else {
                        closeSession();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    closeSession();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
            closeSession();
        }
    }

    private void closeSession() {
        hideDialog();
        if (mSession != null)
            mSession.disconnect();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                showMessage(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            }
        });
    }

    private void showMessage(String messageData, boolean remote) {
        if(!chat)
            return;
        ObjectMapper mapper = new ObjectMapper();
        ChatMessage message = null;
        try {
            message = mapper.readValue(messageData.toString(), ChatMessage.class);
        } catch (IOException e) {
        }catch (OutOfMemoryError e) {
        }catch (Exception e) {
        }
        if (commentList == null) {
            commentList = new ArrayList<>();
        }
//        if (commentList != null && commentList.size() > 4) {
//            commentList.remove(0);
//        }
        if(mMessageHistory == null){
            commentList.add(message);
            initializeRecyclerView();

        }else {
//            mMessageHistory.add(message);
            int pos =  ChatAdapterNew.chatList.size();
            ChatAdapterNew.chatList.add(message);
            mMessageHistory.notifyItemInserted(pos);
//            mMessageHistory.notifyDataSetChanged();
        }
    }


    /* Session Listener methods */

    @Override
    public void onConnected(Session session) {
        //Log.i(LOG_TAG, "Session Connected");
        if (mPublisher != null) {
            mSession.publish(mPublisher);
            messageLayout.setVisibility(View.VISIBLE);
            // Initialize publisher meter view
//            final MeterView meterView = (MeterView) findViewById(R.id.publishermeter);
            mPublisher.setAudioLevelListener(new PublisherKit.AudioLevelListener() {
                @Override
                public void onAudioLevelUpdated(PublisherKit publisher,
                                                float audioLevel) {
                    mv.setMeterValue(audioLevel);
                }
            });
            mv.setOnClickListener(new MeterView.OnClickListener() {
                @Override
                public void onClick(MeterView view) {
                    mPublisher.setPublishAudio(!view.isMuted());
                }
            });
            if(reconnect.getVisibility() == View.VISIBLE)
                reconnect.setVisibility(View.GONE);
        }

    }

    @Override
    public void onDisconnected(Session session) {
        //Log.i(LOG_TAG, "Session Disconnected");
//        closePublishSessionConnection();
        closePublish = true;
//        mPublisher.destroy();

    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {
        //Log.i(LOG_TAG, "Stream Received");
    }

    @Override
    public void onStreamDropped(Session session, Stream stream) {
        //Log.i(LOG_TAG, "Stream Dropped");

    }

    @Override
    public void onError(Session session, OpentokError opentokError) {
        logOpenTokError(opentokError);
    }

    /* Publisher Listener methods */

    @Override
    public void onStreamCreated(PublisherKit publisherKit, Stream stream) {
        //Log.i(LOG_TAG, "Publisher Stream Created");

        hideDialog();
        setRefreshCount();
    }

    @Override
    public void onStreamDestroyed(PublisherKit publisherKit, Stream stream) {
        //Log.i(LOG_TAG, "Publisher Stream Destroyed");
    }

    @Override
    public void onError(PublisherKit publisherKit, OpentokError opentokError) {
        logOpenTokError(opentokError);
    }

    /* Archive Listener methods */
    @Override
    public void onArchiveStarted(Session session, String archiveId, String archiveName) {

//        mCurrentArchiveId = archiveId;
        //Log.i(LOG_TAG, mCurrentArchiveId + "mCurrentArchiveId");

    }

    @Override
    public void onArchiveStopped(Session session, String archiveId) {


    }


    /* Signal Listener method */
    @Override
    public void onSignalReceived(Session session, String type, String data, Connection connection) {
        boolean remote = !connection.equals(mSession.getConnection());
        switch (type) {
            case SIGNAL_TYPE_CHAT:
                showMessage(data, remote);
                break;

            case SIGNAL_TYPE_IMAGE:
                generateHearts();
//                heartImageView.setVisibility(View.VISIBLE);
//                heartImageView.setImageResource(R.drawable.heart_selected);
//                heartImageView.startAnimation(pulse);
//                pulse.setAnimationListener(new Animation.AnimationListener() {
//
//                    @Override
//                    public void onAnimationStart(Animation animation) {
//
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animation animation) {
//                        heartImageView.setVisibility(View.GONE);
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//
//                    }
//                });

        }
    }

    private void showDialog() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!pDialog.isShowing())
                        pDialog.show();
                }
            });
        } catch (IllegalArgumentException e) {

        } catch (Exception e) {

        }
    }

    private void hideDialog() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (pDialog.isShowing())
                        pDialog.dismiss();
                }
            });
        } catch (IllegalArgumentException e) {

        } catch (Exception e) {

        }
    }


    @Override
    public void onResume() {
        super.onResume();
        setGoogleAnalyticScreen("Publish Live");
        networkStateReceiver = new NetworkStateReceiver(this);
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        if (mHandlerRefreshCount != null)
            mHandlerRefreshCount.removeCallbacks(mRunnableCounter);
        if(networkStateReceiver!=null){
            networkStateReceiver.removeListener(this);
            this.unregisterReceiver(networkStateReceiver);
        }
        callSessionCancel();
        super.onPause();
    }

    private void showMessage(final String mesg, final DialogInterface.OnClickListener okClicked) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new AlertDialog.Builder(PublishLiveActivity.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .setCancelable(false)
                            .show();
                }
            });
        } catch (WindowManager.BadTokenException e) {
        } catch (Exception e) {
        }
    }


    @Override
    public void onNetworkAvailable() {
        if (!firstFlag) {

            if(mPublisher!=null) {
                if (mPublisherViewContainer != null)
                    mPublisherViewContainer.removeView(mPublisher.getView());
                if (mSession != null)
                    mSession.disconnect();
                if (mPublisher != null)
                    mPublisher.destroy();
                mSession = null;
                mPublisher = null;
            }
            initializeSession();
            initializePublisher();
        }
    }

    @Override
    public void onNetworkUnavailable() {
// showDialog();
        reconnect.setVisibility(View.VISIBLE);
    }


    private void setGoogleAnalyticScreen(String screenName){
        MyApplication application = (MyApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.setClientId(DataStorage.getInstance().getUserUUID());
        mTracker.setAppVersion(""+ Utils.getInstance().getAppVersion(MyApplication.getAppContext()));
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    private void generateHearts(){
        try {
//    imageView.setGravity(Gravity.BOTTOM);
            final ImageView imageHearts = new ImageView(this);
            Animation pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);
            Random ran = new Random();
            int x = ran.nextInt(maximum);

            imageHearts.setImageResource(images[x]);
            imageHearts.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT|RelativeLayout.ALIGN_PARENT_BOTTOM);
//            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.setMargins(0, 0, 10, 0);
            imageHearts.setLayoutParams(params);
            imageHearts.startAnimation(pulse);
            pulse.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
//            imageView.removeView(imageHearts);
                    imageHearts.clearAnimation();
                    imageHearts.setVisibility(View.GONE);
//                messageLayout.removeView(imageHearts);imageHeart

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            heartLayout.addView(imageHearts);
        }catch (WindowManager.BadTokenException e){

        }catch (Exception e){

        }
    }

}
