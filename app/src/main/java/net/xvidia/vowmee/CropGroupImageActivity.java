
package net.xvidia.vowmee;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.theartofdev.edmodo.cropper.CropImageView;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.CheckNetworkConnection;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.storage.DataStorage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CropGroupImageActivity extends AppCompatActivity implements CropImageView.OnSetImageUriCompleteListener, CropImageView.OnGetCroppedImageCompleteListener {

    // Private Constants ///////////////////////////////////////////////////////////////////////////

    private final int THUMBNAIL_REQUIRED_SIZE = 70;
    private static final int ROTATE_NINETY_DEGREES = 90;



    private boolean createNewGroup;
//    public static String thumbnail;
    public static String groupImage;


    private CropImageView mCropImageView;
    private ImageView mCroppedImageView;
    private String selectedImagePath;
    private String groupName;
    private boolean croping;
    private Bitmap thumbnailProfile;
    Bitmap croppedImage;
    Button buttonCrop;
    Button buttonLoad;
    Button buttonSave;
    Button buttonCancel;
    MenuItem menuCrop;
    MenuItem menuLoad;
    MenuItem menuSave;
    MenuItem menuCancel;
    // Activity Methods ////////////////////////////////////////////////////////////////////////////

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_layout);

        mCropImageView = (CropImageView) findViewById(R.id.CropImageView);
        mCroppedImageView = (ImageView) findViewById(R.id.CroppedImageView);

        croping= false;
        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            createNewGroup = intent.getBooleanExtra(AppConsatants.PROFILE_REGISTER, false);
            groupName = intent.getStringExtra(AppConsatants.NOTIFICATION_FRIEND_USERNAME);
        }
        if(!createNewGroup && groupImage != null && !groupImage.isEmpty()) {
            selectedImagePath = Utils.getInstance().getLocalContentPath(groupImage);
        }else if(groupImage != null && !groupImage.isEmpty()){
            selectedImagePath = Utils.getInstance().getLocalImagePath(groupImage);
        }
        setCropImageViewOptions();
        Bitmap image = Utils.getInstance().loadImageFromStorage(selectedImagePath, false);
        if(image != null) {
            mCropImageView.setImageBitmap(image);
        }else {
             mCropImageView.setImageResource(R.drawable.user);
        }
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(groupName);
        mToolbar.setPadding(0, 0, 0, 0);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    public void setCropImageViewOptions() {

        mCroppedImageView.setVisibility(View.GONE);
        mCropImageView.setVisibility(View.VISIBLE);
        mCropImageView.setScaleType(CropImageView.ScaleType.CENTER_INSIDE);
        mCropImageView.setCropShape(CropImageView.CropShape.RECTANGLE);
        mCropImageView.setGuidelines(CropImageView.Guidelines.OFF);
        mCropImageView.setAspectRatio(1, 1);
        mCropImageView.setFixedAspectRatio(true);
        mCropImageView.setShowCropOverlay(true);
        mCropImageView.setShowProgressBar(true);
        mCropImageView.setAutoZoomEnabled(true);
        mCropImageView.setMaxZoom(2);
        mCropImageView.setCropRect(new Rect(100, 300, 500, 1200));
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        setSaveVisible(false);
    }
    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedImageUri = null;
        if (resultCode == RESULT_OK) {

                selectedImageUri = getPickImageResultUri(data);
                try {
                    selectedImagePath =Utils.getInstance().getPath(this, selectedImageUri, true);
//                    Uri uriImage = Uri.parse(selectedImagePath);
                    mCropImageView.setImageUriAsync(selectedImageUri);
//                    mCropImageView.setImageBitmap(Utils.getInstance().loadImageFromStorage(selectedImagePath,false));
                } catch (Exception e) {
                    e.printStackTrace();
                }
//            }

        }
    }
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == Activity.RESULT_OK) {
//            Uri imageUri = getPickImageResultUri(data);
//            selectedImagePath = imageUri.getPath();
//            mCropImageView.setImageUriAsync(imageUri);
////            ((CropImageView) findViewById(R.id.CropImageView)).setImageUriAsync(imageUri);
//        }
//    }

    /**
     * Create a chooser intent to select the source to get image from.<br/>
     * The source can be camera's (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).<br/>
     * All possible sources are added to the intent chooser.
     */
        public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = Uri.fromFile(new File(Utils.getInstance().getLocalImagePath(groupImage)));//getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
//        Uri outputFileUri = Uri.fromFile(new File(Utils.getInstance().getLocalImagePath()));
//        File getImage = getExternalCacheDir();
//        if (getImage != null) {
//            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
            outputFileUri = Uri.fromFile(new File(Utils.getInstance().getLocalImagePath(groupImage)));
//        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private void uploadProfileImage(){

        UserFileManager userFileManager=Utils.getInstance().getUserFileManager();

        if(userFileManager != null){

            final String path = selectedImagePath;

            if(path == null)
                return;
            if(path.isEmpty())
                return ;
            final File file = new File(path);

            userFileManager.uploadContent(file, groupImage, new ContentProgressListener() {
                @Override
                public void onSuccess(final ContentItem contentItem) {
                    File file = new File(path);
                    if (file.exists()) {
                        Bitmap image1 = Utils.getInstance().loadImageFromStorage(groupImage, false);
                        if (image1 != null)
                            FileCache.getInstance().addBitmapToMemoryCache(groupImage, image1);
                    }
                    Utils.getInstance().storeImage(croppedImage, false, groupImage);
                    thumbnailProfile = Utils.getInstance().getThumbnail(selectedImagePath, THUMBNAIL_REQUIRED_SIZE);

                    Utils.getInstance().storeImage(thumbnailProfile, true, groupImage);
                    uploadThumbnailProfileImage(false);
                }

                @Override
                public void onProgressUpdate(final String fileName, final boolean isWaiting,
                                             final long bytesCurrent, final long bytesTotal) {

                }

                @Override
                public void onError(final String fileName, final Exception ex) {
                    croping=false;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            enableButtons(true);
//                            mProgressBar.setVisibility(View.GONE);
                        }
                    });
                    Utils.getInstance().displayToast(CropGroupImageActivity.this,"update of image failed",Utils.SHORT_TOAST);
//                    Log.i("FIle uploaded error", ex.getMessage());
                }
            });
        }
    }

    private void uploadThumbnailProfileImage(boolean showDialog){
        UserFileManager userFileManager=Utils.getInstance().getUserFileManager();

        if(userFileManager != null){
            String thumbnail = groupImage;
            thumbnail = thumbnail.replace(AppConsatants.FILE_EXTENSION_JPG,AppConsatants.THUMBNAIL_EXTENSION_JPG);
//            final String thumbnailPath = thumbnail;
            final String path = Utils.getInstance().getLocalThumbnailImagePath(thumbnail);
            if(path != null && path.isEmpty())
                return;
            final File file = new File(path);
            userFileManager.uploadContent(file, thumbnail, new ContentProgressListener() {
                @Override
                public void onSuccess(final ContentItem contentItem) {
//                    Log.i("FIle uploaded", contentItem.getFilePath());
                    File file = new File(path);
                    if (file.exists()) {
                        Bitmap image1 = Utils.getInstance().loadImageFromStorage(contentItem.getFilePath(), false);
                        if (image1 != null)
                            FileCache.getInstance().addBitmapToMemoryCache(contentItem.getFilePath(), image1);
                    }
                    croping=false;
                    Utils.getInstance().storeImage(thumbnailProfile, true, contentItem.getFilePath());
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            mProgressBar.setVisibility(View.GONE);
//                            finish();
//                        }
//                    });

                }

                @Override
                public void onProgressUpdate(final String fileName, final boolean isWaiting,
                                             final long bytesCurrent, final long bytesTotal) {
//                    dialog.setProgress((int) bytesCurrent);
                }

                @Override
                public void onError(final String fileName, final Exception ex) {
                    croping=false;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            enableButtons(true);
//                            mProgressBar.setVisibility(View.GONE);
                            finish();
                        }
                    });
//                    showError(R.string.app_name,
//                            "update of image failed");
//                    Log.i("FIle uploaded error", ex.getMessage());
                }
            });
        }
    }
    private void showError(final int resId, final String mesg) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new AlertDialog.Builder(CropGroupImageActivity.this).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                }
            });
        }catch (WindowManager.BadTokenException e){

        }catch (Exception e){

        }
    }

    @Override
    public void onGetCroppedImageComplete(CropImageView view, Bitmap bitmap, Exception error) {
        if (error == null) {
            croppedImage = bitmap;
            setSaveVisible(true);
            mCroppedImageView.setVisibility(View.VISIBLE);
            mCropImageView.setVisibility(View.GONE);
            mCroppedImageView.setImageBitmap(croppedImage);

        } else {
            croping= false;
            Utils.getInstance().displayToast(MyApplication.getAppContext(), "Image crop failed: " + error.getMessage(),Utils.LONG_TOAST);
        }
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if (error != null) {
            Utils.getInstance().displayToast(MyApplication.getAppContext(), "Image load failed: " + error.getMessage(), Utils.LONG_TOAST);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mCropImageView.setOnSetImageUriCompleteListener(this);
        mCropImageView.setOnGetCroppedImageCompleteListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mCropImageView.setOnSetImageUriCompleteListener(null);
        mCropImageView.setOnGetCroppedImageCompleteListener(null);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_crop, menu);
        if (menu != null) {
            menuCrop = menu.findItem(R.id.main_action_crop);
            MenuItemCompat.setActionView(menuCrop, R.layout.group_next);
            View viewCrop =  MenuItemCompat.getActionView(menuCrop);

            menuLoad = menu.findItem(R.id.main_action_load);
            MenuItemCompat.setActionView(menuLoad, R.layout.group_next);
            View viewLoad=  MenuItemCompat.getActionView(menuLoad);

            menuSave = menu.findItem(R.id.main_action_save);
            MenuItemCompat.setActionView(menuSave, R.layout.group_next);
            View viewSave=  MenuItemCompat.getActionView(menuSave);

            menuCancel = menu.findItem(R.id.main_action_cancel);
            MenuItemCompat.setActionView(menuCancel, R.layout.group_next);
            View viewCancel=  MenuItemCompat.getActionView(menuCancel);
//            View viewNext =menuNext.getActionView();
            buttonCrop = (Button) viewCrop.findViewById(R.id.group_next);
            buttonCrop.setText(getString(R.string.action_crop));
            buttonCrop.setTextColor(getResources().getColor(R.color.white));
            buttonCrop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCropImageView.getCroppedImageAsync();
                }
            });

            buttonLoad = (Button) viewLoad.findViewById(R.id.group_next);
            buttonLoad.setText(getString(R.string.action_load));
            buttonLoad.setTextColor(getResources().getColor(R.color.white));
            buttonLoad.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }
            });
            buttonSave = (Button) viewSave.findViewById(R.id.group_next);
            buttonSave.setText(getString(R.string.action_save));
            buttonSave.setTextColor(getResources().getColor(R.color.white));
            buttonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (CheckNetworkConnection.isConnectionAvailable(CropGroupImageActivity.this)) {
                        Utils.getInstance().storeImage(croppedImage, false, groupImage);
//                        selectedImagePath = Utils.getInstance().getLocalImagePath(DataStorage.getInstance().getUsername());
                        DataStorage.getInstance().setGroupImagePath(selectedImagePath);
                        if (createNewGroup) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    onBackPressed();
                                }
                            });
                            croping = false;
                            return;
                        } else {
//                        uploadProfileImage();
                            Utils.getInstance().uploadProfileImage(false, DataStorage.getInstance().getGroupImagePath(), groupImage);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    onBackPressed();
                                }
                            });
                        }
                    }else{
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_no_internet), Utils.LONG_TOAST);

                    }
                }
            });

            buttonCancel = (Button) viewCancel.findViewById(R.id.group_next);
            buttonCancel.setText(getString(R.string.action_cancel));
            buttonCancel.setTextColor(getResources().getColor(R.color.white));
            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mCroppedImageView.setVisibility(View.GONE);
                    mCropImageView.setVisibility(View.VISIBLE);
                    Bitmap image = Utils.getInstance().loadImageFromStorage(selectedImagePath, false);
                    if(image != null) {
                        mCropImageView.setImageBitmap(image);
                    }else {
                        mCropImageView.setImageResource(R.drawable.user);
                    }
                    setSaveVisible(false);
                }
            });

            setSaveVisible(false);

        }
        return true;
    }

    private void setSaveVisible(boolean save){

        if (menuCrop != null)
            menuCrop.setVisible(!save);
        if (menuLoad != null)
            menuLoad.setVisible(!save);
        if (menuSave != null)
            menuSave.setVisible(save);
        if (menuCancel != null)
            menuCancel.setVisible(save);
        if(save) {
            if (buttonCrop != null)
                buttonCrop.setVisibility(View.GONE);
            if (buttonLoad != null)
                buttonLoad.setVisibility(View.GONE);
            if (buttonSave != null)
                buttonSave.setVisibility(View.VISIBLE);
            if (buttonCancel != null)
                buttonCancel.setVisibility(View.VISIBLE);

        }else{
            if (buttonCrop != null)
                buttonCrop.setVisibility(View.VISIBLE);
            if (buttonLoad != null)
                buttonLoad.setVisibility(View.VISIBLE);
            if (buttonSave != null)
                buttonSave.setVisibility(View.GONE);
            if (buttonCancel != null)
                buttonCancel.setVisibility(View.GONE);
        }
    }

}
