package net.xvidia.vowmee;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.amazonaws.mobile.AWSConfiguration;
import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.mobile.content.UserFileManager;
import com.amazonaws.mobile.user.IdentityManager;
import com.amazonaws.mobile.user.IdentityProvider;
import com.amazonaws.mobile.user.signin.SignInManager;
import com.amazonaws.mobile.user.signin.SignInProvider;
import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.CheckNetworkConnection;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.gcm.GCMRegistrationIntentService;
import net.xvidia.vowmee.gcm.MyInstanceIDListenerService;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.Login;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.network.model.ResponseSocialAuth;
import net.xvidia.vowmee.network.model.SocialAuthFacebook;
import net.xvidia.vowmee.network.model.SocialAuthTwitter;
import net.xvidia.vowmee.storage.DataStorage;
import net.xvidia.vowmee.storage.sqlite.manager.DataCacheManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

public class Splash extends Activity {
    /**
     * Called when the activity is first created.
     */
    private final static String LOG_TAG = Splash.class.getSimpleName();
    private SignInManager signInManager;
    private ProgressDialog progressDialog;
    private JsonObjectRequest request = null;
//    private ImageView loadingView;
    boolean cancelPopup;
    /*GCM Regisration*/

    GoogleCloudMessaging gcm;
    String regId;

    /**
     * SignInResultsHandler handles the results from sign-in for a previously signed in user.
     */
    private class SignInResultsHandler implements IdentityManager.SignInResultsHandler {
        /**
         * Receives the successful sign-in result for an alraedy signed in user and starts the main
         * activity.
         *
         * @param provider the identity provider used for sign-in.
         */
        @Override
        public void onSuccess(final IdentityProvider provider) {


            AWSMobileClient.defaultMobileClient()
                    .getIdentityManager()
                    .loadUserInfoAndImage(provider, new Runnable() {
                        @Override
                        public void run() {
                            hideProgressBar();
                            String bucket = AWSConfiguration.AMAZON_S3_USER_FILES_BUCKET;
                            String prefix = "public/";
                            AWSMobileClient.defaultMobileClient()
                                    .createUserFileManager(bucket,
                                            prefix,
                                            new UserFileManager.BuilderResultHandler() {

                                                @Override
                                                public void onComplete(final UserFileManager userFileManager) {
                                                    long maxCachesize = 1024 * 1024 * 120;
                                                    userFileManager.setContentCacheSize(maxCachesize);
                                                    Utils.getInstance().setUserFileManager(userFileManager);
//													if(provider.getDisplayName().equalsIgnoreCase("Digit")) {
//														getProfileRequest();
//													}
//														sendFacebookRegisterRequest(provider.getFacebookUserId(), provider.getToken(), provider.getFacbookTokenExpiryDate());
//													}else if(provider.getDisplayName().equalsIgnoreCase("Twitter")){
//														sendTwitterRegisterRequest(provider.getTwitterUserId(), provider.getToken(), provider.getSecreteToken());
//
//													}
                                                }
                                            });
                        }
                    });


        }

        /**
         * For the case where the user previously was signed in, and an attempt is made to sign the
         * user back in again, there is not an option for the user to cancel, so this is overriden
         * as a stub.
         *
         * @param provider the identity provider with which the user attempted sign-in.
         */
        @Override
        public void onCancel(final IdentityProvider provider) {
//            Log.wtf(LOG_TAG, "Cancel can't happen when handling a previously sign-in user.");
//			if(DataStorage.getInstance().getAppSocialId().isEmpty() && !DataStorage.getInstance().getRegisteredFlag()){
//				goSignIn();
//			}else{
//				if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_FACEBOOK)){
//					SocialAuthFacebook obj = DataCacheManager.getInstance().getFacebookData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && !obj.getUserId().isEmpty()){
//						sendFacebookRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getExpiryDate());
//					}else{
//						goSignIn();
//					}
//				}else if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_TWITTER)){
//					SocialAuthTwitter obj = DataCacheManager.getInstance().getTwitterData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && obj.getUserId()>0){
//						sendTwitterRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getAccessSecret());
//					}else{
//						goSignIn();
//					}
//				}else{
//					goSignIn();
//				}
//			}
//			hideProgressBar();
        }

        /**
         * Receives the sign-in result that an error occurred signing in with the previously signed
         * in provider and re-directs the user to the sign-in activity to sign in again.
         *
         * @param provider the identity provider with which the user attempted sign-in.
         * @param ex       the exception that occurred.
         */
        @Override
        public void onError(final IdentityProvider provider, Exception ex) {
//            Log.e(LOG_TAG,
//                    String.format("Cognito credentials refresh with %s provider failed. Error: %s",
//                            provider.getDisplayName(), ex.getMessage()), ex);
//
//			Toast.makeText(Splash.this, String.format("Sign-in with %s failed.",
//					provider.getDisplayName()), Toast.LENGTH_LONG).show();
//			if(DataStorage.getInstance().getAppSocialId().isEmpty() && !DataStorage.getInstance().getRegisteredFlag()){
//				goSignIn();
//			}else{
//				if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_FACEBOOK)){
//					SocialAuthFacebook obj = DataCacheManager.getInstance().getFacebookData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && !obj.getUserId().isEmpty()){
//						sendFacebookRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getExpiryDate());
//					}else{
//						goSignIn();
//					}
//				}else if(DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_TWITTER)){
//					SocialAuthTwitter obj = DataCacheManager.getInstance().getTwitterData(DataStorage.getInstance().getAppSocialId());
//					if(obj!= null && obj.getUserId()>0){
//						sendTwitterRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getAccessSecret());
//					}else{
//						goSignIn();
//					}
//				}else{
//					goSignIn();
//				}
//			}

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        if (!DataStorage.getInstance().getAppSocialId().isEmpty() && DataStorage.getInstance().getRegisteredFlag()
                && !DataStorage.getInstance().getUsername().isEmpty()) {
            openTimeLine();
            return;
        }
        setContentView(R.layout.splash);
        initilaiseGcm();
        signInManager = new SignInManager(this);
//        loadingView = (ImageView) findViewById(R.id.loading);
        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotate);
        rotation.setRepeatCount(Animation.INFINITE);
//        if (loadingView != null) {
////			loadingView.startAnimation(rotation);
//            loadingView.setVisibility(View.GONE);
//        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (DataStorage.getInstance().getAppSocialId().isEmpty() && !DataStorage.getInstance().getRegisteredFlag()) {
                    goSignIn();
                } else {
                    if (CheckNetworkConnection.isConnectionAvailable(Splash.this)) {
                        final SignInProvider provider = signInManager.getPreviouslySignedInProvider();
                        // if the user was already previously in to a provider.
                        if (provider != null) {
                            // asyncronously handle refreshing credentials and call our handler.
                            if (DataStorage.getInstance().getRegisteredFlag()) {
                                signInManager.refreshCredentialsWithProvider(Splash.this,
                                        provider, new SignInResultsHandler());
                                loginUser();
                            } else {
                                provider.signOut();
                                goSignIn();
                            }
                        } else {
//						provider.signOut();
                            goSignIn();
                            return;
                        }
                    } else {
                        loginUser();
                    }
                }
            }
        }, 1500);
    }


    private void loginUser() {
        if (DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_FACEBOOK)) {
            SocialAuthFacebook obj = DataCacheManager.getInstance().getFacebookData(DataStorage.getInstance().getAppSocialId());
            if (obj != null && !obj.getUserId().isEmpty()) {
                sendFacebookRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getExpiryDate());
            } else {
                goSignIn();
            }
        } else if (DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_TWITTER)) {
            SocialAuthTwitter obj = DataCacheManager.getInstance().getTwitterData(DataStorage.getInstance().getAppSocialId());
            if (obj != null && obj.getUserId() > 0) {
                sendTwitterRegisterRequest(obj.getUserId(), obj.getAccessToken(), obj.getAccessSecret());
            } else {
                goSignIn();
            }
        } else if (DataStorage.getInstance().getLastLoginType().equals(AppConsatants.PROFILE_PHONE)) {
            Map<String, String> headers = new HashMap<>();
            headers.put(AppConsatants.DIGIT_HEADER_PROVIDER, DataStorage.getInstance().getDigitHeaderProvider());
            headers.put(AppConsatants.DIGIT_HEADER_AUTH, DataStorage.getInstance().getDigitHeaderAuth());
            if (headers != null && !DataStorage.getInstance().getUsername().isEmpty()) {
                sendDigits(headers, DataStorage.getInstance().getUsername());
            } else {
                goSignIn();
            }
        } else {
            goSignIn();
        }
    }

    private void initilaiseGcm() {
        if (DataStorage.getInstance().getGcmRegId(this).isEmpty()) {
            Intent intent = new Intent(this, GCMRegistrationIntentService.class);
            startService(intent);
        }
        Intent i = new Intent(getApplicationContext(), MyInstanceIDListenerService.class);
        startService(i);

    }


    /**
     * Starts an activity after the splash timeout.
     *
     * @param intent the intent to start the activity.
     */
    private void goAfterSplashTimeout(final Intent intent) {
//		final Thread thread = new Thread(new Runnable() {
//			public void run() {
        // wait for the splash timeout expiry or for the user to tap.
//				try {
////					timeoutLatch.await();
//				} catch (InterruptedException e) {
//				}

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        Splash.this.runOnUiThread(new Runnable() {
            public void run() {
                startActivity(intent);
                // finish should always be called on the main thread.
                finish();
            }
        });
//			}
//		});
//		thread.start();
    }


    /**
     * Go to the sign in activity after the splash timeout has expired.
     */
    protected void goSignIn() {
//        Log.d(LOG_TAG, "Launching Sign-in Activity...");
//        loadingView.setVisibility(View.GONE);
//		signInManager.setResultsHandler(Splash.this, new SignInResultsHandler());
        goAfterSplashTimeout(new Intent(this, RegisterActivity.class));
//		if(DataStorage.getInstance().getRegisteredFlag()) {
//			goAfterSplashTimeout(new Intent(this, RegisterActivity.class));
//		}else{
//			goAfterSplashTimeout(new Intent(this, RegisterActivity.class));
//
//		}
    }

    @Override
    protected void onResume() {
        super.onResume();

        // pause/resume Mobile Analytics collection
//		AWSMobileClient.defaultMobileClient().handleOnResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // pause/resume Mobile Analytics collection
//		AWSMobileClient.defaultMobileClient().handleOnPause();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void openTimeLine() {
        Intent intent = new Intent(Splash.this, TimelineTabs.class);
        startActivity(intent);
        finish();
    }



    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        this.finish();
    }

    /*Api Request & Response*/
    private void sendDigits(final Map<String, String> headers, final String phone) {
        try {

            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_DIGIT_LOGIN);
            Login login = new Login(phone, phone);
            login.setUsername(phone);
            login.setPassword(phone);
            login.setPhoneNumber(phone);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(login);
            } catch (IOException e) {
                e.printStackTrace();
            }
            request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    hideProgressBar();
                    String getUsername = obj.getUsername();
//				   Map<String, String> headers = request.getResponseHeaders();
                    if (getUsername != null && !getUsername.isEmpty()) {
                        DataStorage.getInstance().setRegisteredFlag(true);
                        DataStorage.getInstance().setUsername(obj.getUsername());
                        DataStorage.getInstance().setUserUUID(obj.getUuid());
                        DataStorage.getInstance().setLastLoginType(AppConsatants.PROFILE_PHONE);
//					   signInManager.refreshCredentialsWithProvider(Splash.this,
//							   new VowmeeSignInProvider(), new SignInResultsHandler());
                        String gcmToken = DataStorage.getInstance().getGcmRegId(Splash.this);
                        if (!gcmToken.isEmpty()) {
                            sendRegistrationToServer(MyApplication.getAppContext(), gcmToken, getUsername);
                        }

                        getProfileRequest();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND) {
                        cancelPopup = true;
                        DataStorage.getInstance().setRegisteredFlag(false);
                        Intent mIntent = new Intent(Splash.this, ProfileActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString(AppConsatants.PROFILE_FACEBOOK, "");
                        mBundle.putLong(AppConsatants.PROFILE_TWITTER, 0);
                        mBundle.putString(AppConsatants.PROFILE_PHONE, phone);
                        mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, true);
                        mIntent.putExtras(mBundle);
                        startActivity(mIntent);
                        finish();
                    } else {
//					   Log.e("VolleyError ", "" + error.getMessage());
                        goSignIn();
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();

                    String provider = headers.get("X-Auth-Service-Provider");
                    String Authorization = headers.get("X-Verify-Credentials-Authorization");
                    params = headers;
                    return params;
                }

                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
//						final long cacheHitButRefreshed = 1 *30 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//						final long cacheExpired = 10*24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONObject(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONObject response) {
                    super.deliverResponse(response);
//                    Log.e("VolleyError ", "" + response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void sendFacebookRegisterRequest(final String userId, String accessToken, String expiryDate) {
        try {

            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_AUTH_FACEBOOK);
            SocialAuthFacebook socialAuthFacebook = new SocialAuthFacebook();
            socialAuthFacebook.setAccessToken(accessToken);
            socialAuthFacebook.setUserId(userId);
            socialAuthFacebook.setExpiryDate(expiryDate);
            DataCacheManager.getInstance().clearSocialAuthFacebook();
            DataCacheManager.getInstance().saveSigninFacebookData(socialAuthFacebook);
            DataStorage.getInstance().setLastLoginType(AppConsatants.PROFILE_FACEBOOK);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(socialAuthFacebook);
            } catch (IOException e) {
                e.printStackTrace();
            }
            request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseSocialAuth obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), ResponseSocialAuth.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    String getUsername = obj.getUsername();
//					Map<String, String> headers = request.getResponseHeaders();
                    if (!getUsername.isEmpty()) {
                        DataStorage.getInstance().setRegisteredFlag(true);
                        DataStorage.getInstance().setUsername(obj.getUsername());
                        String gcmToken = DataStorage.getInstance().getGcmRegId(Splash.this);
                        if (!gcmToken.isEmpty()) {
                            sendRegistrationToServer(MyApplication.getAppContext(), gcmToken, getUsername);
                        }
                        getProfileRequest();
                    } else {
                        hideProgressBar();
                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND) {
//                        Log.e("VolleyError ", "" + error.networkResponse.statusCode);
                        cancelPopup = true;
                        DataStorage.getInstance().setRegisteredFlag(false);
                        Intent mIntent = new Intent(Splash.this, ProfileActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString(AppConsatants.PROFILE_FACEBOOK, userId);
                        mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, true);
                        mIntent.putExtras(mBundle);
                        startActivity(mIntent);
                        finish();
                    } else if (error != null && error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            showError("Oops. Timeout error!", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                        } else {
                            showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                        }
                    } else {
                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                    }
                }
            }) {
                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
//						final long cacheHitButRefreshed = 1 *30 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//						final long cacheExpired = 10*24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONObject(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONObject response) {
                    super.deliverResponse(response);
//                    Log.e("VolleyError ", "" + response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }


    private void sendTwitterRegisterRequest(final long userId, String accessToken, String accessSecret) {
        try {

            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_AUTH_TWITTER);
            SocialAuthTwitter socialAuthTwitter = new SocialAuthTwitter();
            socialAuthTwitter.setAccessToken(accessToken);
            socialAuthTwitter.setUserId(userId);
            socialAuthTwitter.setAccessSecret(accessSecret);
            DataCacheManager.getInstance().clearSocialAuthTwitter();
            DataCacheManager.getInstance().saveSigninTwitterData(socialAuthTwitter);
            DataStorage.getInstance().setLastLoginType(AppConsatants.PROFILE_TWITTER);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(socialAuthTwitter);
            } catch (IOException e) {
                e.printStackTrace();
            }
            request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseSocialAuth obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), ResponseSocialAuth.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    String getUsername = obj.getUsername();
                    Map<String, String> headers = request.getResponseHeaders();
                    hideProgressBar();
                    if (!getUsername.isEmpty()) {
                        DataStorage.getInstance().setRegisteredFlag(true);
                        DataStorage.getInstance().setUsername(obj.getUsername());
                        String gcmToken = DataStorage.getInstance().getGcmRegId(Splash.this);
                        if (!gcmToken.isEmpty()) {
                            sendRegistrationToServer(MyApplication.getAppContext(), gcmToken, getUsername);
                        }
                        getProfileRequest();
                    } else {
                        hideProgressBar();
                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND) {
//                        Log.e("VolleyError ", "" + error.networkResponse.statusCode);
                        cancelPopup = true;
                        DataStorage.getInstance().setRegisteredFlag(false);
                        Intent mIntent = new Intent(Splash.this, ProfileActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putLong(AppConsatants.PROFILE_TWITTER, userId);
                        mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, true);
                        mIntent.putExtras(mBundle);
                        startActivity(mIntent);
                    } else {
                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                    }
                }
            }) {
                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
//						final long cacheHitButRefreshed = 1 *30 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//						final long cacheExpired = 10*24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONObject(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONObject response) {
                    super.deliverResponse(response);
//                    Log.e("VolleyError ", "" + response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void sendRegistrationToServer(final Context context, final String token, String username) {
        if (token == null || context == null)
            return;
        String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_UPDATE_GCM);
        Profile profile = new Profile();
        profile.setUsername(username);
        profile.setGcmId(token);
        ObjectMapper mapper = new ObjectMapper();
        String jsonObject = null;
        try {
            jsonObject = mapper.writeValueAsString(profile);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        Log.d("GCM UPDATE", "GCM ToServer: " + profile.toString());
        JsonObjectRequest ObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
//                Log.d("GCM UPDATE", "Response: " + response.toString());

                if (response.toString().isEmpty() || response.toString().equals(null)) {
//                    Toast.makeText(context, "Register error. Try again.", Toast.LENGTH_LONG);
                } else {

                    //Toast.makeText(context, "Successfully registered with server", Toast.LENGTH_LONG).show();

                    try {
                        DataStorage.getInstance().storeRegistrationIdOnServer(context, true);
                    } catch (Exception e) {
                        // JSON error
                        e.printStackTrace();
//                        Toast.makeText(context, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//				Log.e("GCM UPDATE", "Login Error: " + error !=null? error.getMessage():"");
//				Toast.makeText(context,
//						"Error! Check your internet connection.", Toast.LENGTH_LONG).show();

            }
        });

        // Adding request to request queue
        VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(ObjReq);
        DataStorage.getInstance().storeRegistrationIdOnServer(this, true);
    }

    private void getProfileRequest() {
        try {
//	showProgressBar(getString(R.string.progressbar_fetching_info));
            final String username = DataStorage.getInstance().getUsername();//"F_815817965196060";//
            String url = ServiceURLManager.getInstance().getProfileUrl(IAPIConstants.API_KEY_GET_PROFILE, username, username);

            request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    String getUsername = obj.getUsername();
                    Map<String, String> headers = request.getResponseHeaders();
                    hideProgressBar();
                    if (getUsername != null && !getUsername.isEmpty() && username.equals(getUsername)) {

                        ModelManager.getInstance().setOwnProfile(obj);
                        DataStorage.getInstance().setUserUUID(obj.getUuid());
//						Log.i("X-AUTH-TOKEN ", "" + headers.get("X-AUTH-TOKEN"));
//						if(Utils.getInstance().getIntegerValueOfString(obj.getPosts())>0){
//							startActivity(new Intent(Splash.this, TimelineTabs.class));
//						}else{
//							startActivity(new Intent(Splash.this, TimelineTabs.class));
//						}
//						finish();
                        cancelPopup = true;
                        openTimeLine();
                    } else {
//						showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//							@Override
//							public void onClick(DialogInterface dialog, int which) {
//								finish();
//							}
//						});
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                }
            }) {
                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONObject(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONObject response) {
                    super.deliverResponse(response);
//                    Log.e("VolleyError ", "" + response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void showError(final String mesg, final DialogInterface.OnClickListener okClicked) {
        try {
            if (cancelPopup)
                return;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(Splash.this,R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setCancelable(false)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .show();
                }
            });

        } catch (WindowManager.BadTokenException e) {

        } catch (Exception e) {

        }
    }

    private void showProgressBar(String msg) {
        progressDialog = ProgressDialog.show(this, null, null, true, false);
        progressDialog.setContentView(R.layout.progressbar);
        TextView progressBarMessage = (TextView) progressDialog.findViewById(R.id.progressBar_message);
        progressBarMessage.setText(msg);

    }

    private void hideProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (progressDialog != null) {
                    progressDialog.hide();
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
        });
    }


}
