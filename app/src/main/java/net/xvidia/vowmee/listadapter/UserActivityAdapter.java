package net.xvidia.vowmee.listadapter;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.Utils.ActivityTypes;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.MomentLink;
import net.xvidia.vowmee.Utils.UserNameLink;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.helper.CustomFontTextView;
import net.xvidia.vowmee.network.model.ActivityLog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserActivityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ILoadMoreItems iLoadMore;
    Context context;
    private List<ActivityLog> activityLogs;
    private ProgressDialog progressDialog;
    private boolean mOwnActivity;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;


    public void setActivityLogs(List<ActivityLog> activityLogs) {
        this.activityLogs = activityLogs;
    }
    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mActivityMessageTextView;
        private TextView mActivityDateTextView;
        private CircularImageView mProfileIMage;
        public ViewHolder(View v) {
            super(v);
            mActivityDateTextView = (TextView) v.findViewById(R.id.activity_message_date);
            mActivityMessageTextView = (TextView) v.findViewById(R.id.activity_message);
            mProfileIMage = (CircularImageView) v.findViewById(R.id.activity_person_image);

        }


    }
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public CustomFontTextView loadingMore;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            loadingMore = (CustomFontTextView) itemView.findViewById(R.id.load_more);
        }
    }
     public UserActivityAdapter(Context context, final List<ActivityLog> pendingList, boolean ownActivity) {

         this.context = context;
         this.activityLogs = pendingList;
         this.mOwnActivity = ownActivity;
     }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_notification_listitem, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_more_item, parent, false);
            return new LoadingViewHolder(view);
        }

        return null;

    }
    @Override
    public int getItemViewType(int position) {
        return activityLogs.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof ViewHolder) {
            final ActivityLog profile = activityLogs.get(position);
            final ViewHolder holder = (ViewHolder) viewHolder;
            holder.mActivityDateTextView.setText(Utils.getInstance().getDateDiffString(profile.getDate()));

            String commentsText = ActivityTypes.getActivityMessageString(profile);

            ArrayList<int[]> displayNameSpans = getSpans(commentsText, "@", 0);
            commentsText = commentsText.replace("@", "");
            ArrayList<int[]> momentSpans = getSpans(commentsText, "[", 1);
            commentsText = commentsText.replace("[", "");
            SpannableString commentsContent = new SpannableString(commentsText);

            for (int i = 0; i < momentSpans.size(); i++) {
                int[] span = momentSpans.get(i);
                int momentStart = span[0];
                int momentEnd = span[1];

                if (momentEnd < commentsContent.length())
                    commentsContent.setSpan(new MomentLink(context,mOwnActivity),
                            momentStart,
                            momentEnd, 0);

            }

            for (int i = 0; i < displayNameSpans.size(); i++) {
                int[] span = displayNameSpans.get(i);
                int displayNameStart = span[0];
                int displayNameEnd = span[1];
                if (displayNameEnd < commentsContent.length())
                    commentsContent.setSpan(new UserNameLink(context, mOwnActivity),
                            displayNameStart,
                            displayNameEnd, 0);

            }
            holder.mActivityMessageTextView.setMovementMethod(LinkMovementMethod.getInstance());
            holder.mActivityMessageTextView.setText(commentsContent);
            String profImagePath = profile.getInitiatorThumbNail();
            profImagePath = (profImagePath == null ? (profile.getInitiaterId()+ AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? profile.getInitiaterId() + AppConsatants.FILE_EXTENSION_JPG : profile.getReceiverThumbNail()));

            if(mOwnActivity) {
                profImagePath = profile.getReceiverThumbNail();
                profImagePath = (profImagePath == null ? (profile.getReceiverId()+ AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? profile.getReceiverId() + AppConsatants.FILE_EXTENSION_JPG : profile.getInitiatorThumbNail()));


            }
            holder.mProfileIMage.setTag(profImagePath);//tag of imageView == path to image
//                    new LoadImage(mItemViewHolder.mProfileImageView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            downloadContentFromS3Bucket(holder.mProfileIMage, profImagePath);

        } else if (viewHolder instanceof LoadingViewHolder) {
            final LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewHolder;
            loadingViewHolder.loadingMore.setText(MyApplication.getAppContext().getResources().getString(R.string.action_load_more));

            loadingViewHolder.loadingMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadingViewHolder.loadingMore.setText(MyApplication.getAppContext().getResources().getString(R.string.action_loading));
                    iLoadMore.loadMoreItems(true);
                }
            });
        }
    }

    public ArrayList<int[]> getSpans1(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<int[]>();

        Pattern pattern = Pattern.compile(prefix + "\\w+");
        Matcher matcher = pattern.matcher(body);

        // Check all occurrences
        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return  spans;
    }
    public ArrayList<int[]> getSpanMoments(String str, String prefix) {
        ArrayList<int[]> spans = new ArrayList<int[]>();
        int count = 0;
        int idx1 = str.indexOf(prefix);
        int idx2 = 0;
        while (idx1 != -1) {
            idx2 = str.indexOf(prefix, idx1+1);
            if(idx2<0)
                idx2=idx1+5;
            int[] currentSpan = new int[2];
            int start = idx1-count*2;
            int end = idx2-count*2;
            currentSpan[0] =start;
            currentSpan[1] = end;
            spans.add(currentSpan);
            idx1 = str.indexOf(prefix, idx2+1);
            count=count+1;
        }

        return  spans;
    }
    public ArrayList<int[]> getSpans(String str, String prefix,int mltp) {
        ArrayList<int[]> spans = new ArrayList<int[]>();
        int count =0;
        int idx1 = str.indexOf(prefix);
        int idx2 = 0;
        while (idx1 != -1) {
            idx2 = str.indexOf(prefix, idx1+1);
            if(idx2<0)
                idx2=idx1+5;
            int[] currentSpan = new int[2];
            int start = idx1;
            int end = idx2- 1;
            if(mltp>0 && count==0) {
                start = idx1 - 1;
                end = idx2 - 2;
            }else if(mltp>0 && count>=mltp){
                start = idx1 - 3;
                end = idx2 - 4;
            }
            currentSpan[0] =start;
            currentSpan[1] = end;
            spans.add(currentSpan);
            idx1 = str.indexOf(prefix, idx2+1);
            count=count+1;
        }

        return  spans;
    }
    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
//        Log.d(TAG, "onViewRecycledCalled");
        holder.setIsRecyclable(true);

    }

    @Override
    public int getItemCount() {
        return activityLogs == null ? 0 : activityLogs.size();
//        if(activityLogs == null)
//            return 0;
//        return activityLogs.size();
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            FileCache.getInstance().addBitmapToMemoryCache(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return  FileCache.getInstance().getBitmapFromMemCache(key);
    }
    private void downloadContentFromS3Bucket(ImageView imageView, String filePath){
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                } else {
                    imageView.setImageResource(R.drawable.user);
                }
            }
                download(filePath);
        }
    }
    private void  download(final String filePath){
        UserFileManager userFileManager = Utils.getInstance().getUserFileManager();
        if (userFileManager == null )
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                if (image1 != null)
                    addBitmapToMemoryCache(filePath, image1);
//                if(contentItem !=null &&!contentItem.getFilePath().isEmpty()) {
////                    new LoadImage(imageView).execute();
//                }

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }

    public void setOnLoadMoreListener(ILoadMoreItems iLoadMoreItems) {
        this.iLoadMore = iLoadMoreItems;
    }
}