package net.xvidia.vowmee.listadapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;

import net.xvidia.vowmee.GroupAddMemberActivity;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.SelectPrivateShareActivity;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.network.model.Profile;

import java.io.File;
import java.util.ArrayList;

public class GroupAddMemberListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    public static SparseBooleanArray selectedItems;
    private ArrayList<Profile> searchList;
    Context mContext;
    private UserFileManager userFileManager;
    private boolean shareFalg;

    public class ViewHolder extends RecyclerView.ViewHolder  {

        private CheckBox checkBox;
        private TextView mDisplayNameTextView;
        private CircularImageView mProfileImageView;
        private RelativeLayout memberItemLayout;

        public ViewHolder(View v) {
            super(v);
            mDisplayNameTextView = (TextView) v.findViewById(R.id.friend_name);
            mProfileImageView = (CircularImageView) v.findViewById(R.id.friend_image);
            checkBox = (CheckBox) v.findViewById(R.id.song_checkBox);
            memberItemLayout = (RelativeLayout)v.findViewById(R.id.member_item_layout);
        }


    }

    public GroupAddMemberListAdapter(Context context, final ArrayList<Profile> friendList,boolean sshareFlag) {

        this.mContext = context;
        this.searchList = friendList;
        this.shareFalg = sshareFlag;
        selectedItems = new SparseBooleanArray(friendList.size());
        userFileManager = Utils.getInstance().getUserFileManager();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_group_addmember_listitem, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }



    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        try {
            final Profile profile = searchList.get(position );
            final ViewHolder holder = (ViewHolder) viewHolder;
            holder.mDisplayNameTextView.setText(profile.getDisplayName());
            holder.memberItemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectedItems.get(position, false)) {
                        selectedItems.delete(position);
                        holder.checkBox.setChecked(false);
                    } else {
                        selectedItems.put(position, true);
                        holder.checkBox.setChecked(true);
                    }
                    if(!shareFalg) {
                        showNext();
                    }else{
                        SelectPrivateShareActivity.setSelectedMemberList();
                    }
//                    notifyDataSetChanged();
                }
            });

            holder.mProfileImageView.setVisibility(View.VISIBLE);
            holder.mDisplayNameTextView.setVisibility(View.VISIBLE);

//            Utils.getInstance().underlineTextView(profile.getDisplayName(), holder.mDisplayNameTextView);
//            String url = MyApplication.getAppContext().getString(R.string.s3_url);
//            if (profile.getThumbNail() == null) {
//                url = url + profile.getUsername() + "_thumbnail.jpg";
//            } else {
//                url = url + profile.getThumbNail();
//            }
//            url = url.replace("+","%2B");
//            Picasso.with(mContext).load(url)
//                    .error(R.drawable.user)
//                    .placeholder(R.drawable.user)
//                    .networkPolicy(NetworkPolicy.NO_CACHE)
//                    .into(holder.mProfileImageView);
            boolean checkedState =selectedItems.get(position, false);
            holder.checkBox.setChecked(checkedState);

            holder.checkBox.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (selectedItems.get(position, false)) {
                        selectedItems.delete(position);
                        holder.checkBox.setChecked(false);
                    } else {
                        selectedItems.put(position, true);
                        holder.checkBox.setChecked(true);
                    }
                    if(!shareFalg) {
                        showNext();
                    }else{
                        SelectPrivateShareActivity.setSelectedMemberList();
                    }
//                    notifyDataSetChanged();
                }
            });

            String profImagePath = profile.getThumbNail();
            profImagePath = (profImagePath == null ? (profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG : profile.getThumbNail()));
            holder.mProfileImageView.setTag(profImagePath);//tag of imageView == path to image
//            new LoadImage(holder.mProfileImageView).execute();
            downloadContentFromS3Bucket(holder.mProfileImageView, profImagePath);
        } catch (NullPointerException e) {

        } catch (Exception e) {

        }

    }

    private void showNext(){
        if(selectedItems.size()>0){
            GroupAddMemberActivity.showMenuIcon(true);
        }else{
            GroupAddMemberActivity.showMenuIcon(false);
        }
    }
    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        holder.setIsRecyclable(true);

    }

    @Override
    public int getItemCount() {
        if (searchList == null)
            return 0;
        return searchList.size();
    }
    private void downloadContentFromS3Bucket(ImageView imageView, String filePath){
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null) {
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                        imageView.setImageBitmap(image1);
//                        Animation myFadeInAnimation = AnimationUtils.loadAnimation(mContext, R.anim.fadein);
//                        imageView.startAnimation(myFadeInAnimation);
                    }else {
                        imageView.setImageResource(R.drawable.user);
                    }
                } else {
                    imageView.setImageResource(R.drawable.user);
                }
            }
//            if(!disableLoad)
            download(imageView, filePath);
        }
    }

    private void  download(final ImageView imageView, final String filePath){
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                }

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }

}