package net.xvidia.vowmee.listadapter;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import net.xvidia.vowmee.GroupProfileActivity;
import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.GroupProfile;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import static net.xvidia.vowmee.MyApplication.getAppContext;

public class SearchListGroupAdapter extends RecyclerView.Adapter<SearchListGroupAdapter.ViewHolder> {
    Context context;
    private List<GroupProfile> searchList;
    private ProgressDialog progressDialog;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mDisplayNameTextView;
        public CircularImageView mProfileImageView;
        public TextView mFollowButton;
        public TextView mJoinedButton;
        public boolean following = false;
        public boolean joined = false;

        public ViewHolder(View v) {
            super(v);
            mDisplayNameTextView = (TextView) v.findViewById(R.id.search_person_name);
            mProfileImageView = (CircularImageView) v.findViewById(R.id.search_person_image);
            mFollowButton = (TextView) v.findViewById(R.id.search_follow_button);
            mJoinedButton = (TextView) v.findViewById(R.id.search_friend_button);


        }


    }

    public SearchListGroupAdapter(Context context, final List<GroupProfile> searchList) {

        this.context = context;
        this.searchList = searchList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_search_group_listitem, parent, false);

//        Configuration configuration = context.getResources().getConfiguration();
//        int screenWidthDp = configuration.screenWidthDp; //The current width of the available screen space, in dp units, corresponding to screen width resource qualifier.
//        int smallestScreenWidthDp = configuration.smallestScreenWidthDp; //The smallest screen size an application will see in normal operation, corresponding to smallest screen width resource qualifier.

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final GroupProfile profile = searchList.get(position);
        holder.mDisplayNameTextView.setText(profile.getName());
//        holder.mStatusMessageTextView.setText(context.getResources().getString(R.string.share_group));
        int paddingLeftRight = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_10);
        int paddingTopBottom = Utils.getInstance().convertDimenToInt(R.dimen.button_text_padding);
        holder.mJoinedButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight,paddingTopBottom);
        holder.mFollowButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight,paddingTopBottom);
        holder.mDisplayNameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    ModelManager.getInstance().setGroupProfile(profile);
                    Intent intent = new Intent(context, GroupProfileActivity.class);
                    context.startActivity(intent);
            }
        });
        holder.mProfileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModelManager.getInstance().setGroupProfile(profile);
                Intent intent = new Intent(context, GroupProfileActivity.class);
                context.startActivity(intent);
            }
        });
        holder.mFollowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.following) {
                    holder.following = false;
                } else {
                    holder.following = true;
                }
                sendFollowRequest(holder,profile,position);

                enableButtons(holder,false);
            }
        });
        holder.mJoinedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.joined) {
                    holder.joined = true;
                    sendJoinRequest(holder,profile,position);
                }
                enableButtons(holder,false);
            }
        });



        if (setJoinStatus(profile)) {
            holder.joined = true;
            holder.mJoinedButton.setText(context.getString(R.string.group_joined));
            holder.mFollowButton.setVisibility(View.GONE);

        } else{
            if(profile.isMembershipRequested()){
                holder.joined = true;
                holder.mJoinedButton.setText(context.getString(R.string.action_request_sent_friend));
            }else{
                holder.mJoinedButton.setText(context.getString(R.string.group_join));
            }
            holder.mFollowButton.setVisibility(View.VISIBLE);
            if(profile.getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_FOLLOWER)){
                holder.following = true;
                holder.mFollowButton.setText(context.getString(R.string.action_unfollow));
            }else{
                holder.following = false;
                holder.mFollowButton.setText(context.getString(R.string.action_follow));
            }
        }
        holder.mProfileImageView.setTag(profile.getProfileImage());
        downloadContentFromS3Bucket(holder.mProfileImageView,profile);
//        Utils.getInstance().underlineTextView(profile.getDisplayName(), holder.mDisplayNameTextView);


    }
//    private void downloadContentFromS3Bucket(ViewHolder holder, GroupProfile groupProfile){
//        String filePath = groupProfile.getThumbNail();
//        if(filePath!=null && !filePath.isEmpty()) {
//            String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
//            File file = new File(filePathImage);
//            if (file.exists()) {
//                holder.mProfileImageView.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePathImage, false));
//            }else{
//                String url = MyApplication.getAppContext().getString(R.string.s3_url);
//                url = url+filePath;
//                url = url.replace("+","%2B");
//                Picasso.with(context).load(url)
//                        .error(R.drawable.group_default)
//                        .placeholder(R.drawable.group_default)
//                        .networkPolicy(NetworkPolicy.NO_CACHE)
//                        .into(holder.mProfileImageView);
//            }
//        }
//    }
    private void downloadContentFromS3Bucket(ImageView imageView, GroupProfile groupProfile){
        String filePath = groupProfile.getProfileImage();
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if (image != null) {
                imageView.setImageBitmap(image);
            } else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null) {
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                        imageView.setImageBitmap(image1);
//                        Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fadein);
//                        imageView.startAnimation(myFadeInAnimation);
                    } else {
                        imageView.setImageResource(R.drawable.user);
                    }
                } else {
                    String url = getAppContext().getString(R.string.s3_url);
                    String profImagePath = groupProfile.getThumbNail();
                    url = url + profImagePath;
                    url = url.replace("+", "%2B");
                    Picasso.with(context).load(url)
                            .error(R.drawable.user)
                            .placeholder(R.drawable.user)
//                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .into(imageView);
                }
            }
        }
    }
    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
//        Log.d(TAG, "onViewRecycledCalled");
        holder.setIsRecyclable(true);

    }

    @Override
    public int getItemCount() {
        if(searchList == null)
            return 0;
        return searchList.size();
    }

    private boolean setJoinStatus(GroupProfile profile) {
        boolean visible = false;

        if(profile.getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_OWNER)
                || profile.getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MODERATOR)
                || profile.getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MEMBER)){
            visible=true;
        }
        return visible;
    }


     private void sendFollowRequest(final ViewHolder holder,final GroupProfile profile,final int position){
         try {
             if(profile == null)
                 return;
             final boolean follow = holder.following;
             showProgressBar("Processing your request");
             final String username = DataStorage.getInstance().getUserUUID();
             final String grouUuid = profile.getUuid();
             String url = ServiceURLManager.getInstance().getGroupFollowRequestUrl(grouUuid,username);
             if(!follow)
                 url = ServiceURLManager.getInstance().getGroupUNFollowRequestUrl(grouUuid,username);
             JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                 @Override
                 public void onResponse(JSONObject response) {
                     hideProgressBar();
                     if (follow) {
                         profile.setRoleWithViewer(AppConsatants.GROUP_ROLE_FOLLOWER);
                     } else {
                         profile.setRoleWithViewer("");
                     }
                     try{
                         if (searchList != null && searchList.size() > 0) {
                             if(position<searchList.size()) {
                                 searchList.set(position, profile);
                                 notifyItemChanged(position);
                             }
                         }
                     }catch (IndexOutOfBoundsException e){

                     }catch (Exception e){

                     }
                     enableButtons(holder,true);
                 }
             }, new Response.ErrorListener() {

                 @Override
                 public void onErrorResponse(VolleyError error) {
                     Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                     hideProgressBar();
                     enableButtons(holder,true);
                 }
             });
             VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
         }catch(Exception e){
             e.printStackTrace();
         }
    }

    private void sendJoinRequest(final ViewHolder holder,final GroupProfile profile,final int position){
        try {
            if(profile == null)
                return;
            showProgressBar("Processing your request");
            final String username = DataStorage.getInstance().getUserUUID();
            final String grouUuid = profile.getUuid();
            String url = ServiceURLManager.getInstance().getGroupJoinUrl(grouUuid,username);

            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    try {
                        profile.setIsMembershipRequested(true);
                        if (searchList != null && searchList.size() > 0) {
                            if(position<searchList.size()) {
                                searchList.set(position, profile);
                                notifyItemChanged(position);
                            }
                        }
                    }catch (IndexOutOfBoundsException e){}
                    catch (Exception e){}

                    enableButtons(holder,true);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                    enableButtons(holder,true);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void showProgressBar(String msg) {

        progressDialog = ProgressDialog.show(context, null, null, true, false);
        progressDialog.setContentView(R.layout.progressbar);
        TextView  progressBarMessage = (TextView)progressDialog.findViewById(R.id.progressBar_message);
        progressBarMessage.setText(msg);

    }

    private void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
    private void enableButtons(ViewHolder holder, boolean enable) {
        if (holder != null) {
            holder.mFollowButton.setEnabled(enable);
            holder.mJoinedButton.setEnabled(enable);
        }
    }
}