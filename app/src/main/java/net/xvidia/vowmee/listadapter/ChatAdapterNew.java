package net.xvidia.vowmee.listadapter;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.network.model.ChatMessage;

import java.io.File;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vasu on 8/7/16.
 */
public class ChatAdapterNew extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int VIEW_TYPE_LOCAL = 0;
    public static final int VIEW_TYPE_REMOTE = 1;
    private static final Map<Integer, Integer> viewTypes;
    public static List<ChatMessage> chatList;
    public static LayoutInflater inflater = null;
    Context context;
    public static int deletePosition;

    static {
        Map<Integer, Integer> aMap = new HashMap<Integer, Integer>();
        aMap.put(VIEW_TYPE_LOCAL, R.layout.list_item_message_left);
        aMap.put(VIEW_TYPE_REMOTE, R.layout.list_item_message_left);
        viewTypes = Collections.unmodifiableMap(aMap);
    }

    public ChatAdapterNew(Context context,final List<ChatMessage> chatList) {

        this.context = context;
        this.chatList = chatList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder  {

        public TextView mDisplayNameTextView;
        public TextView mDaysLeftTextView;
        public TextView mCommentTextView;
        public CircularImageView mProfileImageView;
//        private LinearLayout parentLayout;
        private ObjectAnimator alpha;

        public ViewHolder(View itemView) {
            super(itemView);
            mDisplayNameTextView = (TextView) itemView.findViewById(R.id.comment_username);
//            parentLayout = (LinearLayout) itemView.findViewById(R.id.parentLayout);
//            viewHolder.mDaysLeftTextView = (TextView) itemLayoutView.findViewById(R.id.comment_date);
            mCommentTextView = (TextView) itemView.findViewById(R.id.comment);
            mProfileImageView = (CircularImageView) itemView.findViewById(R.id.comment_user_image);
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.timeline_live_comment_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }
    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
//        Log.d(TAG, "onViewRecycledCalled");
        holder.setIsRecyclable(true);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder,final int position) {

        final ViewHolder holder = (ViewHolder) viewHolder;

//        Animation animation = AnimationUtils.loadAnimation(context,
//                android.R.anim.slide_out_right);
//        animation.setDuration(5000);
////        animation.setStartTime(3000);
//        animation.setStartOffset(3000);
//        animation.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                if(position < chatList.size()) {
//                    chatList.remove(0);
//                    notifyDataSetChanged();
//                }
////                mMessageHistory.notifyDataSetChanged();
//            }
//        });
////
//        viewHolder.itemView.startAnimation(animation);

        final ChatMessage comment = chatList.get(position);
        holder.mDisplayNameTextView.setText(comment.getOwnerDisplayName());
//        viewHolder.mDaysLeftTextView.setText(Utils.getInstance().getDateDiffString(comment.getDate()));
        String text = comment.getMessageText();
        try{
            text= Utils.getInstance().replceLast(text, "<p dir=\"ltr\">", "");
            text=Utils.getInstance().replceLast(text, "</p>", "");
        }catch (Exception e) {}
        Spanned bar = Html.fromHtml(text, new Html.ImageGetter() {
            public Drawable getDrawable(String source) {
                Drawable d = new BitmapDrawable(context.getResources(), getImage(source));
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                return d;
            }
        }, null);
        holder.mCommentTextView.setText(bar);
//        String url = MyApplication.getAppContext().getString(R.string.s3_url);
//        url = url + comment.getOwner()+ AppConsatants.FILE_EXTENSION_JPG;
//        Picasso.with(context).load(url)
//                .error(R.drawable.user)
//                .placeholder(R.drawable.user)
//                .networkPolicy(NetworkPolicy.NO_CACHE)
//                .into(viewHolder.mProfileImageView);
        String profImagePath = comment.getOwner()+ AppConsatants.FILE_EXTENSION_JPG;
//        profImagePath = (profImagePath == null ? (profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG : profile.getThumbNail()));
        holder.mProfileImageView.setTag(profImagePath);//tag of imageView == path to image
//            new LoadImage(holder.mProfileImageView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        downloadContentFromS3Bucket(holder.mProfileImageView, profImagePath);

    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder1) {
        super.onViewAttachedToWindow(holder1);
        ViewHolder holder = (ViewHolder)holder1;
        if (holder.alpha == null) {
            holder.alpha = ObjectAnimator.ofFloat(holder.itemView, View.ALPHA, 1f, 0f);
            holder.alpha.setDuration(1000);
            holder.alpha.setStartDelay(4000);
            holder.alpha.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (chatList.size() > 0) {
                        chatList.remove(0);
                        notifyItemRemoved(0);
                    }
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
            holder.alpha.start();
        }
    }


    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder1) {
        super.onViewDetachedFromWindow(holder1);
        ViewHolder holder = (ViewHolder)holder1;
        if(holder.alpha != null) {
            holder.alpha.cancel();
            holder.alpha = null;
        }
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    private void downloadContentFromS3Bucket(ImageView imageView, String filePath){
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                } else {
                    String url = MyApplication.getAppContext().getString(R.string.s3_url);
                    url = url + filePath;
                    Picasso.with(context).load(url)
                            .error(R.drawable.user)
                            .placeholder(R.drawable.user)
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .into(imageView);
                }
            }
//            if(!disableLoad)
//            download(imageView, filePath);
        }
    }
    private Bitmap getImage(String path) {
        AssetManager mngr = context.getAssets();
        InputStream in = null;
        try {
            in = mngr.open("emoticons/" + path);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bitmap temp = BitmapFactory.decodeStream(in, null, null);
        return temp;
    }
}
