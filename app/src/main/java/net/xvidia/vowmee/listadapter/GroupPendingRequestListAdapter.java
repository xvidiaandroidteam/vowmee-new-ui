package net.xvidia.vowmee.listadapter;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.PendingRequestActivity;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.TimelineFriendProfile;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.helper.CustomFontTextView;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.GroupMembership;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class GroupPendingRequestListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static String TAG = "VideosAdapter";

    Context context;
    private List<GroupMembership> pendingList;
    private ProgressDialog progressDialog;

    class ViewHolder extends RecyclerView.ViewHolder {

        private CustomFontTextView mDisplayNameTextView;
        private CircularImageView mProfileImageView;
        private CustomFontTextView mAcceptButton;
        private CustomFontTextView mDeclineButton;

        public ViewHolder(View v) {
            super(v);
            mDisplayNameTextView = (CustomFontTextView) v.findViewById(R.id.pending_person_name);
            mProfileImageView = (CircularImageView) v.findViewById(R.id.pending_person_image);
            mAcceptButton = (CustomFontTextView) v.findViewById(R.id.accept_button);
            mDeclineButton = (CustomFontTextView) v.findViewById(R.id.decline_button);

        }


    }

     public GroupPendingRequestListAdapter(Context context, final List<GroupMembership> pendingList) {

         this.context = context;
         this.pendingList = pendingList;
     }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_pending_listitem, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);

            return viewHolder;

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder viewHolder, final int position) {
        try {
            final GroupMembership profile = pendingList.get(position);
            final ViewHolder holder = (ViewHolder) viewHolder;
            holder.mDisplayNameTextView.setText(profile.getRequester().getDisplayName());
            int paddingLeftRight = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_10);
            int paddingTopBottom = Utils.getInstance().convertDimenToInt(R.dimen.button_text_padding);
            holder.mAcceptButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight,paddingTopBottom);
            holder.mDeclineButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight,paddingTopBottom);
            holder.mProfileImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(profile!=null &&profile.getRequester()!=null) {
                        ModelManager.getInstance().setOtherProfile(profile.getRequester());
                        TimelineFriendProfile.setFriendUsername(profile.getRequester().getUsername());
                        Intent mIntent = new Intent(context, TimelineFriendProfile.class);
                        context.startActivity(mIntent);
                    }
                }
            });
            holder.mDisplayNameTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(profile!=null &&profile.getRequester()!=null) {
                        ModelManager.getInstance().setOtherProfile(profile.getRequester());
                        TimelineFriendProfile.setFriendUsername(profile.getRequester().getUsername());
                        Intent mIntent = new Intent(context, TimelineFriendProfile.class);
                        context.startActivity(mIntent);
                    }
                }
            });
            holder.mDeclineButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendRequest(profile, position,false);
                }
            });
            holder.mAcceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendRequest(profile, position,true);
                }
            });
            String profImagePath = "";
            profImagePath = profile.getRequester().getThumbNail();
            profImagePath = (profImagePath == null ? profile.getRequester().getProfileImage() : (profImagePath.isEmpty() ?profile.getRequester().getProfileImage(): profile.getRequester().getUsername() + AppConsatants.FILE_EXTENSION_JPG));
            holder.mProfileImageView.setTag(profImagePath);//tag of imageView == path to image
//            new LoadImage(holder.mProfileImageView).execute();
            downloadContentFromS3Bucket(holder.mProfileImageView, profImagePath);
        } catch (NullPointerException e) {

        } catch (Exception e) {

        }
    }
    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
//        Log.d(TAG, "onViewRecycledCalled");
        holder.setIsRecyclable(true);

    }

    @Override
    public int getItemCount() {
        if(pendingList == null)
            return 0;
        return pendingList.size();
    }
//    private void downloadContentFromS3Bucket(ViewHolder holder, String filePath){
//        if(filePath!=null && !filePath.isEmpty()) {
//            String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
//            File file = new File(filePathImage);
//            if (file.exists()) {
//                holder.mProfileImageView.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePathImage, false));
//            }else{
//                String url = MyApplication.getAppContext().getString(R.string.s3_url);
//                url = url + filePath;
//                url = url.replace("+","%2B");
//                Picasso.with(context).load(url)
//                        .error(R.drawable.user)
//                        .placeholder(R.drawable.user)
//                        .networkPolicy(NetworkPolicy.NO_CACHE)
//                        .into(holder.mProfileImageView);
//            }
//        }
//    }
    private void downloadContentFromS3Bucket(ImageView imageView, String filePath){
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                }else{
                    String url = MyApplication.getAppContext().getString(R.string.s3_url);
                    url = url + filePath;
                    url = url.replace("+","%2B");
                    Picasso.with(context).load(url)
                            .error(R.drawable.user)
                            .placeholder(R.drawable.user)
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .into(imageView);
                }
            }
//            if(!disableLoad)
//            download(imageView, filePath);
        }
    }

    private void  download(final ImageView imageView, final String filePath){
        UserFileManager userFileManager = Utils.getInstance().getUserFileManager();
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                }

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }
    private void sendRequest(final GroupMembership profile,final int position,boolean accept){
        try {
            if(profile == null)
                return;
            showProgressBar("Processing your request");
            final String userUuid = DataStorage.getInstance().getUserUUID();
            String groupUuid = profile.getGroup().getUuid();
            String url =ServiceURLManager.getInstance().getGroupJoinDenyUrl(groupUuid,profile.getRequester().getUuid(),userUuid);
            if(accept) {
                url = ServiceURLManager.getInstance().getGroupJoinAcceptUrl(groupUuid, profile.getRequester().getUuid(), userUuid);
            }
                JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
                        if(pendingList!=null && pendingList.size()>position) {
                            pendingList.remove(position);
                            notifyItemRemoved(position);
                        }
                        if(pendingList.size() == 0){

                                ((PendingRequestActivity)context).finish();
                        }
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    hideProgressBar();
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    private void showProgressBar(String msg) {

        progressDialog = ProgressDialog.show(context, null, null, true, false);
        progressDialog.setContentView(R.layout.progressbar);
        TextView  progressBarMessage = (TextView)progressDialog.findViewById(R.id.progressBar_message);
        progressBarMessage.setText(msg);

    }

    private void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

}