package net.xvidia.vowmee.listadapter;

/**
 * Created by vasu on 4/1/16.
 */

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;

import net.xvidia.vowmee.R;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.network.model.Comment;
import net.xvidia.vowmee.helper.CircularImageView;

import java.io.File;
import java.io.InputStream;
import java.util.List;

public class CommentsAdapter extends ArrayAdapter<Comment> {

    Context context;
    private List<Comment> searchList;
    public static LayoutInflater inflater = null;

    public CommentsAdapter(Context context, final List<Comment> searchList) {
        super(context,0);

        this.context = context;
        this.searchList = searchList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    // Create new views (invoked by the layout manager)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemLayoutView = convertView;
        ViewHolder viewHolder;
        if (convertView == null) {
            itemLayoutView = inflater.inflate(R.layout.timeline_comments_item, null);
            viewHolder = new ViewHolder();
            viewHolder.mDisplayNameTextView = (TextView) itemLayoutView.findViewById(R.id.comment_username);
            viewHolder.mDaysLeftTextView = (TextView) itemLayoutView.findViewById(R.id.comment_date);
            viewHolder.mCommentTextView = (TextView) itemLayoutView.findViewById(R.id.comment);
            viewHolder.mProfileImageView = (CircularImageView) itemLayoutView.findViewById(R.id.comment_user_image);

            itemLayoutView.setTag(viewHolder);

        }else {
            viewHolder = (ViewHolder) itemLayoutView.getTag();
        }
        final Comment comment = searchList.get(position);
        viewHolder.mDisplayNameTextView.setText(comment.getCommenterDisplayName());
        viewHolder.mDaysLeftTextView.setText(Utils.getInstance().getDateDiffString(comment.getDate()));
        String text = comment.getComment().trim();
        try{
            text= Utils.getInstance().replceLast(text, "<p dir=\"ltr\">", "");
            text=Utils.getInstance().replceLast(text, "</p>", "");
        }catch (Exception e) {}
        Spanned bar = Html.fromHtml(text,new Html.ImageGetter() {
            public Drawable getDrawable(String source) {
                Drawable d = new BitmapDrawable(context.getResources(),getImage(source));
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                return d;
            }
        },null);

        viewHolder.mCommentTextView.setText(bar);
//        viewHolder.mCommentTextView.setText(comment.getComment());
//        Utils.getInstance().underlineTextView(comment.getCommenterDisplayName(), viewHolder.mDisplayNameTextView);

//        String url ="";// MyApplication.getAppContext().getString(R.string.s3_url);
//        url = url + comment.getCommenterThumbNail();
//        if(url.isEmpty())
//            url = comment.getCommenter()+ AppConsatants.FILE_EXTENSION_JPG;
        String profImagePath = comment.getCommenterThumbNail();
        profImagePath = (profImagePath == null ? (comment.getCommenter() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? comment.getCommenter() + AppConsatants.FILE_EXTENSION_JPG : comment.getCommenterThumbNail()));
        viewHolder.mProfileImageView.setTag(profImagePath);//tag of imageView == path to image
//            new LoadImage(holder.mProfileImageView).execute();
        downloadContentFromS3Bucket(viewHolder.mProfileImageView, profImagePath);
        return itemLayoutView;
    }

    private void downloadContentFromS3Bucket(ImageView imageView, String filePath){
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                } else {
                    imageView.setImageResource(R.drawable.user);
                }
            }
//            if(!disableLoad)
            download(imageView, filePath);
        }
    }

    private void  download(final ImageView imageView, final String filePath){
        UserFileManager userFileManager = Utils.getInstance().getUserFileManager();
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                }

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }


        /**
     * For loading smileys from assets
     */
    private Bitmap getImage(String path) {
        AssetManager mngr = context.getAssets();
        InputStream in = null;
        try {
            in = mngr.open("emoticons/" + path);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bitmap temp = BitmapFactory.decodeStream(in, null, null);
        return temp;
    }
    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder {

        public TextView mDisplayNameTextView;
        public TextView mDaysLeftTextView;
        public TextView mCommentTextView;
        public CircularImageView mProfileImageView;

    }


    @Override
     public int getCount() {
        return searchList.size();
    }

    @Override
    public Comment getItem(int position) {
        if(searchList != null && position>0&&position<searchList.size())
            return searchList.get(position);
        return null ;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
