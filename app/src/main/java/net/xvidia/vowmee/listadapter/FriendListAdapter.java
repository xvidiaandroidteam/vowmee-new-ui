package net.xvidia.vowmee.listadapter;


import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.MakeVideoCallActivity;
import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.PersonActivity;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.TimelineFriendProfile;
import net.xvidia.vowmee.TimelineProfile;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.helper.CustomFontTextView;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.FriendRequest;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FriendListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    private List<Profile> searchList;
//    private int pendingListCount;
    private ProgressDialog progressDialog;
    private int adjustCount;
    private int imageDimensPixelsDefault;
    private UserFileManager userFileManager;
//    private Animation myFadeInAnimation;
    class ViewHolder extends RecyclerView.ViewHolder {

        private CustomFontTextView mDisplayNameTextView;
        private CircularImageView mProfileImageView;
        private CustomFontTextView mRemoveButton;
        private CustomFontTextView mCallMeButton;
        private boolean friend = true;

        public ViewHolder(View v) {
            super(v);
            mDisplayNameTextView = (CustomFontTextView) v.findViewById(R.id.friend_name);
            mProfileImageView = (CircularImageView) v.findViewById(R.id.friend_image);
            mRemoveButton = (CustomFontTextView) v.findViewById(R.id.friend_remove_button);
            mCallMeButton = (CustomFontTextView) v.findViewById(R.id.call_button);

        }


    }

    public FriendListAdapter(Context context, final List<Profile> searchList) {

        this.context = context;
        this.searchList = searchList;
        imageDimensPixelsDefault =Utils.getInstance().convertDimenToInt(R.dimen.profile_image_size_list_item);
        userFileManager = Utils.getInstance().getUserFileManager();
//        myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fadein);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_friend_listitem, parent, false);
        v.setMinimumWidth(parent.getMeasuredWidth());
            ViewHolder viewHolder = new ViewHolder(v);

            return viewHolder;
//        }
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder viewHolder, final int position) {
        try {

                final Profile profile = searchList.get(position );
                final ViewHolder holder = (ViewHolder) viewHolder;
                holder.mDisplayNameTextView.setText(profile.getDisplayName());
                holder.mDisplayNameTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (profile.getUsername().equals(DataStorage.getInstance().getUsername())) {
                            Intent intent = new Intent(context, TimelineProfile.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                            ((AppCompatActivity) context).finish();
                        } else {
                            ModelManager.getInstance().setOtherProfile(profile);

                            Intent mIntent = new Intent(context, TimelineFriendProfile.class);
                            TimelineFriendProfile.setFriendUsername(profile.getUsername());
//                            Bundle mBundle = new Bundle();
//                            mBundle.putString(AppConsatants.PERSON_FRIEND, profile.getUsername());
////                            mIntent.putExtras(mBundle);
//                            mIntent.replaceExtras(mBundle);
                            context.startActivity(mIntent);
//                            ((AppCompatActivity) context).finish();
                        }
                    }
                });

            holder.mProfileImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (profile.getUsername().equals(DataStorage.getInstance().getUsername())) {
                        Intent intent = new Intent(context, TimelineProfile.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        ((AppCompatActivity) context).finish();
                    } else {
                        ModelManager.getInstance().setOtherProfile(profile);
                        Intent mIntent = new Intent(context, TimelineFriendProfile.class);
                        TimelineFriendProfile.setFriendUsername(profile.getUsername());
//                            Bundle mBundle = new Bundle();
//                            mBundle.putString(AppConsatants.PERSON_FRIEND, profile.getUsername());
////                            mIntent.putExtras(mBundle);
//                            mIntent.replaceExtras(mBundle);
                        context.startActivity(mIntent);
//                        ((AppCompatActivity) context).finish();
                    }
                }
            });
                holder.mRemoveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (holder.friend) {
//                            holder.friend = false;
                            if (profile.isFriendRequestFromViewer()) {
                                showMessage(context.getResources().getString(R.string.alert_message_withdraw_request), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        enableButtons(holder,false);
                                        sendFriendsWithdrawRequest(holder,profile, position - adjustCount);
                                    }
                                });
                            }else {
                                enableButtons(holder,false);
                                showMessage(context.getResources().getString(R.string.alert_message_remove_friend,profile.getDisplayName()), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        enableButtons(holder,false);
                                        sendFriendsRemoveRequest(holder,profile, position - adjustCount);
                                    }
                                });
                            }

                        } else {
//                            holder.friend = true;
                            enableButtons(holder,false);
                            sendFriendsAddRequest(holder,profile, position - adjustCount);

                        }
                    }
                });
            holder.mCallMeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ModelManager.getInstance().setOtherProfile(profile);
                    callFriendPermission(profile);
                }
            });
            int paddingLeftRight = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_10);
            int paddingTopBottom = Utils.getInstance().convertDimenToInt(R.dimen.button_text_padding);
            holder.mRemoveButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight,paddingTopBottom);
            holder.mCallMeButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight,paddingTopBottom);
                holder.mRemoveButton.setVisibility(View.VISIBLE);
                holder.mProfileImageView.setVisibility(View.VISIBLE);
                holder.mDisplayNameTextView.setVisibility(View.VISIBLE);
                if (profile.isFriendWithViewer()) {
                    holder.friend = true;
                    holder.mCallMeButton.setVisibility(View.VISIBLE);
                    holder.mRemoveButton.setText(context.getString(R.string.action_remove));
                } else if (profile.isFriendRequestFromViewer()) {
                    holder.friend = true;
                    holder.mCallMeButton.setVisibility(View.GONE);
                    holder.mRemoveButton.setText(context.getString(R.string.action_request_sent_friend));
                } else {
                    holder.friend = false;
                    holder.mCallMeButton.setVisibility(View.GONE);
                    holder.mRemoveButton.setText(context.getString(R.string.action_add_friend));
                }
//            Utils.getInstance().underlineTextView(profile.getDisplayName(), holder.mDisplayNameTextView);

//                url = url.replace("+","%2B");
//                Picasso.with(context).load(url)
//                        .error(R.drawable.user)
//                        .placeholder(R.drawable.user)
//                        .networkPolicy(NetworkPolicy.NO_CACHE)
//                        .into(holder.mProfileImageView);
            if (profile.getUsername().equals(DataStorage.getInstance().getUsername())) {
                holder.mCallMeButton.setVisibility(View.GONE);
                holder.mRemoveButton.setVisibility(View.GONE);
            }
            String profImagePath = profile.getThumbNail();
            profImagePath = (profImagePath == null ? (profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG : profile.getThumbNail()));
            holder.mProfileImageView.setTag(profImagePath);//tag of imageView == path to image
//            new LoadImage(holder.mProfileImageView).execute();
            downloadContentFromS3Bucket(holder.mProfileImageView, profImagePath);
//            }
        }catch(NullPointerException e){

        }catch(Exception e){

        }
    }
    private void enableButtons(ViewHolder holder, boolean enable) {
        if (holder != null) {
            holder.mCallMeButton.setEnabled(enable);
            holder.mRemoveButton.setEnabled(enable);
        }
    }
    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
//        Log.d(TAG, "onViewRecycledCalled");
        holder.setIsRecyclable(true);

    }

    @Override
    public int getItemCount() {
        if(searchList == null)
            return 0;
//        if(pendingListCount>0) {
//            return searchList.size() + 1;
//        }else{
            return searchList.size();
//        }
    }

    private void openPersonList(){
        Intent mIntent = new Intent(context, PersonActivity.class);
        Bundle mBundle = new Bundle();
        mBundle.putBoolean(AppConsatants.PERSON_FRIEND, true);
        mBundle.putBoolean(AppConsatants.PERSON_FOLLOWER, false);
        mBundle.putBoolean(AppConsatants.PERSON_FOLLOWING, false);
        mBundle.putBoolean(AppConsatants.PERSON_PENDING, true);
        mBundle.putBoolean(AppConsatants.OTHER_PERSON_PROFILE, false);
        mIntent.putExtras(mBundle);
        context.startActivity(mIntent);
        ((PersonActivity)context).finish();
    }
    private void sendFriendsWithdrawRequest(final ViewHolder holder,final Profile profile,final int position){
        try {
            if(profile == null)
                return;
            showProgressBar("Processing your request");
            String url =ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_WITHDRAW_REQUEST_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" +obj.toString());
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
                        try{
                            profile.setFriendWithViewer(false);
                            profile.setFriendRequestFromViewer(false);
                            if (searchList != null && searchList.size() > 0)
                                searchList.set(position, profile);
                            notifyItemChanged(position);
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                    enableButtons(holder,true);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                    enableButtons(holder,true);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void sendFriendsRemoveRequest(final ViewHolder holder,final Profile profile,final int position){
        try {
            if(profile == null)
                return;
            showProgressBar("Processing your request");
            String url =ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_REMOVE_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" +obj.toString());
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
                        try{
                        profile.setFriendWithViewer(false);
                            if (searchList != null && searchList.size() > 0)
                                searchList.set(position, profile);
                            notifyItemChanged(position);
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                    enableButtons(holder,true);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                    enableButtons(holder,true);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void sendFriendsAddRequest(final ViewHolder holder,final Profile profile, final int position){
        try {
            if(profile == null)
                return;
            showProgressBar("Processing your request");
            String url =ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_ADD_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" +obj.toString());
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
                        try{
                        profile.setFriendRequestFromViewer(true);
                            if (searchList != null && searchList.size() > 0) {
                                searchList.set(position, profile);
                                notifyItemChanged(position);
                            }
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                    enableButtons(holder,true);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                    enableButtons(holder,true);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void showProgressBar(String msg) {

        progressDialog = ProgressDialog.show(context, null, null, true, false);
        progressDialog.setContentView(R.layout.progressbar);
        TextView  progressBarMessage = (TextView)progressDialog.findViewById(R.id.progressBar_message);
        progressBarMessage.setText(msg);

    }

    private void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
    private void downloadContentFromS3Bucket(ImageView imageView, String filePath){
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null) {
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                        imageView.setImageBitmap(image1);
//                        Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fadein);
//                        imageView.startAnimation(myFadeInAnimation);
                    }else {
                        imageView.setImageResource(R.drawable.user);
                    }
                } else {
                    imageView.setImageResource(R.drawable.user);
                }
            }
//            if(!disableLoad)
            download(imageView, filePath);
        }
    }

    private void  download(final ImageView imageView, final String filePath){
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null) {
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                        imageView.setImageBitmap(image1);
                    } else {
                        imageView.setImageResource(R.drawable.user);
                    }
                }

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }

    private void showMessage(final String mesg, final DialogInterface.OnClickListener okClicked) {
        ((AppCompatActivity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(context,R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(mesg)
                        .setPositiveButton(android.R.string.ok, okClicked)
                        .setCancelable(true)
                        .setNegativeButton(android.R.string.cancel,null)
                        .show();
            }
        });
    }


    private void callFriend(Profile obj){
//        Profile obj = ModelManager.getInstance().getOtherProfile();
        if(obj == null)
            return;
        String profImagePath = obj.getThumbNail();
        profImagePath = (profImagePath == null ? (obj.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? obj.getUsername() + AppConsatants.FILE_EXTENSION_JPG : obj.getThumbNail()));

        Intent mIntent = new Intent(context, MakeVideoCallActivity.class);
        Bundle mBundle = new Bundle();
        mBundle.putString(AppConsatants.UUID, obj.getUuid());
        mBundle.putString(AppConsatants.USER_THUMBNAIL,profImagePath);
        mBundle.putString(AppConsatants.PERSON_FRIEND, obj.getDisplayName());
        mIntent.putExtras(mBundle);
        mIntent.replaceExtras(mBundle);
        context.startActivity(mIntent);
    }

    public void callFriendPermission(Profile obj) {

        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!Utils.getInstance().addPermission(permissionsList, Manifest.permission.RECORD_AUDIO))
            permissionsNeeded.add("Record Audio");
        if (!Utils.getInstance().addPermission(permissionsList, android.Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("Handle Calls");

        Map<String, Integer> perms = new HashMap<String, Integer>();
// Initial
        perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);
        perms.put(android.Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);

        if (permissionsList.size() == 0) {
            callFriend(obj);
        } else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
// Need Rationale
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(((PersonActivity) context), permissionsList.toArray(new String[permissionsList.size()]),
                                            PersonActivity.REQUEST_CODE_ASK_CALL_PERMISSIONS);
                                }
                            });
                    return;
                }
                ActivityCompat.requestPermissions(((PersonActivity) context), permissionsList.toArray(new String[permissionsList.size()]),
                        PersonActivity.REQUEST_CODE_ASK_CALL_PERMISSIONS);
                return;

            }

        }

    }

    private void showMessageOKCancel(final String message, final DialogInterface.OnClickListener okListener) {
        ((AppCompatActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(context)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, okListener)
                        .setNegativeButton(android.R.string.cancel, null)
                        .create()
                        .show();
            }
        });
    }

    class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private ImageView imv;
        private String path;

        public LoadImage(ImageView imv) {
            this.imv = imv;

            this.path = imv.getTag().toString();
        }

        @Override
        protected Bitmap doInBackground(Object... params) {
            Bitmap bitmap = null;
            if(path!=null && !path.isEmpty()) {
                String filePathImage = Utils.getInstance().getLocalContentPath(path);
                File file = new File(filePathImage);
                if (file.exists()) {
                    bitmap = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if(imageDimensPixelsDefault>0&&imageDimensPixelsDefault>0)
                        bitmap = Bitmap.createScaledBitmap(bitmap,imageDimensPixelsDefault,imageDimensPixelsDefault,false);
                }
//            download(holder, filePath,width,height);
            }
            return bitmap;
        }
        @Override
        protected void onPostExecute(Bitmap result) {
            if (!imv.getTag().toString().equals(path)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            if(result != null && imv != null){
                imv.setVisibility(View.VISIBLE);
                imv.setImageBitmap(result);
            }else{
                imv.setImageResource(R.drawable.user);
            }
        }

    }

}