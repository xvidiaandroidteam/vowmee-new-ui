package net.xvidia.vowmee.listadapter;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.ProfileSettingActivity;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class BlockedUserListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    private List<Profile> pendingList;
    private ProgressDialog progressDialog;
    private boolean followerPending;

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mDisplayNameTextView;
        private CircularImageView mProfileImageView;
        private TextView mUnBlockButton;

        public ViewHolder(View v) {
            super(v);
            mDisplayNameTextView = (TextView) v.findViewById(R.id.friend_name);
            mProfileImageView = (CircularImageView) v.findViewById(R.id.friend_image);
            mUnBlockButton = (TextView) v.findViewById(R.id.unblock_button);
        }


    }

     public BlockedUserListAdapter(Context context, final List<Profile> pendingList) {

         this.context = context;
         this.pendingList = pendingList;
         this.followerPending = followerPending;
     }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_blocked_listitem, parent, false);

            ViewHolder viewHolder = new ViewHolder(v);

            return viewHolder;

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder viewHolder, final int position) {
        try {
            final Profile profile = pendingList.get(position);
            final ViewHolder holder = (ViewHolder) viewHolder;
            holder.mDisplayNameTextView.setText(profile.getDisplayName());

            holder.mUnBlockButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendUnBlockUserRequest(profile, position);
            }});


            int paddingLeftRight = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_10);
            int paddingTopBottom = Utils.getInstance().convertDimenToInt(R.dimen.button_text_padding);
            holder.mUnBlockButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight,paddingTopBottom);
//        Utils.getInstance().underlineTextView(profile.getDisplayName(), holder.mDisplayNameTextView);

            String profImagePath = profile.getThumbNail();
            profImagePath = (profImagePath == null ? (profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG : profile.getThumbNail()));
            holder.mProfileImageView.setTag(profImagePath);//tag of imageView == path to image
            downloadContentFromS3Bucket(holder.mProfileImageView, profImagePath);
        } catch (NullPointerException e) {

        } catch (Exception e) {

        }
    }
    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
//        Log.d(TAG, "onViewRecycledCalled");
        holder.setIsRecyclable(true);

    }

    @Override
    public int getItemCount() {
        if(pendingList == null)
            return 0;
        return pendingList.size();
    }
    private void sendUnBlockUserRequest(Profile blockedProfile,final int position) {
        try {
            String userUuid = DataStorage.getInstance().getUserUUID();
            String blockinguuid = blockedProfile.getUuid();
            String url = ServiceURLManager.getInstance().getUnBlockUserUrl(userUuid,blockinguuid);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                        if(obj != null){
                            if(pendingList!=null && pendingList.size()>position) {
                                notifyItemRemoved(position);
                                pendingList.remove(position);
                                ProfileSettingActivity.setBlockCount(pendingList.size());
                                refreshList();
                            }else if(pendingList!=null && pendingList.size()==1) {
                                pendingList.remove(0);
                                notifyDataSetChanged();
                                ProfileSettingActivity.setBlockCount(pendingList.size());
                            }
                            getProfileRequest();
                        }
//                        if(alertDialog!=null)
//                            alertDialog.d
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {

                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.LONG_TOAST);

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }
    private void getProfileRequest(){
        try {
            final String username = DataStorage.getInstance().getUsername();
            String url =ServiceURLManager.getInstance().getProfileUrl(IAPIConstants.API_KEY_GET_PROFILE, username, username);
            VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(url, true);
            JsonObjectRequest  request= new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    String getUsername = obj.getUsername();
                    if(getUsername != null && !getUsername.isEmpty() && username.equals(getUsername)) {
                        ModelManager.getInstance().setOwnProfile(obj);
                        ProfileSettingActivity.setBlockCount(obj.getBlockedUserCount());
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            })
            {
                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        final long cacheHitButRefreshed = 30 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + cacheHitButRefreshed;
                        final long ttl = now + cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONObject(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONObject response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){

        }
    }
    private void refreshList(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        },450);
    }

    private void downloadContentFromS3Bucket(final ImageView imageView, final String filePath){
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                } else {
                    imageView.setImageResource(R.drawable.user);
                }
            }
//            if(!disableLoad)
            download(imageView, filePath);
        }
    }

    private void  download(final ImageView imageView, final String filePath){
        UserFileManager userFileManager = Utils.getInstance().getUserFileManager();
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                }

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }

}