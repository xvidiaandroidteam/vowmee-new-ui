package net.xvidia.vowmee.listadapter;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.PersonActivity;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.TimelineFriendProfile;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.helper.CustomFontTextView;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.FriendRequest;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

public class FacebookListAdapter extends RecyclerView.Adapter<FacebookListAdapter.ViewHolder> {

    private static String TAG = "VideosAdapter";

    Context context;
    private List<Profile> searchList;
    private ProgressDialog progressDialog;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public CustomFontTextView mDisplayNameTextView;
        public CircularImageView mProfileImageView;
        public CustomFontTextView mFollowButton;
        public CustomFontTextView mAddFriendButton;
        public boolean following = false;
        public boolean friend = false;

        public ViewHolder(View v) {
            super(v);
            mDisplayNameTextView = (CustomFontTextView) v.findViewById(R.id.follower_person_name);
            mProfileImageView = (CircularImageView) v.findViewById(R.id.follower_person_image);
            mFollowButton = (CustomFontTextView) v.findViewById(R.id.follow_button);
            mAddFriendButton = (CustomFontTextView) v.findViewById(R.id.follower_add_friend_button);

        }


    }

    public FacebookListAdapter(Context context, final List<Profile> searchList) {

        this.context = context;
        this.searchList = searchList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_follower_listitem, parent, false);

        Configuration configuration = context.getResources().getConfiguration();
        int screenWidthDp = configuration.screenWidthDp; //The current width of the available screen space, in dp units, corresponding to screen width resource qualifier.
        int smallestScreenWidthDp = configuration.smallestScreenWidthDp; //The smallest screen size an application will see in normal operation, corresponding to smallest screen width resource qualifier.

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            final Profile profile = searchList.get(position);
            holder.mDisplayNameTextView.setText(profile.getDisplayName());
            holder.mDisplayNameTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ModelManager.getInstance().setOtherProfile(profile);
                    TimelineFriendProfile.setFriendUsername(profile.getUsername());
                    Intent mIntent = new Intent(context, TimelineFriendProfile.class);
//                Bundle mBundle = new Bundle();
//                mBundle.putString(AppConsatants.PERSON_FRIEND, profile.getUsername());
//                mIntent.putExtras(mBundle);
                    context.startActivity(mIntent);
                    ((PersonActivity) context).finish();
                }
            });
            holder.mProfileImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ModelManager.getInstance().setOtherProfile(profile);
                    TimelineFriendProfile.setFriendUsername(profile.getUsername());
                    Intent mIntent = new Intent(context, TimelineFriendProfile.class);
//            Bundle mBundle = new Bundle();
//            mBundle.putString(AppConsatants.PERSON_FRIEND, profile.getUsername());
//            mIntent.putExtras(mBundle);
                    context.startActivity(mIntent);
//                    ((PersonActivity) context).finish();
                }
            });
            holder.mFollowButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.following) {
                        holder.following = false;
                        sendFollowingRemoveRequest(profile, position);
                    } else {
                        holder.following = true;
                        sendFollowingAddRequest(profile, position);
                    }
                }
            });
            holder.mAddFriendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.friend) {
                        holder.friend = false;
                        sendFriendsRemoveRequest(profile, position);
                    } else {
                        holder.friend = true;
                        sendFriendsAddRequest(profile, position);
                    }
                }
            });
            int paddingLeftRight = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_10);
            int paddingTopBottom = Utils.getInstance().convertDimenToInt(R.dimen.button_text_padding);
            holder.mAddFriendButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight,paddingTopBottom);
            holder.mFollowButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight,paddingTopBottom);
            if (profile.isFollowedByViewer()) {
                holder.following = true;
                holder.mFollowButton.setText(context.getString(R.string.action_unfollow));
            } else {
                holder.following = false;
                holder.mFollowButton.setText(context.getString(R.string.action_follow));
            }
            if (profile.isFriendRequestFromViewer()) {
                holder.friend = true;
                holder.mAddFriendButton.setText(context.getString(R.string.action_request_sent_friend));
            } else {
                holder.friend = false;
                holder.mAddFriendButton.setText(context.getString(R.string.action_add_friend));
            }
//        Utils.getInstance().underlineTextView(profile.getDisplayName(), holder.mDisplayNameTextView);
            String url = MyApplication.getAppContext().getString(R.string.s3_url);
            String profImagePath = profile.getThumbNail();
            profImagePath = (profImagePath == null ? (profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG : profile.getThumbNail()));
             url = url + profImagePath;

            url = url.replace("+", "%2B");
            Picasso.with(context).load(url)
                    .error(R.drawable.user)
                    .placeholder(R.drawable.user)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(holder.mProfileImageView);
        } catch (NullPointerException e) {

        } catch (Exception e) {

        }
    }


    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
//        Log.d(TAG, "onViewRecycledCalled");
        holder.setIsRecyclable(true);

    }

    @Override
    public int getItemCount() {
        if (searchList == null)
            return 0;
        return searchList.size();
    }

    private void sendFollowingRemoveRequest(final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_REMOVE_FOLLOW);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {
                Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);
                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        try {
                            profile.setFollowedByViewer(false);
                            if (searchList != null && searchList.size() > 0)
                                searchList.set(position, profile);
                            notifyItemChanged(position);
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFollowingAddRequest(final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_ADD_FOLLOW);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {
                Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);

                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        try {
                            profile.setFollowedByViewer(true);
                            if (searchList != null && searchList.size() > 0)
                                searchList.set(position, profile);
                            notifyItemChanged(position);
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFriendsRemoveRequest(final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_REMOVE_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {
                Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);
                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        try {
                            profile.setFriendWithViewer(false);
                            if (searchList != null && searchList.size() > 0)
                                searchList.set(position, profile);
                            notifyItemChanged(position);
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFriendsAddRequest(final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_ADD_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {
                Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);
                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        try {
                            profile.setFriendRequestFromViewer(true);
                            if (searchList != null && searchList.size() > 0)
                                searchList.set(position, profile);
                            notifyItemChanged(position);
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgressBar(String msg) {

        progressDialog = ProgressDialog.show(context, null, null, true, false);
        progressDialog.setContentView(R.layout.progressbar);
        TextView progressBarMessage = (TextView) progressDialog.findViewById(R.id.progressBar_message);
        progressBarMessage.setText(msg);

    }

    private void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

}