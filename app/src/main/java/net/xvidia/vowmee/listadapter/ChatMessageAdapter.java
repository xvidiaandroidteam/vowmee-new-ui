package net.xvidia.vowmee.listadapter;

/**
 * Created by vasu on 23/12/15.
 */

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.network.model.ChatMessage;
import net.xvidia.vowmee.helper.CircularImageView;

import java.io.File;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatMessageAdapter extends ArrayAdapter<ChatMessage> {

    public static final int VIEW_TYPE_LOCAL = 0;
    public static final int VIEW_TYPE_REMOTE = 1;
    private static final Map<Integer, Integer> viewTypes;
    private List<ChatMessage> searchList;
    public static LayoutInflater inflater = null;
    Context context;

    static {
        Map<Integer, Integer> aMap = new HashMap<Integer, Integer>();
        aMap.put(VIEW_TYPE_LOCAL, R.layout.list_item_message_left);
        aMap.put(VIEW_TYPE_REMOTE, R.layout.list_item_message_left);
        viewTypes = Collections.unmodifiableMap(aMap);
    }

    public ChatMessageAdapter(Context context,final List<ChatMessage> searchList) {
        super(context,0);

        this.context = context;
        this.searchList = searchList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /*@Override
    public View getView1(int position, View convertView, ViewGroup parent) {

        ChatMessage message = getItem(position);

        if (convertView == null) {
            int type = getItemViewType(position);
            convertView = LayoutInflater.from(getContext()).inflate(viewTypes.get(type), null);
        }

        TextView messageTextView = (TextView) convertView.findViewById(R.id.txtMsg);
        if (messageTextView != null) {
            messageTextView.setText(message.getMessageText());
        }

        return convertView;
    }*/

    // Create new views (invoked by the layout manager)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemLayoutView = convertView;
        ViewHolder viewHolder;
        if (convertView == null) {
            itemLayoutView = inflater.inflate(R.layout.timeline_live_comment_item, null);
            viewHolder = new ViewHolder();
            viewHolder.mDisplayNameTextView = (TextView) itemLayoutView.findViewById(R.id.comment_username);
//            viewHolder.mDaysLeftTextView = (TextView) itemLayoutView.findViewById(R.id.comment_date);
            viewHolder.mCommentTextView = (TextView) itemLayoutView.findViewById(R.id.comment);
            viewHolder.mProfileImageView = (CircularImageView) itemLayoutView.findViewById(R.id.comment_user_image);

            itemLayoutView.setTag(viewHolder);

        }else {
            viewHolder = (ViewHolder) itemLayoutView.getTag();
        }
        final ChatMessage comment = searchList.get(position);
        viewHolder.mDisplayNameTextView.setText(comment.getOwnerDisplayName());
//        viewHolder.mDaysLeftTextView.setText(Utils.getInstance().getDateDiffString(comment.getDate()));
        String text = comment.getMessageText();
        try{
            text= Utils.getInstance().replceLast(text, "<p dir=\"ltr\">", "");
            text=Utils.getInstance().replceLast(text, "</p>", "");
        }catch (Exception e) {}
        Spanned bar = Html.fromHtml(text, new Html.ImageGetter() {
            public Drawable getDrawable(String source) {
                Drawable d = new BitmapDrawable(context.getResources(), getImage(source));
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                return d;
            }
        }, null);
        viewHolder.mCommentTextView.setText(bar);
//        String url = MyApplication.getAppContext().getString(R.string.s3_url);
//        url = url + comment.getOwner()+ AppConsatants.FILE_EXTENSION_JPG;
//        Picasso.with(context).load(url)
//                .error(R.drawable.user)
//                .placeholder(R.drawable.user)
//                .networkPolicy(NetworkPolicy.NO_CACHE)
//                .into(viewHolder.mProfileImageView);
        String profImagePath = comment.getOwner()+ AppConsatants.FILE_EXTENSION_JPG;
//        profImagePath = (profImagePath == null ? (profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG : profile.getThumbNail()));
        viewHolder.mProfileImageView.setTag(profImagePath);//tag of imageView == path to image
//            new LoadImage(holder.mProfileImageView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        downloadContentFromS3Bucket(viewHolder.mProfileImageView, profImagePath);
        return itemLayoutView;
    }
    /**
     * For loading smileys from assets
     */
    private Bitmap getImage(String path) {
        AssetManager mngr = context.getAssets();
        InputStream in = null;
        try {
            in = mngr.open("emoticons/" + path);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bitmap temp = BitmapFactory.decodeStream(in, null, null);
        return temp;
    }


    @Override
    public int getViewTypeCount() {
        return viewTypes.size();
    }



    @Override
    public int getCount() {
        return searchList.size();
    }

    @Override
    public ChatMessage getItem(int position) {
        if(searchList != null && position>0&&position<searchList.size())
            return searchList.get(position);
        return null ;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {

        public TextView mDisplayNameTextView;
        public TextView mDaysLeftTextView;
        public TextView mCommentTextView;
        public CircularImageView mProfileImageView;

    }
    private void downloadContentFromS3Bucket(ImageView imageView, String filePath){
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                } else {
                    String url = MyApplication.getAppContext().getString(R.string.s3_url);
                    url = url + filePath;
                    Picasso.with(context).load(url)
                            .error(R.drawable.user)
                            .placeholder(R.drawable.user)
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .into(imageView);
                }
            }
//            if(!disableLoad)
//            download(imageView, filePath);
        }
    }

    private void  download(final ImageView imageView, final String filePath){
        UserFileManager userFileManager = Utils.getInstance().getUserFileManager();
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                }

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }
}

