package net.xvidia.vowmee.listadapter;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.PendingRequestActivity;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.TimelineFriendProfile;
import net.xvidia.vowmee.TimelineProfile;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.helper.CustomFontTextView;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.FriendRequest;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class PendingRequestListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    private List<Profile> pendingList;
    private ProgressDialog progressDialog;
    private boolean followerPending;

    class ViewHolder extends RecyclerView.ViewHolder {

        private CustomFontTextView mDisplayNameTextView;
        private CircularImageView mProfileImageView;
        private CustomFontTextView mAcceptButton;
        private CustomFontTextView mDeclineButton;

        public ViewHolder(View v) {
            super(v);
            mDisplayNameTextView = (CustomFontTextView) v.findViewById(R.id.pending_person_name);
            mProfileImageView = (CircularImageView) v.findViewById(R.id.pending_person_image);
            mAcceptButton = (CustomFontTextView) v.findViewById(R.id.accept_button);
            mDeclineButton = (CustomFontTextView) v.findViewById(R.id.decline_button);

        }


    }

     public PendingRequestListAdapter(Context context, final List<Profile> pendingList,boolean followerPending) {

         this.context = context;
         this.pendingList = pendingList;
         this.followerPending = followerPending;
     }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_pending_listitem, parent, false);

//            Configuration configuration = context.getResources().getConfiguration();
//        int screenHeightDp = configuration.screenHeightDp; //The current width of the available screen space, in dp units, corresponding to screen width resource qualifier.
////        int smallestScreenWidthDp = configuration.smallestScreenWidthDp;
//        int screenHeightPixels = Utils.getInstance().convertDpToPixel(screenHeightDp, context);
//        screenHeightPixels = screenHeightPixels/4;//The smallest screen size an application will see in normal operation, corresponding to smallest screen width resource qualifier.
//v.setMinimumWidth(parent.getMeasuredWidth());
//        v.setMinimumHeight(screenHeightPixels);
            ViewHolder viewHolder = new ViewHolder(v);

            return viewHolder;

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder viewHolder, final int position) {
        try {
            final Profile profile = pendingList.get(position);
            final ViewHolder holder = (ViewHolder) viewHolder;
            holder.mDisplayNameTextView.setText(profile.getDisplayName());
            int paddingLeftRight = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_10);
            int paddingTopBottom = Utils.getInstance().convertDimenToInt(R.dimen.button_text_padding);
            holder.mAcceptButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight,paddingTopBottom);
            holder.mDeclineButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight,paddingTopBottom);
            holder.mProfileImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        ModelManager.getInstance().setOtherProfile(profile);
                    TimelineFriendProfile.setFriendUsername(profile.getUsername());
                        Intent mIntent = new Intent(context, TimelineFriendProfile.class);
                        context.startActivity(mIntent);
                }
            });
            holder.mDisplayNameTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        ModelManager.getInstance().setOtherProfile(profile);
                    TimelineFriendProfile.setFriendUsername(profile.getUsername());
                        Intent mIntent = new Intent(context, TimelineFriendProfile.class);
                        context.startActivity(mIntent);
                }
            });
            holder.mDeclineButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (followerPending) {
                        sendFollowDenyRequest(profile, position);
                    } else {
                        sendFriendsDenyRequest(profile, position);
                    }

                }
            });
            holder.mAcceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (followerPending) {
                        sendFollowAcceptRequest(profile, position);
                    } else {
                        sendFriendsAcceptRequest(profile, position);
                    }
                }
            });

//        Utils.getInstance().underlineTextView(profile.getDisplayName(), holder.mDisplayNameTextView);
            String profImagePath = profile.getThumbNail();
            profImagePath = (profImagePath == null ? (profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG : profile.getThumbNail()));

            downloadContentFromS3Bucket(holder, profImagePath);

        } catch (NullPointerException e) {

        } catch (Exception e) {

        }
    }
    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
//        Log.d(TAG, "onViewRecycledCalled");
        holder.setIsRecyclable(true);

    }

    @Override
    public int getItemCount() {
        if(pendingList == null)
            return 0;
        return pendingList.size();
    }


    private void downloadContentFromS3Bucket(ViewHolder holder, String filePath){
        if(filePath!=null && !filePath.isEmpty()) {
            String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
            File file = new File(filePathImage);
            if (file.exists()) {
                holder.mProfileImageView.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePathImage, false));
            }else{
                holder.mProfileImageView.setImageResource(R.drawable.user);
                String url = MyApplication.getAppContext().getString(R.string.s3_url);
                url = url + filePath;
                url = url.replace("+","%2B");
                Picasso.with(context).load(url)
                        .error(R.drawable.user)
                        .placeholder(R.drawable.user)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .into(holder.mProfileImageView);
            }
        }
    }
    private void openProfile(){
        Intent mIntent = new Intent(context, TimelineProfile.class);
        context.startActivity(mIntent);
        ((PendingRequestActivity)context).finish();
    }
    private void sendFriendsDenyRequest(final Profile profile,final int position){
        try {
            if(profile == null)
                return;
            showProgressBar("Processing your request");
            String url =ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_DENY_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(username);
            obj.setRequestingPersonUsername(profile.getUsername());
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" +obj.toString());
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
                        if(pendingList!=null && pendingList.size()>position) {
                            pendingList.remove(position);
                            notifyItemRemoved(position);
                            refreshList();
                        }else if(pendingList!=null && pendingList.size()==1) {
                            pendingList.remove(0);
                            notifyDataSetChanged();
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                        if(pendingList.size() == 0){
                            if(Utils.getInstance().getIntegerValueOfString(ModelManager.getInstance().getOwnProfile().getFriends())<1){
                                openProfile();
                            }else {
//                                openPersonList(true, false);
                                ((PendingRequestActivity)context).finish();
                            }
                        }
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void sendFriendsAcceptRequest(final Profile profile,final int position){
        try {
            if(profile == null)
                return;
            String url =ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_ACCEPT_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            if(username.equals(profile.getUsername())){
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(username);
            obj.setRequestingPersonUsername(profile.getUsername());
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" +obj.toString());
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
                        if(pendingList!=null && pendingList.size()>position) {
                            notifyItemRemoved(position);
                            pendingList.remove(position);
                            refreshList();
                        }else if(pendingList!=null && pendingList.size()==1) {
                            pendingList.remove(0);
                            notifyDataSetChanged();
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                        if(pendingList.size() == 0){
                            if(ModelManager.getInstance().getOwnProfile()!=null&&Utils.getInstance().getIntegerValueOfString(ModelManager.getInstance().getOwnProfile().getFriends())<1){
                                openProfile();
                            }else {
//                                openPersonList(true, false);
                                ((PendingRequestActivity)context).finish();
                            }
                        }
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    private void refreshList(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        },450);
    }
    private void sendFollowDenyRequest(final Profile profile,final int position){
        try {
            if(profile == null)
                return;
            showProgressBar("Processing your request");
            String url =ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_DENY_FOLLOW);
            final String username = DataStorage.getInstance().getUsername();
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(username);
            obj.setRequestingPersonUsername(profile.getUsername());
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" +obj.toString());
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
                        if(pendingList!=null && pendingList.size()>position) {
                            pendingList.remove(position);
                            notifyItemRemoved(position);
                        }else if(pendingList!=null && pendingList.size()==1) {
                            pendingList.remove(0);
                            notifyDataSetChanged();
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                        if(pendingList.size() == 0){
                            if(Utils.getInstance().getIntegerValueOfString(ModelManager.getInstance().getOwnProfile().getFollowers())<1){
                                openProfile();
                            }else {
//                                openPersonList(true, false);
                                ((PendingRequestActivity)context).finish();
                            }
                        }
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    private void sendFollowAcceptRequest(final Profile profile,final int position){
        try {
            if(profile == null)
                return;
            String url =ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_ACCEPT_FOLLOW);
            final String username = DataStorage.getInstance().getUsername();
            if(username.equals(profile.getUsername())){
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(username);
            obj.setRequestingPersonUsername(profile.getUsername());
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" +obj.toString());
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
                        if(pendingList!=null && pendingList.size()>position) {
                            pendingList.remove(position);
                            notifyItemRemoved(position);
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                        if(pendingList.size() == 0){
                            if(Utils.getInstance().getIntegerValueOfString(ModelManager.getInstance().getOwnProfile().getFollowers())<1){
                                openProfile();
                            }else {
//                                openPersonList(true, false);
                                ((PendingRequestActivity)context).finish();
                            }
                        }
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    private void showProgressBar(String msg) {

        progressDialog = ProgressDialog.show(context, null, null, true, false);
        progressDialog.setContentView(R.layout.progressbar);
        TextView  progressBarMessage = (TextView)progressDialog.findViewById(R.id.progressBar_message);
        progressBarMessage.setText(msg);

    }

    private void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

}