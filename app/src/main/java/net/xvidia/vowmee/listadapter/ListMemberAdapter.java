package net.xvidia.vowmee.listadapter;

/**
 * Created by vasu on 4/1/16.
 */

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.helper.CircularImageView;

import java.io.InputStream;
import java.util.ArrayList;

public class ListMemberAdapter extends ArrayAdapter<Profile> {

    Context context;
    private ArrayList<Profile> searchList;
    public static LayoutInflater inflater = null;

    public ListMemberAdapter(Context context, final ArrayList<Profile> searchList) {
        super(context,0);

        this.context = context;
        this.searchList = searchList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    // Create new views (invoked by the layout manager)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemLayoutView = convertView;
        ViewHolder viewHolder;
        if (convertView == null) {
            itemLayoutView = inflater.inflate(R.layout.activity_group_addmember_listitem, null);
            viewHolder = new ViewHolder();
            viewHolder.mDisplayNameTextView = (TextView) itemLayoutView.findViewById(R.id.friend_name);
            viewHolder.checkBox = (CheckBox) itemLayoutView.findViewById(R.id.song_checkBox);
            viewHolder.mProfileImageView = (CircularImageView) itemLayoutView.findViewById(R.id.friend_image);

            itemLayoutView.setTag(viewHolder);

        }else {
            viewHolder = (ViewHolder) itemLayoutView.getTag();
        }
        final Profile profile = searchList.get(position);
        viewHolder.mDisplayNameTextView.setText(profile.getDisplayName());
        String url = MyApplication.getAppContext().getString(R.string.s3_url);
        if (profile.getThumbNail() == null) {
            url = url + profile.getUsername() + "_thumbnail.jpg";
        } else {
            url = url + profile.getThumbNail();
        }
        url = url.replace("+","%2B");
        Picasso.with(context).load(url)
                .error(R.drawable.user)
                .placeholder(R.drawable.user)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(viewHolder.mProfileImageView);
        viewHolder.checkBox.setVisibility(View.GONE);
        return itemLayoutView;
    }



        /**
     * For loading smileys from assets
     */
    private Bitmap getImage(String path) {
        AssetManager mngr = context.getAssets();
        InputStream in = null;
        try {
            in = mngr.open("emoticons/" + path);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bitmap temp = BitmapFactory.decodeStream(in, null, null);
        return temp;
    }
    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder {

        private CheckBox checkBox;
        private TextView mDisplayNameTextView;
        private CircularImageView mProfileImageView;

    }


    @Override
     public int getCount() {
        return searchList.size();
    }



    @Override
    public long getItemId(int position) {
        return position;
    }

}
