package net.xvidia.vowmee.listadapter;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.TimelineFriendProfile;
import net.xvidia.vowmee.TimelineProfile;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.helper.CustomFontTextView;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.FriendRequest;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class FollowerListAdapter extends RecyclerView.Adapter<FollowerListAdapter.ViewHolder> {

    private static String TAG = "VideosAdapter";

    Context context;
    private List<Profile> searchList;
    int startPos = 0;
    private ProgressDialog progressDialog;
    private UserFileManager userFileManager;
    private int imageDimensPixelsDefault;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public CustomFontTextView mDisplayNameTextView;
        public CircularImageView mProfileImageView;
        public CustomFontTextView mFollowButton;
        public CustomFontTextView mAddFriendButton;
        public boolean following = false;
        public boolean friend = false;

        public ViewHolder(View v) {
            super(v);
            mDisplayNameTextView = (CustomFontTextView) v.findViewById(R.id.follower_person_name);
            mProfileImageView = (CircularImageView) v.findViewById(R.id.follower_person_image);
            mFollowButton = (CustomFontTextView) v.findViewById(R.id.follow_button);
            mAddFriendButton = (CustomFontTextView) v.findViewById(R.id.follower_add_friend_button);

        }


    }

    public FollowerListAdapter(Context context, final List<Profile> searchList) {

        this.context = context;
        this.searchList = searchList;
        userFileManager = Utils.getInstance().getUserFileManager();
        imageDimensPixelsDefault =Utils.getInstance().convertDimenToInt(R.dimen.profile_image_size_list_item);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_follower_listitem, parent, false);

        Configuration configuration = context.getResources().getConfiguration();
        int screenWidthDp = configuration.screenWidthDp; //The current width of the available screen space, in dp units, corresponding to screen width resource qualifier.
        int smallestScreenWidthDp = configuration.smallestScreenWidthDp; //The smallest screen size an application will see in normal operation, corresponding to smallest screen width resource qualifier.

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            final Profile profile = searchList.get(position);
            holder.mDisplayNameTextView.setText(profile.getDisplayName());
            holder.mDisplayNameTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (profile.getUsername().equals(DataStorage.getInstance().getUsername())) {
                        Intent intent = new Intent(context, TimelineProfile.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        ((AppCompatActivity) context).finish();
                    } else {
                        ModelManager.getInstance().setOtherProfile(profile);

                        Intent mIntent = new Intent(context, TimelineFriendProfile.class);
                        TimelineFriendProfile.setFriendUsername(profile.getUsername());
                        context.startActivity(mIntent);
//                        ((AppCompatActivity) context).finish();
                    }
                }
            });
            holder.mProfileImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (profile.getUsername().equals(DataStorage.getInstance().getUsername())) {
                        Intent intent = new Intent(context, TimelineProfile.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        ((AppCompatActivity) context).finish();
                    } else {
                        ModelManager.getInstance().setOtherProfile(profile);

                        Intent mIntent = new Intent(context, TimelineFriendProfile.class);
                        TimelineFriendProfile.setFriendUsername(profile.getUsername());
                        context.startActivity(mIntent);
//                        ((AppCompatActivity) context).finish();
                    }
                }
            });
            holder.mFollowButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.following) {
                        holder.following = false;
                        sendFollowingRemoveRequest(holder,profile, position);
                    } else {
                        holder.following = true;
                        sendFollowingAddRequest(holder,profile, position);
                    }
                    enableButtons(holder,false);
                }
            });
            holder.mAddFriendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.friend) {
//                        holder.friend = false;
                        if (profile.isFriendRequestFromViewer()) {
                            showMessage(context.getResources().getString(R.string.alert_message_withdraw_request), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    enableButtons(holder,false);
                                    sendFriendsWithdrawRequest(holder,profile, position);
                                }
                            });
                        } else {
                           showMessage(context.getResources().getString(R.string.alert_message_remove_friend,profile.getDisplayName()), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    enableButtons(holder,false);
                                    sendFriendsRemoveRequest(holder,profile, position);
                                }
                            });
                        }
                    } else {
                        enableButtons(holder,false);
                        sendFriendsAddRequest(holder,profile, position);
                    }
                }
            });
            int paddingLeftRight = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_10);
            int paddingTopBottom = Utils.getInstance().convertDimenToInt(R.dimen.button_text_padding);
            holder.mAddFriendButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight,paddingTopBottom);
            holder.mFollowButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight,paddingTopBottom);
            if (profile.isFollowedByViewer()) {
                holder.following = true;
                holder.mFollowButton.setText(context.getString(R.string.action_unfollow));
            } else {
                holder.following = false;
                holder.mFollowButton.setText(context.getString(R.string.action_follow));
            }
            if (profile.isFriendRequestFromViewer()) {
                holder.friend = true;
                holder.mAddFriendButton.setText(context.getString(R.string.action_request_sent_friend));
            } else {
                holder.friend = false;
                holder.mAddFriendButton.setText(context.getString(R.string.action_add_friend));
            }
            if (profile.getUsername().equals(DataStorage.getInstance().getUsername())) {
                holder.mFollowButton.setVisibility(View.GONE);
                holder.mAddFriendButton.setVisibility(View.GONE);
            }
            String profImagePath = profile.getThumbNail();
            profImagePath = (profImagePath == null ? (profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG : profile.getThumbNail()));
            holder.mProfileImageView.setTag(profImagePath);//tag of imageView == path to image
//            new LoadImage(holder.mProfileImageView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            downloadContentFromS3Bucket(holder.mProfileImageView, profImagePath);
        } catch (NullPointerException e) {

        } catch (Exception e) {

        }
    }


    private void enableButtons(ViewHolder holder, boolean enable) {
        if (holder != null) {
                holder.mAddFriendButton.setEnabled(enable);
                holder.mFollowButton.setEnabled(enable);
        }
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
//        Log.d(TAG, "onViewRecycledCalled");
        holder.setIsRecyclable(true);

    }

    @Override
    public int getItemCount() {
        if (searchList == null)
            return 0;
        return searchList.size();
    }

    private void sendFollowingRemoveRequest(final ViewHolder holder, final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_REMOVE_FOLLOW);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {

                Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);
                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        try {
                            profile.setFollowedByViewer(false);
                            if (searchList != null && searchList.size() > 0)
                                searchList.set(position, profile);
                            notifyItemChanged(position);
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
                    }
                    enableButtons(holder,true);

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    hideProgressBar();
                    enableButtons(holder,true);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFollowingAddRequest(final ViewHolder holder,final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_ADD_FOLLOW);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {

                Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);
                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
//            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);  } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        try {
                            profile.setFollowedByViewer(true);
                            if (searchList != null && searchList.size() > 0)
                                searchList.set(position, profile);
                            notifyItemChanged(position);
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
                    }

                    enableButtons(holder,true);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    hideProgressBar();
                    enableButtons(holder,true);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFriendsRemoveRequest(final ViewHolder holder,final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_REMOVE_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {

                Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);
                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
//            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        try {
                            profile.setFriendWithViewer(false);
                            profile.setFriendRequestFromViewer(false);
                            if (searchList != null && searchList.size() > 0)
                                searchList.set(position, profile);
                            notifyItemChanged(position);
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                    enableButtons(holder,true);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    hideProgressBar();
                    enableButtons(holder,true);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFriendsAddRequest(final ViewHolder holder,final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_ADD_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {

                Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);
                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
//            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        try {
                            profile.setFriendRequestFromViewer(true);
                            if (searchList != null && searchList.size() > 0)
                                searchList.set(position, profile);
                            notifyItemChanged(position);
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                    enableButtons(holder,true);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    hideProgressBar();
                    enableButtons(holder,true);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFriendsWithdrawRequest(final ViewHolder holder,final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            showProgressBar("Processing your request");
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_WITHDRAW_REQUEST_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
//            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        try {
                            profile.setFriendWithViewer(false);
                            profile.setFriendRequestFromViewer(false);
                            if (searchList != null && searchList.size() > 0)
                                searchList.set(position, profile);
                            notifyItemChanged(position);
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                    enableButtons(holder,true);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    hideProgressBar();
                    enableButtons(holder,true);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgressBar(String msg) {

        progressDialog = ProgressDialog.show(context, null, null, true, false);
        progressDialog.setContentView(R.layout.progressbar);
        TextView progressBarMessage = (TextView) progressDialog.findViewById(R.id.progressBar_message);
        progressBarMessage.setText(msg);

    }

    private void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private void downloadContentFromS3Bucket(ImageView imageView, String filePath){
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null) {
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                        imageView.setImageBitmap(image1);
//                        Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fadein);
//                        imageView.startAnimation(myFadeInAnimation);
                    }else {
                        imageView.setImageResource(R.drawable.user);
                    }
                } else {
                    imageView.setImageResource(R.drawable.user);
                }
            }
//            if(!disableLoad)
            download(imageView, filePath);
        }
    }

    private void  download(final ImageView imageView, final String filePath){
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                }

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }


    private void showMessage(final String mesg, final DialogInterface.OnClickListener okClicked) {
        ((AppCompatActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(context).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(mesg)
                        .setPositiveButton(android.R.string.ok, okClicked)
                        .setCancelable(true)
                        .setNegativeButton(android.R.string.cancel, null)
                        .show();
            }
        });
    }


    class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private ImageView imv;
        private String path;

        public LoadImage(ImageView imv) {
            this.imv = imv;

            this.path = imv.getTag().toString();
        }

        @Override
        protected Bitmap doInBackground(Object... params) {
            Bitmap bitmap = null;
            if(path!=null && !path.isEmpty()) {
                String filePathImage = Utils.getInstance().getLocalContentPath(path);
                File file = new File(filePathImage);
                if (file.exists()) {
                    bitmap = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if(imageDimensPixelsDefault>0&&imageDimensPixelsDefault>0)
                        bitmap = Bitmap.createScaledBitmap(bitmap,imageDimensPixelsDefault,imageDimensPixelsDefault,false);
                }
//            download(holder, filePath,width,height);
            }
            return bitmap;
        }
        @Override
        protected void onPostExecute(Bitmap result) {
            if (!imv.getTag().toString().equals(path)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            if(result != null && imv != null){
                imv.setVisibility(View.VISIBLE);
                imv.setImageBitmap(result);
            }else{
                imv.setImageResource(R.drawable.user);
            }
        }

    }
}