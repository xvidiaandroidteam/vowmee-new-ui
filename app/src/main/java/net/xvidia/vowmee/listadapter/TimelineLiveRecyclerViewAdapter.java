package net.xvidia.vowmee.listadapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.SubscriberActivity;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.helper.CustomFontTextView;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;

import static net.xvidia.vowmee.MyApplication.getAppContext;


public class TimelineLiveRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ILoadMoreItems iLoadMore;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private UserFileManager userFileManager;
    private Context context;
    private List<Moment> momentList;
//    private Animation myFadeInAnimation;
    public List<Moment> getMomentList() {
        return momentList;
    }

    public void setMomentList(List<Moment> momentList) {
        this.momentList = momentList;
    }
    public TimelineLiveRecyclerViewAdapter(boolean ownProfile, Context context, final List<Moment> moments) {

        this.context = context;
        this.momentList = moments;
        userFileManager = Utils.getInstance().getUserFileManager();
    }
    @Override
    public int getItemViewType(int position) {
        return momentList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.timeline_live_moment_item, parent, false);
            ItemViewHolder viewHolder = new ItemViewHolder(v);
            return viewHolder;
        }else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_more_item, parent, false);
            return new LoadingViewHolder(view);
        }

        return null;
//        }
//        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder) {
            final int newPosition = position;
//                if(mWithHeader)
//                    newPosition = position-1;
            final Moment video = momentList.get(newPosition);
            final ItemViewHolder mItemViewHolder = (ItemViewHolder) holder;
            mItemViewHolder.mDisplayNameTextView.setText(video.getOwnerDisplayName());
            mItemViewHolder.mCaptionTextView.setText(video.getCaption());
            String description = video.getDescription();

            if (description == null) {
                mItemViewHolder.mDescriptionTextView.setVisibility(View.GONE);
            } else if (description.isEmpty()) {
                mItemViewHolder.mDescriptionTextView.setVisibility(View.GONE);
            } else {
                mItemViewHolder.mDescriptionTextView.setText(video.getDescription().trim());
            }

            if (video.getStatus() != null && !video.getStatus().equalsIgnoreCase(AppConsatants.STATUS_ACTIVE)) {
                mItemViewHolder.mLiveStatusTextView.setText(MyApplication.getAppContext().getResources().getString(R.string.inactive));
                mItemViewHolder.mLiveStatusTextView.setBackgroundColor(MyApplication.getAppContext().getResources().getColor(R.color.grey_light));
            } else {
                mItemViewHolder.mLiveStatusTextView.setText(MyApplication.getAppContext().getResources().getString(R.string.title_live));
                mItemViewHolder.mLiveStatusTextView.setBackgroundColor(MyApplication.getAppContext().getResources().getColor(R.color.red_color));
            }
            mItemViewHolder.mDaysLeftTextView.setText(Utils.getInstance().getDateDiffString(video.getDate()));
            video.setIndexPosition("" + newPosition);
            momentList.set(newPosition, video);
            mItemViewHolder.mDescriptionTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (video.getIsLive()) {
                        Intent mIntent = new Intent(context, SubscriberActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString(AppConsatants.LIVE_PUBLISHER_ID, video.getSessionId());
                        mIntent.putExtras(mBundle);
                        context.startActivity(mIntent);
                    } else {
//                    if (video.getOwner().equals(DataStorage.getInstance().getUsername())) {
//                        Intent intent = new Intent(context, TimelineProfile.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        context.startActivity(intent);
////                        ((AppCompatActivity)context).finish();
//                    } else {
//                        Intent mIntent = new Intent(context, TimelineFriendProfile.class);
//                        Bundle mBundle = new Bundle();
//                        mBundle.putString(AppConsatants.PERSON_FRIEND, video.getOwner());
//                        mIntent.putExtras(mBundle);
//                        context.startActivity(mIntent);
//                    }
                    }
                }
            });

            mItemViewHolder.mDaysLeftTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (video.getIsLive()) {
                        Intent mIntent = new Intent(context, SubscriberActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString(AppConsatants.LIVE_PUBLISHER_ID, video.getSessionId());
                        mIntent.putExtras(mBundle);
                        context.startActivity(mIntent);
                    } else {
//                    if (video.getOwner().equals(DataStorage.getInstance().getUsername())) {
//                        Intent intent = new Intent(context, TimelineProfile.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        context.startActivity(intent);
////                        ((AppCompatActivity)context).finish();
//                    } else {
//                        Intent mIntent = new Intent(context, TimelineFriendProfile.class);
//                        Bundle mBundle = new Bundle();
//                        mBundle.putString(AppConsatants.PERSON_FRIEND, video.getOwner());
//                        mIntent.putExtras(mBundle);
//                        context.startActivity(mIntent);
//                    }
                    }
                }
            });
            mItemViewHolder.mProfileImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (video.getIsLive()) {
                        Intent mIntent = new Intent(context, SubscriberActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString(AppConsatants.LIVE_PUBLISHER_ID, video.getSessionId());
                        mIntent.putExtras(mBundle);
                        context.startActivity(mIntent);
                    } else {
//                        if (video.getOwner().equals(DataStorage.getInstance().getUsername())) {
//                            Intent intent = new Intent(context, TimelineProfile.class);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            context.startActivity(intent);
////                        ((AppCompatActivity)context).finish();
//                        } else {
//                            Intent mIntent = new Intent(context, TimelineFriendProfile.class);
//                            Bundle mBundle = new Bundle();
//                            mBundle.putString(AppConsatants.PERSON_FRIEND, video.getOwner());
//                            mIntent.putExtras(mBundle);
//                            context.startActivity(mIntent);
//                        }
                    }
                }
            });
//        mItemViewHolder.layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent mIntent = new Intent(context, SubscriberActivity.class);
//                Bundle mBundle = new Bundle();
//                mBundle.putString(AppConsatants.LIVE_PUBLISHER_ID, video.getSessionId());
//                mIntent.putExtras(mBundle);
//                context.startActivity(mIntent);
//            }
//        });

            mItemViewHolder.mDisplayNameTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (video.getIsLive()) {
                        Intent mIntent = new Intent(context, SubscriberActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString(AppConsatants.LIVE_PUBLISHER_ID, video.getSessionId());
                        mIntent.putExtras(mBundle);
                        context.startActivity(mIntent);
                    } else {
//                        if (video.getOwner().equals(DataStorage.getInstance().getUsername())) {
//                            Intent intent = new Intent(context, TimelineProfile.class);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            context.startActivity(intent);
////                        ((AppCompatActivity)context).finish();
//                        } else {
//                            Intent mIntent = new Intent(context, TimelineFriendProfile.class);
//                            Bundle mBundle = new Bundle();
//                            mBundle.putString(AppConsatants.PERSON_FRIEND, video.getOwner());
//                            mIntent.putExtras(mBundle);
//                            context.startActivity(mIntent);
//                        }
                    }
                }
            });
            mItemViewHolder.mMoreImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getInstance().setMomentObj(video);
                    v.showContextMenu();
                }
            });
            if (video.getCaption() == null) {
                mItemViewHolder.mCaptionTextView.setVisibility(View.GONE);
            } else if (video.getCaption().trim().isEmpty()) {
                mItemViewHolder.mCaptionTextView.setVisibility(View.GONE);
            } else {
                mItemViewHolder.mCaptionTextView.setVisibility(View.VISIBLE);
            }

            String profImagePath = "";
            profImagePath = video.getOwnerThumbNail();
            profImagePath = (profImagePath == null ? (video.getOwner() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? video.getOwner() + AppConsatants.FILE_EXTENSION_JPG : video.getOwnerThumbNail()));
            mItemViewHolder.mProfileImageView.setTag(profImagePath);//tag of imageView == path to image
//            new LoadImage(holder.mProfileImageView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            downloadContentFromS3Bucket(mItemViewHolder.mProfileImageView, profImagePath);
        }  else if (getItemViewType(position) == VIEW_TYPE_LOADING) {
            final LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.loadingMore.setText(MyApplication.getAppContext().getResources().getString(R.string.action_load_more));
            loadingViewHolder.loadingMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadingViewHolder.loadingMore.setText(MyApplication.getAppContext().getResources().getString(R.string.action_loading));
                    iLoadMore.loadMoreItems(true);
                }
            });
        }
    }

    private void downloadContentFromS3Bucket(ImageView imageView, String filePath){
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null) {
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                        imageView.setImageBitmap(image1);
//                        Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fadein);
//                        imageView.startAnimation(myFadeInAnimation);
                    }else {
                        imageView.setImageResource(R.drawable.user);
                    }
                } else {
                    imageView.setImageResource(R.drawable.user);
                }
            }
//            if(!disableLoad)
            download(imageView, filePath);
        }
    }

    private void  download(final ImageView imageView, final String filePath){
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null) {
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                        imageView.setImageBitmap(image1);
//                        Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fadein);
//                        imageView.startAnimation(myFadeInAnimation);
                    } else {
                        imageView.setImageResource(R.drawable.user);
                    }

                }

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }


    @Override
    public int getItemCount() {
        if(momentList == null)
            return 0;
       return momentList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        holder.setIsRecyclable(true);

    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public CustomFontTextView loadingMore;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            loadingMore = (CustomFontTextView) itemView.findViewById(R.id.load_more);
        }
    }
    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        public CustomFontTextView mDisplayNameTextView;
        public CustomFontTextView mDaysLeftTextView;
        public CustomFontTextView mCommentsCountTextView;
        public CustomFontTextView mShareCountTextView;
        public CustomFontTextView mLikesTextView;
        public CustomFontTextView mCaptionTextView;
        public CustomFontTextView mLiveStatusTextView;
        public CustomFontTextView mDescriptionTextView;
        public CircularImageView mProfileImageView;
        public ImageView mMoreImage;

        public CardView layout;

        ItemViewHolder(View v) {
            super(v);
            layout = (CardView) v.findViewById(R.id.layout);
            mDisplayNameTextView = (CustomFontTextView) v.findViewById(R.id.timeline_post_user);
            mCommentsCountTextView = (CustomFontTextView) v.findViewById(R.id.timeline_comments);
            mShareCountTextView = (CustomFontTextView) v.findViewById(R.id.timeline_share);
            mLiveStatusTextView = (CustomFontTextView) v.findViewById(R.id.timeline_live_status);
            mLikesTextView = (CustomFontTextView) v.findViewById(R.id.timeline_likes);
            mDaysLeftTextView = (CustomFontTextView) v.findViewById(R.id.timeline_time);
            mCaptionTextView = (CustomFontTextView) v.findViewById(R.id.timeline_caption);
            mDescriptionTextView = (CustomFontTextView) v.findViewById(R.id.timeline_description);
            mProfileImageView = (CircularImageView) v.findViewById(R.id.timeline_friend_image);
            mMoreImage = (ImageView) v.findViewById(R.id.timeline_more_image);
            v.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            Moment moment = Utils.getInstance().getMomentObj();

            if (moment != null) {
                if (moment.getOwner().equalsIgnoreCase(DataStorage.getInstance().getUsername())) {
                    MenuItem delete = menu.add(100, 101, 0, context.getResources().getString(R.string.action_delete));
                    delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            final Moment moment = Utils.getInstance().getMomentObj();
                            showOkCancel(context.getResources().getString(R.string.alert_message_delete_moment), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (moment != null)
                                        deleteMoment(Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()), moment);
                                }
                            });

                            return true;
                        }
                    });

                } else {
                    if (moment.getOwnerBlocked() != null && !moment.getOwnerBlocked()) {
                        MenuItem block = menu.add(100, 102, 1, context.getResources().getString(R.string.block_user, moment.getOwnerDisplayName()));
                        block.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                final Moment moment = Utils.getInstance().getMomentObj();
                                showOkCancel(context.getResources().getString(R.string.alert_message_block_user, moment.getOwnerDisplayName()), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (moment != null)
                                            sendBlockUserRequest(moment, true, Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()));
                                    }
                                });

                                return true;
                            }
                        });
                    } else {
                        MenuItem unblock = menu.add(100, 102, 1, context.getResources().getString(R.string.unblock_user, moment.getOwnerDisplayName()));
                        unblock.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                final Moment moment = Utils.getInstance().getMomentObj();
                                if (moment != null)
                                    sendBlockUserRequest(moment, false, Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()));

                                return true;
                            }
                        });
                    }
                    MenuItem report = menu.add(100, 103, 2, context.getResources().getString(R.string.report));
                    report.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            final Moment moment = Utils.getInstance().getMomentObj();
                            showOkCancel(context.getResources().getString(R.string.alert_message_report_user, moment.getOwnerDisplayName()), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (moment != null)
                                        sendReportMomentRequest(Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()), moment);

                                }
                            });
                            return true;
                        }
                    });
                }
//                } else {
//
//
//                    MenuItem share = menu.add(100, 104, 3, context.getResources().getString(R.string.share));
//                    share.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//                        @Override
//                        public boolean onMenuItemClick(MenuItem item) {
//                            Moment moment = Utils.getInstance().getMomentObj();
//                            MomentShare momentShare = new MomentShare();
//                            momentShare.setOriginalMoment(moment);
//                            UploadService.setMomentShare(momentShare);
//                            Intent mIntent = new Intent(context, UploadActivity.class);
//                            Bundle mBundle = new Bundle();
//                            mBundle.putString(AppConsatants.MOMENT_PATH, Utils.getInstance().getLocalContentPath(moment.getLink()));
//                            mBundle.putBoolean(AppConsatants.RESHARE, true);
//                            mIntent.putExtras(mBundle);
//                            context.startActivity(mIntent);
//                            return true;
//                        }
//                    });
//                    MenuItem facebook = menu.add(100, 105, 4, "Share on facebook");
//                    facebook.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//                        @Override
//                        public boolean onMenuItemClick(MenuItem item) {
//                            Moment moment = Utils.getInstance().getMomentObj();
//
//                            if (moment != null) {
//                                String url = getAppContext().getString(R.string.s3_url);
//                                url = url + moment.getThumbNail();
//                                ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
//                                        .setContentTitle(moment.getCaption())
//                                        .setContentDescription(moment.getDescription())
//                                        .setContentUrl(Uri.parse("www.vowmee.com"))
//                                        .setImageUrl(Uri.parse(url))
//                                        .build();
//                                ShareDialog.show(activity, shareLinkContent);
////                            if (moment != null)
////                                deleteMoment(Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()), moment);
//                            }
//                            return true;
//                        }
//                    });
//                    MenuItem twitter = menu.add(100, 106, 5, "Share on twitter");
//                    twitter.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//                        @Override
//                        public boolean onMenuItemClick(MenuItem item) {
//                            Moment moment = Utils.getInstance().getMomentObj();
//                            String url = getAppContext().getString(R.string.s3_url);
//                            url = url + moment.getThumbNail();
//                            URL vowmeeUrl = null;
//                            String filePath = Utils.getInstance().getLocalContentPath(moment.getThumbNail());// Utils.getInstance().getUserFileManager().getLocalContentPath() + File.separator + moment.getThumbNail();
//                            File myImageFile = new File(filePath);
//                            Uri imageUri = null;
//                            if (myImageFile.exists()) {
//                                imageUri = Uri.fromFile(myImageFile);
//                            } else {
//                                imageUri = Uri.parse("www.vowmee.com");
//                            }
//                            try {
//                                vowmeeUrl = new URL("http://www.vowmee.com");
//                            } catch (MalformedURLException e) {
//                                e.printStackTrace();
//                            }
//                            TweetComposer.Builder builder = new TweetComposer.Builder(context)
//                                    .text("Vowmee\n" + moment.getCaption() + "\n" + moment.getDescription())
//                                    .image(imageUri)
//                                    .url(vowmeeUrl);
//                            builder.show();
////                            if (moment != null)
////                                deleteMoment(Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()), moment);
//
//                            return true;
//                        }
//                    });
//                }
            }
        }
    }

    public void deleteMoment(final int position, final Moment video) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                deleteMyMoment(position, video);
            }
        }, 250);
    }
    private void sendReportMomentRequest(final int position, final Moment momentObj) {
        try {
//            showProgressBar("Processing your request");
            String userUuid = DataStorage.getInstance().getUserUUID();
            String momentUuid = momentObj.getUuid();
            String url = ServiceURLManager.getInstance().getMomentReportUrl(momentUuid, userUuid);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Moment obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Moment.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        momentObj.setReportedAsAbuse(true);
                        momentList.set(position, momentObj);
                        final String username = DataStorage.getInstance().getUsername();
                        String url = ServiceURLManager.getInstance().getMomentFeedsListUrl(IAPIConstants.API_KEY_GET_MOMENT_FEEDS_TIMELINE_LIST, username, username, AppConsatants.FALSE, "0", "0");
                        VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(url, true);
                        notifyItemChanged(position);
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_report), Utils.SHORT_TOAST);

                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null && error.networkResponse != null) {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    }else {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_no_internet), Utils.SHORT_TOAST);

                    }

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void sendBlockUserRequest(final Moment momentObj,final boolean block,final int position) {
        try {
//            showProgressBar("Processing your request");
            String userUuid = DataStorage.getInstance().getUserUUID();
            String blockinguuid = momentObj.getOwnerUuid();
            String url = ServiceURLManager.getInstance().getBlockUserUrl(userUuid, blockinguuid);
            if(!block)
                url = ServiceURLManager.getInstance().getUnBlockUserUrl(userUuid, blockinguuid);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        momentObj.setOwnerBlocked(block);
                        momentList.set(position, momentObj);
                        notifyItemChanged(position);
                    }
                    notifyDataSetChanged();
                    if(block) {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_blocked), Utils.SHORT_TOAST);
                    }else{
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_unblocked), Utils.SHORT_TOAST);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    if (error != null && error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_CONFLICT) {
                            showError(context.getResources().getString(R.string.error_user_already_block, momentObj.getOwnerDisplayName()), null);

                        } else {
                            Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                        }
                    }else {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_no_internet), Utils.SHORT_TOAST);

                    }

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void deleteMyMoment(final int position, Moment video) {
        try {
            if (video == null)
                return;
            String url = ServiceURLManager.getInstance().getMomentDeleteUrl(video.getUuid(), DataStorage.getInstance().getUsername());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Moment obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Moment.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        momentList.remove(position);

                        final String username = DataStorage.getInstance().getUsername();
                        String url = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, username, username, AppConsatants.FALSE, "1", "10");
                        VolleySingleton.getInstance(getAppContext()).refresh(url, true);
                    }
                    notifyDataSetChanged();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null && error.networkResponse != null) {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    }else {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_no_internet), Utils.SHORT_TOAST);

                    }
                }
            });
            VolleySingleton.getInstance(getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showError(final String message, final DialogInterface.OnClickListener okClicked) {
            try{
                ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        new AlertDialog.Builder(context).setIcon(android.R.drawable.ic_dialog_alert)
                                .setMessage(message)
                                .setPositiveButton(android.R.string.ok, okClicked)
                                .show();
                    }
                });
            }catch(WindowManager.BadTokenException e){

            }catch (Exception e){}
        }

        private void showOkCancel(final String message, final DialogInterface.OnClickListener okClicked) {
            try {
                ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new AlertDialog.Builder(context).setIcon(android.R.drawable.ic_dialog_alert)
                                .setMessage(message)
                                .setNegativeButton(android.R.string.cancel, null)
                                .setPositiveButton(android.R.string.ok, okClicked)
                                .show();
                    }
                });
            }catch(WindowManager.BadTokenException e){

            }catch (Exception e){}
        }

        public void setOnLoadMoreListener(ILoadMoreItems iLoadMoreItems) {
        this.iLoadMore = iLoadMoreItems;
    }
}
