package net.xvidia.vowmee.listadapter;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.helper.CustomFontTextView;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.FriendRequest;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static net.xvidia.vowmee.MyApplication.getAppContext;

public class ViewersListAdapter extends RecyclerView.Adapter<ViewersListAdapter.ViewHolder> {

    private static final int TYPE_HEADER = 0;  // Declaring Variable to Understand which View is being worked on
    // IF the viaew under inflation and population is header or Item
    private static final int TYPE_ITEM = 1;

    Context context;
    private List<Profile> searchList;
    private ProgressDialog progressDialog;
    private int count, totalCount;

    public class ViewHolder extends RecyclerView.ViewHolder {

        int Holderid;
        public CustomFontTextView mDisplayNameTextView;
        public CircularImageView mProfileImageView;
        public CustomFontTextView mFollowButton;
        public CustomFontTextView mFriendButton;
        public boolean following = false;
        public boolean friend = false;
        public TextView totalViewersCounts, liveViewersCounts;

        public ViewHolder(View v, int ViewType) {                 // Creating ViewHolder Constructor with View and viewType As a parameter
            super(v);
            itemView.setClickable(true);
            if (ViewType == TYPE_ITEM) {
                mDisplayNameTextView = (CustomFontTextView) v.findViewById(R.id.search_person_name);
                mProfileImageView = (CircularImageView) v.findViewById(R.id.search_person_image);
                mFollowButton = (CustomFontTextView) v.findViewById(R.id.search_follow_button);
                mFriendButton = (CustomFontTextView) v.findViewById(R.id.search_friend_button);
                Holderid = TYPE_ITEM;
            } else if (ViewType == TYPE_HEADER) {
                totalViewersCounts = (TextView) v.findViewById(R.id.totalViewersText);
                liveViewersCounts = (TextView) v.findViewById(R.id.liveViewersText);
                Holderid = TYPE_HEADER;
            }


        }


    }

    public ViewersListAdapter(Context context, final List<Profile> searchList,int liveCount, int totalViewer) {

        this.context = context;
        this.searchList = searchList;
        this.count= liveCount;
        this.totalCount = totalViewer;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
      /*  View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_search_person_listitem, parent, false);

        Configuration configuration = context.getResources().getConfiguration();
        int screenWidthDp = configuration.screenWidthDp; //The current width of the available screen space, in dp units, corresponding to screen width resource qualifier.
        int smallestScreenWidthDp = configuration.smallestScreenWidthDp; //The smallest screen size an application will see in normal operation, corresponding to smallest screen width resource qualifier.

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;*/
        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_search_person_listitem,parent,false); //Inflating the layout
            ViewHolder vhItem = new ViewHolder(v,viewType); //Creating ViewHolder and passing the object of type view

            return vhItem; // Returning the created object

            //inflate your layout and pass it to view holder

        } else if (viewType == TYPE_HEADER) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_viewers_list,parent,false); //Inflating the layout

            ViewHolder vhHeader = new ViewHolder(v,viewType); //Creating ViewHolder and passing the object of type view

            return vhHeader; //returning the object created


        }
        return null;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if(holder.Holderid ==TYPE_ITEM) {
            final Profile profile = searchList.get(position-1);
            holder.mDisplayNameTextView.setText(profile.getDisplayName());
            int paddingLeftRight = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_10);
            int paddingTopBottom = Utils.getInstance().convertDimenToInt(R.dimen.button_text_padding);
            holder.mFriendButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight, paddingTopBottom);
            holder.mFollowButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight, paddingTopBottom);
//            holder.mDisplayNameTextView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (profile.getUsername().equals(DataStorage.getInstance().getUsername())) {
//                        Intent intent = new Intent(context, TimelineProfile.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        context.startActivity(intent);
//                    } else {
//
//                        ModelManager.getInstance().setOtherProfile(profile);
//                        Intent mIntent = new Intent(context, TimelineFriendProfile.class);
//                        TimelineFriendProfile.setFriendUsername(profile.getUsername());
//                        context.startActivity(mIntent);
//                    }
//                }
//            });
//            holder.mProfileImageView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (profile.getUsername().equals(DataStorage.getInstance().getUsername())) {
//                        Intent intent = new Intent(context, TimelineProfile.class);
////                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        context.startActivity(intent);
////                    ((SearchActivity) context).finish();
//                    } else {
//
//                        Intent mIntent = new Intent(context, TimelineFriendProfile.class);
//                        TimelineFriendProfile.setFriendUsername(profile.getUsername());
//                        context.startActivity(mIntent);
////                    ((SearchActivity)context).finish();
//                    }
//                }
//            });
//            holder.mFollowButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (holder.following) {
//                        holder.following = false;
//                        sendFollowingRemoveRequest(holder, profile, position-1);
//                    } else {
//                        holder.following = true;
//                        sendFollowingAddRequest(holder, profile, position-1);
//                    }
//                    enableButtons(holder, false);
//                }
//            });
//            holder.mFriendButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (holder.friend) {
//                        if (profile.isFriendRequestFromViewer()) {
//                            showMessage(context.getResources().getString(R.string.alert_message_withdraw_request), new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    enableButtons(holder, false);
//                                    sendFriendsWithdrawRequest(holder, profile, position-1);
//                                }
//                            });
//                        } else {
//                            showMessage(context.getResources().getString(R.string.alert_message_remove_friend, profile.getDisplayName()), new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//
//                                    enableButtons(holder, false);
//                                    sendFriendsRemoveRequest(holder, profile, position-1);
//                                }
//                            });
//                        }
//                    } else {
//                        enableButtons(holder, false);
//                        sendFriendsAddRequest(holder, profile, position-1);
//                    }
//                }
//            });


//            if (profile.isFriendRequestFromViewer()) {
//                holder.friend = true;
//                holder.mFriendButton.setText(context.getString(R.string.action_request_sent_friend));
//                holder.mFollowButton.setVisibility(View.VISIBLE);
//                if (profile.isFollowedByViewer()) {
//                    holder.following = true;
//                    holder.mFollowButton.setText(context.getString(R.string.action_unfollow));
//                } else {
//                    holder.following = false;
//                    holder.mFollowButton.setText(context.getString(R.string.action_follow));
//                }
//            } else if (profile.isFriendWithViewer()) {
//                holder.friend = true;
//                holder.mFriendButton.setText(context.getString(R.string.action_remove));
//                holder.mFollowButton.setVisibility(View.GONE);
//            } else {
//                holder.friend = false;
//                holder.mFriendButton.setText(context.getString(R.string.action_add_friend));
//                holder.mFollowButton.setVisibility(View.VISIBLE);
//                if (profile.isFollowedByViewer()) {
//                    holder.following = true;
//                    holder.mFollowButton.setText(context.getString(R.string.action_unfollow));
//                } else {
//                    holder.following = false;
//                    holder.mFollowButton.setText(context.getString(R.string.action_follow));
//                }
//
//            }
            String profImagePath = profile.getProfileImage();
            profImagePath = (profImagePath == null ? (profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG : profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG));

            holder.mProfileImageView.setTag(profImagePath);
            downloadContentFromS3Bucket(holder.mProfileImageView, profImagePath);
            holder.mFollowButton.setVisibility(View.GONE);
            holder.mFriendButton.setVisibility(View.GONE);
        }else  if(holder.Holderid == TYPE_HEADER){
            holder.totalViewersCounts.setText(String.valueOf(totalCount));
            holder.liveViewersCounts.setText(String.valueOf(count));
        }
    }

    private void downloadContentFromS3Bucket(ImageView imageView, String filePath) {
        if (filePath != null && !filePath.isEmpty() && imageView != null) {
            if (!imageView.getTag().toString().equals(filePath)) {
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if (image != null) {
                imageView.setImageBitmap(image);
            } else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null) {
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                        imageView.setImageBitmap(image1);
//                        Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fadein);
//                        imageView.startAnimation(myFadeInAnimation);
                    } else {
                        imageView.setImageResource(R.drawable.user);
                    }
                } else {
                    String url = getAppContext().getString(R.string.s3_url);
                    String profImagePath = filePath;
                    url = url + profImagePath;
                    url = url.replace("+", "%2B");
                    Picasso.with(context).load(url)
                            .error(R.drawable.user)
                            .placeholder(R.drawable.user)
//                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .into(imageView);
                }
            }
        }
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
//        Log.d(TAG, "onViewRecycledCalled");
        holder.setIsRecyclable(true);

    }

    @Override
    public int getItemCount() {
        if (searchList == null)
            return 0;
        return searchList.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private void sendFollowingRemoveRequest(final ViewHolder holder, final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_REMOVE_FOLLOW);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);

                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    } catch (OutOfMemoryError e) {
                    } catch (Exception e) {
                    }
                    if (obj != null) {
                        try {
                            profile.setFollowedByViewer(false);
                            if (searchList != null && searchList.size() > 0) {
                                if (position < searchList.size()) {
                                    searchList.set(position, profile);
                                    notifyItemChanged(position);
                                }
                            }
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                    enableButtons(holder, true);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                    enableButtons(holder, true);
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFollowingAddRequest(final ViewHolder holder, final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_ADD_FOLLOW);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);

                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    } catch (OutOfMemoryError e) {
                    } catch (Exception e) {
                    }
                    if (obj != null) {
                        try {
                            profile.setFollowedByViewer(true);
                            if (searchList != null && searchList.size() > 0) {
                                if (position < searchList.size()) {
                                    searchList.set(position, profile);
                                    notifyItemChanged(position);
                                }
                            }
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                    enableButtons(holder, true);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                    enableButtons(holder, true);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFriendsRemoveRequest(final ViewHolder holder, final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_REMOVE_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);

                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    } catch (OutOfMemoryError e) {
                    } catch (Exception e) {
                    }
                    if (obj != null) {
                        try {
                            profile.setFriendWithViewer(false);
                            if (searchList != null && searchList.size() > 0) {
                                if (position < searchList.size()) {
                                    searchList.set(position, profile);
                                    notifyItemChanged(position);
                                }
                            }
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
                    }

                    enableButtons(holder, true);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                    enableButtons(holder, true);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFriendsAddRequest(final ViewHolder holder, final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_ADD_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);

                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    } catch (OutOfMemoryError e) {
                    } catch (Exception e) {
                    }
                    if (obj != null) {
                        try {
                            profile.setFriendRequestFromViewer(true);
                            if (searchList != null && searchList.size() > 0) {
                                if (position < searchList.size()) {
                                    searchList.set(position, profile);
                                    notifyItemChanged(position);
                                }
                            }
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                    enableButtons(holder, true);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                    enableButtons(holder, true);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFriendsWithdrawRequest(final ViewHolder holder, final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            showProgressBar("Processing your request");
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_WITHDRAW_REQUEST_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    } catch (OutOfMemoryError e) {
                    } catch (Exception e) {
                    }
                    if (obj != null) {
                        try {
                            profile.setFriendWithViewer(false);
                            profile.setFriendRequestFromViewer(false);
                            if (searchList != null && searchList.size() > 0) {
                                if (position < searchList.size()) {
                                    searchList.set(position, profile);
                                    notifyItemChanged(position);
                                }
                            }
                        } catch (IndexOutOfBoundsException e) {
                        } catch (Exception e) {
                        }
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                    enableButtons(holder, true);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                    enableButtons(holder, true);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgressBar(String msg) {

        progressDialog = ProgressDialog.show(context, null, null, true, false);
        progressDialog.setContentView(R.layout.progressbar);
        TextView progressBarMessage = (TextView) progressDialog.findViewById(R.id.progressBar_message);
        progressBarMessage.setText(msg);

    }

    private void enableButtons(ViewHolder holder, boolean enable) {
        if (holder != null) {
            holder.mFollowButton.setEnabled(enable);
            holder.mFriendButton.setEnabled(enable);
        }
    }

    private void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private void showMessage(final String mesg, final DialogInterface.OnClickListener okClicked) {
        ((AppCompatActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(context).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(mesg)
                        .setPositiveButton(android.R.string.ok, okClicked)
                        .setCancelable(true)
                        .setNegativeButton(android.R.string.cancel, null)
                        .show();
            }
        });
    }
}