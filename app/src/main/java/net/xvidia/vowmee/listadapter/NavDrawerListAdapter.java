package net.xvidia.vowmee.listadapter;

/**
 * Created by vasu on 15/3/16.
 */

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.ConnectionTabs;
import net.xvidia.vowmee.GroupListActivity;
import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.PhoneContactInviteActivity;
import net.xvidia.vowmee.ProfileSettingActivity;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.RecordVideo;
import net.xvidia.vowmee.RegisterActivity;
import net.xvidia.vowmee.SuggestionsActivity;
import net.xvidia.vowmee.TimelineProfile;
import net.xvidia.vowmee.TimelineTabs;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.network.model.ResultServer;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NavDrawerListAdapter extends RecyclerView.Adapter<NavDrawerListAdapter.ViewHolder> {
    private static final int TYPE_HEADER = 0;  // Declaring Variable to Understand which View is being worked on
    // IF the viaew under inflation and population is header or Item
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    private String mNavTitles[]; // String Array to store the passed titles Value from MainActivity.java
//    private String counter[];
//    private String name;        //String Resource for header View Name
    private int profile;        //int Resource for header view profile picture
    private int icon[];
    private static Context context;
    private LinearLayout helpLayout, shareLayout, inviteLayout;
    private LinearLayout followers, friends, following;


    public class ViewHolder extends RecyclerView.ViewHolder {
       int Holderid;

        public TextView title, counter, help, share, invite;
        public ImageView imageView, helpImage,shareImage, inviteImage;
        public CircularImageView headerProfile;
        public TextView Name;
        public TextView mFriendsTextView;
        public TextView mFollowersTextView;
        public TextView mFollowingTextView;
        public RelativeLayout itemLayout;



       public ViewHolder(View itemView,int ViewType,Context c) {                 // Creating ViewHolder Constructor with View and viewType As a parameter
           super(itemView);
           context = c;
           itemView.setClickable(true);
           // Here we set the appropriate view in accordance with the the view type as passed when the holder object is created

           if(ViewType == TYPE_ITEM) {
               title = (TextView) itemView.findViewById(R.id.title);
               counter = (TextView) itemView.findViewById(R.id.counter);
               imageView = (ImageView) itemView.findViewById(R.id.icon);
               itemLayout = (RelativeLayout) itemView.findViewById(R.id.nav_item_layout);
               Holderid = 1;                                               // setting holder id as 1 as the object being populated are of type item row
           }
           else if(ViewType == TYPE_HEADER){

               mFriendsTextView = (TextView)itemView.findViewById(R.id.home_profile_friends);
               mFollowersTextView = (TextView)itemView.findViewById(R.id.home_profile_followers);
               mFollowingTextView = (TextView)itemView.findViewById(R.id.home_profile_following);
               Name = (TextView) itemView.findViewById(R.id.name);         // Creating Text View object from header.xml for name
               headerProfile = (CircularImageView) itemView.findViewById(R.id.circleView);// Creating Image view object from header.xml for profile pic
               Holderid = 0;                                                // Setting holder id = 0 as the object being populated are of type header view
           }
           else if(ViewType == TYPE_FOOTER){

               help = (TextView) itemView.findViewById(R.id.help);
               share = (TextView) itemView.findViewById(R.id.share);
               invite = (TextView) itemView.findViewById(R.id.invite);
               helpImage = (ImageView) itemView.findViewById(R.id.helpImage);
               shareImage = (ImageView) itemView.findViewById(R.id.shareImage);
               inviteImage = (ImageView) itemView.findViewById(R.id.inviteImage);

               Holderid = 2;

           }



       }

   }

    public NavDrawerListAdapter(String Titles[],  int Icon[], int Profile, Context passedContext){ // MyAdapter Constructor with titles and icons parameter
        // titles, icons, name, email, profile pic are passed from the main activity as we
        mNavTitles = Titles;                //have seen earlier
//        counter = Counter;
//        name = Name;
        profile = Profile;                     //here we assign those passed values to the values we declared here
        icon = Icon;
        context = passedContext;

        //in adapter

    }

    @Override
    public NavDrawerListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row,parent,false); //Inflating the layout
            ViewHolder vhItem = new ViewHolder(v,viewType,context); //Creating ViewHolder and passing the object of type view

            return vhItem; // Returning the created object

            //inflate your layout and pass it to view holder

        } else if (viewType == TYPE_HEADER) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header,parent,false); //Inflating the layout

            friends = (LinearLayout)v.findViewById(R.id.drawer_friends_layout);
            followers = (LinearLayout)v.findViewById(R.id.drawer_followers_layout);
            following = (LinearLayout)v.findViewById(R.id.drawer_following_layout);
                ViewHolder vhHeader = new ViewHolder(v,viewType,context); //Creating ViewHolder and passing the object of type view

            return vhHeader; //returning the object created


        } else if (viewType == TYPE_FOOTER) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_drawer_footer,parent,false); //Inflating the layout

            helpLayout = (LinearLayout)v.findViewById(R.id.help_layout);
            shareLayout = (LinearLayout)v.findViewById(R.id.share_layout);
            inviteLayout = (LinearLayout)v.findViewById(R.id.invite_layout);
            helpLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "Help", Toast.LENGTH_SHORT).show();
                }
            });

            shareLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TimelineTabs.Drawer.closeDrawer(Gravity.LEFT);
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, context.getResources().getText(R.string.invite_friends_message));
                    sendIntent.putExtra(Intent.EXTRA_SUBJECT, context.getResources().getText(R.string.invite_friends_subject));
                    sendIntent.putExtra(Intent.EXTRA_EMAIL, context.getResources().getText(R.string.invite_friends_message));
                    sendIntent.setType("text/plain");
                    context.startActivity(Intent.createChooser(sendIntent, context.getResources().getText(R.string.invite_friends)));
                }
            });

            inviteLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TimelineTabs.Drawer.closeDrawer(Gravity.LEFT);
                    Intent mIviteActivity = new Intent(context, PhoneContactInviteActivity.class);
                    context.startActivity(mIviteActivity);
                }
            });

            ViewHolder vhHeader = new ViewHolder(v,viewType,context); //Creating ViewHolder and passing the object of type view

            return vhHeader;


        }
        return null;

    }


    //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
    // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
    // which view type is being created 1 for item row
    @Override
    public void onBindViewHolder(NavDrawerListAdapter.ViewHolder holder, final int position) {
        if(holder.Holderid ==1) { // as the list view is going to be called after the header view so we decrement the
            // position by 1 and pass it to the holder while setting the text and image
            holder.title.setText(mNavTitles[position - 1]); // Setting the Text with the array of our Titles
            holder.imageView.setImageResource(icon[position-1]);
//            holder.counter.setText(counter[position - 1]);
            holder.counter.setVisibility(View.GONE);
            holder.itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TimelineTabs.Drawer.closeDrawer(Gravity.LEFT);
                    switch (position){
                        case 1:

//                            TimelineTabs.Drawer.closeDrawer(Gravity.LEFT);
                            break;

                        /*case 2:
                            Intent mActivities = new Intent(context, TimelineActivityTabs.class);
                            context.startActivity(mActivities);
                            break;*/
                        case 2:
                            Intent mIntentVideo = new Intent(context, GroupListActivity.class);
                            context.startActivity(mIntentVideo);
                            break;
                        case 3:
                            DataStorage.getInstance().setIsLiveFlag(true);
                            callCameraApp(true);
                            break;

                        case 4:
                            DataStorage.getInstance().setIsLiveFlag(false);
                            callCameraApp(false);
                            break;

                        case 5:
                            selectVideo();
                            break;
                        case 6:
                            Intent mInviteActivity = new Intent(context, ConnectionTabs.class);
                            context.startActivity(mInviteActivity);
                            break;

                        case 7:
                            Intent mActivity = new Intent(context, SuggestionsActivity.class);
                            context.startActivity(mActivity);
                            break;
                        case 8:
                            Intent mSetting = new Intent(context, ProfileSettingActivity.class);
                            context.startActivity(mSetting);
                            break;
                        case 9:
                          sendLogoutRequest();
                            break;

                    }
                }
            });
        }
        else if(holder.Holderid ==0) {
            final Profile obj;
            obj = ModelManager.getInstance().getOwnProfile();
            String profImagePath = "";
            if (obj != null) {
                profImagePath = obj.getProfileImage();
                profImagePath = (profImagePath == null ? (obj.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? obj.getUsername() + AppConsatants.FILE_EXTENSION_JPG : obj.getThumbNail()));

/*
                String filePathImage = Utils.getInstance().getLocalContentPath(profImagePath);

                File file = new File(filePathImage);
                if (file.exists()) {
                    holder.headerProfile.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePathImage, false));
                } else {
                    holder.headerProfile.setImageResource(profile);
                }*/
                if (obj.getDisplayName() != null || obj.getDisplayName().isEmpty()) {
                    holder.Name.setText(obj.getDisplayName());
                }
                holder.headerProfile.setTag(profImagePath);
                downloadContentFromS3Bucket(holder.headerProfile,profImagePath);
                holder.headerProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TimelineTabs.Drawer.closeDrawer(Gravity.LEFT);
                        Intent mIntentProfile = new Intent(context, TimelineProfile.class);
                        context.startActivity(mIntentProfile);
//                        ((TimelineTabs)context).finish();
                    }
                });
                holder.Name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TimelineTabs.Drawer.closeDrawer(Gravity.LEFT);
                        Intent mIntentProfile = new Intent(context, TimelineProfile.class);
                        context.startActivity(mIntentProfile);
//                        ((TimelineTabs)context).finish();
                    }
                });
                friends.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (Utils.getInstance().getIntegerValueOfString(obj.getFriends()) > 0
                                || Utils.getInstance().getIntegerValueOfString(obj.getPendingFriendRequests()) > 0)
                            TimelineRecyclerViewAdapter.openPersonList( context,true, false, false);
                        TimelineTabs.Drawer.closeDrawer(Gravity.LEFT);
                    }
                });

                followers.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (Utils.getInstance().getIntegerValueOfString(obj.getFollowers()) > 0
                                || Utils.getInstance().getIntegerValueOfString(obj.getPendingFollowRequests()) > 0)
                            TimelineRecyclerViewAdapter.openPersonList(context,false, true, false);
                        TimelineTabs.Drawer.closeDrawer(Gravity.LEFT);
                    }
                });

                following.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (Utils.getInstance().getIntegerValueOfString(obj.getFollowings()) > 0)
                            TimelineRecyclerViewAdapter.openPersonList(context,false, false, true);
                        TimelineTabs.Drawer.closeDrawer(Gravity.LEFT);
                    }
                });
                holder.mFriendsTextView.setText(obj.getFriends());
                holder.mFollowersTextView.setText(obj.getFollowers());
                holder.mFollowingTextView.setText(obj.getFollowings());
            }

        }
    }

    // This method returns the number of items present in the list
    @Override
    public int getItemCount() {
        return mNavTitles.length+2; // the number of items in the list will be +2 the titles including the header view and
                                    // the footer view .
    }

    public void callCameraApp(boolean live) {

        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write External Storage");
        if (!addPermission(permissionsList, Manifest.permission.RECORD_AUDIO))
            permissionsNeeded.add("Record Audio");

        Map<String, Integer> perms = new HashMap<String, Integer>();
        // Initial
        perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);

        if (permissionsList.size() == 0) {
            Intent mIntent = new Intent(context, RecordVideo.class);
            Bundle mBundle = new Bundle();
            mBundle.putBoolean(AppConsatants.LIVE_VIDEO, live);
            mIntent.putExtras(mBundle);
            context.startActivity(mIntent);
        } else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    // Need Rationale
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(((TimelineTabs) context), permissionsList.toArray(new String[permissionsList.size()]),
                                            TimelineTabs.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                }
                            });
                    return;
                }
                ActivityCompat.requestPermissions(((TimelineTabs) context), permissionsList.toArray(new String[permissionsList.size()]),
                        TimelineTabs.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;

            }

        }

    }

    private void showMessageOKCancel(final String message, final DialogInterface.OnClickListener okListener) {
        ((TimelineTabs) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
        new AlertDialog.Builder(context,R.style.AppCompatAlertDialogStyle)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
            }
        });
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(((TimelineTabs) context), permission))
                return false;
        }
        return true;
    }
    // Witht the following method we check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        if (isPositionFooter(position))
            return TYPE_FOOTER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }
    private boolean isPositionFooter(int position) {
        return position == mNavTitles.length+1;
    }
//    private void downloadContentFromS3Bucket(ViewHolder holder, String filePath){
//        String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
//        File file = new File(filePathImage);
//        if(file.exists()) {
//            holder.headerProfile.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePathImage, false));
//        } else {
//            String url = getAppContext().getString(R.string.s3_url);
//            String profImagePath = filePath;
//            url = url + profImagePath;
//            url = url.replace("+", "%2B");
//            Picasso.with(context).load(url)
//                    .error(R.drawable.user)
//                    .placeholder(R.drawable.user)
////                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                    .networkPolicy(NetworkPolicy.NO_CACHE)
//                    .into(holder.headerProfile);
//            Picasso.with(context).load(url)
//                    .error(R.drawable.user)
//                    .placeholder(R.drawable.user)
////                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                    .networkPolicy(NetworkPolicy.NO_CACHE)
//                    .into(holder.headerProfile);
//        }
//        download(holder,filePath);
//    }

    private void downloadContentFromS3Bucket(ImageView imageView, String filePath){
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                } else {
                    imageView.setImageResource(R.drawable.user);
                }
            }
//            if(!disableLoad)
            download(imageView, filePath);
        }
    }

    private void  download(final ImageView imageView, final String filePath){
        UserFileManager userFileManager = Utils.getInstance().getUserFileManager();
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                }

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }

//    private void  download(final ViewHolder holder, String filePath){
//        UserFileManager userFileManager = Utils.getInstance().getUserFileManager();
//        if (userFileManager == null)
//            return;
////        userFileManager.setContentCacheSize(1024 * 1024 * 35);
//        long fileSize = (1024 * 512);
////            userFileManager.setContentCacheSize(1024 * 1024 * 15);
//        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
//            @Override
//            public void onSuccess(ContentItem contentItem) {
//                String filePath = Utils.getInstance().getLocalContentPath(contentItem.getFilePath());
//                holder.headerProfile.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePath, false));
//
//            }
//
//            @Override
//            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
//            }
//
//            @Override
//            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
//            }
//        });
//    }

    public void selectVideo() {
//        sendMomentList();
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            Intent intent = new Intent();
            intent.setType("video/*");
            intent.setAction(Intent.ACTION_PICK);
            ((TimelineTabs) context).startActivityForResult(intent, TimelineTabs.GALLERY_INTENT_CALLED);
//            ((TimelineTabs) context).startActivityForResult(Intent.createChooser(intent, context.getResources().getString(R.string.home_profile_select_video)), TimelineTabs.GALLERY_INTENT_CALLED);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(((TimelineTabs) context), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showMessageOKCancel("You need to allow access to Camera",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(((TimelineTabs) context), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, TimelineTabs.REQUEST_CODE_ASK_PERMISSIONS);

                            }
                        });

                return;
            } else {
                ActivityCompat.requestPermissions(((TimelineTabs) context), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, TimelineTabs.REQUEST_CODE_ASK_PERMISSIONS);

                return;
            }
        }

    }

    private void sendLogoutRequest(){
        try {
            final String username = DataStorage.getInstance().getUserUUID();
            String url = ServiceURLManager.getInstance().getLogoutUrl(username);

            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    ResultServer obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), ResultServer.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null&&obj.isSuccess()) {
                        AWSMobileClient.defaultMobileClient().getIdentityManager().signOut();
                        VolleySingleton.getInstance(context).clearCache();
                        DataStorage.getInstance().setRegisteredFlag(false);
                        Intent mIntentLogin = new Intent(context, RegisterActivity.class);
                        mIntentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(mIntentLogin);
                        ((TimelineTabs)context).finish();
                    }else {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}