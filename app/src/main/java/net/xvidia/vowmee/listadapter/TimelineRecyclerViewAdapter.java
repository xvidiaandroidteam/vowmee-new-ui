package net.xvidia.vowmee.listadapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import net.xvidia.vowmee.CommentActivity;
import net.xvidia.vowmee.CropImageActivityNew;
import net.xvidia.vowmee.GroupProfileActivity;
import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.PersonActivity;
import net.xvidia.vowmee.ProfileActivity;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.TimelineFriendProfile;
import net.xvidia.vowmee.TimelineMomentDetail;
import net.xvidia.vowmee.TimelineProfile;
import net.xvidia.vowmee.UploadActivity;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.BlurBitmap;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.helper.CustomFontTextView;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.UploadffmpegService;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.FriendRequest;
import net.xvidia.vowmee.network.model.GroupProfile;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.network.model.MomentShare;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;
import net.xvidia.vowmee.videoplayer.VideoPlayer;
import net.xvidia.vowmee.videoplayer.VideoPlayerController;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static net.xvidia.vowmee.MyApplication.getAppContext;


public class TimelineRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ILoadMoreItems iLoadMore;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private final int VIEW_TYPE_LOADING = 2;
    private ProgressDialog progressDialog;
    private boolean mWithHeader;
    private static boolean mOwnProfile;
    private static Context context;
    private Activity activity;
    public static boolean refreshimeline = false;
    private int screenHeightDp,screenHeightPixels,screenWidthPixels,screenWidthDp;
    private int profileImageDimensPixelsDefault;
    public static boolean disableLoad;
//    private Animation myFadeInAnimation ;

    public List<Moment> getMomentList() {
        return momentList;
    }

    public void setMomentList(List<Moment> momentList) {
        this.momentList = momentList;
    }

    private List<Moment> momentList;
    static boolean moreFlag;
    public VideoPlayerController videoPlayerController;

    private UserFileManager userFileManager;

    public TimelineRecyclerViewAdapter(boolean ownProfile, Context context, Activity activity, final List<Moment> moments) {
        this.activity = activity;
        this.context = context;
        this.momentList = moments;
        videoPlayerController = new VideoPlayerController(context);
        mWithHeader = false;
        disableLoad =false;
        mOwnProfile = ownProfile;
        userFileManager = Utils.getInstance().getUserFileManager();
        Configuration configuration = MyApplication.getAppContext().getResources().getConfiguration();
        screenHeightDp = configuration.screenHeightDp;
        screenWidthDp = configuration.screenWidthDp;//The current width of the available screen space, in dp units, corresponding to screen width resource qualifier.
        screenHeightPixels = Utils.getInstance().convertDpToPixel(screenHeightDp, context);
        screenWidthPixels= Utils.getInstance().convertDpToPixel(screenWidthDp, context);
        screenHeightPixels = (screenHeightPixels / 9) * 4;
        profileImageDimensPixelsDefault =Utils.getInstance().convertDimenToInt(R.dimen.profile_image_size_list_item);
//        myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fadein);

    }

    public TimelineRecyclerViewAdapter(boolean withHeader, boolean ownProfile, Context context, Activity activity, final List<Moment> moments) {
        this.activity = activity;
        this.context = context;
        this.momentList = moments;
        videoPlayerController = new VideoPlayerController(context);
        mWithHeader = withHeader;
        disableLoad = false;
        mOwnProfile = ownProfile;
        userFileManager = Utils.getInstance().getUserFileManager();
        Configuration configuration = MyApplication.getAppContext().getResources().getConfiguration();
        screenHeightDp = configuration.screenHeightDp;
        screenWidthDp = configuration.screenWidthDp;//The current width of the available screen space, in dp units, corresponding to screen width resource qualifier.
        screenHeightPixels = Utils.getInstance().convertDpToPixel(screenHeightDp, context);
        screenWidthPixels= Utils.getInstance().convertDpToPixel(screenWidthDp, context);
        screenHeightPixels = (screenHeightPixels / 9) * 4;
        profileImageDimensPixelsDefault =Utils.getInstance().convertDimenToInt(R.dimen.profile_image_size_list_item);
//        myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fadein);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.timeline_header_item, parent, false);
            return new HeaderViewHolder(v);
        } else if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.timeline_moment_item, parent, false);
            ItemViewHolder viewHolder = new ItemViewHolder(v);
            RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            viewHolder.layout.setLayoutParams(rel_btn);

            return viewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_more_item, parent, false);
            return new LoadingViewHolder(view);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (context == null)
            context = getAppContext();
        if (getItemViewType(position) == TYPE_HEADER) {
            try {
                final HeaderViewHolder mHeaderViewHolder = (HeaderViewHolder) holder;
                final Profile obj;
                if (mOwnProfile) {
                    obj = ModelManager.getInstance().getOwnProfile();
                    mHeaderViewHolder.addFriendButton.setVisibility(View.GONE);
                    mHeaderViewHolder.followButton.setVisibility(View.GONE);

                } else {
                    mHeaderViewHolder.mProfileNameTextView.setFocusable(false);
                    obj = ModelManager.getInstance().getOtherProfile();
                }
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, screenHeightPixels);
                mHeaderViewHolder.mHeaderLayout.setLayoutParams(params);
                mHeaderViewHolder.mHeaderLayout.setMinimumHeight(screenHeightPixels);
//
                if (obj != null) {
                    String profImagePath = obj.getProfileImage();
                    profImagePath = (profImagePath == null ? (obj.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? obj.getUsername() + AppConsatants.FILE_EXTENSION_JPG : obj.getProfileImage()));
                    downloadContentFromS3Bucket(mHeaderViewHolder, profImagePath);
                    mHeaderViewHolder.mProfileNameTextView.setText(obj.getDisplayName());
                    mHeaderViewHolder.mProfileNameTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mOwnProfile) {
                                String facebookId = AWSMobileClient.defaultMobileClient().getIdentityManager().getFacebookUserId();
                                long twitterId = AWSMobileClient.defaultMobileClient().getIdentityManager().getTwitterUserId();
                                Intent mIntent = new Intent(context, ProfileActivity.class);
                                Bundle mBundle = new Bundle();
                                mBundle.putString(AppConsatants.PROFILE_FACEBOOK, facebookId);
                                mBundle.putLong(AppConsatants.PROFILE_TWITTER, twitterId);
                                mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, false);
                                mIntent.putExtras(mBundle);
                                context.startActivity(mIntent);
                            }
                        }
                    });

                /*mHeaderViewHolder.mStatusMessageTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        String facebookId = AWSMobileClient.defaultMobileClient().getIdentityManager().getFacebookUserId();
//                        long twitterId = AWSMobileClient.defaultMobileClient().getIdentityManager().getTwitterUserId();
//                        Intent mIntent = new Intent(context, ProfileActivity.class);
//                        Bundle mBundle = new Bundle();
//                        mBundle.putString(AppConsatants.PROFILE_FACEBOOK, facebookId);
//                        mBundle.putLong(AppConsatants.PROFILE_TWITTER, twitterId);
//                        mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, false);
//                        mIntent.putExtras(mBundle);
//                        context.startActivity(mIntent);
                    }
                });*/
                    if (mOwnProfile) {
                        mHeaderViewHolder.mProfileImageViewEdit.setVisibility(View.VISIBLE);
                    } else {
                        mHeaderViewHolder.mProfileImageViewEdit.setVisibility(View.GONE);
                    }
                    mHeaderViewHolder.mProfileImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mOwnProfile) {
                                Intent mIntent = new Intent(context, CropImageActivityNew.class);
                                context.startActivity(mIntent);
                            }
                        }
                    });
                    mHeaderViewHolder.mFriendLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.getInstance().getIntegerValueOfString(obj.getFriends()) > 0)
                                openPersonList(context, true, false, false);
                        }
                    });
                    mHeaderViewHolder.mFollowerLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.getInstance().getIntegerValueOfString(obj.getFollowers()) > 0)
                                openPersonList(context, false, true, false);
                        }
                    });
                    mHeaderViewHolder.mFollowingLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.getInstance().getIntegerValueOfString(obj.getFollowings()) > 0)
                                openPersonList(context, false, false, true);
                        }
                    });
                    mHeaderViewHolder.mFollowingTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.getInstance().getIntegerValueOfString(obj.getFollowings()) > 0)
                                openPersonList(context, false, false, true);
                        }
                    });
                    mHeaderViewHolder.mFriendsTextView.setText(obj.getFriends());
                    mHeaderViewHolder.mFollowersTextView.setText(obj.getFollowers());
                    mHeaderViewHolder.mFollowingTextView.setText(obj.getFollowings());
                    int paddingLeft = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_3);
                    int paddingRight = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_10);
                    int paddingTopBottom = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_3);
                    mHeaderViewHolder.addFriendButton.setPadding(paddingLeft, paddingTopBottom, paddingRight,paddingTopBottom);
                    mHeaderViewHolder.followButton.setPadding(paddingLeft, paddingTopBottom, paddingRight,paddingTopBottom);
                    if (!mOwnProfile) {
                        mHeaderViewHolder.addFriendButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (mHeaderViewHolder.friend) {
                                    mHeaderViewHolder.friend = false;
                                    sendFriendsRemoveRequest(obj, position);
                                } else {
                                    mHeaderViewHolder.friend = true;
                                    sendFriendsAddRequest(obj, position);
                                }
                            }
                        });
                        mHeaderViewHolder.followButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (mHeaderViewHolder.following) {
                                    mHeaderViewHolder.following = false;
                                    sendFollowingRemoveRequest(obj, position);
                                } else {
                                    mHeaderViewHolder.following = true;
                                    sendFollowingAddRequest(obj, position);
                                }
                            }
                        });
                        if (obj.isFriendRequestFromViewer()) {
                            mHeaderViewHolder.friend = true;
                            mHeaderViewHolder.addFriendButton.setText(context.getString(R.string.action_request_sent_friend));
                            mHeaderViewHolder.followButton.setVisibility(View.VISIBLE);
                            mHeaderViewHolder.addFriendButton.setVisibility(View.VISIBLE);
                            if (obj.isFollowedByViewer()) {
                                mHeaderViewHolder.following = true;
                                mHeaderViewHolder.followButton.setText(context.getString(R.string.action_unfollow));
                            } else {
                                mHeaderViewHolder.following = false;
                                mHeaderViewHolder.followButton.setText(context.getString(R.string.action_follow));
                            }
                        } else if (obj.isFriendWithViewer()) {
                            mHeaderViewHolder.friend = true;
                            mHeaderViewHolder.addFriendButton.setText(context.getString(R.string.action_remove));
                            mHeaderViewHolder.addFriendButton.setGravity(Gravity.CENTER);
                            mHeaderViewHolder.followButton.setVisibility(View.GONE);
                            mHeaderViewHolder.addFriendButton.setVisibility(View.VISIBLE);
                        } else if (obj.isHasBlockedViewer()) {
                            mHeaderViewHolder.friend = false;
                            if (obj.isFollowedByViewer()) {
                                mHeaderViewHolder.following = true;
                                mHeaderViewHolder.followButton.setVisibility(View.VISIBLE);
                                mHeaderViewHolder.followButton.setText(context.getString(R.string.action_unfollow));
                            } else if (obj.isFriendWithViewer()) {
                                mHeaderViewHolder.friend = true;
                                mHeaderViewHolder.addFriendButton.setText(context.getString(R.string.action_remove));
                                mHeaderViewHolder.addFriendButton.setGravity(Gravity.CENTER);
                                mHeaderViewHolder.addFriendButton.setVisibility(View.VISIBLE);
                                mHeaderViewHolder.followButton.setVisibility(View.GONE);
                            } else {
                                mHeaderViewHolder.followButton.setVisibility(View.GONE);
                                mHeaderViewHolder.following = false;
                                mHeaderViewHolder.addFriendButton.setVisibility(View.GONE);
                                mHeaderViewHolder.friend = false;
                            }
                        } else {
                            mHeaderViewHolder.friend = false;
                            mHeaderViewHolder.addFriendButton.setText(context.getString(R.string.action_add_friend));
                            mHeaderViewHolder.followButton.setVisibility(View.VISIBLE);
                            mHeaderViewHolder.addFriendButton.setVisibility(View.VISIBLE);
                            if (obj.isFollowedByViewer()) {
                                mHeaderViewHolder.following = true;
                                mHeaderViewHolder.followButton.setText(context.getString(R.string.action_unfollow));
                            } else {
                                mHeaderViewHolder.following = false;
                                mHeaderViewHolder.followButton.setText(context.getString(R.string.action_follow));
                            }

                        }
                    }
                }
            } catch (NullPointerException e) {

            } catch (ArrayIndexOutOfBoundsException e) {

            } catch (Exception e) {

            }
        } else if (getItemViewType(position) == TYPE_ITEM) {
            try {
                final int newPosition = position;
//                if(mWithHeader)
//                    newPosition = position-1;
                if (momentList.size() > newPosition) {
                    final Moment video = momentList.get(newPosition);
                    if (video == null) {
                        momentList.remove(newPosition);
                        notifyDataSetChanged();
                        return;
                    }
                    final ItemViewHolder mItemViewHolder = (ItemViewHolder) holder;
//                    String url = getAppContext().getString(R.string.s3_url);
                    //                    url = url + profImagePath;
                    if (video.getGroupUuid() != null && !video.getGroupUuid().isEmpty()) {
                        mItemViewHolder.mSharedLayout.setVisibility(View.VISIBLE);
                        mItemViewHolder.mShareProfileName.setText(video.getGroupName());
                        mItemViewHolder.mSharedLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                geteGroupInfoRequest(video);
                            }
                        });
                    } else {
                        mItemViewHolder.mSharedLayout.setVisibility(View.GONE);
                    }

                    String description = video.getDescription();

//                    if(mOwnProfile)
//                        mItemViewHolder.mMoreImage.setVisibility(View.GONE);
                    if (description == null) {
                        mItemViewHolder.mDescriptionTextView.setVisibility(View.GONE);
                    } else if (description.isEmpty()) {
                        mItemViewHolder.mDescriptionTextView.setVisibility(View.GONE);
                    } else {
                        mItemViewHolder.mDescriptionTextView.setText(video.getDescription().trim());
                    }
                    mItemViewHolder.mDisplayNameTextView.setText(video.getOwnerDisplayName());
                    mItemViewHolder.mCaptionTextView.setText(video.getCaption());
                    mItemViewHolder.mCommentsCountTextView.setText(MyApplication.getAppContext().getString(R.string.comment_count,video.getCommentCount()));
                    mItemViewHolder.mShareCountTextView.setText(MyApplication.getAppContext().getString(R.string.share_count,video.getShareCount()));
                    mItemViewHolder.mLikesTextView.setText(MyApplication.getAppContext().getString(R.string.like_count,video.getLikes()));
                    mItemViewHolder.mDaysLeftTextView.setText(Utils.getInstance().getDateDiffString(video.getDate()));
                    video.setIndexPosition("" + newPosition);
                    momentList.set(newPosition, video);
                    String profImagePath = video.getOwnerThumbNail();
                    profImagePath = (profImagePath == null ? (video.getOwner() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? video.getOwner() + AppConsatants.FILE_EXTENSION_JPG : video.getOwnerThumbNail()));
                    mItemViewHolder.mProfileImageView.setTag(profImagePath);//tag of imageView == path to image
//                    new LoadImage(mItemViewHolder.mProfileImageView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    downloadContentFromS3Bucket(mItemViewHolder.mProfileImageView, profImagePath);
                    mItemViewHolder.mProfileImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (video.getOwner().equals(DataStorage.getInstance().getUsername())) {
                                Intent intent = new Intent(context, TimelineProfile.class);
                                context.startActivity(intent);
                            } else {
                                ModelManager.getInstance().setOtherProfile(new Profile());
                                TimelineFriendProfile.setFriendUsername(video.getOwner());
                                Intent mIntent = new Intent(context, TimelineFriendProfile.class);
                                context.startActivity(mIntent);
                            }
                        }
                    });
                    mItemViewHolder.mDisplayNameTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (video.getOwner().equals(DataStorage.getInstance().getUsername())) {
                                Intent intent = new Intent(context, TimelineProfile.class);
                                context.startActivity(intent);
                            } else {
                                ModelManager.getInstance().setOtherProfile(new Profile());
                                TimelineFriendProfile.setFriendUsername(video.getOwner());
                                Intent mIntent = new Intent(context, TimelineFriendProfile.class);
                                context.startActivity(mIntent);
                            }
                        }
                    });

                    final VideoPlayer mVideoPlayer = new VideoPlayer(context, Utils.getInstance().getLocalContentPath(video.getLink()));
                    final ImageView mImageView = new ImageView(context);//,Utils.getInstance().getLocalContentPath(video.getThumbNail()));
                    mItemViewHolder.videoPlayer.setOrientation(LinearLayout.VERTICAL);
                    mItemViewHolder.videoPlayer.setGravity(Gravity.CENTER_HORIZONTAL);
                    int screenHeightPixelsDefault = (screenWidthPixels / 3 + screenWidthPixels / 15);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                    LinearLayout.LayoutParams paramVideos = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                    params.width = screenHeightPixels;
//                    params.height = screenHeightPixelsDefault;
//                    paramVideos.gravity = Gravity.CENTER;
                    mItemViewHolder.videoPlayer.setLayoutParams(params);
                    mItemViewHolder.videoPlayer.setMinimumHeight(screenHeightPixelsDefault);
                    mImageView.setMinimumWidth(screenWidthPixels);
                    mImageView.setMinimumHeight(screenHeightPixelsDefault);
                    mImageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    Bitmap originalThumbnail = FileCache.getInstance().getBitmapFromMemCache(video.getThumbNail());
                    if(originalThumbnail==null) {
                        originalThumbnail = Utils.getInstance().decodeScaledBitmapFromSdCard(Utils.getInstance().getLocalContentPath(video.getThumbNail()));
                        if(originalThumbnail!=null)
                            FileCache.getInstance().addBitmapToMemoryCache(video.getThumbNail(),originalThumbnail);
                    }//                    Bitmap blurredBitmap=null;
//                    if(originalThumbnail!=null)
//                         blurredBitmap = BlurBitmap.blur(context, originalThumbnail);


                    if(originalThumbnail!=null)
                        mItemViewHolder.videoPlayer.setBackground(new BitmapDrawable(MyApplication.getAppContext().getResources(), originalThumbnail));
                    else
                        mItemViewHolder.videoPlayer.setBackgroundColor(getAppContext().getResources().getColor(R.color.grey_light));

//                    mItemViewHolder.videoPlayer.setBackgroundColor(getAppContext().getResources().getColor(R.color.grey_light));
                    mImageView.setLayoutParams(params);
                    mVideoPlayer.setLayoutParams(params);
                    mVideoPlayer.requestLayout();
                    mImageView.requestLayout();
                    mImageView.setBackgroundColor(getAppContext().getResources().getColor(R.color.grey_light));

                    mItemViewHolder.videoPlayer.addView(mVideoPlayer);
                    mItemViewHolder.videoPlayer.addView(mImageView);
                    int pos = 0;
                    if(mWithHeader)
                        pos=1;
                    if (newPosition == pos && getVisibility(video.getLink())) {
                        mImageView.setVisibility(View.GONE);
                        mVideoPlayer.setVisibility(View.VISIBLE);
                        videoPlayerController.handlePlayBack(video,false);
                        mItemViewHolder.progressBar.setVisibility(View.GONE);
                    } else if (getVisibility(video.getThumbNail())) {
                        setImageViewDimension(mImageView,originalThumbnail);
                        mImageView.setVisibility(View.VISIBLE);
                        mVideoPlayer.setVisibility(View.GONE);
                        mImageView.setBackground(new BitmapDrawable(MyApplication.getAppContext().getResources(), originalThumbnail));
                        mItemViewHolder.progressBar.setVisibility(View.GONE);
                    }else{
                        mImageView.setVisibility(View.VISIBLE);
                        mVideoPlayer.setVisibility(View.GONE);
                        mImageView.setBackgroundColor(getAppContext().getResources().getColor(R.color.grey_light));
                        mItemViewHolder.progressBar.setVisibility(View.VISIBLE);
                    }

//                    mItemViewHolder.videoPlayer.setMinimumHeight(screenHeightPixelsDefault);

                    videoPlayerController.loadVideoWithThumbnail(video, mVideoPlayer, mItemViewHolder.progressBar, mImageView, mItemViewHolder.videoPlayer);

                    mItemViewHolder.videoPlayer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            refreshimeline = true;
                            Utils.getInstance().setMomentObj(video);
                            Intent mIntent = new Intent(context, TimelineMomentDetail.class);
                            Bundle mBundle = new Bundle();
                            mBundle.putString(AppConsatants.UUID, video.getUuid());
                            mBundle.putBoolean(AppConsatants.UPLOAD_FILE_PATH, false);
                            mIntent.putExtras(mBundle);
                            context.startActivity(mIntent);
//                            mVideoPlayer.changePlayState();
                        }
                    });
                    mItemViewHolder.mLikesImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            long count = video.getLikes();//Utils.getInstance().getIntegerValueOfString(video.getLikes());

                            if (!video.getIsLikedBy()) {
                                count = count + 1;
                                video.setLikes(count);
                                mItemViewHolder.mLikesTextView.setText(MyApplication.getAppContext().getString(R.string.like_count,count));
//                        mItemViewHolder.mLikesImageView.setCompoundDrawables(context.getDrawable(R.drawable.heart_selected), null, null, null);
                                mItemViewHolder.mLikesImageView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.heart_selected, 0, 0, 0);
                                video.setIsLikedBy(true);
                                momentList.set(newPosition, video);
                            } else {
                                count = count - 1;
                                if (count < 0)
                                    count = 0;

                                video.setLikes(count);
                                mItemViewHolder.mLikesTextView.setText(MyApplication.getAppContext().getString(R.string.like_count,count));

                                mItemViewHolder.mLikesImageView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.heart, 0, 0, 0);

                                video.setIsLikedBy(false);
                                momentList.set(newPosition, video);
                            }
                            updateLike(newPosition, video);

                        }
                    });
                    mItemViewHolder.mCommentsImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utils.getInstance().setMomentObj(video);
                            Intent mIntent = new Intent(context, CommentActivity.class);
                            Bundle mBundle = new Bundle();
                            mBundle.putString(AppConsatants.UUID, video.getUuid());
                            mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, false);
                            mIntent.putExtras(mBundle);
                            context.startActivity(mIntent);
                        }
                    });
                    mItemViewHolder.mShareImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            moreFlag = false;
                            Utils.getInstance().setMomentObj(video);
                            v.showContextMenu();
                        }
                    });
                    mItemViewHolder.mMoreLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            moreFlag = true;
                            Utils.getInstance().setMomentObj(video);
                            v.showContextMenu();
                        }
                    });
                    if (video.getIsLikedBy()) {
                        mItemViewHolder.mLikesImageView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.heart_selected, 0, 0, 0);
                    } else {
                        mItemViewHolder.mLikesImageView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.heart, 0, 0, 0);
                    }
//            Utils.getInstance().underlineTextView(video.getOwnerDisplayName(), mItemViewHolder.mDisplayNameTextView);

//                    url = url.replace("+", "%2B");
//                    Log.i("Picasso", url);
//                    Picasso.with(context).load(url)
//                            .error(R.drawable.user)
//                            .placeholder(R.drawable.user)
////                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                            .networkPolicy(NetworkPolicy.NO_CACHE)
//                            .into(mItemViewHolder.mProfileImageView);
                    if (video.getCaption() == null) {
                        mItemViewHolder.mCaptionLayout.setVisibility(View.GONE);
                    } else if (video.getCaption().trim().isEmpty()) {
                        mItemViewHolder.mCaptionLayout.setVisibility(View.GONE);
                    } else {
                        mItemViewHolder.mCaptionLayout.setVisibility(View.VISIBLE);
                    }

                    if (video.getDescription() == null) {
                        mItemViewHolder.mDescriptionLayout.setVisibility(View.GONE);
                    } else if (video.getDescription().trim().isEmpty()) {
                        mItemViewHolder.mDescriptionLayout.setVisibility(View.GONE);
                    } else {
                        mItemViewHolder.mDescriptionLayout.setVisibility(View.VISIBLE);
                    }
                }
            } catch (NullPointerException e) {

            } catch (ArrayIndexOutOfBoundsException e) {

            } catch (Exception e) {

            }
        } else if (getItemViewType(position) == VIEW_TYPE_LOADING) {
            final LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;

            loadingViewHolder.loadingMore.setText(MyApplication.getAppContext().getResources().getString(R.string.action_load_more));
            loadingViewHolder.loadingMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadingViewHolder.loadingMore.setText(MyApplication.getAppContext().getResources().getString(R.string.action_loading));
                    iLoadMore.loadMoreItems(true);
                }
            });
        }
    }

    private boolean getVisibility(String fileName) {
        boolean visible = false;
        if (fileName != null && !fileName.isEmpty()) {
            String filePath = Utils.getInstance().getLocalContentPath(fileName);
            File vidFile = new File(filePath);
            if (vidFile != null && vidFile.exists()) {
                visible = true;
            }
        }
        return visible;
    }

    public static void openPersonList(Context contex, boolean friend, boolean follower, boolean following) {
        try {
            Intent mIntent = new Intent(contex, PersonActivity.class);
            Bundle mBundle = new Bundle();
            mBundle.putBoolean(AppConsatants.PERSON_FRIEND, friend);
            mBundle.putBoolean(AppConsatants.PERSON_FOLLOWER, follower);
            mBundle.putBoolean(AppConsatants.PERSON_FOLLOWING, following);
            mBundle.putBoolean(AppConsatants.OTHER_PERSON_PROFILE, !mOwnProfile);
            mIntent.putExtras(mBundle);
            contex.startActivity(mIntent);
        } catch (Exception e) {

        }
    }

    // need to override this method
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        return momentList.get(position) == null ? VIEW_TYPE_LOADING : TYPE_ITEM;
    }

    private void updateLike(final int position, final Moment video) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                updateLikeStatus(position, video);
            }
        }, 250);
    }

    public void deleteMoment(final int position, final Moment video) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                deleteMyMoment(position, video);
            }
        }, 250);
    }

    private void downloadContentFromS3Bucket(HeaderViewHolder holder, String filePath) {
        if(filePath!=null && !filePath.isEmpty()) {

            holder.mProfileImageViewBackground.setImageResource(R.drawable.cover_bg);
            Bitmap image = getBitmapFromMemCache(filePath);
            if (image != null) {
                holder.mProfileImageView.setImageBitmap(image);
                Bitmap blurredBitmap = BlurBitmap.blur(context, image);
                holder.mProfileImageViewBackground.setImageBitmap(blurredBitmap);
            } else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null) {
                        addBitmapToMemoryCache(filePath, image1);
                        holder.mProfileImageView.setImageBitmap(image1);
                        Bitmap blurredBitmap = BlurBitmap.blur(context, image1);
                        holder.mProfileImageViewBackground.setImageBitmap(blurredBitmap);
//                        Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fadein);
//                        holder.mProfileImageView.startAnimation(myFadeInAnimation);
                    }
                }
            }
            download(holder, filePath);
        }
    }

    private void download(final HeaderViewHolder holder, String filePath) {
        if (userFileManager == null)
            return;

//        userFileManager.setContentCacheSize(1024 * 1024 * 35);
        long fileSize = (1024 * 512);
//            userFileManager.setContentCacheSize(1024 * 1024 * 15);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePath = Utils.getInstance().getLocalContentPath(contentItem.getFilePath());
                Bitmap image = Utils.getInstance().loadImageFromStorage(filePath, false);
                if (image != null) {
                    addBitmapToMemoryCache(filePath, image);
                    holder.mProfileImageView.setImageBitmap(image);
                    Bitmap blurredBitmap = BlurBitmap.blur(context, image);
                    holder.mProfileImageViewBackground.setImageBitmap(blurredBitmap);
//                    Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fadein);
//                    holder.mProfileImageView.startAnimation(myFadeInAnimation);
                }

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }

    private boolean isPositionHeader(int position) {
        return this.mWithHeader && position == 0;
    }

    @Override
    public int getItemCount() {
        if (momentList == null)
            return 0;
        return momentList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
//        holder.setIsRecyclable(true);
        try {
            if (holder instanceof ItemViewHolder) {
//                Moment video = ((ItemViewHolder) holder).videoPlayer.getVideo();
                ((ItemViewHolder) holder).videoPlayer.removeAllViews();
//                Log.i("recycling &&&&&&&&&&& ", video.toString());
//                videoPlayerController.loadVideoReset(video, ((ItemViewHolder) holder).videoPlayer);

//                ((ItemViewHolder) holder).videoPlayer.reset();
            }
        } catch (Exception e) {

        }
    }

    private void setImageViewDimension(ImageView imageView,Bitmap bitmap){
        int newWidth,newHeight;

        Configuration configuration = MyApplication.getAppContext().getResources().getConfiguration();
        int screenHeightDp = configuration.screenWidthDp;
        int deviceWidth = Utils.getInstance().convertDpToPixel(screenHeightDp, context);
        newWidth = deviceWidth;
        newHeight = newWidth;
        try {
            if(bitmap!=null) {
                int intWidth = bitmap.getWidth();
                int intHeight = bitmap.getHeight();
                if (intWidth < deviceWidth) {
                    float height = intHeight;
                    float width = intWidth;
                    height = (deviceWidth / width) * height;
                    newHeight = (int) height;
                    newWidth = deviceWidth;
                }
            }
            imageView.setMinimumWidth(newWidth);
            imageView.setMinimumHeight(newHeight);
        } catch(IllegalStateException e) {
        }catch (Exception e) {
//            Log.i("Videosize", "setting size: " +e.getMessage());
        }

    }

    // need to override this method
    private void geteGroupInfoRequest(Moment moment) {
        try {

            String userUuid = DataStorage.getInstance().getUserUUID();
            String url = ServiceURLManager.getInstance().getGroupProfileUrl(moment.getGroupUuid(), userUuid);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    GroupProfile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), GroupProfile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        String userUuid = DataStorage.getInstance().getUserUUID();
                        String urlList = ServiceURLManager.getInstance().getGroupListUrl(IAPIConstants.API_KEY_GROUP_LIST, userUuid);

                        VolleySingleton.getInstance(getAppContext()).refresh(urlList, false);

                        ModelManager.getInstance().setGroupProfile(obj);
                        String username = obj.getUuid();
                        if (username != null && !username.isEmpty()) {
                            ModelManager.getInstance().setGroupProfile(obj);
                            Intent mIntentActivity = new Intent(context, GroupProfileActivity.class);
                            context.startActivity(mIntentActivity);
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            VolleySingleton.getInstance(getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void updateLikeStatus(final int position, final Moment video) {
        try {
            if (video == null)
                return;
            String url = ServiceURLManager.getInstance().getMomentLikeUrl(IAPIConstants.API_KEY_MOMENT_LIKE, video.getUuid(), DataStorage.getInstance().getUsername());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Moment obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Moment.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
//                        momentList.set(position, obj);
//                        notifyItemChanged(position);
                        String username = DataStorage.getInstance().getUsername();
                        String url = ServiceURLManager.getInstance().getMomentFeedsListUrl(IAPIConstants.API_KEY_GET_MOMENT_FEEDS_TIMELINE_LIST, username, username, AppConsatants.FALSE, "0", "0");

                        String url1 = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, username, username, AppConsatants.FALSE, "0", "0");
                        VolleySingleton.getInstance(getAppContext()).refresh(url1, false);
                        VolleySingleton.getInstance(getAppContext()).refresh(url, false);
                    }
//                    notifyDataSetChanged();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    long count = video.getLikes();
                    if (!video.getIsLikedBy()) {
                        count = count + 1;
                        video.setLikes(count);
                        video.setIsLikedBy(true);
                        momentList.set(position, video);
                    } else {
                        count = count - 1;
                        if (count < 0)
                            count = 0;

                        video.setLikes(count);
                        video.setIsLikedBy(false);
                        momentList.set(position, video);
                    }
                    notifyItemChanged(position);
                }
            });
            VolleySingleton.getInstance(getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public CustomFontTextView loadingMore;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            loadingMore = (CustomFontTextView) itemView.findViewById(R.id.load_more);
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        final ImageView mProfileImageViewBackground;
        final CustomFontTextView mProfileNameTextView;
        public CustomFontTextView mFriendsTextView;
        public CustomFontTextView mFollowersTextView;
        public CustomFontTextView mFollowingTextView;
        public LinearLayout mProfileLayout;
        public LinearLayout mFriendLayout;
        public LinearLayout mFollowerLayout;
        public LinearLayout mFollowingLayout;
        public RelativeLayout mHeaderLayout;
        private CircularImageView mProfileImageView;
        private ImageView mProfileImageViewEdit;
        public CustomFontTextView addFriendButton, followButton;
        public boolean following = false;
        public boolean friend = false;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            mProfileImageViewBackground = (ImageView) itemView.findViewById(R.id.home_profile_image_background);
            mProfileImageView = (CircularImageView) itemView.findViewById(R.id.home_profile_image);
            mProfileImageViewEdit = (ImageView) itemView.findViewById(R.id.home_profile_image_edit);
            mProfileNameTextView = (CustomFontTextView) itemView.findViewById(R.id.home_profile_name);
            mFriendsTextView = (CustomFontTextView) itemView.findViewById(R.id.home_profile_friends);
            mFollowersTextView = (CustomFontTextView) itemView.findViewById(R.id.home_profile_followers);
            mFollowingTextView = (CustomFontTextView) itemView.findViewById(R.id.home_profile_following);
            addFriendButton = (CustomFontTextView) itemView.findViewById(R.id.add_friend_button);
            followButton = (CustomFontTextView) itemView.findViewById(R.id.follow_button);

            mProfileLayout = (LinearLayout) itemView.findViewById(R.id.home_profile_layout);
            mFriendLayout = (LinearLayout) itemView.findViewById(R.id.home_profile_friends_layout);
            mFollowerLayout = (LinearLayout) itemView.findViewById(R.id.home_profile_followers_layout);
            mFollowingLayout = (LinearLayout) itemView.findViewById(R.id.home_profile_following_layout);
            mHeaderLayout = (RelativeLayout) itemView.findViewById(R.id.timeline_header);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        public CustomFontTextView mDisplayNameTextView;
        public CustomFontTextView mDaysLeftTextView;
        public CustomFontTextView mCommentsCountTextView;
        public CustomFontTextView mShareCountTextView;
        public CustomFontTextView mLikesTextView;
        public CustomFontTextView mCaptionTextView;
        public CustomFontTextView mDescriptionTextView;
        public CircularImageView mProfileImageView;
        public CustomFontTextView mLikesImageView;
        public CustomFontTextView mCommentsImageView;
        public CustomFontTextView mShareImageView;
        public CustomFontTextView mShareProfileName;
        //        public RelativeLayout mThumbnailLayout;
        public LinearLayout mCaptionLayout;
        public LinearLayout mDescriptionLayout;
        public LinearLayout mSharedLayout;
        public LinearLayout mMoreLayout;
//        public ImageView mThumbnailImage;
//        public ImageView mMoreImage;

        public ProgressBar progressBar;
        public RelativeLayout layout;
        public LinearLayout videoPlayer;
//        public LinearLayout videoPlayer1;

        ItemViewHolder(View v) {
            super(v);
            layout = (RelativeLayout) v.findViewById(R.id.layout);
            mDisplayNameTextView = (CustomFontTextView) v.findViewById(R.id.timeline_post_user);
            mCommentsCountTextView = (CustomFontTextView) v.findViewById(R.id.timeline_comments);
            mShareCountTextView = (CustomFontTextView) v.findViewById(R.id.timeline_share);
            mLikesTextView = (CustomFontTextView) v.findViewById(R.id.timeline_likes);
            mDaysLeftTextView = (CustomFontTextView) v.findViewById(R.id.timeline_time);
            mCaptionTextView = (CustomFontTextView) v.findViewById(R.id.timeline_caption);
            mDescriptionTextView = (CustomFontTextView) v.findViewById(R.id.timeline_description);
            mCaptionLayout = (LinearLayout) itemView.findViewById(R.id.timeline_caption_layout);
            mDescriptionLayout = (LinearLayout) itemView.findViewById(R.id.timeline_description_layout);
            mProfileImageView = (CircularImageView) v.findViewById(R.id.timeline_friend_image);
            mLikesImageView = (CustomFontTextView) v.findViewById(R.id.timeline_likes_button);
            mCommentsImageView = (CustomFontTextView) v.findViewById(R.id.timeline_comments_button);
            mShareImageView = (CustomFontTextView) v.findViewById(R.id.timeline_share_button);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
            videoPlayer = (LinearLayout) v.findViewById(R.id.timeline_videoplayer_layout);
//            videoPlayer = (RelativeLayout) v.findViewById(R.id.timeline_videoplayer_layout);
            mSharedLayout = (LinearLayout) v.findViewById(R.id.timeline_shared_layout);
            mShareProfileName = (CustomFontTextView) v.findViewById(R.id.timeline_shared);
//            mThumbnailLayout = (RelativeLayout) itemView.findViewById(R.id.timeline_thumbnail_layout);
//            mThumbnailImage = (ImageView) v.findViewById(R.id.timeline_thumbnail);
            mMoreLayout = (LinearLayout) itemView.findViewById(R.id.more_layout);
//            mMoreImage = (ImageView) v.findViewById(R.id.timeline_more_image);
            v.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            Moment moment = Utils.getInstance().getMomentObj();

            if (moment != null) {
                if (moreFlag) {
                    if (moment.getOwner().equalsIgnoreCase(DataStorage.getInstance().getUsername())) {
                        MenuItem delete = menu.add(100, 101, 0, context.getResources().getString(R.string.action_delete));
                        delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                final Moment moment = Utils.getInstance().getMomentObj();
                                showOkCancel(context.getResources().getString(R.string.alert_message_delete_moment), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (moment != null)
                                            deleteMoment(Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()), moment);
                                    }
                                });

                                return true;
                            }
                        });

                    } else {
                        if(moment.getOwnerBlocked()!=null &&!moment.getOwnerBlocked()) {
                            MenuItem block = menu.add(100, 102, 1, context.getResources().getString(R.string.block_user,moment.getOwnerDisplayName()));
                            block.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    final Moment moment = Utils.getInstance().getMomentObj();
                                    showOkCancel(context.getResources().getString(R.string.alert_message_block_user, moment.getOwnerDisplayName()), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (moment != null)
                                                sendBlockUserRequest(moment, true,Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()));
                                        }
                                    });

                                    return true;
                                }
                            });
                        }else{
                            MenuItem unblock = menu.add(100, 102, 1, context.getResources().getString(R.string.unblock_user,moment.getOwnerDisplayName()));
                            unblock.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    final Moment moment = Utils.getInstance().getMomentObj();
                                    if (moment != null)
                                        sendBlockUserRequest(moment, false,Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()));

                                    return true;
                                }
                            });
                        }
                        MenuItem report = menu.add(100, 103, 2, context.getResources().getString(R.string.report));
                        report.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                final Moment moment = Utils.getInstance().getMomentObj();
                                showOkCancel(context.getResources().getString(R.string.alert_message_report_user, moment.getOwnerDisplayName()), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (moment != null)
                                            sendReportMomentRequest(Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()), moment);

                                    }
                                });
                                return true;
                            }
                        });
                    }
                } else {


                    MenuItem share = menu.add(100, 104, 3, context.getResources().getString(R.string.share));
                    share.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            Moment moment = Utils.getInstance().getMomentObj();
                            MomentShare momentShare = new MomentShare();
                            momentShare.setOriginalMoment(moment);
                            UploadffmpegService.setMomentShare(momentShare);
                            Intent mIntent = new Intent(context, UploadActivity.class);
                            Bundle mBundle = new Bundle();
                            mBundle.putString(AppConsatants.MOMENT_PATH, Utils.getInstance().getLocalContentPath(moment.getLink()));
                            mBundle.putBoolean(AppConsatants.RESHARE, true);
                            mIntent.putExtras(mBundle);
                            context.startActivity(mIntent);
                            return true;
                        }
                    });
                    MenuItem facebook = menu.add(100, 105, 4, "Share on facebook");
                    facebook.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            Moment moment = Utils.getInstance().getMomentObj();

                            if (moment != null) {
                                String url = getAppContext().getString(R.string.s3_url);
                                String appUrl = getAppContext().getString(R.string.api_google_play_store_url);
                                url = url + moment.getThumbNail();
                                ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
                                        .setContentTitle(moment.getCaption())
                                        .setContentDescription(moment.getDescription())
                                        .setContentUrl(Uri.parse(appUrl))
                                        .setImageUrl(Uri.parse(url))
                                        .build();
                                ShareDialog.show(activity, shareLinkContent);
//                            if (moment != null)
//                                deleteMoment(Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()), moment);
                            }
                            return true;
                        }
                    });
                    MenuItem twitter = menu.add(100, 106, 5, "Share on twitter");
                    twitter.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            Moment moment = Utils.getInstance().getMomentObj();
                            String url = getAppContext().getString(R.string.s3_url);
                            url = url + moment.getThumbNail();
                            URL vowmeeUrl = null;
                            String filePath = Utils.getInstance().getLocalContentPath(moment.getThumbNail());// Utils.getInstance().getUserFileManager().getLocalContentPath() + File.separator + moment.getThumbNail();
                            File myImageFile = new File(filePath);
                            Uri imageUri = null;
                            if (myImageFile.exists()) {
                                imageUri = Uri.fromFile(myImageFile);
                            } else {
                                imageUri = Uri.parse("www.vowmee.com");
                            }
                            try {
                                vowmeeUrl = new URL("http://www.vowmee.com");
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                            TweetComposer.Builder builder = new TweetComposer.Builder(context)
                                    .text("Vowmee\n" + moment.getCaption() + "\n" + moment.getDescription())
                                    .image(imageUri)
                                    .url(vowmeeUrl);
                            builder.show();
//                            if (moment != null)
//                                deleteMoment(Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()), moment);

                            return true;
                        }
                    });
                }
            }
        }

    }

    private void sendFollowingRemoveRequest(final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_REMOVE_FOLLOW);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);

                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        profile.setFollowedByViewer(false);
//                        searchList.set(position, profile);
                        notifyDataSetChanged();
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFollowingAddRequest(final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_ADD_FOLLOW);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);

                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        profile.setFollowedByViewer(true);
//                        searchList.set(position, profile);
                        notifyDataSetChanged();
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFriendsRemoveRequest(final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_REMOVE_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);

                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        profile.setFriendWithViewer(false);
//                        searchList.set(position, profile);
                        notifyDataSetChanged();
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    ;
                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFriendsAddRequest(final Profile profile, final int position) {
        try {
            if (profile == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_ADD_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            if (username.equals(profile.getUsername())) {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);

                return;
            }

            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(profile.getUsername());
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        profile.setFriendRequestFromViewer(true);
//                        searchList.set(position, profile);
                        notifyDataSetChanged();
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Friend request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgressBar(final String msg) {
        ((AppCompatActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = ProgressDialog.show(context, null, null, true, false);
                progressDialog.setContentView(R.layout.progressbar);
                TextView progressBarMessage = (TextView) progressDialog.findViewById(R.id.progressBar_message);
                progressBarMessage.setText(msg);
            }
        });
    }

    private void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            FileCache.getInstance().addBitmapToMemoryCache(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return  FileCache.getInstance().getBitmapFromMemCache(key);
    }

    private void downloadContentFromS3Bucket(ImageView imageView, String filePath){
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null) {
                        addBitmapToMemoryCache(filePath, image1);
                        imageView.setImageBitmap(image1);
                        Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fadein);
                        imageView.startAnimation(myFadeInAnimation);
                    }else {
                        imageView.setImageResource(R.drawable.user);
                    }
                } else {
                    imageView.setImageResource(R.drawable.user);
                }
            }
            if(!disableLoad)
                download(imageView, filePath);
        }
    }

    private void  download(final ImageView imageView,final String filePath){
        if (userFileManager == null )
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                if (image1 != null)
                    addBitmapToMemoryCache(filePath, image1);
//                if(contentItem !=null &&!contentItem.getFilePath().isEmpty()) {
////                    new LoadImage(imageView).execute();
//                }

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }

    private void sendReportMomentRequest(final int position, final Moment momentObj) {
        try {
//            showProgressBar("Processing your request");
            String userUuid = DataStorage.getInstance().getUserUUID();
            String momentUuid = momentObj.getUuid();
            String url = ServiceURLManager.getInstance().getMomentReportUrl(momentUuid, userUuid);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Moment obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Moment.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        momentObj.setReportedAsAbuse(true);
                        momentList.set(position, momentObj);
                        final String username = DataStorage.getInstance().getUsername();
                        String url = ServiceURLManager.getInstance().getMomentFeedsListUrl(IAPIConstants.API_KEY_GET_MOMENT_FEEDS_TIMELINE_LIST, username, username, AppConsatants.FALSE, "0", "0");
                        VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(url, true);
                        notifyItemChanged(position);
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_report), Utils.SHORT_TOAST);

                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null && error.networkResponse != null) {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    }else {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_no_internet), Utils.SHORT_TOAST);

                    }

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void sendBlockUserRequest(final Moment momentObj,final boolean block,final int position) {
        try {
//            showProgressBar("Processing your request");
            String userUuid = DataStorage.getInstance().getUserUUID();
            String blockinguuid = momentObj.getOwnerUuid();
            String url = ServiceURLManager.getInstance().getBlockUserUrl(userUuid, blockinguuid);
            if(!block)
                url = ServiceURLManager.getInstance().getUnBlockUserUrl(userUuid, blockinguuid);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        momentObj.setOwnerBlocked(block);
                        momentList.set(position, momentObj);
                        notifyItemChanged(position);
                    }
                    notifyDataSetChanged();
                    if(block) {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_blocked), Utils.SHORT_TOAST);
                    }else{
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_unblocked), Utils.SHORT_TOAST);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    if (error != null && error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_CONFLICT) {
                            showError(context.getResources().getString(R.string.error_user_already_block, momentObj.getOwnerDisplayName()), null);

                        } else {
                            Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                        }
                    }else {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_no_internet), Utils.SHORT_TOAST);

                    }

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void deleteMyMoment(final int position, Moment video) {
        try {
            if (video == null)
                return;
            String url = ServiceURLManager.getInstance().getMomentDeleteUrl(video.getUuid(), DataStorage.getInstance().getUsername());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Moment obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Moment.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        momentList.remove(position);

                        final String username = DataStorage.getInstance().getUsername();
                        String url = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, username, username, AppConsatants.FALSE, "1", "10");
                        VolleySingleton.getInstance(getAppContext()).refresh(url, true);
                    }
                    notifyDataSetChanged();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null && error.networkResponse != null) {
                         Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    }else {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_no_internet), Utils.SHORT_TOAST);

                    }
                }
            });
            VolleySingleton.getInstance(getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showError(final String message, final DialogInterface.OnClickListener okClicked) {
        try{
        ((AppCompatActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {

                new AlertDialog.Builder(context).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, okClicked)
                        .show();
            }
        });
    }catch(WindowManager.BadTokenException e){

    }catch (Exception e){}
    }

    private void showOkCancel(final String message, final DialogInterface.OnClickListener okClicked) {
        try {
            ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(context).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(message)
                            .setNegativeButton(android.R.string.cancel, null)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .show();
                }
            });
        }catch(WindowManager.BadTokenException e){

        }catch (Exception e){}
    }

/*    class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private ImageView imv;
        private String path;

        public LoadImage(ImageView imv) {
            this.imv = imv;

            this.path = imv.getTag().toString();
        }

        @Override
        protected Bitmap doInBackground(Object... params) {
            Bitmap bitmap = null;
            if(path!=null && !path.isEmpty()) {
                String filePathImage = Utils.getInstance().getLocalContentPath(path);
                File file = new File(filePathImage);
                if (file.exists()) {
                    bitmap = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if(profileImageDimensPixelsDefault >0&& profileImageDimensPixelsDefault >0)
                        bitmap = Bitmap.createScaledBitmap(bitmap, profileImageDimensPixelsDefault, profileImageDimensPixelsDefault,false);
                }
//            download(holder, filePath,width,height);
            }
            return bitmap;
        }
        @Override
        protected void onPostExecute(Bitmap result) {
            if (!imv.getTag().toString().equals(path)) {
               *//* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. *//*
                return;
            }

            if(result != null && imv != null){
                imv.setVisibility(View.VISIBLE);
                imv.setImageBitmap(result);
            }else{
                imv.setImageResource(R.drawable.user);
            }
        }

    }*/

    public void setOnLoadMoreListener(ILoadMoreItems iLoadMoreItems) {
        this.iLoadMore = iLoadMoreItems;
    }
}
