package net.xvidia.vowmee.listadapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;

import net.xvidia.vowmee.GroupProfileActivity;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CustomFontTextView;
import net.xvidia.vowmee.network.model.GroupProfile;
import net.xvidia.vowmee.network.model.ModelManager;

import java.io.File;
import java.util.ArrayList;

public class GroupsGridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

	private ArrayList<GroupProfile> groupProfiles;
	Context mContext;
	boolean groupSelection;
	public static SparseBooleanArray selectedItems;
	private int screenWidthPixels;
	private UserFileManager userFileManager;
	private int imageWidthPixelsDefault;


	public GroupsGridAdapter(Context context, ArrayList<GroupProfile> groupProfiles,boolean selection) {
		this.mContext = context;
		this.groupProfiles = groupProfiles;
		userFileManager = Utils.getInstance().getUserFileManager();
		groupSelection = selection;
		selectedItems = new SparseBooleanArray(groupProfiles.size());
		Configuration configuration = mContext.getResources().getConfiguration();
		int screenWidthDp = configuration.screenWidthDp; //The current width of the available screen space, in dp units, corresponding to screen width resource qualifier.
//                    int smallestScreenWidthDp = configuration.smallestScreenWidthDp;
		screenWidthPixels = Utils.getInstance().convertDpToPixel(screenWidthDp, mContext);
		imageWidthPixelsDefault = (screenWidthPixels /2)- (screenWidthPixels / 10);
		screenWidthPixels = imageWidthPixelsDefault-imageWidthPixelsDefault/10;
	}


	public class ViewHolder extends RecyclerView.ViewHolder  {

		private CustomFontTextView mDisplayNameTextView;
		private CustomFontTextView mCreationDateTextView;
		private ImageView mProfileImageView;
		private CheckBox checkBox;
		private RelativeLayout groupLayout;

		public ViewHolder(View v) {
			super(v);
			mDisplayNameTextView = (CustomFontTextView) v.findViewById(R.id.group_name);
			mCreationDateTextView = (CustomFontTextView) v.findViewById(R.id.group_creation_date);
			mProfileImageView = (ImageView) v.findViewById(R.id.group_image);
			checkBox = (CheckBox) v.findViewById(R.id.group_select_checkbox);
			groupLayout = (RelativeLayout)v.findViewById(R.id.group_layout);
		}


	}

	
	@Override
	public int getItemCount() {
		return groupProfiles.size();
	}
	

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		// create a new view
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_grideview_item, parent, false);
		ViewHolder viewHolder = new ViewHolder(v);

		return viewHolder;
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
		try {
			final GroupProfile obj = groupProfiles.get(position);
			String profImagePath = obj.getProfileImage();
			final ViewHolder holder = (ViewHolder) viewHolder;
			holder.mProfileImageView.setMinimumWidth(imageWidthPixelsDefault);
			holder.mProfileImageView.setMinimumHeight(screenWidthPixels);
			holder.mProfileImageView.setMaxWidth(imageWidthPixelsDefault);
			holder.mProfileImageView.setMaxHeight(screenWidthPixels);
//			holder.mProfileImageView.setScaleType(ImageView.ScaleType.FIT_XY);
			holder.mProfileImageView.setTag(profImagePath);//tag of imageView == path to image
//			new LoadImage(holder.mProfileImageView).execute();
			downloadContentFromS3Bucket(holder.mProfileImageView,profImagePath);
			downloadThumbnail(obj.getThumbNail());
			if(groupSelection) {
				holder.checkBox.setChecked(selectedItems.get(position));
			}
			holder.mDisplayNameTextView.setText(obj.getName());
				holder.checkBox.setVisibility(groupSelection==false?View.GONE:View.VISIBLE);
			holder.mCreationDateTextView.setVisibility(View.GONE);
//				holder.mCreationDateTextView.setText(mContext.getResources().getString(R.string.group_last_active,Utils.getInstance().getDateDiffString(obj.getDateOfCreation())));
				holder.groupLayout.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if(!groupSelection) {
							ModelManager.getInstance().setGroupProfile(obj);
							Intent mIntentActivity = new Intent(mContext, GroupProfileActivity.class);
							mContext.startActivity(mIntentActivity);
						}else{
							if (selectedItems.get(position, false)) {
								selectedItems.delete(position);
//								holder.checkBox.setChecked(false);
							} else {
								selectedItems.put(position, true);
//								holder.checkBox.setChecked(true);
							}

							notifyItemChanged(position);
						}
					}
				});

		} catch (NullPointerException e) {

		} catch (Exception e) {

		}
	}
	private void downloadContentFromS3Bucket(ImageView imageView, String filePath){
		if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
			if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
				return;
			}

			imageView.setVisibility(View.VISIBLE);
			Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);

			if(image!=null ) {
				image = Bitmap.createScaledBitmap(image,imageWidthPixelsDefault,screenWidthPixels,false);
				imageView.setScaleType(ImageView.ScaleType.FIT_XY);
				imageView.setImageBitmap(image);
			}else {
				String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
				File file = new File(filePathImage);
				if (file.exists()) {
					Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
					if (image1 != null) {
						image1 = Bitmap.createScaledBitmap(image1,imageWidthPixelsDefault,screenWidthPixels,false);
						FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
						imageView.setScaleType(ImageView.ScaleType.FIT_XY);
						imageView.setImageBitmap(image1);
						Animation myFadeInAnimation = AnimationUtils.loadAnimation(mContext, R.anim.fadein);
						imageView.startAnimation(myFadeInAnimation);

					}else{
						imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
						imageView.setImageResource(R.drawable.group_default);
					}
				} else {
					imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
					imageView.setImageResource(R.drawable.group_default);
				}
			}
//            if(!disableLoad)
			download(imageView, filePath);
		}
	}

	private void  download(final ImageView imageView, final String filePath){
		if (userFileManager == null)
			return;
		long fileSize = (1024 * 512);
		userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
			@Override
			public void onSuccess(ContentItem contentItem) {
				String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
				File file = new File(filePathImage);
				if (file.exists()) {
					Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);

					if (image1 != null) {
						image1 = Bitmap.createScaledBitmap(image1,imageWidthPixelsDefault,screenWidthPixels,false);
						FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
						imageView.setScaleType(ImageView.ScaleType.FIT_XY);
						imageView.setImageBitmap(image1);
					}
				}

			}

			@Override
			public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
			}

			@Override
			public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
			}
		});
	}


	private void  downloadThumbnail(String filePath){
		if (userFileManager == null)
		return;
		long fileSize = (1024 * 512);
		userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
			@Override
			public void onSuccess(ContentItem contentItem) {

			}

			@Override
			public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//				Log.i("F/Ile download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
			}

			@Override
			public void onError(String filePath, Exception ex) {
//				Log.i("FIle download " + filePath, ex.getMessage());
			}
		});
	}

	@Override
	public void onViewRecycled(RecyclerView.ViewHolder holder) {
		super.onViewRecycled(holder);
        holder.setIsRecyclable(false);
	}

	class LoadImage extends AsyncTask<Object, Void, Bitmap> {

		private ImageView imv;
		private String path;

		public LoadImage(ImageView imv) {
			this.imv = imv;

			this.path = imv.getTag().toString();
		}

		@Override
		protected Bitmap doInBackground(Object... params) {
			Bitmap bitmap = null;
			if(path!=null && !path.isEmpty()) {
				String filePathImage = Utils.getInstance().getLocalContentPath(path);
				File file = new File(filePathImage);
				if (file.exists()) {
					bitmap = Utils.getInstance().loadImageFromStorage(filePathImage, false);
					if(imageWidthPixelsDefault>0&&screenWidthPixels>0)
						bitmap = Bitmap.createScaledBitmap(bitmap,imageWidthPixelsDefault,screenWidthPixels,false);
				}
//            download(holder, filePath,width,height);
			}
			return bitmap;
		}
		@Override
		protected void onPostExecute(Bitmap result) {
			if (!imv.getTag().toString().equals(path)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
				return;
			}

			if(result != null && imv != null){
				imv.setVisibility(View.VISIBLE);
				imv.setImageBitmap(result);
			}else{
				imv.setImageResource(R.drawable.group_default);
			}
		}

	}

}
