package net.xvidia.vowmee.listadapter;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CursorAdapter;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.PhoneContactInviteActivity;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.TimelineFriendProfile;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.contactsync.provider.FeedContract;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.helper.CustomFontButton;
import net.xvidia.vowmee.helper.CustomFontTextView;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.FriendRequest;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Ravi_office on 05-Feb-16.
 */
public class PhoneContactsAdapter extends CursorAdapter {
//    private Context mContext;
    LayoutInflater mInflater;
//    public static SparseArray<String> selectedItems;


    public PhoneContactsAdapter(Context context, Cursor c, int flags) {
        super(context, c, 0);
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = mInflater.inflate(R.layout.activity_invite_listitem, parent, false);
        return v;
    }

    @Override
    public void bindView(View itemLayoutView, Context context, final Cursor cursor) {
        try {
            CustomFontTextView mDisplayNameTextView = (CustomFontTextView) itemLayoutView.findViewById(R.id.invite_person_name);
//            final CheckBox checkBox = (CheckBox) itemLayoutView.findViewById(R.id.select_checkBox);
            CircularImageView mProfileImageView = (CircularImageView) itemLayoutView.findViewById(R.id.invite_person_image);
            CustomFontButton mFollowButton = (CustomFontButton) itemLayoutView.findViewById(R.id.follow_button);
            CustomFontButton mAddFriendButton = (CustomFontButton) itemLayoutView.findViewById(R.id.add_friend_button);
            final CustomFontButton mInviteButton = (CustomFontButton) itemLayoutView.findViewById(R.id.invite_button);
//            LinearLayout friendRequestLayout = (LinearLayout) itemLayoutView.findViewById(R.id.friend_request_layout);
            LinearLayout buttonLayout = (LinearLayout) itemLayoutView.findViewById(R.id.button_layout);
            mDisplayNameTextView.setText(cursor.getString(cursor.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_DISPLAY_NAME)));
            String url = MyApplication.getAppContext().getString(R.string.s3_url);
            String thumbnail = cursor.getString(cursor.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_THUNMBNAIL));
            final String username = cursor.getString(cursor.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_VOWMEE_USERNAME));
            final boolean isFriendRequestFromViewer = Boolean.getBoolean(cursor.getString(cursor.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_IS_FRIEND_REQUESTED_FROM_VIEWER)));
            final boolean isFriendRequestToViewer = Boolean.getBoolean(cursor.getString(cursor.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_IS_FRIEND_REQUESTED_TO_VIEWER)));
            final boolean isFriend = Boolean.getBoolean(cursor.getString(cursor.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_IS_FRIEND)));
            final boolean isFollowingViewer = Boolean.getBoolean(cursor.getString(cursor.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_IS_FOLLOWING)));
            mDisplayNameTextView.setVisibility(View.VISIBLE);
            final String number = cursor.getString(cursor.getColumnIndex(FeedContract.Entry.COLUMN_CONTACT_PHONENUMBER));
            if (username != null && !username.isEmpty()) {
                buttonLayout.setVisibility(View.VISIBLE);
                mAddFriendButton.setVisibility(View.VISIBLE);
                mFollowButton.setVisibility(View.GONE);
                mInviteButton.setVisibility(View.GONE);
            } else {
                mAddFriendButton.setVisibility(View.GONE);
                mFollowButton.setVisibility(View.GONE);
                mInviteButton.setVisibility(View.VISIBLE);
                buttonLayout.setVisibility(View.VISIBLE);
//                friendRequestLayout.setVisibility(View.GONE);
            }
//            if (isFollowingViewer) {
//                mFollowButton.setText(context.getString(R.string.action_unfollow));
//            } else {
//                mFollowButton.setText(context.getString(R.string.action_follow));
//            }
//            if (isFriendRequestToViewer) {
//                mAddFriendButton.setText(context.getString(R.string.action_request_sent_friend));
//            } else if (isFriendRequestFromViewer) {
//                buttonLayout.setVisibility(View.GONE);
//                friendRequestLayout.setVisibility(View.VISIBLE);
//            } else if (isFriend) {
//                mAddFriendButton.setText(context.getString(R.string.action_remove));
//            } else {
//                mAddFriendButton.setText(context.getString(R.string.action_add_friend));
//            }
//            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    checkBox.setChecked(isChecked);
//                }
//            } );

            mInviteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!number.isEmpty())
                        if(sendInviteMessage(number)){
                         mInviteButton.setText(MyApplication.getAppContext().getString(R.string.invited));
                        }
                }
            });
            if (username != null && !username.isEmpty()) {
                mAddFriendButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent mIntent = new Intent(mContext, TimelineFriendProfile.class);
                        TimelineFriendProfile.setFriendUsername(username);
                        mContext.startActivity(mIntent);
                    }
                });
                mDisplayNameTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent mIntent = new Intent(mContext, TimelineFriendProfile.class);
                        TimelineFriendProfile.setFriendUsername(username);
                        mContext.startActivity(mIntent);
                    }
                });
                mProfileImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent mIntent = new Intent(mContext, TimelineFriendProfile.class);
                        TimelineFriendProfile.setFriendUsername(username);
                        mContext.startActivity(mIntent);
                    }
                });
            }
//            mFollowButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (isFollowingViewer) {
//                        sendFollowingRemoveRequest(username);
//                    } else {
//                        sendFollowingAddRequest(username);
//                    }
//                }
//            });
//            if (thumbnail == null) {
//                url = url + username + "_thumbnail.jpg";
//            } else {
//                url = url + thumbnail;
//            }
//            String profImagePath = thumbnail;
            thumbnail = (thumbnail == null ? (username + AppConsatants.FILE_EXTENSION_JPG) : (thumbnail.isEmpty() ? username + AppConsatants.FILE_EXTENSION_JPG : thumbnail));
            url = url + thumbnail;
            Picasso.with(context).load(url)
                    .error(R.drawable.user)
                    .placeholder(R.drawable.user)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(mProfileImageView);
        }catch(NullPointerException e){
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private boolean sendInviteMessage(String number){
        boolean returnFlag = false;
        if (ContextCompat.checkSelfPermission(PhoneContactInviteActivity.activity, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {

            String message = "";
            SmsManager sms = SmsManager.getDefault();

            message = MyApplication.getAppContext().getResources().getString(R.string.invite_friends_message);
            sms.sendTextMessage(number, null, message, null, null);
            Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.alert_message_invitation), Utils.SHORT_TOAST);
            returnFlag = true;
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(PhoneContactInviteActivity.activity,Manifest.permission.READ_SMS)) {
                showMessageOKCancel(MyApplication.getAppContext().getString(R.string.alert_message_sms_permission),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                returnFlag = false;
            }
        }
        return returnFlag;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(PhoneContactInviteActivity.activity)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
    private void sendFollowingRemoveRequest(final String usernameOfViewer){
        try {
//            if(profile == null)
//                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_REMOVE_FOLLOW);
            final String username = DataStorage.getInstance().getUsername();
            if(username.equals(usernameOfViewer)){
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);
                return;
            }

//            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(usernameOfViewer);
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" + obj.toString());
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
//                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
//                        profile.setFollowedByViewer(false);
//                        searchList.set(position, profile);
                        notifyDataSetChanged();
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

//                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);

        }catch(Exception e){
            e.printStackTrace();
        }
    }
    private void sendFollowingAddRequest(final String usernameOfViewer){
        try {
//            if(profile == null)
//                return;
            String url =ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_ADD_FOLLOW);
            final String username = DataStorage.getInstance().getUsername();
            if(username.equals(usernameOfViewer)){
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);

                return;
            }

//            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(usernameOfViewer);
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" +obj.toString());
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
//                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
//                        profile.setFollowedByViewer(true);
//                        searchList.set(position, profile);
                        notifyDataSetChanged();
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

//                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    private void sendFriendsRemoveRequest(final String usernameOfViewer){
        try {
//            if(profile == null)
//                return;
            String url =ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_REMOVE_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            if(username.equals(usernameOfViewer)){
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);

                return;
            }

//            showProgressBar("Processing your request");
            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(usernameOfViewer);
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" +obj.toString());
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
//                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
//                        profile.setFriendWithViewer(false);
//                        searchList.set(position, profile);
                        notifyDataSetChanged();
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

//                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    private void sendFriendsAddRequest(final String usernameOfViewer){
        try {
//            if(profile == null)
//                return;
            String url =ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_ADD_FRIEND);
            final String username = DataStorage.getInstance().getUsername();
            if(username.equals(usernameOfViewer)){
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_wrong_request), Utils.SHORT_TOAST);

                return;
            }

            FriendRequest obj = new FriendRequest();
            obj.setRequestedPersonUsername(usernameOfViewer);
            obj.setRequestingPersonUsername(username);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Friend req object ", "" +obj.toString());
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
//                        profile.setFriendRequestFromViewer(true);
                        notifyDataSetChanged();
                        ModelManager.getInstance().setOwnProfile(obj);
//                        Toast.makeText(context, "Request sent successfully",
//                                Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

//                    hideProgressBar();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
