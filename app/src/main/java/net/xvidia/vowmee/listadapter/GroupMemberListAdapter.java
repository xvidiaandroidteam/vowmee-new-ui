package net.xvidia.vowmee.listadapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.TimelineFriendProfile;
import net.xvidia.vowmee.TimelineProfile;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.helper.ItemTouchHelperAdapter;
import net.xvidia.vowmee.helper.ItemTouchHelperViewHolder;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class GroupMemberListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemTouchHelperAdapter {
    public static ArrayList<Profile> searchList;
    Context mContext;
    private static Profile selectedProfile;
    private static int selectedPos;

    public class ViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder, View.OnCreateContextMenuListener {

        private ImageView optionsMore;
        private TextView mDisplayNameTextView;
        private TextView mMemberRole;
        private CircularImageView mProfileImageView;

        public ViewHolder(View v) {
            super(v);
            mDisplayNameTextView = (TextView) v.findViewById(R.id.friend_name);
            mMemberRole = (TextView) v.findViewById(R.id.member_role);
            mProfileImageView = (CircularImageView) v.findViewById(R.id.friend_image);
            optionsMore = (ImageView) v.findViewById(R.id.image_options);
            v.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

            if (selectedProfile != null) {
                if (ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_OWNER)) {
                    if (selectedProfile.getGroupRole().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MEMBER)) {
                        MenuItem admin = menu.add(100, 101, 0, mContext.getString(R.string.group_admin));
                        admin.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                sendAssignRequest(selectedPos);
                                return true;
                            }
                        });
                    } else if (selectedProfile.getGroupRole().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MODERATOR)) {
                        MenuItem admin = menu.add(100, 101, 0, mContext.getString(R.string.group_remove_admin));
                        admin.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                sendUnassignRequest(selectedPos);
                                return true;
                            }
                        });
                    }

                    MenuItem delete = menu.add(100, 101, 0, mContext.getString(R.string.action_group_member_delete));
                    delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            sendDeleteMemberRequest(selectedPos);
                            return true;
                        }
                    });
                } else if (ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MODERATOR)) {
                    MenuItem delete = menu.add(100, 101, 0, mContext.getString(R.string.action_group_member_delete));
                    delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            sendDeleteMemberRequest(selectedPos);
                            return true;
                        }
                    });
                }
            }
        }

        @Override
        public void onItemSelected() {

        }

        @Override
        public void onItemClear() {

        }
    }

    public GroupMemberListAdapter(Context context, final ArrayList<Profile> friendList) {

        this.mContext = context;
        this.searchList = friendList;
//        selectedItems = new SparseBooleanArray(friendList.size());
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_group_member_listitem, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        try {
            final Profile profile = searchList.get(position);
            final ViewHolder holder = (ViewHolder) viewHolder;
            holder.mDisplayNameTextView.setText(profile.getDisplayName());

            holder.mProfileImageView.setVisibility(View.VISIBLE);
            holder.mDisplayNameTextView.setVisibility(View.VISIBLE);
            holder.mMemberRole.setVisibility(View.GONE);
            holder.mProfileImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (profile.getUsername().equals(DataStorage.getInstance().getUsername())) {
                        Intent intent = new Intent(mContext, TimelineProfile.class);
                        mContext.startActivity(intent);
                    } else {
                        TimelineFriendProfile.setFriendUsername(profile.getUsername());
                        Intent mIntent = new Intent(mContext, TimelineFriendProfile.class);
                        mContext.startActivity(mIntent);
                    }
                }
            });
            holder.mDisplayNameTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (profile.getUsername().equals(DataStorage.getInstance().getUsername())) {
                        Intent intent = new Intent(mContext, TimelineProfile.class);
                        mContext.startActivity(intent);
                    } else {
                        TimelineFriendProfile.setFriendUsername(profile.getUsername());
                        Intent mIntent = new Intent(mContext, TimelineFriendProfile.class);
                        mContext.startActivity(mIntent);
                    }
                }
            });
            if (setOptionsVisibility()) {
                holder.optionsMore.setVisibility(View.VISIBLE);
            } else {
                holder.optionsMore.setVisibility(View.GONE);
            }
            if (profile.getGroupRole().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MODERATOR)) {
                holder.mMemberRole.setVisibility(View.VISIBLE);
                holder.mMemberRole.setText(mContext.getResources().getString(R.string.group_admin));
            } else if (profile.getGroupRole().equalsIgnoreCase(AppConsatants.GROUP_ROLE_OWNER)) {
                holder.mMemberRole.setVisibility(View.VISIBLE);
                holder.optionsMore.setVisibility(View.GONE);
                holder.mMemberRole.setText(mContext.getResources().getString(R.string.group_owner));
            } else if (profile.getGroupRole().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MEMBER)) {
                holder.mMemberRole.setVisibility(View.VISIBLE);
                holder.mMemberRole.setText(mContext.getResources().getString(R.string.group_memberr));
//                holder.mMemberRole.setTextColor(mContext.getResources().getColor(R.color.color1_gradient));
            } else if (profile.getGroupRole().equalsIgnoreCase(AppConsatants.GROUP_ROLE_FOLLOWER)) {
                holder.mMemberRole.setVisibility(View.VISIBLE);
                holder.mMemberRole.setText(mContext.getResources().getString(R.string.group_follower));
//                holder.mMemberRole.setTextColor(mContext.getResources().getColor(R.color.grey_light));
            }
            holder.optionsMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPos = position;
                    selectedProfile = profile;
                    v.showContextMenu();
                }
            });

//            Utils.getInstance().underlineTextView(profile.getDisplayName(), holder.mDisplayNameTextView);
            String profImagePath = profile.getThumbNail();
            profImagePath = (profImagePath == null ? (profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG : profile.getThumbNail()));
            holder.mProfileImageView.setTag(profImagePath);//tag of imageView == path to image
//            new LoadImage(holder.mProfileImageView).execute();
            downloadContentFromS3Bucket(holder.mProfileImageView, profImagePath);
        } catch (NullPointerException e) {

        } catch (Exception e) {

        }

    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        return false;
    }

    private boolean setOptionsVisibility() {
        boolean visible = false;

        if (ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MODERATOR)
                || ModelManager.getInstance().getGroupProfile().getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_OWNER)) {
            visible = true;
        }

        return visible;
    }

    @Override
    public void onItemDismiss(int position) {
        if (searchList.size() > position) {
            if (searchList.size() > 1) {
                searchList.remove(position);
//            selectedItems.put(position, true);
                notifyItemRemoved(position);
            }
        } else {
            if (searchList.size() == 1) {
                searchList.remove(0);
            }
        }
        notifyDataSetChanged();
    }

    private void setRole(int position, boolean setRole) {
        if (searchList.size() > position) {
            Profile obj = searchList.get(position);
            if (setRole) {
                obj.setGroupRole(AppConsatants.GROUP_ROLE_MODERATOR);
            } else {
                obj.setGroupRole(AppConsatants.GROUP_ROLE_MEMBER);
            }
            searchList.remove(position);
            searchList.add(position, obj);
        } else {
            if (searchList.size() == 1) {
                searchList.remove(0);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        holder.setIsRecyclable(true);

    }

    @Override
    public int getItemCount() {
        if (searchList == null)
            return 0;
        return searchList.size();
    }

    //    private void selectOptions(View view,final int position) {
//        final PopupMenu popupMenu = new PopupMenu(mContext, view);
//        final Menu menu = popupMenu.getMenu();
//
//        popupMenu.getMenuInflater().inflate(R.menu.menu_edit_member, menu);
//
//        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                if (item.getItemId() == R.id.action_admin_group) {
//
//                } else if (item.getItemId() == R.id.action_remove_member) {
//                   sendDeleteMemberRequest(position);
//                    popupMenu.dismiss();
//                }
//                return false;
//            }
//        });
//        popupMenu.show();
//
//    }
    private void downloadContentFromS3Bucket(ImageView imageView, String filePath){
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null) {
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                        imageView.setImageBitmap(image1);
//                        Animation myFadeInAnimation = AnimationUtils.loadAnimation(mContext, R.anim.fadein);
//                        imageView.startAnimation(myFadeInAnimation);
                    }else {
                        imageView.setImageResource(R.drawable.user);
                    }
                } else {
                    imageView.setImageResource(R.drawable.user);
                }
            }
//            if(!disableLoad)
            download(imageView, filePath);
        }
    }

    private void  download(final ImageView imageView, final String filePath){
        UserFileManager userFileManager = Utils.getInstance().getUserFileManager();
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                }

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }
    private void sendDeleteMemberRequest(final int position) {
        try {
            Profile obj = searchList.get(position);
            final String userUuid = DataStorage.getInstance().getUserUUID();
            final String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
            String memberUuid = obj.getUuid();
            final String url = ServiceURLManager.getInstance().getGroupMemberDeleteUrl(groupUuid, memberUuid, userUuid);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    onItemDismiss(position);
                    String url1 = ServiceURLManager.getInstance().getGroupMemberListUrl(groupUuid,userUuid,"0","0");
                    VolleySingleton.getInstance(mContext).refresh(url1,true);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_general), Utils.SHORT_TOAST);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void sendAssignRequest(final int position) {
        try {
            Profile obj = searchList.get(position);
            String userUuid = DataStorage.getInstance().getUserUUID();
            String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
            String memberUuid = obj.getUuid();
            String url = ServiceURLManager.getInstance().getGroupMemberAssignRoleUrl(groupUuid, memberUuid, userUuid);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    setRole(position, true);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_general), Utils.SHORT_TOAST);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void sendUnassignRequest(final int position) {
        try {
            Profile obj = searchList.get(position);
            String userUuid = DataStorage.getInstance().getUserUUID();
            String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
            String memberUuid = obj.getUuid();
            String url = ServiceURLManager.getInstance().getGroupMemberUnassignRoleUrl(groupUuid, memberUuid, userUuid);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    setRole(position, false);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_general), Utils.SHORT_TOAST);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }
}