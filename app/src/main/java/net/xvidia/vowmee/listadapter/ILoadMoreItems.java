package net.xvidia.vowmee.listadapter;

/**
 * Created by Ravi_office on 27-Jun-16.
 */
public interface ILoadMoreItems {
    public void loadMoreItems(boolean load);
}
