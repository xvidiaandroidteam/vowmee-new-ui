package net.xvidia.vowmee.listadapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;

import net.xvidia.vowmee.R;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.network.model.Profile;

import java.io.File;
import java.util.ArrayList;

public class GroupAddMemberListSettingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private ArrayList<Profile> searchList;
    Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder  {

        private CheckBox checkBox;
        private TextView mDisplayNameTextView;
        private CircularImageView mProfileImageView;

        public ViewHolder(View v) {
            super(v);
            mDisplayNameTextView = (TextView) v.findViewById(R.id.friend_name);
            mProfileImageView = (CircularImageView) v.findViewById(R.id.friend_image);
            checkBox = (CheckBox) v.findViewById(R.id.song_checkBox);
        }


    }

    public GroupAddMemberListSettingAdapter(Context context, final ArrayList<Profile> friendList) {

        this.mContext = context;
        this.searchList = friendList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_group_addmember_listitem, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }



    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        try {
            final Profile profile = searchList.get(position );
            final ViewHolder holder = (ViewHolder) viewHolder;
            holder.mDisplayNameTextView.setText(profile.getDisplayName());

            holder.mProfileImageView.setVisibility(View.VISIBLE);
            holder.mDisplayNameTextView.setVisibility(View.VISIBLE);

//            Utils.getInstance().underlineTextView(profile.getDisplayName(), holder.mDisplayNameTextView);
            String profImagePath = profile.getThumbNail();
            profImagePath = (profImagePath == null ? (profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? profile.getUsername() + AppConsatants.FILE_EXTENSION_JPG : profile.getThumbNail()));
            holder.mProfileImageView.setTag(profImagePath);//tag of imageView == path to image
//            new LoadImage(holder.mProfileImageView).execute();
            downloadContentFromS3Bucket(holder.mProfileImageView, profImagePath);
            holder.checkBox.setVisibility(View.GONE);
//            notifyItemChanged(position);
        } catch (NullPointerException e) {

        } catch (Exception e) {

        }

    }
    private void downloadContentFromS3Bucket(ImageView imageView, String filePath){
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                } else {
                    imageView.setImageResource(R.drawable.user);
                }
            }
//            if(!disableLoad)
            download(imageView, filePath);
        }
    }

    private void  download(final ImageView imageView, final String filePath){
        UserFileManager userFileManager = Utils.getInstance().getUserFileManager();
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                }

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        holder.setIsRecyclable(true);

    }

    @Override
    public int getItemCount() {
        if (searchList == null)
            return 0;
        return searchList.size();
    }

}