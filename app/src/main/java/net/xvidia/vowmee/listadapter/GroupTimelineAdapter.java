package net.xvidia.vowmee.listadapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.mobile.content.ContentDownloadPolicy;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import net.xvidia.vowmee.CommentActivity;
import net.xvidia.vowmee.CropGroupImageActivity;
import net.xvidia.vowmee.GroupAddMemberActivity;
import net.xvidia.vowmee.GroupMemberActivity;
import net.xvidia.vowmee.GroupProfileActivity;
import net.xvidia.vowmee.GroupProfileSettingActivity;
import net.xvidia.vowmee.GroupTimelineTabs;
import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.TimelineFriendProfile;
import net.xvidia.vowmee.TimelineMomentDetail;
import net.xvidia.vowmee.TimelineProfile;
import net.xvidia.vowmee.UploadActivity;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.helper.CustomFontEditTextView;
import net.xvidia.vowmee.helper.CustomFontTextView;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.UploadffmpegService;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.GroupProfile;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.network.model.MomentShare;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;
import net.xvidia.vowmee.videoplayer.VideoPlayer;
import net.xvidia.vowmee.videoplayer.VideoPlayerController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static net.xvidia.vowmee.MyApplication.getAppContext;


public class GroupTimelineAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ILoadMoreItems iLoadMore;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private final int VIEW_TYPE_LOADING = 2;
    private ProgressDialog progressDialog;
    private boolean mWithHeader;
//    private static boolean mOwnProfile;
    private boolean groupMember;
    private boolean follower;
    private static Context context;
    private Activity activity;
    private List<Moment> momentList;
    static boolean moreFlag;
    public static boolean refreshimeline = false;
    public VideoPlayerController videoPlayerController;
    private int screenHeightPixels,screenWidthPixels;
    private int profileImageDimensPixelsDefault;

    public List<Moment> getMomentList() {
        return momentList;
    }

    public void setMomentList(List<Moment> momentList) {
        this.momentList = momentList;
    }

    private UserFileManager userFileManager;
    public GroupTimelineAdapter(Context context, Activity activity, final List<Moment> moments) {
        this.activity = activity;
        this.context = context;
        this.momentList = moments;
        videoPlayerController = new VideoPlayerController(context);
        mWithHeader = false;
//        mOwnProfile = ownProfile;
    }

    public GroupTimelineAdapter(boolean withHeader, Context context, Activity activity, final List<Moment> moments) {
        this.activity = activity;
        this.context = context;
        this.momentList = moments;
        videoPlayerController = new VideoPlayerController(context);
        mWithHeader = withHeader;
//        mOwnProfile = ownProfile;
        userFileManager = Utils.getInstance().getUserFileManager();
        Configuration configuration = MyApplication.getAppContext().getResources().getConfiguration();
        int screenHeightDp = configuration.screenHeightDp;
        int screenWidthDp = configuration.screenWidthDp;//The current width of the available screen space, in dp units, corresponding to screen width resource qualifier.
        screenHeightPixels = Utils.getInstance().convertDpToPixel(screenHeightDp, context);
        screenWidthPixels= Utils.getInstance().convertDpToPixel(screenWidthDp, context);
        screenHeightPixels = (screenHeightPixels / 9) * 4;
        profileImageDimensPixelsDefault =Utils.getInstance().convertDimenToInt(R.dimen.profile_image_size_list_item);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_timeline_header_item, parent, false);
            return new HeaderViewHolder(v);
        } else if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.timeline_moment_item, parent, false);
            ItemViewHolder viewHolder = new ItemViewHolder(v);
            RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            viewHolder.layout.setLayoutParams(rel_btn);

            return viewHolder;
        }else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_more_item, parent, false);
            return new LoadingViewHolder(view);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if(getItemViewType(position) == TYPE_HEADER) {
            try {
                final HeaderViewHolder mHeaderViewHolder = (HeaderViewHolder) holder;
                final GroupProfile groupProfile = ModelManager.getInstance().getGroupProfile();


                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, screenHeightPixels);
                mHeaderViewHolder.mHeaderLayout.setLayoutParams(params);
                mHeaderViewHolder.mHeaderLayout.setMinimumHeight(screenHeightPixels);
                if (groupProfile != null) {
                    if (groupProfile.getRoleWithViewer() != null &&(groupProfile.getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_OWNER)
                            ||  groupProfile.getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MODERATOR))) {
                        groupMember = true;
                        mHeaderViewHolder.addMemberButton.setVisibility(View.VISIBLE);
                        mHeaderViewHolder.addMemberButton.setText(context.getResources().getString(R.string.add_member));

                    }else if (groupProfile.getRoleWithViewer() != null && groupProfile.getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MEMBER)) {
                        groupMember = true;
                        mHeaderViewHolder.addMemberButton.setVisibility(View.GONE);
//                        mHeaderViewHolder.addMemberButton.setText(context.getResources().getString(R.string.add_member));

                    }else if (groupProfile.getRoleWithViewer() != null && groupProfile.getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_FOLLOWER)) {
                        groupMember = false;
                        follower=true;
                        mHeaderViewHolder.addMemberButton.setVisibility(View.VISIBLE);
                        mHeaderViewHolder.addMemberButton.setText(context.getResources().getString(R.string.action_unfollow));
//                        mHeaderViewHolder.addMemberButton.setText(context.getResources().getString(R.string.add_member));

                    } else {//(groupProfile.getRoleWithViewer() != null && !groupProfile.getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MEMBER))
                        groupMember = false;
                        follower=false;
                        mHeaderViewHolder.addMemberButton.setVisibility(View.VISIBLE);
                        mHeaderViewHolder.addMemberButton.setText(context.getResources().getString(R.string.follow_group));
                    }
                    if (groupProfile.getProfile() != null && groupProfile.getProfile().equalsIgnoreCase(AppConsatants.PROFILE_PUBLIC)) {
                        mHeaderViewHolder.mPrivatePublicScope.setVisibility(View.VISIBLE);
                        mHeaderViewHolder.mPrivatePublicScope.setText(context.getResources().getString(R.string.prompt_public));

                    } else if (groupProfile.getProfile() != null && groupProfile.getProfile().equalsIgnoreCase(AppConsatants.PROFILE_PRIVATE)
                            ||groupProfile.getProfile().equalsIgnoreCase(AppConsatants.PROFILE_PRIVATE_SEARCHABLE)) {
                        mHeaderViewHolder.mPrivatePublicScope.setVisibility(View.VISIBLE);
                        mHeaderViewHolder.mPrivatePublicScope.setText(context.getResources().getString(R.string.prompt_private));
                    }
                    mHeaderViewHolder.mMemberCount.setText(context.getResources().getString(R.string.member_count, groupProfile.getNoOfMembers()));

                    String profImagePath = groupProfile.getProfileImage();
                    if (profImagePath != null && !profImagePath.isEmpty())
                        downloadContentFromS3Bucket(mHeaderViewHolder, profImagePath);
                    mHeaderViewHolder.mProfileNameTextView.setText(groupProfile.getName());
                    if (groupMember) {
                        mHeaderViewHolder.mProfileNameTextView.setEnabled(true);
                        mHeaderViewHolder.mJoinTextView.setTextColor(context.getResources().getColor(R.color.appcolor));
                        mHeaderViewHolder.mJoinTextView.setText(context.getResources().getString(R.string.group_joined));
                        mHeaderViewHolder.joinImageView.setImageResource(R.drawable.join_activate);
                        mHeaderViewHolder.mProfileImageEdit.setVisibility(View.VISIBLE);
                        mHeaderViewHolder.mProfileNameTextView.setEnabled(true);
                    } else {
                        mHeaderViewHolder.mProfileNameTextView.setEnabled(false);
                        mHeaderViewHolder.mJoinTextView.setText(context.getResources().getString(R.string.group_join));
                        mHeaderViewHolder.joinImageView.setImageResource(R.drawable.join_deactivate);
                        mHeaderViewHolder.mJoinTextView.setTextColor(context.getResources().getColor(R.color.grey_text));
                        mHeaderViewHolder.mProfileNameTextView.setEnabled(false);
                        mHeaderViewHolder.mProfileImageEdit.setVisibility(View.GONE);
                        if(groupProfile.isMembershipRequested())
                            mHeaderViewHolder.mJoinTextView.setText(context.getResources().getString(R.string.action_request_sent_friend));

                    }
                    mHeaderViewHolder.mProfileNameTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(mHeaderViewHolder.mProfileNameTextView.isEnabled()){
                                GroupProfileActivity.mMessageTextView.setVisibility(View.GONE);
                            }
                        }
                    });
                    mHeaderViewHolder.mProfileNameTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if (event != null && event.getAction() != KeyEvent.ACTION_DOWN) {
                                return false;
                            } else if (actionId == EditorInfo.IME_ACTION_DONE
                                    || event == null
                                    || event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                           sendUpdateRequest(mHeaderViewHolder.mProfileNameTextView.getText().toString());
                            if(momentList.size()== 1)
                                GroupProfileActivity.mMessageTextView.setVisibility(View.VISIBLE);
                            return true;
                            }else {
                                mHeaderViewHolder.mProfileNameTextView.clearFocus();
                            }
                            return false;
                        }
                    });
                    mHeaderViewHolder.mProfileImageEdit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (groupMember) {
                                CropGroupImageActivity.groupImage = groupProfile.getProfileImage();
                                Intent mIntent = new Intent(context, CropGroupImageActivity.class);
                                Bundle mBundle = new Bundle();
                                mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, false);
                                mIntent.putExtras(mBundle);
                                context.startActivity(mIntent);
                            }
                        }
                    });

                    mHeaderViewHolder.mJoinLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (groupMember) {
//                                if (DataStorage.getInstance().getUserUUID().equalsIgnoreCase(ModelManager.getInstance().getGroupProfile().getOwnerUuid())) {
//                                    showExitGroupMessageOwner(context.getResources().getString(R.string.alert_message_owner_exit), new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            getGroupMemberList();
//                                        }
//                                    });
//                                } else {
//                                    showExitGroupMessageOwner(context.getResources().getString(R.string.alert_message_exit), new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            sendGroupExitRequest();
//                                        }
//                                    });
//                                }
                            }else{
                                sendJoinRequest(groupProfile,mHeaderViewHolder);
                            }
                        }
                    });
                    mHeaderViewHolder.mMembersLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent mIntent = new Intent(context, GroupMemberActivity.class);
                            Bundle mBundle = new Bundle();
                            mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, groupMember);
                            mIntent.putExtras(mBundle);
                            context.startActivity(mIntent);
                        }
                    });
                    mHeaderViewHolder.moreInfoLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent mIntent = new Intent(context, GroupProfileSettingActivity.class);
                            Bundle mBundle = new Bundle();
                            mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, groupMember);
                            mIntent.putExtras(mBundle);
                            context.startActivity(mIntent);
                        }
                    });
                    int paddingLeftRight = Utils.getInstance().convertDimenToInt(R.dimen.activity_margin_10);
                    int paddingTopBottom = Utils.getInstance().convertDimenToInt(R.dimen.button_text_padding);
                    mHeaderViewHolder.addMemberButton.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight,paddingTopBottom);

                    mHeaderViewHolder.addMemberButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(groupMember) {
                                if (groupProfile.getRoleWithViewer() != null) {
                                    if (groupProfile.getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_OWNER)
                                            || groupProfile.getRoleWithViewer().equalsIgnoreCase(AppConsatants.GROUP_ROLE_MODERATOR)) {
                                        Intent mIntent = new Intent(context, GroupAddMemberActivity.class);
                                        Bundle mBundle = new Bundle();
                                        mBundle.putBoolean(AppConsatants.PROFILE_REGISTER, false);
                                        mIntent.putExtras(mBundle);
                                        context.startActivity(mIntent);
                                    }
                                }
                            }else if(follower){
                                sendFollowRequest(groupProfile,follower,mHeaderViewHolder);
                            }else{
                                sendFollowRequest(groupProfile,follower,mHeaderViewHolder);
                            }
                        }
                    });


                }
            } catch (NullPointerException e) {

            } catch (ArrayIndexOutOfBoundsException e) {

            } catch (Exception e) {

            }
        }else if(getItemViewType(position)==TYPE_ITEM) {
            try {
                final int newPosition = position;
//                if(mWithHeader)
//                    newPosition = position-1;
                if( momentList.size()>newPosition) {
                    final Moment video = momentList.get(newPosition);
                    if (video == null) {
                        momentList.remove(newPosition);
                        notifyDataSetChanged();
                        return;
                    }
                    final ItemViewHolder mItemViewHolder = (ItemViewHolder) holder;

                    if(video.getGroupUuid()!=null && !video.getGroupUuid().isEmpty()){
                        mItemViewHolder.mSharedLayout.setVisibility(View.VISIBLE);
                        mItemViewHolder.mShareProfileName.setText(video.getGroupName());
                        mItemViewHolder.mSharedLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                geteGroupInfoRequest(video);
                            }
                        });
                    }else{
                        mItemViewHolder.mSharedLayout.setVisibility(View.GONE);
                    }
                    mItemViewHolder.mDisplayNameTextView.setText(video.getOwnerDisplayName());
                    mItemViewHolder.mCaptionTextView.setText(video.getCaption());
                    mItemViewHolder.mDescriptionTextView.setText(video.getDescription());
                    mItemViewHolder.mCommentsCountTextView.setText(MyApplication.getAppContext().getString(R.string.comment_count,video.getCommentCount()));
                    mItemViewHolder.mShareCountTextView.setText(MyApplication.getAppContext().getString(R.string.share_count,video.getShareCount()));
                    mItemViewHolder.mLikesTextView.setText(MyApplication.getAppContext().getString(R.string.like_count,video.getLikes()));
                    mItemViewHolder.mDaysLeftTextView.setText(Utils.getInstance().getDateDiffString(video.getDate()));
                    video.setIndexPosition("" + newPosition);
                    momentList.set(newPosition, video);
                    String profImagePath = video.getOwnerThumbNail();
                    profImagePath = (profImagePath == null ? (video.getOwner() + AppConsatants.FILE_EXTENSION_JPG) : (profImagePath.isEmpty() ? video.getOwner() + AppConsatants.FILE_EXTENSION_JPG : video.getOwnerThumbNail()));
                    mItemViewHolder.mProfileImageView.setTag(profImagePath);//tag of imageView == path to image
//                    new LoadImage(mItemViewHolder.mProfileImageView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    downloadContentFromS3Bucket(mItemViewHolder.mProfileImageView, profImagePath);
                    mItemViewHolder.mProfileImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            if (video.getOwner().equals(DataStorage.getInstance().getUsername())) {
                                Intent intent = new Intent(context, TimelineProfile.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);
//                        ((AppCompatActivity)context).finish();
                            } else {
                                Intent mIntent = new Intent(context, TimelineFriendProfile.class);
                                Bundle mBundle = new Bundle();
                                mBundle.putString(AppConsatants.PERSON_FRIEND, video.getOwner());
                                mIntent.putExtras(mBundle);
                                context.startActivity(mIntent);
                            }
                        }
                    });
                    mItemViewHolder.mDisplayNameTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (video.getOwner().equals(DataStorage.getInstance().getUsername())) {
                                Intent intent = new Intent(context, TimelineProfile.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);
//                        ((AppCompatActivity)context).finish();
                            } else {
                                Intent mIntent = new Intent(context, TimelineFriendProfile.class);
                                Bundle mBundle = new Bundle();
                                mBundle.putString(AppConsatants.PERSON_FRIEND, video.getOwner());
                                mIntent.putExtras(mBundle);
                                context.startActivity(mIntent);
                            }
                        }
                    });
                    final VideoPlayer mVideoPlayer = new VideoPlayer(context, Utils.getInstance().getLocalContentPath(video.getLink()));
                    final ImageView mImageView = new ImageView(context);//,Utils.getInstance().getLocalContentPath(video.getThumbNail()));
                    mItemViewHolder.videoPlayer.setOrientation(LinearLayout.VERTICAL);
                    mItemViewHolder.videoPlayer.setGravity(Gravity.CENTER_HORIZONTAL);
//                    Configuration configuration = context.getResources().getConfiguration();
//                    int screenHeightDp = configuration.screenWidthDp; //The current width of the available screen space, in dp units, corresponding to screen width resource qualifier.
////                    int smallestScreenWidthDp = configuration.smallestScreenWidthDp;
//                    int screenHeightPixels = Utils.getInstance().convertDpToPixel(screenHeightDp, context);
                    int screenHeightPixelsDefault = (screenHeightPixels / 3+ screenHeightPixels / 15);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                    LinearLayout.LayoutParams paramVideos = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                    params.width = screenHeightPixels;
//                    params.height = screenHeightPixelsDefault;
//                    paramVideos.gravity = Gravity.CENTER;
                    mItemViewHolder.videoPlayer.setLayoutParams(params);
                    mItemViewHolder.videoPlayer.setMinimumHeight(screenHeightPixelsDefault);
                    mImageView.setMinimumWidth(screenHeightPixels);
                    mImageView.setMinimumHeight(screenHeightPixelsDefault);
                    mImageView.setScaleType(ImageView.ScaleType.FIT_XY);
//                    Bitmap originalThumbnail = Utils.getInstance().decodeScaledBitmapFromSdCard(Utils.getInstance().getLocalContentPath(video.getThumbNail()));
                    Bitmap originalThumbnail = FileCache.getInstance().getBitmapFromMemCache(video.getThumbNail());
                    if(originalThumbnail==null) {
                        originalThumbnail = Utils.getInstance().decodeScaledBitmapFromSdCard(Utils.getInstance().getLocalContentPath(video.getThumbNail()));
                        FileCache.getInstance().addBitmapToMemoryCache(video.getThumbNail(),originalThumbnail);
                    }
                    if(originalThumbnail!=null)
                        mItemViewHolder.videoPlayer.setBackground(new BitmapDrawable(MyApplication.getAppContext().getResources(), originalThumbnail));
                    else
                        mItemViewHolder.videoPlayer.setBackgroundColor(getAppContext().getResources().getColor(R.color.grey_light));

//                    mItemViewHolder.videoPlayer.setBackgroundColor(getAppContext().getResources().getColor(R.color.grey_light));
                    mImageView.setLayoutParams(params);
                    mVideoPlayer.setLayoutParams(params);
                    mVideoPlayer.requestLayout();
                    mImageView.requestLayout();
                    mImageView.setBackgroundColor(getAppContext().getResources().getColor(R.color.grey_light));

                    mItemViewHolder.videoPlayer.addView(mVideoPlayer);
                    mItemViewHolder.videoPlayer.addView(mImageView);
                    int pos = 0;
                    if(mWithHeader)
                        pos=1;
                    if (newPosition == pos && getVisibility(video.getLink())) {
                        mImageView.setVisibility(View.GONE);
                        mVideoPlayer.setVisibility(View.VISIBLE);
                        videoPlayerController.handlePlayBack(video,false);
                        mItemViewHolder.progressBar.setVisibility(View.GONE);
                    } else if (getVisibility(video.getThumbNail())) {
                        setImageViewDimension(mImageView,originalThumbnail);
                        mImageView.setVisibility(View.VISIBLE);
                        mVideoPlayer.setVisibility(View.GONE);
                        mImageView.setBackground(new BitmapDrawable(MyApplication.getAppContext().getResources(), originalThumbnail));
                        mItemViewHolder.progressBar.setVisibility(View.GONE);
                    }else{
                        mImageView.setVisibility(View.VISIBLE);
                        mVideoPlayer.setVisibility(View.GONE);
                        mImageView.setBackgroundColor(getAppContext().getResources().getColor(R.color.grey_light));
                        mItemViewHolder.progressBar.setVisibility(View.VISIBLE);
                    }
                    videoPlayerController.loadVideoWithThumbnail(video, mVideoPlayer, mItemViewHolder.progressBar, mImageView, mItemViewHolder.videoPlayer);
                    mImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            refreshimeline = true;
                            Utils.getInstance().setMomentObj(video);
                            Intent mIntent = new Intent(context, TimelineMomentDetail.class);
                            Bundle mBundle = new Bundle();
                            mBundle.putString(AppConsatants.UUID, video.getUuid());
                            mBundle.putBoolean(AppConsatants.UPLOAD_FILE_PATH, false);
                            mIntent.putExtras(mBundle);
                            context.startActivity(mIntent);
                        }
                    });
                    mItemViewHolder.videoPlayer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            refreshimeline = true;
                            Utils.getInstance().setMomentObj(video);
                            Intent mIntent = new Intent(context, TimelineMomentDetail.class);
                            Bundle mBundle = new Bundle();
                            mBundle.putString(AppConsatants.UUID, video.getUuid());
                            mBundle.putBoolean(AppConsatants.UPLOAD_FILE_PATH, false);
                            mIntent.putExtras(mBundle);
                            context.startActivity(mIntent);
                        }
                    });
                    mItemViewHolder.mLikesImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            updateLike(newPosition, video);
                            long count = video.getLikes();//Utils.getInstance().getIntegerValueOfString(video.getLikes());

                            if (!video.getIsLikedBy()) {
                                count = count + 1;
                                video.setLikes(count);
                                mItemViewHolder.mLikesTextView.setText(MyApplication.getAppContext().getString(R.string.like_count,count));
//                        mItemViewHolder.mLikesImageView.setCompoundDrawables(context.getDrawable(R.drawable.heart_selected), null, null, null);
                                mItemViewHolder.mLikesImageView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.heart_selected, 0, 0, 0);
                                video.setIsLikedBy(true);
                                momentList.set(newPosition, video);
                            } else {
                                count = count - 1;
                                if (count < 0)
                                    count = 0;

                                video.setLikes(count);
                                mItemViewHolder.mLikesTextView.setText(MyApplication.getAppContext().getString(R.string.like_count,count));
                                mItemViewHolder.mLikesImageView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.heart, 0, 0, 0);

                                video.setIsLikedBy(false);
                                momentList.set(newPosition, video);
                            }
                        }
                    });
                    mItemViewHolder.mCommentsImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utils.getInstance().setMomentObj(video);
                            Intent mIntent = new Intent(context, CommentActivity.class);
                            Bundle mBundle = new Bundle();
                            mBundle.putString(AppConsatants.UUID, video.getUuid());
                            mBundle.putBoolean(AppConsatants.NOTIFICATION_NEXT_INTENT, false);
                            mIntent.putExtras(mBundle);
                            context.startActivity(mIntent);
                        }
                    });
                    mItemViewHolder.mShareImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            moreFlag= false;
                            Utils.getInstance().setMomentObj(video);
                            v.showContextMenu();
                        }
                    });
                    mItemViewHolder.mMoreImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            moreFlag= true;
                            Utils.getInstance().setMomentObj(video);
                            v.showContextMenu();
                        }
                    });
                    if (video.getIsLikedBy()) {
                        mItemViewHolder.mLikesImageView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.heart_selected, 0, 0, 0);
                    } else {
                        mItemViewHolder.mLikesImageView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.heart, 0, 0, 0);
                    }
                    if (video.getCaption() == null) {
                        mItemViewHolder.mCaptionLayout.setVisibility(View.GONE);
                    } else if (video.getCaption().trim().isEmpty()) {
                        mItemViewHolder.mCaptionLayout.setVisibility(View.GONE);
                    } else {
                        mItemViewHolder.mCaptionLayout.setVisibility(View.VISIBLE);
                    }

                    if (video.getDescription() == null) {
                        mItemViewHolder.mDescriptionLayout.setVisibility(View.GONE);
                    } else if (video.getDescription().trim().isEmpty()) {
                        mItemViewHolder.mDescriptionLayout.setVisibility(View.GONE);
                    } else {
                        mItemViewHolder.mDescriptionLayout.setVisibility(View.VISIBLE);
                    }
                }
            } catch (NullPointerException e) {

            }catch (ArrayIndexOutOfBoundsException e) {

            } catch (Exception e) {

            }
        } else if (getItemViewType(position) == VIEW_TYPE_LOADING) {
            final LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.loadingMore.setText(MyApplication.getAppContext().getResources().getString(R.string.action_load_more));

            loadingViewHolder.loadingMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadingViewHolder.loadingMore.setText(MyApplication.getAppContext().getResources().getString(R.string.action_loading));

                    iLoadMore.loadMoreItems(true);
                }
            });
        }
    }
    private boolean getVisibility(String fileName){
        boolean visible = false;
        if(fileName!=null && !fileName.isEmpty()) {
            String filePath = Utils.getInstance().getLocalContentPath(fileName);
            File vidFile = new File(filePath);
            if (vidFile != null && vidFile.exists()) {
                visible = true;
            }
        }
        return visible;
    }

    // need to override this method
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        return momentList.get(position) == null ? VIEW_TYPE_LOADING : TYPE_ITEM;
    }

    private void updateLike(final int position,final Moment video){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                updateLikeStatus(position, video);
            }
        },250);
    }

    private void sendReportMomentRequest(final int position, final Moment momentObj) {
        try {
//            showProgressBar("Processing your request");
            String userUuid = DataStorage.getInstance().getUserUUID();
            String momentUuid = momentObj.getUuid();
            String url = ServiceURLManager.getInstance().getMomentReportUrl(momentUuid, userUuid);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Moment obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Moment.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        momentObj.setReportedAsAbuse(true);
                        momentList.set(position, momentObj);
                        final String username = DataStorage.getInstance().getUsername();
                        String url = ServiceURLManager.getInstance().getMomentFeedsListUrl(IAPIConstants.API_KEY_GET_MOMENT_FEEDS_TIMELINE_LIST, username, username, AppConsatants.FALSE, "0", "0");
                        VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(url, true);
                        notifyItemChanged(position);
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_report), Utils.SHORT_TOAST);

                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null && error.networkResponse != null) {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);
                    }else {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_no_internet), Utils.SHORT_TOAST);

                    }

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void sendBlockUserRequest(final Moment momentObj,final boolean block,final int position) {
        try {
//            showProgressBar("Processing your request");
            String userUuid = DataStorage.getInstance().getUserUUID();
            String blockinguuid = momentObj.getOwnerUuid();
            String url = ServiceURLManager.getInstance().getBlockUserUrl(userUuid, blockinguuid);
            if(!block)
                url = ServiceURLManager.getInstance().getUnBlockUserUrl(userUuid, blockinguuid);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        momentObj.setOwnerBlocked(block);
                        momentList.set(position, momentObj);
                        notifyItemChanged(position);
                    }
                    notifyDataSetChanged();
                    if(block) {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_blocked), Utils.SHORT_TOAST);
                    }else{
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_unblocked), Utils.SHORT_TOAST);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    if (error != null && error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_CONFLICT) {
                            showError(context.getResources().getString(R.string.error_user_already_block, momentObj.getOwnerDisplayName()));

                        } else {
                            Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                        }
                    }else {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_no_internet), Utils.SHORT_TOAST);

                    }

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void deleteMyMoment(final int position, Moment video) {
        try {
            if (video == null)
                return;
            String url = ServiceURLManager.getInstance().getMomentDeleteUrl(video.getUuid(), DataStorage.getInstance().getUsername());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Moment obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Moment.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        momentList.remove(position);

                        final String username = DataStorage.getInstance().getUsername();
                        String url = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, username, username, AppConsatants.FALSE, "1", "10");
                        VolleySingleton.getInstance(getAppContext()).refresh(url, true);
                    }
                    notifyDataSetChanged();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null && error.networkResponse != null) {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    }else {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_no_internet), Utils.SHORT_TOAST);

                    }
                }
            });
            VolleySingleton.getInstance(getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteMoment(final int position, final Moment video) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                deleteMyMoment(position, video);
            }
        }, 250);
    }

    private void downloadContentFromS3Bucket(HeaderViewHolder holder, String filePath){
        if(filePath!=null && !filePath.isEmpty()) {
            String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
            File file = new File(filePathImage);
            if (file.exists()) {
                holder.mProfileImageViewBackground.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePathImage, false));
            } else {
                filePathImage = Utils.getInstance().getLocalImagePath(filePath);
                file = new File(filePathImage);
                if (file.exists()) {
                    holder.mProfileImageViewBackground.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePathImage, false));
                } else {
                    holder.mProfileImageViewBackground.setImageResource(R.drawable.cover_bg);
                }
            }
//        else{
//            String url = MyApplication.getAppContext().getString(R.string.s3_url);
//            String profImagePath = filePath;
//            url = url + profImagePath;
//            url = url.replace("+","%2B");
//            Log.i("Picasso",url);
////            Picasso.with(context).load(url)
////                    .error(R.drawable.user)
////                    .placeholder(R.drawable.user)
//////                    .memoryPolicy(MemoryPolicy.NO_CACHE)
////                    .networkPolicy(NetworkPolicy.NO_CACHE)
////                    .into(holder.mProfileImageView);
//            Picasso.with(context).load(url)
//                    .error(R.drawable.user)
//                    .placeholder(R.drawable.user)
////                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                    .networkPolicy(NetworkPolicy.NO_CACHE)
//                    .into( holder.mProfileImageViewBackground);
//            holder.mProfileImageViewBackground.setImageAlpha(80);
//        }
            download(holder, filePath);
        }
    }

    private void  download(final HeaderViewHolder holder, String filePath){
        if (userFileManager == null)
            return;
//        userFileManager.setContentCacheSize(1024 * 1024 * 35);
        long fileSize = (1024 * 512);
//            userFileManager.setContentCacheSize(1024 * 1024 * 15);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
                String filePath = Utils.getInstance().getLocalContentPath(contentItem.getFilePath());
                holder.mProfileImageViewBackground.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePath, false));
            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }

    private boolean isPositionHeader(int position) {
        return this.mWithHeader && position == 0;
    }

    @Override
    public int getItemCount() {
        if(momentList == null)
            return 0;
       return momentList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
//        holder.setIsRecyclable(true);
        try {
            if (holder instanceof ItemViewHolder) {
//                Moment video = ((ItemViewHolder) holder).videoPlayer.getVideo();
                ((ItemViewHolder) holder).videoPlayer.removeAllViews();
//                Log.i("recycling &&&&&&&&&&& ", video.toString());
//                videoPlayerController.loadVideoReset(video, ((ItemViewHolder) holder).videoPlayer);

//                ((ItemViewHolder) holder).videoPlayer.reset();
            }
        }catch(Exception e){

        }
    }

    private void setImageViewDimension(ImageView imageView,Bitmap bitmap){
        int newWidth,newHeight;

        Configuration configuration = MyApplication.getAppContext().getResources().getConfiguration();
        int screenHeightDp = configuration.screenWidthDp;
        int deviceWidth = Utils.getInstance().convertDpToPixel(screenHeightDp, context);
        newWidth = deviceWidth;
        newHeight = newWidth;
        try {
            if(bitmap!=null) {
                int intWidth = bitmap.getWidth();
                int intHeight = bitmap.getHeight();
                if (intWidth < deviceWidth) {
                    float height = intHeight;
                    float width = intWidth;
                    height = (deviceWidth / width) * height;
                    newHeight = (int) height;
                    newWidth = deviceWidth;
                }
            }
            imageView.setMinimumWidth(newWidth);
            imageView.setMinimumHeight(newHeight);
        } catch(IllegalStateException e) {
        }catch (Exception e) {
//            Log.i("Videosize", "setting size: " +e.getMessage());
        }
    }

    // need to override this method
    private void geteGroupInfoRequest(Moment moment) {
        try {

            String userUuid = DataStorage.getInstance().getUserUUID();
            String url = ServiceURLManager.getInstance().getGroupProfileUrl(moment.getGroupUuid(),userUuid);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    GroupProfile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), GroupProfile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        String userUuid = DataStorage.getInstance().getUserUUID();
                        String urlList = ServiceURLManager.getInstance().getGroupListUrl(IAPIConstants.API_KEY_GROUP_LIST, userUuid);

                        VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(urlList, false);

                        ModelManager.getInstance().setGroupProfile(obj);
                        String username = obj.getUuid();
                        if(username != null && !username.isEmpty()) {
                            ModelManager.getInstance().setGroupProfile(obj);
                            Intent mIntentActivity = new Intent(context, GroupTimelineTabs.class);
                            context.startActivity(mIntentActivity);
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void updateLikeStatus(final int position,Moment video) {
        try {
            if(video == null)
                return;
            String url =ServiceURLManager.getInstance().getMomentLikeUrl(IAPIConstants.API_KEY_MOMENT_LIKE, video.getUuid(), DataStorage.getInstance().getUsername());

            JsonObjectRequest request =new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Moment obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Moment.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if(obj!=null) {
                        momentList.set(position, obj);
                        String username = DataStorage.getInstance().getUsername();
                        String url = ServiceURLManager.getInstance().getMomentFeedsListUrl(IAPIConstants.API_KEY_GET_MOMENT_FEEDS_TIMELINE_LIST, username, username, AppConsatants.FALSE, "0", "0");

                        String url1 = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, username, username, AppConsatants.FALSE, "0", "0");
                        VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(url1, false);
                        VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(url,false);
                    }
//                    notifyDataSetChanged();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        final ImageView mProfileImageViewBackground;
        final CustomFontEditTextView mProfileNameTextView;
        public CustomFontTextView mJoinTextView;
        public CustomFontTextView mPrivatePublicScope;
        public CustomFontTextView mMemberCount;
        public LinearLayout mProfileLayout;
        public LinearLayout mJoinLayout;
        public LinearLayout mMembersLayout;
        public LinearLayout moreInfoLayout;
        public ImageView joinImageView;
        public LinearLayout mHeaderLayout;
        private ImageView mProfileImageEdit;
        public CustomFontTextView addMemberButton;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            mProfileImageViewBackground = (ImageView)itemView.findViewById(R.id.home_profile_image_background);
            mProfileImageEdit = (ImageView)itemView.findViewById(R.id.camera_icon);
            mProfileNameTextView = (CustomFontEditTextView)itemView.findViewById(R.id.group_name);
            mPrivatePublicScope = (CustomFontTextView)itemView.findViewById(R.id.group_scope);
            mMemberCount = (CustomFontTextView)itemView.findViewById(R.id.member_number_count);
            addMemberButton = (CustomFontTextView) itemView.findViewById(R.id.add_member_button);

            joinImageView = (ImageView)itemView.findViewById(R.id.join_image);
            mJoinLayout = (LinearLayout)itemView.findViewById(R.id.join_layout);
            mJoinTextView = (CustomFontTextView)itemView.findViewById(R.id.join_textView);
            mMembersLayout = (LinearLayout)itemView.findViewById(R.id.members_layout);
            moreInfoLayout = (LinearLayout)itemView.findViewById(R.id.info_layout);
            mProfileLayout = (LinearLayout)itemView.findViewById(R.id.home_profile_layout);
            mHeaderLayout = (LinearLayout) itemView.findViewById(R.id.timeline_header);
            mJoinLayout.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        }

    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public CustomFontTextView loadingMore;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            loadingMore = (CustomFontTextView) itemView.findViewById(R.id.load_more);
        }
    }
    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        public CustomFontTextView mDisplayNameTextView;
        public CustomFontTextView mDaysLeftTextView;
        public CustomFontTextView mCommentsCountTextView;
        public CustomFontTextView mShareCountTextView;
        public CustomFontTextView mLikesTextView;
        public CustomFontTextView mCaptionTextView;
        public CustomFontTextView mDescriptionTextView;
        public CircularImageView mProfileImageView;
        public CustomFontTextView mLikesImageView;
        public CustomFontTextView mCommentsImageView;
        public CustomFontTextView mShareImageView;
        public CustomFontTextView mShareProfileName;
//        public RelativeLayout mThumbnailLayout;
        public LinearLayout mCaptionLayout;
        public LinearLayout mDescriptionLayout;
        public LinearLayout mSharedLayout;
//        public ImageView mThumbnailImage;
        public ImageView mMoreImage;

        public ProgressBar progressBar;
        public RelativeLayout layout;
        public LinearLayout videoPlayer;

        ItemViewHolder(View v) {
            super(v);
            layout = (RelativeLayout) v.findViewById(R.id.layout);
            mDisplayNameTextView = (CustomFontTextView) v.findViewById(R.id.timeline_post_user);
            mCommentsCountTextView = (CustomFontTextView) v.findViewById(R.id.timeline_comments);
            mShareCountTextView = (CustomFontTextView) v.findViewById(R.id.timeline_share);
            mLikesTextView = (CustomFontTextView) v.findViewById(R.id.timeline_likes);
            mDaysLeftTextView = (CustomFontTextView) v.findViewById(R.id.timeline_time);
            mCaptionTextView = (CustomFontTextView) v.findViewById(R.id.timeline_caption);
            mDescriptionTextView = (CustomFontTextView) v.findViewById(R.id.timeline_description);
            mCaptionLayout = (LinearLayout) itemView.findViewById(R.id.timeline_caption_layout);
            mDescriptionLayout = (LinearLayout) itemView.findViewById(R.id.timeline_description_layout);
            mProfileImageView = (CircularImageView) v.findViewById(R.id.timeline_friend_image);
            mLikesImageView = (CustomFontTextView) v.findViewById(R.id.timeline_likes_button);
            mCommentsImageView = (CustomFontTextView) v.findViewById(R.id.timeline_comments_button);
            mShareImageView = (CustomFontTextView) v.findViewById(R.id.timeline_share_button);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
            videoPlayer = (LinearLayout) v.findViewById(R.id.timeline_videoplayer_layout);
            mSharedLayout = (LinearLayout) v.findViewById(R.id.timeline_shared_layout);
            mShareProfileName = (CustomFontTextView) v.findViewById(R.id.timeline_shared);
//            mThumbnailLayout = (RelativeLayout) itemView.findViewById(R.id.timeline_thumbnail_layout);
//            mThumbnailImage = (ImageView) v.findViewById(R.id.timeline_thumbnail);
            mMoreImage = (ImageView) v.findViewById(R.id.timeline_more_image);
            v.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            Moment moment = Utils.getInstance().getMomentObj();

            if (moment != null) {
                if (moreFlag) {
                    if (moment.getOwner().equalsIgnoreCase(DataStorage.getInstance().getUsername())) {
                        MenuItem delete = menu.add(100, 101, 0, context.getResources().getString(R.string.action_delete));
                        delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                final Moment moment = Utils.getInstance().getMomentObj();
                                showOkCancel(context.getResources().getString(R.string.alert_message_delete_moment), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (moment != null)
                                            deleteMoment(Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()), moment);
                                    }
                                });

                                return true;
                            }
                        });

                    } else {
                        if(moment.getOwnerBlocked()!=null &&!moment.getOwnerBlocked()) {
                            MenuItem block = menu.add(100, 102, 1, context.getResources().getString(R.string.block_user,moment.getOwnerDisplayName()));
                            block.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    final Moment moment = Utils.getInstance().getMomentObj();
                                    showOkCancel(context.getResources().getString(R.string.alert_message_block_user, moment.getOwnerDisplayName()), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (moment != null)
                                                sendBlockUserRequest(moment, true,Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()));
                                        }
                                    });

                                    return true;
                                }
                            });
                        }else{
                            MenuItem unblock = menu.add(100, 102, 1, context.getResources().getString(R.string.unblock_user,moment.getOwnerDisplayName()));
                            unblock.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    final Moment moment = Utils.getInstance().getMomentObj();
                                    if (moment != null)
                                        sendBlockUserRequest(moment, false,Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()));

                                    return true;
                                }
                            });
                        }
                        MenuItem report = menu.add(100, 103, 2, context.getResources().getString(R.string.report));
                        report.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                final Moment moment = Utils.getInstance().getMomentObj();
                                    showOkCancel(context.getResources().getString(R.string.alert_message_report_user, moment.getOwnerDisplayName()), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (moment != null)
                                                sendReportMomentRequest(Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()), moment);

                                        }
                                    });
                                    return true;
                                }
                            });
                    }
                } else {


                    MenuItem share = menu.add(100, 104, 3, context.getResources().getString(R.string.share));
                    share.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            Moment moment = Utils.getInstance().getMomentObj();
                            MomentShare momentShare = new MomentShare();
                            momentShare.setOriginalMoment(moment);
                            UploadffmpegService.setMomentShare(momentShare);
                            Intent mIntent = new Intent(context, UploadActivity.class);
                            Bundle mBundle = new Bundle();
                            mBundle.putString(AppConsatants.MOMENT_PATH, Utils.getInstance().getLocalContentPath(moment.getLink()));
                            mBundle.putBoolean(AppConsatants.RESHARE, true);
                            mIntent.putExtras(mBundle);
                            context.startActivity(mIntent);
                            return true;
                        }
                    });
                    MenuItem facebook = menu.add(100, 105, 4, "Share on facebook");
                    facebook.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            Moment moment = Utils.getInstance().getMomentObj();

                            if (moment != null) {
                                String url = getAppContext().getString(R.string.s3_url);
                                String appUrl = getAppContext().getString(R.string.api_google_play_store_url);
                                url = url + moment.getThumbNail();
                                ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
                                        .setContentTitle(moment.getCaption())
                                        .setContentDescription(moment.getDescription())
                                        .setContentUrl(Uri.parse(appUrl))
                                        .setImageUrl(Uri.parse(url))
                                        .build();
                                ShareDialog.show(activity, shareLinkContent);
//                            if (moment != null)
//                                deleteMoment(Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()), moment);
                            }
                            return true;
                        }
                    });
                    MenuItem twitter = menu.add(100, 106, 5, "Share on twitter");
                    twitter.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            Moment moment = Utils.getInstance().getMomentObj();
                            String url = getAppContext().getString(R.string.s3_url);
                            url = url + moment.getThumbNail();
                            String appUrl = getAppContext().getString(R.string.api_google_play_store_url);
                            URL vowmeeUrl = null;
                            String filePath = Utils.getInstance().getLocalContentPath(moment.getThumbNail());// Utils.getInstance().getUserFileManager().getLocalContentPath() + File.separator + moment.getThumbNail();
                            File myImageFile = new File(filePath);
                            Uri imageUri = null;
                            if (myImageFile.exists()) {
                                imageUri = Uri.fromFile(myImageFile);
                            } else {
                                imageUri = Uri.parse(appUrl);
                            }
                            try {
                                vowmeeUrl = new URL("http://www.vowmee.com");
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                            TweetComposer.Builder builder = new TweetComposer.Builder(context)
                                    .text("Vowmee\n" + moment.getCaption() + "\n" + moment.getDescription())
                                    .image(imageUri)
                                    .url(vowmeeUrl);
                            builder.show();
//                            if (moment != null)
//                                deleteMoment(Utils.getInstance().getIntegerValueOfString(moment.getIndexPosition()), moment);

                            return true;
                        }
                    });
                }
            }
        }

      /*  @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case 101:
                   break;
                case 102:
                    Log.i("Menu item","Block clicked");
                    break;
                case 103:
                    Log.i("Menu item","Report clicked");
                    break;
                case 104:
                    Log.i("Menu item","facebook clicked");
                    break;
                case 105:
                    Log.i("Menu item","twitter clicked");
                    break;
            }
            return false;
        }*/
    }

     private void showMemberListDialog(final ArrayList<Profile> memberList){
        ((GroupProfileActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                LayoutInflater inflater = ((GroupProfileActivity) context).getLayoutInflater();
                View convertView = inflater.inflate(R.layout.listview_member, null);
                alertDialog.setView(convertView);
                alertDialog.setTitle(context.getResources().getString(R.string.prompt_members));
                ListView lv = (ListView) convertView.findViewById(R.id.list_view);
                ListMemberAdapter adapter = new ListMemberAdapter(context,memberList);
                lv.setAdapter(adapter);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        sendOwnerChangeRequest(memberList.get(position));
                    }
                });
                alertDialog.show();
            }
        });

    }

    private void showProgressBar(String msg) {

        progressDialog = ProgressDialog.show(context, null, null, true, false);
        progressDialog.setContentView(R.layout.progressbar);
        TextView  progressBarMessage = (TextView)progressDialog.findViewById(R.id.progressBar_message);
        progressBarMessage.setText(msg);

    }

    private void sendUpdateRequest(String name) {
        try {
            if(name.isEmpty())
                return;
            showProgressBar("Updating Group name");
            GroupProfile profile =ModelManager.getInstance().getGroupProfile();
            profile.setName(name);
            String userUuid = DataStorage.getInstance().getUserUUID();
//            String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
            String url = ServiceURLManager.getInstance().getGroupEditUrl(userUuid);
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(profile);
            } catch (IOException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    GroupProfile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), GroupProfile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        ModelManager.getInstance().setGroupProfile(obj);
                    }
                    String userUuid = DataStorage.getInstance().getUserUUID();
                    String urlList = ServiceURLManager.getInstance().getGroupListUrl(IAPIConstants.API_KEY_GROUP_LIST, userUuid);

                    VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(urlList, false);
                    Intent intent = new Intent(context, GroupProfileActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    ((GroupProfileActivity)context).finish();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    showError(context.getResources().getString(R.string.error_general));

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void sendGroupExitRequest() {
        try {
            showProgressBar("Exiting Group");
            String userUuid = DataStorage.getInstance().getUserUUID();
            String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
            String url = ServiceURLManager.getInstance().getGroupMemberExitUrl(groupUuid, userUuid);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    GroupProfile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), GroupProfile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        ModelManager.getInstance().setGroupProfile(obj);
                    }
                    String userUuid = DataStorage.getInstance().getUserUUID();
                    String urlList = ServiceURLManager.getInstance().getGroupListUrl(IAPIConstants.API_KEY_GROUP_LIST, userUuid);

                    VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(urlList, false);
                    Intent intent = new Intent(context, GroupProfileActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    ((GroupProfileActivity)context).finish();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    showError(context.getResources().getString(R.string.error_general));

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }
    private void showError(final String mesg) {
        ((GroupProfileActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {

                new AlertDialog.Builder(context,R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(mesg)
                        .setPositiveButton(android.R.string.ok, null)
                        .show();
            }
        });
    }
    private void showExitGroupMessageOwner(final String mesg, final DialogInterface.OnClickListener okListner) {
        ((GroupProfileActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {

                new AlertDialog.Builder(context,R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(mesg)
                        .setPositiveButton(android.R.string.ok, okListner)
                        .setNegativeButton(android.R.string.cancel,null)
                        .show();
            }
        });
    }
    private void getGroupMemberList() {
        try {
            showProgressBar("");
            String userUUID = DataStorage.getInstance().getUserUUID();
            String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
            String url = ServiceURLManager.getInstance().getGroupMemberListUrl(groupUuid, userUUID,"0","0");

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<Profile> obj = null;
                    try {
//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Profile>>() {
                        });
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {

                        if (obj.size() > 0) {
                            showMemberListDialog((ArrayList<Profile>) obj);
                        }
                    }
                    hideProgressBar();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
//                    showError(R.string.app_name, getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
                }
            })
            {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        //                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void sendOwnerChangeRequest(Profile obj) {
        try {
            showProgressBar("Updating owner of the Group");
            String userUuid = DataStorage.getInstance().getUserUUID();
            String groupUuid = ModelManager.getInstance().getGroupProfile().getUuid();
            String url = ServiceURLManager.getInstance().getGroupOwnerChangeUrl(groupUuid, userUuid,obj.getUuid());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    sendGroupExitRequest();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    showError(context.getResources().getString(R.string.error_general));

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void sendFollowRequest(final GroupProfile profile,final boolean follow,final HeaderViewHolder holder){
        try {
            if(profile == null)
                return;
            showProgressBar("Processing your request");
            final String username = DataStorage.getInstance().getUserUUID();
            final String grouUuid = profile.getUuid();
            String url = ServiceURLManager.getInstance().getGroupFollowRequestUrl(grouUuid,username);
            if(follow)
                url = ServiceURLManager.getInstance().getGroupUNFollowRequestUrl(grouUuid,username);
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    GroupProfile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), GroupProfile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        ModelManager.getInstance().setGroupProfile(obj);
                        if (!follow) {
                            profile.setRoleWithViewer(AppConsatants.GROUP_ROLE_FOLLOWER);
                            holder.addMemberButton.setText(context.getResources().getString(R.string.follow_group));
                        } else {
                            profile.setRoleWithViewer("");
                            holder.addMemberButton.setText(context.getResources().getString(R.string.action_unfollow));
                        }
//                        ModelManager.getInstance().setGroupProfile(profile);
                        notifyDataSetChanged();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                    notifyDataSetChanged();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void sendJoinRequest(final GroupProfile profile,final HeaderViewHolder holder){
        try {
            if(profile == null)
                return;
            showProgressBar("Processing your request");
            final String username = DataStorage.getInstance().getUserUUID();
            final String grouUuid = profile.getUuid();
            String url = ServiceURLManager.getInstance().getGroupJoinUrl(grouUuid,username);

            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    GroupProfile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), GroupProfile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    if (obj != null) {
                        ModelManager.getInstance().setGroupProfile(obj);
                        profile.setIsMembershipRequested(true);
                        ModelManager.getInstance().setGroupProfile(profile);
                        holder.mJoinTextView.setText(context.getResources().getString(R.string.action_request_sent_friend));
                        notifyDataSetChanged();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_request_failed), Utils.SHORT_TOAST);

                    hideProgressBar();
                    notifyDataSetChanged();
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }


    private void downloadContentFromS3Bucket(ImageView imageView, String filePath){
        if(filePath!=null && !filePath.isEmpty()&&imageView!=null) {
            if (!imageView.getTag().toString().equals(filePath)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
                return;
            }

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null)
                        addBitmapToMemoryCache(filePath, image1);
                    imageView.setImageBitmap(image1);
                } else {
                    imageView.setImageResource(R.drawable.user);
                }
            }
//            if(!disableLoad)
                download(imageView, filePath);
        }
    }
    private void  download(final ImageView imageView, String filePath){
        if (userFileManager == null)
            return;
        long fileSize = (1024 * 512);
        userFileManager.getContent(filePath, fileSize, ContentDownloadPolicy.DOWNLOAD_IF_NEWER_EXIST, false, new ContentProgressListener() {
            @Override
            public void onSuccess(ContentItem contentItem) {
//                if(contentItem !=null &&!contentItem.getFilePath().isEmpty()) {
////                    new LoadImage(imageView).execute();
//                }

            }

            @Override
            public void onProgressUpdate(String filePath, boolean isWaiting, long bytesCurrent, long bytesTotal) {
//                Log.i("FIle download " + filePath, "" + bytesCurrent + " / " + bytesTotal);
            }

            @Override
            public void onError(String filePath, Exception ex) {
//                Log.i("FIle download " + filePath, ex.getMessage());
            }
        });
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            FileCache.getInstance().addBitmapToMemoryCache(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return  FileCache.getInstance().getBitmapFromMemCache(key);
    }
//    class LoadImage extends AsyncTask<Object, Void, Bitmap> {
//
//        private ImageView imv;
//        private String path;
//
//        public LoadImage(ImageView imv) {
//            this.imv = imv;
//
//            this.path = imv.getTag().toString();
//        }
//
//        @Override
//        protected Bitmap doInBackground(Object... params) {
//            Bitmap bitmap = null;
//            if(path!=null && !path.isEmpty()) {
//                String filePathImage = Utils.getInstance().getLocalContentPath(path);
//                File file = new File(filePathImage);
//                if (file.exists()) {
//                    bitmap = Utils.getInstance().loadImageFromStorage(filePathImage, false);
//                    if(profileImageDimensPixelsDefault >0&& profileImageDimensPixelsDefault >0)
//                        bitmap = Bitmap.createScaledBitmap(bitmap, profileImageDimensPixelsDefault, profileImageDimensPixelsDefault,false);
//                }
////            download(holder, filePath,width,height);
//            }
//            return bitmap;
//        }
//        @Override
//        protected void onPostExecute(Bitmap result) {
//            if (!imv.getTag().toString().equals(path)) {
//               /* The path is not same. This means that this
//                  image view is handled by some other async task.
//                  We don't do anything and return. */
//                return;
//            }
//
//            if(result != null && imv != null){
//                imv.setVisibility(View.VISIBLE);
//                imv.setImageBitmap(result);
//            }else{
//                imv.setImageResource(R.drawable.user);
//            }
//        }
//
//    }

    public void setOnLoadMoreListener(ILoadMoreItems iLoadMoreItems) {
        this.iLoadMore = iLoadMoreItems;
    }

    private void showOkCancel(final String message, final DialogInterface.OnClickListener okClicked) {
        try {
            ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new android.support.v7.app.AlertDialog.Builder(context).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(message)
                            .setNegativeButton(android.R.string.cancel, null)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .show();
                }
            });
        }catch(WindowManager.BadTokenException e){

        }catch (Exception e){}
    }
}
