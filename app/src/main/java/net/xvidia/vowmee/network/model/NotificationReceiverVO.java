package net.xvidia.vowmee.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationReceiverVO {

	@JsonProperty("friends")private Boolean friends;
	@JsonProperty("followers")private Boolean followers;
	@JsonProperty("followings")private Boolean followings;
	@JsonProperty("usernameList")List<String> usernameList;
	@JsonProperty("contactNoList")List<String> contactNoList;
	
	
	public Boolean getFriends() {
		return friends;
	}
	public void setFriends(Boolean friends) {
		this.friends = friends;
	}
	public Boolean getFollowers() {
		return followers;
	}
	public void setFollowers(Boolean followers) {
		this.followers = followers;
	}
	public List<String> getUsernameList() {
		return usernameList;
	}
	public void setUsernameList(List<String> usernameList) {
		this.usernameList = usernameList;
	}
	public List<String> getContactNoList() {
		return contactNoList;
	}
	public void setContactNoList(List<String> contactNoList) {
		this.contactNoList = contactNoList;
	}
	public Boolean getFollowings() {
		return followings;
	}

	public void setFollowings(Boolean followings) {
		this.followings = followings;
	}




}
