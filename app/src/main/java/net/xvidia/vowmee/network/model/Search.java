package net.xvidia.vowmee.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Search {
	@JsonProperty("personVos")List<Profile> personVos;
	@JsonProperty("groupVos")List<GroupProfile> groupVos;

	public List<Profile> getPersonVos() {
		return personVos;
	}

	public void setPersonVos(List<Profile> personVos) {
		this.personVos = personVos;
	}

	public List<GroupProfile> getGroupVos() {
		return groupVos;
	}

	public void setGroupVos(List<GroupProfile> groupVos) {
		this.groupVos = groupVos;
	}
}
