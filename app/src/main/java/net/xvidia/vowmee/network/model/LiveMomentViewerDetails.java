package net.xvidia.vowmee.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LiveMomentViewerDetails {
	@JsonProperty("totalViewerCount")long totalViewerCount; //
	@JsonProperty("activeViewerCount")long activeViewerCount;
	@JsonProperty("lastFewActiveViewers")List<Profile> lastFewActiveViewers;

	public long getTotalViewerCount() {
		return totalViewerCount;
	}

	public void setTotalViewerCount(long totalViewerCount) {
		this.totalViewerCount = totalViewerCount;
	}

	public long getActiveViewerCount() {
		return activeViewerCount;
	}

	public void setActiveViewerCount(long activeViewerCount) {
		this.activeViewerCount = activeViewerCount;
	}

	public List<Profile> getLastFewActiveViewers() {
		return lastFewActiveViewers;
	}

	public void setLastFewActiveViewers(List<Profile> lastFewActiveViewers) {
		this.lastFewActiveViewers = lastFewActiveViewers;
	}
}
