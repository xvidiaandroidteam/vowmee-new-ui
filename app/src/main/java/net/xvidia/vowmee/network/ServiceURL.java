package net.xvidia.vowmee.network;


import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;

public class ServiceURL {

	public static final String API_KEY_AUTH_VOWMEE = MyApplication.getAppContext().getResources().getString(R.string.api_auth_login);
	public static final String API_KEY_AUTH_FACEBOOK = MyApplication.getAppContext().getResources().getString(R.string.api_auth_facebook);
	public static final String API_KEY_AUTH_TWITTER = MyApplication.getAppContext().getResources().getString(R.string.api_auth_twitter);
	public static final String API_KEY_SOCIAL_REGISTER= MyApplication.getAppContext().getResources().getString(R.string.api_social_regiter);
	public static final String API_KEY_DIGIT_REGISTER= MyApplication.getAppContext().getResources().getString(R.string.api_digit_regiter);
	public static final String API_KEY_DIGIT_LOGIN= MyApplication.getAppContext().getResources().getString(R.string.api_digit_login);
	public static final String API_KEY_LOGOUT= MyApplication.getAppContext().getResources().getString(R.string.api_logout);
	public static final String API_KEY_DIGIT_LOGIN_REGENERATE= MyApplication.getAppContext().getResources().getString(R.string.api_digit_login_regenrate);
	public static final String API_KEY_DEACTIVATE_ACCOUNT= MyApplication.getAppContext().getResources().getString(R.string.api_deactivate_account);
	public static final String API_KEY_GET_PROFILE= MyApplication.getAppContext().getResources().getString(R.string.api_get_profile);
	public static final String API_KEY_UPDATE_PROFILE = MyApplication.getAppContext().getResources().getString(R.string.api_update_profile);
	public static final String API_KEY_SEARCH_PERSON= MyApplication.getAppContext().getResources().getString(R.string.api_search_person);
	public static final String API_KEY_ADD_FRIEND = MyApplication.getAppContext().getResources().getString(R.string.api_add_friend);
	public static final String API_KEY_ACCEPT_FRIEND = MyApplication.getAppContext().getResources().getString(R.string.api_accept_friend);
	public static final String API_KEY_DENY_FRIEND = MyApplication.getAppContext().getResources().getString(R.string.api_deny_friend);
	public static final String API_KEY_REMOVE_FRIEND = MyApplication.getAppContext().getResources().getString(R.string.api_remove_friend);
	public static final String API_KEY_GET_FRIEND_LIST = MyApplication.getAppContext().getResources().getString(R.string.api_get_friend_list);
	public static final String API_KEY_GET_PENDING_FRIENDS_LIST = MyApplication.getAppContext().getResources().getString(R.string.api_get_pending_friend_list);
	public static final String  API_KEY_WITHDRAW_REQUEST_FRIEND =  MyApplication.getAppContext().getResources().getString(R.string.api_withdraw_request_friend);;

	public static final String API_KEY_ADD_FOLLOW = MyApplication.getAppContext().getResources().getString(R.string.api_add_follow);
	public static final String API_KEY_ACCEPT_FOLLOW = MyApplication.getAppContext().getResources().getString(R.string.api_accept_follow);
	public static final String API_KEY_DENY_FOLLOW = MyApplication.getAppContext().getResources().getString(R.string.api_deny_follow);
	public static final String API_KEY_REMOVE_FOLLOW = MyApplication.getAppContext().getResources().getString(R.string.api_remove_follow);
	public static final String API_KEY_GET_FOLLOW_LIST = MyApplication.getAppContext().getResources().getString(R.string.api_get_follow_list);
	public static final String API_KEY_GET_FOLLOWERS_LIST = MyApplication.getAppContext().getResources().getString(R.string.api_get_followers_list);
	public static final String API_KEY_GET_PENDING_FOLLOWERS_LIST = MyApplication.getAppContext().getResources().getString(R.string.api_get_pending_followers_list);
	public static final String API_KEY_GET_PERSON_BY_FACEBOOK_ID = MyApplication.getAppContext().getResources().getString(R.string.api_search_by_facebook_id);
	public static final String API_KEY_GET_PERSON_BY_TWITTER_ID = MyApplication.getAppContext().getResources().getString(R.string.api_search_by_twitter_id);
	public static final String API_KEY_GET_PERSON_BY_PHONE_CONTACT = MyApplication.getAppContext().getResources().getString(R.string.api_search_by_phone_contact);
	public static final String API_KEY_GET_OWN_ACTIVITY_LIST = MyApplication.getAppContext().getResources().getString(R.string.api_own_activity);
	public static final String API_KEY_GET_OTHER_ACTIVITY_LIST = MyApplication.getAppContext().getResources().getString(R.string.api_other_activity);
	public static final String API_KEY_PERSON_BLOCK= MyApplication.getAppContext().getResources().getString(R.string.api_block_user);
	public static final String API_KEY_PERSON_UNBLOCK= MyApplication.getAppContext().getResources().getString(R.string.api_unblock_user);
	public static final String API_KEY_PERSON_BLOCK_LIST= MyApplication.getAppContext().getResources().getString(R.string.api_block_list);

	public static final String API_KEY_MOMENT_ADD = MyApplication.getAppContext().getResources().getString(R.string.api_moment_add);
	public static final String API_KEY_MOMENT_NOTIFY = MyApplication.getAppContext().getResources().getString(R.string.api_moment_notify);

	public static final String API_KEY_MOMENT_LIKE = MyApplication.getAppContext().getResources().getString(R.string.api_moment_like);
	public static final String API_KEY_GET_MOMENT_TIMELINE_LIST = MyApplication.getAppContext().getResources().getString(R.string.api_moment_timeline_list);
	public static final String API_KEY_GET_MOMENT_FEEDS_TIMELINE_LIST = MyApplication.getAppContext().getResources().getString(R.string.api_moment_feeds_timeline_list);
	public static final String API_KEY_ADD_COMMENT = MyApplication.getAppContext().getResources().getString(R.string.api_add_comments);
	public static final String API_KEY_GET_COMMENT_LIST = MyApplication.getAppContext().getResources().getString(R.string.api_get_comment_list);
	public static final String API_KEY_GET_MOMENT_BY_UUID = MyApplication.getAppContext().getResources().getString(R.string.api_get_moment_by_uuid);
	public static final String API_KEY_MOMENT_DELETE = MyApplication.getAppContext().getResources().getString(R.string.api_delete_moment_by_uuid);
	public static final String API_KEY_GET_PUBLISHER = MyApplication.getAppContext().getResources().getString(R.string.live_publisher_url);
	public static final String API_KEY_GET_SUBSCRIBER = MyApplication.getAppContext().getResources().getString(R.string.live_subscriber_url);
	public static final String API_KEY_LIVE_CLOSE = MyApplication.getAppContext().getResources().getString(R.string.live_close_url);
	public static final String API_KEY_LIVE_END = MyApplication.getAppContext().getResources().getString(R.string.live_end_url);
	public static final String API_KEY_LIVE_END_ALL = MyApplication.getAppContext().getResources().getString(R.string.live_end_all_url);
	public static final String API_KEY_LIVE_COUNT = MyApplication.getAppContext().getResources().getString(R.string.live_viewer_count);
	public static final String API_KEY_UPDATE_GCM = MyApplication.getAppContext().getResources().getString(R.string.api_update_gcm);
	public static final String API_KEY_MOMENT_REPORT= MyApplication.getAppContext().getResources().getString(R.string.api_moment_report);

	/*GROUP*/
	public static final String API_KEY_GROUP_CREATE = MyApplication.getAppContext().getResources().getString(R.string.api_group_create_url);
	public static final String API_KEY_GROUP_EDIT = MyApplication.getAppContext().getResources().getString(R.string.api_group_edit_url);
	public static final String API_KEY_GROUP_DELETE = MyApplication.getAppContext().getResources().getString(R.string.api_group_delete_url);
	public static final String API_KEY_GROUP_EXIT = MyApplication.getAppContext().getResources().getString(R.string.api_group_exit_url);
	public static final String API_KEY_GROUP_PROFILE = MyApplication.getAppContext().getResources().getString(R.string.api_group_profile_url);
	public static final String API_KEY_GROUP_OWNER_CHANGE = MyApplication.getAppContext().getResources().getString(R.string.api_group_owner_change_url);
	public static final String API_KEY_GROUP_LIST = MyApplication.getAppContext().getResources().getString(R.string.api_group_list_url);
	public static final String API_KEY_GROUP_MEMBER_LIST = MyApplication.getAppContext().getResources().getString(R.string.api_group_memeber_list_url);
	public static final String API_KEY_GROUP_MEMBER_ADD = MyApplication.getAppContext().getResources().getString(R.string.api_group_memeber_add_url);
	public static final String API_KEY_GROUP_MEMBER_DELETE = MyApplication.getAppContext().getResources().getString(R.string.api_group_memeber_delete_url);
	public static final String API_KEY_GROUP_MEMBER_ASSIGN_ROLE = MyApplication.getAppContext().getResources().getString(R.string.api_group_memeber_assign_role);
	public static final String API_KEY_GROUP_MEMBER_UNASSIGN_ROLE = MyApplication.getAppContext().getResources().getString(R.string.api_group_memeber_unassign_role);
	public static final String API_KEY_GROUP_MOMENT_LIST = MyApplication.getAppContext().getResources().getString(R.string.api_group_moment_list_url);
	public static final String API_KEY_GROUP_MOMENT_DELETE = MyApplication.getAppContext().getResources().getString(R.string.api_group_moment_delete_url);
	public static final String API_KEY_GROUP_MOMENT_POST= MyApplication.getAppContext().getResources().getString(R.string.api_group_moment_post_url);
	public static final String API_KEY_GROUP_JOIN= MyApplication.getAppContext().getResources().getString(R.string.api_group_join);
	public static final String API_KEY_GROUP_JOIN_ACCEPT= MyApplication.getAppContext().getResources().getString(R.string.api_group_join_accept);
	public static final String API_KEY_GROUP_JOIN_DENY= MyApplication.getAppContext().getResources().getString(R.string.api_group_join_deny);
	public static final String API_KEY_GROUP_JOIN_REQUEST= MyApplication.getAppContext().getResources().getString(R.string.api_group_join_request);
	public static final String API_KEY_GROUP_FOLLOW= MyApplication.getAppContext().getResources().getString(R.string.api_group_follow);
	public static final String API_KEY_GROUP_UNFOLLOW= MyApplication.getAppContext().getResources().getString(R.string.api_group_unfollow);
	public static final String API_KEY_SUGGEST= MyApplication.getAppContext().getResources().getString(R.string.api_suggestion);
	public static final String API_KEY_GROUP_REPORT= MyApplication.getAppContext().getResources().getString(R.string.api_group_report);
	/*CALL URls*/
	public static final String API_KEY_MAKE_CALL= MyApplication.getAppContext().getResources().getString(R.string.api_make_call);
	public static final String API_KEY_ACCEPT_CALL= MyApplication.getAppContext().getResources().getString(R.string.api_accept_call);
	public static final String API_KEY_DECLINE_CALL= MyApplication.getAppContext().getResources().getString(R.string.api_decline_call);
	public static final String API_KEY_END_CALL= MyApplication.getAppContext().getResources().getString(R.string.api_end_call);
	public static final String API_KEY_ADD_MEMBER_CALL= MyApplication.getAppContext().getResources().getString(R.string.api_add_member_to_call);

}
