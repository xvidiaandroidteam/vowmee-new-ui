package net.xvidia.vowmee.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Ravi on 11/5/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Opentok {
    @JsonProperty("sessionId")
    private String sessionId;
    @JsonProperty("token")
    private String token;
    @JsonProperty("apiKey")
    private String apiKey;
    @JsonProperty("status")
    private String status;
    @JsonProperty("liveChatOn")
    private boolean liveChatOn;

    public boolean isLiveChatOn() {
        return liveChatOn;
    }

    public void setLiveChatOn(boolean liveChatOn) {
        this.liveChatOn = liveChatOn;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
