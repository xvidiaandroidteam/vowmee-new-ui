package net.xvidia.vowmee.network.model;

public class UploadData {

    private String momentShareText;
    private String momentUuid;
    private String momentPath;
    private String momentCompressedPath;
    private Boolean recordedMoment;
//    private Boolean uploadedMoment;
    private Boolean mReshare;

//    public Boolean getUploadedMoment() {
//        return uploadedMoment;
//    }
//
//    public void setUploadedMoment(Boolean uploadedMoment) {
//        this.uploadedMoment = uploadedMoment;
//    }

    public String getMomentCompressedPath() {
        return momentCompressedPath;
    }

    public void setMomentCompressedPath(String momentCompressedPath) {
        this.momentCompressedPath = momentCompressedPath;
    }

    public String getMomentPath() {
        return momentPath;
    }

    public void setMomentPath(String momentPath) {
        this.momentPath = momentPath;
    }

    public Boolean getRecordedMoment() {
        return recordedMoment;
    }

    public void setRecordedMoment(Boolean recordedMoment) {
        this.recordedMoment = recordedMoment;
    }

    public String getMomentShareText() {
        return momentShareText;
    }

    public void setMomentShareText(String momentShareText) {
        this.momentShareText = momentShareText;
    }

    public String getMomentUuid() {
        return momentUuid;
    }

    public void setMomentUuid(String momentUuid) {
        this.momentUuid = momentUuid;
    }

    public Boolean getmReshare() {
        return mReshare;
    }

    public void setmReshare(Boolean mReshare) {
        this.mReshare = mReshare;
    }

    @Override
    public String toString() {
        return "UploadData{" +
                "momentShareText='" + momentShareText + '\'' +
                ", momentUuid='" + momentUuid + '\'' +
                ", mReshare=" + mReshare +
                '}';
    }
}
