package net.xvidia.vowmee.network;


import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleySingleton {
    private static VolleySingleton mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    private VolleySingleton() {
    }

    public static synchronized VolleySingleton getInstance(Context ctx) {
        if (mInstance == null) {
            mInstance = new VolleySingleton();
            mCtx = ctx;
        }
        return mInstance;
    }

    public void refresh(String key,boolean fullexpire) {
        if (mRequestQueue != null) {
           getRequestQueue().getCache().invalidate(key,fullexpire);
        }
    }
    public void clearCache() {
        if (mRequestQueue != null) {
            getRequestQueue().getCache().clear();
        }
    }
    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mCtx);
        }
        return mRequestQueue;
    }
    public void cancelAll(){
        getRequestQueue().cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
    }
    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}
