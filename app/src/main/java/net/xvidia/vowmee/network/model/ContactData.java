package net.xvidia.vowmee.network.model;

/**
 * Created by Ravi on 01/2/16.
 */
public class ContactData {

    private String displayName;
    private String phone;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "ContactData{" +
                "displayName='" + displayName + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
