package net.xvidia.vowmee.network;


import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;

public class ServiceURLManager implements IAPIConstants {

    private static ServiceURLManager instance = null;

    private ServiceURLManager() {

    }

    public static ServiceURLManager getInstance() {
        if (instance == null) {
            instance = new ServiceURLManager();
        }
        return instance;
    }



    private String getURL(int SERVICE_API) {
        String service = "";
        final String base_URL = MyApplication.getAppContext().getResources().getString(R.string.base_url);
//        final String base_URL_Live= MyApplication.getAppContext().getResources().getString(R.string.base_url_live);
        switch (SERVICE_API) {
            case API_KEY_AUTH_VOWMEE:
                service = base_URL + ServiceURL.API_KEY_AUTH_VOWMEE;
                break;
            case API_KEY_AUTH_FACEBOOK:
                service = base_URL + ServiceURL.API_KEY_AUTH_FACEBOOK;
                break;
            case API_KEY_AUTH_TWITTER:
                service = base_URL + ServiceURL.API_KEY_AUTH_TWITTER;
                break;
            case API_KEY_SOCIAL_REGISTER:
                service = base_URL + ServiceURL.API_KEY_SOCIAL_REGISTER;
                break;
            case API_KEY_DIGIT_REGISTER:
                service = base_URL + ServiceURL.API_KEY_DIGIT_REGISTER;
                break;
            case API_KEY_DIGIT_LOGIN:
                service = base_URL + ServiceURL.API_KEY_DIGIT_LOGIN;
                break;
            case API_KEY_LOGOUT:
                service = base_URL + ServiceURL.API_KEY_LOGOUT;
                break;
            case API_KEY_DIGIT_LOGIN_REGENERATE:
                service = base_URL + ServiceURL.API_KEY_DIGIT_LOGIN_REGENERATE;
                break;
            case API_KEY_MOMENT_ADD:
                service = base_URL + ServiceURL.API_KEY_MOMENT_ADD;
                break;
            case API_KEY_UPDATE_PROFILE:
                service = base_URL + ServiceURL.API_KEY_UPDATE_PROFILE;
                break;
            case API_KEY_DEACTIVATE_ACCOUNT:
                service = base_URL + ServiceURL.API_KEY_DEACTIVATE_ACCOUNT;
                break;
            case API_KEY_GET_PROFILE:
                service = base_URL + ServiceURL.API_KEY_GET_PROFILE;
                break;
            case API_KEY_MOMENT_LIKE:
                service = base_URL + ServiceURL.API_KEY_MOMENT_LIKE;
                break;
            case API_KEY_SEARCH_PERSON:
                service = base_URL + ServiceURL.API_KEY_SEARCH_PERSON;
                break;
            case API_KEY_ADD_FRIEND:
                service = base_URL + ServiceURL.API_KEY_ADD_FRIEND;
                break;
            case API_KEY_ACCEPT_FRIEND:
                service = base_URL + ServiceURL.API_KEY_ACCEPT_FRIEND;
                break;
            case API_KEY_DENY_FRIEND:
                service = base_URL + ServiceURL.API_KEY_DENY_FRIEND;
                break;
            case API_KEY_REMOVE_FRIEND:
                service = base_URL + ServiceURL.API_KEY_REMOVE_FRIEND;
                break;
            case API_KEY_WITHDRAW_REQUEST_FRIEND:
                service = base_URL + ServiceURL.API_KEY_WITHDRAW_REQUEST_FRIEND;
                break;
            case API_KEY_PERSON_BLOCK:
                service = base_URL + ServiceURL.API_KEY_PERSON_BLOCK;
                break;
            case API_KEY_PERSON_UNBLOCK:
                service = base_URL + ServiceURL.API_KEY_PERSON_UNBLOCK;
                break;
            case API_KEY_PERSON_BLOCK_LIST:
                service = base_URL + ServiceURL.API_KEY_PERSON_BLOCK_LIST;
                break;
            case API_KEY_GET_FRIEND_LIST:
                service = base_URL + ServiceURL.API_KEY_GET_FRIEND_LIST;
                break;
            case API_KEY_ADD_FOLLOW:
                service = base_URL + ServiceURL.API_KEY_ADD_FOLLOW;
                break;
            case API_KEY_ACCEPT_FOLLOW:
                service = base_URL + ServiceURL.API_KEY_ACCEPT_FOLLOW;
                break;
            case API_KEY_DENY_FOLLOW:
                service = base_URL + ServiceURL.API_KEY_DENY_FOLLOW;
                break;
            case API_KEY_REMOVE_FOLLOW:
                service = base_URL + ServiceURL.API_KEY_REMOVE_FOLLOW;
                break;
            case API_KEY_GET_FOLLOW_LIST:
                service = base_URL + ServiceURL.API_KEY_GET_FOLLOW_LIST;
                break;
            case API_KEY_GET_FOLLOWERS_LIST:
                service = base_URL + ServiceURL.API_KEY_GET_FOLLOWERS_LIST;
                break;

            case API_KEY_GET_PENDING_FOLLOWERS_LIST:
                service = base_URL + ServiceURL.API_KEY_GET_PENDING_FOLLOWERS_LIST;
                break;

            case API_KEY_GET_PENDING_FRIENDS_LIST:
                service = base_URL + ServiceURL.API_KEY_GET_PENDING_FRIENDS_LIST;
                break;
            case API_KEY_GET_MOMENT_TIMELINE_LIST:
                service = base_URL + ServiceURL.API_KEY_GET_MOMENT_TIMELINE_LIST;
                break;
            case API_KEY_MOMENT_DELETE:
                service = base_URL + ServiceURL.API_KEY_MOMENT_DELETE;
                break;

            case API_KEY_MOMENT_NOTIFY:
                service = base_URL + ServiceURL.API_KEY_MOMENT_NOTIFY;
                break;

            case API_KEY_GET_MOMENT_FEEDS_TIMELINE_LIST:
                service = base_URL + ServiceURL.API_KEY_GET_MOMENT_FEEDS_TIMELINE_LIST;
                break;

            case API_KEY_GET_PERSON_BY_FACEBOOK_ID:
                service = base_URL + ServiceURL.API_KEY_GET_PERSON_BY_FACEBOOK_ID;
                break;
            case API_KEY_GET_PERSON_BY_TWITTER_ID:
                service = base_URL + ServiceURL.API_KEY_GET_PERSON_BY_TWITTER_ID;
                break;

            case API_KEY_GET_PERSON_BY_PHONE_CONTACT:
                service = base_URL + ServiceURL.API_KEY_GET_PERSON_BY_PHONE_CONTACT;
                break;
            case API_KEY_GET_OWN_ACTIVITY_LIST:
                service = base_URL + ServiceURL.API_KEY_GET_OWN_ACTIVITY_LIST;
                break;
            case API_KEY_GET_OTHER_ACTIVITY_LIST:
                service = base_URL +ServiceURL.API_KEY_GET_OTHER_ACTIVITY_LIST;
                break;
            case API_KEY_ADD_COMMENT:
                service = base_URL + ServiceURL.API_KEY_ADD_COMMENT;
                break;
            case API_KEY_GET_COMMENT_LIST:
                service = base_URL + ServiceURL.API_KEY_GET_COMMENT_LIST;
                break;
            case API_KEY_GET_MOMENT_BY_UUID:
                service = base_URL + ServiceURL.API_KEY_GET_MOMENT_BY_UUID;
                break;
            case API_KEY_UPDATE_GCM:
                service = base_URL + ServiceURL.API_KEY_UPDATE_GCM;
                break;
            case API_KEY_GROUP_CREATE:
                service = base_URL + ServiceURL.API_KEY_GROUP_CREATE;
                break;
            case API_KEY_GROUP_EDIT:
                service = base_URL + ServiceURL.API_KEY_GROUP_EDIT;
                break;
            case API_KEY_GROUP_DELETE:
                service = base_URL + ServiceURL.API_KEY_GROUP_DELETE;
                break;
            case API_KEY_GROUP_EXIT:
                service = base_URL + ServiceURL.API_KEY_GROUP_EXIT;
                break;
            case API_KEY_GROUP_OWNER_CHANGE:
                service = base_URL + ServiceURL.API_KEY_GROUP_OWNER_CHANGE;
                break;
            case API_KEY_GROUP_LIST:
                service = base_URL + ServiceURL.API_KEY_GROUP_LIST;
                break;
            case API_KEY_GROUP_MOMENT_LIST:
                service = base_URL + ServiceURL.API_KEY_GROUP_MOMENT_LIST;
                break;

            case API_KEY_GROUP_MOMENT_DELETE:
                service = base_URL + ServiceURL.API_KEY_GROUP_MOMENT_DELETE;
                break;
            case API_KEY_GROUP_MOMENT_POST:
                service = base_URL + ServiceURL.API_KEY_GROUP_MOMENT_POST;
                break;
            case API_KEY_GROUP_MEMBER_LIST:
                service = base_URL + ServiceURL.API_KEY_GROUP_MEMBER_LIST;
                break;
            case API_KEY_GROUP_MEMBER_ADD:
                service = base_URL + ServiceURL.API_KEY_GROUP_MEMBER_ADD;
                break;
            case API_KEY_GROUP_MEMBER_DELETE:
                service = base_URL + ServiceURL.API_KEY_GROUP_MEMBER_DELETE;
                break;
            case API_KEY_GROUP_MEMBER_ASSIGN_ROLE:
                service = base_URL + ServiceURL.API_KEY_GROUP_MEMBER_ASSIGN_ROLE;
                break;
            case API_KEY_GROUP_MEMBER_UNASSIGN_ROLE:
                service = base_URL + ServiceURL.API_KEY_GROUP_MEMBER_UNASSIGN_ROLE;
                break;
            case API_KEY_GROUP_PROFILE:
                service = base_URL + ServiceURL.API_KEY_GROUP_PROFILE;
                break;
            case API_KEY_GROUP_JOIN:
                service = base_URL + ServiceURL.API_KEY_GROUP_JOIN;
                break;
            case API_KEY_GROUP_JOIN_ACCEPT:
                service = base_URL + ServiceURL.API_KEY_GROUP_JOIN_ACCEPT;
                break;
            case API_KEY_GROUP_JOIN_DENY:
                service = base_URL + ServiceURL.API_KEY_GROUP_JOIN_DENY;
                break;
            case API_KEY_GROUP_JOIN_REQUEST:
                service = base_URL + ServiceURL.API_KEY_GROUP_JOIN_REQUEST;
                break;
            case API_KEY_GROUP_FOLLOW:
                service = base_URL + ServiceURL.API_KEY_GROUP_FOLLOW;
                break;
            case API_KEY_GROUP_UNFOLLOW:
                service = base_URL + ServiceURL.API_KEY_GROUP_UNFOLLOW;
                break;
            case API_KEY_GROUP_REPORT:
                service = base_URL + ServiceURL.API_KEY_GROUP_REPORT;
                break;
            case API_KEY_MOMENT_REPORT:
                service = base_URL + ServiceURL.API_KEY_MOMENT_REPORT;
                break;
            case API_KEY_SUGGEST:
                service = base_URL + ServiceURL.API_KEY_SUGGEST;
                break;
/*CALL*/
            case API_KEY_MAKE_CALL:
                service = base_URL + ServiceURL.API_KEY_MAKE_CALL;
                break;
            case API_KEY_ACCEPT_CALL:
                service = base_URL + ServiceURL.API_KEY_ACCEPT_CALL;
                break;
            case API_KEY_DECLINE_CALL:
                service = base_URL + ServiceURL.API_KEY_DECLINE_CALL;
                break;
            case API_KEY_END_CALL:
                service = base_URL + ServiceURL.API_KEY_END_CALL;
                break;
            case API_KEY_ADD_MEMBER_CALL:
                service = base_URL + ServiceURL.API_KEY_ADD_MEMBER_CALL;
                break;
            /*LIVE */
            case API_KEY_GET_PUBLISHER:
                service = base_URL + ServiceURL.API_KEY_GET_PUBLISHER;
                break;
            case API_KEY_GET_SUBSCRIBER:
                service = base_URL + ServiceURL.API_KEY_GET_SUBSCRIBER;
                break;
            case API_KEY_LIVE_CLOSE:
                service = base_URL + ServiceURL.API_KEY_LIVE_CLOSE;
                break;
            case API_KEY_LIVE_END:
                service = base_URL + ServiceURL.API_KEY_LIVE_END;
                break;
            case API_KEY_LIVE_END_ALL:
                service = base_URL + ServiceURL.API_KEY_LIVE_END_ALL;
                break;
            case API_KEY_LIVE_COUNT:
                service = base_URL + ServiceURL.API_KEY_LIVE_COUNT;
                break;
        }
        return service;
    }


    /**
     * gets the url string for a particular url key
     *
     * @param urlKey
     * @return url
     */
    public String getUrl(int urlKey) {
        String url = getURL(urlKey);
        url = url.replace("+","%2B");
        url = url.replace(" ","%20");
        return url;
    }

    /**
     * gets the moment list url
     *
     * @param urlKey
     * @param user
     * @param page
     * @param size
     * @return moment url
     */
    public String getMomentListUrl(int urlKey, String user, String page, String size) {
        String url = getURL(urlKey) + user + "&page=" + page + "&size=" + size;
        return url;
    }

    /**
     *
     * @param personUuid
     * @return
     */
    public String getLogoutUrl(String personUuid){
        String url = getURL(API_KEY_LOGOUT)+personUuid;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     * gets the profile information of the other user
     *
     * @param urlKey
     * @param viewer
     * @param viewee
     * @return profile url
     */
    public String getProfileUrl(int urlKey, String viewer, String viewee) {
        String url = getURL(urlKey) + viewer + "&viewee=" + viewee;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     * gets the profile information of the other user
     *
     * @param urlKey
     * @param Uuid
     * @return profile url
     */
    public String getMomentByUuid(int urlKey, String Uuid, String viewer) {
        String url = getURL(urlKey) + Uuid+ "&viewer="+viewer;
        url = url.replace("+","%2B");
        return url;
    }
    /**
     * gets the search url
     *
     * @param urlKey
     * @param user
     * @param searchString
     * @param page
     * @param size
     * @return url
     */
    public String getSearchListUrl(int urlKey, String user, String searchString, String page, String size) {
        String url = getURL(urlKey) + user + "&text=" + searchString + "&page=" + page + "&size=" + size;
        url = url.replace("+","%2B");
        url = url.replace(" ","%20");
        return url;
    }

    /**
     *
     * @param blockerUuid
     * @param blockingUuid
     * @return
     */
    public String getBlockUserUrl(String blockerUuid,String blockingUuid) {
        String url = getURL(API_KEY_PERSON_BLOCK)+blockerUuid+"&blocking="+blockingUuid;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     *
     * @param blockerUuid
     * @param blockingUuid
     * @return
     */
    public String getUnBlockUserUrl(String blockerUuid,String blockingUuid) {
        String url = getURL(API_KEY_PERSON_UNBLOCK)+blockerUuid+"&blocking="+blockingUuid;
        url = url.replace("+","%2B");
        return url;
    }


    /**
     * @param page
     * @param size
     * @return
     */
    public String getBlockedListUrl(String userUuid,String page, String size) {
        String url = getURL(API_KEY_PERSON_BLOCK_LIST)+userUuid+"&page=" + page + "&size=" + size;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param urlKey
     * @param usernameOwn
     * @param usernameOther
     * @param page
     * @param size
     * @return
     */
    public String getListUrl(int urlKey, String usernameOwn, String usernameOther, String page, String size) {
        String url = getURL(urlKey) + usernameOther  + "&viewer="+ usernameOwn + "&page=" + page + "&size=" + size;
        url = url.replace("+","%2B");
        return url;
    }


    /**
     * gets the relationship list url
     *
     * @param urlKey
     * @param user
     * @param userFriend
     * @param isLive
     * @param page
     * @param size
     * @return moment url
     */
    public String getMomentFeedsListUrl(int urlKey, String user, String userFriend, String isLive, String page, String size) {
        String url = getURL(urlKey) + user + "&viewee=" + userFriend + "&isLive=" + isLive + "&page=" + page + "&size=" + size;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     * gets the relationship list url
     *
     * @param urlKey
     * @param user
     * @param userFriend
     * @param isLive
     * @param page
     * @param size
     * @return moment url
     */
    public String getMomentTimelineListUrl(int urlKey, String user, String userFriend, String isLive, String page, String size) {
        String url = getURL(urlKey) + user + "&viewee=" + userFriend + "&isLive=" + isLive + "&page=" + page + "&size=" + size;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param momentUuid
     * @param person
     * @return
     */
    public String getMomentReportUrl(String momentUuid,String person) {
        String url = getURL(API_KEY_MOMENT_REPORT) + momentUuid + "&person=" + person;
        url = url.replace("+", "%2B");
        return url;
    }
    /**
     * @param uuid
     * @param username
     * @return
     */
    public String getMomentLikeUrl(int urlKey,String uuid, String username) {
        String url = getURL(urlKey) + uuid + "&user=" + username;
        url = url.replace("+","%2B");
        return url;
    }
    public String getMomentDeleteUrl(String uuid,String user) {
        String url = getURL(API_KEY_MOMENT_DELETE) +uuid+"&user="+user;;
        url = url.replace("+","%2B");
        return url;
    }


    /**
     * @param username
     * @return
     */
    public String getConctactList(String username) {
        String url = getURL(API_KEY_GET_PERSON_BY_PHONE_CONTACT) + username;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     * @param urlKey
     * @param username
     * @return
     */
    public String getFacebookAddList(int urlKey, String username) {
        String url = getURL(urlKey) + username;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     * @param urlKey
     * @param username
     * @return
     */
    public String getActivityList(int urlKey, String username, String page, String size) {
        String url = getURL(urlKey) + username + "&page=" + page + "&size=" + size;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     * gets the relationship list url
     *
     * @param urlKey
     * @param uuid
     * @param page
     * @param size
     * @return moment url
     */
    public String getCommentList(int urlKey, String uuid, String page, String size) {
        String url = getURL(urlKey) + uuid + "&page=" + page + "&size=" + size;
        url = url.replace("+","%2B");
        return url;
    }


    public String getLiveSubscriberTokenUrl(String sessionId,String viewerUuid) {
        String url = getURL(API_KEY_GET_SUBSCRIBER) + sessionId+"&viewer=" + viewerUuid;
        url = url.replace("+","%2B");
        return url;
    }

    public String getCloseLiveUrl(String pubSessionId,String subsessionId,String viewerUuid) {
        String url = getURL(API_KEY_LIVE_CLOSE)+pubSessionId +"&subsession="+ subsessionId+"&viewer=" + viewerUuid;
        url = url.replace("+","%2B");
        return url;
    }

    public String getDestroyLive(String subsessionId,String viewerUuid) {
        String url = getURL(API_KEY_LIVE_END)+subsessionId +"&viewer=" + viewerUuid;
        url = url.replace("+","%2B");
        return url;
    }
    public String getDestroyAllLive(String viewerUuid) {
        String url = getURL(API_KEY_LIVE_END_ALL)+ viewerUuid;
        url = url.replace("+","%2B");
        return url;
    }
    /**
     * @param urlKey
     * @param username
     * @return
     */
    public String getGroupListUrl(int urlKey, String username) {
        String url = getURL(urlKey) + username;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     * Edit group
     * @param userUuid
     * @return
     */
    public String getGroupEditUrl(String userUuid) {
        String url = getURL(API_KEY_GROUP_EDIT) + userUuid;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     * Delete group
     * @param userUuid
     * @return
     */
    public String getGroupDeleteUrl(String groupUuid,String userUuid) {
        String url = getURL(API_KEY_GROUP_DELETE)+groupUuid+"&person=" + userUuid;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     *Group Moment List
     * @param groupUuid
     * @param userUuid
     * @param isLive
     * @param page
     * @param size
     * @return
     */
    public String getGroupMomentListUrl(String groupUuid,String userUuid, String isLive, String page, String size) {
        String url = getURL(API_KEY_GROUP_MOMENT_LIST)+groupUuid+"&viewer=" + userUuid+ "&isLive=" + isLive + "&page=" + page + "&size=" + size;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     * Group Member list
     * @param groupUuid
     * @param userUuid
     * @param page
     * @param size
     * @return
     */
    public String getGroupMemberListUrl(String groupUuid,String userUuid, String page, String size) {
        String url = getURL(API_KEY_GROUP_MEMBER_LIST)+groupUuid+"&viewer=" + userUuid+ "&page=" + page + "&size=" + size;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param groupUuid
     * @return
     */
    public String getGroupMomentAddUrl(String groupUuid) {
        String url = getURL(API_KEY_GROUP_MOMENT_POST)+groupUuid;
        url = url.replace("+","%2B");
        return url;

    }

    public String getGroupMemberDeleteUrl(String groupUuid,String memberUuid,String deleterUuid) {
        String url = getURL(API_KEY_GROUP_MEMBER_DELETE)+groupUuid+"&member="+memberUuid+"&deleter="+deleterUuid;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param groupUuid
     * @param memberUuid
     * @return
     */
    public String getGroupMemberExitUrl(String groupUuid,String memberUuid) {
        String url = getURL(API_KEY_GROUP_EXIT)+groupUuid+"&member="+memberUuid;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param groupUuid
     * @param person
     * @return
     */
    public String getGroupMemberAddUrl(String groupUuid,String person) {
        String url = getURL(API_KEY_GROUP_MEMBER_ADD)+groupUuid+"&person="+person;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param groupUuid
     * @param member
     * @param assigner
     * @return
     */
    public String getGroupMemberAssignRoleUrl(String groupUuid,String member,String assigner) {
        String url = getURL(API_KEY_GROUP_MEMBER_ASSIGN_ROLE)+groupUuid+"&member="+member+"&assigner="+assigner;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param groupUuid
     * @param member
     * @param assigner
     * @return
     */
    public String getGroupMemberUnassignRoleUrl(String groupUuid,String member,String assigner) {
        String url = getURL(API_KEY_GROUP_MEMBER_UNASSIGN_ROLE)+groupUuid+"&member="+member+"&assigner="+assigner;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param groupUuid
     * @param currentOwner
     * @param newOwner
     * @return
     */
    public String getGroupOwnerChangeUrl(String groupUuid,String currentOwner,String newOwner) {
        String url = getURL(API_KEY_GROUP_OWNER_CHANGE)+groupUuid+"&currentOwner="+currentOwner+"&newOwner="+newOwner;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param groupUuid
     * @param viewer
     * @return
     */
    public String getGroupProfileUrl(String groupUuid,String viewer) {
        String url = getURL(API_KEY_GROUP_PROFILE)+groupUuid+"&viewer="+viewer;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param groupUuid
     * @param person
     * @return
     */
    public String getGroupJoinUrl(String groupUuid,String person) {
        String url = getURL(API_KEY_GROUP_JOIN)+groupUuid+"&person="+person;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param groupUuid
     * @param requester
     * @param accepter
     * @return
     */
    public String getGroupJoinAcceptUrl(String groupUuid,String requester,String accepter) {
        String url = getURL(API_KEY_GROUP_JOIN_ACCEPT)+groupUuid+"&requester="+requester+"&accepter="+accepter;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param groupUuid
     * @param requester
     * @param accepter
     * @return
     */
    public String getGroupJoinDenyUrl(String groupUuid,String requester,String accepter) {
        String url = getURL(API_KEY_GROUP_JOIN_DENY)+groupUuid+"&requester="+requester+"&accepter="+accepter;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param groupUuid
     * @param person
     * @return
     */
    public String getGroupJoinPendingRequestUrl(String groupUuid,String person) {
        String url = getURL(API_KEY_GROUP_JOIN_REQUEST)+groupUuid+"&viewer="+person;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param groupUuid
     * @param requester
     * @return
     */
    public String getGroupFollowRequestUrl(String groupUuid,String requester) {
        String url = getURL(API_KEY_GROUP_FOLLOW)+groupUuid+"&requester="+requester;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param groupUuid
     * @param requester
     * @return
     */
    public String getGroupUNFollowRequestUrl(String groupUuid,String requester) {
        String url = getURL(API_KEY_GROUP_UNFOLLOW)+groupUuid+"&requester="+requester;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     * @param page
     * @param size
     * @return
     */
    public String getSuggestListUrl(String userUuid,String page, String size) {
        String url = getURL(API_KEY_SUGGEST)+userUuid+"&page=" + page + "&size=" + size;
        url = url.replace("+","%2B");
        return url;

    }

    /**
     *
     * @param groupUuid
     * @param person
     * @return
     */
    public String getGroupReportUrl(String groupUuid,String person) {
        String url = getURL(API_KEY_GROUP_REPORT)+groupUuid+"&person="+person;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     *
     * @param callerUuid
     * @return
     */
    public String getMakeCallUrl(String callerUuid){
        String url = getURL(API_KEY_MAKE_CALL)+callerUuid;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     *
     * @param sessionId
     * @param calleeUuid
     * @return
     */
    public String getAcceptCallUrl(String sessionId,String calleeUuid){
        String url = getURL(API_KEY_ACCEPT_CALL)+sessionId+"&callee="+calleeUuid;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     *
     * @param sessionId
     * @param calleeUuid
     * @return
     */
    public String getDeclineCallUrl(String sessionId,String calleeUuid){
        String url = getURL(API_KEY_DECLINE_CALL)+sessionId+"&callee="+calleeUuid;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     *
     * @param sessionId
     * @param calleeUuid
     * @return
     */
    public String getEndCallUrl(String sessionId,String calleeUuid){
        String url = getURL(API_KEY_END_CALL)+sessionId+"&callee="+calleeUuid;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     *
     * @param sessionId
     * @param callerUuid
     * @return
     */
    public String getAddMemberToCallUrl(String sessionId,String callerUuid){
        String url = getURL(API_KEY_END_CALL)+sessionId+"&caller="+callerUuid;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     *
     * @param sessionId
     * @return
     */
    public String getLiveCountUrl(String sessionId){
        String url = getURL(API_KEY_LIVE_COUNT)+sessionId;
        url = url.replace("+","%2B");
        return url;
    }

    /**
     *
     * @param username
     * @return
     */
    public String getDeactivateAccountUrl(String username){
        String url = getURL(API_KEY_DEACTIVATE_ACCOUNT)+username;
        url = url.replace("+","%2B");
        return url;
    }

}
