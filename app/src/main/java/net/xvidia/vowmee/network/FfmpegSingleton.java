package net.xvidia.vowmee.network;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.network.model.UploadData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vasu on 29/7/16.
 */
public class FfmpegSingleton {

    private static FfmpegSingleton mInstance;
    public static FFmpeg ffmpeg;
    private List<FfmpegSingletonListener> mListeners;
    private static FfmpegSingletonListener ffmpegSingletonListener;

    public FfmpegSingleton() {
        mListeners = new ArrayList<>();
    }

    public static synchronized FfmpegSingleton getInstance() {
        if (mInstance == null) {
            mInstance = new FfmpegSingleton();
        }
        return mInstance;
    }


    public FFmpeg getFfmpeg(Context c) {
        if (ffmpeg == null) {
            createfFmpeg(c);
        }
        return ffmpeg;
    }

    public void createfFmpeg(final Context c) {
        try {
            ffmpeg = FFmpeg.getInstance(MyApplication.getAppContext());
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    showUnsupportedExceptionDialog(c);
                }
            });
        } catch (FFmpegNotSupportedException e) {
            showUnsupportedExceptionDialog(c);
        }
    }

    private void showUnsupportedExceptionDialog(Context context) {
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(context.getString(R.string.error_general))
                .setMessage(context.getString(R.string.error_general))
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        Home.this.finish();
                    }
                })
                .create()
                .show();

    }

   /* private void notifyState(FfmpegSingletonListener listener) {
        if (listener != null) {
            if (ffmpeg != null) {
                listener.onFfmpegSuccess();
            } else listener.onFfmpegError();
        }
    }*/

    public void addListener(FfmpegSingletonListener l) {

        mListeners.add(l);
        ffmpegSingletonListener = l;
//        notifyState(l);

    }

    public void execFFmpegBinary(final String[] command, final UploadData uploadData) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    if(ffmpegSingletonListener!=null)
                    ffmpegSingletonListener.onFfmpegError(uploadData);
                }

                @Override
                public void onSuccess(String s) {

                }

                @Override
                public void onProgress(String s) {

                }

                @Override
                public void onStart() {

                }

                @Override
                public void onFinish() {
                    if(ffmpegSingletonListener!=null)
                    ffmpegSingletonListener.onFfmpegSuccess(uploadData);
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // do nothing for now
        }
    }


    public void removeListener(FfmpegSingletonListener l) {
        mListeners.remove(l);
    }

    public interface FfmpegSingletonListener {
        void onFfmpegSuccess(UploadData uploadData);

        void onFfmpegError(UploadData uploadData);
    }
}
