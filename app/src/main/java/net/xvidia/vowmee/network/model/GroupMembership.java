package net.xvidia.vowmee.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GroupMembership {

	@JsonProperty("requester")private Profile requester;
	@JsonProperty("group")private GroupProfile group;
	@JsonProperty("uuid")private String uuid;
	@JsonProperty("isGranted")private boolean isGranted;
	@JsonProperty("isDenied")private boolean isDenied;
	@JsonProperty("groupRole")private String groupRole;
	@JsonProperty("accepterUuid")private String accepterUuid;
	@JsonProperty("denierUuid")private String denierUuid;
	@JsonProperty("requestedDate")private long requestedDate;

	public Profile getRequester() {
		return requester;
	}

	public void setRequester(Profile requester) {
		this.requester = requester;
	}

	public GroupProfile getGroup() {
		return group;
	}

	public void setGroup(GroupProfile group) {
		this.group = group;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public boolean isGranted() {
		return isGranted;
	}

	public void setGranted(boolean granted) {
		isGranted = granted;
	}

	public boolean isDenied() {
		return isDenied;
	}

	public void setDenied(boolean denied) {
		isDenied = denied;
	}

	public String getGroupRole() {
		return groupRole;
	}

	public void setGroupRole(String groupRole) {
		this.groupRole = groupRole;
	}

	public String getAccepterUuid() {
		return accepterUuid;
	}

	public void setAccepterUuid(String accepterUuid) {
		this.accepterUuid = accepterUuid;
	}

	public String getDenierUuid() {
		return denierUuid;
	}

	public void setDenierUuid(String denierUuid) {
		this.denierUuid = denierUuid;
	}

	public long getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(long requestedDate) {
		this.requestedDate = requestedDate;
	}
}
