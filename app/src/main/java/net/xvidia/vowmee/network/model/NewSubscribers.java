package net.xvidia.vowmee.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NewSubscribers {
    private Boolean isFollowingsChecked;
    private Boolean isFollowersChecked;
    private Boolean isFriendsChecked;
    private ArrayList<ContactData> selectUsers;

    public ArrayList<ContactData> getSelectUsers() {
        return selectUsers;
    }

    public void setSelectUsers(ArrayList<ContactData> selectUsers) {
        this.selectUsers = selectUsers;
    }

    public Boolean getIsFollowingsChecked() {
        return isFollowingsChecked;
    }

    public void setIsFollowingsChecked(Boolean isFollowingsChecked) {
        this.isFollowingsChecked = isFollowingsChecked;
    }

    public Boolean getIsFollowersChecked() {
        return isFollowersChecked;
    }

    public void setIsFollowersChecked(Boolean isFollowersChecked) {
        this.isFollowersChecked = isFollowersChecked;
    }

    public Boolean getIsFriendsChecked() {
        return isFriendsChecked;
    }

    public void setIsFriendsChecked(Boolean isFriendsChecked) {
        this.isFriendsChecked = isFriendsChecked;
    }

}
