package net.xvidia.vowmee.network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.amazonaws.AmazonClientException;
import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.network.model.MomentShare;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

/**
 * Created by Ravi_office on 18-Feb-16.
 */
public class UploadService extends IntentService {

    private static final String TAG = "UploadService";
    private String mCaptionEditText;
    private String mDescriptionAutoCompleteTextView;
    private String path;
    private String groupUuid;
    private boolean groupMoment;
    private String fileNameThumnail;
    private String workFolder = null;
    private String vowmeeFolder = null;
    private String compressedFolder = null;
    private String vkLogPath = null;
    private String compressedFilePath = null;
    private final int STOP_TRANSCODING_MSG = -1;
    FFmpeg ffmpeg;
    private boolean commandValidationFailedFlag = false;
    private NotificationManager mNotifyManager;
    private boolean recordedVideo;
    private Builder mBuilder;
    int notifId = 0;
    Context mContext;
    private static MomentShare momentShare;
    private boolean reshareFlag;

    public static MomentShare getMomentShare() {
        return momentShare;
    }

    public static void setMomentShare(MomentShare momentShare) {
        UploadService.momentShare = momentShare;
    }

    private String duration, height, width, framerate, bitrate;
    private static int defaultHeight = 360, defaultWidth = 640, defaultBitrate = 400, defaultFramerate = 24;
    private int intWidth, intHeight, intBitrate, intFramerate, intDuration;
    private MediaMetadataRetriever fetchVideoInfo;
    public UploadService() {
        super(TAG);
    }


    @Override
    protected void onHandleIntent(Intent intent) {

        try {

            initialiseNotif();
            if (intent != null) {
                mCaptionEditText = intent.getExtras().getString(AppConsatants.UPLOAD_FILE_CAPTION);
                mDescriptionAutoCompleteTextView = intent.getExtras().getString(AppConsatants.UPLOAD_FILE_DESCRIPTION);
                path = intent.getExtras().getString(AppConsatants.UPLOAD_FILE_PATH);
                groupUuid = intent.getExtras().getString(AppConsatants.UUID);
                groupMoment = intent.getExtras().getBoolean(AppConsatants.UPLOAD_MOMENT_GROUP);
                reshareFlag = intent.getBooleanExtra(AppConsatants.RESHARE,false);
                recordedVideo = intent.getBooleanExtra(AppConsatants.RECORDED_VIDEO,false);
                workFolder = getApplicationContext().getFilesDir().getAbsolutePath() + "/";
                vowmeeFolder = Environment.getExternalStorageDirectory().getAbsolutePath() + AppConsatants.FILE_VOWMEE;
                vkLogPath = workFolder + "vk.log";
                compressedFolder = vowmeeFolder + "compressed/";
                mContext = this;

            } else {
//                initialiseNotif();
                mBuilder.setContentText("Upload Failed");
                mBuilder.setProgress(0, 0, false);
                mBuilder.setSmallIcon(android.R.drawable.stat_sys_upload_done);
                mNotifyManager.notify(notifId, mBuilder.build());
                stopSelf();
            }
            if(momentShare==null){
//                initialiseNotif();
                mBuilder.setContentText("Upload Failed");
                mBuilder.setProgress(0, 0, false);
                mBuilder.setSmallIcon(android.R.drawable.stat_sys_upload_done);
                mNotifyManager.notify(notifId, mBuilder.build());
                stopSelf();
            }else if(momentShare.getNewMoment()==null){
//                initialiseNotif();
                mBuilder.setContentText("Upload Failed");
                mBuilder.setProgress(0, 0, false);
                mBuilder.setSmallIcon(android.R.drawable.stat_sys_upload_done);
                mNotifyManager.notify(notifId, mBuilder.build());
                stopSelf();
            }
            if(reshareFlag) {
//                initialiseNotif();
                momentShare.getNewMoment().setLink(momentShare.getOriginalMoment().getLink());
                momentShare.getNewMoment().setThumbNail(momentShare.getOriginalMoment().getThumbNail());
                sendMomentAddSuccess(momentShare);
            }else {
                loadFFMpegBinary();
                initialiseProgressBar();
            }
        } catch (Exception e) {
        }
    }

    private void loadFFMpegBinary() {
        try {
            ffmpeg = FFmpeg.getInstance(MyApplication.getAppContext());
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    showUnsupportedExceptionDialog();
                }
            });
        } catch (FFmpegNotSupportedException e) {
            showUnsupportedExceptionDialog();
        }
    }

    private void showUnsupportedExceptionDialog() {
        new AlertDialog.Builder(mContext)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.error_general))
                .setMessage(getString(R.string.error_general))
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        Home.this.finish();
                    }
                })
                .create()
                .show();

    }
    private void transcodeVideoFile(){
//
        File dir = new File(compressedFolder);
        if(!dir.exists()){
            dir.mkdirs();
        }
        compressedFilePath = compressedFolder+Utils.getInstance().generateUUIDStringDateTime() + AppConsatants.FILE_EXTENSION_MP4;
        //        String cmd = "ffmpeg -i "+path+" " + compressedFilePath;
        String cmd = "-y -i "+path+" -vcodec libx264 -profile:v main -preset veryfast -b:v 500k -minrate 200k -maxrate 500k -bufsize 1000k -threads 0 -acodec copy "+ compressedFilePath;
//        String cmd = "ffmpeg -i "+path+" -codec:v libx264 -profile:v main -preset veryfast -b:v 500k -maxrate 500k -bufsize 1000k -vf scale=-1:480 -threads 0 -codec:a libfdk_aac -b:a 128k " + compressedFilePath;
        if(recordedVideo)
            cmd = "-y -i "+path+" -vcodec libx264 -profile:v main -preset veryfast -b:v 500k -maxrate 500k -bufsize 1000k -vf scale=-1:480 -threads 0 -acodec copy "+ compressedFilePath;
        String[] command = cmd.split(" ");
        if (command.length != 0) {
            if(!ffmpeg.isFFmpegCommandRunning())
                execFFmpegBinary(command);
        } else {
//            Toast.makeText(UploadActivity.this, getString(R.string.error_general), Toast.LENGTH_LONG).show();
        }
    }


    private void execFFmpegBinary(final String[] command) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    Log.d(TAG, "Started command : ffmpeg "+s);
                    mBuilder.setProgress(0, 0, false);
                    mBuilder.setContentText("Upload failed ffmpeg");
                    mNotifyManager.notify(notifId, mBuilder.build());
                }

                @Override
                public void onSuccess(String s) {

                }

                @Override
                public void onProgress(String s) {
                    Log.d(TAG, "Started command : ffmpeg "+s);

//                    mBuilder.setProgress(0, 0, true);
//                    mNotifyManager.notify(notifId, mBuilder.build());
                }

                @Override
                public void onStart() {

                }

                @Override
                public void onFinish() {
                    uploadMoment();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // do nothing for now
        }
    }

    private void initialiseProgressBar() {
        if (Utils.getInstance().checkIfFileExistAndNotEmpty(path)) {
            transcodeVideoFile();
//            new TranscdingBackground(mContext).execute();
        } else {
            Utils.getInstance().displayToast(MyApplication.getAppContext(), path + " not found", Utils.SHORT_TOAST);
        }

        // Progress update thread
       /* new Thread() {
            ProgressCalculator pc = new ProgressCalculator(vkLogPath);
            public void run() {
                Log.d(Prefs.TAG,"Progress update started");
                int progress = -1;
                try {
                    while (true) {
                        sleep(300);
                        progress = pc.calcProgress();
                        if (progress != 0 && progress < 100) {
                            mBuilder.setProgress(100, progress, false);
                            mNotifyManager.notify(notifId, mBuilder.build());
                        }
                        else if (progress == 100) {
                            Log.i(Prefs.TAG, "==== progress is 100, exiting Progress update thread");
                            pc.initCalcParamsForNextInter();
                            break;
                        }
                    }

                } catch(Exception e) {
                    Log.e("threadmessage",e.getMessage());
                }
            }
        }.start();*/
    }

    private void initialiseNotif(){
        notifId = DataStorage.getInstance().getUniqueNotificationId();
        notifId=notifId+1;
        DataStorage.getInstance().setUniqueNotificationId(notifId);
        int color = getResources().getColor(R.color.appcolor);
        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle("Upload")
                .setContentText("Processing..")
                .setAutoCancel(false)
                .setColor(color)
                .setSmallIcon(android.R.drawable.stat_sys_upload);
        mBuilder.setTicker("Processing");
        mBuilder.setProgress(0, 0, true);
        mNotifyManager.notify(notifId, mBuilder.build());
    }
    private void uploadMoment() {
        try {
            UserFileManager userFileManager = Utils.getInstance().getUserFileManager();
            if(userFileManager == null)
                return;
//            String compressedFile = compressedFolder + "Compressed_" + Utils.getInstance().generateUUIDStringDateTime() + ".mp4";
            final File file = new File(compressedFilePath);
            Utils.getInstance().storeImageCache(Utils.getInstance().getVideoThumbnail(compressedFilePath),Utils.getInstance().getLocalMomentThumbnailImagePath());
            final File thumbnailFile = new File(Utils.getInstance().getLocalMomentThumbnailImagePath());

            final String uuidString = Utils.getInstance().generateUUIDString();
            String fileName = uuidString + AppConsatants.FILE_EXTENSION_MP4;
            fileNameThumnail = uuidString + AppConsatants.FILE_EXTENSION_JPG;

            userFileManager.uploadContent(thumbnailFile, fileNameThumnail, new ContentProgressListener() {
                @Override
                public void onSuccess(final ContentItem contentItem) {

                }

                @Override
                public void onProgressUpdate(final String fileName, final boolean isWaiting,
                                             final long bytesCurrent, final long bytesTotal) {
                }

                @Override
                public void onError(final String fileName, final Exception ex) {
                    fileNameThumnail = "";
                }
            });
            userFileManager.uploadContent(file, fileName, new ContentProgressListener() {
                @Override
                public void onSuccess(final ContentItem contentItem) {
//                    Log.i("FIle uploaded", contentItem.getFilePath() + " getUsername " + ModelManager.getInstance().getOwnProfile().getUsername());

//                    mNotifyManager.notify(notifId, mBuilder.build());
                    mNotifyManager.cancel(notifId);
//                    Moment moment = new Moment();
//                    moment.setUuid("");
//                    moment.setCaption(mCaptionEditText);
//                    moment.setDescription(mDescriptionAutoCompleteTextView);
//                    moment.setLink(contentItem.getFilePath());
//                    moment.setProfile(AppConsatants.PROFILE_PUBLIC);
//                    moment.setThumbNail(fileNameThumnail);
//                    moment.setIsLive(false);
//                    moment.setOwner(DataStorage.getInstance().getUsername());
                    momentShare.getNewMoment().setLink(contentItem.getFilePath());
                    momentShare.getNewMoment().setThumbNail(fileNameThumnail);

//                    Log.i("Moment uploaded", moment.toString());
                    sendMomentAddSuccess(momentShare);
                }

                @Override
                public void onProgressUpdate(final String fileName, final boolean isWaiting,
                                             final long bytesCurrent, final long bytesTotal) {

                    mBuilder.setContentText("Uploading..");
                    mBuilder.setSmallIcon(android.R.drawable.stat_sys_upload);
                    mBuilder.setTicker("Uploading..");
                    mBuilder.setProgress((int) bytesTotal, (int)bytesCurrent, false);
                    mNotifyManager.notify(notifId, mBuilder.build());
                }

                @Override
                public void onError(final String fileName, final Exception ex) {
//                    showError(R.string.app_name, getString(R.string.error_upload_failed), null);

                    mBuilder.setContentText("Upload Failed");
                    mBuilder.setSmallIcon(android.R.drawable.stat_sys_upload_done);
                    mBuilder.setProgress(0, 0, false);
                    mNotifyManager.notify(notifId, mBuilder.build());
                    stopSelf();
                }
            });
        } catch (Exception e) {

        }
    }

    private void deleteMoment(Moment obj) {
        try {
            UserFileManager userFileManager = Utils.getInstance().getUserFileManager();
            if(userFileManager == null)
                return;
            if(obj == null)
                return;
            userFileManager.deleteRemoteContent(obj.getThumbNail(), new UserFileManager.ResponseHandler() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(AmazonClientException exception) {

                }
            });
            userFileManager.deleteRemoteContent(obj.getLink(), new UserFileManager.ResponseHandler() {
                @Override
                public void onSuccess() {
                    stopSelf();
                }

                @Override
                public void onError(AmazonClientException exception) {
                    stopSelf();
                }
            });
        } catch (Exception e) {

        }
    }
    private void sendMomentAddSuccess(final MomentShare moment){
        try {
            if(moment== null)
                return;
            String url =ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_MOMENT_ADD);

//            if(groupMoment && groupUuid !=null){
//                url = ServiceURLManager.getInstance().getGroupMomentAddUrl(groupUuid);
//            }

//            showProgressBar();
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(moment);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Add moment",jsonObject);
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Moment obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Moment.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    hideProgressBar();
                    if(obj!=null) {
                        String uuid = obj.getUuid();
                        if (uuid !=null &&!uuid.isEmpty()) {
//                            Intent mIntent = new Intent(UploadActivity.this, TimelineProfile.class);
//                            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            startActivity(mIntent);
//                            finish();
                            mBuilder.setContentText("Upload successful");
                            mBuilder.setSmallIcon(android.R.drawable.stat_sys_upload_done);
                            mBuilder.setProgress(100, 100, false);
                            mNotifyManager.notify(notifId, mBuilder.build());
                            String username = DataStorage.getInstance().getUsername();
                            String url = ServiceURLManager.getInstance().getMomentFeedsListUrl(IAPIConstants.API_KEY_GET_MOMENT_FEEDS_TIMELINE_LIST, username, username, AppConsatants.FALSE, "0", "0");

                            String url1 = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, username, username, AppConsatants.FALSE, "0", "0");
                            VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(url1, true);
                            VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(url,false);
                            mNotifyManager.cancel(notifId);
                        }

                    }
                    stopSelf();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    deleteMoment(moment.getNewMoment());
                    mBuilder.setContentText("Upload Failed");
                    mBuilder.setSmallIcon(android.R.drawable.stat_sys_upload_done);
                    mBuilder.setProgress(0, 0, false);
                    mNotifyManager.notify(notifId, mBuilder.build());
                    stopSelf();
//                    hideProgressBar();
//                    showError(R.string.app_name, getString(R.string.error_upload_failed), null);
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public class TranscdingBackground extends AsyncTask<String, Integer, Integer>
    {

        //        ProgressDialog progressDialog;
        Context _act;

        public TranscdingBackground (Context act) {
            _act = act;
        }



        @Override
        protected void onPreExecute() {
          /*  progressDialog = new ProgressDialog(_act);
            progressDialog.setMessage("please wait while we process your video.");
            progressDialog.show();*/

        }

        protected Integer doInBackground(String... paths) {
//            Log.i(Prefs.TAG, "doInBackground started...");
            fetchVideoInfo = new MediaMetadataRetriever();
            fetchVideoInfo.setDataSource(path);
            bitrate = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE);
            duration = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            height = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
            width = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
            framerate = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_CAPTURE_FRAMERATE);


            if(width != null){
                intWidth = Utils.getInstance().getIntegerValueOfString(width);
                intHeight = Utils.getInstance().getIntegerValueOfString(height);
                if(intHeight<intWidth) {
                    if (intWidth >= defaultWidth) {

                        intHeight = getHeight(intWidth, intHeight);
                        intWidth = defaultWidth;
                        height = Integer.toString(intHeight);
                        width = Integer.toString(intWidth);
                    }
                }else{
                    defaultHeight = 640;
                    defaultWidth = 360;
                    if (intWidth >= defaultHeight) {

                        intWidth = getWidth(intWidth, intHeight);
                        intHeight = defaultHeight;
                        height = Integer.toString(intHeight);
                        width = Integer.toString(intWidth);
                    }
                }
            }


            if(bitrate != null){
                intBitrate = Integer.parseInt(bitrate);
                intBitrate = intBitrate/8192; //converting bits/sec to kb/sec
                if(intBitrate >= defaultBitrate) {
                    bitrate = Integer.toString(defaultBitrate);
                }else{
                    bitrate = Integer.toString(intBitrate);
                }
            }

            if(framerate != null){
                intFramerate = Integer.parseInt(framerate);
                if(intFramerate >= defaultFramerate) {
                    framerate = Integer.toString(defaultBitrate);
                }
            }
            if(duration != null){
                intDuration = Integer.parseInt(duration);
                intDuration = intDuration/1000; // converting ms to sec
            }
            int compressedVideoSize = intBitrate*8*intDuration;
            String newVideoSize = Integer.toString(compressedVideoSize);
            Log.i("compressedVideoSize", newVideoSize);
            PowerManager powerManager = (PowerManager)_act.getSystemService(Activity.POWER_SERVICE);
            PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "VK_LOCK");
//            Log.d(Prefs.TAG, "Acquire wake lock");
            wakeLock.acquire();
            File dir = new File(compressedFolder);
            if(!dir.exists()){
                dir.mkdirs();
            }
            String commandStr = null;
            compressedFilePath = compressedFolder+Utils.getInstance().generateUUIDStringDateTime() + AppConsatants.FILE_EXTENSION_MP4;
            if(width != null && framerate != null && bitrate != null) {
                commandStr = "ffmpeg -y -i " + path + " -strict experimental -s " + width + "x" + height + " -r " + framerate + " -vcodec mpeg4 -b " + bitrate + "k -ab 48000 -ac 2 -ar 22050 " + compressedFilePath;
            }else if(width == null && framerate ==null){
                commandStr = "ffmpeg -y -i " + path + " -strict experimental -vcodec mpeg4 -b " + bitrate + "k -ab 48000 -ac 2 -ar 22050 " + compressedFilePath;

            }else if(width == null && bitrate ==null){
                commandStr = "ffmpeg -y -i " + path + " -strict experimental -r " + framerate + " -vcodec mpeg4 -ab 48000 -ac 2 -ar 22050 " + compressedFilePath;

            }else if(framerate == null && bitrate ==null){
                commandStr = "ffmpeg -y -i " + path + " -strict experimental -s " + width + "x" + height +" -vcodec mpeg4 -ab 48000 -ac 2 -ar 22050 " + compressedFilePath;

            }else if(width == null) {
                commandStr = "ffmpeg -y -i " + path + " -strict experimental -r " + framerate + " -vcodec mpeg4 -b " + bitrate + "k -ab 48000 -ac 2 -ar 22050 " + compressedFilePath;
            }
            else if(framerate == null){
                commandStr = "ffmpeg -y -i " + path + " -strict experimental -s " + width + "x" + height +" -vcodec mpeg4 -b " + bitrate + "k -ab 48000 -ac 2 -ar 22050 " + compressedFilePath;
//                commandStr = "ffmpeg -y -i " + path + " -strict experimental -s " + width + "x" + height +" -vcodec mpeg4 -ab 48000 -ac 2 -ar 22050 " + compressedFilePath;

            }else if(bitrate == null){
                commandStr = "ffmpeg -y -i " + path + " -strict experimental -s " + width + "x" + height + " -r " + framerate + " -vcodec mpeg4 -ab 48000 -ac 2 -ar 22050 " + compressedFilePath;
            }
//            vk = new LoadJNI();
//
//            try {
//
//                // complex command
//                //vk.run(complexCommand, workFolder, getApplicationContext());
//
//                vk.run(GeneralUtils.utilConvertToComplex(commandStr), workFolder, getApplicationContext());
////                vk.run(complexCommand, workFolder, getApplicationContext());
//
//                // copying vk.log (internal native log) to the videokit folder
//                GeneralUtils.copyFileToFolder(vkLogPath, vowmeeFolder);
//
////                vk.run(watermarkCommand, workFolder, getApplicationContext());
////                GeneralUtils.copyFileToFolder(vkLogPath, vowmeeFolder);
//
//            } catch (CommandValidationException e) {
//                Log.e(Prefs.TAG, "vk run exeption.", e);
//                commandValidationFailedFlag = true;
//
//            } catch(Exception e) {
//                e.printStackTrace();
//            }
//            catch(Throwable e) {
//                e.printStackTrace();
//                Log.e(Prefs.TAG, "vk run exeption.", e);
//            }
//            finally {
//                if (wakeLock.isHeld())
//                    wakeLock.release();
//                else{
//                    Log.i(Prefs.TAG, "Wake lock is already released, doing nothing");
//                }
////                File original = new File(path);
////                if(original.exists()){
////                    original.delete();
////                }
//            }
//            Log.i(Prefs.TAG, "doInBackground finished");
            return Integer.valueOf(0);
        }

        public int getHeight( int width, int height){
            int newHeight = height;
            try {
                newHeight = (640*height)/width;
            }catch (NumberFormatException e){

            } catch(Exception e) {
                e.printStackTrace();
            }
            return newHeight;
        }

        public int getWidth( int width, int height){
            int newWidth = width;
            try {
                newWidth = (640*width)/height;
            }catch (NumberFormatException e){

            } catch(Exception e) {
                e.printStackTrace();
            }
            return newWidth;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected void onCancelled() {
//            Log.i(Prefs.TAG, "onCancelled");
            //progressDialog.dismiss();
            super.onCancelled();
        }


        @Override
        protected void onPostExecute(Integer result) {
            Log.i("onPostExecute", "onPostExecute");
//            progressDialog.dismiss();
            super.onPostExecute(result);

            // finished Toast
            String rc = null;
            if (commandValidationFailedFlag) {
                rc = "Command Vaidation Failed";
            }
            else {
//                rc = GeneralUtils.getReturnCodeFromLog(vkLogPath);
            }
            final String status = rc;
            if (status.equals("Transcoding Status: Failed")) {

                mBuilder.setProgress(0, 0, false);
                mBuilder.setContentText("Upload failed");
                mBuilder.setSmallIcon(android.R.drawable.stat_sys_upload_done);
                mNotifyManager.notify(notifId, mBuilder.build());
            }else{
                uploadMoment();
            }
        }

    }
    // [END subscribe_topics]

}