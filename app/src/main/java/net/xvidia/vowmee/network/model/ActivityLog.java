package net.xvidia.vowmee.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivityLog {

	private String uuid;
	private Long date;
	private String initiaterId;
	private String initiaterName;
	private String initiatorThumbNail;
	private String receiverId;
	private String receiverName;
	private String receiverThumbNail;
	private String momentId;
	private String momentName;
	private String momentCaption;
	private String momentLink;
	private String momentThumbNail;
	private String momentApiKey;
	private String momentSessionId;
	private String momentToken;
	private String activityType;

	private String groupId; //uuid
	private String groupName;
	private String groupThumbNail;
	private String callSessionId;
	private String memberId;
	private String memberName;
	private String memberThumbNail;

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberThumbNail() {
		return memberThumbNail;
	}

	public void setMemberThumbNail(String memberThumbNail) {
		this.memberThumbNail = memberThumbNail;
	}

	public String getCallSessionId() {
		return callSessionId;
	}

	public void setCallSessionId(String callSessionId) {
		this.callSessionId = callSessionId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupThumbNail() {
		return groupThumbNail;
	}

	public void setGroupThumbNail(String groupThumbNail) {
		this.groupThumbNail = groupThumbNail;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public String getInitiaterId() {
		return initiaterId;
	}

	public void setInitiaterId(String initiaterId) {
		this.initiaterId = initiaterId;
	}

	public String getInitiaterName() {
		return initiaterName;
	}

	public void setInitiaterName(String initiaterName) {
		this.initiaterName = initiaterName;
	}

	public String getInitiatorThumbNail() {
		return initiatorThumbNail;
	}

	public void setInitiatorThumbNail(String initiatorThumbNail) {
		this.initiatorThumbNail = initiatorThumbNail;
	}

	public String getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverThumbNail() {
		return receiverThumbNail;
	}

	public void setReceiverThumbNail(String receiverThumbNail) {
		this.receiverThumbNail = receiverThumbNail;
	}

	public String getMomentId() {
		return momentId;
	}

	public void setMomentId(String momentId) {
		this.momentId = momentId;
	}

	public String getMomentName() {
		return momentName;
	}

	public void setMomentName(String momentName) {
		this.momentName = momentName;
	}

	public String getMomentCaption() {
		return momentCaption;
	}

	public void setMomentCaption(String momentCaption) {
		this.momentCaption = momentCaption;
	}

	public String getMomentLink() {
		return momentLink;
	}

	public void setMomentLink(String momentLink) {
		this.momentLink = momentLink;
	}

	public String getMomentThumbNail() {
		return momentThumbNail;
	}

	public void setMomentThumbNail(String momentThumbNail) {
		this.momentThumbNail = momentThumbNail;
	}

	public String getMomentApiKey() {
		return momentApiKey;
	}

	public void setMomentApiKey(String momentApiKey) {
		this.momentApiKey = momentApiKey;
	}

	public String getMomentSessionId() {
		return momentSessionId;
	}

	public void setMomentSessionId(String momentSessionId) {
		this.momentSessionId = momentSessionId;
	}

	public String getMomentToken() {
		return momentToken;
	}

	public void setMomentToken(String momentToken) {
		this.momentToken = momentToken;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
}
