package net.xvidia.vowmee.network.model;

/**
 * Created by Ravi_office on 15-Feb-16.
 */
    public class DigitAuth {

    String name;
    String authenticationStatus;

    public DigitAuth() {
    }

    public DigitAuth(String name, String authenticationStatus) {
        this.name = name;
        this.authenticationStatus = authenticationStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthenticationStatus() {
        return authenticationStatus;
    }

    public void setAuthenticationStatus(String authenticationStatus) {
        this.authenticationStatus = authenticationStatus;
    }

    @Override
    public String toString() {
        return "DigitAuthVo{" +
                "name='" + name + '\'' +
                ", authenticationStatus='" + authenticationStatus + '\'' +
                '}';
    }
}
