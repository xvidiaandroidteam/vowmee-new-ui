package net.xvidia.vowmee.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Ravi_office on 23-Nov-15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FriendRequest {
    private String requestedPersonUsername;
    private String requestingPersonUsername;

    public String getRequestedPersonUsername() {
        return requestedPersonUsername;
    }

    public void setRequestedPersonUsername(String requestedPersonUsername) {
        this.requestedPersonUsername = requestedPersonUsername;
    }

    public String getRequestingPersonUsername() {
        return requestingPersonUsername;
    }

    public void setRequestingPersonUsername(String requestingPersonUsername) {
        this.requestingPersonUsername = requestingPersonUsername;
    }
}
