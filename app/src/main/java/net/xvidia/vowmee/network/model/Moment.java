package net.xvidia.vowmee.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Moment {


	private String uuid;
	private String name;
	private String caption;
	private String description;
	private String thumbNail;
	private String link;
	private long date;
	private String profile;
	private String owner;
	private String hashValue;

	private String status;
	private boolean isLive;
	private String apiKey;
	private String sessionId;
	private String token;
	private long viewCount;
	private long viewerCount;

	private long likes;
	private boolean isLikedBy;
	private String ownerThumbNail;
	private String ownerUuid;
	private String ownerDisplayName;
	private long commentCount;
	private long shareCount;
	private String indexPosition;

	private String groupUuid;
	private String groupName;
	private String groupThumbNail;
	private long reportAbuseCount;
	private boolean reportedAsAbuse;
	private Double latitude;
	private Double longitude;
	private Boolean liveChatOn;

	public Boolean getOwnerBlocked() {
		return ownerBlocked;
	}

	public void setOwnerBlocked(Boolean ownerBlocked) {
		this.ownerBlocked = ownerBlocked;
	}

	private Boolean ownerBlocked;
	public Moment(){

	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Boolean getLiveChatOn() {
		return liveChatOn;
	}

	public void setLiveChatOn(Boolean liveChatOn) {
		this.liveChatOn = liveChatOn;
	}

	public String getOwnerUuid() {
		return ownerUuid;
	}

	public void setOwnerUuid(String ownerUuid) {
		this.ownerUuid = ownerUuid;
	}

	public long getReportAbuseCount() {
		return reportAbuseCount;
	}

	public void setReportAbuseCount(long reportAbuseCount) {
		this.reportAbuseCount = reportAbuseCount;
	}

	public boolean isReportedAsAbuse() {
		return reportedAsAbuse;
	}

	public void setReportedAsAbuse(boolean reportedAsAbuse) {
		this.reportedAsAbuse = reportedAsAbuse;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getHashValue() {
		return hashValue;
	}

	public void setHashValue(String hashValue) {
		this.hashValue = hashValue;
	}



	public Moment(String uuid, String name, String caption, String description, String thumbNail, String link, long date, String profile, String owner, Boolean isLive, String apiKey, String sessionId, String token, long viewCount, long viewerCount, long likes, Boolean isLikedBy, String ownerThumbNail, String ownerDisplayName, long commentCount, long shareCount, String indexPosition) {
		this.uuid = uuid;
		this.name = name;
		this.caption = caption;
		this.description = description;
		this.thumbNail = thumbNail;
		this.link = link;
		this.date = date;
		this.profile = profile;
		this.owner = owner;
		this.isLive = isLive;
		this.apiKey = apiKey;
		this.sessionId = sessionId;
		this.token = token;
		this.viewCount = viewCount;
		this.viewerCount = viewerCount;
		this.likes = likes;
		this.isLikedBy = isLikedBy;
		this.ownerThumbNail = ownerThumbNail;
		this.ownerDisplayName = ownerDisplayName;
		this.commentCount = commentCount;
		this.shareCount = shareCount;
		this.indexPosition = indexPosition;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getThumbNail() {
		return thumbNail;
	}

	public void setThumbNail(String thumbNail) {
		this.thumbNail = thumbNail;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}



	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public long getViewCount() {
		return viewCount;
	}

	public void setViewCount(long viewCount) {
		this.viewCount = viewCount;
	}

	public long getViewerCount() {
		return viewerCount;
	}

	public void setViewerCount(long viewerCount) {
		this.viewerCount = viewerCount;
	}

	public long getLikes() {
		return likes;
	}

	public void setLikes(long likes) {
		this.likes = likes;
	}


	public String getOwnerThumbNail() {
		return ownerThumbNail;
	}

	public void setOwnerThumbNail(String ownerThumbNail) {
		this.ownerThumbNail = ownerThumbNail;
	}

	public String getOwnerDisplayName() {
		return ownerDisplayName;
	}

	public void setOwnerDisplayName(String ownerDisplayName) {
		this.ownerDisplayName = ownerDisplayName;
	}

	public long getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(long commentCount) {
		this.commentCount = commentCount;
	}

	public long getShareCount() {
		return shareCount;
	}

	public void setShareCount(long shareCount) {
		this.shareCount = shareCount;
	}

	public String getIndexPosition() {
		return indexPosition;
	}

	public void setIndexPosition(String indexPosition) {
		this.indexPosition = indexPosition;
	}

	public boolean getIsLive() {
		return isLive;
	}

	public void setIsLive(boolean isLive) {
		this.isLive = isLive;
	}

	public boolean getIsLikedBy() {
		return isLikedBy;
	}

	public void setIsLikedBy(boolean isLikedBy) {
		this.isLikedBy = isLikedBy;
	}

	public String getGroupThumbNail() {
		return groupThumbNail;
	}

	public void setGroupThumbNail(String groupThumbNail) {
		this.groupThumbNail = groupThumbNail;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupUuid() {
		return groupUuid;
	}

	public void setGroupUuid(String groupUuid) {
		this.groupUuid = groupUuid;
	}

	@Override
	public String toString() {
		return "Moment{" +
				"uuid='" + uuid + '\'' +
				", name='" + name + '\'' +
				", caption='" + caption + '\'' +
				", description='" + description + '\'' +
				", thumbNail='" + thumbNail + '\'' +
				", link='" + link + '\'' +
				", date=" + date +
				", profile='" + profile + '\'' +
				", owner='" + owner + '\'' +
				", isLive=" + isLive +
				", apiKey='" + apiKey + '\'' +
				", sessionId='" + sessionId + '\'' +
				", token='" + token + '\'' +
				", viewCount=" + viewCount +
				", viewerCount=" + viewerCount +
				", likes=" + likes +
				", isLikedBy=" + isLikedBy +
				", ownerThumbNail='" + ownerThumbNail + '\'' +
				", ownerDisplayName='" + ownerDisplayName + '\'' +
				", commentCount=" + commentCount +
				", shareCount=" + shareCount +
				", indexPosition='" + indexPosition + '\'' +
				", groupUuid='" + groupUuid + '\'' +
				", groupName='" + groupName + '\'' +
				", groupThumbNail='" + groupThumbNail + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Moment moment = (Moment) o;

		return uuid.equals(moment.uuid);

	}

	@Override
	public int hashCode() {
		return uuid.hashCode();
	}
}
