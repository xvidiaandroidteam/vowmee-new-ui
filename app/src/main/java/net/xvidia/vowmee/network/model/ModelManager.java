package net.xvidia.vowmee.network.model;

import java.util.List;

public class ModelManager {
	private static ModelManager instance = null;
	public static ModelManager getInstance() {
		if (instance == null) {
			instance = new ModelManager();
		}
		return instance;
	}

	private ModelManager(){

	}
	private Profile ownProfile;
	private Profile otherProfile;
	private GroupProfile groupProfile;
	private List<Profile> pendingFriendList;
	private List<Profile> pendingFollowList;
	private List<Profile> facebookFriendList;
	private List<ActivityLog> othersActivity;
	private List<ActivityLog> ownActivity;

	public GroupProfile getGroupProfile() {
		return groupProfile;
	}

	public void setGroupProfile(GroupProfile groupProfile) {
		this.groupProfile = groupProfile;
	}

	public List<ActivityLog> getOthersActivity() {
		return othersActivity;
	}

	public void setOthersActivity(List<ActivityLog> othersActivity) {
		this.othersActivity = othersActivity;
	}

	public List<ActivityLog> getOwnActivity() {
		return ownActivity;
	}

	public void setOwnActivity(List<ActivityLog> ownActivity) {
		this.ownActivity = ownActivity;
	}

	public List<Profile> getFacebookFriendList() {
		return facebookFriendList;
	}

	public void setFacebookFriendList(List<Profile> facebookFriendList) {
		this.facebookFriendList = facebookFriendList;
	}

	public List<Profile> getPendingFollowList() {
		return pendingFollowList;
	}

	public void setPendingFollowList(List<Profile> pendingFollowList) {
		this.pendingFollowList = pendingFollowList;
	}

	public List<Profile> getPendingFriendList() {
		return pendingFriendList;
	}

	public void setPendingFriendList(List<Profile> pendingFriendList) {
		this.pendingFriendList = pendingFriendList;
	}

	public Profile getOwnProfile() {
		return ownProfile;
	}

	public void setOwnProfile(Profile ownPrfile) {
		ownProfile = ownPrfile;
	}

	public Profile getOtherProfile() {
		return otherProfile;
	}

	public void setOtherProfile(Profile otherPrfile) {
		otherProfile = otherPrfile;
	}





}