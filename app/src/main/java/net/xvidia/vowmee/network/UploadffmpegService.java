package net.xvidia.vowmee.network;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.amazonaws.mobile.content.ContentItem;
import com.amazonaws.mobile.content.ContentProgressListener;
import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;

import net.xvidia.vowmee.MyApplication;
import net.xvidia.vowmee.R;
import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.CheckNetworkConnection;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.broadcastreceiver.NetworkStateReceiver;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.network.model.MomentShare;
import net.xvidia.vowmee.network.model.UploadData;
import net.xvidia.vowmee.storage.DataStorage;
import net.xvidia.vowmee.storage.sqlite.manager.DataCacheManager;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Ravi_office on 18-Feb-16.
 */
public class UploadffmpegService extends Service implements FfmpegSingleton.FfmpegSingletonListener {

    private String path;
    //    private String groupUuid;
//    private boolean groupMoment;
    private String fileNameThumnail;
    private String vowmeeFolder = null;
    private String compressedFolder = null;
    //    private String vkLogPath = null;
    private String compressedFilePath = null;
    //    private final int STOP_TRANSCODING_MSG = -1;
    FFmpeg ffmpeg;
    private boolean commandValidationFailedFlag = false;
    private NotificationManager mNotifyManager;
    private boolean recordedVideo;
    private Builder mBuilder;
    final static int notifId = 989;
    Context mContext;
    private static MomentShare momentShare;
    private boolean reshareFlag;
    private String message = "";
    private String queueMessage = "";
    private static int nextData;
    private static boolean uploading;
    private NetworkStateReceiver networkStateReceiver;
    private static Boolean networkRestarted = false;
    private FfmpegSingleton ffmpegSingleton;
//    private UploadData uploadData2;


    public static MomentShare getMomentShare() {
        return momentShare;
    }

    public static void setMomentShare(MomentShare momentShare) {
        UploadffmpegService.momentShare = momentShare;
    }

    //    private static int defaultHeight = 360, defaultWidth = 640, defaultBitrate = 400, defaultFramerate = 24;
//    private int intWidth, intHeight, intBitrate, intFramerate, intDuration;
    private MediaMetadataRetriever fetchVideoInfo;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        onHandleIntent(intent);

        return Service.START_REDELIVER_INTENT;
    }


    private void onHandleIntent(Intent intent) {

        try {
            initialiseNotif();
            ffmpegSingleton = new FfmpegSingleton();
            ffmpegSingleton.addListener(this);

            if (intent != null) {
//                mCaptionEditText = intent.getExtras().getString(AppConsatants.UPLOAD_FILE_CAPTION);
//                mDescriptionAutoCompleteTextView = intent.getExtras().getString(AppConsatants.UPLOAD_FILE_DESCRIPTION);
                path = intent.getExtras().getString(AppConsatants.UPLOAD_FILE_PATH);
//                groupUuid = intent.getExtras().getString(AppConsatants.UUID);
//                groupMoment = intent.getExtras().getBoolean(AppConsatants.UPLOAD_MOMENT_GROUP);
                reshareFlag = intent.getBooleanExtra(AppConsatants.RESHARE, false);
                recordedVideo = intent.getBooleanExtra(AppConsatants.RECORDED_VIDEO, false);
//                workFolder = getApplicationContext().getFilesDir().getAbsolutePath() + "/";
                vowmeeFolder = Environment.getExternalStorageDirectory().getAbsolutePath() + AppConsatants.FILE_VOWMEE;
//                vkLogPath = workFolder + "vk.log";
                compressedFolder = vowmeeFolder + "compressed/";
                networkRestarted = intent.getBooleanExtra(AppConsatants.NETWORK_RESTART, false);
                mContext = this;

            } else {
//                initialiseNotif();
                mBuilder.setContentText(MyApplication.getAppContext().getString(R.string.error_upload_failed));
                mBuilder.setProgress(0, 0, false);
                mNotifyManager.notify(notifId, mBuilder.build());
                ffmpegSingleton.removeListener(this);
                stopSelf();

            }
            if (networkRestarted) {
                checkForScheduledUploads(0);
            } else {
                if (momentShare == null) {
//                initialiseNotif();
                    mBuilder.setContentText(MyApplication.getAppContext().getString(R.string.error_upload_failed));
                    mBuilder.setProgress(0, 0, false);
                    mNotifyManager.notify(notifId, mBuilder.build());
                    ffmpegSingleton.removeListener(this);
                    stopSelf();
                } else if (momentShare.getNewMoment() == null) {
//                initialiseNotif();
                    mBuilder.setContentText(MyApplication.getAppContext().getString(R.string.error_upload_failed));
                    mBuilder.setProgress(0, 0, false);
                    mNotifyManager.notify(notifId, mBuilder.build());
                    ffmpegSingleton.removeListener(this);
                    stopSelf();
                }
                if (reshareFlag) {
//                initialiseNotif();
                    momentShare.getNewMoment().setLink(momentShare.getOriginalMoment().getLink());
                    momentShare.getNewMoment().setThumbNail(momentShare.getOriginalMoment().getThumbNail());
                    UploadData uploadData = new UploadData();
                    String jsonString = getJsonObectString(momentShare);
                    uploadData.setmReshare(reshareFlag);
                    uploadData.setMomentShareText(jsonString);
                    uploadData.setMomentUuid(Utils.getInstance().generateUUIDString());
                    DataCacheManager.getInstance().saveUploadData(uploadData);
                    sendMomentAddSuccess(momentShare, uploadData.getMomentUuid());
//                    uploadData2 = uploadData;
                } else {
                    if (recordedVideo) {
                        ffmpeg = FfmpegSingleton.getInstance().getFfmpeg(UploadffmpegService.this);

                    }
                    compressedFilePath = compressedFolder + Utils.getInstance().generateUUIDStringDateTime() + AppConsatants.FILE_EXTENSION_MP4;
                    File dir = new File(compressedFolder);
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }

                    UploadData uploadData = new UploadData();
                    String jsonString = getJsonObectString(momentShare);
                    uploadData.setmReshare(reshareFlag);
                    uploadData.setMomentShareText(jsonString);
                    uploadData.setRecordedMoment(recordedVideo);
                    uploadData.setMomentCompressedPath(compressedFilePath);
                    uploadData.setMomentPath(path);
                    uploadData.setMomentUuid(Utils.getInstance().generateUUIDString());
                    DataCacheManager.getInstance().saveUploadData(uploadData);
                    initialiseProgressBar(uploadData);

                }
            }

        } catch (Exception e) {
        }
    }



    private void transcodeVideoFile(UploadData obj) {
        String compressedFilePath = obj.getMomentCompressedPath();
        String path = obj.getMomentPath();
        String uuid = obj.getMomentUuid();
        boolean recordedVideo = obj.getRecordedMoment();

//        fetchVideoInfo = new MediaMetadataRetriever();
//        fetchVideoInfo.setDataSource(path);
//        videoRotation = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
//        videoHeight = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
//        videoWidth = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
//        Log.e("video height: ", videoHeight);
//        Log.e("video width: ", videoWidth);
//        Log.e("video Rotation: ", videoRotation);

        String cmd = "-y -i " + path + " -vcodec libx264 -profile:v main -preset veryfast -b:v 500k -minrate 200k -maxrate 500k -bufsize 1000k -threads 0 -acodec copy " + compressedFilePath;
//        String cmd = "ffmpeg -i "+path+" -codec:v libx264 -profile:v main -preset veryfast -b:v 500k -maxrate 500k -bufsize 1000k -vf scale=-1:480 -threads 0 -codec:a libfdk_aac -b:a 128k " + compressedFilePath;
        if (recordedVideo) {
            cmd = "-y -i " + path + " -vcodec libx264 -profile:v main -preset veryfast -b:v 500k -minrate 200k -maxrate 500k -bufsize 1000k -vf scale=-1:480 -threads 0 -acodec copy " + compressedFilePath;
            String[] command = cmd.split(" ");
            if (command.length != 0) {
                if (!ffmpeg.isFFmpegCommandRunning()) {
                    MomentShare momentShare = getMomentShareObj(obj.getMomentShareText());
                    if (momentShare != null)
                        mBuilder.setContentText(momentShare.getNewMoment().getCaption());
                    mBuilder.setSmallIcon(android.R.drawable.stat_sys_upload);
                    mBuilder.setProgress(0, 0, true);
                    mNotifyManager.notify(notifId, mBuilder.build());
//                    execFFmpegBinary(command, obj);
//                    uploadData2 = obj;
                    FfmpegSingleton.getInstance().execFFmpegBinary(command, obj);

                }
            } else {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), "FFmpeg command is already running", Utils.SHORT_TOAST);

//            Toast.makeText(UploadActivity.this, getString(R.string.error_general), Toast.LENGTH_LONG).show();
            }
        } else {
            if(!uploadMoment(obj)){
                checkForScheduledUploads(1);
            }
        }
    }


    private void initialiseProgressBar(UploadData data) {
        if (Utils.getInstance().checkIfFileExistAndNotEmpty(data.getMomentPath())) {
            transcodeVideoFile(data);
        } else {
            Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_upload_failed), Utils.SHORT_TOAST);
            DataCacheManager.getInstance().removeUploadData(data.getMomentUuid());
            checkForScheduledUploads(1);
        }

    }

    private void initialiseNotif() {
        if (!uploading || mBuilder == null) {

//            notifId = DataStorage.getInstance().getUniqueNotificationId();
//            notifId = notifId + 1;
//            DataStorage.getInstance().setUniqueNotificationId(notifId);
            int color = getResources().getColor(R.color.appcolor);
            mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setContentTitle("Processing")
                    .setContentText("Processing..")
                    .setAutoCancel(false)
                    .setColor(color)
                    .setSmallIcon(android.R.drawable.stat_sys_upload);
            mBuilder.setTicker("Processing");
            mBuilder.setProgress(0, 0, true);
        }
    }

    private boolean uploadMoment(final UploadData data) {
        try {
            ArrayList<UploadData> uploadDataList = DataCacheManager.getInstance().getAllUploadData();
            queueMessage = " Queued ";
            if (uploadDataList != null) {
                if (uploadDataList.size() > 1)
                    queueMessage = queueMessage + (uploadDataList.size() - 1);
                else {
                    queueMessage = "";
                }
            }
            UserFileManager userFileManager = Utils.getInstance().getUserFileManager();
            if (userFileManager == null){
                Utils.getInstance().initialiseAmazon();
            }else if(uploading){
                return true;
            }


            compressedFilePath = data.getMomentPath();
            if (data.getRecordedMoment()) {
                compressedFilePath = data.getMomentCompressedPath();
            }

            MomentShare momentShare1 = getMomentShareObj(data.getMomentShareText());
            message = "uploading the moment";
            if (momentShare1 != null)
                message = momentShare1.getNewMoment().getCaption();
            if (userFileManager == null) {
                uploading = false;
                return false;
            }
            if (compressedFilePath.isEmpty()) {
                uploading = false;
                return false;
            }
//            String compressedFile = compressedFolder + "Compressed_" + Utils.getInstance().generateUUIDStringDateTime() + ".mp4";
            final File file = new File(compressedFilePath);
            Utils.getInstance().storeImage(Utils.getInstance().getVideoThumbnail(compressedFilePath), true, data.getMomentUuid());
            final File thumbnailFile = new File(Utils.getInstance().getLocalThumbnailImagePath(data.getMomentUuid()));

            final String uuidString = Utils.getInstance().generateUUIDString();
            String fileName = uuidString + AppConsatants.FILE_EXTENSION_MP4;
            fileNameThumnail = uuidString + AppConsatants.FILE_EXTENSION_JPG;

            userFileManager.uploadContent(thumbnailFile, fileNameThumnail, new ContentProgressListener() {
                @Override
                public void onSuccess(final ContentItem contentItem) {

                }

                @Override
                public void onProgressUpdate(final String fileName, final boolean isWaiting,
                                             final long bytesCurrent, final long bytesTotal) {
                }

                @Override
                public void onError(final String fileName, final Exception ex) {
                    fileNameThumnail = "";
                }
            });
            userFileManager.uploadContent(file, fileName, new ContentProgressListener() {
                @Override
                public void onSuccess(final ContentItem contentItem) {
                    uploading = false;
                    MomentShare momentShare = getMomentShareObj(data.getMomentShareText());
                    momentShare.getNewMoment().setLink(contentItem.getFilePath());
                    momentShare.getNewMoment().setThumbNail(fileNameThumnail);
                    data.setMomentUuid(data.getMomentUuid());
                    data.setmReshare(true);
                    data.setMomentShareText(getJsonObectString(momentShare));
                    DataCacheManager.getInstance().saveUploadData(data);
//                    Log.i("Moment uploaded", moment.toString());

                    mNotifyManager.cancel(notifId);
                    sendMomentAddSuccess(momentShare, data.getMomentUuid());
                    Log.e("onSuccess", String.valueOf(uploading));

                }

                @Override
                public void onProgressUpdate(final String fileName, final boolean isWaiting,
                                             final long bytesCurrent, final long bytesTotal) {

                    uploading = true;
                    mBuilder.setContentTitle("Uploading   \t" + queueMessage);
                    mBuilder.setContentText(message);
                    mBuilder.setSmallIcon(android.R.drawable.stat_sys_upload);
                    mBuilder.setTicker("Uploading..");
                    mBuilder.setProgress((int) bytesTotal, (int) bytesCurrent, false);
//                    if(networkRestarted){
//                        mBuilder.setContentText("Upload unsuccessful");
//                        mBuilder.setSmallIcon(android.R.drawable.stat_sys_upload_done);
//                        mBuilder.setProgress(100, 100, false);
//                        mNotifyManager.notify(notifId, mBuilder.build());
//                        mNotifyManager.cancel(notifId);
//                    }

                    mNotifyManager.notify(notifId, mBuilder.build());
                }

                @Override
                public void onError(final String fileName, final Exception ex) {
                    uploading = false;
                    Log.e("onError", String.valueOf(uploading));
                    checkForScheduledUploads(1);
//                    showError(R.string.app_name, getString(R.string.error_upload_failed), null);

//                    mBuilder.setContentText("Upload Failed");
//                    mBuilder.setSmallIcon(android.R.drawable.stat_sys_upload_done);
//                    mBuilder.setProgress(0, 0, false);
//                    mNotifyManager.notify(notifId, mBuilder.build());
//                    stopSelf();
                }
            });
        } catch (Exception e) {
            uploading = false;

        }
        return true;
    }

    private void checkForScheduledUploads(int next) {
        if(uploading)
            return;
        ArrayList<UploadData> uploadDataList = DataCacheManager.getInstance().getAllUploadData();
        nextData = nextData + next;
        if (uploadDataList != null && uploadDataList.size() > 0 && nextData < uploadDataList.size()) {
            UploadData data = uploadDataList.get(nextData);
            if (data != null) {
                MomentShare momentShare = getMomentShareObj(data.getMomentShareText());
                if (momentShare != null) {
                    if (data.getmReshare()) {
                        sendMomentAddSuccess(momentShare, data.getMomentUuid());
                    } else {
                        mBuilder.setContentTitle("Processing");
                        mBuilder.setContentText(momentShare.getNewMoment().getCaption());
                        mBuilder.setSmallIcon(android.R.drawable.stat_sys_upload);
                        mBuilder.setProgress(0, 0, true);
                        mNotifyManager.notify(notifId, mBuilder.build());
                        initialiseProgressBar(data);
                    }
                } else {
                    if (data != null) {
                        String uuid = data.getMomentUuid();
                        if (!uuid.isEmpty())
                            DataCacheManager.getInstance().removeUploadData();
                    }
                    checkForScheduledUploads(1);
                }
            } else {
                checkForScheduledUploads(1);
            }
        } else if (CheckNetworkConnection.isConnectionAvailable(MyApplication.getAppContext()) && uploadDataList.size() > 0) {
            nextData = 0;
            checkForScheduledUploads(0);
        } else {
            nextData = 0;

            if (mNotifyManager != null) {
                mNotifyManager.cancel(notifId);
                ffmpegSingleton.removeListener(this);
                stopSelf();

            }

        }
    }

   /* private void deleteMoment(Moment obj) {
        try {
            UserFileManager userFileManager = Utils.getInstance().getUserFileManager();
            if (userFileManager == null)
                return;
            if (obj == null)
                return;
            userFileManager.deleteRemoteContent(obj.getThumbNail(), new UserFileManager.ResponseHandler() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(AmazonClientException exception) {

                }
            });
            userFileManager.deleteRemoteContent(obj.getLink(), new UserFileManager.ResponseHandler() {
                @Override
                public void onSuccess() {
                    stopSelf();
                }

                @Override
                public void onError(AmazonClientException exception) {
                    stopSelf();
                }
            });
        } catch (Exception e) {

        }
    }*/

    private void sendMomentAddSuccess(final MomentShare moment, final String uuid) {
        try {
            if (moment == null)
                return;
            String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_MOMENT_ADD);
            String jsonObject = getJsonObectString(moment);
//            Log.i("Add moment",jsonObject);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Moment obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Moment.class);
                    } catch (IOException e) {
                    } catch (OutOfMemoryError e) {
                    } catch (Exception e) {
                    }
//                    hideProgressBar();
                    if (obj != null) {
//                        String uuid = obj.getUuid();
                        if (uuid != null && !uuid.isEmpty()) {
                            DataCacheManager.getInstance().removeUploadData(uuid);
                            String username = DataStorage.getInstance().getUsername();
                            String url = ServiceURLManager.getInstance().getMomentFeedsListUrl(IAPIConstants.API_KEY_GET_MOMENT_FEEDS_TIMELINE_LIST, username, username, AppConsatants.FALSE, "0", "0");

                            String url1 = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, username, username, AppConsatants.FALSE, "0", "0");
                            VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(url1, true);
                            VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(url, false);
                            mNotifyManager.cancel(notifId);
                            Utils.getInstance().displayToast(MyApplication.getAppContext(), obj.getCaption() + " Uploaded successfully", Utils.SHORT_TOAST);
                            checkForScheduledUploads(0);
                        }

                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    checkForScheduledUploads(1);
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//       }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getJsonObectString(MomentShare moment) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonObject = null;
        try {
            jsonObject = mapper.writeValueAsString(moment);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private MomentShare getMomentShareObj(String momentShareString) {
        if (momentShareString == null)
            return null;
        if (momentShareString.isEmpty())
            return null;
        ObjectMapper mapper = new ObjectMapper();
        MomentShare obj = null;
        try {
            obj = mapper.readValue(momentShareString.toString(), MomentShare.class);
        } catch (IOException e) {
        } catch (OutOfMemoryError e) {
        } catch (Exception e) {
        }
        return obj;
    }

    @Override
    public void onFfmpegSuccess(UploadData uploadData) {

        String momentPath = uploadData.getMomentPath();
        uploadData.setRecordedMoment(false);
        uploadData.setMomentPath(uploadData.getMomentCompressedPath());
        DataCacheManager.getInstance().saveUploadData(uploadData);
        try {
            File original = new File(momentPath);
            if (original.exists()) {
                original.delete();
            }
        } catch (Exception e) {

        }
        if(!uploadMoment(uploadData)){
            checkForScheduledUploads(1);
        }


    }

    @Override
    public void onFfmpegError(UploadData uploadData) {

        mBuilder.setProgress(0, 0, false);
        mBuilder.setContentTitle("Processing failed");
        mNotifyManager.notify(notifId, mBuilder.build());
        checkForScheduledUploads(1);
    }
}