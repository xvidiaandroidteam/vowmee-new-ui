package net.xvidia.vowmee.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Ravi_office on 8-Apr-2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GroupProfile {
    private String uuid;
    private String name;
    private String creatorUuid;
    private String ownerUuid;
    private String profile; // PUBLIC / PRIVATE
    private String status; // ACTIVE / INACTIVE

    private String thumbNail;
    private String profileImage;
    private String statusMsg;
    private String description;
    private long dateOfCreation;
    private long lastActiveDate;
    private String roleWithViewer;
    private int noOfPosts;
    private int noOfMembers;
    private boolean isMembershipRequested;
    private int noOfPendingRequests;
    private long reportAbuseCount;
    private boolean reportedAsAbuse;
    private List<String> initialMembers;

    public long getReportAbuseCount() {
        return reportAbuseCount;
    }

    public void setReportAbuseCount(long reportAbuseCount) {
        this.reportAbuseCount = reportAbuseCount;
    }

    public boolean isReportedAsAbuse() {
        return reportedAsAbuse;
    }

    public void setReportedAsAbuse(boolean reportedAsAbuse) {
        this.reportedAsAbuse = reportedAsAbuse;
    }

    public int getNoOfPendingRequests() {
        return noOfPendingRequests;
    }

    public void setNoOfPendingRequests(int noOfPendingRequests) {
        this.noOfPendingRequests = noOfPendingRequests;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatorUuid() {
        return creatorUuid;
    }

    public void setCreatorUuid(String creatorUuid) {
        this.creatorUuid = creatorUuid;
    }

    public String getOwnerUuid() {
        return ownerUuid;
    }

    public void setOwnerUuid(String ownerUuid) {
        this.ownerUuid = ownerUuid;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getThumbNail() {
        return thumbNail;
    }

    public void setThumbNail(String thumbNail) {
        this.thumbNail = thumbNail;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(long dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }


    public List<String> getInitialMembers() {
        return initialMembers;
    }

    public void setInitialMembers(List<String> initialMembers) {
        this.initialMembers = initialMembers;
    }

    public long getLastActiveDate() {
        return lastActiveDate;
    }

    public void setLastActiveDate(long lastActiveDate) {
        this.lastActiveDate = lastActiveDate;
    }

    public String getRoleWithViewer() {
        return roleWithViewer;
    }

    public void setRoleWithViewer(String roleWithViewer) {
        this.roleWithViewer = roleWithViewer;
    }

    public int getNoOfPosts() {
        return noOfPosts;
    }

    public void setNoOfPosts(int noOfPosts) {
        this.noOfPosts = noOfPosts;
    }

    public int getNoOfMembers() {
        return noOfMembers;
    }

    public void setNoOfMembers(int noOfMembers) {
        this.noOfMembers = noOfMembers;
    }

    public boolean isMembershipRequested() {
        return isMembershipRequested;
    }

    public void setIsMembershipRequested(boolean membershipRequested) {
        isMembershipRequested = membershipRequested;
    }
}
