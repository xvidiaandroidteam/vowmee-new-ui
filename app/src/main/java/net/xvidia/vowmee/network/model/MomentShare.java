package net.xvidia.vowmee.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Ravi_office on 18-Apr-16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class MomentShare {
    Moment originalMoment;
    Moment newMoment;
    Boolean followers;
    Boolean friends;
    List<String> userUuids;
    List<String> groupUuids;

    public Moment getOriginalMoment() {
        return originalMoment;
    }

    public void setOriginalMoment(Moment originalMoment) {
        this.originalMoment = originalMoment;
    }

    public Moment getNewMoment() {
        return newMoment;
    }

    public void setNewMoment(Moment newMoment) {
        this.newMoment = newMoment;
    }

    public Boolean getFollowers() {
        return followers;
    }

    public void setFollowers(Boolean followers) {
        this.followers = followers;
    }

    public Boolean getFriends() {
        return friends;
    }

    public void setFriends(Boolean friends) {
        this.friends = friends;
    }

    public List<String> getUserUuids() {
        return userUuids;
    }

    public void setUserUuids(List<String> userUuids) {
        this.userUuids = userUuids;
    }

    public List<String> getGroupUuids() {
        return groupUuids;
    }

    public void setGroupUuids(List<String> groupUuids) {
        this.groupUuids = groupUuids;
    }
}
