package net.xvidia.vowmee.network.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FacebookFriend {


	@JsonProperty("id")private String id;
	@JsonProperty("name")private String name;
	@JsonProperty("picture")private PictureFriend picture;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PictureFriend getPicture() {
		return picture;
	}

	public void setPicture(PictureFriend picture) {
		this.picture = picture;
	}

	@Override
	public String toString() {
		return "FacebookFriend{" +
				"id='" + id + '\'' +
				", name='" + name + '\'' +
				", picture='" + picture + '\'' +
				'}';
	}
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class PictureFriend {

		@JsonProperty("data")public PictureFriendUrl data;

		@JsonCreator
		public PictureFriend(@JsonProperty("data") PictureFriendUrl data) {
			this.data = data;
		}

		public FacebookFriend.PictureFriendUrl getData() {
			return data;
		}

		public void setData(FacebookFriend.PictureFriendUrl data) {
			this.data = data;
		}

	}
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class PictureFriendUrl{

		@JsonProperty("url")public String url;


		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public PictureFriendUrl(){
			//constructor Code
		}
		@JsonCreator
		public PictureFriendUrl(@JsonProperty("url")String url) {
			this.url = url;
		}
	}

}
