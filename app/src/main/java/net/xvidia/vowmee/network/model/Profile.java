package net.xvidia.vowmee.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Ravi_office on 23-Nov-15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Profile {
    private String uuid;
    private String username;
    private String profileImage;
    private String thumbNail;
    private String fullName;
    private String displayName;
    private String email;
    private String phone;
    private String profile;
    private String dobString;
    private String gender;
    private String statusMsg;
    private String errorMessage;
    private String twitterId;
    private String facebookId;
    private String followers;
    private String followings;
    private String friends;
    private String posts;
    private boolean friendWithViewer;
    private boolean followingViewer;
    private boolean followedByViewer;
    private boolean blockedByViewer;
    private boolean hasBlockedViewer;
    private boolean friendRequestFromViewer;
    private boolean friendRequestToViewer;
    private boolean followRequestFromViewer;
    private boolean followRequestToViewer;
    private String pendingFollowRequests;
    private String pendingFriendRequests;
    private String gcmId;
    private String cognitoId;
    private String cognitoToken;
    private String groupRole;
    private long groupJoiningDate;
    private int blockedUserCount;

    public int getBlockedUserCount() {
        return blockedUserCount;
    }

    public void setBlockedUserCount(int blockedUserCount) {
        this.blockedUserCount = blockedUserCount;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCognitoId() {
        return cognitoId;
    }

    public void setCognitoId(String cognitoId) {
        this.cognitoId = cognitoId;
    }

    public String getCognitoToken() {
        return cognitoToken;
    }

    public void setCognitoToken(String cognitoToken) {
        this.cognitoToken = cognitoToken;
    }

    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getPendingFollowRequests() {
        return pendingFollowRequests;
    }

    public void setPendingFollowRequests(String pendingFollowRequests) {
        this.pendingFollowRequests = pendingFollowRequests;
    }

    public String getPendingFriendRequests() {
        return pendingFriendRequests;
    }

    public void setPendingFriendRequests(String pendingFriendRequests) {
        this.pendingFriendRequests = pendingFriendRequests;
    }

    public boolean isFriendRequestFromViewer() {
        return friendRequestFromViewer;
    }

    public void setFriendRequestFromViewer(boolean friendRequestFromViewer) {
        this.friendRequestFromViewer = friendRequestFromViewer;
    }

    public boolean isFriendRequestToViewer() {
        return friendRequestToViewer;
    }

    public void setFriendRequestToViewer(boolean friendRequestToViewer) {
        this.friendRequestToViewer = friendRequestToViewer;
    }

    public boolean isFollowRequestFromViewer() {
        return followRequestFromViewer;
    }

    public void setFollowRequestFromViewer(boolean followRequestFromViewer) {
        this.followRequestFromViewer = followRequestFromViewer;
    }

    public boolean isFollowRequestToViewer() {
        return followRequestToViewer;
    }

    public void setFollowRequestToViewer(boolean followRequestToViewer) {
        this.followRequestToViewer = followRequestToViewer;
    }

    public boolean isHasBlockedViewer() {
        return hasBlockedViewer;
    }

    public void setHasBlockedViewer(boolean hasBlockedViewer) {
        this.hasBlockedViewer = hasBlockedViewer;
    }

    public String getThumbNail() {
        return thumbNail;
    }

    public void setThumbNail(String thumbNail) {
        this.thumbNail = thumbNail;
    }

    public boolean isFriendWithViewer() {
        return friendWithViewer;
    }

    public void setFriendWithViewer(boolean friendWithViewer) {
        this.friendWithViewer = friendWithViewer;
    }

    public boolean isFollowingViewer() {
        return followingViewer;
    }

    public void setFollowingViewer(boolean followingViewer) {
        this.followingViewer = followingViewer;
    }

    public boolean isFollowedByViewer() {
        return followedByViewer;
    }

    public void setFollowedByViewer(boolean followedByViewer) {
        this.followedByViewer = followedByViewer;
    }

    public boolean isBlockedByViewer() {
        return blockedByViewer;
    }

    public void setBlockedByViewer(boolean blockedByViewer) {
        this.blockedByViewer = blockedByViewer;
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public String getFollowings() {
        return followings;
    }

    public void setFollowings(String followings) {
        this.followings = followings;
    }

    public String getFriends() {
        return friends;
    }

    public void setFriends(String friends) {
        this.friends = friends;
    }

    public String getPosts() {
        return posts;
    }

    public void setPosts(String posts) {
        this.posts = posts;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getDobString() {
        return dobString;
    }

    public void setDobString(String dobString) {
        this.dobString = dobString;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getGroupRole() {
        return groupRole;
    }

    public void setGroupRole(String groupRole) {
        this.groupRole = groupRole;
    }

    public long getGroupJoiningDate() {
        return groupJoiningDate;
    }

    public void setGroupJoiningDate(long groupJoiningDate) {
        this.groupJoiningDate = groupJoiningDate;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "uuid='" + uuid + '\'' +
                ", username='" + username + '\'' +
                ", profileImage='" + profileImage + '\'' +
                ", thumbNail='" + thumbNail + '\'' +
                ", fullName='" + fullName + '\'' +
                ", displayName='" + displayName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", profile='" + profile + '\'' +
                ", dobString='" + dobString + '\'' +
                ", gender='" + gender + '\'' +
                ", statusMsg='" + statusMsg + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", twitterId='" + twitterId + '\'' +
                ", facebookId='" + facebookId + '\'' +
                ", followers='" + followers + '\'' +
                ", followings='" + followings + '\'' +
                ", friends='" + friends + '\'' +
                ", posts='" + posts + '\'' +
                ", friendWithViewer=" + friendWithViewer +
                ", followingViewer=" + followingViewer +
                ", followedByViewer=" + followedByViewer +
                ", blockedByViewer=" + blockedByViewer +
                ", hasBlockedViewer=" + hasBlockedViewer +
                ", friendRequestFromViewer=" + friendRequestFromViewer +
                ", friendRequestToViewer=" + friendRequestToViewer +
                ", followRequestFromViewer=" + followRequestFromViewer +
                ", followRequestToViewer=" + followRequestToViewer +
                ", pendingFollowRequests='" + pendingFollowRequests + '\'' +
                ", pendingFriendRequests='" + pendingFriendRequests + '\'' +
                ", gcmId='" + gcmId + '\'' +
                ", cognitoId='" + cognitoId + '\'' +
                ", cognitoToken='" + cognitoToken + '\'' +
                ", groupRole='" + groupRole + '\'' +
                ", groupJoiningDate=" + groupJoiningDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Profile profile = (Profile) o;

        return uuid.equals(profile.uuid);

    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }
}
