package net.xvidia.vowmee;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opentok.android.AudioDeviceManager;
import com.opentok.android.BaseAudioDevice;
import com.opentok.android.BaseVideoRenderer;
import com.opentok.android.Connection;
import com.opentok.android.OpentokError;
import com.opentok.android.Publisher;
import com.opentok.android.PublisherKit;
import com.opentok.android.Session;
import com.opentok.android.Session.SessionListener;
import com.opentok.android.Stream;
import com.opentok.android.Subscriber;
import com.opentok.android.SubscriberKit;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.FileCache;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.broadcastreceiver.NetworkStateReceiver;
import net.xvidia.vowmee.fragments.MeterView;
import net.xvidia.vowmee.fragments.SubscriberQualityFragment;
import net.xvidia.vowmee.helper.CircularImageView;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.Opentok;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MakeVideoCallActivity extends Activity implements NetworkStateReceiver.NetworkStateReceiverListener,
        PublisherKit.PublisherListener, SessionListener, SubscriberKit.SubscriberListener, SubscriberKit.VideoListener, Session.SignalListener {

    public static final String SIGNAL_TYPE_CLOSE = "close";
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 1111;
    private static final String LOGTAG = "demo-voice-only";
    private String sessionClosed = null;
    private Boolean isClosedByPublisher = false;
    private Session mSession;
    private Publisher mPublisher;
    private ArrayList<Subscriber> mSubscribers = new ArrayList<Subscriber>();
    private HashMap<Stream, Subscriber> mSubscriberStream = new HashMap<Stream, Subscriber>();
    private Handler mHandler = new Handler();
    //    private LinearLayout callLayout;
    private RelativeLayout defaultUserBackgroundLayout, afterConnectLayout;
    private ProgressDialog pDialog;
    private TextView timerValue, callerName, callStatus;
    private Activity activity;
    //            callStatus;
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;
    MeterView meterView1;

    private String mApiKey;
    private String mToken;
    private boolean firstFlag, initializeTimer;

    private String groupUuid;
    public static String mSessionId;
    private Subscriber mSubscriber;
    private ArrayList<Stream> mStreams;
    private boolean mSubscriberAudioOnly = false;
    private SubscriberQualityFragment.CongestionLevel congestion = SubscriberQualityFragment.CongestionLevel.Low;
    private FrameLayout mSubscriberViewContainer,mPublisherViewContainer;
    private boolean notificationIntent;
//    private RelativeLayout mSubscriberAudioOnlyView;

    private ImageView imageView;
    private CircularImageView circularImageView;
    private String thumbnailPath, userDisplayName;
    private SubscriberQualityFragment mSubscriberQualityFragment;
    private FragmentTransaction mFragmentTransaction;
    private MeterView mv;
    private PhoneStateListener callStateListener;
    private TelephonyManager telephonyManager;
    private ImageView switchCamera;
    private ImageView videoCameraOnOffImageView;
    private boolean isPublisherViewSmall;
    private boolean pauseTimer = false;
    public static boolean closeMakeVideoCall;
    private long startTime2 = 0L;
    private Handler customHandler2 = new Handler();
    long timeInMilliseconds2 = 0L;
    long timeSwapBuff2 = 0L;
    long updatedTime2 = 0L;
    private Button endCall;
    private AudioManager audioManager;
    private static MediaPlayer mediaPlayer;
    private NetworkStateReceiver networkStateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_make_video_call);

        initialiseRingBackTone();

        timerValue = (TextView) findViewById(R.id.timerText);
        endCall = (Button) findViewById(R.id.view);
        timerValue.setVisibility(View.INVISIBLE);
        switchCamera = (ImageView) findViewById(R.id.button_switch_camera);
        switchCamera.setVisibility(View.GONE);
        switchCamera.setOnClickListener(new View.OnClickListener(

        ) {
            @Override
            public void onClick(View v) {
                flipCamera();
            }
        });
        videoCameraOnOffImageView = (ImageView) findViewById(R.id.video_camera_imageview);
        videoCameraOnOffImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mPublisher != null) {
                    if (mPublisher.getPublishVideo() == true) {
                        mPublisher.setPublishVideo(false);
                        videoCameraOnOffImageView.setImageDrawable(getResources().getDrawable(R.drawable.video));
                    } else {
                        mPublisher.setPublishVideo(true);
                        videoCameraOnOffImageView.setImageDrawable(getResources().getDrawable(R.drawable.video_off));
                    }
                }
            }
        });

//        initialiseRingBackTone();
        closeMakeVideoCall =false;
        callStatus = (TextView) findViewById(R.id.callProgress);
        callStatus.setText(getString(R.string.connecting));
        defaultUserBackgroundLayout = (RelativeLayout) findViewById(R.id.hangupLayout);
        afterConnectLayout = (RelativeLayout) findViewById(R.id.afterConnectLayout);
        imageView = (ImageView) findViewById(R.id.home_profile_image_background);
        circularImageView = (CircularImageView) findViewById(R.id.home_profile_image);
        callerName = (TextView) findViewById(R.id.callerName);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Calling...wait");
        pDialog.setCancelable(true);
        activity = this;
        initializeTimer = true;
//        showDialog();
        firstFlag = true;
        Intent intent = getIntent();
        if (null != intent) { //Null Checking
//            momentCaption = "CALL";
//            momentDescription = intent.getStringExtra(AppConsatants.LIVE_DESCRIPTION);
            groupUuid = intent.getStringExtra(AppConsatants.UUID);
            thumbnailPath = intent.getStringExtra(AppConsatants.USER_THUMBNAIL);
            userDisplayName = intent.getStringExtra(AppConsatants.PERSON_FRIEND);
//            privateFlag = true;
        }
        if (savedInstanceState == null) {
            mFragmentTransaction = getFragmentManager().beginTransaction();
            initSubscriberQualityFragment();
            mFragmentTransaction.commitAllowingStateLoss();
        }
//        mSubscriberAudioOnlyView = (RelativeLayout) findViewById(R.id.audioOnlyView);
        mSubscriberViewContainer = (FrameLayout) findViewById(R.id.subscriber_container);
        mPublisherViewContainer = (FrameLayout) findViewById(R.id.publisher_container);
        isPublisherViewSmall = true;
        mPublisherViewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mPublisher != null && mSubscriber !=null) {
                    if (isPublisherViewSmall) {
                        mPublisherViewContainer.removeView(mPublisher.getView());
                        mSubscriberViewContainer.removeView(mSubscriber.getView());
                        mPublisherViewContainer.addView(mSubscriber.getView(),0);
                        mSubscriberViewContainer.addView(mPublisher.getView());
                       /* RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                                RelativeLayout.LayoutParams.MATCH_PARENT,
                                RelativeLayout.LayoutParams.MATCH_PARENT);
                        mPublisherViewContainer.setLayoutParams(layoutParams);

                        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(
                                100,
                                140);
                        mSubscriberViewContainer.setLayoutParams(layoutParams1);*/
                        isPublisherViewSmall = false;
                    } else {
                        mPublisherViewContainer.removeView(mSubscriber.getView());
                        mSubscriberViewContainer.removeView(mPublisher.getView());
                        mPublisherViewContainer.addView(mPublisher.getView(),0);
                        mSubscriberViewContainer.addView(mSubscriber.getView());
                       /* RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                                100,
                                140);
                        mPublisherViewContainer.setLayoutParams(layoutParams);

                        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(
                                RelativeLayout.LayoutParams.MATCH_PARENT,
                                RelativeLayout.LayoutParams.MATCH_PARENT);
                        mSubscriberViewContainer.setLayoutParams(layoutParams1);*/
                        isPublisherViewSmall = true;
                    }
                }
            }
        });
        mStreams = new ArrayList<Stream>();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSubscriber != null) {
                    if (mSubscriberAudioOnly) {
                        mSubscriber.getView().setVisibility(View.GONE);
//                        setAudioOnlyView(true);
                        congestion = SubscriberQualityFragment.CongestionLevel.High;
                    }
                }
            }
        }, 0);

        mv = (MeterView) findViewById(R.id.volumeMic);
        mv.setIcons(BitmapFactory.decodeResource(getResources(),
                R.drawable.unmute_pub), BitmapFactory.decodeResource(
                getResources(), R.drawable.mute_pub));

//        AudioManager Audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//        Audio.setMode(AudioManager.MODE_NORMAL);
//        this.setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
        downloadContentFromS3Bucket(thumbnailPath);
        callerName.setText(getString(R.string.calling_friend, userDisplayName));

        fetchPublishSessionConnectionData();
        endCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEndCall();
            }
        });

        startTime2 = SystemClock.uptimeMillis();
        customHandler2.postDelayed(updateTimerThread2, 0);
        DataStorage.getInstance().setCallProgress(true);
    }
    private Runnable updateTimerThread2 = new Runnable() {

        public void run() {

            timeInMilliseconds2 = SystemClock.uptimeMillis() - startTime2;
            updatedTime2 = timeSwapBuff2 + timeInMilliseconds2;

            if (updatedTime2 >= AppConsatants.timeoutMakeCallEnd) {
                onBackPressed();
                Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_failed_connect),Utils.LONG_TOAST);

            }
            customHandler2.postDelayed(this, 0);

        }
    };
    @Override
    protected void onPause() {
        try{
            if(customHandler!=null && updateTimerThread != null)
                customHandler.removeCallbacks(updateTimerThread);
            if(customHandler2!=null && updateTimerThread2 != null)
                customHandler2.removeCallbacks(updateTimerThread2);
        }catch (Exception e){}
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
//        callSessionCancel();
        closeMakeVideoCall =true;
        DataStorage.getInstance().setCallProgress(false);
        super.onPause();
    }

    public void flipCamera() {
        try {
            if (isPublisherViewSmall) {
                mPublisherViewContainer.removeView(mPublisher.getView());
                mPublisher.cycleCamera();
                mPublisherViewContainer.addView(mPublisher.getView(), 0);
                mPublisherViewContainer.requestFocus();
                switchCamera.requestFocus();
            } else {
                mSubscriberViewContainer.removeView(mPublisher.getView());
                mPublisher.cycleCamera();
                mSubscriberViewContainer.addView(mPublisher.getView(), 0);
            }
        } catch (OutOfMemoryError e) {

        } catch (Exception e) {

        }
    }

//    private void downloadContentFromS3Bucket(String filePath) {
//        String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
//        File file = new File(filePathImage);
//        if (file.exists()) {
//            imageView.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePathImage, false));
//            circularImageView.setImageBitmap(Utils.getInstance().loadImageFromStorage(filePathImage, false));
//            imageView.setImageAlpha(80);
//        }
//    }
    private void downloadContentFromS3Bucket(String filePath){
        if(filePath!=null && !filePath.isEmpty()) {

            imageView.setVisibility(View.VISIBLE);
            Bitmap image = FileCache.getInstance().getBitmapFromMemCache(filePath);
            if(image!=null ) {
                imageView.setImageBitmap(image);
                circularImageView.setImageBitmap(image);
                imageView.setImageAlpha(80);
            }else {
                String filePathImage = Utils.getInstance().getLocalContentPath(filePath);
                File file = new File(filePathImage);
                if (file.exists()) {
                    Bitmap image1 = Utils.getInstance().loadImageFromStorage(filePathImage, false);
                    if (image1 != null) {
                        FileCache.getInstance().addBitmapToMemoryCache(filePath, image1);
                        imageView.setImageBitmap(image1);
                        circularImageView.setImageBitmap(image1);
                        imageView.setImageAlpha(80);
                    }
                } else {
                    circularImageView.setImageResource(R.drawable.user);
                }
            }
        }
    }



    private void attachSubscriberView(Subscriber subscriber) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        mSubscriberViewContainer.removeView(mSubscriber.getView());
        mSubscriberViewContainer.addView(subscriber.getView(), layoutParams);
        subscriber.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                BaseVideoRenderer.STYLE_VIDEO_FIT);
    }


    public void fetchPublishSessionConnectionData() {
        try {
            if(groupUuid==null) {
                finish();
                return;
            }
            if(groupUuid.isEmpty()){
                finish();
                return;
            }
            ArrayList<String> memberUuidList = new ArrayList<>();
            memberUuidList.add(groupUuid);
            String url = ServiceURLManager.getInstance().getMakeCallUrl(DataStorage.getInstance().getUserUUID());
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(memberUuidList);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("Add moment", jsonObject);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
//                    hideDialog();
                    ObjectMapper mapper = new ObjectMapper();
                    Opentok obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Opentok.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        mSessionId = obj.getSessionId();
                        mToken = obj.getToken();
                        mApiKey = obj.getApiKey();
                        if (mSessionId != null && !mSessionId.equalsIgnoreCase("null")) {
                            Log.e("InitiatorSessionID", mSessionId);
                            initializeSession();
                            initializePublisher();
                            firstFlag = false;
                        }else{
                            closeSession();
                        }
                    } else {
                        closeSession();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    closeSession();
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
            closeSession();
        }
    }

    private void closeSession() {
//        hideDialog();
        if (mSession != null)
            mSession.disconnect();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                showMessage(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            }
        });
    }


    private void showMessage(final String mesg, final DialogInterface.OnClickListener okClicked) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(MakeVideoCallActivity.this).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(mesg)
                        .setPositiveButton(android.R.string.ok, okClicked)
                        .setCancelable(false)
                        .show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    public void onBackPressed() {
        stopRing();
        DataStorage.getInstance().setCallProgress(false);
        restartAudioMode();
        if(telephonyManager!=null && callStateListener!=null)
            telephonyManager.listen(callStateListener, PhoneStateListener.LISTEN_NONE);

        if (mSession != null) {
//            hideDialog();
            mSession.disconnect();
            if (mSession != null)
                mSession.disconnect();
            if (mPublisher != null)
                mPublisher.destroy();
            if (mSubscriber != null)
                mSubscriber.destroy();
        }
        closeMakeVideoCall =true;
        finish();
        //       restartAudioMode();

        super.onBackPressed();
    }
    public void initSubscriberQualityFragment() {
        mSubscriberQualityFragment = new SubscriberQualityFragment();
        getFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_sub_quality_container,
                        mSubscriberQualityFragment).commit();
    }

    public void restartAudioMode() {
        AudioManager Audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        Audio.setMode(AudioManager.MODE_NORMAL);
        this.setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
    }

    private void initializeSession() {
        mSession = new Session(this, mApiKey, mSessionId);
        mSession.setSessionListener(this);
        mSession.setSignalListener(this);
        mSession.connect(mToken);
//        changeVoiceMode(true);
    }



    private void initializePublisher() {
        mPublisher = new Publisher(getApplicationContext(), "Xvidia's Demo",
                Publisher.CameraCaptureResolution.MEDIUM,
                Publisher.CameraCaptureFrameRate.FPS_15);
        mPublisher.getRenderer().setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                BaseVideoRenderer.STYLE_VIDEO_FILL);
        mPublisher.setPublisherListener(this);
        mPublisher.setPublishVideo(true);
        mPublisher.setPublishAudio(true);
        mPublisherViewContainer.addView(mPublisher.getView(), 0);
        mPublisherViewContainer.setVisibility(View.VISIBLE);
        videoCameraOnOffImageView.setVisibility(View.VISIBLE);
        switchCamera.setVisibility(View.VISIBLE);
        AudioDeviceManager.getAudioDevice().setOutputMode(BaseAudioDevice.OutputMode.SpeakerPhone);
//        hideDialog();

    }

    private void onEndCall() {
        if(mSession!=null)
        mSession.sendSignal(SIGNAL_TYPE_CLOSE, getString(R.string.error_live_session_ended));
        onBackPressed();
    }

    private void initialiseRingBackTone(){
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int maxVolume = 100;
        int progress = 80;
        int maxAudVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
        int progVal = (progress * maxAudVolume) / maxVolume;
        audioManager.setStreamVolume(AudioManager.STREAM_RING, progVal, 0);
        mediaPlayer = MediaPlayer.create(this, R.raw.rings);
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                mp.setVolume(1f,1f);
//                mp.start();
            }
        });
        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.e("error",String.valueOf(what +" , "+ extra));
                return false;
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Log.e("error",mp.toString());
            }
        });

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startRing();
            }
        }, 5000);

    }
    private void reinitailseAudioManager(){
        int maxVolume = 100;
        int progress = 100;
        int maxAudVolume = audioManager
                .getStreamMaxVolume(AudioManager.STREAM_RING);
        int progVal = (progress * maxAudVolume) / maxVolume;
        audioManager.setStreamVolume(AudioManager.STREAM_RING, progVal, 0);
    }

    private void stopRing() {
        try {
            reinitailseAudioManager();
            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
//                    mediaPlayer.setVolume(1f,1f);
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer =null;
                }
            }
        } catch (Exception e) {
            Log.e("MediaPlayer", e.getMessage());
        }

    }

    private void startRing() {
        try {
            if (mediaPlayer != null) {
                if (!mediaPlayer.isPlaying())
                    mediaPlayer.start();
            }

        } catch (Exception e) {
            Log.e("MediaPlayer", e.getMessage());
        }

    }

    @Override
    public void onConnected(Session session) {
        if (mPublisher != null) {
            mSession.publish(mPublisher);
            mPublisher.setAudioLevelListener(new PublisherKit.AudioLevelListener() {
                @Override
                public void onAudioLevelUpdated(PublisherKit publisher,
                                                float audioLevel) {
                    meterView1.setMeterValue(audioLevel);
                }
            });
        }
//        hideDialog();
        Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_LONG);

//        startRing();
        // Initialize publisher meter view
        meterView1 = (MeterView) findViewById(R.id.volumeMic);
        meterView1.setVisibility(View.INVISIBLE);
        meterView1.setIcons(BitmapFactory.decodeResource(getResources(),
                R.drawable.unmute_pub), BitmapFactory.decodeResource(
                getResources(), R.drawable.mute_pub));

        meterView1.setOnClickListener(new MeterView.OnClickListener() {
            @Override
            public void onClick(MeterView view) {
                mPublisher.setPublishAudio(!view.isMuted());
            }
        });
    }

    private Runnable updateTimerThread = new Runnable() {

        public void run() {
            if(!pauseTimer) {

                timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
                updatedTime = timeSwapBuff + timeInMilliseconds;
                int secs = (int) (updatedTime / 1000);
                int mins = secs / 60;
                secs = secs % 60;
//            int milliseconds = (int) (updatedTime % 1000);
                timerValue.setText("" + String.format("%02d", mins) + ":"
                        + String.format("%02d", secs));
            /*timerValue.setText("" + mins + ":"
                    + String.format("%02d", secs) + ":"
                    + String.format("%03d", milliseconds));*/
            }else{
                startTime = SystemClock.uptimeMillis() - updatedTime;
//                startTime = SystemClock.uptimeMillis();
            }

            customHandler.postDelayed(this, 0);

        }
    };

    @Override
    public void onDisconnected(Session session) {
//        closePublishSessionConnection();
//        mPublisher.destroy();
    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {
        stopRing();
        if(customHandler2!=null && updateTimerThread2 != null)
            customHandler2.removeCallbacks(updateTimerThread2);
//        Log.i("MakeCallActivity", "Stream Received");
        mStreams.add(stream);
        if (mSubscriber == null) {
            subscribeToStream(stream);
//            hideDialog();
            meterView1.setVisibility(View.VISIBLE);
            final MeterView meterView = (MeterView) findViewById(R.id.volume);
            meterView.setMuted(false);
            meterView.setIcons(BitmapFactory.decodeResource(getResources(),
                    R.drawable.unmute_sub), BitmapFactory.decodeResource(
                    getResources(), R.drawable.mute_sub));
//            AudioDeviceManager.getAudioDevice().setOutputMode(
//                    BaseAudioDevice.OutputMode.SpeakerPhone);

            meterView.setOnClickListener(new MeterView.OnClickListener() {
                @Override
                public void onClick(MeterView view) {
//                    mSubscriber.setSubscribeToAudio(!view.isMuted());
                    changeVoiceMode(view.isMuted());

                }
            });
            mSubscriber
                    .setAudioLevelListener(new SubscriberKit.AudioLevelListener() {
                        @Override
                        public void onAudioLevelUpdated(
                                SubscriberKit subscriber, float audioLevel) {
                            meterView.setMeterValue(audioLevel);
                        }
                    });
//            meterView.setMuted(true);
            callerName.setText(userDisplayName);
            callStatus.setText(getString(R.string.call_in_progress));
            pauseTimer = false;
            timerValue.setVisibility(View.VISIBLE);
            if(initializeTimer) {
                startTime = SystemClock.uptimeMillis();
                initializeTimer = false;
            }
            customHandler.postDelayed(updateTimerThread, 0);
        }

        loadFragments();
    }
    private void initialiseIncomingCallListener() {
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        // register PhoneStateListener
        callStateListener = new PhoneStateListener() {
            public void onCallStateChanged(int state, String incomingNumber) {

                // If phone ringing
                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    onBackPressed();

                }
                // If incoming call received
                if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    onBackPressed();
                }


//                if(state== TelephonyManager.CALL_STATE_IDLE)
//                {
//                    Toast.makeText(getApplicationContext(),"phone is neither ringing nor in a call", Toast.LENGTH_LONG).show();
//                }
            }
        };
        telephonyManager.listen(callStateListener, PhoneStateListener.LISTEN_CALL_STATE);


    }
    @Override
    public void onResume() {
        super.onResume();
        checkPermission();
        networkStateReceiver = new NetworkStateReceiver(this);
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

    }
    private void changeVoiceMode(boolean isMuted){
        if(isMuted){
            AudioDeviceManager.getAudioDevice().setOutputMode(
                    BaseAudioDevice.OutputMode.Handset);
        }else{
            AudioDeviceManager.getAudioDevice().setOutputMode(
                    BaseAudioDevice.OutputMode.SpeakerPhone);
        }
    }
    public void loadFragments() {
        // show subscriber status
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSubscriber != null) {
//                    mSubscriberFragment.showSubscriberWidget(true);
//                    mSubscriberFragment.initSubscriberUI();

                    if (congestion != SubscriberQualityFragment.CongestionLevel.Low) {
                        mSubscriberQualityFragment.setCongestion(congestion);
                        mSubscriberQualityFragment.showSubscriberWidget(true);
                    }
                }
            }
        }, 0);

    }
    private void subscribeToStream(Stream stream) {
        mSubscriber = new Subscriber(this, stream);
        mSubscriber.setSubscriberListener(this);
        mSubscriber.setVideoListener(this);
        mSubscriber.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                BaseVideoRenderer.STYLE_VIDEO_FILL);
        mSession.subscribe(mSubscriber);
        if (mSubscriber.getSubscribeToVideo()) {
//            showDialog();
        }
        defaultUserBackgroundLayout.setVisibility(View.INVISIBLE);
        mSubscriber.setSubscribeToVideo(true);
        mSubscriber.setSubscribeToAudio(true);
    }



    @Override
    public void onStreamDropped(Session session, Stream stream) {

        mStreams.remove(stream);

        if(callStatus.getText().toString() == getString(R.string.call_in_progress)) {
            callStatus.setText("Reconnecting...");
            pauseTimer = true;
        }
        if (isClosedByPublisher) {
            Utils.getInstance().displayToast(MakeVideoCallActivity.this,sessionClosed,Utils.SHORT_TOAST);
            onBackPressed();
        }
        if (mSubscriber != null
                && mSubscriber.getStream().getStreamId()
                .equals(stream.getStreamId())) {
            mSubscriberViewContainer.removeView(mSubscriber.getView());
            mSubscriber = null;
            mSubscriberAudioOnly = false;
            if (!mStreams.isEmpty()) {
//                subscribeToStream(mStreams.get(0));
            }
//            Toast.makeText(this, "Call has ended", Toast.LENGTH_LONG).show();
//            onDisconnected(mSession);


        }
    }

    @Override
    public void onError(Session session, OpentokError error) {
//        Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
    }




    // Publisher Listener Methods

    @Override
    public void onStreamCreated(PublisherKit publisherKit, Stream stream) {
        Log.e("onStreamCreated","onStreamCreated");

    }

    @Override
    public void onStreamDestroyed(PublisherKit publisherKit, Stream stream) {

    }

    @Override
    public void onError(PublisherKit publisherKit, OpentokError opentokError) {

    }

    // Subscriber Listener Methods

    @Override
    public void onConnected(SubscriberKit subscriberKit) {

    }

    @Override
    public void onDisconnected(SubscriberKit subscriberKit) {

    }

    @Override
    public void onError(SubscriberKit subscriberKit, OpentokError opentokError) {

    }

    @Override
    public void onVideoDataReceived(SubscriberKit subscriberKit) {
//        hideDialog();
        attachSubscriberView(mSubscriber);
    }

    @Override
    public void onVideoDisabled(SubscriberKit subscriber, String reason) {
        if (mSubscriber == subscriber) {
//            setAudioOnlyView(true);
            defaultUserBackgroundLayout.setVisibility(View.VISIBLE);
        }

        if (reason.equals("quality")) {
            mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.High);
            congestion = SubscriberQualityFragment.CongestionLevel.High;
            setSubQualityMargins();
            mSubscriberQualityFragment.showSubscriberWidget(true);
        }
    }

    public void setSubQualityMargins() {
        RelativeLayout subQualityContainer = mSubscriberQualityFragment.getSubQualityContainer();
        if (subQualityContainer != null) {
            RelativeLayout.LayoutParams subQualityLayoutParams = (RelativeLayout.LayoutParams) subQualityContainer.getLayoutParams();
//            RelativeLayout.LayoutParams audioMeterLayoutParams = (RelativeLayout.LayoutParams) mAudioLevelView.getLayoutParams();

            int bottomMargin = 0;

            // control pub fragment
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {

                if (bottomMargin == 0) {
                    bottomMargin = Utils.getInstance().convertDpToPixel(10, this);
                }
                subQualityLayoutParams.rightMargin = Utils.getInstance().convertDpToPixel(10, this);
            }

            subQualityLayoutParams.bottomMargin = bottomMargin;

            mSubscriberQualityFragment.getSubQualityContainer().setLayoutParams(
                    subQualityLayoutParams);
        }

    }

    @Override
    public void onVideoEnabled(SubscriberKit subscriber, String reason) {
        if (mSubscriber == subscriber) {
//            setAudioOnlyView(false);
            defaultUserBackgroundLayout.setVisibility(View.INVISIBLE);
        }
        if (reason.equals("quality")) {
            mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.Low);
            congestion = SubscriberQualityFragment.CongestionLevel.Low;
            mSubscriberQualityFragment.showSubscriberWidget(false);
        }
    }

    @Override
    public void onVideoDisableWarning(SubscriberKit subscriberKit) {
        mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.Mid);
        congestion = SubscriberQualityFragment.CongestionLevel.Mid;
        setSubQualityMargins();
        mSubscriberQualityFragment.showSubscriberWidget(true);
    }

    @Override
    public void onVideoDisableWarningLifted(SubscriberKit subscriberKit) {
        mSubscriberQualityFragment.setCongestion(SubscriberQualityFragment.CongestionLevel.Low);
        congestion = SubscriberQualityFragment.CongestionLevel.Low;
        mSubscriberQualityFragment.showSubscriberWidget(false);
    }


    //Incoming call state listener permission

    private void checkPermission() {

        activity = this;
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, android.Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("Handle Incoming Call");

// final String[] PERMISSIONS_CONTACT = {Manifest.permission.READ_CONTACTS,Manifest.permission.READ_SMS};

        Map<String, Integer> perms = new HashMap<String, Integer>();
// Initial
        perms.put(android.Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);

        if (permissionsList.size() == 0) {
        } else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
// Need Rationale
// String message = "You need to grant access to Contacts and SMS permissions";
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                            REQUEST_CODE_ASK_PERMISSIONS);
                                }
                            });
                    return;
                }
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;

            }
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<String, Integer>();
// Initial
            perms.put(android.Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);

// Fill with results
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
// Check for ACCESS_FINE_LOCATION
            if (perms.get(android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                initialiseIncomingCallListener();
            } else {
                Toast.makeText(getApplicationContext(), "Permissions has not been granted", Toast.LENGTH_LONG).show();

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
// Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
                return false;
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(MakeVideoCallActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    @Override
    public void onSignalReceived(Session session, String type, String data, Connection connection) {
        switch (type){
            case SIGNAL_TYPE_CLOSE:
                sessionClosed = data;
                isClosedByPublisher = true;
// onDisconnected(session);
                break;
        }
    }

    @Override
    public void onNetworkAvailable() {
        if (!firstFlag) {

            if(mSubscriber!=null) {
                if (mSubscriberViewContainer != null)
                    mSubscriberViewContainer.removeView(mSubscriber.getView());
                if (mPublisherViewContainer != null)
                    mPublisherViewContainer.removeView(mPublisher.getView());
                if (mSession != null)
                    mSession.disconnect();
                if (mPublisher != null)
                    mPublisher.destroy();
                if (mSubscriber != null)
                    mSubscriber.destroy();
                mPublisher = null;
                mSession = null;
                mSubscriber = null;
            }
            initializeSession();
            initializePublisher();
        }
    }

    @Override
    public void onNetworkUnavailable() {
// showDialog();
        callStatus.setText("Reconnecting...");
        pauseTimer = true;
        initializeTimer = false;
    }
}
