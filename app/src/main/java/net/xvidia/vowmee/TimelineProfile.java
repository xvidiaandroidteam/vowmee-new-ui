package net.xvidia.vowmee;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.mobile.content.UserFileManager;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.FacebookSdk;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.helper.DividerItemDecoration;
import net.xvidia.vowmee.listadapter.BottomListAdapter;
import net.xvidia.vowmee.listadapter.ILoadMoreItems;
import net.xvidia.vowmee.listadapter.TimelineRecyclerViewAdapter;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;
import net.xvidia.vowmee.videoplayer.IVideoDownloadListener;
import net.xvidia.vowmee.videoplayer.VideosDownloader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static net.xvidia.vowmee.MyApplication.getAppContext;

public class TimelineProfile extends AppCompatActivity implements IVideoDownloadListener, SwipeRefreshLayout.OnRefreshListener {
    private final int GALLERY_INTENT_CALLED = 1234;
    private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 123;
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 125;
//    private boolean isLive;
    private Activity activity;
    private UserFileManager userFileManager;
    private boolean refrshed;
    private Context context;
    private RecyclerView mRecyclerView;
    private TimelineRecyclerViewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Moment> momentList;
    VideosDownloader videosDownloader;
    private ProgressDialog progressDialog;
    private FloatingActionButton videoMenu;
    private TextView mMessageTextView;
    //    private TwitterLoginButton twitterLoginButton;
    private SwipeRefreshLayout swipeRefreshLayout;
    public static RelativeLayout layout;
    private Animation slideUp;
    private static Animation slideDown;
    private ImageButton cancel;
    private LinearLayout tintLayout;
    private boolean canscroll;
    String[] bottomListItemName = {
            MyApplication.getAppContext().getResources().getString(R.string.menu_go_live),
//            MyApplication.getAppContext().getResources().getString(R.string.menu_create_event),
            MyApplication.getAppContext().getResources().getString(R.string.menu_record_video),
            MyApplication.getAppContext().getResources().getString(R.string.menu_upload_video)
    };
    //    public static boolean closelist;
    int[] bottomListImages = new int[]{
            R.drawable.go_live,
//            R.drawable.event,
            R.drawable.record_new,
            R.drawable.upload_new
    };


    private boolean isLoading;
    private int currentPage;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount, firstVisibleItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);
        FacebookSdk.sdkInitialize(this);
        activity = this;
        TwitterAuthConfig authConfig = new TwitterAuthConfig(getString(R.string.twitter_consumer_key), getString(R.string.twitter_secret_key));
        Fabric.with(this, new TwitterCore(authConfig), new TweetComposer());
        ListView listView = (ListView) findViewById(R.id.bottomlistview);
        listView.setAdapter(new BottomListAdapter(this, bottomListItemName, bottomListImages));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {

                    case 0:
                        if (Utils.getInstance().checkCameraHardware(context)) {
//                            isLive = true;
                            DataStorage.getInstance().setIsLiveFlag(true);
                            callCameraApp(true);
                        } else {
                            showError("Your phone does not support camera", null);
                        }
                        CloseBottomListView();
                        break;

                    case 1:
                        if (Utils.getInstance().checkCameraHardware(context)) {
//                            isLive = false;
                            DataStorage.getInstance().setIsLiveFlag(false);
                            callCameraApp(false);
                        } else {
                            showError("Your phone does not support camera", null);
                        }
                        CloseBottomListView();
                        break;

                    case 2:
                        selectVideo();
                        CloseBottomListView();
                        break;

                }
            }
        });
        layout = (RelativeLayout) findViewById(R.id.bottomLayout);
        tintLayout = (LinearLayout) findViewById(R.id.tint_layout);
        cancel = (ImageButton) findViewById(R.id.cancel);
        tintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseBottomListView();
            }
        });
        tintLayout.setClickable(false);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseBottomListView();
            }
        });
        slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        videoMenu = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        videoMenu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                slideToTop(tintLayout);
                layout.startAnimation(slideUp);
                layout.setVisibility(View.VISIBLE);

            }
        });
        mRecyclerView = (RecyclerView) findViewById(R.id.timeline_recycler_view);
        mMessageTextView = (TextView) findViewById(R.id.message_no_moments);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
//        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.appcolor);
        swipeRefreshLayout.setColorSchemeResources(R.color.init_gradient, R.color.com_facebook_blue, R.color.red_color);
//        swipeRefreshLayout.setDistanceToTriggerSync(20);
        userFileManager = Utils.getInstance().getUserFileManager();
//        twitterLoginButton = (TwitterLoginButton) findViewById(R.id.twitter_button);
//        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
//            @Override
//            public void success(Result<TwitterSession> result) {
//                postTwitterAppInvite();
//            }
//
//            @Override
//            public void failure(TwitterException exception) {
//            }
//        });
        context = TimelineProfile.this;
        momentList = new ArrayList<>();
        refrshed = false;
        initialiseRecyclerView();
        mMessageTextView.setVisibility(View.GONE);

    }

//    private boolean addPermission(List<String> permissionsList, String permission) {
//        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
//            permissionsList.add(permission);
//            // Check for Rationale Option
//            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
//                return false;
//        }
//        return true;
//    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(TimelineProfile.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<String, Integer>();
            // Initial
            perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
            perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
            perms.put(android.Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);

            // Fill with results
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
            // Check for ACCESS_FINE_LOCATION
            if (perms.get(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                    && perms.get(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && perms.get(android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                callCameraApp(DataStorage.getInstance().getIsLiveFlag());
            } else {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_message_camera_permission_denied), Utils.LONG_TOAST);
            }
        } else if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectVideo();
            } else {
                Utils.getInstance().displayToast(MyApplication.getAppContext(), MyApplication.getAppContext().getString(R.string.error_message_upload_permission_denied), Utils.LONG_TOAST);

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void slideToBottom(View view) {
        tintLayout.setClickable(false);
        TranslateAnimation animate = new TranslateAnimation(0, 0, 0, view.getHeight());
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);

    }

    public void slideToTop(View view) {
        tintLayout.setBackgroundColor(getResources().getColor(R.color.bottom_list_tint));
        tintLayout.setClickable(true);
        TranslateAnimation animate = new TranslateAnimation(0, 0, view.getHeight(), 0);
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);

    }

    public void CloseBottomListView() {

        if (layout.getVisibility() == View.VISIBLE) {
            slideToBottom(tintLayout);
//        tintLayout.setVisibility(View.INVISIBLE);
            layout.startAnimation(slideDown);
            layout.setVisibility(View.INVISIBLE);
        }

    }

    private void callCameraApp(boolean live) {

        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!Utils.getInstance().addPermission(permissionsList, android.Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!Utils.getInstance().addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write External Storage");
        if (!Utils.getInstance().addPermission(permissionsList, android.Manifest.permission.RECORD_AUDIO))
            permissionsNeeded.add("Record Audio");

        Map<String, Integer> perms = new HashMap<String, Integer>();
        // Initial
        perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
        perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
        perms.put(android.Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);

        if (permissionsList.size() == 0) {
            Intent mIntent = new Intent(TimelineProfile.this, RecordVideo.class);
            Bundle mBundle = new Bundle();
            mBundle.putBoolean(AppConsatants.LIVE_VIDEO, live);
            mIntent.putExtras(mBundle);
            startActivity(mIntent);
        } else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    // Need Rationale
                    String message = "You need to grant access to " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                }
                            });
                    return;
                }
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;

            }
         /*   if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
                showMessageOKCancel("You need to allow access to Camera",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });

                return;
            } else {
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;
            }*/
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        bindActivity();
    }

    private void bindActivity() {

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.title_Profile));
        mToolbar.setPadding(0, 0, 0, 0);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        momentList = new ArrayList<Moment>();
        showProgressBar(getString(R.string.progressbar_fetching_info));
        TimelineRecyclerViewAdapter.refreshimeline = false;
        sendMomentList(true);
//        getVideoUrls();
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void initialiseRecyclerView() {
        // RVAdapter adapter = new RVAdapter();

        if (momentList != null && momentList.size() > 0) {
            Moment obj = momentList.get(0);
            if (obj != null && obj.getOwner() != null) {
                Moment objDummy = new Moment();
                momentList.add(0, objDummy);
            }
        } else {
            momentList = new ArrayList<>();
            Moment objDummy = new Moment();
            momentList.add(0, objDummy);
        }

        swipeRefreshLayout.setRefreshing(false);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new TimelineRecyclerViewAdapter(true, true, TimelineProfile.this, activity, momentList); // true: with header
        final LinearLayoutManager layoutManager;
        canscroll = true;
        if (momentList != null && momentList.size() < 2)
            canscroll = false;
        layoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return canscroll;
            }
        };
        mRecyclerView.setLayoutManager(layoutManager);
        if (!refrshed) {
            refrshed = true;
            mRecyclerView.addItemDecoration(new DividerItemDecoration(20, true));
        }
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnLoadMoreListener(new ILoadMoreItems() {
            @Override
            public void loadMoreItems(boolean load) {
                    isLoading = true;
                    TimelineRecyclerViewAdapter.disableLoad= false;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadMore();
                        }
                    },450);
            }
        });
        videosDownloader = new VideosDownloader(context);
        videosDownloader.setOnVideoDownloadListener(this);

//        if(Utils.getInstance().hasConnection(context))
//        {
//            getVideoUrls();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
//                int visibleItemCount = layoutManager.getChildCount();
//                int totalItemCount = layoutManager.getItemCount();
//                int lastVisibleItemPos = layoutManager.findLastVisibleItemPosition();
////                Log.i("getChildCount", String.valueOf(visibleItemCount));
////                Log.i("getItemCount", String.valueOf(totalItemCount));
////                Log.i("lastVisibleItemPos", String.valueOf(lastVisibleItemPos));
//                if ((visibleItemCount + lastVisibleItemPos) >= totalItemCount) {
////                    Log.i("LOG", "Last Item Reached!");
//                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
                int findFirstCompletelyVisibleItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition();
//                Log.i("CompletelyVisibleItem", String.valueOf(findFirstCompletelyVisibleItemPosition));
//                Log.i("firstVisiblePosition", String.valueOf(firstVisiblePosition));
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                firstVisibleItem = firstVisiblePosition;
                Moment video;
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (momentList != null && momentList.size() > 1) {
                        if (findFirstCompletelyVisibleItemPosition > 0) {
                            video = momentList.get(findFirstCompletelyVisibleItemPosition);
                            mAdapter.videoPlayerController.setcurrentPositionOfItemToPlay(findFirstCompletelyVisibleItemPosition);
                            mAdapter.videoPlayerController.handlePlayBack(video, false);
                        } else if (firstVisiblePosition > 0) {
                            video = momentList.get(firstVisiblePosition);
                            mAdapter.videoPlayerController.setcurrentPositionOfItemToPlay(firstVisiblePosition);
                            mAdapter.videoPlayerController.handlePlayBack(video, false);
                        } else if (firstVisiblePosition > -1) {
                            video = momentList.get(1);
                            mAdapter.videoPlayerController.setcurrentPositionOfItemToPlay(1);
                            mAdapter.videoPlayerController.handlePlayBack(video, false);
                        }
                    }
                } else {
                    mAdapter.videoPlayerController.pauseVideo();
                    if (!isLoading && totalItemCount > 9 && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        isLoading = true;
                        momentList.add(null);
                        mAdapter.notifyItemInserted(momentList.size() - 1);

//                        Log.i("Loading More","currentPage ="+currentPage);
                    }
                }
            }
        });
    }

    private void loadMore() {
        try {
            final int previousPage = currentPage;
//            String friendUsername = ModelManager.getInstance().getOtherProfile().getUsername();
            final String username = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, username, username, AppConsatants.FALSE, "" + currentPage, "10");
//            Log.i("Loading completed","currentPage ="+currentPage +"\nUrl "+url);
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<Moment> obj = null;
                    try {
//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Moment>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    isLoading = false;
                    hideProgressBar();
                    Moment lastMoment = momentList.get(momentList.size() - 1);
                    if (lastMoment == null) {
                        momentList.remove(momentList.size() - 1);
                        mAdapter.notifyItemRemoved(momentList.size());
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {
                            if (momentList == null)
                                momentList = obj;
                            if (currentPage == previousPage) {
                                currentPage = previousPage + 1;
                                mMessageTextView.setVisibility(View.GONE);
                                momentList.addAll(obj);
                                videosDownloader.startVideosDownloading(obj, true);

//                            HashSet<Moment> set = new HashSet<>();
//                            set.addAll(momentList);
//                            momentList.clear();
//                            momentList.addAll(set);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                int curSize = mAdapter.getItemCount();
                                mAdapter.setMomentList(momentList);
                                mAdapter.notifyItemRangeInserted(curSize, momentList.size() - 1);
                            }
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    isLoading = false;
                    currentPage = currentPage - 1;
                    if (currentPage < previousPage)
                        currentPage = previousPage;
                    if (momentList != null) {
                        if (momentList.size() > 0) {
                            Moment lastMoment = momentList.get(momentList.size() - 1);
                            if (lastMoment == null) {
                                momentList.remove(momentList.size() - 1);
                                mAdapter.notifyItemRemoved(momentList.size());
                            }
                        }
                        if (momentList != null && momentList.size() > 0) {
                            mMessageTextView.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.VISIBLE);
                        } else {
                            mMessageTextView.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                        }
                    }
                }
            }) {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        //                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        if (userFileManager != null) {
            userFileManager.destroy();
        }
        super.onDestroy();
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user_profile, menu);
        return true;
    }*/

    private void selectVideo() {
//        sendMomentList();
        if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent();
            intent.setType("video/*");
            intent.setAction(Intent.ACTION_PICK);
//            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//            intent.setType("video/*");
            startActivityForResult(intent, GALLERY_INTENT_CALLED);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showMessageOKCancel("You need to allow access to Media Storage",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);

                            }
                        });

                return;
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);

                return;
            }
        }

    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedImageUri = null;
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_INTENT_CALLED) {
                selectedImageUri = data.getData();
                String path = Utils.getInstance().getPath(this, selectedImageUri, false);
                if(!path.isEmpty()) {
                    Intent mIntent = new Intent(TimelineProfile.this, UploadActivity.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString(AppConsatants.MOMENT_PATH, path);
                    mBundle.putBoolean(AppConsatants.RESHARE, false);
                    mIntent.putExtras(mBundle);
                    startActivity(mIntent);
                }

            }
        }
    }

    //New
   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
         *//*   case R.id.action_video:
//                AWSMobileClient.defaultMobileClient().getIdentityManager().signOut();
                Intent mIntentVideo = new Intent(TimelineTabs.this, RecordVideo.class);
                startActivity(mIntentVideo);
//                selectVideo();
                break;*//*
            case R.id.action_activity:
//                AWSMobileClient.defaultMobileClient().getIdentityManager().signOut();
                Intent mIntentActivity = new Intent(TimelineProfile.this, TimelineActivityTabs.class);
                startActivity(mIntentActivity);
                break;
            case R.id.action_timeline:
//                AWSMobileClient.defaultMobileClient().getIdentityManager().signOut();
                Intent mIntentProfile = new Intent(TimelineProfile.this, TimelineTabs.class);
                startActivity(mIntentProfile);
                finish();
                break;
            case R.id.action_search:
//                AWSMobileClient.defaultMobileClient().getIdentityManager().signOut();
                Intent mIntentSearch = new Intent(TimelineProfile.this, SearchActivity.class);
                startActivity(mIntentSearch);
                break;
            case R.id.action_setting:
                break;
            case R.id.action_facebook_search:

                if(AWSMobileClient.defaultMobileClient()!= null &&
                        AWSMobileClient.defaultMobileClient().getIdentityManager() != null &&
                        AWSMobileClient.defaultMobileClient().getIdentityManager().getFacebookFriendList()!= null &&
                        AWSMobileClient.defaultMobileClient().getIdentityManager().getFacebookFriendList().size()>0) {
                    openPersonList(false, false, false, true);
                }else{
                    showMessage(getString(R.string.error_invite_facebook), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sendFacebookInvites();
                        }
                    });
                }
                break;
            case R.id.action_invite_facebook_friends:
                sendFacebookInvites();

                break;
            case R.id.action_twitter_invite:
                postTwitterAppInvite();
                break;
            case R.id.action_logout:
                AWSMobileClient.defaultMobileClient().getIdentityManager().signOut();
                DataStorage.getInstance().setRegisteredFlag(false);
                Intent mIntentLogin = new Intent(TimelineProfile.this, RegisterActivity.class);
                startActivity(mIntentLogin);
                finish();
                break;
            default:
                break;
        }

        return true;
    }
*/


    private void getProfileRequest() {
        try {
            final String username = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getProfileUrl(IAPIConstants.API_KEY_GET_PROFILE, username, username);
            VolleySingleton.getInstance(MyApplication.getAppContext()).refresh(url, true);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Profile obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Profile.class);
                    } catch (IOException e) {
                    }catch (OutOfMemoryError e) {
                    }catch (Exception e) {
                    }
                    String getUsername = obj.getUsername();
                    if (getUsername != null && !getUsername.isEmpty() && username.equals(getUsername)) {
                        ModelManager.getInstance().setOwnProfile(obj);
                        if(!DataStorage.getInstance().getProfileImagePath().isEmpty()&&mAdapter!=null)
                            mAdapter.notifyDataSetChanged();
//                        if(mAdapter!=null)
//                             mAdapter.notifyDataSetChanged();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {
                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        final long cacheHitButRefreshed = 30 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + cacheHitButRefreshed;
                        final long ttl = now + cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONObject(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONObject response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {

        }
    }

    @Override
    protected void onResume() {
        getProfileRequest();
        if(PublishLiveActivity.closePublish){
            closePublishSessionConnection();
        }
        setGoogleAnalyticScreen("My Profile");
        super.onResume();
    }
//    private void updateProfileImage() {
//    }
    public void closePublishSessionConnection() {

        if (PublishLiveActivity.mSessionId == null) {
            return;
        }
        if (PublishLiveActivity.mSessionId.isEmpty()) {
            return;
        }
        String subUrl = ServiceURLManager.getInstance().getDestroyLive(PublishLiveActivity.mSessionId, DataStorage.getInstance().getUserUUID());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, subUrl, "{}", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    PublishLiveActivity.mSessionId="";
                    PublishLiveActivity.closePublish = false;
                    String url = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, DataStorage.getInstance().getUsername(), DataStorage.getInstance().getUsername(), AppConsatants.TRUE, "0", "0");
                    VolleySingleton.getInstance(getAppContext()).refresh(url, false);
                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    @Override
    public void onVideoDownloaded(Moment video, boolean image) {
        mAdapter.videoPlayerController.handlePlayBack(video, image);
    }

    private void sendMomentList(final boolean firstTime) {
        try {
            currentPage = 1;
            final String username = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getMomentTimelineListUrl(IAPIConstants.API_KEY_GET_MOMENT_TIMELINE_LIST, username, username, AppConsatants.FALSE, "1", "10");

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    ObjectMapper mapper = new ObjectMapper();
                    List<Moment> obj = null;
                    try {
//                        obj = mapper.readValue(response.toString(), MomentList.class);
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Moment>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    hideProgressBar();
                    if (obj != null) {

                        if (obj.size() > 0) {
                            currentPage = 2;
                            momentList = obj;
                            initialiseRecyclerView();
                            mAdapter.notifyDataSetChanged();
                            mMessageTextView.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.VISIBLE);
                            videosDownloader.startVideosDownloading(momentList, firstTime);
                        } else {
                            initialiseRecyclerView();
                            mAdapter.notifyDataSetChanged();
                            mRecyclerView.setVisibility(View.VISIBLE);
                            if (momentList.size() == 1)
                                mMessageTextView.setVisibility(View.VISIBLE);
                            else
                                mMessageTextView.setVisibility(View.GONE);
//                            showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    finish();
//                                }
//                            });
                        }
//
//                    }else{
//                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                finish();
//                            }
//                        });
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    if (momentList != null && momentList.size() > 0) {
                        if (momentList.size() == 1)
                            mMessageTextView.setVisibility(View.VISIBLE);
                        else
                            mMessageTextView.setVisibility(View.GONE);
                    } else {
                            mMessageTextView.setVisibility(View.VISIBLE);

                    }
                    mRecyclerView.setVisibility(View.VISIBLE);
                }
            }) {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        final long cacheHitButRefreshed =AppConsatants.cacheHitButRefreshed; // in 3 minutes cache will be hit, but also refreshed on background
                        final long cacheExpired = AppConsatants.cacheExpired; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + cacheHitButRefreshed;
                        final long ttl = now + cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showError(final String mesg, final DialogInterface.OnClickListener okClicked) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new AlertDialog.Builder(TimelineProfile.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .setCancelable(false)
                            .show();
                }
            });
        } catch (WindowManager.BadTokenException e) {
        } catch (Exception e) {
        }
    }

    private void showProgressBar(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                progressDialog = ProgressDialog.show(context, null, null, true, false);
//                progressDialog.setContentView(R.layout.progressbar);
//                TextView progressBarMessage = (TextView) progressDialog.findViewById(R.id.progressBar_message);
//                progressBarMessage.setText(msg);
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(true);
            }
        });

    }

    private void hideProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

//                if (progressDialog != null) {
//                    progressDialog.dismiss();
//                }
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (layout.getVisibility() == View.VISIBLE) {
            CloseBottomListView();
        } else {
//            Intent mIntent = new Intent(TimelineProfile.this, TimelineTabs.class);
//            startActivity(mIntent);
            finish();
            super.onBackPressed();
        }
    }

    @Override
    public void onRefresh() {
//        getProfileRequest();
        TimelineRecyclerViewAdapter.refreshimeline = false;
        sendMomentList(false);
    }

    private void setGoogleAnalyticScreen(String screenName){
        MyApplication application = (MyApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.setClientId(DataStorage.getInstance().getUserUUID());
        mTracker.setAppVersion(""+Utils.getInstance().getAppVersion(MyApplication.getAppContext()));
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

}
