package net.xvidia.vowmee;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.Moment;
import net.xvidia.vowmee.storage.DataStorage;
import net.xvidia.vowmee.videoplayer.IVideoDownloadListener;
import net.xvidia.vowmee.videoplayer.VideoPlayerController;
import net.xvidia.vowmee.videoplayer.VideosDownloader;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import static net.xvidia.vowmee.MyApplication.getAppContext;

public class TimelineMomentDetail extends AppCompatActivity implements IVideoDownloadListener {

    private Context context;
    VideosDownloader videosDownloader;
    private String momentUuid;
    public VideoPlayerController videoPlayerController;
    public RelativeLayout layout;

    public ProgressBar progressBar;
    public VideoView videoPlayer;
    private boolean notificationIntent;
    private boolean uploadVideo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timeline_moment_detail);

        context = TimelineMomentDetail.this;
        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            momentUuid = intent.getStringExtra(AppConsatants.UUID);
            notificationIntent = intent.getBooleanExtra(AppConsatants.NOTIFICATION_NEXT_INTENT, false);
            uploadVideo = intent.getBooleanExtra(AppConsatants.UPLOAD_FILE_PATH, false);
        }
        if (momentUuid == null) {
            finish();
        }
        if (momentUuid.isEmpty()) {
            finish();
        }
        bindActivity();
    }

    private void initialiseView() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        layout = (RelativeLayout) findViewById(R.id.preview_videoplayer);
        videoPlayer = (VideoView) findViewById(R.id.videoplayer);
    }

    private void initialiseViewValue(final Moment obj, boolean uploadVideo1) {
        if (!uploadVideo1) {
            if (obj == null)
                return;
            if (obj.getLink() == null)
                return;
            if (obj.getLink().isEmpty())
                return;
        }
//        videoPlayer = new VideoPlayer(context,Utils.getInstance().getLocalContentPath(obj.getLink()));
//        videoPlayer.setFullscreen(true);
//        RelativeLayout.LayoutParams paramVideos = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//        paramVideos.addRule(Gravity.CENTER);// = Gravity.CENTER;
//        videoPlayer.setLayoutParams(paramVideos);

        videoPlayer.requestLayout();
        progressBar.setVisibility(View.VISIBLE);
        MediaController mc = new MediaController(this);
        mc.setAnchorView(videoPlayer);
        videoPlayer.setMediaController(mc);
        videoPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progressBar.setVisibility(View.GONE);
                mp.setVolume(1.0f, 1.0f);
                mp.start();
                mp.setLooping(true);
            }
        });
        String filePathImage = "";
        if (uploadVideo1) {
            filePathImage = momentUuid;
        } else {
            filePathImage = Utils.getInstance().getLocalContentPath(obj.getLink());
        }
        if (filePathImage.isEmpty())
            return;
//        String filePathImage = Utils.getInstance().getLocalContentPath(obj.getLink());
        File file = new File(filePathImage);
        if (file.exists()) {
            if (videoPlayer != null && !videoPlayer.isPlaying()) {
                videoPlayer.setVideoPath(file.getAbsolutePath());
                videoPlayer.start();
            }
        } else {
            String url = getAppContext().getString(R.string.s3_url);
            String profImagePath = obj.getLink();
            url = url + profImagePath;
            url = url.replace("+", "%2B");
            videoPlayer.setVideoURI(Uri.parse(url));
//            videosDownloader.startVideosDownloading(obj);
        }

    }
//    @Override
//    protected void onPostCreate(Bundle savedInstanceState) {
//        super.onPostCreate(savedInstanceState);
//
//    }

    private void bindActivity() {
        videoPlayerController = new VideoPlayerController(this);
        videosDownloader = new VideosDownloader(this);
        videosDownloader.setOnVideoDownloadListener(this);

        initialiseView();
//        showProgressBar(getString(R.string.progressbar_fetching_info));
        if (notificationIntent) {
            getMomentRequest();
        } else if (uploadVideo) {
            lockOrientation(Utils.getInstance().getMomentObj(), true);
            initialiseViewValue(Utils.getInstance().getMomentObj(), true);
        } else if (Utils.getInstance().getMomentObj() != null) {
            lockOrientation(Utils.getInstance().getMomentObj(), false);
            initialiseViewValue(Utils.getInstance().getMomentObj(), false);
        } else {
            getMomentRequest();
        }
    }

    private void lockOrientation(Moment obj, boolean uploadVideo) {
//            Configuration configuration = MyApplication.getAppContext().getResources().getConfiguration();
//            int screenHeightDp = configuration.screenWidthDp;
//            int deviceWidth= Utils.getInstance().convertDpToPixel(screenHeightDp,this );
        String url = "";
        if (uploadVideo) {
            url = momentUuid;
        } else {
            url = Utils.getInstance().getLocalContentPath(obj.getLink());
        }
        if (url.isEmpty())
            return;
        File file = new File(url);
        if (file.exists()) {
            MediaMetadataRetriever fetchVideoInfo = new MediaMetadataRetriever();
            fetchVideoInfo.setDataSource(url);
            String height = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
            String width = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
            String orientation = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
            if (Build.VERSION.SDK_INT >= 17) {
                String s = fetchVideoInfo.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
                Log.e("Rotation", s);
            }
//                int intHeight = Utils.getInstance().getIntegerValueOfString(width);
//                int intWidth = Utils.getInstance().getIntegerValueOfString(height);
            int intWidth, intHeight;
            if (orientation.equalsIgnoreCase("90")) {
                intHeight = Utils.getInstance().getIntegerValueOfString(width);
                intWidth = Utils.getInstance().getIntegerValueOfString(height);
            } else {
                intWidth = Utils.getInstance().getIntegerValueOfString(width);
                intHeight = Utils.getInstance().getIntegerValueOfString(height);
            }
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            if (intWidth > intHeight) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            } else if (intHeight > intWidth) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }

        } else {

        }
    }

    private void getMomentRequest() {
        try {

            final String username = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getMomentByUuid(IAPIConstants.API_KEY_GET_MOMENT_BY_UUID, momentUuid, username);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    Moment obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), Moment.class);
                    } catch (IOException e) {
                    } catch (OutOfMemoryError e) {
                    } catch (Exception e) {
                    }
                    String getUuid = obj.getUuid();
                    obj.setIndexPosition("0");
                    if (getUuid != null && !getUuid.isEmpty() && momentUuid.equals(getUuid)) {
//                        getSupportActionBar().setTitle(obj.getCaption());

                        lockOrientation(obj, false);
                        initialiseViewValue(obj, false);
                    } else {
                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        } catch (Exception e) {

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoPlayer != null && videoPlayer.isPlaying()) {
            videoPlayer.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        restartAudioMode();
    }

    //    private void isVideoDownloaded(Moment video) {
//
//        String filePath = Utils.getInstance().getLocalContentPath(video.getLink());
//        File vidFile = new File(filePath);
//        boolean isVideoAvailable = vidFile.exists();
//        if(isVideoAvailable){
//            if(vidFile == null) {
//                videosDownloader.startVideosDownloading(video);
//                return;
//            }
//
//            hideProgressSpinner();
//        }else{
//            showProgressSpinner();
//        }
//        Log.i("isVideoDownloaded ", "isVideoAvailable " + isVideoAvailable + "Moment" + video.toString());
//
//    }


    public void restartAudioMode() {
        AudioManager Audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        Audio.setMode(AudioManager.MODE_NORMAL);
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
    }

    @Override
    public void onVideoDownloaded(Moment video, boolean image) {
//        videoPlayerController.handlePlayBack(video, false);
        if (videoPlayer != null && !videoPlayer.isPlaying()) {
            videoPlayer.setVideoPath(Utils.getInstance().getLocalContentPath(video.getLink()));
            videoPlayer.start();
        }
//        isVideoDownloaded(video);
    }

    private void showError(final String mesg, final DialogInterface.OnClickListener okClicked) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new AlertDialog.Builder(TimelineMomentDetail.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .setCancelable(false)
                            .show();
                }
            });
        } catch (WindowManager.BadTokenException e) {
        } catch (Exception e) {
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//        Log.i("onConfigurationChanged ", "onConfigurationChanged ");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (videoPlayer != null) {
            videoPlayer.stopPlayback();
        }
        if (notificationIntent) {
            Intent intent = new Intent(context, TimelineTabs.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            finish();
        }
    }

}
