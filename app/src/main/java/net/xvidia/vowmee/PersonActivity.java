package net.xvidia.vowmee;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amazonaws.mobile.AWSMobileClient;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.xvidia.vowmee.Utils.AppConsatants;
import net.xvidia.vowmee.Utils.Utils;
import net.xvidia.vowmee.gcm.NotifBuilderSingleton;
import net.xvidia.vowmee.helper.CustomFontEditTextView;
import net.xvidia.vowmee.listadapter.FacebookListAdapter;
import net.xvidia.vowmee.listadapter.FollowerListAdapter;
import net.xvidia.vowmee.listadapter.FollowingListAdapter;
import net.xvidia.vowmee.listadapter.FriendListAdapter;
import net.xvidia.vowmee.listadapter.RecyclerViewScrollListener;
import net.xvidia.vowmee.network.IAPIConstants;
import net.xvidia.vowmee.network.ServiceURLManager;
import net.xvidia.vowmee.network.VolleySingleton;
import net.xvidia.vowmee.network.model.FacebookFriend;
import net.xvidia.vowmee.network.model.ModelManager;
import net.xvidia.vowmee.network.model.Profile;
import net.xvidia.vowmee.storage.DataStorage;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class PersonActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    public static final int REQUEST_CODE_ASK_CALL_PERMISSIONS = 127;
    private Context context;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private List<Profile> pendingList;
    private List<Profile> searchList;
    private ProgressDialog progressDialog;
    private boolean friends;
    private boolean followers;
    private boolean following;
    private boolean others;
    private boolean facebookFriends;
    private boolean notificationIntent;
    private int notificationId, idInboxStyle;
    private Toolbar toolbar;
    private String usernameOwnProfile, usernameOtherProfile;
    private int pendingRequestCount;
    private TextView mMessageTextView;
    private CustomFontEditTextView mSearchQueryTextView;

    private ProgressBar progressBar;
    private String quesryString;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mMessageTextView = (TextView) findViewById(R.id.message_no_results);
        mMessageTextView.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView_list);
        mRecyclerView.setHasFixedSize(true);

        initialiseRecyclerView();
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.init_gradient, R.color.com_facebook_blue, R.color.red_color);
//        swipeRefreshLayout.setDistanceToTriggerSync(20);


        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        context = PersonActivity.this;
        toolbar.setPadding(0, 0, 0, 0);
        mMessageTextView = (TextView) findViewById(R.id.message_no_results);
        mSearchQueryTextView = (CustomFontEditTextView) findViewById(R.id.search_query);
        mSearchQueryTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                quesryString = s.toString().toLowerCase();

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (quesryString != null) {
                    if (searchList != null) {
                        if (quesryString.length() > 0) {

                            final List<Profile> filteredList = new ArrayList<>();
                            for (int i = 0; i < searchList.size(); i++) {

                                final String text = searchList.get(i).toString().toLowerCase();
                                if (text.contains(quesryString)) {
                                    filteredList.add(searchList.get(i));
                                }
                            }
                            if (followers) {
                                mAdapter = new FollowerListAdapter(PersonActivity.this, filteredList);
                            } else if (facebookFriends) {
                                mAdapter = new FacebookListAdapter(PersonActivity.this, filteredList);
                            } else if (following) {
                                mAdapter = new FollowingListAdapter(PersonActivity.this, filteredList);
                            } else {
                                mAdapter = new FriendListAdapter(PersonActivity.this, filteredList);

                            }
                            mRecyclerView.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            if (followers) {
                                mAdapter = new FollowerListAdapter(PersonActivity.this, searchList);
                            } else if (facebookFriends) {
                                mAdapter = new FacebookListAdapter(PersonActivity.this, searchList);
                            } else if (following) {
                                mAdapter = new FollowingListAdapter(PersonActivity.this, searchList);
                            } else {
                                mAdapter = new FriendListAdapter(PersonActivity.this, searchList);
                            }
                            mRecyclerView.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }
        });
        mSearchQueryTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Utils.getInstance().hideKeyboard(PersonActivity.this, mSearchQueryTextView);
                    return true;
                }
                return false;
            }
        });
        mMessageTextView.setVisibility(View.GONE);

        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            friends = intent.getBooleanExtra(AppConsatants.PERSON_FRIEND, false);
            followers = intent.getBooleanExtra(AppConsatants.PERSON_FOLLOWER, false);
            following = intent.getBooleanExtra(AppConsatants.PERSON_FOLLOWING, false);
            others = intent.getBooleanExtra(AppConsatants.OTHER_PERSON_PROFILE, false);
            facebookFriends = intent.getBooleanExtra(AppConsatants.PERSON_FACEBOOK, false);
            notificationIntent = intent.getBooleanExtra(AppConsatants.NOTIFICATION_NEXT_INTENT, false);
            notificationId = intent.getIntExtra(AppConsatants.NOTIFICATION_ID_INTENT, 0);
            idInboxStyle = intent.getIntExtra(AppConsatants.IDINBOXSTYLE, 0);
        }
        if (others) {
            if (ModelManager.getInstance().getOwnProfile() != null) {
                usernameOwnProfile = ModelManager.getInstance().getOwnProfile().getUsername();
            } else {
                finish();
            }
            if (ModelManager.getInstance().getOtherProfile() != null) {
                usernameOtherProfile = ModelManager.getInstance().getOtherProfile().getUsername();
            } else {
                finish();
            }
        } else {
                usernameOwnProfile = DataStorage.getInstance().getUsername();
                usernameOtherProfile = usernameOwnProfile;
        }
        if(notificationIntent) {
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancel(notificationId);
            NotifBuilderSingleton.getInstance().resetInboxStyle(idInboxStyle);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        showProgressBar();
        if(progressBar!=null)
            progressBar.setVisibility(View.VISIBLE);
        initialiseData();
    }

    private void initialiseData() {
        if (following) {
            getSupportActionBar().setTitle(getString(R.string.title_following));
            mSearchQueryTextView.setHint(getString(R.string.title_following));
            sendListRequest(IAPIConstants.API_KEY_GET_FOLLOW_LIST, usernameOwnProfile, usernameOtherProfile, "0", "0");
            setGoogleAnalyticScreen("Following");
        } else if (followers) {
            getSupportActionBar().setTitle(getString(R.string.title_followers));
            mSearchQueryTextView.setHint(getString(R.string.title_followers));
            sendListRequest(IAPIConstants.API_KEY_GET_FOLLOWERS_LIST, usernameOwnProfile, usernameOtherProfile, "0", "0");
            setGoogleAnalyticScreen("Followers");
        } else if (facebookFriends) {
            getSupportActionBar().setTitle(getString(R.string.title_facebook_friends));
            mSearchQueryTextView.setHint(getString(R.string.title_facebook_friends));
            getFacebookFriendList();
            setGoogleAnalyticScreen("Facebook Friends");
        } else {
            getSupportActionBar().setTitle(getString(R.string.title_friends));
            mSearchQueryTextView.setHint(getString(R.string.title_friends));
            sendListRequest(IAPIConstants.API_KEY_GET_FRIEND_LIST, usernameOwnProfile, usernameOtherProfile, "0", "0");
            setGoogleAnalyticScreen("Friends");
        }
    }

    private void initialiseRecyclerView() {
        swipeRefreshLayout.setRefreshing(false);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        if (followers) {
            mAdapter = new FollowerListAdapter(PersonActivity.this, searchList);
        } else if (facebookFriends) {
            mAdapter = new FacebookListAdapter(PersonActivity.this, searchList);
        } else if (following) {
            mAdapter = new FollowingListAdapter(PersonActivity.this, searchList);
        } else {
            mAdapter = new FriendListAdapter(PersonActivity.this, searchList);
        }
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerViewScrollListener(this) {
            @Override
            public void onMoved(int distance) {
                toolbar.setTranslationY(-distance);
            }
        });
    }


    private void getFacebookFriendList() {
        List<FacebookFriend> facebookFriendList = AWSMobileClient.defaultMobileClient().getIdentityManager().getFacebookFriendList();
        ArrayList<String> list = null;
        if (facebookFriendList != null && facebookFriendList.size() > 0) {
            if (ModelManager.getInstance().getFacebookFriendList() == null) {

                list = new ArrayList<>();
                for (FacebookFriend obj : facebookFriendList) {
                    list.add(obj.getId());
                }
                getFacebookFriendInfoRequest(list);
            } else {
                hideProgressBar();
                searchList = ModelManager.getInstance().getFacebookFriendList();
                initialiseRecyclerView();
                if (mAdapter != null)
                    mAdapter.notifyDataSetChanged();
            }
        }
    }
      /*Api Request & Response
      * Moment added to s3 bucket*/



    private void getFacebookFriendInfoRequest(ArrayList<String> stringArrayList) {
        try {

            String url = ServiceURLManager.getInstance().getFacebookAddList(IAPIConstants.API_KEY_GET_PERSON_BY_FACEBOOK_ID, DataStorage.getInstance().getUsername());

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(stringArrayList);
            } catch (IOException e) {
                e.printStackTrace();
            }
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    List<Profile> obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Profile>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {
                            if (searchList != null)
                                searchList.clear();
                            searchList = obj;
                            ModelManager.getInstance().setFacebookFriendList(searchList);
                            //Log..i("Moment uuid", "" + obj.get(0).toString());
                            initialiseRecyclerView();
                            if (mAdapter != null)
                                mAdapter.notifyDataSetChanged();
                        } else {
                            Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_try_again), Utils.LONG_TOAST);
                        }
                    } else {
                        if(searchList==null) {
                            showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                            return;
                        }else if( searchList.size()==0) {
                            showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                            return;
                        }else {
                            Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_no_internet), Utils.LONG_TOAST);
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressBar();
                    if(searchList==null) {
                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        return;
                    }else if( searchList.size()==0) {
                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        return;
                    }else {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_no_internet), Utils.LONG_TOAST);
                    }
                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    private void sendListRequest(int urlKey, String usernameOwn, String usernameOther, String page, String size) {
        try {

            String url = ServiceURLManager.getInstance().getListUrl(urlKey, usernameOwn, usernameOther, page, size);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    hideProgressBar();
                    ObjectMapper mapper = new ObjectMapper();
                    List<Profile> obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<List<Profile>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {

                            mMessageTextView.setVisibility(View.GONE);
                            if (searchList != null)
                                searchList.clear();
                            searchList = obj;
                            initialiseRecyclerView();
                            if (mRecyclerView != null)
                                mRecyclerView.setVisibility(View.VISIBLE);

                            if (mAdapter != null)
                                mAdapter.notifyDataSetChanged();

                        } else {
//                            if (pendingRequestCount > 0) {
//                                if (searchList != null)
//                                    searchList.clear();
//                                searchList = obj;
//                                initialiseRecyclerView();
//                                if (mAdapter != null)
//                                    mAdapter.notifyDataSetChanged();
//                            } else {
                            if(searchList!=null && searchList.size()==0) {
                                mMessageTextView.setVisibility(View.VISIBLE);
                                if (mRecyclerView != null)
                                    mRecyclerView.setVisibility(View.GONE);
                            }
//                            }
                        }
                    } else {
                        if(searchList==null) {
                            showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                            return;
                        }else if( searchList.size()==0) {
                            showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                            return;
                        }else {
                            Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_no_internet), Utils.LONG_TOAST);
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    if(searchList==null) {
                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        return;
                    }else if( searchList.size()==0) {
                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        return;
                    }else {
                        Utils.getInstance().displayToast(MyApplication.getAppContext(), getString(R.string.error_no_internet), Utils.LONG_TOAST);
                    }

                }
            }){
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        //                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + AppConsatants.cacheHitButRefreshed;
                        final long ttl = now + AppConsatants.cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONArray response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openProfile() {
        Intent mIntent = new Intent(this, TimelineProfile.class);
        startActivity(mIntent);
        finish();
    }

    private void showError( final String mesg, final DialogInterface.OnClickListener okClicked) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new AlertDialog.Builder(PersonActivity.this,R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(mesg)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .show();
                }
            });
        }catch (WindowManager.BadTokenException e){

        }catch (Exception e){

        }
    }

    private void showProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });

    }

    private void hideProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if(progressBar!=null)
                    progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (notificationIntent) {
            Intent intent = new Intent(context, TimelineTabs.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            finish();
        }
    }

    @Override
    public void onRefresh() {
        initialiseData();
    }
    private void callFriend(Profile obj){
//        Profile obj = ModelManager.getInstance().getOtherProfile();
        if(obj == null)
            return;
        String profImagePath = obj.getThumbNail();
        profImagePath = (profImagePath == null ? obj.getProfileImage() : (profImagePath.isEmpty() ?obj.getProfileImage(): obj.getUsername() + AppConsatants.FILE_EXTENSION_JPG));

        Intent mIntent = new Intent(context, MakeCallActivity.class);
        Bundle mBundle = new Bundle();
        mBundle.putString(AppConsatants.UUID, obj.getUuid());
        mBundle.putString(AppConsatants.USER_THUMBNAIL,profImagePath);
        mBundle.putString(AppConsatants.PERSON_FRIEND, obj.getDisplayName());
        mIntent.putExtras(mBundle);
        mIntent.replaceExtras(mBundle);
        context.startActivity(mIntent);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
         if(requestCode == REQUEST_CODE_ASK_CALL_PERMISSIONS){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                callFriend(ModelManager.getInstance().getOtherProfile());
            }else{
                Utils.getInstance().displayToast(MyApplication.getAppContext(),MyApplication.getAppContext().getString(R.string.error_message_upload_permission_denied), Utils.LONG_TOAST);
            }
        }else{
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private void setGoogleAnalyticScreen(String screenName){
        MyApplication application = (MyApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.setClientId(DataStorage.getInstance().getUserUUID());
        mTracker.setAppVersion(""+Utils.getInstance().getAppVersion(MyApplication.getAppContext()));
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

}
